<?php
//Set flag for parent file
define('PG_PARENT', 1);

//Include file(s)
require_once 'configuration.php';
require_once 'system.php';;
if (CFG_GESHI) {
	require_once 'libraries/geshi.php';
}

//Create application
$app = new App();
$app->connect(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);
$app->init();

$app->query("ALTER TABLE  `imb` ADD  `imbPungutanBendaharaID` INT NOT NULL , ADD  `imbPungutanMengetahuiID` INT NOT NULL;");
$app->query("ALTER TABLE  `imb` ADD  `imbPungutanNama` VARCHAR( 500 ) NOT NULL;");
$app->query("ALTER TABLE  `imb` ADD  `imbPungutanJumlah` INT NOT NULL;");
$app->query("CREATE TABLE IF NOT EXISTS `pungutanimb` (
		  `pimbImbID` int(11) NOT NULL,
		  `pimbNama` varchar(500) NOT NULL,
		  `pimbNilai1` decimal(18,2) NOT NULL,
		  `pimbSatuan1` varchar(50) NOT NULL,
		  `pimbNilai2` decimal(18,2) NOT NULL,
		  `pimbSatuan2` varchar(50) NOT NULL,
		  `pimbNilai3` decimal(18,2) NOT NULL,
		  `pimbSatuan3` varchar(50) NOT NULL,
		  `pimbNilaiRupiah` int(11) NOT NULL,
		  `pimbUnit` int(11) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

echo 'Done';		
?>