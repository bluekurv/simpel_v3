//Variables
var natDays = [];

//Functions
function backup() {
	if (confirm("Backup basisdata?")) {
		window.open('index2.php?act=database', '_self');
	}
}

function noWeekendsOrHolidays(date) {
	var noWeekend = $.datepicker.noWeekends(date);
	if (noWeekend[0]) {
		return nationalDays(date);
	} else {
		return noWeekend;
	}
}

function nationalDays(date) {
	for (i = 0; i < natDays.length; i++) {
		if (date.getMonth() == natDays[i][0] - 1 && date.getDate() == natDays[i][1] && date.getFullYear() == natDays[i][2]) {
			return [false, '', natDays[i][3]];
		}
	}
	return [true, ''];
}

$(document).ready(function(){
	$('input[placeholder], textarea[placeholder]').placeholder();
	
	jQuery('ul.sf-menu').superfish();
	
	$(".bulat").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
	$(".desimal").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
	$(".desimal1").maskMoney({ thousands:'.', decimal:',', precision:1, defaultZero:true, allowZero:true });
	$(".desimal2").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });

	//Untuk semua tanggal, seperti entri kalender
	$("input.box.date").datepicker({
		yearRange: "-90:+10",
		changeMonth: true, 
		changeYear: true,
		dateFormat: "dd-mm-yy"
	});
	
	//Untuk tanggal selain hari sabtu, minggu, dan libur nasional
	//Jangan lupa menambahkan sbb sebelum skrip di act yang bersesuaian:
	//	natDays = <?php echo json_encode($app->getHolidays()); ?>;
	$("input.box.date2").datepicker({ 
		yearRange: "-90:+10",
		changeMonth: true, 
		changeYear: true,
		dateFormat: "dd-mm-yy",
		beforeShowDay: noWeekendsOrHolidays 
	});
	
	$(".cb-enable").click(function(){
		var parent = $(this).parents('.switch');
		$('.cb-netral',parent).removeClass('selected');
		$('.cb-disable',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.cb',parent).attr('checked', true);
		
		if (typeof(window[$(this).attr('for') + "Fn"]) === 'function') { 
		    window[$(this).attr('for') + "Fn"]();
		}
	});

	$(".cb-netral").click(function(){
		var parent = $(this).parents('.switch');
		$('.cb-enable',parent).removeClass('selected');
		$('.cb-disable',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.cb',parent).attr('checked', false);
		
		if (typeof(window[$(this).attr('for') + "Fn"]) === 'function') { 
		    window[$(this).attr('for') + "Fn"]();
		}
	});

	$(".cb-disable").click(function(){
		var parent = $(this).parents('.switch');
		$('.cb-enable',parent).removeClass('selected');
		$('.cb-netral',parent).removeClass('selected');
		$(this).addClass('selected');
		$('.cb',parent).attr('checked', false);
		
		if (typeof(window[$(this).attr('for') + "Fn"]) === 'function') { 
		    window[$(this).attr('for') + "Fn"]();
		}
	});
	
	var timer = $.timer(function() {
		$.getJSON(
			CFG_LIVE_PATH + '/check.php',  
			function(json) { 
				if (json.success) {
					$('#hInformation').hide();
				} else {
					$('#hInformation').show();
				}
				$('#hMsg').html(json.msg);
			}  
		);
	});
	
	timer.set({ time : 60000, autostart : true });
});