<?php
//Set flag for parent file
define('PG_PARENT', 1);

//Include file(s)
require_once 'configuration.php';
require_once 'system.php';

//Create application
$app = new App();
$app->init();

//Check access
if ($_SESSION['sesipengguna']->ID > 0) { 
	$result = new stdClass();
	$result->success = true;
	$result->msg = '';
	echo json_encode($result);
} else {
	$result = new stdClass();
	$result->success = false;
	$result->msg = 'Sesi login sudah berakhir. Silahkan logout, kemudian login lagi';
	echo json_encode($result);
}
?>