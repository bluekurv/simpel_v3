<?php
//Include file(s)
require_once 'configuration.php';
require_once 'system.php';
if (CFG_GESHI) {
	require_once 'libraries/geshi.php';
}

//Create application
$app = new App();
$app->init();
$app->connect(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);

//Get parameter(s)
$login = $app->getInt('login', 1);

//Start output buffering
ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title><?php echo CFG_SITE_NAME; ?></title>
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="icon" href="images/favicon.ico" />
	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<!-- Scripts -->
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/index.js"></script>
</head>
<marquee bgcolor="green" style="font-family: impact; font-size:16px; color:#ffffff;" onmouseover="this.stop()" onmouseout="this.start()">Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan </marquee>
<body style="background:url('images/pattern-batik.jpg');">
<?php
	$starttime = microtime(true);
?>
	<div id="b">
		<div id="bContent">
<!-- Start main body -->
			<div class="halaman login">
				<h1 style="margin-top:50px;"><?php echo strtoupper(CFG_SITE_NAME); ?></h1>
				<h2><?php echo strtoupper(CFG_COMPANY_NAME); ?></h2>
				<a href="index.php"><img src="images/logo.png" border="0"/></a>
<?php
	if ($login < 0) {
		$app->showMessage(false, 'Sesi login sudah berakhir. Silahkan login lagi');
	} else if ($login == 0) {
		$app->showMessage(false, 'Login gagal. Nama pengguna salah, kata kunci salah atau anda tidak diperbolehkan mengakses');
	}
?>
 				<br>
				<div style="height:100px;">
					<p>Masukkan Nama Pengguna dan Kata Kunci Anda</p>
					<form id="myForm" name="myForm" action="index2.php?act=pengguna&task=login&html=0" method="POST">
						<input class="box" id="username" name="username" maxlength="15" size="20" placeholder="Nama pengguna" value="" />
						<input class="box" type="password" id="password" name="password" maxlength="15" size="20" placeholder="Kata kunci" value="" />
						<input class="button-link inline" type="submit" id="login" name="login" value="Login" />
					</form>
				</div>
			</div>
<!-- End main body -->
			<br><br>
		</div>
	</div>
<?php
	$endtime = microtime(true);
	$duration = $endtime - $starttime;
?>
	<div id="f">
		<div id="fContent">
			<?php echo CFG_COMPANY_NAME; ?>, 2013 - <?php echo date('Y'); ?>. This page took <?php echo round($duration, 4); ?> seconds to load.
		</div>
	</div>
</body>
</html>
<?php
//ob_end_clean();

//End output buffering and send our HTML to the browser as a whole
ob_end_flush();
?>