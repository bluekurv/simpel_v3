<?php
class App {
	//Database
	public $connection;
	public $sql;
	
	//Variable(s)
	public $act;
	public $detail;
	public $task;
	public $subtask;
	public $id;
	public $id2;
	public $success;
	public $msg;
	public $html;
	
	public $fromact;
	public $fromtask;
	public $fromid;
	
	//Specific
	public $sqlNo = 0;
	
	public $kalender = array();
	public $bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember" );
	
	//OK
	public function init() {
		//This function has no effect when PHP is running in safe mode. 
		//There is no workaround other than turning off safe mode or changing the time limit in the php.ini. 
		set_time_limit(CFG_TIME_LIMIT) ;
		
		//Use session
		session_name(CFG_SESSION_NAME);
		session_start();
		
		if (!isset($_SESSION['sesipengguna'])) {
			$_SESSION['sesipengguna'] = new UserSession();
		}
	}

	//OK
	public function debug($var) {
		if ((is_array($var)) || (is_object($var))) {
			echo '<xmp style="text-align:left;">';
			print_r($var);
			echo '</xmp>';
		} else {
			echo $var;
		}
	}
	
	public function createSelectGroup($id, $val) {
?>
		<select class="box" id="<?php echo $id; ?>" name="<?php echo $id; ?>">
			<option value="0">Tidak Ada</option>
<?php
		$arr = array();
		
		//Query
		$sql = "SELECT * FROM grup ORDER BY grupParentID, grupNama";
		$rs = $this->query($sql);
		
		while(($x = mysql_fetch_object($rs)) == true){
			$arr[$x->grupParentID][] = $x;
		}
		
		if (count($arr[0]) > 0) {
			foreach ($arr[0] as $v) {
?>
							<option value="<?php echo $v->grupID; ?>" <?php echo $val == $v->grupID ? 'selected' : ''; ?>><?php echo $v->grupNama; ?></option>
<?php
				if (isset($arr[$v->grupID])) {
					foreach ($arr[$v->grupID] as $v2) {
?>
							<option value="<?php echo $v2->grupID; ?>" <?php echo $val == $v2->grupID ? 'selected' : ''; ?>>--- <?php echo $v2->grupNama; ?></option>
<?php
						if (isset($arr[$v2->grupID])) {
							foreach ($arr[$v2->grupID] as $v3) {
?>
							<option value="<?php echo $v3->grupID; ?>" <?php echo $val == $v3->grupID ? 'selected' : ''; ?>>--- --- <?php echo $v3->grupNama; ?></option>
<?php
								
							}
						}
					}
				}
			}
		}
?>
		</select>
<?php
	}
	
	/*
	 * Page related
	 */
	
	//OK
	public function pageVar($act, $var, $default, $filter = '') {
		//NOTE: nilai $act boleh bersumber selain dari act=
		
		if (isset($_SESSION[$act.'-'.$var])) {
			if (isset($_REQUEST[$var])) {
				switch ($filter) {
					case 'intval':
						$_SESSION[$act.'-'.$var] = intval($_REQUEST[$var]);
						break;
					case 'strval':
						$_SESSION[$act.'-'.$var] = mysql_escape_string(trim($_REQUEST[$var]));
						break;
					default:
						$_SESSION[$act.'-'.$var] = $_REQUEST[$var];
						break;
				}
			} else {
				
			}
		} else {
			if (isset($_REQUEST[$var])) {
				switch ($filter) {
					case 'intval':
						$_SESSION[$act.'-'.$var] = intval($_REQUEST[$var]);
						break;
					case 'strval':
						$_SESSION[$act.'-'.$var] = mysql_escape_string(trim($_REQUEST[$var]));
						break;
					default:
						$_SESSION[$act.'-'.$var] = $_REQUEST[$var];
						break;
				}
			} else {
				$_SESSION[$act.'-'.$var] = $default;
			}
		}
		
		return $_SESSION[$act.'-'.$var];
	}
	
	//OK
	public function showPageError($msg = 'Halaman yang anda cari tidak ditemukan. Biasanya hal ini dikarenakan anda tidak memiliki hak akses ke halaman ini, halaman tidak dipublikasikan, alamat (URL) yang dimasukkan belum benar atau sudah berubah.') {
	?>
		<div class="halaman" style="text-align:center;">
			<br><br><br>
			<img src="<?php echo CFG_LIVE_PATH; ?>/images/exclamation.png" />
			<h1>Error 404</h1>
			<p><?php echo $msg; ?></p>
		</div>
	<?php
	}

	//OK
	public function showMessage($success, $msg) {
		$class = $success ? 'msgOK' : 'msgError';
		
		if ($msg != '') {
			echo '<br/><div class="'.$class.'">'.$msg.'</div>';
		}
	}
	
	//SKIP
	public function showPageLoginFirst() {
?>
	<div class="halaman" style="text-align:center;">
		<br><br><br>
		<img src="<?php echo CFG_LIVE_PATH; ?>/images/exclamation.png" />
		<h1>Error 404</h1>
		<p>Halaman ini tidak dapat diakses. Silahkan <a href="index.php">login</a>.</p>
	</div>
<?php
	}

	/**
	* Menentukan header pada tabel
	*
	* @param string 	key
	* @param string 	text label
	* @param boolean 	is sortable?
	* @param integer 	lebar kolom (0 = tidak ditentukan)
	* @param string 	alignment ('' = tidak ditentukan)
	*/
	//OK
	public function setHeader($key, $text, $sortable, $width = 0, $align = '', $colspan = 1) {
		$obj = new stdClass();
		$obj->key = $key;
		$obj->text = $text;
		$obj->sortable = $sortable;
		$obj->width = $width;
		$obj->align = $align;
		$obj->colspan= $colspan;
		return $obj;
	}
	
	/**
	* Menampilkan header pada tabel
	*
	* @param array of object
	* @param string 	sort field
	* @param string 	direction
	*/
	//OK
	public function displayHeader($cols, $sort, $dir, $id = 0) {
		$str = '<tr>';
		if (count($cols) > 0) {
			foreach ($cols as $col) {
				$str .= '<th'.($col->colspan > 1 ? ' colspan="'.$col->colspan.'"' : '').($col->align != '' ? ' align="'.$col->align.'"' : '').($col->width > 0 ? ' width="'.$col->width.'"' : '').'>';
				if ($col->sortable) {
					if ($col->key == $sort) {
						if ($dir == 'asc') {
							$str .= '<a href="javascript:sort(\''.$col->key.'\',\'desc\''.($id > 0 ? ','.$id : '').');">';
						} else {
							$str .= '<a href="javascript:sort(\''.$col->key.'\',\'asc\''.($id > 0 ? ','.$id : '').');">';
						}
					} else {
						//Always start using ascending direction
						$str .= '<a href="javascript:sort(\''.$col->key.'\',\'asc\''.($id > 0 ? ','.$id : '').');">';
					}
				}
				$str .= $col->text;
				if ($col->sortable) {
					$str .= '</a>';
				}
				if ($col->key == $sort) {
					if ($dir == 'asc') {
						$str .= '<img src="images/icons/s_asc.png">';
					} else {
						$str .= '<img src="images/icons/s_desc.png">';
					}
				}
				$str .= '</th>';
			}
		}
		$str .= '</tr>';
		return $str;
	}

	/**
	* Membuat select box berisikan tahun (2000 s/d sepuluh tahun dari sekarang)
	*
	* @param string 	nama
	* @param integer 	selected ID
	* @param array 		blank
	* @param string 	style
	*/
	//OK
	public function createSelectYear($nama, $id, $blank = array(), $style = "") {
		echo '<select class="box" id="'.$nama.'" name="'.$nama.'"';
		if ($style != "") {
			echo " style='".$style."'";
		}
		echo '>';
		if (count($blank) > 0) {
			foreach ($blank as $k=>$v) {
				echo '<option value="'.$k.'"';
				if ($k == $id) {
					echo ' selected';
				}
				echo ">".$v."</option>\n";
			}
		}
		for ($i=2000; $i<=date('Y')+10; $i++) {
			echo '<option value="'.$i.'"';
			if ($i == $id) {
				echo ' selected';
			}
			echo ">".$i."</option>\n";
		}
		echo '</select>';
	}
	
	//OK
	public function createSelectMonth($nama, $id, $blank = array(), $style = "") {
		echo '<select class="box" id="'.$nama.'" name="'.$nama.'"';
		if ($style != "") {
			echo " style='".$style."'";
		}
		echo '>';
		if (count($blank) > 0) {
			foreach ($blank as $k=>$v) {
				echo '<option value="'.$k.'"';
				if ($k == $id) {
					echo ' selected';
				}
				echo ">".$v."</option>\n";
			}
		}
		foreach ($this->bulan as $k=>$v) {
		echo '<option value="'.$k.'"';
			if ($k == $id) {
				echo ' selected';
			}
			echo ">".$v."</option>\n";
		}
		echo '</select>';
	}
	
	/**
	* Membuat select box berdasarkan SQL
	*
	* @param string 	nama
	* @param string 	SQL (SELECT x AS id, y AS nama FROM Z)
	* @param integer 	selected ID
	* @param array 		blank
	*/
	//OK
	public function createSelect($nama, $sql, $id, $blank = array()) {
		$arr = array();
		
		$rs = $this->query($sql);
		if ($rs) {
			while(($obj = mysql_fetch_object($rs)) == true){
				$arr[$obj->id] = $obj->nama;
			}
		}
		
		echo '<select class="box" id="'.$nama.'" name="'.$nama.'">';
		if (count($blank) > 0) {
			foreach ($blank as $k=>$v) {
				echo '<option value="'.$k.'"';
				if ($k == $id) {
					echo ' selected';
				}
				echo ">".$v."</option>\n";
			}
		}
		if (count($arr) > 0) {
			foreach ($arr as $k=>$v) {
				echo '<option value="'.$k.'"';
				if ($k == $id) {
					echo ' selected';
				}
				echo ">".(strlen($v) > 50 ? substr($v, 0, 50).'...' : $v)."</option>\n";
			}
		}
		echo '</select>';
	}
	
	//OK
	public function createSwitchCheckBox($nama, $posValue, $posText, $negValue, $negText, $value) {
	?>
		<div class="switch">
			<label class="cb-enable<?php echo ($value == $posValue) ? ' selected' : ''; ?>"><span><?php echo $posText; ?></span></label>
			<label class="cb-disable<?php echo ($value == $negValue) ? ' selected' : ''; ?>"><span><?php echo $negText; ?></span></label>
			<input type="checkbox" class="cb" id="<?php echo $nama; ?>" name="<?php echo $nama; ?>" <?php echo ($value == $posValue) ? 'checked' : ''; ?> style="display:none;"/>
			<label for=<?php echo $nama; ?> style="display:none;"><?php echo $posText; ?></label>
		</div>
	<?php
	}
	
	//OK
	public function createSwitchRadio($nama, $posValue, $posText, $negValue, $negText, $value) {
	?>
		<div class="switch">
		    <input type="radio" id="<?php echo $nama; ?>1" name="<?php echo $nama; ?>" value="<?php echo $posValue; ?>" style="display:none;" <?php echo ($value == $posValue) ? 'checked' : ''; ?>/>
		    <input type="radio" id="<?php echo $nama; ?>2" name="<?php echo $nama; ?>" value="<?php echo $negValue; ?>" style="display:none;" <?php echo ($value == $negValue) ? 'checked' : ''; ?> />
		    <label for="<?php echo $nama; ?>1" class="cb-enable<?php echo ($value == $posValue) ? ' selected' : ''; ?>"><span><?php echo $posText; ?></span></label>
		    <label for="<?php echo $nama; ?>2" class="cb-disable<?php echo ($value == $negValue) ? ' selected' : ''; ?>"><span><?php echo $negText; ?></span></label>
		</div>
	<?php
	}
	
	//OK
	public function createSwitchRadioThree($nama, $value1, $text1, $value2, $text2, $value3, $text3, $value) {
	?>
		<div class="switch">
		    <input type="radio" id="<?php echo $nama; ?>1" name="<?php echo $nama; ?>" value="<?php echo $value1; ?>" style="display:none;" <?php echo ($value == $value1) ? 'checked' : ''; ?>/>
		    <input type="radio" id="<?php echo $nama; ?>2" name="<?php echo $nama; ?>" value="<?php echo $value2; ?>" style="display:none;" <?php echo ($value == $value2) ? 'checked' : ''; ?> />
		    <input type="radio" id="<?php echo $nama; ?>3" name="<?php echo $nama; ?>" value="<?php echo $value3; ?>" style="display:none;" <?php echo ($value == $value3) ? 'checked' : ''; ?> />
		    <label for="<?php echo $nama; ?>1" class="cb-enable<?php echo ($value == $value1) ? ' selected' : ''; ?>"><span><?php echo $text1; ?></span></label>
		    <label for="<?php echo $nama; ?>2" class="cb-netral<?php echo ($value == $value2) ? ' selected' : ''; ?>"><span><?php echo $text2; ?></span></label>
		    <label for="<?php echo $nama; ?>3" class="cb-disable<?php echo ($value == $value3) ? ' selected' : ''; ?>"><span><?php echo $text3; ?></span></label>
		</div>
	<?php
	}
	
	//OK
	public function displayNavigation($arr) {
		if ($arr['totalPage']) {
			echo 'Halaman ';
			
			if ($arr['page'] > 1) { 
				echo '<a href="javascript:lihat(1)" title="Halaman Awal"><img src="images/icons/resultset_first.png" border="0"></a>';
			} else {
				echo '<img src="images/icons/resultset_first-grayed.png" border="0">';
			}
			
			if ($arr['page'] > 1) {
				echo '<a href="javascript:lihat('.($arr['page']-1).')" title="Halaman Sebelumnya"><img src="images/icons/resultset_previous.png" border="0"></a>';
			} else {
				echo '<img src="images/icons/resultset_previous-grayed.png" border="0">';
			}
			
			echo '<select class="box" id="page" name="page">';
			for ($i=1;$i<=$arr['totalPage'];$i++) {
				echo '<option';
				if ($i == $arr['page']) {
					echo ' selected';
				}
				echo '>'.$i.'</option>';
			}
			echo '</select>';
			
			if ($arr['page'] < $arr['totalPage']) {
				echo '<a href="javascript:lihat('.($arr['page']+1).')" title="Halaman Berikutnya"><img src="images/icons/resultset_next.png" border="0"></a>';
			} else {
				echo '<img src="images/icons/resultset_last-grayed.png" border="0">';
			}
			
			if ($arr['page'] < $arr['totalPage']) {
				echo '<a href="javascript:lihat('.$arr['totalPage'].')" title="Halaman Akhir"><img src="images/icons/resultset_last.png" border="0"></a>';
			} else {
				echo '<img src="images/icons/resultset_last-grayed.png" border="0">';
			}
			
			echo 'dari '.$arr['totalPage'].', menampilkan ';
			
			if ($arr['totalData'] == 1) {
				echo '1 data';
			} else {
				if ($arr['startData'] == $arr['endData']) {
					echo $arr['startData'].' dari '.$arr['totalData'].' data';
				} else {
					echo $arr['startData'].' - '.$arr['endData'].' dari '.$arr['totalData'].' data';
				}
			}
		}
	}
	
	// $param bernilai kombinasi l, r, t, b
	function border($param = 'lrtb', $color = '#000000') {
		$border = array();
	
		if (stripos($param, 'l') !== false) {
		    $border[] = 'border-left: solid 1px '.$color.';';
		}
		if (stripos($param, 'r') !== false) {
		    $border[] = 'border-right: solid 1px '.$color.';';
		}
		if (stripos($param, 't') !== false) {
		    $border[] = 'border-top: solid 1px '.$color.';';
		}
		if (stripos($param, 'b') !== false) {
		    $border[] = 'border-bottom: solid 1px '.$color.';';
		}
		
		if (count($border) == 4) {
			$border = array('border: solid 1px '.$color.';');
		}
		
		return implode(' ',$border);
	}
	
	/*
	 * File related
	 */
	
	// This function will create a thumbnail of the uploaded image
	// NEW: check if source image is available, and suppress warning from imagecreatefromjpeg() and imagecreatefrompng()
	public function createImageThumbnail($img_name, $filename, $ext, $thumb_width, $thumb_height) {
		$doCreateThumbnail = true;
		
		//creates the new image using the appropriate function from gd library
		if ($ext == "jpg" || $ext == "jpeg") {
			$src_img = @imagecreatefromjpeg($img_name);
		}
		if ($ext == "png") {
			$src_img = @imagecreatefrompng($img_name);
		}
		if ($ext == "gif") {
			$src_img = @imagecreatefromgif($img_name);
		}
		
		//See if it failed 
		if (!$src_img) {
			$doCreateThumbnail = false;
		} else {
			//gets the dimensions of the image
			$width = imagesx($src_img);
			$height = imagesy($src_img);
			
			$original_aspect = $width / $height;
			$thumb_aspect = $thumb_width / $thumb_height;
			
			if($original_aspect >= $thumb_aspect) {
			   //if image is wider than thumbnail (in aspect ratio sense)
			   $new_height = $thumb_height;
			   $new_width = $width / ($height / $thumb_height);
			} else {
			   //if the thumbnail is wider than the image
			   $new_width = $thumb_width;
			   $new_height = $height / ($width / $thumb_width);
			}
			
			$dst_img = imagecreatetruecolor($thumb_width, $thumb_height);
			
			//resize and crop
			imagecopyresampled($dst_img,$src_img,
			                   0 - ($new_width - $thumb_width) / 2, //center the image horizontally
			                   0 - ($new_height - $thumb_height) / 2, //center the image vertically
			                   0, 0,
			                   $new_width, $new_height, $width, $height);
			
			//output the created image to the file. Now we will have the thumbnail into the file named by $filename
			if ($ext == "jpg" || $ext == "jpeg") {
				imagejpeg($dst_img,$filename,100);	//Default 75 
			}
			if ($ext == "png") {
				imagepng($dst_img,$filename); 
			}
			if ($ext == "gif") {
				imagepng($dst_img,$filename); 
			}
			
			//destroys source and destination images. 
			imagedestroy($dst_img); 
			imagedestroy($src_img); 
		}
		
		return $doCreateThumbnail;
	}
	
	// This function will create a scaled down version of the uploaded image
	// The resize will be done considering the width and height defined, without deforming the image
	public function resizeImage($img_name, $filename, $ext, $new_w, $new_h) {
		//creates the new image using the appropriate function from gd library
		if (!strcmp("jpg",$ext) || !strcmp("jpeg",$ext)) {
			$src_img = imagecreatefromjpeg($img_name);
		}
		if (!strcmp("png",$ext)) {
			$src_img = imagecreatefrompng($img_name);
		}
		
		//gets the dimensions of the image
		$old_x = imagesx($src_img);
		$old_y = imagesy($src_img);
		
		// next we will calculate the new dimmensions for the thumbnail image
		// the next steps will be taken: 
		//   1. calculate the ratio by dividing the old dimmensions with the new ones
		//	 2. if the ratio for the width is higher, the width will remain the one define in WIDTH variable
		//	    and the height will be calculated so the image ratio will not change
		//	 3. otherwise we will use the height ratio for the image
		//      as a result, only one of the dimmensions will be from the fixed ones
		$ratio1 = $old_x / $new_w;
		$ratio2 = $old_y / $new_h;
		if ($ratio1 > $ratio2)	{
			$thumb_w = $new_w;
			$thumb_h = $old_y / $ratio1;
		} else {
			$thumb_h = $new_h;
			$thumb_w = $old_x / $ratio2;
		}
	
		// we create a new image with the new dimmensions
		$dst_img = imagecreatetruecolor($thumb_w,$thumb_h);
	
		// resize the big image to the new created one
		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y); 
	
		// output the created image to the file. Now we will have the thumbnail into the file named by $filename
		if (!strcmp("png",$ext)) {
			imagepng($dst_img,$filename); 
		} else {
			imagejpeg($dst_img,$filename); 
		}
		
		//destroys source and destination images. 
		imagedestroy($dst_img); 
		imagedestroy($src_img); 
	}
	
	// This function reads the extension of the file. 
	// It is used to determine if the file is an image by checking the extension. 
	public function getFileExtension($str) {
		$i = strrpos($str,".");
		if (!$i) { 
			return ""; 
		}
		$l = strlen($str) - $i;
		$ext = substr($str,$i+1,$l);
		return $ext;
	}
	
	public function getFileSize($file, $type) {  
	    switch($type){  
	        case "KB":  
	            $filesize = filesize($file) * .0009765625; // bytes to KB  
	        	break;  
	        case "MB":  
	            $filesize = (filesize($file) * .0009765625) * .0009765625; // bytes to MB  
	        	break;  
	        case "GB":  
	            $filesize = ((filesize($file) * .0009765625) * .0009765625) * .0009765625; // bytes to GB  
	        	break;  
	    }  
	    
	    if ($filesize <= 0){  
	        return 'Unknown file size';  
		} else {
	    	return round($filesize, 2).' '.$type;
	   	}  
	}  
	
	// Recursively delete directories:
	// JANGAN DIGUNAKAN
	/*public function removeDirRecursively($dir) { 
		foreach(glob($dir . '/*') as $file) { 
			if(is_dir($file)) rrmdir($file); else unlink($file); 
	  	} 
	  	rmdir($dir); 
	}*/

	/*
	 * Security related
	 */
	
	public function checkACL($acl) {
		$allow = false;
		
		if (array_key_exists($_SESSION['sesipengguna']->levelAkses, $acl)) {
			if (array_key_exists('all', $acl[$_SESSION['sesipengguna']->levelAkses])) {
				$allow = $acl[$_SESSION['sesipengguna']->levelAkses]['all'];
			} else if (array_key_exists($this->task, $acl[$_SESSION['sesipengguna']->levelAkses])) {
				$allow = $acl[$_SESSION['sesipengguna']->levelAkses][$this->task];
			}
		}
		
		return $allow;
	}

	/**
	* Menulis log aplikasi
	*
	* @param string 	koneksi
	* @param integer 	EV_INFORMASI, EV_PERINGATAN, EV_KESALAHAN
	* @param string 	jenis obyek (nama tabel)
	* @param integer 	ID obyek (primary key pada tabel)
	* @param string 	nama obyek (pengenalan teks)
	* @param string 	aksi (ditampilkan)
	*/
	public function writeLog($tipe, $obyek, $obyekID, $obyekNama, $aksi) {
		$sql = "INSERT INTO eventviewer (evTipe, evObyek, evObyekID, evObyekNama, evAksi, evDibuatPada, evDibuatOlehID, evDibuatOlehNama) 
				VALUES ('".$tipe."', '".$obyek."', '".$obyekID."', '".$obyekNama."', '".$aksi."', '".date("Y-m-d H:i:s")."', '".$_SESSION['sesipengguna']->ID."', '".$_SESSION['sesipengguna']->namaPengguna."')";
		$this->query($sql);
	}
	
	/*
	 * Database related
	 */
	
	public function connect($host, $user, $password, $database) {
		$this->connection = mysql_connect($host, $user, $password);
		if (!$this->connection) {
			die("Tidak dapat terkoneksi dengan host ".$host);
		}
		
		$db = mysql_select_db($database, $this->connection);
		if (!$db) {
			die("Tidak dapat terkoneksi dengan database ".$database);
		}
	}
	
	public function bind($obj) {
		foreach (array_keys(get_object_vars($obj)) as $k) {
			if (isset($_REQUEST[$k])) {
				if ($_REQUEST [$k] != "") {
					//$obj->$k = htmlspecialchars(mysql_escape_string(trim($_REQUEST[$k])), ENT_QUOTES);
					$obj->$k = mysql_escape_string(trim($_REQUEST[$k]));
				}
			}
		}
	}
	
	public function createSQLforInsert($obj) {
		$formatSQL = "INSERT INTO ".$obj->tabel." ( %s ) VALUES ( %s )";
		
		foreach (get_object_vars($obj) as $k => $v) {
			if (is_array($v) or is_object($v)) {
				continue;
			}
			if (($k != "tabel") && ($k != "primaryKey") && ($k != $obj->primaryKey)) {
				$fields[] = "$k";
				$values[] = "'".$v."'";
			}
		}
		return sprintf($formatSQL, implode(", ", $fields), implode(", ", $values));
	}
	
	public function createSQLforUpdate($obj, $exclude = array()) {
		$fmtsql = "UPDATE ".$obj->tabel." SET %s WHERE %s";
		
		foreach (get_object_vars($obj) as $k => $v) {
			if (is_array ($v) or is_object($v)) {
				continue;
			}
			if (is_array($exclude)) {
				if (!in_array($k, $exclude)) {
					if ($k == $obj->primaryKey) {
						$where = $obj->primaryKey."='".$v."'";
					} else if (($k != "tabel") && ($k != "primaryKey")) {
						$tmp[] = " $k='".$v."'";
					}
				}
			} else {
				if ($k == $obj->primaryKey) {
					$where = $obj->primaryKey."='".$v."'";
				} else if (($k != "tabel") && ($k != "primaryKey")) {
					$tmp[] = " $k='".$v."'";
				}
			}
		}
		return sprintf($fmtsql, implode(",", $tmp), $where);
	}
	
	public function isExist($sql, $item) {
		$this->sql = $sql;
		
		$this->queryDebug($this->sql);
		
		$result = mysql_query($this->sql, $this->connection);
		if (!$result) {
			$this->queryError($this->sql);
		}
		
		$num = mysql_num_rows($result);
		if ($num > 0) {
			$obj = mysql_fetch_object($result);
			if ($item == $obj->id) {
				$doExist = true;
			} else {
				$doExist = false;
			}
		} else {
			$doExist = true;
		}
		
		return $doExist;
	}
	
	public function query($sql) {
		$this->sql = $sql;
		
		$this->queryDebug($this->sql);
		
		$result = mysql_query($this->sql, $this->connection);
		if (!$result) {
			$this->queryError($this->sql);
		}
		
		return $result;
	}
	
	public function queryArray($sql) {
		$this->sql = $sql;
		
		$this->queryDebug($this->sql);
		
		$result = mysql_query($this->sql, $this->connection);
		if (!$result) {
			$this->queryError($sql);
		}
		
		$data = mysql_fetch_array($result, MYSQL_ASSOC);
		
		mysql_free_result($result);
		
		return $data;
	}
	
	//As array of object
	public function queryArrayOfObjects($sql, $level = 1) {
		$this->sql = $sql;
		
		$this->queryDebug($this->sql);
		
		$result = mysql_query($this->sql, $this->connection);
		if (!$result) {
			$this->queryError($sql);
		}
		
		$arr = array();
		
		if ($level == 1) {
			while(($obj = mysql_fetch_object($result)) == true){
				$arr[$obj->id] = $obj;
			}
		} else {
			while(($obj = mysql_fetch_object($result)) == true){
				$arr[$obj->parentid][$obj->id] = $obj;
			}
		}
		
		mysql_free_result($result);
		
		return $arr;
	}
	
	public function queryObject($sql, $class = '') {
		$this->sql = $sql;
		
		$this->queryDebug($this->sql);
		
		$result = mysql_query($this->sql, $this->connection);
		if (!$result) {
			$this->queryError($this->sql);
		}
		
		if ($class == '') {
			$data = mysql_fetch_object($result);
		} else {
			$data = mysql_fetch_object($result, $class);
		}
		
		mysql_free_result($result);
		
		return $data;
	}
	
	public function queryField($sql) {
		$this->sql = $sql;
		
		$this->queryDebug($this->sql);
		
		$result = mysql_query($this->sql, $this->connection);
		if (!$result) {
			$this->queryError($this->sql);
		}
		
		$data = mysql_fetch_array($result);
		
		mysql_free_result($result);
		
		return $data[0];
	}
	
	public function queryDebug($sql) {
		if (CFG_DEBUG) {
			if (CFG_GESHI) {
				$sql = preg_replace('!\s+!', ' ', $sql);
				$geshi = new GeSHi($sql, 'sql');
				$geshi->set_header_type(GESHI_HEADER_DIV);
				$sql = $geshi->parse_code();
			}
			
			$this->sqlNo = $this->sqlNo + 1;
			echo '<div class="halaman debug">SQL ke-'.$this->sqlNo.': '.$sql.'</div>';
		}
	}
	
	public function queryError($sql) {
	?>
		<div class="halaman" style="text-align:center;">
			<br><br><br>
			<img src="<?php echo CFG_LIVE_PATH; ?>/images/exclamation.png" />
			<h1>Query Failed</h1>
	<?php 
		if (CFG_DEBUG) {
	?>
			<p>Halaman yang anda cari tidak ditemukan. Hal ini dikarenakan kesalahan query berikut : <br><span style="color:#FF0000;"><?php echo addslashes(mysql_error())." in query: ".addslashes($sql); ?></span>.</p>
	<?php 
		} else {
	?>
			<p>Halaman yang anda cari tidak ditemukan. Hal ini dikarenakan kesalahan query.</p>
	<?php 
		}
	?>
		</div>
	<?php
		exit();
	}

	/*
	 * Date related functions
	 */
	
	//OK
	public function MySQLDateToNormal($tgl, $return = '') {
		if (strlen($tgl) == 19) {
			if ($tgl != '0000-00-00 00:00:00') {
				return substr($tgl,8,2)."-".substr($tgl,5,2)."-".substr($tgl,0,4)." ".substr($tgl,11,8);
			} else {
				return $return;
			}
		} else if (strlen($tgl) == 10) {
			if ($tgl != '0000-00-00') {
				return substr($tgl,8,2)."-".substr($tgl,5,2)."-".substr($tgl,0,4);
			} else {
				return $return;
			}
		} else {
			return $tgl;
		}
	}
	
	//OK
	public function MySQLDateToIndonesia($tgl, $return = '') {
		if ($tgl == "0000-00-00" || $tgl == "1899-11-30") {
			return $return;
		} else {
			return intval(substr($tgl,8,2))." ".$this->bulan[intval(substr($tgl,5,2))]." ".substr($tgl,0,4);
		}
	}
	
	//OK
	public function NormalDateToMySQL($tgl) {
		//Received in 'd-m-Y', then returned in 'Y-m-d'
		if (strlen($tgl) == 10) {
			$arr = explode('-', $tgl);
			if (checkdate(intval($arr[1]), intval($arr[0]), intval($arr[2]))) {
				return $arr[2].'-'.$arr[1].'-'.$arr[0];
			} else {
				return "0000-00-00";
			}
		} else {
			return "0000-00-00";
		}
	}
	
	//OK
	public function timeDifference($date) {
	    if (empty($date)) {
	        return "No date provided";
	    }
	 
	    $periods = array("detik", "menit", "jam", "hari", "minggu", "bulan", "tahun", "dekade");
	    $lengths = array("60","60","24","7","4.35","12","10");
	 
	    $now = time();
	    $unix_date = strtotime($date);
	 
	    // check validity of date
	    if (empty($unix_date)) {
	        return "Bad date";
	    }
	 
	    // is it future date or past date
	    if ($now > $unix_date) {
	        $difference = $now - $unix_date;
	        $tense = "yang lalu";
	    } else {
	        $difference = $unix_date - $now;
	        $tense = "dari sekarang";
	    }
	 
	    for ($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
	        $difference /= $lengths[$j];
	    }
	 
	    $difference = round($difference);
	 
	    /*if ($difference != 1) {
	        $periods[$j].= "s";
	    }*/
	    
	    $result = new stdClass();
	    
	    if ($difference == 0) {
	    	$result->difference = $difference;
	    	$result->period = "baru saja";
	    	$result->label = "baru saja";
	    } else {
		    // is it future date or past date
		    if ($now > $unix_date) {
			    $result->difference = $difference;
		    } else {
		    	$result->difference = -$difference;
		    }
		    $result->period = "$periods[$j] {$tense}";
		    $result->label = "$difference $periods[$j] {$tense}";
	    }
	    
	    return $result;
	}
	
	//OK
	public function dateFromInterval($fromDate, $interval) {
		if (count($this->kalender) > 0) {
			//Belum pernah di-load sebelumnya
			$this->kalender = $this->queryArrayOfObjects("SELECT *, kalTgl AS id FROM kalender WHERE YEAR(kalTgl)='".date('Y')."'");
		}
		$kalender = $this->kalender;
		
		$currentday = $fromDate;
		$days = 0;
		$workdays = 0;
		
		while ($workdays < $interval) {
			$currentday = date('Y-m-d', strtotime($currentday." +1 days"));
			$days++;
			$w = date('w', strtotime($currentday));
			//echo $currentday.' : '.$w.'<br>';
			
			//This will be 0 for Sunday through 6, Saturday.
			while ($w == 0 || $w == 6 || isset($kalender[$currentday])) {
				$currentday = date('Y-m-d', strtotime($currentday." +1 days"));
				$days++;
				$w = date('w', strtotime($currentday));
				//echo $currentday.' : '.$w.'<br>';
			}
			
			$workdays++;
		}
		
		$result = new stdClass();
		$result->fromDate = $fromDate;
		$result->interval = $interval;
		$result->toDate = $currentday;
		$result->days = $days;
		$result->workdays = $workdays;
		
		return $result;
	}
	
	//OK
	public function _date_diff($one, $two) {
	    $invert = false;
	    if ($one > $two) {
	        list($one, $two) = array($two, $one);
	        $invert = true;
	    }
	
	    $key = array("y", "m", "d", "h", "i", "s");
	    $a = array_combine($key, array_map("intval", explode(" ", date("Y m d H i s", $one))));
	    $b = array_combine($key, array_map("intval", explode(" ", date("Y m d H i s", $two))));
	
	    $result = array();
	    $result["y"] = $b["y"] - $a["y"];
	    $result["m"] = $b["m"] - $a["m"];
	    $result["d"] = $b["d"] - $a["d"];
	    $result["h"] = $b["h"] - $a["h"];
	    $result["i"] = $b["i"] - $a["i"];
	    $result["s"] = $b["s"] - $a["s"];
	    $result["invert"] = $invert ? 1 : 0;
	    $result["days"] = intval(abs(($one - $two)/86400));
	
	    if ($invert) {
	        $this->_date_normalize($a, $result);
	    } else {
	        $this->_date_normalize($b, $result);
	    }
	    
	    //convert to object
	    $objResult = new stdClass();
		$objResult->y = $result["y"];
		$objResult->m = $result["m"];
		$objResult->d = $result["d"];
		$objResult->h = $result["h"];
		$objResult->i = $result["i"];
		$objResult->s = $result["s"];
		$objResult->invert = $result["invert"];
		$objResult->days = $result["days"];
		
	    return $objResult;
	}
	
	//OK
	function _date_range_limit($start, $end, $adj, $a, $b, $result) {
	    if ($result[$a] < $start) {
	        $result[$b] -= intval(($start - $result[$a] - 1) / $adj) + 1;
	        $result[$a] += $adj * intval(($start - $result[$a] - 1) / $adj + 1);
	    }
	
	    if ($result[$a] >= $end) {
	        $result[$b] += intval($result[$a] / $adj);
	        $result[$a] -= $adj * intval($result[$a] / $adj);
	    }
	
	    return $result;
	}
	
	//OK
	function _date_range_limit_days($base, $result) {
	    $days_in_month_leap = array(31, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	    $days_in_month = array(31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
	
	    $this->_date_range_limit(1, 13, 12, "m", "y", $base);
	
	    $year = $base["y"];
	    $month = $base["m"];
	
	    if (!$result["invert"]) {
	        while ($result["d"] < 0) {
	            $month--;
	            if ($month < 1) {
	                $month += 12;
	                $year--;
	            }
	
	            $leapyear = $year % 400 == 0 || ($year % 100 != 0 && $year % 4 == 0);
	            $days = $leapyear ? $days_in_month_leap[$month] : $days_in_month[$month];
	
	            $result["d"] += $days;
	            $result["m"]--;
	        }
	    } else {
	        while ($result["d"] < 0) {
	            $leapyear = $year % 400 == 0 || ($year % 100 != 0 && $year % 4 == 0);
	            $days = $leapyear ? $days_in_month_leap[$month] : $days_in_month[$month];
	
	            $result["d"] += $days;
	            $result["m"]--;
	
	            $month++;
	            if ($month > 12) {
	                $month -= 12;
	                $year++;
	            }
	        }
	    }
	
	    return $result;
	}

	//OK
	function _date_normalize($base, $result) {
	    $result = $this->_date_range_limit(0, 60, 60, "s", "i", $result);
	    $result = $this->_date_range_limit(0, 60, 60, "i", "h", $result);
	    $result = $this->_date_range_limit(0, 24, 24, "h", "d", $result);
	    $result = $this->_date_range_limit(0, 12, 12, "m", "y", $result);
	
	    $result = $this->_date_range_limit_days($base, $result);
	
	    $result = $this->_date_range_limit(0, 12, 12, "m", "y", $result);
	
	    return $result;
	}

	function getHolidays() {
		$arr = array();
		
		$sql = "SELECT * FROM kalender";
		$rs = $this->query($sql);
		
		while(($obj = mysql_fetch_object($rs)) == true){
			$arr[] = array(intval(substr($obj->kalTgl,5,2)), intval(substr($obj->kalTgl,8,2)), intval(substr($obj->kalTgl,0,4)), addcslashes($obj->kalNama, '"\\'));
		}
		
		return $arr;
	}
	
	/*
	 * Money related functions
	 */
	
	//input		x.xxx,xx
	//output	xxxx.xx
	//OK
	public function MoneyToMySQL($val) {
		if ($val == '' || $val == '0') {
			return '0.00';
		} else {
			$arr = explode('.', $val);
			$val = implode('', $arr);
			return str_replace(',', '.', $val);
		}
	}
	
	//input		xxxx.xx	DECIMAL (NOT VARCHAR)
	//output	x.xxx,xx
	//OK
	public function MySQLToMoney($val, $precision = 0, $task = 'print', $zero = 0) {
		if ($val == 0) {
			return $zero;
		} else {
			if (substr($task,0,7) == 'preview' || substr($task,0,5) == 'print') {
				if ($precision >= 0) {
					return number_format($val, $precision, ',', '.');
				} else {
					$val = floatval($val);
					$val2 = explode('.',$val);
					
					if (count($val2) > 1) {
						return number_format($val, strlen($val2[1]), ',', '.');
					} else {
						return $val;
					}
				}
			} else {
				//ekspor
				//NOTE: pada server inggris cukup return $val, sementara pada server indonesia, sbb:
				//      formatnya dilakukan di excel via style, jadi angka tetap bisa dihitung di excel
				if ($precision > 0) {
					return $val;
				} else {
					$val2 = explode('.',$val);
					
					return $val2[0];
				}
			}
		}
	}
	
	public function getRoman($value) {
		//Hanya sampai 12
		$roman = array(1=>'I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII');
		
		if (isset($roman[$value])) {
			return $roman[$value];
		} else {
			return '';
		}
	}
	
	//OK
	public function terhitung($arr) {
		if (is_array($arr)) {
			$total = 0;
			
			if (isset($arr['plus'])) {
				if (count($arr['plus']) > 0) {
					foreach ($arr['plus'] as $plus) {
						$total = bcadd($total, $plus, CFG_PRECISION);
					}
				}
			}
			
			if (isset($arr['minus'])) {
				if (count($arr['minus']) > 0) {
					foreach ($arr['minus'] as $minus) {
						$total = bcsub($total, $minus, CFG_PRECISION);
					}
				}
			}
			
			return $total;
		} else {
			return "Invalid value";
		}
	}
	
	//OK
	public function terbilang($bilangan) {
		$angka = array('0','0','0','0','0','0','0','0','0','0', '0','0','0','0','0','0');
		$kata = array('','satu','dua','tiga','empat','lima', 'enam','tujuh','delapan','sembilan');
		$tingkat = array('','ribu','juta','milyar','triliun');
		
		$panjang_bilangan = strlen($bilangan);
		
		/* pengujian panjang bilangan */
		if ($panjang_bilangan > 15) {
			$kalimat = "Diluar Batas";
			return $kalimat;
		}
		
		/* mengambil angka-angka yang ada dalam bilangan, dimasukkan ke dalam array */
		for ($i = 1; $i <= $panjang_bilangan; $i++) {
			$angka[$i] = substr($bilangan,-($i),1);
		}
		
		$i = 1;
		$j = 0;
		$kalimat = "";
		
		/* mulai proses iterasi terhadap array angka */
		while ($i <= $panjang_bilangan) {
			$subkalimat = "";
			$kata1 = "";
			$kata2 = "";
			$kata3 = "";
			
			/* untuk ratusan */
			if ($angka[$i+2] != "0") {
				if ($angka[$i+2] == "1") {
					$kata1 = "seratus";
				} else {
					$kata1 = $kata[$angka[$i+2]] . " ratus";
				}
			}
			
			/* untuk puluhan atau belasan */
			if ($angka[$i+1] != "0") {
				if ($angka[$i+1] == "1") {
					if ($angka[$i] == "0") {
						$kata2 = "sepuluh";
					} elseif ($angka[$i] == "1") {
						$kata2 = "sebelas";
					} else {
						$kata2 = $kata[$angka[$i]] . " belas";
					}
				} else {
					$kata2 = $kata[$angka[$i+1]] . " puluh";
				}
			}
			
			/* untuk satuan */
			if ($angka[$i] != "0") {
				if ($angka[$i+1] != "1") {
					$kata3 = $kata[$angka[$i]];
				}
			}
	
			/* pengujian angka apakah tidak nol semua, lalu ditambahkan tingkat */
			if (($angka[$i] != "0") || ($angka[$i+1] != "0") || ($angka[$i+2] != "0")) {
				$subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
			}
	
			/* gabungkan variabe sub kalimat (untuk satu blok 3 angka)	ke variabel kalimat */
			$kalimat = $subkalimat . $kalimat;
			$i = $i + 3;
			$j = $j + 1;
		}
		
		/* mengganti satu ribu jadi seribu jika diperlukan */
		if (($angka[5] == "0") AND ($angka[6] == "0")) {
			$kalimat = str_replace("satu ribu","seribu",$kalimat);
		}
		
		return trim($kalimat);
	}
	
	/*
	 * Variable(s) related
	 */
	
	//OK
	public function getInt($key, $default = 0) {
		return isset($_REQUEST[$key]) ? intval($_REQUEST[$key]) : $default;
	}
	
	//OK
	public function getStr($key, $default = '') {
		return isset($_REQUEST[$key]) ? mysql_escape_string(trim($_REQUEST[$key])) : $default;
	}
	
	//OK
	public function getInt2($key, $key2, $default = 0) {
		return isset($_REQUEST[$key][$key2]) ? intval($_REQUEST[$key][$key2]) : $default;
	}
	
	//OK
	public function getStr2($key, $key2, $default = '') {
		return isset($_REQUEST[$key][$key2]) ? mysql_escape_string(trim($_REQUEST[$key][$key2])) : $default;
	}
	
	//OK
	public function getVars() {
		$this->act		= $this->getStr('act', 'home');
		$this->detail	= $this->getStr('detail');
		$this->task		= $this->getStr('task', 'view');
		$this->subtask	= $this->getStr('subtask');
		$this->id		= $this->getInt('id');
		$this->id2		= $this->getInt('id2');
		$this->success	= $this->getStr('success');
		$this->msg		= $this->getStr('msg');
		$this->html		= $this->getInt('html', 1);
		
		$this->fromact	= $this->getStr('fromact', '');
		$this->fromtask	= $this->getStr('fromtask', '');
		$this->fromid	= $this->getInt('fromid', 0);
	}
	
	//OK
	public function getVarsFrom($isFrom = true) {
		$from = array();
		if ($isFrom) {
			if (isset($_REQUEST['fromact'])) {
				$from[] = 'fromact='.mysql_escape_string(trim($_REQUEST['fromact']));
				if (isset($_REQUEST['fromtask'])) {
					$from[] = 'fromtask='.mysql_escape_string(trim($_REQUEST['fromtask']));
				}
				if (isset($_REQUEST['fromid'])) {
					$from[] = 'fromid='.intval($_REQUEST['fromid']);
				}
				return '&'.implode('&', $from);
			} else {
				return '';
			}
		} else {
			if (isset($_REQUEST['fromact'])) {
				$from[] = 'act='.mysql_escape_string(trim($_REQUEST['fromact']));
			}
			if (isset($_REQUEST['fromtask'])) {
				$from[] = 'task='.mysql_escape_string(trim($_REQUEST['fromtask']));
			}
			if (isset($_REQUEST['fromid'])) {
				$from[] = 'id='.intval($_REQUEST['fromid']);
			}
			return implode('&', $from);
		}
	}
	
	/*
	 * Reports(s) related
	 */
}

class Report {
	public $title;
	public $filename;
	
	public $pdfWidth;
	public $pdfHeight;
	public $pdfOrientation;
	
	public $marginLeft;
	public $marginRight;
	public $marginTop;
	public $marginBottom;
	
	public $fontFamily;
	public $fontStyle;
	public $fontSize;
	
	public $pageWidth;
	public $pageHeight;
	
	public $cellHeightRatio;
	
	public $headerOffset;
	
	public $button = array();
	public $buttonParameter = array();
	
	function __construct($title = 'Untitled', $filename = 'Untitled.pdf') {
		$this->title = $title;
		$this->filename = $filename;
		
		$this->pdfWidth 		= isset($_REQUEST['pdfWidth']) 			? intval($_REQUEST['pdfWidth']) 	: 215;
		$this->pdfHeight 		= isset($_REQUEST['pdfHeight']) 		? intval($_REQUEST['pdfHeight']) 	: 330;
		$this->pageWidth 		= isset($_REQUEST['pageWidth']) 		? intval($_REQUEST['pageWidth']) 	: 215;
		$this->pageHeight 		= isset($_REQUEST['pageHeight']) 		? intval($_REQUEST['pageHeight']) 	: 330;
		$this->pdfOrientation 	= isset($_REQUEST['pdfOrientation']) 	? mysql_escape_string(trim($_REQUEST['pdfOrientation'])) 	: 'P';
		$this->marginLeft 		= isset($_REQUEST['marginLeft']) 		? intval($_REQUEST['marginLeft']) 	: PDF_MARGIN_LEFT;
		$this->marginRight 		= isset($_REQUEST['marginRight']) 		? intval($_REQUEST['marginRight']) 	: PDF_MARGIN_RIGHT;
		$this->marginTop 		= isset($_REQUEST['marginTop']) 		? intval($_REQUEST['marginTop']) 	: 15;
		$this->marginBottom 	= isset($_REQUEST['marginBottom']) 		? intval($_REQUEST['marginBottom']) : PDF_MARGIN_BOTTOM;
		$this->fontFamily 		= isset($_REQUEST['fontFamily']) 		? mysql_escape_string(trim($_REQUEST['fontFamily'])) 		: 'helvetica';
		$this->fontStyle 		= isset($_REQUEST['fontStyle']) 		? mysql_escape_string(trim($_REQUEST['fontStyle'])) 		: '';
		$this->fontSize 		= isset($_REQUEST['fontSize']) 			? intval($_REQUEST['fontSize']) 	: 10;
		$this->cellHeightRatio 	= isset($_REQUEST['cellHeightRatio']) 	? mysql_escape_string(trim($_REQUEST['cellHeightRatio'])) 	: 1.25;
		$this->headerOffset 	= isset($_REQUEST['headerOffset']) 		? intval($_REQUEST['headerOffset']) : 0;
	}

	public function header($pdf, $konfigurasi) {
?>
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="15%"><img src="<?php echo CFG_LIVE_PATH; ?>/images/report/pelalawan3.jpg" width="60"></td>
		<td width="85%" align="center">
			<?php echo isset($konfigurasi['reportheader']) ? $konfigurasi['reportheader'] : ''; ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->Line($this->marginLeft, $this->marginTop + 32 + $this->headerOffset, $pdf->GetPageWidth() - $this->marginRight, $this->marginTop + 32 + $this->headerOffset, array('width' => 0.5));
		$pdf->Line($this->marginLeft, $this->marginTop + 33 + $this->headerOffset, $pdf->GetPageWidth() - $this->marginRight, $this->marginTop + 33 + $this->headerOffset, array('width' => 0.25));
	}

	public function signature($konfigurasi, $objSatuanKerja, $objPengesahan, $tgl) {
?>
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td align="right">Ditetapkan di</td>
		<td align="left"><?php echo $objSatuanKerja->wilIbukota; ?></td>
	</tr>
	<tr>
		<td align="right">pada tanggal</td>
		<td align="left"><?php echo $tgl; ?></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<b><?php echo isset($konfigurasi['signature']) ? $konfigurasi['signature'] : ''; ?></b><br>
			<br><br><br>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<table width="100%">
			<tr>
				<td width="26%"></td>
				<td align="left" width="50%" nowrap>
					<b><u><?php echo $objPengesahan->pnNama; ?></u></b><br>
					<?php echo $objPengesahan->pgrPangkat; ?><br>
					NIP. <?php echo $objPengesahan->pnNIP; ?>
				</td>
				<td width="24%"></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<?php
	}

	public function signature2($konfigurasi, $objPengesahan) {
?>
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td colspan="2" align="center">
			<b><?php echo isset($konfigurasi['signature']) ? $konfigurasi['signature'] : ''; ?></b><br>
			<br><br><br><br>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" width="100%">
			<tr>
				<td width="25%"></td>
				<td width="75%">
					<b><u><?php echo $objPengesahan->pnNama; ?></u></b><br>
					<?php echo $objPengesahan->pgrPangkat; ?><br>
					NIP. <?php echo $objPengesahan->pnNIP; ?>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<?php
	}
	
	public function showDialog() {
?>
	<div style="display:none;">
		<form id="printForm" method="post" action="">
			<div style="font-family:Calibri,'Lucida Grande',Tahoma,Verdana,Arial,sans-serif;">
				<input type="hidden" id="reportButton" name="reportButton" value="">
				<input type="hidden" id="reportCode" name="reportCode" value="">
				<input type="hidden" id="reportTask" name="reportTask" value="">
				<input type="hidden" id="reportID" name="reportID" value="">
				<fieldset>
					<legend><h3>Pengaturan Cetak</h3></legend>
					<p style="font-size:12px;"><i>Petunjuk: pergunakan <b>Cell Height Ratio</b> untuk mengatur jarak antar baris teks,<br>misalnya dengan nilai default 1.25, maka nilai 1.00 untuk lebih rapat,<br>dan nilai 1.50 untuk lebih renggang dan seterusnya.</i></p>
					<table border="0" cellpadding="0" cellspacing="0" style="font-size:12px;">
					<tr>
						<td>PDF Size</td><td>&nbsp;:&nbsp;</td>
						<td>
							<input type="text" class="box" id="pdfWidth" name="pdfWidth" maxlength="3" size="3" value="<?php echo $this->pdfWidth; ?>">
							x
							<input type="text" class="box" id="pdfHeight" name="pdfHeight" maxlength="3" size="3" value="<?php echo $this->pdfHeight; ?>">
						</td>
					</tr>
					<tr>
						<td>Page Size</td><td>&nbsp;:&nbsp;</td>
						<td>
							<input type="text" class="box" id="pageWidth" name="pageWidth" maxlength="3" size="3" value="<?php echo $this->pageWidth; ?>">
							x
							<input type="text" class="box" id="pageHeight" name="pageHeight" maxlength="3" size="3" value="<?php echo $this->pageHeight; ?>">
						</td>
					</tr>
					<tr>
						<td>Orientation</td><td>&nbsp;:&nbsp;</td>
						<td>
							<select class="box" id="pdfOrientation" name="pdfOrientation">
								<option value="P" <?php echo ($this->pdfOrientation == 'P') ? 'selected' : ''; ?>>Portrait</option>
								<option value="L" <?php echo ($this->pdfOrientation == 'L') ? 'selected' : ''; ?>>Landscape</option>
							</select>
						</td>
					</tr>
					<tr>
						<td valign="top" style="padding-top:8px;">Margin</td><td valign="top" style="padding-top:8px;"></td>
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>Left</td><td>&nbsp;:&nbsp;</td>
								<td>
									<input type="text" class="box" id="marginLeft" name="marginLeft" maxlength="3" size="3" value="<?php echo $this->marginLeft; ?>">
								</td>
								<td>Right</td><td>&nbsp;:&nbsp;</td>
								<td>
									<input type="text" class="box" id="marginRight" name="marginRight" maxlength="3" size="3" value="<?php echo $this->marginRight; ?>">
								</td>
							</tr>
							<tr>
								<td>Top</td><td>&nbsp;:&nbsp;</td>
								<td>
									<input type="text" class="box" id="marginTop" name="marginTop" maxlength="3" size="3" value="<?php echo $this->marginTop; ?>">
								</td>
								<td>Bottom</td><td>&nbsp;:&nbsp;</td>
								<td>
									<input type="text" class="box" id="marginBottom" name="marginBottom" maxlength="3" size="3" value="<?php echo $this->marginBottom; ?>">
								</td>
							</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>Font Family</td><td>&nbsp;:&nbsp;</td>
						<td>
							<select class="box" id="fontFamily" name="fontFamily">
								<option value="helvetica" <?php echo ($this->fontFamily == 'helvetica') ? 'selected' : ''; ?>>Helvetica</option>
								<option value="times" <?php echo ($this->fontFamily == 'times') ? 'selected' : ''; ?>>Times-Roman</option>
								<option value="courier" <?php echo ($this->fontFamily == 'courier') ? 'selected' : ''; ?>>Courier</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Font Style</td><td>&nbsp;:&nbsp;</td>
						<td>
							<input type="text" class="box" id="fontStyle" name="fontStyle" maxlength="50" size="28" value="<?php echo $this->fontStyle; ?>">
						</td>
					</tr>
					<tr>
						<td>Font Size</td><td>&nbsp;:&nbsp;</td>
						<td>
							<input type="text" class="box" id="fontSize" name="fontSize" maxlength="3" size="3" value="<?php echo $this->fontSize; ?>">
						</td>
					</tr>
					<tr>
						<td>Cell Height Ratio</td><td>&nbsp;:&nbsp;</td>
						<td>
							<input type="text" class="box" id="cellHeightRatio" name="cellHeightRatio" maxlength="5" size="5" value="<?php echo $this->cellHeightRatio; ?>">
							(<i>Pergunakan tanda titik sebagai tanda desimal</i>)
						</td>
					</tr>
					<tr>
						<td>Header Offset</td><td>&nbsp;:&nbsp;</td>
						<td>
							<input type="text" class="box" id="headerOffset" name="headerOffset" maxlength="3" size="3" value="<?php echo $this->headerOffset; ?>">
						</td>
					</tr>
					<tr>
						<td></td><td></td>
						<td>
							<input type="button" id="btnReportPrint" class="button-link inline dark-blue" value="Cetak" />
<?php 
		//Tampilkan tombol custom
		if (count($this->button) > 0) {
			foreach ($this->button as $button) {
?>
							<input type="button" id="btnReportPrint-<?php echo $button['act']; ?>-<?php echo $button['task']; ?>" class="button-link inline dark-blue btnReportPrint <?php echo $button['act']; ?>" value="<?php echo $button['text']; ?>" />
<?php 
			}
		}
?>
							<input type="button" id="btnReportDefault" class="button-link inline blue" value="Load Default" />
						</td>
					</tr>
					</table>
				</fieldset>
			</div>
		</form>
	</div>
	<script type="text/javascript">
		var report = <?php echo json_encode($this); ?>;
		
		$(".btnPrint").fancybox({
			'scrolling'		: 'no',
			'titleShow'		: false
		});
		
		$(".btnPrint").click(function() {
			var a = $(this).attr('id').split('-');

			$("#reportButton").val(a[0]);
			$("#reportCode").val(a[1]);
			$("#reportTask").val(a[2]);
			$("#reportID").val(a[3]);
			
			//Sembunyikan tombol custom
			$(".button-link.inline.dark-blue.btnReportPrint").hide();
			//Jika berasal dari btnPrint, maka tampilkan tombol custom berdasarkan kode
			if (a[0] == 'btnPrint') {
				$(".button-link.inline.dark-blue.btnReportPrint." + $("#reportCode").val()).show();
			}
		});
		
		$("#btnReportPrint").click(function() {
			var p = '&pdfWidth=' + $("#pdfWidth").val();
			p += '&pdfHeight=' + $("#pdfHeight").val();
			p += '&pageWidth=' + $("#pageWidth").val();
			p += '&pageHeight=' + $("#pageHeight").val();
			p += '&pdfOrientation=' + $("#pdfOrientation").val();
			p += '&marginLeft=' + $("#marginLeft").val();
			p += '&marginRight=' + $("#marginRight").val();
			p += '&marginTop=' + $("#marginTop").val();
			p += '&marginBottom=' + $("#marginBottom").val();
			p += '&fontFamily=' + $("#fontFamily").val();
			p += '&fontStyle=' + $("#fontStyle").val();
			p += '&fontSize=' + $("#fontSize").val();
			p += '&cellHeightRatio=' + $("#cellHeightRatio").val();
			p += '&headerOffset=' + $("#headerOffset").val();
			
			//$.fancybox.close();

			//additional parameter
<?php 
		if (count($this->buttonParameter) > 0) {
			foreach ($this->buttonParameter as $button => $parameters) {
?>
			if ($("#reportButton").val() + '-' + $("#reportCode").val() + '-' + $("#reportTask").val() + '-' + $("#reportID").val() == '<?php echo  $button; ?>') {
<?php 
				if (count($parameters) > 0) {
					foreach ($parameters as $parameter) {
?>
				p += '&<?php echo $parameter; ?>=' + $("#<?php echo $parameter; ?>").val();
<?php 
					}
				}
?>
			}				
<?php 
			}
		}
?>
			
			window.open('index2.php?act=' + $("#reportCode").val() + '&task=' + $("#reportTask").val() + '&id=' + $("#reportID").val() + '&html=0' + p, '_blank');
		});
		
		$(".btnReportPrint").click(function() {
			var a = $(this).attr('id').split('-');
			
			var p = '&pdfWidth=' + $("#pdfWidth").val();
			p += '&pdfHeight=' + $("#pdfHeight").val();
			p += '&pageWidth=' + $("#pageWidth").val();
			p += '&pageHeight=' + $("#pageHeight").val();
			p += '&pdfOrientation=' + $("#pdfOrientation").val();
			p += '&marginLeft=' + $("#marginLeft").val();
			p += '&marginRight=' + $("#marginRight").val();
			p += '&marginTop=' + $("#marginTop").val();
			p += '&marginBottom=' + $("#marginBottom").val();
			p += '&fontFamily=' + $("#fontFamily").val();
			p += '&fontStyle=' + $("#fontStyle").val();
			p += '&fontSize=' + $("#fontSize").val();
			p += '&cellHeightRatio=' + $("#cellHeightRatio").val();
			p += '&headerOffset=' + $("#headerOffset").val();
			
			//$.fancybox.close();
			
			window.open('index2.php?act=' + $("#reportCode").val() + '&task=' + a[2] + '&id=' + $("#reportID").val() + '&html=0' + p, '_blank');
		});

		$("#btnReportDefault").click(function() {
			$("#pdfWidth").val(report.pdfWidth);
			$("#pdfHeight").val(report.pdfHeight);
			$("#pageWidth").val(report.pageWidth);
			$("#pageHeight").val(report.pageHeight);
			$("#pdfOrientation").val(report.pdfOrientation);
			$("#marginLeft").val(report.marginLeft);
			$("#marginRight").val(report.marginRight);
			$("#marginTop").val(report.marginTop);
			$("#marginBottom").val(report.marginBottom);
			$("#fontFamily").val(report.fontFamily);
			$("#fontStyle").val(report.fontStyle);
			$("#fontSize").val(report.fontSize);
			$("#cellHeightRatio").val(report.cellHeightRatio);
			$("#headerOffset").val(report.headerOffset);
		});
		
		$(document).ready(function(){
			$("#pdfWidth").maskMoney({ thousands:'', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#pdfHeight").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#pageWidth").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#pageHeight").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#marginLeft").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#marginRight").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#marginTop").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#marginBottom").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#fontSize").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
			$("#cellHeightRatio").maskMoney({ thousands:',', decimal:'.', precision:2, defaultZero:true, allowZero:true });
			$("#headerOffset").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
		});
	</script>
<?php
	}
	
	public function addButton($act, $task, $text) {
		$this->button[] = array('act' => $act, 'task' => $task, 'text' => $text);
	}
	
	public function addButtonParameter($button, $fields) {
		if (count($fields) > 0) {
			foreach ($fields as $field) {
				$this->buttonParameter[$button][] = $field;
			}
		}
	}
}

//OK
class UserSession {	
	public $ID = 0;
	public $namaPengguna = '';
	public $nama = '';
	public $levelAkses = '';
	public $loginSebelumnya = '0000-00-00 00:00:00';
	public $loginSaatIni = '0000-00-00 00:00:00';
	public $defaultPassword = false;
	public $loketID = 0;
	public $loketNama = '';
	public $aksesLaporan = 0;
	public $bidangID = 0;
	public $bidangNama = '';
}
?>