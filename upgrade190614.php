<?php
//Set flag for parent file
define('PG_PARENT', 1);

//Include file(s)
require_once 'configuration.php';
require_once 'system.php';;
if (CFG_GESHI) {
	require_once 'libraries/geshi.php';
}

//Create application
$app = new App();
$app->connect(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);
$app->init();

$app->query("ALTER TABLE  `kblipublikasi` ADD  `kpubPublikasi` BOOLEAN NOT NULL;");
$app->query("INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringHO', '<table border=".'0'." cellpadding=".'0'." cellspacing=".'0'.">
<tr><td width=".'4%'.">1.</td><td width=".'96%'.">Undang-undang Gangguan (Hinder Ordonatie) stablat Tahun 1926 Nomor 226 yang dirubah dengan Stablat Tahun 1949 Nomor 14 dan 450</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 32 Tahun 2004</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>5.</td><td>Undang-undang Nomor 28 Tahun 2009</td></tr>
<tr><td>6.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>7.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>8.</td><td>Peraturan Menteri Dalam Negeri Nomor 27 Tahun 2009</td></tr>
<tr><td>9.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 04 Tahun 2012</td></tr>
<tr><td>10.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</td></tr>
<tr><td>11.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014</td></tr>
<tr><td>12.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/674</td></tr>
</table>');");
$app->query("INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringSITU', '<table>
<tr><td width=".'4%'.">1.</td><td width=".'96%'.">Undang-undang Gangguan (Hinder Ordonatie) stablat Tahun 1926 Nomor 226 yang dirubah dengan Stablat Tahun 1949 Nomor 14 dan 450</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 32 Tahun 2004</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>5.</td><td>Undang-undang Nomor 28 Tahun 2009</td></tr>
<tr><td>6.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>7.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>8.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</td></tr>
<tr><td>9.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014</td></tr>
<tr><td>10.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/674</td></tr>
</table>');");
$app->query("INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringReklame', '<table>
<tr><td width=".'4%'.">1.</td><td width=".'96%'.">Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 32 Tahun 2004</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 28 Tahun 2009</td></tr>
<tr><td>5.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>6.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>7.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 20 Tahun 2007</td></tr>
<tr><td>8.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</td></tr>
<tr><td>9.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014</td></tr>
<tr><td>10.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/674</td></tr>
</table>');");
$app->query("INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringPariwisata', '<table>
<tr><td width=".'4%'.">1.</td><td width=".'96%'.">Undang-undang Nomor 12 Tahun 1956</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 18 Tahun 1997</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 22 Tahun 1999</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>5.</td><td>Undang-undang Nomor 34 Tahun 2000</td></tr>
<tr><td>6.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>7.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>8.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>9.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 07 Tahun 2006</td></tr>
<tr><td>10.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</td></tr>
<tr><td>11.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/21</td></tr>
</table>');");
$app->query("INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringIUI', '<table>
<tr><td width=".'4%'.">1.</td><td width=".'96%'.">Undang-undang No. 5 Tahun 1984 tentang Perindustrian</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 32 Tahun 2004 tentang Pemerintahan Daerah sebagaimana telah diubah dengan Undang-undang Nomor 12 Tahun 2008;</td></tr>
<tr><td>3.</td><td>Peraturan Pemerintah No. 17 Tahun 1986 tentang Kewenangan Pengaturan, Pembinaan dan Pengembangan Industri;</td></tr>
<tr><td>4.</td><td>Peraturan Pemerintah No. 13 Tahun 1955 tentang Pengaturan Usaha Industri;</td></tr>
<tr><td>5.</td><td>Peraturan Pemerintah Nomor 38 Tahun 2007 tentang Pembagian Urusan Pemerintahan Antara Pemerintah, Pemerintah Daerah Provinsi, dan Pemerintah Daerah Kabupaten/Kota</td></tr>
<tr><td>6.</td><td>Keputusan Menteri Perindustrian Nomor 148/M/SK/7/1995 tentang Penetapan Jenis Dan Komoditi Industri yang diproses Produksinya tidak Merusak ataupun Membahayakan Lingkungan Serta Tidak Menggunakan Sumber Daya Alam Secara Berlebihan;</td></tr>
<tr><td>7.</td><td>Keputusan Menteri Negara Lingkungan Hidup Nomor 86 Tahun 2002 tentang Pedoman Pengelolaan Lingkungan Hidup dan Upaya Pemantauan Lingkungan Hidup;</td></tr>
<tr><td>8.</td><td>Peraturan Menteri Lingkungan Hidup Nomor 11 Tahun 2006 tentang Jenis Rencana Usaha dan atau Kegiatan Yang Wajib Dilengkapi Dengan Analisis Mengenai Lingkungan Hidup;</td></tr>
<tr><td>9.</td><td>Peraturan Menteri Perindustrian Nomor 41/M-IND/PER/6/2008 tentang Ketentuan dan Tata Cara Pemberian Izin Usaha Industri,Izin Perluasan dan Tanda Daftar Industri</td></tr>
</table>');");
$app->query("INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringIMB', '<table>
<tr><td width=".'4%'.">1.</td><td width=".'96%'.">Undang-undang Gangguan (Hinder Ordonatie) stablat Tahun 1926 Nomor 226 yang dirubah dengan Stablat Tahun 1949 Nomor 14 dan 450</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 32 Tahun 2004</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>5.</td><td>Undang-undang Nomor 28 Tahun 2009</td></tr>
<tr><td>6.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>7.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>8.</td><td>Keputusan Menteri Dalam Negeri Nomor 7 Tahun 1992</td></tr>
<tr><td>9.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 03 Tahun 2010</td></tr>
<tr><td>10.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 04 Tahun 2012</td></tr>
<tr><td>11.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014</td></tr>
<tr><td>12.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/674</td></tr>
</table>');");
?>