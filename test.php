<?php
//Set flag for parent file
define('PG_PARENT', 1);

//Include file(s)
require_once 'configuration.php';
require_once 'system.php';;
if (CFG_GESHI) {
	require_once 'libraries/geshi.php';
}

//Create application
$app = new App();
$app->connect(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);
$app->init();

$nomor = isset($_REQUEST["nomor"]) ? trim($_REQUEST["nomor"]) : "";

$objPermohonan = $app->queryObject("SELECT * FROM permohonan WHERE mhnNoUrutLengkap='".$nomor."'");
if ($objPermohonan) {
	$arr['subdata'] = array();
	
	$hasil = "";
	
	$sql = "SELECT *
		  	FROM detailpermohonan
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
		  	WHERE dmhnMhnID='".$objPermohonan->mhnID."'";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$badge = '';
	 	if ($obj->dmhnDikembalikan == 0) {
			if ($obj->dmhnPola > 0) {
		 		if ($obj->dmhnPerluSurvey == 1) {
					if ($obj->dmhnTelahSurvey == 0) {
						$badge = 'Belum Selesai'; //Belum Survey, dianggap Belum Selesai
					} else {
						if ($obj->dmhnHasilSurvey == 'Memenuhi Syarat') {
							if ($obj->dmhnTelahSelesai == 0) {
								$badge = 'Belum Selesai';
							} else {
								if ($obj->dmhnTelahAmbil == 0) { 
									$badge = 'Belum Diambil';
								} else {
									$badge = 'Sudah Diambil';
								}
							}
						} else if ($obj->dmhnHasilSurvey == 'Ditunda') {
							$badge = 'Hasil Survey: Ditunda';
						} else if ($obj->dmhnHasilSurvey == 'Batal') {
							$badge = 'Hasil Survey: Batal';
						}
					}
				} else {
					if ($obj->dmhnTelahSelesai == 0) {
						$badge = 'Belum Selesai';
					} else {
						if ($obj->dmhnTelahAmbil == 0) { 
							$badge = 'Belum Diambil';
						} else {
							$badge = 'Sudah Diambil';
						}
					}
				}
			} else {
				$badge = 'Belum Selesai'; //Belum Ditentukan Polanya, dianggap Belum Selesai
			}
		} else {
			$badge = 'Dikembalikan';
		}
		
		//$obj->badge = $badge;
		
		$arr['subdata'][$obj->dmhnID] = "- ".$obj->jsiNama." : ".$badge;
	}
	
	$hasil = implode("\n", $arr['subdata']);
} else {
	if ($nomor == "") {
		$hasil = "Nomor Pendaftaran harus diisi";
	} else {
		$hasil = "Nomor Pendaftaran tidak ditemukan";
	}
}
?>
<pengurusan>
	<hasil><?php echo $hasil; ?></hasil>
</pengurusan>