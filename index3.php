<?php
//Set flag for parent file
define('PG_PARENT', 1);

//Include file(s)
require_once 'configuration.php';
require_once 'system.php';
if (CFG_GESHI) {
	require_once 'libraries/geshi.php';
}
require_once 'libraries/tcpdf/tcpdf.php';

//Create application
$app = new App();
$app->init();
$app->connect(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);
$app->getVars();

//Check access
if ($_SESSION['sesipengguna']->ID == 0) {
	header('Location:index.php?login=-1');
}

if ((substr($app->task,0,5) == 'print') || (substr($app->task,0,6) == 'export')) {
	if (substr($app->task,0,6) == 'export') {
		header("Content-Disposition: attachment; filename=export_".date('Ymd_His').".xls");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title><?php echo CFG_SITE_NAME; ?></title>
	
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="icon" href="images/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="<?php echo CFG_LIVE_PATH; ?>/css/print.css"/>
	<style>
		/* Money related */
		
		.style0 {
			mso-number-format: General;
			/*text-align: general;
			vertical-align: bottom;
			white-space: nowrap;
			mso-rotate: 0;
			mso-background-source: auto;
			mso-pattern: auto;
			color: black;
			font-size: 11.0pt;
			font-weight: 400;
			font-style: normal;
			text-decoration: none;
			font-family: Calibri, sans-serif;
			mso-font-charset: 0;
			border: none;
			mso-protection: locked visible;
			mso-style-name: Normal;
			mso-style-id: 0;*/
		}
		
		.xText {
			mso-style-parent: style0;
			mso-number-format: "\@";
		}
		
		.xNumberFixed {
			mso-style-parent: style0;
			mso-number-format: Fixed;
		}
		
		.xNumberStandard {
			mso-style-parent: style0;
			mso-number-format: Standard;
		}
		
		.xNumberStandard2 {
			mso-style-parent: style0;
			mso-number-format:"\#\,\#\#0";
		}
		
		.xNumberStandard3 {
			mso-style-parent: style0;
			mso-number-format:"\#\,\#\#0.00";
		}
		
		.xCurrency {
			mso-style-parent: style0;
			mso-number-format: "\0022$\0022\#\,\#\#0\.00";
		}
		
		.xAccounting {
			mso-style-parent: style0;
			mso-number-format: "_\(\0022$\0022* \#\,\#\#0\.00_\)\;_\(\0022$\0022* \\\(\#\,\#\#0\.00\\\)\;_\(\0022$\0022* \0022-\0022??_\)\;_\(\@_\)";
		}
	</style>
</head>
<body>
<?php
	if ($app->act != "") {
		if ($app->detail == "") {
			//Workaround for : Unsafe use of variable in call include()/require()
			define('PG_PATH', "acts/".$app->act."/controller.".$app->act.".php");
			if (file_exists(PG_PATH)) {
				require_once(PG_PATH);
			} else {
				echo 'Tidak ditemukan';
			}
		} else {
			//Workaround for : Unsafe use of variable in call include()/require()
			define('PG_PATH', "acts/".$app->act."/detail/".$app->detail."/controller.".$app->detail.".php");
			if (file_exists(PG_PATH)) {
				require_once(PG_PATH);
			} else {
				echo 'Tidak ditemukan';
			}
		}
	}
?>
</body>
</html>
<?php
} else {
	if ($app->act != "") {
		if ($app->detail == "") {
			//Workaround for : Unsafe use of variable in call include()/require()
			define('PG_PATH', "acts/".$app->act."/controller.".$app->act.".php");
			if (file_exists(PG_PATH)) {
				require_once(PG_PATH);
			} else {
				echo 'Tidak ditemukan';
			}
		} else {
			//Workaround for : Unsafe use of variable in call include()/require()
			define('PG_PATH', "acts/".$app->act."/detail/".$app->detail."/controller.".$app->detail.".php");
			if (file_exists(PG_PATH)) {
				require_once(PG_PATH);
			} else {
				echo 'Tidak ditemukan';
			}
		}
	}
}
?>