<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Mesin_View {
	static function editMesin($success, $msg, $obj, $objTdi) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=mesin&task=edit&id=<?php echo $obj->msnID; ?>"><img src="images/icons/rosette.png" border="0" /> <?php echo ($obj->msnID > 0) ? 'Ubah' : 'Tambah'; ?> Mesin/Peralatan Produksi untuk TDI No. <?php echo $objTdi->tdiNoLengkap; ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=mesin&task=save" method="POST" >
				<input type="hidden" id="msnID" name="msnID" value="<?php echo $obj->msnID; ?>" />
				<input type="hidden" id="msnTdiID" name="msnTdiID" value="<?php echo $obj->msnTdiID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=mesin&id=<?php echo $obj->msnTdiID; ?>">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="170">Jenis <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="msnJenis" name="msnJenis" maxlength="255" size="100" value="<?php echo $obj->msnJenis; ?>" />
					</td>
				</tr>
				<tr>
					<td>Jumlah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="msnJumlah" name="msnJumlah" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->msnJumlah); ?>" />
					</td>
				</tr>
				<tr>
					<td>Merek</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="msnMerek" name="msnMerek" maxlength="255" size="100" value="<?php echo $obj->msnMerek; ?>" />
					</td>
				</tr>
				<tr>
					<td>Spesifikasi Negara Asal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="msnSpesifikasiNegaraAsal" name="msnSpesifikasiNegaraAsal" maxlength="255" size="100" value="<?php echo $obj->msnSpesifikasiNegaraAsal; ?>" />
					</td>
				</tr>
				<tr>
					<td>Kapasitas Mesin</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="msnKapasitasMesin" name="msnKapasitasMesin" maxlength="25" size="25" value="<?php echo $app->MySQLToMoney($obj->msnKapasitasMesin, 2); ?>" />
					</td>
				</tr>
				<tr>
					<td>Satuan Kapasitas Mesin</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="msnSatuanKapasitasMesin" name="msnSatuanKapasitasMesin" maxlength="255" size="50" value="<?php echo $obj->msnSatuanKapasitasMesin; ?>" />
					</td>
				</tr>
				<tr>
					<td>Harga (Rp)</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="msnHarga" name="msnHarga" maxlength="20" size="25" value="<?php echo $app->MySQLToMoney($obj->msnHarga, 2); ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/mesin/js/edit.mesin.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewMesin($success, $msg, $arr, $objTdi) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=mesin&id=<?php echo $objTdi->tdiID; ?>"><img src="images/icons/rosette.png" border="0" /> Mesin/Peralatan Produksi untuk TDI No. <?php echo $objTdi->tdiNoLengkap; ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<input type="hidden" id="id" name="id" value="<?php echo $objTdi->tdiID; ?>" />
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=mesin&task=add&id=<?php echo $objTdi->tdiID; ?>">Tambah</a>
				<a class="button-link inline blue" href="index2.php?act=tdi&task=edit&id=<?php echo $objTdi->tdiDmhnID; ?>">Kembali ke Entri TDI</a>
				<a class="button-link inline blue" href="index2.php?act=permohonan">Selesai</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Jenis : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('msnJenis', 'Jenis', true),
			$app->setHeader('msnJumlah', 'Jumlah', true, 0, 'right'),
			$app->setHeader('msnMerek', 'Merek', true),
			$app->setHeader('msnSpesifikasiNegaraAsal', 'Spesifikasi Negara Asal', true),
			$app->setHeader('msnKapasitasMesin', 'Kapasitas Mesin', true),
			$app->setHeader('msnHarga', 'Harga (Rp)', true, 0, 'right')
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=mesin&task=edit&id='.$v->msnID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> ';
				echo '<a href="javascript:hapus('.$v->msnID.', '."'".$v->msnJenis."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=mesin&task=edit&id='.$v->msnID.'">'.$v->msnJenis.'</a></td>';
				echo '<td align="right">'.$app->MySQLToMoney($v->msnJumlah).'</td>';
				echo '<td>'.$v->msnMerek.'</td>';
				echo '<td>'.$v->msnSpesifikasiNegaraAsal.'</td>';
				echo '<td>'.$app->MySQLToMoney($v->msnKapasitasMesin, 2).' '.$v->msnSatuanKapasitasMesin.'</td>';
				echo '<td align="right">'.$app->MySQLToMoney($v->msnHarga).'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="7">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/mesin/js/mesin.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>