//Objects
$("#msnJumlah").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#msnKapasitasMesin").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
$("#msnHarga").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });

//Variables
var form = $("#myForm");
var msnJenis = $("#msnJenis");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'msnJenis' || this.id === undefined) {
		if (msnJenis.val().length == 0){
			doSubmit = false;
			msnJenis.addClass("error");		
		} else {
			msnJenis.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
msnJenis.blur(validateForm);

msnJenis.keyup(validateForm);

form.submit(submitForm);

//Set focus
msnJenis.focus();