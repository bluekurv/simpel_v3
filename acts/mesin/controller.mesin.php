<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.mesin.php';
require_once 'view.mesin.php';

switch ($app->task) {
	case 'add':
		addMesin($app->id);
		break;
	case 'edit':
		editMesin($app->id);
		break;
	case 'delete':
		deleteMesin($app->id);
		break;
	case 'save':
		saveMesin();
		break;
	default:
		viewMesin($app->success, $app->msg, $app->id);
		break;
}

function addMesin($id) {
	global $app;
	
	//Get object
	$objTdi = $app->queryObject("SELECT * FROM tdi WHERE tdiID='".$id."'");
	if (!$objTdi) {
		$app->showPageError();
		exit();
	}

	$objMesin = new Mesin_Model();
	$objMesin->msnTdiID = $id;
	
	Mesin_View::editMesin(true, '', $objMesin, $objTdi);
}

function deleteMesin($id) {
	global $app;
	
	//Get object
	$objMesin = $app->queryObject("SELECT * FROM mesin WHERE msnID='".$id."'");
	if (!$objMesin) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM mesin WHERE msnID='".$id."'");
		
		$success = true;
		$msg = 'Mesin/Peralatan Produksi "'.$objMesin->msnJenis.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'mesin', $objMesin->msnID, $objMesin->msnJenis, $msg);
	} else {
		$success = false;
		$msg = 'Mesin/Peralatan Produksi "'.$objMesin->msnJenis.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewMesin($success, $msg, $objMesin->msnTdiID);
}

function editMesin($id) {
	global $app;
	
	//Query
	$objMesin = $app->queryObject("SELECT * FROM mesin WHERE msnID='".$id."'");
	if (!$objMesin) {
		$app->showPageError();
		exit();
	}
	
	//Get object
	$objTdi = $app->queryObject("SELECT * FROM tdi WHERE tdiID='".$objMesin->msnTdiID."'");
	if (!$objTdi) {
		$app->showPageError();
		exit();
	}

	Mesin_View::editMesin(true, '', $objMesin, $objTdi);
}

function saveMesin() {
	global $app;
	
	//Create object
	$objMesin = new Mesin_Model();
	$app->bind($objMesin);
	
	//Modify object (if necessary)
	$objMesin->msnJumlah = $app->MoneyToMySQL($objMesin->msnJumlah);
	$objMesin->msnKapasitasMesin = $app->MoneyToMySQL($objMesin->msnKapasitasMesin);
	$objMesin->msnHarga = $app->MoneyToMySQL($objMesin->msnHarga);
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objMesin->msnJenis == '') {
		$doSave = false;
		$msg[] = '- Jenis belum diisi';
	}
	
	$isExist = $app->isExist("SELECT msnID AS id FROM mesin WHERE msnJenis='".$objMesin->msnJenis."' AND msnTdiID='".$objMesin->msnTdiID."'", $objMesin->msnID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Jenis "'.$objMesin->msnJenis.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objMesin->msnID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objMesin);
		} else {
			$sql = $app->createSQLforUpdate($objMesin);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objMesin->msnID = $app->queryField("SELECT LAST_INSERT_ID() FROM mesin");
			
			$success = true;
			$msg = 'Mesin/Peralatan Produksi "'.$objMesin->msnJenis.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'mesin', $objMesin->msnID, $objMesin->msnJenis, $msg);
		} else {
			$success = true;
			$msg = 'Mesin/Peralatan Produksi "'.$objMesin->msnJenis.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'mesin', $objMesin->msnID, $objMesin->msnJenis, $msg);
		}
	} else {
		$success = false;
		$msg = 'Mesin/Peralatan Produksi "'.$objMesin->msnJenis.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewMesin($success, $msg, $objMesin->msnTdiID);
	} else {
		//Get object
		$objTdi = $app->queryObject("SELECT * FROM tdi WHERE tdiID='".$objMesin->msnTdiID."'");
		if (!$objTdi) {
			$app->showPageError();
			exit();
		}
		
		Mesin_View::editMesin($success, $msg, $objMesin, $objTdi);
	}
}

function viewMesin($success, $msg, $id) {
	global $app;
	
	//Query
	$objTdi = $app->queryObject("SELECT * FROM tdi WHERE tdiID='".$id."'");
	if (!$objTdi) {
		$app->showPageError();
		exit();
	}

	//Get variable(s)
	$page 		= $app->pageVar('mesin', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('mesin', 'sort', 'msnJenis');
	$dir   		= $app->pageVar('mesin', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('mesin', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "msnJenis LIKE '%".$nama."%'";
	}
	$filter[] = "msnTdiID='".$id."'";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM mesin
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM mesin
			".$where." 
			ORDER BY ".$sort." ".$dir.", msnJenis ASC 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Mesin_View::viewMesin($success, $msg, $arr, $objTdi);
}
?>