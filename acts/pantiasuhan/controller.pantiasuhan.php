<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'pantiasuhan';
$childTitle = 'Izin Operasional Panti Asuhan';
$childKodeSurat = 'IOP-PA';
$childTembusan = 'Kepala Dinas Kesejahteraan Sosial Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>