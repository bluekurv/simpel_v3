//Variables
var form = $("#myForm");
var legKode = $("#legKode");
var legNama = $("#legNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'legKode' || this.id === undefined) {
		if (legKode.val().length == 0){
			doSubmit = false;
			legKode.addClass("error");		
		} else {
			legKode.removeClass("error");
		}
	}
	
	if (this.id == 'legNama' || this.id === undefined) {
		if (legNama.val().length == 0){
			doSubmit = false;
			legNama.addClass("error");		
		} else {
			legNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
legKode.blur(validateForm);
legNama.blur(validateForm);

legKode.keyup(validateForm);
legNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
legKode.focus();