<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.legalisir.php';
require_once 'view.legalisir.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editLegalisir($app->id);
		break;
	case 'delete':
		deleteLegalisir($app->id);
		break;
	case 'save':
		saveLegalisir();
		break;
	default:
		viewLegalisir(true, '');
		break;
}

function deleteLegalisir($id) {
	global $app;
	
	//Get object
	$objLegalisir = $app->queryObject("SELECT * FROM legalisir WHERE legID='".$id."'");
	if (!$objLegalisir) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM legalisir WHERE legID='".$id."'");
		
		$success = true;
		$msg = 'Legalisir "'.$objLegalisir->legNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'legalisir', $objLegalisir->legID, $objLegalisir->legNama, $msg);
	} else {
		$success = false;
		$msg = 'Legalisir "'.$objLegalisir->legNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewLegalisir($success, $msg);
}

function editLegalisir($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objLegalisir = $app->queryObject("SELECT * FROM legalisir WHERE legID='".$id."'");
		if (!$objLegalisir) {
			$app->showPageError();
			exit();
		}
	} else {
		$objLegalisir = new Legalisir_Model();
	}
	
	Legalisir_View::editLegalisir(true, '', $objLegalisir);
}

function saveLegalisir() {
	global $app;
	
	//Create object
	$objLegalisir = new Legalisir_Model();
	$app->bind($objLegalisir);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objLegalisir->legKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objLegalisir->legNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT legID AS id FROM legalisir WHERE legKode='".$objLegalisir->legKode."'", $objLegalisir->legID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objLegalisir->legKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT legID AS id FROM legalisir WHERE legNama='".$objLegalisir->legNama."'", $objLegalisir->legID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objLegalisir->legNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objLegalisir->legID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objLegalisir);
		} else {
			$sql = $app->createSQLforUpdate($objLegalisir);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objLegalisir->legID = $app->queryField("SELECT LAST_INSERT_ID() FROM legalisir");
			
			$success = true;
			$msg = 'Legalisir "'.$objLegalisir->legNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'legalisir', $objLegalisir->legID, $objLegalisir->legNama, $msg);
		} else {
			$success = true;
			$msg = 'Legalisir "'.$objLegalisir->legNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'legalisir', $objLegalisir->legID, $objLegalisir->legNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Legalisir "'.$objLegalisir->legNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewLegalisir($success, $msg);
	} else {
		Legalisir_View::editLegalisir($success, $msg, $objLegalisir);
	}
}

function viewLegalisir($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('legalisir', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('legalisir', 'sort', 'legNo');
	$dir		= $app->pageVar('legalisir', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('legalisir', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(legKode LIKE '%".$nama."%' OR legNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM legalisir
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM legalisir
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Legalisir_View::viewLegalisir($success, $msg, $arr);
}
?>