<?php
class Reklame_Model {	
	public $tabel = 'reklame';
	public $primaryKey = 'reklameID';
	
	public $reklameID = 0;
	public $reklameDmhnID = 0;
	public $reklameGrupID = 0;
	public $reklameNo = 0;
	public $reklameNoLengkap = '';
	public $reklameBahan = '';
	public $reklameBunyi = '';
	public $reklamePanjang = '';
	public $reklameLebar = '';
	public $reklamePanjang2 = '';
	public $reklameLebar2 = '';
	public $reklamePanjang3 = '';
	public $reklameLebar3 = '';
	public $reklameJumlah = 0;
	public $reklamePemasanganLokasi = '';
	public $reklamePemasanganKelurahanID = 0;
	public $reklamePemasanganTglMulai = '0000-00-00';
	public $reklamePemasanganTglSampai = '0000-00-00';
	public $reklameKeterangan = '';
	public $reklameTglBerlaku = '0000-00-00';
	public $reklameTglDaftarUlang = '0000-00-00';
	public $reklamePejID = 0;
	public $reklameTglPengesahan = '0000-00-00'; 	 	 	 	 	
	public $reklameArsip = '';
}
?>