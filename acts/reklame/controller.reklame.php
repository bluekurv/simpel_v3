<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.reklame.php';
require_once 'view.reklame.php';

switch ($app->task) {
	case 'add':
		addReklame($app->id);
		break;
	case 'edit':
		editReklame($app->id);
		break;
	case 'delete':
		deleteReklame($app->id);
		break;
	case 'print':
		printReklame($app->id);
		break;
	case 'read':
		readReklame($app->id);
		break;
	case 'save':
		saveReklame();
		break;
	default:
		viewReklame(true, '');
		break;
}

function addReklame($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editReklame($id);
}

function editReklame($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objReklame = $app->queryObject("SELECT * FROM reklame WHERE reklameDmhnID='".$id."'");
	if (!$objReklame) {
		$objReklame = new Reklame_Model();
	}
	
	$objReklame->reklameDmhnID = $objDetailPermohonan->dmhnID;
	
	Reklame_View::editReklame(true, '', $objDetailPermohonan, $objReklame);
}

function deleteReklame($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM reklame WHERE reklameDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Izin Pemasangan Reklame berhasil dihapus';
		
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Izin Pemasangan Reklame', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function printReklame($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN reklame ON reklameDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objReklame = $app->queryObject($sql);
	if (!$objReklame) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objReklame->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objReklame->perKelurahanID."'");
	$objReklame->kelurahan = $objKelurahan->wilNama;
	
	$objKecamatan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");
	$objReklame->kecamatan = $objKecamatan->wilNama;
	
	$objReklame->kabupaten = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKecamatan->wilParentID."'");
	
	Reklame_View::printReklame($objReklame);
}

function readReklame($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN reklame ON reklameDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objReklame = $app->queryObject($sql);
	if (!$objReklame) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objReklame->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Reklame_View::readReklame($objReklame);
}

function saveReklame() {
	global $app;
	
	//Get object
	$objReklameSebelumnya = $app->queryObject("SELECT * FROM reklame WHERE reklameID='".$app->getInt('reklameID')."'");
	if (!$objReklameSebelumnya) {
		$objReklameSebelumnya = new Reklame_Model();
	}
	
	//Create object
	$objReklame = new Reklame_Model();
	$app->bind($objReklame);
	
	//Modify object (if necessary)
	$objReklame->reklamePemasanganTglMulai = $app->NormalDateToMySQL($objReklame->reklamePemasanganTglMulai);
	$objReklame->reklamePemasanganTglSampai = $app->NormalDateToMySQL($objReklame->reklamePemasanganTglSampai);
	
	$objReklame->reklameTglPengesahan = $app->NormalDateToMySQL($objReklame->reklameTglPengesahan);
	if ($objReklame->reklameTglPengesahan != '0000-00-00') {
		if ($objReklameSebelumnya->reklameNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objReklame->reklameNo = intval($app->queryField("SELECT MAX(reklameNo) AS nomor FROM reklame WHERE YEAR(reklameTglPengesahan)='".substr($objReklame->reklameTglPengesahan,0,4)."'")) + 1;
		}
			
		/*if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objReklame->reklameTglBerlaku = date('Y-m-d', strtotime($objReklame->reklameTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objReklame->reklameTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objReklame->reklameTglDaftarUlang = date('Y-m-d', strtotime($objReklame->reklameTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objReklame->reklameTglDaftarUlang = '0000-00-00';
		}*/
		$objReklame->reklameTglBerlaku = $app->NormalDateToMySQL($objReklame->reklameTglBerlaku);
		$objReklame->reklameTglDaftarUlang = $app->NormalDateToMySQL($objReklame->reklameTglDaftarUlang);
	} else {
		$objReklame->reklameNo = '';
		
		$objReklame->reklameTglBerlaku = '0000-00-00';
		$objReklame->reklameTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objReklame->reklameNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/IR/'.substr($objReklame->reklameTglPengesahan,0,4).'/'.$objReklame->reklameNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
			
	//Jika nomor urut bisa diubah
	if ($objReklame->reklameNo > 0){
		$isExist = $app->isExist("SELECT reklameID AS id FROM reklame WHERE reklameNo='".$objReklame->reklameNo."' AND YEAR(reklameTglPengesahan)='".substr($objReklame->reklameTglPengesahan,0,4)."'", $objReklame->reklameID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objReklame->reklameNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objReklame->reklameID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objReklame);
		} else {
			$sql = $app->createSQLforUpdate($objReklame);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objReklame->reklameID = $app->queryField("SELECT LAST_INSERT_ID() FROM reklame");
		}
		
		$success = true;
		$msg = 'Izin Pemasangan Reklame berhasil disimpan';
		
		$app->writeLog(EV_INFORMASI, 'reklame', $objReklame->reklameID, $objReklame->reklameNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objReklame->reklameDmhnID."'");
	} else {
		$success = false;
		$msg = 'Izin Pemasangan Reklame tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objReklame->reklameDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Reklame_View::editReklame($success, $msg, $objDetailPermohonan, $objReklame);
	}
}

function viewReklame($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('reklame', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('reklame', 'sort', 'reklameTglPengesahan');
	//$dir		= $app->pageVar('reklame', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('reklame', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('reklame', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(reklameNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(reklameTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM reklame
				  LEFT JOIN detailpermohonan ON reklameDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM reklame
			LEFT JOIN detailpermohonan ON reklameDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(reklameTglPengesahan) DESC, reklameNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Reklame_View::viewReklame($success, $msg, $arr);
}
?>