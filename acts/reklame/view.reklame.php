<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Reklame_View {
	static function editReklame($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=reklame&task=edit&id=<?php echo $obj->reklameDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Izin Pemasangan Reklame</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=reklame&task=save" method="POST" >
				<input type="hidden" id="reklameID" name="reklameID" value="<?php echo $obj->reklameID; ?>" />
				<input type="hidden" id="reklameDmhnID" name="reklameDmhnID" value="<?php echo $obj->reklameDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->reklameImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('reklameGrupID', $obj->reklameGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=reklame&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=reklame&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo ($objDetailPermohonan->mhnPerusahaanKelurahanID > 0 ? $objDetailPermohonan->mhnPerusahaanAlamat : $objDetailPermohonan->perAlamat); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".($objDetailPermohonan->mhnPerusahaanKelurahanID > 0 ? $objDetailPermohonan->mhnPerusahaanKelurahanID : $objDetailPermohonan->perKelurahanID)."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="reklameKeterangan" name="reklameKeterangan" maxlength="255" size="50" value="<?php echo $obj->reklameKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Reklame</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="reklameBahan" name="reklameBahan" maxlength="255" size="50" value="<?php echo $obj->reklameBahan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bunyi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="reklameBunyi" name="reklameBunyi" maxlength="255" size="50" value="<?php echo $obj->reklameBunyi; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Ukuran 1</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="reklamePanjang" name="reklamePanjang" maxlength="11" size="11" value="<?php echo $obj->reklamePanjang; ?>" />
						m x
						<input class="box" id="reklameLebar" name="reklameLebar" maxlength="11" size="11" value="<?php echo $obj->reklameLebar; ?>" />
						m
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Ukuran 2</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="reklamePanjang2" name="reklamePanjang2" maxlength="11" size="11" value="<?php echo $obj->reklamePanjang2; ?>" />
						m x
						<input class="box" id="reklameLebar2" name="reklameLebar2" maxlength="11" size="11" value="<?php echo $obj->reklameLebar2; ?>" />
						m
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Ukuran 3</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="reklamePanjang3" name="reklamePanjang3" maxlength="11" size="11" value="<?php echo $obj->reklamePanjang3; ?>" />
						m x
						<input class="box" id="reklameLebar3" name="reklameLebar3" maxlength="11" size="11" value="<?php echo $obj->reklameLebar3; ?>" />
						m
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jumlah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="reklameJumlah" name="reklameJumlah" maxlength="11" size="11" value="<?php echo $obj->reklameJumlah; ?>" />
						Unit
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Pemasangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="reklamePemasanganLokasi" name="reklamePemasanganLokasi" maxlength="255" size="50" value="<?php echo $obj->reklamePemasanganLokasi; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->reklamePemasanganKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$reklamePemasanganKelurahan = $app->queryObject($sql);
		if (!$reklamePemasanganKelurahan) {
			$reklamePemasanganKelurahan = new stdClass();
			$reklamePemasanganKelurahan->kode = '';
			$reklamePemasanganKelurahan->value = '';
		}
?>
						<input type="hidden" id="reklamePemasanganKelurahanID" name="reklamePemasanganKelurahanID" value="<?php echo $obj->reklamePemasanganKelurahanID; ?>" />
						<input class="box readonly" id="reklamePemasanganKelurahanKode" name="reklamePemasanganKelurahanKode" size="8" value="<?php echo $reklamePemasanganKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="reklamePemasanganKelurahan" name="reklamePemasanganKelurahan" maxlength="500" size="70" value="<?php echo $reklamePemasanganKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lama Pemasangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date" id="reklamePemasanganTglMulai" name="reklamePemasanganTglMulai" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->reklamePemasanganTglMulai); ?>">
						s.d.
						<input class="box date" id="reklamePemasanganTglSampai" name="reklamePemasanganTglSampai" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->reklamePemasanganTglSampai); ?>">
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->reklameNo > 0) {
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/IR/<?php echo substr($obj->reklameTglPengesahan,0,4); ?>/</span>
						<input class="box" id="reklameNo" name="reklameNo" maxlength="5" size="5" value="<?php echo $obj->reklameNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="reklameNo" name="reklameNo" value="<?php echo $obj->reklameNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="reklameTglPengesahan" name="reklameTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->reklameTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="reklamePejID" name="reklamePejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->reklamePejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		/*$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='reklame'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->reklameTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->reklameTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}*/
?>
						<!-- <input type="hidden" id="reklameTglBerlaku" name="reklameTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->reklameTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>"> -->
						
						<input class="box date2" id="reklameTglBerlaku" name="reklameTglBerlaku" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->reklameTglBerlaku); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		/*if ($obj->reklameTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->reklameTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}*/
?>
						<!-- <input type="hidden" id="reklameTglDaftarUlang" name="reklameTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->reklameTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>"> -->
						
						<input class="box date2" id="reklameTglDaftarUlang" name="reklameTglDaftarUlang" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->reklameTglDaftarUlang); ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jsreklame/edit.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printReklame($obj) {
		global $app;
		
		$objReport 		= new Report('Izin Pemasangan Reklame', 'IR-'.substr($obj->reklameTglPengesahan,0,4).'-'.$obj->reklameNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->reklamePejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<h4 align="center">
		KEPUTUSAN KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?><br>
		NOMOR : <?php echo $obj->reklameNoLengkap; ?><br>
		TENTANG<br>
		SURAT IZIN PEMASANGAN REKLAME<br>
		<br>
		KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?>,
	</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			Surat Permohonan dari Sdr <?php echo strtoupper($obj->mhnNama); ?> tentang Izin Pemasangan Reklame tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>.
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">
			Bahwa untuk kelancaran Pemasangan Reklame (Billboard, Baliho, Umbul-umbul, Spanduk, Banner) yang bersangkutan telah memenuhi syarat-syarat untuk itu kami dapat memberikan Izin Pemasangan Reklame.
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
<?php 
		echo $konfigurasi['consideringReklame'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">PERTAMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Memberikan Izin Kepada :</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top"></td>
		<td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0">
			<tr>
				<td align="left" valign="top" width="38%">- Nama Instansi</td>
				<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="60%"><?php echo strtoupper($obj->perNama); ?></td>
			</tr>
<?php 
		$sql = "SELECT wilayah2.wilNama AS kecamatan, CONCAT(wilayah3.wilTingkat,' ',wilayah3.wilNama) AS kabupaten  
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".($obj->mhnPerusahaanKelurahanID > 0 ? $obj->mhnPerusahaanKelurahanID : $obj->perKelurahanID)."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasiPerusahaan = $app->queryObject($sql); 
?>
			<tr>
				<td align="left" valign="top">- Alamat</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo ($obj->mhnPerusahaanKelurahanID > 0 ? $obj->mhnPerusahaanAlamat : $obj->perAlamat).', '.$objLokasiPerusahaan->kecamatan.', '.$objLokasiPerusahaan->kabupaten; ?></td>
			</tr>
			<tr>
				<td colspan="3">Untuk memasang Reklame</td>
			</tr>
			<tr>
				<td align="left" valign="top">- Reklame berbunyi</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->reklameBunyi; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">- Lokasi Pemasangan terletak</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->reklamePemasanganLokasi; ?></td>
			</tr>
<?php 
		$sql = "SELECT wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilNama AS kabupaten 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->reklamePemasanganKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objPemasangan = $app->queryObject($sql);
?>
			<tr>
				<td align="left" valign="top">- Desa/Kelurahan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $objPemasangan->kelurahan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">- Kecamatan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $objPemasangan->kecamatan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">- Kabupaten</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $objPemasangan->kabupaten; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">- Terbuat dari</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->reklameBahan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">- Ukuran Reklame<?php echo ($obj->reklamePanjang2 != '') ? ' I' : ''; ?></td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->reklamePanjang; ?> m X <?php echo $obj->reklameLebar; ?> m</td>
			</tr>
<?php
		if ($obj->reklamePanjang2 != '') {
?>
			<tr>
				<td align="left" valign="top">- Ukuran Reklame II</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->reklamePanjang2; ?> m X <?php echo $obj->reklameLebar2; ?> m</td>
			</tr>
<?php
		}
		if ($obj->reklamePanjang3 != '') {
?>
			<tr>
				<td align="left" valign="top">- Ukuran Reklame III</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->reklamePanjang3; ?> m X <?php echo $obj->reklameLebar3; ?> m</td>
			</tr>
<?php
		}
?>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3"><br></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEDUA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			Izin diberikan dengan ketentuan sebagai berikut:
		</td>
	</tr>
<?php 
	$masa = $app->_date_diff(strtotime($obj->reklamePemasanganTglSampai), strtotime($obj->reklamePemasanganTglMulai));
?>
	<tr>
		<td width="20%" align="left" valign="top"></td><td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table>
				<tr><td width="4%">1.</td><td width="96%">Pemasangan tidak bertentangan dengan ketertiban, keamanan, kesusilaan, dan Lalu Lintas Jalan.</td></tr>
				<tr><td>2.</td><td>Lokasi pemasangan Reklame telah di tetapkan.</td></tr>
				<tr><td>3.</td><td>Jika Pemerintah memerlukan dan menghendaki tempat tersebut, maka Pemasangan Reklame harus di pindahkan tanpa mendapatkan ganti rugi.</td></tr>
				<tr><td>4.</td><td>Yang bersangkutan membayar Pajak Reklame sesuai dengan masa yang di tentukan dalam surat Izin ini.</td></tr>
				<tr><td>5.</td><td>Pemasangan Reklame berlaku untuk masa <?php echo ($masa->days + 1).' ('.$app->terbilang($masa->days + 1).')'; ?> hari terhitung mulai tanggal <?php echo $app->MySQLDateToIndonesia($obj->reklamePemasanganTglMulai); ?> s.d. <?php echo $app->MySQLDateToIndonesia($obj->reklamePemasanganTglSampai); ?></td></tr>
				<tr><td>6.</td><td>Jika ingin memperpanjang Izinnya kembali, maka harus mengajukan Permohonan perpanjangan kepada Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (<?php echo CFG_COMPANY_SHORT_NAME; ?>) Kabupaten Pelalawan selambat-lambatnya dalam waktu 1 (satu) bulan masa berlaku Izinnya berakhir.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="3"><br></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KETIGA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Surat Izin ini dicabut kembali, apabila Pengusaha/Pemegang Izin tidak mentaati ketentuan-ketentuan tersebut di atas dan melanggar Peraturan Perundang-undangan yang berlaku.</td>
	</tr>
	<tr>
		<td colspan="3"><br></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEEMPAT</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Keputusan ini mulai berlaku sejak tanggal ditetapkan.</td>
	</tr>
	</table>

	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="48%">&nbsp;</td>
		<td width="52%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->reklameTglPengesahan)); ?>
		</td>
	</tr>
	</table>

	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:8px">
	<tr>
		<td width="50">Tembusan :</td>
		<td>Disampaikan Kepada Yth. :</td>
	</tr>
	<tr>
		<td></td>
		<td>1. Kepala BPKAD <?php echo $objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>2. Camat <?php echo $objPemasangan->kecamatan; ?></td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readReklame($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=reklame&task=edit&id=<?php echo $obj->reklameDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Pemasangan Reklame</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=reklame&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=reklame&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnPerusahaanKelurahanID > 0 ? $obj->mhnPerusahaanAlamat : $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".($obj->mhnPerusahaanKelurahanID > 0 ? $obj->mhnPerusahaanKelurahanID : $obj->perKelurahanID)."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->reklameKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Reklame</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->reklameBahan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bunyi</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->reklameBunyi; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Ukuran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->reklamePanjang; ?> m x <?php echo $obj->reklameLebar; ?> m
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jumlah</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->reklameJumlah; ?> Unit
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Lokasi Pemasangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->reklamePemasanganLokasi; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->reklamePemasanganKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$reklamePemasanganKelurahan = $app->queryObject($sql);
		if (!$reklamePemasanganKelurahan) {
			$reklamePemasanganKelurahan = new stdClass();
			$reklamePemasanganKelurahan->kode = '';
			$reklamePemasanganKelurahan->value = '';
		}
?>
				<?php echo $reklamePemasanganKelurahan->value; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Lama Pemasangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->reklamePemasanganTglMulai); ?> s.d. <?php echo $app->MySQLDateToNormal($obj->reklamePemasanganTglSampai); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->reklameNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->reklameTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->reklamePejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->reklameTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->reklameTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->reklameTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->reklameTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jsreklame/read.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewReklame($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=reklame"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Pemasangan Reklame</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('reklameNo', 'Nomor Surat', true),
			$app->setHeader('reklameTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('reklameTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('reklameTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=reklame&task=read&id='.$v->reklameDmhnID.'&fromact=reklame" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->reklameDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=reklame&task=read&id='.$v->reklameDmhnID.'&fromact=reklame" title="Lihat Surat Izin">'.$v->reklameNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->reklameTglPengesahan).'</td>';
				
				if ($v->reklameTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->reklameTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->reklameTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->reklameTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/jsreklame/view.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>