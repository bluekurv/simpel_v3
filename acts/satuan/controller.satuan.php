<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.satuan.php';
require_once 'view.satuan.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editSatuan($app->id);
		break;
	case 'delete':
		deleteSatuan($app->id);
		break;
	case 'save':
		saveSatuan();
		break;
	default:
		viewSatuan(true, '');
		break;
}

function deleteSatuan($id) {
	global $app;
	
	//Get object
	$objSatuan = $app->queryObject("SELECT * FROM satuan WHERE satID='".$id."'");
	if (!$objSatuan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM satuan WHERE satID='".$id."'");
		
		$success = true;
		$msg = 'Satuan "'.$objSatuan->satNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'satuan', $objSatuan->satID, $objSatuan->satNama, $msg);
	} else {
		$success = false;
		$msg = 'Satuan "'.$objSatuan->satNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewSatuan($success, $msg);
}

function editSatuan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objSatuan = $app->queryObject("SELECT * FROM satuan WHERE satID='".$id."'");
		if (!$objSatuan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objSatuan = new Satuan_Model();
	}
	
	Satuan_View::editSatuan(true, '', $objSatuan);
}

function saveSatuan() {
	global $app;
	
	//Create object
	$objSatuan = new Satuan_Model();
	$app->bind($objSatuan);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objSatuan->satKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objSatuan->satNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT satID AS id FROM satuan WHERE satKode='".$objSatuan->satKode."'", $objSatuan->satID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objSatuan->satKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT satID AS id FROM satuan WHERE satNama='".$objSatuan->satNama."'", $objSatuan->satID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objSatuan->satNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objSatuan->satID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objSatuan);
		} else {
			$sql = $app->createSQLforUpdate($objSatuan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objSatuan->satID = $app->queryField("SELECT LAST_INSERT_ID() FROM satuan");
			
			$success = true;
			$msg = 'Satuan "'.$objSatuan->satNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'satuan', $objSatuan->satID, $objSatuan->satNama, $msg);
		} else {
			$success = true;
			$msg = 'Satuan "'.$objSatuan->satNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'satuan', $objSatuan->satID, $objSatuan->satNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Satuan "'.$objSatuan->satNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewSatuan($success, $msg);
	} else {
		Satuan_View::editSatuan($success, $msg, $objSatuan);
	}
}

function viewSatuan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('satuan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('satuan', 'sort', 'satKode');
	$dir   		= $app->pageVar('satuan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('satuan', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(satKode LIKE '%".$nama."%' OR satNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM satuan
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM satuan
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Satuan_View::viewSatuan($success, $msg, $arr);
}
?>