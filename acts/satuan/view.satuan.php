<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Satuan_View {
	static function editSatuan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=satuan&task=edit&id=<?php echo $obj->satID; ?>"><img src="images/icons/database_table.png" border="0" /> <?php echo ($obj->satID > 0) ? 'Ubah' : 'Tambah'; ?> Satuan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=satuan&task=save" method="POST" >
				<input type="hidden" id="satID" name="satID" value="<?php echo $obj->satID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=satuan">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Kode <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="satKode" name="satKode" maxlength="5" size="5" value="<?php echo $obj->satKode; ?>" />
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="satNama" name="satNama" maxlength="255" size="50" value="<?php echo $obj->satNama; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/satuan/js/edit.satuan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewSatuan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=satuan"><img src="images/icons/database_table.png" border="0" /> Satuan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=satuan&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Kode/Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('satKode', 'Kode', true, 40),
			$app->setHeader('satNama', 'Nama', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=satuan&task=edit&id='.$v->satID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->satID.', '."'".$v->satNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=satuan&task=edit&id='.$v->satID.'">'.$v->satKode.'</a></td>';
				echo '<td>'.$v->satNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="3">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/satuan/js/satuan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>