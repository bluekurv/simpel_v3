//Variables
var form = $("#myForm");
var satKode = $("#satKode");
var satNama = $("#satNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'satKode' || this.id === undefined) {
		if (satKode.val().length == 0){
			doSubmit = false;
			satKode.addClass("error");		
		} else {
			satKode.removeClass("error");
		}
	}
	
	if (this.id == 'satNama' || this.id === undefined) {
		if (satNama.val().length == 0){
			doSubmit = false;
			satNama.addClass("error");		
		} else {
			satNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
satKode.blur(validateForm);
satNama.blur(validateForm);

satKode.keyup(validateForm);
satNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
satKode.focus();