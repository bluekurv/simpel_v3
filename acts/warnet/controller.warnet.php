<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'warnet';
$childTitle = 'Izin Operasional Warnet';
$childKodeSurat = 'IOP-W';
$childTembusan = 'Kepala Dinas Perhubungan, Komunikasi dan Informasi Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>