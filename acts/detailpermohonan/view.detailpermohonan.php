<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class DetailPermohonan_View {
	static function editDetailPermohonan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=detailpermohonan&task=edit&id=<?php echo $obj->dmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Entri Detail Permohonan (Alur Penyelesaian) untuk <?php echo $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : ''); ?> pada No. Pendaftaran <?php echo $obj->mhnNoUrutLengkap; ?>, tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl);  ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=detailpermohonan&task=save<?php echo $app->getVarsFrom(true); ?>" method="POST" >
				<input type="hidden" id="dmhnID" name="dmhnID" value="<?php echo $obj->dmhnID; ?>" />
				<input type="hidden" id="dmhnMhnID" name="dmhnMhnID" value="<?php echo $obj->dmhnMhnID; ?>" />
				<input type="hidden" id="dmhnPerID" name="dmhnPerID" value="<?php echo $obj->dmhnPerID; ?>" />
				<input type="hidden" id="dmhnJsiID" name="dmhnJsiID" value="<?php echo $obj->dmhnJsiID; ?>" />
				<input type="hidden" id="dmhnTipe" name="dmhnTipe" value="<?php echo $obj->dmhnTipe; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) diisi oleh anda disesuaikan dengan kebutuhan.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="180">Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		switch ($obj->dmhnTipe) {
			case 'Baru':
				echo '<span class="badge open">Baru</span>';
				break;
			case 'Perubahan':
				echo '<span class="badge info">Perubahan</span>';
				break;
			case 'Perpanjangan':
				echo '<span class="badge close">Perpanjangan</span>';
				break;
		}
?>
					</td>
				</tr>
				<tr>
					<td>Jenis Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : ''); ?>
					</td>
				</tr>
				<tr>
					<td>Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td>Perusahaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->perNama; ?>
					</td>
				</tr>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$obj->dmhnJsiID."'");
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Administrasi') {
?>
				<tr>
					<td>Prosedur Pelayanan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="dmhnPola" name="dmhnPola">
							<option value="0">Belum Ditentukan</option>
<?php 
			if ($objJenisSuratIzin->jsiLamaPengurusan1 > 0) {
?>
							<option value="1" <?php echo ($obj->dmhnPola == 1) ? 'selected' : ''; ?>>Pola 1 (<?php echo $objJenisSuratIzin->jsiLamaPengurusan1; ?> hari kerja)</option>
<?php 
			}
			if ($objJenisSuratIzin->jsiLamaPengurusan2 > 0) {
?>
							<option value="2" <?php echo ($obj->dmhnPola == 2) ? 'selected' : ''; ?>>Pola 2 (<?php echo $objJenisSuratIzin->jsiLamaPengurusan2; ?> hari kerja)</option>
<?php 
			}
?>
						</select>
					</td>
				</tr>
<?php 
		} else {
?>
				<tr>
					<td>Prosedur Pelayanan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnPola" name="dmhnPola" value="<?php echo $obj->dmhnPola; ?>">
<?php 
			switch ($obj->dmhnPola) {
				case 2:
					echo 'Pola 2 ('.$objJenisSuratIzin->jsiLamaPengurusan2.' hari kerja)';
					break;
				case 1:
					echo 'Pola 1 ('.$objJenisSuratIzin->jsiLamaPengurusan1.' hari kerja)';
					break;
				default:
					echo 'Belum Ditentukan';
			}
?>
					</td>
				</tr>
<?php
		}
?>
				<tr>
					<td colspan="3"><p><b><u>Survey</u></b></p></td>
				</tr>
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Administrasi') {
?>
				<tr>
					<td style="padding-left:20px;">Perlu Survey? <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
			$app->createSwitchCheckBox('dmhnPerluSurvey', 1, 'Ya', 0, 'Tidak', $obj->dmhnPerluSurvey);
?>
					</td>
				</tr>
<?php 
		} else {
?>
				<tr>
					<td style="padding-left:20px;">Perlu Survey?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnPerluSurvey" name="dmhnPerluSurvey" value="<?php echo ($obj->dmhnPerluSurvey == 1) ? 'on' : ''; ?>">
						<?php echo ($obj->dmhnPerluSurvey == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
<?php 
		}
		if ($_SESSION['sesipengguna']->levelAkses == 'Surveyor') {
?>
				<tr>
					<td style="padding-left:20px;">Telah Survey? <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
			$app->createSwitchCheckBox('dmhnTelahSurvey', 1, 'Ya', 0, 'Tidak', $obj->dmhnTelahSurvey);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Survey <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="dmhnTglSurvey" name="dmhnTglSurvey" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglSurvey); ?>" />
					</td>
				</tr>
				<tr>
					<td valign="top" style="padding-left:20px;">Hasil Survey <span class="wajib">*</span></td><td valign="top">&nbsp;:&nbsp;</td>
					<td valign="top">
						<!-- <textarea class="box" id="dmhnHasilSurvey" name="dmhnHasilSurvey" cols="80" rows="5"><?php echo $obj->dmhnHasilSurvey; ?></textarea> -->
<?php 
				$app->createSwitchRadioThree('dmhnHasilSurvey', 'Memenuhi Syarat', 'Memenuhi Syarat', 'Ditunda', 'Ditunda', 'Batal', 'Batal', $obj->dmhnHasilSurvey);
?>

					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Dientri Oleh <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="dmhnSurveyOleh" name="dmhnSurveyOleh">
							<option value="0"></option>
<?php 
			$sql = "SELECT pnID AS id, pnNama AS nama
					FROM pengguna 
					WHERE pnLevelAkses='Surveyor' 
					ORDER BY pnNama";
			$rsSurveyOleh = $app->query($sql);
			
			while(($objSurveyOleh = mysql_fetch_object($rsSurveyOleh)) == true){
				echo '<option value="'.$objSurveyOleh->id.'"';
				if ($objSurveyOleh->id == $obj->dmhnSurveyOleh) {
					echo ' selected';	
				}
				echo '>'.$objSurveyOleh->nama.'</option>';
			}
?>
						</select>
					</td>
				</tr>
<?php 
		} else {
			if ($obj->dmhnPerluSurvey == 1) {
?>
				<tr>
					<td style="padding-left:20px;">Telah Survey?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnTelahSurvey" name="dmhnTelahSurvey" value="<?php echo ($obj->dmhnTelahSurvey == 1) ? 'on' : ''; ?>">
						<?php echo ($obj->dmhnTelahSurvey == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Survey</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnTglSurvey" name="dmhnTglSurvey" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglSurvey); ?>">
						<?php echo $app->MySQLDateToNormal($obj->dmhnTglSurvey); ?>
					</td>
				</tr>
				<tr>
					<td valign="top" style="padding-left:20px;">Hasil Survey</td><td valign="top">&nbsp;:&nbsp;</td>
					<td valign="top">
						<input type="hidden" id="dmhnHasilSurvey" name="dmhnHasilSurvey" value="<?php echo $obj->dmhnHasilSurvey; ?>">
						<?php echo $obj->dmhnHasilSurvey; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Dientri Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnSurveyOleh" name="dmhnSurveyOleh" value="<?php echo $obj->dmhnSurveyOleh; ?>">
<?php 
			echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->dmhnSurveyOleh."'");
?>
					</td>
				</tr>
<?php
			} else {
?>
				<input type="hidden" id="dmhnTelahSurvey" name="dmhnTelahSurvey" value="<?php echo ($obj->dmhnTelahSurvey == 1) ? 'on' : ''; ?>">
				<input type="hidden" id="dmhnTglSurvey" name="dmhnTglSurvey" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglSurvey); ?>">
				<input type="hidden" id="dmhnHasilSurvey" name="dmhnHasilSurvey" value="<?php echo $obj->dmhnHasilSurvey; ?>">
				<input type="hidden" id="dmhnSurveyOleh" name="dmhnSurveyOleh" value="<?php echo $obj->dmhnSurveyOleh; ?>">
<?php
			}
		}
?>
				<tr>
					<td colspan="3"><p><b><u>Pengurusan</u></b></p></td>
				</tr>
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Administrasi') {
?>
				<tr>
					<td style="padding-left:20px;">Tanggal Target Selesai <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="dmhnTglTargetSelesai" name="dmhnTglTargetSelesai" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglTargetSelesai); ?>" />
					</td>
				</tr>
<?php 
		} else {
?>
				<tr>
					<td style="padding-left:20px;">Tanggal Target Selesai</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnTglTargetSelesai" name="dmhnTglTargetSelesai" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglTargetSelesai); ?>">
						<?php echo $app->MySQLDateToNormal($obj->dmhnTglTargetSelesai); ?>
					</td>
				</tr>
<?php
		}
?>
				<tr>
					<td colspan="3"><p><b><u>Penyerahan</u></b></p></td>
				</tr>
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Loket Penyerahan') {
?>
				<tr>
					<td style="padding-left:20px;">Telah Selesai? <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
			$app->createSwitchCheckBox('dmhnTelahSelesai', 1, 'Ya', 0, 'Tidak', $obj->dmhnTelahSelesai);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Selesai <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="dmhnTglSelesai" name="dmhnTglSelesai" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglSelesai); ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Telah Diserahkan? <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
			$app->createSwitchCheckBox('dmhnTelahAmbil', 1, 'Ya', 0, 'Tidak', $obj->dmhnTelahAmbil);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Penyerahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="dmhnTglAmbil" name="dmhnTglAmbil" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglAmbil); ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Diserahkan Kepada <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" type="text" id="dmhnPengambil" name="dmhnPengambil" maxlength="255" size="50" value="<?php echo $obj->dmhnPengambil; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Identitas <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" type="text" id="dmhnNoIdentitas" name="dmhnNoIdentitas" maxlength="255" size="50" value="<?php echo $obj->dmhnNoIdentitas; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Diserahkan Oleh <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="dmhnDiserahkanOleh" name="dmhnDiserahkanOleh">
							<option value="0"></option>
<?php 
			$sql = "SELECT pnID AS id, pnNama AS nama
					FROM pengguna 
					WHERE pnLevelAkses='Petugas Loket Penyerahan' 
					ORDER BY pnNama";
			$rsDiserahkanOleh = $app->query($sql);
			
			while(($objDiserahkanOleh = mysql_fetch_object($rsDiserahkanOleh)) == true){
				echo '<option value="'.$objDiserahkanOleh->id.'"';
				if ($objDiserahkanOleh->id == $obj->dmhnDiserahkanOleh) {
					echo ' selected';	
				}
				echo '>'.$objDiserahkanOleh->nama.'</option>';
			}
?>
						</select>
					</td>
				</tr>
<?php 
		} else {
?>
				<tr>
					<td style="padding-left:20px;">Telah Selesai?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnTelahSelesai" name="dmhnTelahSelesai" value="<?php echo ($obj->dmhnTelahSelesai == 1) ? 'on' : ''; ?>">
						<?php echo ($obj->dmhnTelahSelesai == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Selesai</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnTglSelesai" name="dmhnTglSelesai" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglSelesai); ?>">
						<?php echo $app->MySQLDateToNormal($obj->dmhnTglSelesai); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Telah Diserahkan?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnTelahAmbil" name="dmhnTelahAmbil" value="<?php echo ($obj->dmhnTelahAmbil == 1) ? 'on' : ''; ?>">
						<?php echo ($obj->dmhnTelahAmbil == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Penyerahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnTglAmbil" name="dmhnTglAmbil" value="<?php echo $app->MySQLDateToNormal($obj->dmhnTglAmbil); ?>" />
						<?php echo $app->MySQLDateToNormal($obj->dmhnTglAmbil); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Diserahkan Kepada</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnPengambil" name="dmhnPengambil" value="<?php echo $obj->dmhnPengambil; ?>" />
						<?php echo $obj->dmhnPengambil; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Identitas</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnNoIdentitas" name="dmhnNoIdentitas" value="<?php echo $obj->dmhnNoIdentitas; ?>" />
						<?php echo $obj->dmhnNoIdentitas; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Diserahkan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnDiserahkanOleh" name="dmhnDiserahkanOleh" value="<?php echo $obj->dmhnDiserahkanOleh; ?>" />
						<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->dmhnDiserahkanOleh."'"); ?>
					</td>
				</tr>
<?php 
		}
?>
				<tr>
					<td colspan="3"><p><b><u>Pengembalian</u></b></p></td>
				</tr>
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Administrasi') {
?>
				<tr>
					<td style="padding-left:20px;">Dikembalikan? <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
			$app->createSwitchCheckBox('dmhnDikembalikan', 1, 'Ya', 0, 'Tidak', $obj->dmhnDikembalikan);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alasan Pengembalian <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="dmhnDikembalikanAlasan" name="dmhnDikembalikanAlasan" cols="80" rows="5"><?php echo $obj->dmhnDikembalikanAlasan; ?></textarea>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pengembalian <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->dmhnDikembalikanNo > 0) {
?>
						<input class="box" id="dmhnDikembalikanNo" name="dmhnDikembalikanNo" maxlength="11" size="11" value="<?php echo $obj->dmhnDikembalikanNo; ?>">
<?php 
			$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
			echo '/'.$objSatuanKerja->skrNamaSingkat.'/TU-TTP/'.$app->getRoman(intval(substr($obj->dmhnDikembalikanTgl,5,2))).'/'.substr($obj->dmhnDikembalikanTgl,0,4);
		}  else {
?>
						<input type="hidden" id="dmhnDikembalikanNo" name="dmhnDikembalikanNo" value="<?php echo $obj->dmhnDikembalikanNo; ?>">
<?php
			echo 'Terakhir';
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Pengembalian <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="dmhnDikembalikanTgl" name="dmhnDikembalikanTgl" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->dmhnDikembalikanTgl); ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Mengetahui <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="dmhnDikembalikanMengetahui" name="dmhnDikembalikanMengetahui">
							<option value="0"></option>
<?php 
			$sql = "SELECT pnID AS id, pnNama AS nama
					FROM pengguna 
					WHERE pnLevelAkses='Pejabat' 
					ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
			$rsDikembalikanMengetahui = $app->query($sql);
			
			while(($objDikembalikanMengetahui = mysql_fetch_object($rsDikembalikanMengetahui)) == true){
				echo '<option value="'.$objDikembalikanMengetahui->id.'"';
				if ($objDikembalikanMengetahui->id == $obj->dmhnDikembalikanMengetahui) {
					echo ' selected';	
				}
				echo '>'.$objDikembalikanMengetahui->nama.'</option>';
			}
?>
						</select>
					</td>
				</tr>
<?php 
		} else {
?>
				<tr>
					<td style="padding-left:20px;">Dikembalikan?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnDikembalikan" name="dmhnDikembalikan" value="<?php echo $obj->dmhnDikembalikan; ?>">
						<?php echo ($obj->dmhnDikembalikan == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alasan Pengembalian</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnDikembalikanAlasan" name="dmhnDikembalikanAlasan" value="<?php echo $obj->dmhnDikembalikanAlasan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pengembalian</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnDikembalikanNo" name="dmhnDikembalikanNo" value="<?php echo $obj->dmhnDikembalikanNo; ?>">
<?php 
		if ($obj->dmhnDikembalikanNo > 0) {
			$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
			echo '/'.$objSatuanKerja->skrNamaSingkat.'/TU-TTP/'.$app->getRoman(intval(substr($obj->mhnDikembalikanTgl,5,2))).'/'.substr($obj->mhnDikembalikanTgl,0,4);
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Pengembalian</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnDikembalikanTgl" name="dmhnDikembalikanTgl" value="<?php echo $app->MySQLDateToNormal($obj->dmhnDikembalikanTgl); ?>" />
						<?php echo $app->MySQLDateToNormal($obj->dmhnDikembalikanTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Mengetahui</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="dmhnDikembalikanMengetahui" name="dmhnDikembalikanMengetahui" value="<?php echo $obj->dmhnDikembalikanMengetahui; ?>" />
						<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->dmhnDikembalikanMengetahui."'"); ?>
					</td>
				</tr>
<?php 
		}
?>
				</table>
			</form>
		</div>
		
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript">
		natDays = <?php echo json_encode($app->getHolidays()); ?>;
	</script>
	<script type="text/javascript" src="acts/detailpermohonan/js/edit.detailpermohonan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printpengembalianDetailPermohonan($obj) {
		global $app;
		
		$objReport 		= new Report('Tanda Terima Pengembalian Dokumen', 'TandaTerimaPengembalianDokumen-'.$obj->dmhnDikembalikanNo.'-'.substr($obj->dmhnDikembalikanTgl,0,4).'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengguna 	= $app->queryObject("SELECT * FROM pengguna WHERE pnID='".$obj->dmhnDikembalikanMengetahui."'");
		
		$sql = "SELECT wilayah.*, wilayah2.wilNama AS parent1, wilayah3.wilNama AS parent2, wilayah4.wilNama AS parent3
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objMhnKelurahan = $app->queryObject($sql);
		
		$sql = "SELECT wilayah.*, wilayah2.wilNama AS parent1, wilayah3.wilNama AS parent2, wilayah4.wilNama AS parent3
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objPerKelurahan = $app->queryObject($sql);
		
		/*$sql = "SELECT *, dmhnID AS id 
				FROM detailpermohonan, jenissuratizin
				WHERE dmhnMhnID='".$obj->mhnID."' AND dmhnJsiID=jsiID";
		$arrDetailPermohonan = $app->queryArrayOfObjects($sql);*/
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<h3 align="center">TANDA TERIMA PENGEMBALIAN DOKUMEN TIDAK LENGKAP</h3>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td>
			Nomor : <?php echo $obj->dmhnDikembalikanNo.'/'.$objSatuanKerja->skrNamaSingkat.'/TU-TTP/'.$app->getRoman(intval(substr($obj->dmhnDikembalikanTgl,5,2))).'/'.substr($obj->dmhnDikembalikanTgl,0,4); ?>
		</td>
		<td align="right">
			Tanggal : <?php echo $app->MySQLDateToIndonesia($obj->dmhnDikembalikanTgl); ?>
		</td>
	</tr>
	</table>

	<br><br>

	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="25%">Nomor Pendaftaran</td><td width="2%">&nbsp;:&nbsp;</td>
		<td width="73%"><?php echo $obj->mhnNoUrutLengkap; ?></td>
	</tr>
	<tr>
		<td>Tanggal Pendaftaran</td><td>&nbsp;:&nbsp;</td>
		<td><?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?></td>
	</tr>
	<tr>
		<td>Nomor KTP</td><td>&nbsp;:&nbsp;</td>
		<td><?php echo $obj->mhnNoKtp; ?></td>
	</tr>
	<tr>
		<td>Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
		<td><?php echo $obj->mhnNama; ?></td>
	</tr>
	<tr>
		<td>Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
		<td><?php echo $obj->mhnAlamat.', '.$objMhnKelurahan->wilNama.', '.$objMhnKelurahan->parent1.', '.$objMhnKelurahan->parent2.', '.$objMhnKelurahan->parent3; ?></td>
	</tr>
	<tr>
		<td>Nama Perusahaan</td><td>&nbsp;:&nbsp;</td>
		<td><?php echo $obj->perNama; ?></td>
	</tr>
	<tr>
		<td>Alamat Perusahaan</td><td>&nbsp;:&nbsp;</td>
		<td><?php echo $obj->perAlamat.', '.$objPerKelurahan->wilNama.', '.$objPerKelurahan->parent1.', '.$objPerKelurahan->parent2.', '.$objPerKelurahan->parent3; ?></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">Untuk Pengurusan Perizinan : </td>
	</tr>
	<tr>
		<td colspan="3">
<?php 
		/*if (count($arrDetailPermohonan) > 1) {
?>
			<ol>
<?php
			foreach ($arrDetailPermohonan as $objDetailPermohonan) {
				if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
					$perkiraanselesai = ' dengan Pola yang belum ditentukan (Pola 1 atau Pola 2).';
				} else {
					$perkiraanselesai = ' dengan Pola '.$objDetailPermohonan->dmhnPola.', perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai).'.';
				}
?>
					<li>Pendaftaran <?php echo $objDetailPermohonan->dmhnTipe.' '.$objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : '').' '.$perkiraanselesai; ?></li>		
<?php
			}
?>
			</ol>
<?php
		} else if (count($arrDetailPermohonan) == 1) {
			$objDetailPermohonan = current($arrDetailPermohonan);
			
			if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
				$perkiraanselesai = ' dengan Pola yang belum ditentukan (Pola 1 atau Pola 2).';
			} else {
				$perkiraanselesai = ' dengan Pola '.$objDetailPermohonan->dmhnPola.', perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai).'.';
			}
?>
			<p>Pendaftaran <?php echo $objDetailPermohonan->dmhnTipe.' '.$objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : '').' '.$perkiraanselesai; ?></p>
<?php
		}*/
?>
<?php
			if ($obj->dmhnTglTargetSelesai == '0000-00-00') {
				$perkiraanselesai = ' dengan Pola yang belum ditentukan (Pola 1 atau Pola 2).';
			} else {
				$perkiraanselesai = ' dengan Pola '.$obj->dmhnPola.', perkiraan selesai '.$app->MySQLDateToIndonesia($obj->dmhnTglTargetSelesai).'.';
			}
?>
			<p>Pendaftaran <?php echo $obj->dmhnTipe.' '.$obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : '').' '.$perkiraanselesai; ?></p>
		</td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">SYARAT-SYARAT YANG TIDAK LENGKAP DAN TIDAK BENAR SERTA HARUS DILENGKAPI :</td>
	</tr>
	<tr>
		<td colspan="3"><?php echo $obj->dmhnDikembalikanAlasan; ?></td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="50%" align="center">
			<p>
				Yang Mengetahui:<br>
				an. Kepala <?php echo $objSatuanKerja->skrNama.' '.$objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?><br>
				<?php echo $objPengguna->pnJabatan; ?>
			</p>
		</td>
		<td width="50%" align="center">
			<p>Pemohon,</p>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td align="center"><u><?php echo strtoupper($objPengguna->pnNama); ?></u></td>
		<td align="center">(<b>......................................</b>)</td>
	</tr>
	<tr>
		<td align="center">NIP. <?php echo $objPengguna->pnNIP; ?></td>
		<td>&nbsp;</td>
	</tr>
	</table>
<?php		
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	static function printpenolakanDetailPermohonan($obj) {
		global $app;
		
		$objReport 		= new Report('Surat Penolakan Permohonan', 'SuratPenolakanPermohonan-'.$obj->dmhnDikembalikanNo.'-'.substr($obj->dmhnDikembalikanTgl,0,4).'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengguna = $app->queryObject("SELECT * FROM pengguna WHERE pnID='".$obj->dmhnDikembalikanMengetahui."'");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<h3 align="center">SURAT PENOLAKAN PERMOHONAN</h3>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%"><?php echo $objSatuanKerja->wilIbukota; ?>, <?php echo $app->MySQLDateToIndonesia($obj->dmhnDikembalikanTgl); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>Kepada Yth.</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td><?php echo strtoupper($obj->mhnNama); ?></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>di <?php echo $obj->mhnAlamat; ?></td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="15%">Nomor</td>
		<td width="85%">: <?php echo $obj->dmhnDikembalikanNo.'/'.$objSatuanKerja->skrNamaSingkat.'/TU-SP/'.$app->getRoman(intval(substr($obj->dmhnDikembalikanTgl,5,2))).'/'.substr($obj->dmhnDikembalikanTgl,0,4); ?></td>
	</tr>
	<tr>
		<td>Lampiran</td>
		<td>: -</td>
	</tr>
	<tr>
		<td>Perihal</td>
		<td>: Penolakan Permohonan Perizinan</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<p align="justify" style="text-indent:50px;">Setelah membaca permohonan Saudara nomor <?php echo $obj->mhnNoUrutLengkap; ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> tentang pengajuan permohonan dan proses verifikasi maka permohonan Saudara untuk <?php echo $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : ''); ?> tidak dapat kami penuhi dan tidak dapat diproses lebih lanjut.</p>
			<p align="justify" style="text-indent:50px;">Adapun kekurangan dari permohonan Saudara adalah sebagai berikut :</p>
			<p align="justify" style="text-indent:50px;"><?php echo $obj->dmhnDikembalikanAlasan; ?></p>
			<p align="justify" style="text-indent:50px;">Demikian disampaikan, atas kerjasamanya diucapkan terima kasih.</p>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%" align="center">
			<p><?php echo $objPengguna->pnJabatan; ?></p>
		</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td align="center"><u><?php echo strtoupper($objPengguna->pnNama); ?></u></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td align="center">NIP. <?php echo $objPengguna->pnNIP; ?></td>
	</tr>
	</table>
<?php		
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	static function readDetailPermohonan($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=detailpermohonan&task=read&id=<?php echo $obj->dmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Detail Permohonan (Alur Penyelesaian) untuk <?php echo $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : ''); ?> pada No. Pendaftaran <?php echo $obj->mhnNoUrutLengkap; ?>, tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl);  ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=detailpermohonan&task=save" method="POST" >
				<input type="hidden" id="dmhnID" name="dmhnID" value="<?php echo $obj->dmhnID; ?>" />
				<div id="toolbarEntri">
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Administrasi') {
			if ($obj->dmhnDikembalikan == 1) {
?>			
					<a class="button-link inline dark-blue" href="index2.php?act=detailpermohonan&task=printpengembalian&id=<?php echo $obj->dmhnID; ?>&html=0" target="_blank">Cetak Tanda Terima Pengembalian</a>
					<a class="button-link inline dark-blue" href="index2.php?act=detailpermohonan&task=printpenolakan&id=<?php echo $obj->dmhnID; ?>&html=0" target="_blank">Cetak Surat Penolakan</a>
<?php 
			}
		}
?>
					<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Kembali</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Administrasi') {
			if ($obj->dmhnDikembalikan == 1) {
?>			
					<a class="button-link inline dark-blue" href="index2.php?act=detailpermohonan&task=printpengembalian&id=<?php echo $obj->dmhnID; ?>&html=0" target="_blank">Cetak Tanda Terima Pengembalian</a>
					<a class="button-link inline dark-blue" href="index2.php?act=detailpermohonan&task=printpenolakan&id=<?php echo $obj->dmhnID; ?>&html=0" target="_blank">Cetak Surat Penolakan</a>
<?php 
			}
		}
?>
					<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Kembali</a>
					<br>
					<br>
				</div>
				<br>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		switch ($obj->dmhnTipe) {
			case 'Baru':
				echo '<span class="badge open">Baru</span>';
				break;
			case 'Perubahan':
				echo '<span class="badge info">Perubahan</span>';
				break;
			case 'Perpanjangan':
				echo '<span class="badge close">Perpanjangan</span>';
				break;
		}
?>
					</td>
				</tr>
				<tr>
					<td>Jenis Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : ''); ?>
					</td>
				</tr>
				<tr>
					<td>Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->mhnNama; ?>
					</td>
				</tr>
<?php
		if ($obj->dmhnPerID > 0) {
?>
				<tr>
					<td>Perusahaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->perNama; ?>
					</td>
				</tr>
<?php
		}
?>
				<tr>
					<td>Prosedur Pelayanan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		if ($obj->dmhnPola == 2) {
			echo 'Pola '.$obj->dmhnPola.' ('.$obj->jsiLamaPengurusan2.' hari kerja) dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima';
		} else if ($obj->dmhnPola == 1) {
			echo 'Pola '.$obj->dmhnPola.' ('.$obj->jsiLamaPengurusan1.' hari kerja)';
		} else {
			echo '<span class="badge tidak">Belum Ditentukan</span>';
		}
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Survey</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Perlu Survey?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->dmhnPola > 0) {
			if ($obj->dmhnPerluSurvey == 1) {
				echo '<span class="badge ya">Ya</span>';
			} else {
				echo '<span class="badge tidak">Tidak</span>'; 
			}
		} else {
			echo '<span class="badge tidak">Belum Ditentukan</span>';
		}
?>
					</td>
				</tr>
<?php 
		if ($obj->dmhnPerluSurvey == 1) {
?>
				<tr>
					<td style="padding-left:20px;">Telah Survey?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo ($obj->dmhnTelahSurvey == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
<?php 
			if ($obj->dmhnTelahSurvey == 1) {
?>
				<tr>
					<td style="padding-left:20px;">Tanggal Survey</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToNormal($obj->dmhnTglSurvey); ?>
					</td>
				</tr>
				<tr>
					<td valign="top" style="padding-left:20px;">Hasil Survey</td><td valign="top">&nbsp;:&nbsp;</td>
					<td valign="top">
						<?php echo $obj->dmhnHasilSurvey; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Dientri Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->dmhnSurveyOleh."'"); ?>
					</td>
				</tr>
<?php 
			}
		}
?>
				<tr>
					<td colspan="3"><p><b><u>Pengurusan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Target Selesai</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		if ($obj->dmhnTglTargetSelesai != "0000-00-00") {
			echo $app->MySQLDateToNormal($obj->dmhnTglTargetSelesai);
		} else {
			echo '<span class="badge tidak">Belum Ditentukan</span>';
		}
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Penyerahan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Telah Selesai?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo ($obj->dmhnTelahSelesai == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
<?php 
		if ($obj->dmhnTelahSelesai == 1) {
?>
				<tr>
					<td style="padding-left:20px;">Tanggal Selesai</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToNormal($obj->dmhnTglSelesai); ?>
					</td>
				</tr>
<?php 
		}
?>
				<tr>
					<td style="padding-left:20px;">Telah Diserahkan?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo ($obj->dmhnTelahAmbil == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
<?php 
		if ($obj->dmhnTelahAmbil == 1) {
?>
				<tr>
					<td style="padding-left:20px;">Tanggal Penyerahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToNormal($obj->dmhnTglAmbil); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Diserahkan Kepada</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->dmhnPengambil; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Identitas</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->dmhnNoIdentitas; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Diserahkan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->dmhnDiserahkanOleh."'"); ?>
					</td>
				</tr>
<?php 
		}
?>
				<tr>
					<td colspan="3"><p><b><u>Pengembalian</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Dikembalikan?</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo ($obj->dmhnDikembalikan == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>'); ?>
					</td>
				</tr>
<?php 
		if ($obj->dmhnDikembalikan == 1) {
?>
				<tr>
					<td style="padding-left:20px;">Alasan Pengembalian</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $obj->dmhnDikembalikanAlasan; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pengembalian</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
			if ($obj->dmhnDikembalikanNo > 0) {
				$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
				echo $obj->dmhnDikembalikanNo.'/'.$objSatuanKerja->skrNamaSingkat.'/TU-TTP/'.$app->getRoman(intval(substr($obj->dmhnDikembalikanTgl,5,2))).'/'.substr($obj->dmhnDikembalikanTgl,0,4);
			}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Pengembalian</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToNormal($obj->dmhnDikembalikanTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Mengetahui</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->dmhnDikembalikanMengetahui."'"); ?>
					</td>
				</tr>
<?php 
		}
?>
				</table>
			</form>
		</div>
		
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/permohonan/js/read.permohonan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}

	static function detailDetailPermohonan() {
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php"><img src="images/icons/rosette.png" border="0" /> Detail Permohonan</a></h1>
	</div>
	<div class="halamanTengah">
		
	</div>
	<div class="halamanBawah"></div>
<?php
	}
}
?>