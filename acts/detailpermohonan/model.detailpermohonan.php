<?php
class DetailPermohonan_Model {	
	public $tabel = 'detailpermohonan';
	public $primaryKey = 'dmhnID';
	
	public $dmhnID = 0;
	public $dmhnMhnID = 0;
	public $dmhnPerID = 0;
	public $dmhnJsiID = 0;
	public $dmhnTipe = '';	//Baru, Perubahan, Perpanjangan
	public $dmhnBiayaLeges = 0;
	public $dmhnBiayaAdministrasi = 0;
	public $dmhnPola = 0;	//1 atau 2, default 0
	//Survey
	public $dmhnPerluSurvey = 0;		//Ingat, ada pola 1 dan 2. Field ini mengacu kesana.
	public $dmhnTelahSurvey = 0;
	public $dmhnTglSurvey = '0000-00-00';
	public $dmhnHasilSurvey = '';
	public $dmhnSurveyOleh = 0;			//Oleh Surveyor
	//Penyelesaian
	public $dmhnTglTargetSelesai = '0000-00-00';	//Ingat, ada pola 1 dan 2. Field ini mengacu kesana, meskipun bisa diubah secara manual
	public $dmhnTelahSelesai = 0;
	public $dmhnTglSelesai = '0000-00-00';
	//Penyerahan
	public $dmhnTelahAmbil = 0;
	public $dmhnPengambil = '';
	public $dmhnNoIdentitas = '';
	public $dmhnTglAmbil = '0000-00-00';
	public $dmhnDiserahkanOleh = 0;		//Oleh Petugas Loket Penyerahan
	//Pengadministrasian
	public $dmhnDiadministrasikanOleh = 0;
	public $dmhnDiadministrasikanPada = '0000-00-00 00:00:00';
	//Pengembalian
	public $dmhnDikembalikan = 0;
	public $dmhnDikembalikanAlasan = '';
	public $dmhnDikembalikanNo = 0;
	public $dmhnDikembalikanTgl = '0000-00-00';
	public $dmhnDikembalikanMengetahui = 0;
}
?>














