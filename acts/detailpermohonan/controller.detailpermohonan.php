<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.detailpermohonan.php';
require_once 'view.detailpermohonan.php';

switch ($app->task) {
	case 'edit':
		editDetailPermohonan($app->id);
		break;
	case 'save':
		saveDetailPermohonan();
		break;
	case 'printpengembalian':
		printpengembalianDetailPermohonan($app->id);
		break;
	case 'printpenolakan':
		printpenolakanDetailPermohonan($app->id);
		break;
	case 'read':
	default:
		readDetailPermohonan($app->id);
		break;
}

function saveDetailPermohonan() {
	global $app;
	
	//NOTE: HANYA UPDATE
	
	//Create object
	$objDetailPermohonan = new DetailPermohonan_Model();
	$app->bind($objDetailPermohonan);
	
	//Modify object (if necessary)
	if (isset($_REQUEST['dmhnPerluSurvey'])) {
		$objDetailPermohonan->dmhnPerluSurvey = ($_REQUEST['dmhnPerluSurvey'] == 'on') ? 1 : 0;
	} else {
		$objDetailPermohonan->dmhnPerluSurvey = 0;
	}
	if (isset($_REQUEST['dmhnTelahSurvey'])) {
		$objDetailPermohonan->dmhnTelahSurvey = ($_REQUEST['dmhnTelahSurvey'] == 'on') ? 1 : 0;
	} else {
		$objDetailPermohonan->dmhnTelahSurvey = 0;
	}
	if (isset($_REQUEST['dmhnTelahSelesai'])) {
		$objDetailPermohonan->dmhnTelahSelesai = ($_REQUEST['dmhnTelahSelesai'] == 'on') ? 1 : 0;
	} else {
		$objDetailPermohonan->dmhnTelahSelesai = 0;
	}
	if (isset($_REQUEST['dmhnTelahAmbil'])) {
		$objDetailPermohonan->dmhnTelahAmbil = ($_REQUEST['dmhnTelahAmbil'] == 'on') ? 1 : 0;
	} else {
		$objDetailPermohonan->dmhnTelahAmbil = 0;
	}
	if (isset($_REQUEST['dmhnDikembalikan'])) {
		$objDetailPermohonan->dmhnDikembalikan = ($_REQUEST['dmhnDikembalikan'] == 'on') ? 1 : 0;
	} else {
		$objDetailPermohonan->dmhnDikembalikan = 0;
	}
	
	/*$app->debug($_REQUEST);
	$app->debug($objDetailPermohonan);
	exit();*/
	
	$objDetailPermohonan->dmhnTglSurvey = $app->NormalDateToMySQL($objDetailPermohonan->dmhnTglSurvey);
	$objDetailPermohonan->dmhnTglTargetSelesai = $app->NormalDateToMySQL($objDetailPermohonan->dmhnTglTargetSelesai);
	$objDetailPermohonan->dmhnTglSelesai = $app->NormalDateToMySQL($objDetailPermohonan->dmhnTglSelesai);
	$objDetailPermohonan->dmhnTglAmbil = $app->NormalDateToMySQL($objDetailPermohonan->dmhnTglAmbil);
	$objDetailPermohonan->dmhnDikembalikanTgl = $app->NormalDateToMySQL($objDetailPermohonan->dmhnDikembalikanTgl);
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objDetailPermohonan->dmhnTelahSurvey) {
		if ($objDetailPermohonan->dmhnTglSurvey == '0000-00-00') {
			$doSave = false;
			$msg[] = '- Tanggal Survey belum diisi';
		}
		
		if ($objDetailPermohonan->dmhnHasilSurvey == '') {
			$doSave = false;
			$msg[] = '- Hasil Survey belum diisi';
		}
		
		if ($objDetailPermohonan->dmhnSurveyOleh == 0) {
			$doSave = false;
			$msg[] = '- Dientri Oleh belum dipilih';
		}
	} else {
		$objDetailPermohonan->dmhnTglSurvey = '0000-00-00';
		$objDetailPermohonan->dmhnHasilSurvey = '';
		$objDetailPermohonan->dmhnSurveyOleh = 0;
	}
	
	if ($objDetailPermohonan->dmhnTelahSelesai) {
		if ($objDetailPermohonan->dmhnTglSelesai == '0000-00-00') {
			$doSave = false;
			$msg[] = '- Tanggal Selesai belum diisi';
		}
	}
	
	if ($objDetailPermohonan->dmhnTelahAmbil) {
		if ($objDetailPermohonan->dmhnTglAmbil == '0000-00-00') {
			$doSave = false;
			$msg[] = '- Tanggal Penyerahan belum diisi';
		}
		
		if ($objDetailPermohonan->dmhnPengambil == '') {
			$doSave = false;
			$msg[] = '- Diserahkan Kepada belum diisi';
		}
		
		if ($objDetailPermohonan->dmhnNoIdentitas == '') {
			$doSave = false;
			$msg[] = '- No. Identitas belum diisi';
		}
		
		if ($objDetailPermohonan->dmhnDiserahkanOleh == 0) {
			$doSave = false;
			$msg[] = '- Diserahkan Oleh belum dipilih';
		}
	} else {
		$objDetailPermohonan->dmhnTglAmbil = '0000-00-00';
		$objDetailPermohonan->dmhnPengambil = '';
		$objDetailPermohonan->dmhnNoIdentitas = '';
		$objDetailPermohonan->dmhnDiserahkanOleh = 0;
	}
	
	if ($objDetailPermohonan->dmhnDikembalikan) {
		if ($objDetailPermohonan->dmhnDikembalikanAlasan == '') {
			$doSave = false;
			$msg[] = '- Alasan Pengembalian belum diisi';
		}
		
		if ($objDetailPermohonan->dmhnDikembalikanNo > 0) {
			$isExist = $app->isExist("SELECT dmhnID AS id FROM detailpermohonan WHERE dmhnDikembalikanNo='".$objDetailPermohonan->dmhnDikembalikanNo."'", $objDetailPermohonan->dmhnID);
			if (!$isExist) {
				$doSave = false;
				$msg[]  = '- No. Pengembalian "'.$objDetailPermohonan->dmhnDikembalikanNo.'" sudah ada';
			}
		}
		
		if ($objDetailPermohonan->dmhnDikembalikanTgl == '0000-00-00') {
			$doSave = false;
			$msg[] = '- Tanggal Pengembalian belum diisi';
		} else {
			if ($objDetailPermohonan->dmhnDikembalikanNo == 0) {
				//Ambil terakhir
				$objDetailPermohonan->dmhnDikembalikanNo = intval($app->queryField("SELECT MAX(dmhnDikembalikanNo) AS nomor FROM detailpermohonan WHERE YEAR(dmhnDikembalikanTgl)='".substr($objDetailPermohonan->dmhnDikembalikanTgl,0,4)."'")) + 1;
			}
		}
		
		if ($objDetailPermohonan->dmhnDikembalikanMengetahui == 0) {
			$doSave = false;
			$msg[] = '- Mengetahui belum dipilih';
		}
	} else {
		$objDetailPermohonan->dmhnDikembalikanAlasan = '';
		$objDetailPermohonan->dmhnDikembalikanNo = 0;
		$objDetailPermohonan->dmhnDikembalikanTgl = '0000-00-00';
		$objDetailPermohonan->dmhnDikembalikanMengetahui = 0;
	}
	
	//Query
	if ($doSave) {
		$arrExcludeField = array('dmhnDiadministrasikanOleh', 'dmhnDiadministrasikanPada');
		
		$sql = $app->createSQLforUpdate($objDetailPermohonan, $arrExcludeField);
		$app->query($sql);
		
		$success = true;
		$msg = 'Detail Permohonan berhasil diubah';
		
		//Pindahkan IP ke konfigurasi
		/*if ($objDetailPermohonan->dmhnTelahSelesai == 1 && $objDetailPermohonan->dmhnTglSelesai != '0000-00-00') {
			 $objPermohonan = $app->queryObject("SELECT * FROM permohonan, detailpermohonan, jenissuratizin WHERE dmhnID='".$objDetailPermohonan->dmhnID."' AND dmhnMhnID=mhnID AND dmhnJsiID=jsiID");
			 //IP TERPASANG MODEM
			 require_once CFG_ABSOLUTE_PATH.'/libraries/Curl/Curl.php';
			 $txtsms = "Status Permohonan ".$objPermohonan->mhnNoUrutLengkap.": ".$objPermohonan->jsiNama." sudah selesai";
			 $url = "http://10.10.10.2/kirimsms/kirim.php?notlp=".$objPermohonan->mhnNoTelp."&txtsms=".urlencode($txtsms);
			 $curl = new Curl();
			 $curl->get($url);
		}*/
		
		$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, '', $msg);
	} else {
		$success = false;
		$msg = 'Detail Permohonan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	$objDetailPermohonan->jsiNama = $app->queryField("SELECT jsiNama FROM jenissuratizin WHERE jsiID='".$objDetailPermohonan->dmhnJsiID."'");
	$objDetailPermohonan->perNama = $app->queryField("SELECT perNama FROM perusahaan WHERE perID='".$objDetailPermohonan->dmhnPerID."'");
	
	if ($success) {
		if ($app->fromact != '') {
			header('Location:index2.php?'.$app->getVarsFrom(false).'&success=true&msg='.$msg);
		} else {
			if ($objDetailPermohonan->dmhnDikembalikan) {
				header('Location:index2.php?act=detailpermohonan&task=read&id='.$objDetailPermohonan->dmhnID);
			} else {
				header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
			}
		}
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objDetailPermohonan->dmhnID."'";
		$objDetailPermohonan2 = $app->queryObject($sql);
		
		foreach ($objDetailPermohonan2 as $k=>$v) {
			if (!isset($objDetailPermohonan->$k)) {
				$objDetailPermohonan->$k = $v;
			}
		}
		
		DetailPermohonan_View::editDetailPermohonan($success, $msg, $objDetailPermohonan);
	}
}

function editDetailPermohonan($id) {
	global $app;
	
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	DetailPermohonan_View::editDetailPermohonan(true, '', $objDetailPermohonan);
}

function printpengembalianDetailPermohonan($id) {
	global $app;
	
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	DetailPermohonan_View::printpengembalianDetailPermohonan($objDetailPermohonan);
}

function printpenolakanDetailPermohonan($id) {
	global $app;
	
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	DetailPermohonan_View::printpenolakanDetailPermohonan($objDetailPermohonan);
}

function readDetailPermohonan($id) {
	global $app;
	
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	DetailPermohonan_View::readDetailPermohonan($objDetailPermohonan);
}
?>