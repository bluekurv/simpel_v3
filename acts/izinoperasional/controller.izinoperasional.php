<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.izinoperasional.php';
require_once 'view.izinoperasional.php';

switch ($app->task) {
	case 'edit':
		editIzinOperasional($app->id);
		break;
	case 'print':
		printIzinOperasional($app->id);
		break;
	case 'read':
		readIzinOperasional($app->id);
		break;
	case 'save':
		saveIzinOperasional();
		break;
	default:
		viewIzinOperasional(true, '');
		break;
}

function debug($x) {
	echo '<pre>';
	print_r($x);
	echo '</pre>';
}

function debux($x) {
	debug($x);
	exit();
}

function editIzinOperasional($id) {
	global $app, $childAct;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objIzinOperasional = $app->queryObject("SELECT * FROM izinoperasional WHERE izinoperasionalTipe='".$childAct."' AND izinoperasionalDmhnID='".$id."'");
	if (!$objIzinOperasional) {
		$objIzinOperasional = new IzinOperasional_Model();

		$objIzinOperasional->izinoperasionalTipe = $childAct;
		$objIzinOperasional->izinoperasionalNamaPemohon = $objDetailPermohonan->mhnNama;
		$objIzinOperasional->izinoperasionalTglPemohon = $objDetailPermohonan->mhnTgl;
	}
	
	$objIzinOperasional->izinoperasionalDmhnID = $objDetailPermohonan->dmhnID;
	
	$view = new IzinOperasional_View();
	$view->editIzinOperasional(true, '', $objDetailPermohonan, $objIzinOperasional);
}

function printIzinOperasional($id) {
	global $app, $childAct, $childTitle;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
 			LEFT JOIN izinoperasional ON izinoperasionalDmhnID=dmhnID AND izinoperasionalTipe='".$childAct."'
			WHERE dmhnID='".$id."'";
	$objIzinOperasional = $app->queryObject($sql);
	if (!$objIzinOperasional) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objIzinOperasional->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objIzinOperasional->izinoperasionalKelurahanID."'");
	$objIzinOperasional->kelurahan = $objKelurahan->wilNama;
	
	$objKecamatan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");
	$objIzinOperasional->kecamatan = $objKecamatan->wilNama;
	
	$objIzinOperasional->kabupaten = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKecamatan->wilParentID."'");
	
	$view = new IzinOperasional_View();
	$view->printIzinOperasional($objIzinOperasional);
}

function readIzinOperasional($id) {
	global $app, $childAct, $childTitle;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN izinoperasional ON izinoperasionalDmhnID=dmhnID AND izinoperasionalTipe='".$childAct."'
			WHERE dmhnID='".$id."'";
	$objIzinOperasional = $app->queryObject($sql);
	if (!$objIzinOperasional) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objIzinOperasional->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	$view = new IzinOperasional_View();
	$view->readIzinOperasional($objIzinOperasional);
}

function saveIzinOperasional() {
	global $app, $childAct, $childTitle, $childKodeSurat;
	
	//Get object
	$objIzinOperasionalSebelumnya = $app->queryObject("SELECT * FROM izinoperasional WHERE izinoperasionalTipe='".$childAct."' AND izinoperasionalID='".$app->getInt('izinoperasionalID')."'");
	if (!$objIzinOperasionalSebelumnya) {
		$objIzinOperasionalSebelumnya = new IzinOperasional_Model();
		$objIzinOperasionalSebelumnya->izinoperasionalTipe = $childAct;
	}
	
	//Create object
	$objIzinOperasional = new IzinOperasional_Model();
	$app->bind($objIzinOperasional);
	
	//Modify object (if necessary)
	$objIzinOperasional->izinoperasionalTglPemohon = $app->NormalDateToMySQL($objIzinOperasional->izinoperasionalTglPemohon);
	$objIzinOperasional->izinoperasionalTglIzinGangguan = $app->NormalDateToMySQL($objIzinOperasional->izinoperasionalTglIzinGangguan);
	
	$objIzinOperasional->izinoperasionalTglPengesahan = $app->NormalDateToMySQL($objIzinOperasional->izinoperasionalTglPengesahan);
	if ($objIzinOperasional->izinoperasionalTglPengesahan != '0000-00-00') {
		if ($objIzinOperasionalSebelumnya->izinoperasionalNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objIzinOperasional->izinoperasionalNo = intval($app->queryField("SELECT MAX(izinoperasionalNo) AS nomor FROM izinoperasional WHERE izinoperasionalTipe='".$childAct."' AND YEAR(izinoperasionalTglPengesahan)='".substr($objIzinOperasional->izinoperasionalTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objIzinOperasional->izinoperasionalTglBerlaku = date('Y-m-d', strtotime($objIzinOperasional->izinoperasionalTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objIzinOperasional->izinoperasionalTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objIzinOperasional->izinoperasionalTglDaftarUlang = date('Y-m-d', strtotime($objIzinOperasional->izinoperasionalTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objIzinOperasional->izinoperasionalTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objIzinOperasional->izinoperasionalNo = 0;
		
		$objIzinOperasional->izinoperasionalTglBerlaku = '0000-00-00';
		$objIzinOperasional->izinoperasionalTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objIzinOperasional->izinoperasionalNoLengkap = '503/'.CFG_COMPANY_SHORT_NAME.'/'.$childKodeSurat.'/'.substr($objIzinOperasional->izinoperasionalTglPengesahan,0,4).'/'.$objIzinOperasional->izinoperasionalNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objIzinOperasional->izinoperasionalNo > 0){
		$isExist = $app->isExist("SELECT izinoperasionalID AS id FROM izinoperasional WHERE izinoperasionalTipe='".$childAct."' AND izinoperasionalNo='".$objIzinOperasional->izinoperasionalNo."' AND YEAR(izinoperasionalTglPengesahan)='".substr($objIzinOperasional->izinoperasionalTglPengesahan,0,4)."'", $objIzinOperasional->izinoperasionalID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objIzinOperasional->izinoperasionalNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objIzinOperasional->izinoperasionalID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objIzinOperasional);
		} else {
			$sql = $app->createSQLforUpdate($objIzinOperasional);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objIzinOperasional->izinoperasionalID = $app->queryField("SELECT LAST_INSERT_ID() FROM izinoperasional");
		} 
		
		$success = true;
		$msg = $childTitle.' berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, $childAct, $objIzinOperasional->izinoperasionalID, $objIzinOperasional->izinoperasionalNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objIzinOperasional->izinoperasionalDmhnID."'");
	} else {
		$success = false;
		$msg = $childTitle.' tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objIzinOperasional->izinoperasionalDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		$view = new IzinOperasional_View();
		$view->editIzinOperasional($success, $msg, $objDetailPermohonan, $objIzinOperasional);
	}
}

function viewIzinOperasional($success, $msg) {
	global $app, $childAct;
	
	//Get variable(s)
	$page 		= $app->pageVar($childAct, 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar($childAct, 'sort', 'izinoperasionalTglPengesahan');
	//$dir		= $app->pageVar($childAct, 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar($childAct, 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar($childAct, 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(izinoperasionalNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(izinoperasionalTglPengesahan)='".$tahun."'";
	}
	$filter[] = "izinoperasionalTipe='".$childAct."'";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM izinoperasional
				  LEFT JOIN detailpermohonan ON izinoperasionalDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM izinoperasional
			LEFT JOIN detailpermohonan ON izinoperasionalDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(izinoperasionalTglPengesahan) DESC, izinoperasionalNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	$view = new IzinOperasional_View();
	$view->viewIzinOperasional($success, $msg, $arr);
}
?>