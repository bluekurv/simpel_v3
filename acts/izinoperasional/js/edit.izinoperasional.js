//Variables
var form = $("#myForm");
var izinoperasionalKelurahan = $("#izinoperasionalKelurahan");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'izinoperasionalKelurahan' || this.id === undefined) {
		if (izinoperasionalKelurahan.val().length == 0) {
			doSubmit = false;
			izinoperasionalKelurahan.addClass("error");		
		} else {
			izinoperasionalKelurahan.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

form.submit(submitForm);

$('#izinoperasionalKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#izinoperasionalKelurahanID').val(suggestion.data.id);
		$('#izinoperasionalKelurahanKode').val(suggestion.data.kode);
		$('#izinoperasionalKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

//Set focus
$("#izinoperasionalNama").focus();