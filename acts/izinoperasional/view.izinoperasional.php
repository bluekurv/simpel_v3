<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class IzinOperasional_View {
	public function editIzinOperasional($success, $msg, $objDetailPermohonan, $obj) {
		global $app, $childAct, $childTitle, $childKodeSurat;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=<?php echo $childAct; ?>&task=edit&id=<?php echo $obj->izinoperasionalDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri <?php echo $childTitle; ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=<?php echo $childAct; ?>&task=save" method="POST" >
				<input type="hidden" id="izinoperasionalID" name="izinoperasionalID" value="<?php echo $obj->izinoperasionalID; ?>" />
				<input type="hidden" id="izinoperasionalDmhnID" name="izinoperasionalDmhnID" value="<?php echo $obj->izinoperasionalDmhnID; ?>" />
				<input type="hidden" id="izinoperasionalTipe" name="izinoperasionalTipe" value="<?php echo $obj->izinoperasionalTipe; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->izinoperasionalImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=<?php echo $childAct; ?>&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Membaca</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Surat Permohonan dari</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalNamaPemohon" name="izinoperasionalNamaPemohon" maxlength="500" size="50" value="<?php echo $obj->izinoperasionalNamaPemohon; ?>" />
						Tgl.
						<input class="box date2" id="izinoperasionalTglPemohon" name="izinoperasionalTglPemohon" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->izinoperasionalTglPemohon); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Surat Keputusan nomor</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalNoIzinGangguan" name="izinoperasionalNoIzinGangguan" maxlength="500" size="50" value="<?php echo $obj->izinoperasionalNoIzinGangguan; ?>" />
						Tgl.
						<input class="box date2" id="izinoperasionalTglIzinGangguan" name="izinoperasionalTglIzinGangguan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->izinoperasionalTglIzinGangguan); ?>">
						tentang Izin Gangguan
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Izin diberikan kepada</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalNama" name="izinoperasionalNama" maxlength="500" size="50" value="<?php echo $obj->izinoperasionalNama; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalNoKTP" name="izinoperasionalNoKTP" maxlength="500" size="50" value="<?php echo $obj->izinoperasionalNoKTP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalJenisUsaha" name="izinoperasionalJenisUsaha" maxlength="255" size="50" value="<?php echo $obj->izinoperasionalJenisUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Merek/Nama Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalMerekNamaUsaha" name="izinoperasionalMerekNamaUsaha" maxlength="255" size="50" value="<?php echo $obj->izinoperasionalMerekNamaUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalNPWPD" name="izinoperasionalNPWPD" maxlength="255" size="50" value="<?php echo $obj->izinoperasionalNPWPD; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalTempatUsaha" name="izinoperasionalTempatUsaha" maxlength="255" size="50" value="<?php echo $obj->izinoperasionalTempatUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->izinoperasionalKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$izinoperasionalKelurahan = $app->queryObject($sql);
		if (!$izinoperasionalKelurahan) {
			$izinoperasionalKelurahan = new stdClass();
			$izinoperasionalKelurahan->kode = '';
			$izinoperasionalKelurahan->value = '';
		}
?>
						<input type="hidden" id="izinoperasionalKelurahanID" name="izinoperasionalKelurahanID" value="<?php echo $obj->izinoperasionalKelurahanID; ?>" />
						<input class="box readonly" id="izinoperasionalKelurahanKode" name="izinoperasionalKelurahanKode" size="8" value="<?php echo $izinoperasionalKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="izinoperasionalKelurahan" name="izinoperasionalKelurahan" maxlength="500" size="70" value="<?php echo $izinoperasionalKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izinoperasionalKeterangan" name="izinoperasionalKeterangan" maxlength="255" size="50" value="<?php echo $obj->izinoperasionalKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->izinoperasionalNo > 0) {
?>
						<span>503/<?php echo CFG_COMPANY_SHORT_NAME; ?>/<?php echo $childKodeSurat; ?>/<?php echo substr($obj->izinoperasionalTglPengesahan,0,4); ?>/</span>
						<input class="box" id="izinoperasionalNo" name="izinoperasionalNo" maxlength="5" size="5" value="<?php echo $obj->izinoperasionalNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="izinoperasionalNo" name="izinoperasionalNo" value="<?php echo $obj->izinoperasionalNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="izinoperasionalTglPengesahan" name="izinoperasionalTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->izinoperasionalTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="izinoperasionalPejID" name="izinoperasionalPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->izinoperasionalPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='".$childAct."'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->izinoperasionalTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->izinoperasionalTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="izinoperasionalTglBerlaku" name="izinoperasionalTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->izinoperasionalTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->izinoperasionalTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->izinoperasionalTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="izinoperasionalTglDaftarUlang" name="izinoperasionalTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->izinoperasionalTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/izinoperasional/js/edit.izinoperasional.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printIzinOperasional($obj) {
		global $app, $childAct, $childTitle, $childKodeSurat, $childTembusan, $childTembusan2;
		
		$objReport 		= new Report($childTitle, $childKodeSurat.'-'.substr($obj->izinoperasionalTglPengesahan,0,4).'-'.$obj->izinoperasionalNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->izinoperasionalPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<h4 align="center">
		KEPUTUSAN KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?><br>
		NOMOR : <?php echo $obj->izinoperasionalNoLengkap; ?><br>
		TENTANG<br>
		<?php echo strtoupper($childTitle); ?><br>
		<br>
		KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?>,
	</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table>
				<tr><td width="4%">1.</td><td width="96%">Surat Permohonan dari sdr <?php echo strtoupper($obj->izinoperasionalNamaPemohon); ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->izinoperasionalTglPemohon); ?> tentang untuk mendapatkan Izin Operasional.</td></tr>
				<tr><td>2.</td><td>Surat Keputusan Kepala Badan Penanaman Modal dan Pelayanan Perijinan Terpadu Nomor :  <?php echo $obj->izinoperasionalNoIzinGangguan; ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->izinoperasionalTglIzinGangguan); ?> tentang Ijin Gangguan.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">
			<table>
				<tr><td width="4%">a.</td><td width="96%">Bahwa pengendalian, penertiban dan pengawasan bidang usaha perlu diatur oleh Pemerintah Daerah.</td></tr>
				<tr><td>b.</td><td>Bahwa untuk melaksanakan usaha yang bersangkutan, maka perlu <?php echo $childTitle; ?> dari Kepala Badan Penanaman Modal Dan Pelayanan Perizinan Terpadu.</td></tr>
				<tr><td>c.</td><td>Bahwa berdasarkan pertimbangan sebagaimana dimaksud huruf a, huruf b perlu menetapkan Izin Operasional bidang usaha.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
<?php 
		echo $konfigurasi['consideringIzinOperasional'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">PERTAMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Memberikan <?php echo $childTitle; ?> kepada :</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">
			<table width="100px" height="100px" border="0">
			<tr>
				<td>
					<img src="<?php echo CFG_LIVE_PATH; ?>/images/report/3x4.png" width="93" height="122">
				</td>
			</tr>
			</table>
		</td>
		<td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0">
			<tr>
				<td align="left" valign="top" width="38%">Nama</td>
				<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="60%"><?php echo strtoupper($obj->izinoperasionalNama); ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">No. KTP</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->izinoperasionalNoKTP; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Jenis Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->izinoperasionalJenisUsaha; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Merek/Nama Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->izinoperasionalMerekNamaUsaha; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Nomor NPWPD</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->izinoperasionalNPWPD; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Tempat Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->izinoperasionalTempatUsaha; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Kelurahan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->kelurahan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Kecamatan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->kecamatan; ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEDUA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			Pemegang Izin ini harus memenuhi ketentuan sebagai berikut :
		</td>
	</tr>
	<tr>
		<td width="20%"></td><td width="7"></td>
		<td width="75%">
			<table>
				<tr><td width="4%">1.</td><td width="96%">Memasang papan merek sebagaimana tersebut pada dictum pertama.</td></tr>
				<tr><td>2.</td><td>Memelihara ketertiban / keamanan dan kebersihan lingkungan.</td></tr>
				<tr><td>3.</td><td>Memenuhi dan menaati Peraturan Perundang-Undangan yang berlaku.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KETIGA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify"><?php echo $childTitle; ?> ini berlaku selama <?php echo $obj->jsiMasaBerlaku; ?> Tahun.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEEMPAT</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Surat izin ini dicabut kembali, apabila pengusaha/pemegang izin tidak mentaati ketentuan-ketentuan tersebut diatas dan melanggar peraturan perundang-undangan yang berlaku.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KELIMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Keputusan ini berlaku sejak tanggal ditetapkan.</td>
	</tr>
	</table>

	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="44%">&nbsp;</td>
		<td width="56%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->izinoperasionalTglPengesahan)); ?>
		</td>
	</tr>
	</table>

	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:8px">
	<tr>
		<td width="50">Tembusan :</td>
		<td>Disampaikan Kepada Yth. :</td>
	</tr>
	<tr>
		<td colspan="2">1. <?php echo $childTembusan; ?></td>
	</tr>
<?php 
		$no = 2;
		
		if (isset($childTembusan2)) {
			if ($childTembusan2 != '') {
?>
	<tr>
		<td colspan="2"><?php echo $no; ?>. <?php echo $childTembusan2; ?></td>
	</tr>
<?php
				$no++;
			}
		}
?>
	<tr>
		<td colspan="2"><?php echo $no; ?>. Camat <?php echo $obj->kecamatan; ?></td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readIzinOperasional($obj) {
		global $app, $childAct, $childTitle;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=<?php echo $childAct; ?>&task=read&id=<?php echo $obj->izinoperasionalDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat <?php echo $childTitle; ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoUrutLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Izin diberikan kepada</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->izinoperasionalNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->izinoperasionalNoKTP; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->izinoperasionalJenisUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Merek/Nama Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->izinoperasionalMerekNamaUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->izinoperasionalNPWPD; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->izinoperasionalTempatUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->izinoperasionalKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->izinoperasionalKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->izinoperasionalNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->izinoperasionalTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->izinoperasionalPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->izinoperasionalTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->izinoperasionalTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->izinoperasionalTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->izinoperasionalTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/izinoperasional/js/read.izinoperasional.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewIzinOperasional($success, $msg, $arr) {
		global $app, $childAct, $childTitle;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=<?php echo $childAct; ?>"><img src="images/icons/rosette.png" border="0" /> Buku Register <?php echo $childTitle; ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('izinoperasionalNo', 'Nomor Surat', true),
			$app->setHeader('izinoperasionalTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('izinoperasionalTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('izinoperasionalTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act='.$childAct.'&task=read&id='.$v->izinoperasionalDmhnID.'&fromact='.$childAct.'" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->izinoperasionalDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act='.$childAct.'&task=read&id='.$v->izinoperasionalDmhnID.'&fromact='.$childAct.'" title="Entri">'.$v->izinoperasionalNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->izinoperasionalTglPengesahan).'</td>';
				
				if ($v->izinoperasionalTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->izinoperasionalTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->izinoperasionalTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->izinoperasionalTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/izinoperasional/js/view.izinoperasional.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>