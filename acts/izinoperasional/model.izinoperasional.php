<?php
class IzinOperasional_Model {	
	public $tabel = 'izinoperasional';
	public $primaryKey = 'izinoperasionalID';
	
	public $izinoperasionalID = 0;
	public $izinoperasionalDmhnID = 0;
	public $izinoperasionalTipe = '';
	public $izinoperasionalNo = 0;
	public $izinoperasionalNoLengkap = '';
	public $izinoperasionalNamaPemohon = '';
	public $izinoperasionalTglPemohon = '0000-00-00';
	public $izinoperasionalNoIzinGangguan = '';
	public $izinoperasionalTglIzinGangguan = '0000-00-00';
	public $izinoperasionalNama = '';
	public $izinoperasionalNoKTP = '';
	public $izinoperasionalJenisUsaha = '';
	public $izinoperasionalMerekNamaUsaha= '';
	public $izinoperasionalNPWPD = '';
	public $izinoperasionalTempatUsaha = '';
	public $izinoperasionalKelurahanID = 0;
	public $izinoperasionalKeterangan = '';
	public $izinoperasionalTglBerlaku = '0000-00-00'; 
	public $izinoperasionalTglDaftarUlang = '0000-00-00';
	public $izinoperasionalPejID = 0;
	public $izinoperasionalTglPengesahan = '0000-00-00'; 	 	 	 	 	
	public $izinoperasionalArsip = '';	 	 	 	 	
	public $izinoperasionalImpor = 0;
}
?>