<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kblikategori.php';
require_once 'view.kblikategori.php';
require_once CFG_ABSOLUTE_PATH.'/acts/kbli/view.kbli.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKBLIKategori($app->id);
		break;
	case 'delete':
		deleteKBLIKategori($app->id);
		break;
	case 'save':
		saveKBLIKategori();
		break;
	default:
		viewKBLIKategori(true, '');
		break;
}

function deleteKBLIKategori($id) {
	global $app;
	
	//Get object
	$objKBLIKategori = $app->queryObject("SELECT * FROM kblikategori WHERE kkatID='".$id."'");
	if (!$objKBLIKategori) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalKBLIGolonganPokok = intval($app->queryField("SELECT COUNT(*) AS total FROM kbligolonganpokok WHERE kgpokKkatID='".$id."'"));
	if ($totalKBLIGolonganPokok > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLIGolonganPokok.' golongan pokok untuk kategori tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM kblikategori WHERE kkatID='".$id."'");
		
		$success = true;
		$msg = 'Kategori Lapangan Usaha "'.$objKBLIKategori->kkatNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'kblikategori', $objKBLIKategori->kkatID, $objKBLIKategori->kkatNama, $msg);
	} else {
		$success = false;
		$msg = 'Kategori Lapangan Usaha "'.$objKBLIKategori->kkatNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKBLIKategori($success, $msg);
}

function editKBLIKategori($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKBLIKategori = $app->queryObject("SELECT * FROM kblikategori WHERE kkatID='".$id."'");
		if (!$objKBLIKategori) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKBLIKategori = new KBLIKategori_Model();
	}
	
	KBLIKategori_View::editKBLIKategori(true, '', $objKBLIKategori);
}

function saveKBLIKategori() {
	global $app;
	
	//Create object
	$objKBLIKategori = new KBLIKategori_Model();
	$app->bind($objKBLIKategori);
	
	//Modify object (if necessary)
	if (isset($_REQUEST['kkatTampil'])) {
		$objKBLIKategori->kkatTampil = ($_REQUEST['kkatTampil'] == 'on') ? 1 : 0;
	} else {
		$objKBLIKategori->kkatTampil = 0;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKBLIKategori->kkatKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKBLIKategori->kkatNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objKBLIKategori->kkatKpubID == 0) {
		$doSave = false;
		$msg[] = '- Publikasi belum diisi';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKBLIKategori->kkatID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKBLIKategori);
		} else {
			$sql = $app->createSQLforUpdate($objKBLIKategori);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKBLIKategori->kkatID = $app->queryField("SELECT LAST_INSERT_ID() FROM kblikategori");
			
			$success = true;
			$msg = 'Kategori Lapangan Usaha "'.$objKBLIKategori->kkatNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'kblikategori', $objKBLIKategori->kkatID, $objKBLIKategori->kkatNama, $msg);
		} else {
			$success = true;
			$msg = 'Kategori Lapangan Usaha "'.$objKBLIKategori->kkatNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'kblikategori', $objKBLIKategori->kkatID, $objKBLIKategori->kkatNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Kategori Lapangan Usaha "'.$objKBLIKategori->kkatNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKBLIKategori($success, $msg);
	} else {
		KBLIKategori_View::editKBLIKategori($success, $msg, $objKBLIKategori);
	}
}

function viewKBLIKategori($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kblikategori', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kblikategori', 'sort', 'kkatKode');
	$dir   		= $app->pageVar('kblikategori', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'parent' => 0
	);
	
	$nama 		= $app->pageVar('kblikategori', 'filternama', $default['nama'], 'strval');
	$parent		= $app->pageVar('kblikategori', 'filterparent', $default['parent'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(kkatKode LIKE '%".$nama."%' OR kkatNama LIKE '%".$nama."%')";
	}
	if ($parent != $default['parent']) {
		$filter[] = "kkatKpubID='".$parent."'";
	}
	$filter[] = "kkatKpubID=kpubID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'parent' => $parent
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM kblikategori, kblipublikasi
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM kblikategori, kblipublikasi
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	KBLIKategori_View::viewKBLIKategori($success, $msg, $arr);
}
?>