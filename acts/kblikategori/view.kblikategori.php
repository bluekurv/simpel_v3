<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class KBLIKategori_View {
	static function editKBLIKategori($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kblikategori&task=edit&id=<?php echo $obj->kkatID; ?>"><img src="images/icons/database_table.png" border="0" /> <?php echo ($obj->kkatID > 0) ? 'Ubah' : 'Tambah'; ?> Kategori Lapangan Usaha</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=kblikategori&task=save" method="POST" >
				<input type="hidden" id="kkatID" name="kkatID" value="<?php echo $obj->kkatID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=kblikategori">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Kode <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="kkatKode" name="kkatKode" maxlength="10" size="10" value="<?php echo $obj->kkatKode; ?>" />
					</td>
				</tr>
				<tr>
					<td valign="top">Nama <span class="wajib">*</span></td><td valign="top">&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="kkatNama" name="kkatNama" cols="80" rows="5"><?php echo $obj->kkatNama; ?></textarea>
					</td>
				</tr>
				<tr>
					<td>Publikasi</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelect('kkatKpubID', "SELECT kpubID AS id, kpubNama AS nama FROM kblipublikasi ORDER BY kpubNama", $obj->kkatKpubID);
?>
					</td>
				</tr>
				<tr>
					<td>Tampil?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('kkatTampil', 1, 'Ya', 0, 'Tidak', $obj->kkatTampil);
?>
					</td>
				</tr>
				<tr>
					<td valign="top">Keterangan</td><td valign="top">&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="kkatKeterangan" name="kkatKeterangan" cols="80" rows="5"><?php echo $obj->kkatKeterangan; ?></textarea>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kblikategori/js/edit.kblikategori.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewKBLIKategori($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kblikategori"><img src="images/icons/database_table.png" border="0" /> Kategori Lapangan Usaha</a></h1>
		<?php KBLI_View::menuKBLI('kblikategori'); ?>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=kblikategori&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Kode/Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
<?php 
		$app->createSelect('filterparent', "SELECT kpubID AS id, kpubNama AS nama FROM kblipublikasi ORDER BY kpubNama", $arr['filter']['parent'], array(0=>'--Seluruhnya--'));
?>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['parent'] != $arr['default']['parent']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('kkatKode', 'Kode', true, 40),
			$app->setHeader('kkatNama', 'Nama', true),
			$app->setHeader('kpubNama', 'Publikasi', true),
			$app->setHeader('kkatTampil', 'Tampil', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=kblikategori&task=edit&id='.$v->kkatID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->kkatID.', '."'".$v->kkatNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=kblikategori&task=edit&id='.$v->kkatID.'">'.$v->kkatKode.'</a></td>';
				echo '<td>'.$v->kkatNama.'</td>';
				echo '<td>'.$v->kpubNama.'</td>';
				echo '<td>'.($v->kkatTampil == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="5">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kblikategori/js/kblikategori.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>