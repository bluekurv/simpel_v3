//Variables
var form = $("#myForm");
var kkatKode = $("#kkatKode");
var kkatNama = $("#kkatNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'kkatKode' || this.id === undefined) {
		if (kkatKode.val().length == 0){
			doSubmit = false;
			kkatKode.addClass("error");		
		} else {
			kkatKode.removeClass("error");
		}
	}
	
	if (this.id == 'kkatNama' || this.id === undefined) {
		if (kkatNama.val().length == 0){
			doSubmit = false;
			kkatNama.addClass("error");		
		} else {
			kkatNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
kkatKode.blur(validateForm);
kkatNama.blur(validateForm);

kkatKode.keyup(validateForm);
kkatNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
kkatKode.focus();