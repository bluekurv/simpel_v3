<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'objekwisata';
$childTitle = 'Izin Operasional Objek Wisata';
$childKodeSurat = 'IOP-OW';
$childTembusan = 'Kepala Dinas Kebudayaan, Pariwisata, Pemuda dan Olahraga Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>