<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'rekreasidanhiburnaumum';
$childTitle = 'Izin Operasional Rekreasi dan Hiburan Umum';
$childKodeSurat = 'IOP-HU';
$childTembusan = 'Kepala Dinas Kebudayaan, Pariwisata, Pemuda dan Olahraga Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>