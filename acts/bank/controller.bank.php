<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.bank.php';
require_once 'view.bank.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editBank($app->id);
		break;
	case 'delete':
		deleteBank($app->id);
		break;
	case 'save':
		saveBank();
		break;
	default:
		viewBank(true, '');
		break;
}

function deleteBank($id) {
	global $app;
	
	//Get object
	$objBank = $app->queryObject("SELECT * FROM bank WHERE bnkID='".$id."'");
	if (!$objBank) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM bank WHERE bnkID='".$id."'");
		
		$success = true;
		$msg = 'Bank "'.$objBank->bnkNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'bank', $objBank->bnkID, $objBank->bnkNama, $msg);
	} else {
		$success = false;
		$msg = 'Bank "'.$objBank->bnkNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewBank($success, $msg);
}

function editBank($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objBank = $app->queryObject("SELECT * FROM bank WHERE bnkID='".$id."'");
		if (!$objBank) {
			$app->showPageError();
			exit();
		}
	} else {
		$objBank = new Bank_Model();
	}
	
	Bank_View::editBank(true, '', $objBank);
}

function saveBank() {
	global $app;
	
	//Create object
	$objBank = new Bank_Model();
	$app->bind($objBank);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objBank->bnkKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objBank->bnkNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT bnkID AS id FROM bank WHERE bnkKode='".$objBank->bnkKode."'", $objBank->bnkID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objBank->bnkKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT bnkID AS id FROM bank WHERE bnkNama='".$objBank->bnkNama."'", $objBank->bnkID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objBank->bnkNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objBank->bnkID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objBank);
		} else {
			$sql = $app->createSQLforUpdate($objBank);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objBank->bnkID = $app->queryField("SELECT LAST_INSERT_ID() FROM bank");
			
			$success = true;
			$msg = 'Bank "'.$objBank->bnkNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'bank', $objBank->bnkID, $objBank->bnkNama, $msg);
		} else {
			$success = true;
			$msg = 'Bank "'.$objBank->bnkNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'bank', $objBank->bnkID, $objBank->bnkNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Bank "'.$objBank->bnkNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewBank($success, $msg);
	} else {
		Bank_View::editBank($success, $msg, $objBank);
	}
}

function viewBank($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('bank', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('bank', 'sort', 'bnkKode');
	$dir		= $app->pageVar('bank', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('bank', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(bnkKode LIKE '%".$nama."%' OR bnkNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM bank
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM bank
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Bank_View::viewBank($success, $msg, $arr);
}
?>