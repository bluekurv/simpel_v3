//Variables
var form = $("#myForm");
var bnkKode = $("#bnkKode");
var bnkNama = $("#bnkNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'bnkKode' || this.id === undefined) {
		if (bnkKode.val().length == 0){
			doSubmit = false;
			bnkKode.addClass("error");		
		} else {
			bnkKode.removeClass("error");
		}
	}
	
	if (this.id == 'bnkNama' || this.id === undefined) {
		if (bnkNama.val().length == 0){
			doSubmit = false;
			bnkNama.addClass("error");		
		} else {
			bnkNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
bnkKode.blur(validateForm);
bnkNama.blur(validateForm);

bnkKode.keyup(validateForm);
bnkNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
bnkKode.focus();