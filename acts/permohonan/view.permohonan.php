<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Permohonan_View {
	static function editPermohonan($success, $msg, $obj, $tipe) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=permohonan<?php echo ($obj->mhnID > 0) ? '&task=edit&id='.$obj->mhnID : '&task=add'; ?>"><img src="images/icons/rosette.png" border="0" /> <?php echo ($obj->mhnID > 0) ? 'Ubah Permohonan' : 'Permohonan Baru'; ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=permohonan&task=save" method="POST" >
				<input type="hidden" id="mhnID" name="mhnID" value="<?php echo $obj->mhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="editTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="mhnTgl" name="mhnTgl" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->mhnTgl); ?>">
					</td>
				</tr>
				<tr>
					<td>No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="mhnNoUrut" name="mhnNoUrut" value="<?php echo $obj->mhnNoUrut; ?>">
<?php 
		if ($obj->mhnID > 0) {
?>
						<input class="box" id="mhnNoUrutLengkap" name="mhnNoUrutLengkap" maxlength="11" size="11" value="<?php echo $obj->mhnNoUrutLengkap; ?>">
<?php		
		}  else {
			echo 'Terakhir';
		}
?>
					</td>
				</tr>
				<tr>
					<td>Loket</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="mhnLokID" name="mhnLokID" value="<?php echo $obj->mhnLokID; ?>">
						<?php echo $app->queryField("SELECT lokNama FROM loket WHERE lokID='".$obj->mhnLokID."'"); ?>
					</td>
				</tr>
				<tr>
					<td>Petugas</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="mhnDibuatOleh" name="mhnDibuatOleh" value="<?php echo $obj->mhnDibuatOleh; ?>">
						<?php echo ($obj->mhnID > 0 ? $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->mhnDibuatOleh."'") : $_SESSION['sesipengguna']->nama); ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Pemohon</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnNoKtp" name="mhnNoKtp" maxlength="50" size="50" value="<?php echo $obj->mhnNoKtp; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Jika No. KTP yang dimasukkan sudah ada, detail pemohon akan ditampilkan berdasarkan No. KTP tersebut</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnNama" name="mhnNama" maxlength="50" size="50" value="<?php echo $obj->mhnNama; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnAlamat" name="mhnAlamat" maxlength="500" size="100" value="<?php echo $obj->mhnAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->mhnKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$mhnKelurahan = $app->queryObject($sql);
		if (!$mhnKelurahan) {
			$mhnKelurahan = new stdClass();
			$mhnKelurahan->kode = '';
			$mhnKelurahan->value = '';
		}
?>
						<input type="hidden" id="mhnKelurahanID" name="mhnKelurahanID" value="<?php echo $obj->mhnKelurahanID; ?>" />
						<input class="box readonly" id="mhnKelurahanKode" name="mhnKelurahanKode" size="8" value="<?php echo $mhnKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="mhnKelurahan" name="mhnKelurahan" maxlength="500" size="70" value="<?php echo $mhnKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, informasikan kepada Administrator untuk menambahkannya</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tempat Lahir <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnTempatLahir" name="mhnTempatLahir" maxlength="255" size="50" value="<?php echo $obj->mhnTempatLahir; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Lahir <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date" id="mhnTglLahir" name="mhnTglLahir" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->mhnTglLahir); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kewarganegaraan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnKewarganegaraan" name="mhnKewarganegaraan" maxlength="255" size="50" value="<?php echo $obj->mhnKewarganegaraan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pekerjaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnPekerjaan" name="mhnPekerjaan" maxlength="255" size="50" value="<?php echo $obj->mhnPekerjaan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnNoTelp" name="mhnNoTelp" maxlength="255" size="50" value="<?php echo $obj->mhnNoTelp; ?>" />
					</td>
				</tr>
<?php 
		if ($tipe != 'Perseorangan') {
?>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Perusahaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $obj->perID; ?>">
						<input class="box" id="perNama" name="perNama" maxlength="500" size="50" value="<?php echo $obj->perNama; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama perusahaan untuk menampilkan pilihan yang ada, kemudian pilih perusahaan yang diinginkan atau masukkan nama perusahaan baru</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Perusahaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnPerusahaanAlamat" name="mhnPerusahaanAlamat" maxlength="500" size="100" value="<?php echo ($obj->mhnPerusahaanKelurahanID > 0) ? $obj->mhnPerusahaanAlamat : $obj->perAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->mhnPerusahaanKelurahanID > 0) {
			$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
					FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
					WHERE w.wilID='".$obj->mhnPerusahaanKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		} else {
			$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
					FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
					WHERE w.wilID='".$obj->perKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		}
		$perKelurahan = $app->queryObject($sql);
		if (!$perKelurahan) {
			$perKelurahan = new stdClass();
			$perKelurahan->kode = '';
			$perKelurahan->value = '';
		}
		if ($obj->mhnPerusahaanKelurahanID > 0) {
			$perKelurahan->id = $obj->mhnPerusahaanKelurahanID;
		} else {
			$perKelurahan->id = $obj->perKelurahanID;
		}
?>
						<input type="hidden" id="mhnPerusahaanKelurahanID" name="mhnPerusahaanKelurahanID" value="<?php echo $perKelurahan->id; ?>" />
						<input class="box readonly" id="mhnPerusahaanKelurahanKode" name="mhnPerusahaanKelurahanKode" size="8" value="<?php echo $perKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="mhnPerusahaanKelurahan" name="mhnPerusahaanKelurahan" maxlength="500" size="70" value="<?php echo $perKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, pergunakan sub menu yang ada di menu <b>Pelayanan &gt; Lokasi</b> untuk mengelola data lokasi</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Lokasi Usaha (<span style="color:#FF0000;">Jika Berbeda dengan Alamat Perusahaan</span>)</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="mhnLokasiUsahaAlamat" name="mhnLokasiUsahaAlamat" maxlength="500" size="100" value="<?php echo $obj->mhnLokasiUsahaAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
				WHERE w.wilID='".$obj->mhnLokasiUsahaKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$perKelurahan = $app->queryObject($sql);
		if (!$perKelurahan) {
			$perKelurahan = new stdClass();
			$perKelurahan->kode = '';
			$perKelurahan->value = '';
		}
?>
						<input type="hidden" id="mhnLokasiUsahaKelurahanID" name="mhnLokasiUsahaKelurahanID" value="<?php echo $obj->mhnLokasiUsahaKelurahanID; //perKelurahanID; ?>" />
						<input class="box readonly" id="mhnLokasiUsahaKelurahanKode" name="mhnLokasiUsahaKelurahanKode" size="8" value="<?php echo $perKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="mhnLokasiUsahaKelurahan" name="mhnLokasiUsahaKelurahan" maxlength="500" size="70" value="<?php echo $perKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, pergunakan sub menu yang ada di menu <b>Pelayanan &gt; Lokasi</b> untuk mengelola data lokasi</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNoTelp" name="perNoTelp" maxlength="255" size="50" value="<?php echo $obj->perNoTelp; ?>" />
					</td>
				</tr>
<?php 
		}
?>
				<tr>
					<td colspan="3">
						<p><b><u>Untuk Pengurusan Surat Izin</u></b></p>
						<p><i>Petunjuk: persyaratan dengan <span class="wajib">warna merah</span> adalah persyaratan minimal. Jika perkiraan selesai belum diketahui, kosongkan.</i></p>
<?php 
		$arrJenisSuratIzin = array();
		if ($tipe == '') {
			$arrJenisSuratIzin['Perusahaan'] = array();
			$arrJenisSuratIzin['Perseorangan'] = array();
			
			$sql = "SELECT *
				FROM syaratsuratizin, jenissuratizin
				WHERE ssiJsiID=jsiID AND jsiTampil=1
				ORDER BY jsiTipe, jsiNama, jsiSubNama, ssiNo";
		} else {
			$arrJenisSuratIzin[$tipe] = array();
			
			$sql = "SELECT *
				FROM syaratsuratizin, jenissuratizin
				WHERE ssiJsiID=jsiID AND jsiTampil=1 AND jsiTipe='".$tipe."'
				ORDER BY jsiTipe, jsiNama, jsiSubNama, ssiNo";
		}
		
		$arrSyaratSuratIzin = array();
		$arrAdministrasiOleh = array();
		
		$rsSyaratSuratIzin = $app->query($sql);
		while(($objSyaratSuratIzin = mysql_fetch_object($rsSyaratSuratIzin)) == true){
			$arrJenisSuratIzin[$objSyaratSuratIzin->jsiTipe][$objSyaratSuratIzin->jsiID] = $objSyaratSuratIzin;
			
			if (!isset($arrSyaratSuratIzin[$objSyaratSuratIzin->jsiTipe][$objSyaratSuratIzin->jsiID])) {
				$arrSyaratSuratIzin[$objSyaratSuratIzin->jsiTipe][$objSyaratSuratIzin->jsiID]['Umum'] = array();
				$arrSyaratSuratIzin[$objSyaratSuratIzin->jsiTipe][$objSyaratSuratIzin->jsiID]['Khusus'] = array();
			}
			
			$arrSyaratSuratIzin[$objSyaratSuratIzin->jsiTipe][$objSyaratSuratIzin->jsiID][$objSyaratSuratIzin->ssiUmumKhusus][] = $objSyaratSuratIzin;
		}
		
		$sql = "SELECT pnID AS id, CONCAT(pnNama,IF(pnAksesAdministrasi='','',' - '),pnAksesAdministrasi) AS nama, (SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDiadministrasikanOleh=pnID AND dmhnTglSelesai=0) AS total_detailpermohonan 
				FROM pengguna 
				WHERE pnLevelAkses='Petugas Administrasi' 
				ORDER BY pnNama";
		$rsAdministrasiOleh = $app->query($sql);
		
		while(($objAdministrasiOleh = mysql_fetch_object($rsAdministrasiOleh)) == true){
			$arrAdministrasiOleh[$objAdministrasiOleh->id] = $objAdministrasiOleh;
		}
?>
					</td>
				</tr>
<?php 
		foreach (array_keys($arrJenisSuratIzin) as $jsiTipe) {
?>
				<tr>
					<td valign="top" style="padding-left:20px;"><?php echo $jsiTipe; ?></td><td valign="top">&nbsp;:&nbsp;</td>
					<td valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
<?php
			$totalJenisSuratIzin = 0;
			
			if (count($arrJenisSuratIzin[$jsiTipe]) > 0) {
				foreach ($arrJenisSuratIzin[$jsiTipe] as $jsiID=>$objJenisSuratIzin) {
					$display = isset($obj->jenissuratizin[$jsiID]) ? '' : 'display:none;';
					
					if (!isset($obj->pola[$objJenisSuratIzin->jsiID])) {
						$obj->pola[$objJenisSuratIzin->jsiID] = 0;
					}
					
					if (!isset($obj->perkiraanselesai[$objJenisSuratIzin->jsiID])) {
						$obj->perkiraanselesai[$objJenisSuratIzin->jsiID] = '0000-00-00';
					}
?>
						<tr id="trJenis[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>]" class="trJenis <?php echo (isset($obj->jenissuratizin[$jsiID])) ? 'selected' : ''; ?>">
							<td colspan="2">
								<select class="box selectJenis" id="jenissuratizin[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>]" name="jenissuratizin[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>]">
<?php
					foreach (array('', 'Baru', 'Perubahan', 'Perpanjangan') as $v) {
						echo '<option';
						if (isset($obj->jenissuratizin[$objJenisSuratIzin->jsiID])) {
							if ($v == $obj->jenissuratizin[$objJenisSuratIzin->jsiID]) {
								echo ' selected';
							}
						}
						echo '>'.$v."</option>\n";
					}
?>
								</select>
								<?php echo $objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : ''); ?>
							</td>
						</tr>
						<tr class="trSyarat[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>]" style="<?php echo $display; ?> background-color:#E5E5E5;">
							<td colspan="2" style="padding-left:5px;">
								Prosedur Pelayanan
								<select class="box selectPola" id="pola[<?php echo $objJenisSuratIzin->jsiID; ?>]" name="pola[<?php echo $objJenisSuratIzin->jsiID; ?>]">
									<option value="0">Belum Ditentukan</option>
<?php 
					if ($objJenisSuratIzin->jsiLamaPengurusan1 > 0) {
?>
									<option value="1" <?php echo ($obj->pola[$objJenisSuratIzin->jsiID] == 1) ? 'selected' : ''; ?>>Pola 1 (<?php echo $objJenisSuratIzin->jsiLamaPengurusan1; ?> hari kerja)</option>
<?php 
					}
					if ($objJenisSuratIzin->jsiLamaPengurusan2 > 0) {
?>
									<option value="2" <?php echo ($obj->pola[$objJenisSuratIzin->jsiID] == 2) ? 'selected' : ''; ?>>Pola 2 (<?php echo $objJenisSuratIzin->jsiLamaPengurusan2; ?> hari kerja)</option>
<?php 
					}
?>
								</select>
<?php 
					if ($obj->pola[$objJenisSuratIzin->jsiID] == 0) {
						$teksperkiraanselesaiValue = '';
						$teksperkiraanselesaiStyle = 'display:none;';
					} else {
						if ($obj->perlusurvey[$objJenisSuratIzin->jsiID]) {
							$teksperkiraanselesaiValue = 'dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Perkiraan selesai ';
						} else {
							$teksperkiraanselesaiValue = 'dengan perkiraan selesai';
						} 
						$teksperkiraanselesaiStyle = '';
					}
?>
								<span id="teksperkiraanselesai[<?php echo $objJenisSuratIzin->jsiID; ?>]" style="<?php echo $teksperkiraanselesaiStyle; ?>"><?php echo $teksperkiraanselesaiValue; ?></span>
<?php 
					$perlusurvey1Value = $objJenisSuratIzin->jsiPerluSurvey1;
					$perlusurvey2Value = $objJenisSuratIzin->jsiPerluSurvey2;
					
					if (isset($obj->perlusurvey[$objJenisSuratIzin->jsiID])) {
						$perlusurveyValue = $obj->perlusurvey[$objJenisSuratIzin->jsiID];
					} else {
						$perlusurveyValue = 0;
					}
?>
								<input type="hidden" id="perlusurvey1[<?php echo $objJenisSuratIzin->jsiID; ?>]" name="perlusurvey1[<?php echo $objJenisSuratIzin->jsiID; ?>]" value="<?php echo $perlusurvey1Value; ?>">
								<input type="hidden" id="perlusurvey2[<?php echo $objJenisSuratIzin->jsiID; ?>]" name="perlusurvey2[<?php echo $objJenisSuratIzin->jsiID; ?>]" value="<?php echo $perlusurvey2Value; ?>">
								<input type="hidden" id="perlusurvey[<?php echo $objJenisSuratIzin->jsiID; ?>]" name="perlusurvey[<?php echo $objJenisSuratIzin->jsiID; ?>]" value="<?php echo $perlusurveyValue; ?>">
<?php 
					if ($objJenisSuratIzin->jsiPerluSurvey1) {
						$perkiraanselesai1Value = '';
					} else {
						$perkiraanselesai1Value = $app->MySQLDateToNormal($app->dateFromInterval($obj->mhnTgl, $objJenisSuratIzin->jsiLamaPengurusan1)->toDate);
					}
					if ($objJenisSuratIzin->jsiPerluSurvey2) {
						$perkiraanselesai2Value = '';
					} else {
						$perkiraanselesai2Value = $app->MySQLDateToNormal($app->dateFromInterval($obj->mhnTgl, $objJenisSuratIzin->jsiLamaPengurusan2)->toDate);
					}
					
					$perkiraanselesaiValue = $app->MySQLDateToNormal($obj->perkiraanselesai[$objJenisSuratIzin->jsiID]);
					if ($obj->pola[$objJenisSuratIzin->jsiID] == 0) {
						$perkiraanselesaiStyle = 'display:none;';
					} else {
						/*if ($obj->perlusurvey[$objJenisSuratIzin->jsiID]) {
							$perkiraanselesaiStyle = 'display:none;';
						} else {*/
							$perkiraanselesaiStyle = '';
						//}
					}
?>
								<input type="hidden" id="perkiraanselesai1[<?php echo $objJenisSuratIzin->jsiID; ?>]" name="perkiraanselesai1[<?php echo $objJenisSuratIzin->jsiID; ?>]" value="<?php echo $perkiraanselesai1Value; ?>">
								<input type="hidden" id="perkiraanselesai2[<?php echo $objJenisSuratIzin->jsiID; ?>]" name="perkiraanselesai2[<?php echo $objJenisSuratIzin->jsiID; ?>]" value="<?php echo $perkiraanselesai2Value; ?>">
								<input class="box date2" id="perkiraanselesai[<?php echo $objJenisSuratIzin->jsiID; ?>]" name="perkiraanselesai[<?php echo $objJenisSuratIzin->jsiID; ?>]" maxlength="10" size="10" value="<?php echo $perkiraanselesaiValue; ?>" style="<?php echo $perkiraanselesaiStyle; ?>">
							</td>
						</tr>
						<tr class="trSyarat[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>]" style="<?php echo $display; ?> background-color:#E5E5E5;">
							<td colspan="2" style="padding-left:5px;">
								Administrasi oleh
								<select class="box" id="administrasioleh[<?php echo $objJenisSuratIzin->jsiID; ?>]" name="administrasioleh[<?php echo $objJenisSuratIzin->jsiID; ?>]">
<?php 
					if (count($arrAdministrasiOleh) > 0) {
						foreach ($arrAdministrasiOleh as $k=>$v) {
							echo '<option value="'.$k.'"';
							if (isset($obj->administrasioleh[$objJenisSuratIzin->jsiID])) {
								if ($k == $obj->administrasioleh[$objJenisSuratIzin->jsiID]) {
									echo ' selected';
								}
							}
							echo '>'.$v->nama.' ('.$v->total_detailpermohonan.')</option>';
						}
					}
?>
								</select>
							</td>
						</tr>
<?php 
					if (count($arrSyaratSuratIzin[$jsiTipe][$jsiID]['Umum']) > 0) {
?>

						<tr class="trSyarat[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>]" style="<?php echo $display; ?> background:#D0DAFD; color:#009;">
							<td style="padding-left:3px;"><b>Kelengkapan Dokumen berdasarkan Syarat Umum untuk <?php echo $objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : ''); ?></b></td>
							<td width="100px">Pendaftaran</td>
						</tr>
<?php
						foreach ($arrSyaratSuratIzin[$jsiTipe][$jsiID]['Umum'] as $v) {
?>
						<tr class="trSyarat[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>] trUmum" style="<?php echo $display; ?> background:#E5E5E5;">
							<td>
								<input type="checkbox" id="syaratsuratizin[<?php echo $v->ssiID; ?>]" name="syaratsuratizin[<?php echo $v->ssiID; ?>]" value="<?php echo $objJenisSuratIzin->jsiID; ?>" <?php echo (isset($obj->syaratsuratizin[$v->ssiID])) ? 'checked' : ''; ?>>
								<label for="syaratsuratizin[<?php echo $v->ssiID; ?>]" style="<?php echo ($v->ssiMinimal == 1) ? 'color:#FF0000;' : ''; ?>">
									<?php echo $v->ssiNama.($v->ssiJumlah > 1 ? ' sebanyak '.$v->ssiJumlah.' lembar' : ''); ?>
								</label><br>
							</td>
<?php 
							$pembuatan = array();
							if ($v->ssiBaru) {
								$pembuatan[] = '<span class="badge open" title="Baru">B</span>';
							} else {
								$pembuatan[] = '<span class="badge none">B</span>';
							}
							if ($v->ssiPerpanjangan) {
								$pembuatan[] = '<span class="badge close" title="Perpanjangan">PJ</span>';
							} else {
								$pembuatan[] = '<span class="badge none">PJ</span>';
							}
							if ($v->ssiPerubahan) {
								$pembuatan[] = '<span class="badge info" title="Perubahan">U</span>';
							} else {
								$pembuatan[] = '<span class="badge none">U</span>';
							}
							echo '<td>'.implode('', $pembuatan).'</td>';
?>
						</tr>
<?php
						}
					}
					
					if (count($arrSyaratSuratIzin[$jsiTipe][$jsiID]['Khusus']) > 0) {
?>

						<tr class="trSyarat[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>]" style="<?php echo $display; ?> background:#D0DAFD; color:#009;">
							<td style="padding-left:3px;"><b>Kelengkapan Dokumen berdasarkan Syarat Khusus untuk <?php echo $objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : ''); ?></b></td>
							<td width="100px">Pendaftaran</td>
						</tr>
<?php
						foreach ($arrSyaratSuratIzin[$jsiTipe][$jsiID]['Khusus'] as $v) {
?>
						<tr class="trSyarat[<?php echo $jsiTipe.'-'.$objJenisSuratIzin->jsiID; ?>] trKhusus" style="<?php echo $display; ?> background:#E5E5E5;">
							<td>
								<input type="checkbox" id="syaratsuratizin[<?php echo $v->ssiID; ?>]" name="syaratsuratizin[<?php echo $v->ssiID; ?>]" value="<?php echo $objJenisSuratIzin->jsiID; ?>" <?php echo (isset($obj->syaratsuratizin[$v->ssiID])) ? 'checked' : ''; ?>>
								<label for="syaratsuratizin[<?php echo $v->ssiID; ?>]" style="<?php echo ($v->ssiMinimal == 1) ? 'color:#FF0000;' : ''; ?>">
									<?php echo $v->ssiNama.($v->ssiJumlah > 1 ? ' sebanyak '.$v->ssiJumlah.' lembar' : ''); ?>
								</label><br>
							</td>
<?php 
							$pembuatan = array();
							if ($v->ssiBaru) {
								$pembuatan[] = '<span class="badge open" title="Baru">B</span>';
							} else {
								$pembuatan[] = '<span class="badge none">B</span>';
							}
							if ($v->ssiPerpanjangan) {
								$pembuatan[] = '<span class="badge close" title="Perpanjangan">PJ</span>';
							} else {
								$pembuatan[] = '<span class="badge none">PJ</span>';
							}
							if ($v->ssiPerubahan) {
								$pembuatan[] = '<span class="badge info" title="Perubahan">U</span>';
							} else {
								$pembuatan[] = '<span class="badge none">U</span>';
							}
							echo '<td>'.implode('', $pembuatan).'</td>';
?>
						</tr>
<?php
						}
					}
					$totalJenisSuratIzin++;
				}
			}
			
			if ($totalJenisSuratIzin == 0) {
?>
						<tr>
							<td colspan="2" valign="top">Tidak ada</td>
						</tr>
<?php
			}
?>
						</table>
					</td>
				</tr>
<?php 
		}
?>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript">
		natDays = <?php echo json_encode($app->getHolidays()); ?>;
	</script>
	<script type="text/javascript" src="acts/permohonan/js/edit.permohonan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printPermohonan($obj) {
		global $app;
		
		$objReport 		= new Report('Formulir Pendaftaran', 'FormulirPendaftaran-'.$obj->mhnNoUrutLengkap.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");

		$sql = "SELECT wilayah.*, wilayah2.wilNama AS parent1, CONCAT(wilayah3.wilTingkat,' ',wilayah3.wilNama) AS parent2, wilayah4.wilNama AS parent3
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objMhnKelurahan = $app->queryObject($sql);
		
		if ($obj->mhnPerusahaanKelurahanID > 0) {
			$sql = "SELECT wilayah.*, wilayah2.wilNama AS parent1, CONCAT(wilayah3.wilTingkat,' ',wilayah3.wilNama) AS parent2, wilayah4.wilNama AS parent3
					FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4
					WHERE wilayah.wilID='".$obj->mhnPerusahaanKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		} else {
			$sql = "SELECT wilayah.*, wilayah2.wilNama AS parent1, CONCAT(wilayah3.wilTingkat,' ',wilayah3.wilNama) AS parent2, wilayah4.wilNama AS parent3
					FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4
					WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		}
		$objPerKelurahan = $app->queryObject($sql);
		
		$sql = "SELECT *, dmhnID AS id 
				FROM detailpermohonan, jenissuratizin
				WHERE dmhnMhnID='".$obj->mhnID."' AND dmhnJsiID=jsiID";
		$arrDetailPermohonan = $app->queryArrayOfObjects($sql);
		
		$sql = "SELECT * 
				FROM pengguna
				WHERE pnID='".$obj->mhnDibuatOleh."'";
		$objPengguna = $app->queryObject($sql);
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
		<h3 align="center">FORMULIR PENDAFTARAN</h3>
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td>
				Nomor Pendaftaran : <?php echo $obj->mhnNoUrutLengkap; ?>
			</td>
			<td align="right">
				Tanggal : <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		</table>
		
		<br><br>
		
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width="26%">No. KTP</td><td width="4%">&nbsp;:&nbsp;</td>
			<td width="70%"><?php echo $obj->mhnNoKtp; ?></td>
		</tr>
		<tr>
			<td>Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td><?php echo strtoupper($obj->mhnNama); ?></td>
		</tr>
		<tr>
			<td>Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td><?php echo strtoupper($obj->mhnAlamat).', '.strtoupper($objMhnKelurahan->wilNama).', '.strtoupper($objMhnKelurahan->parent1).', '.strtoupper($objMhnKelurahan->parent2).', '.strtoupper($objMhnKelurahan->parent3); ?></td>
		</tr>
<?php 
		if (isset($obj->perNama)) {
?>
		<tr>
			<td>Nama Perusahaan</td><td>&nbsp;:&nbsp;</td>
			<td><?php echo strtoupper($obj->perNama); ?></td>
		</tr>
		<tr>
			<td>Alamat Perusahaan</td><td>&nbsp;:&nbsp;</td>
			<td><?php echo strtoupper($obj->mhnPerusahaanKelurahanID > 0 ? $obj->mhnPerusahaanAlamat : $obj->perAlamat).', '.strtoupper($objPerKelurahan->wilNama).', '.strtoupper($objPerKelurahan->parent1).', '.strtoupper($objPerKelurahan->parent2).', '.strtoupper($objPerKelurahan->parent3); ?></td>
		</tr>
<?php
		}
?>
		<tr>
			<td colspan="3">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">Untuk Pengurusan Perizinan : </td>
		</tr>
		<tr>
			<td colspan="3">
<?php 
		if (count($arrDetailPermohonan) > 1) {
?>
				<ol>
<?php
			foreach ($arrDetailPermohonan as $objDetailPermohonan) {
				if ($objDetailPermohonan->dmhnPola == 2) {
					if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
						$perkiraanselesai = ' dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima.'; // Belum ditentukan perkiraan selesainya';
					} else {
						$perkiraanselesai = ' dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai);
					}
				} else if ($objDetailPermohonan->dmhnPola == 1) {
					if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
						$perkiraanselesai = ' belum ditentukan perkiraan selesainya';
					} else {
						$perkiraanselesai = ' dengan perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai);
					}
				} else {
					$perkiraanselesai = ' belum ditentukan prosedur pelayanannya (Pola 1 atau Pola 2)';
				}
?>
					<li>Pendaftaran <?php echo $objDetailPermohonan->dmhnTipe.' '.$objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : '').' '.$perkiraanselesai; ?></li>		
<?php
			}
?>
				</ol>
<?php
		} else if (count($arrDetailPermohonan) == 1) {
			$objDetailPermohonan = current($arrDetailPermohonan);
			
			if ($objDetailPermohonan->dmhnPola == 2) {
				if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
					$perkiraanselesai = ' dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima.'; // Belum ditentukan perkiraan selesainya';
				} else {
					$perkiraanselesai = ' dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai);
				}
			} else if ($objDetailPermohonan->dmhnPola == 1) {
				if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
					$perkiraanselesai = ' belum ditentukan perkiraan selesainya';
				} else {
					$perkiraanselesai = ' dengan perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai);
				}
			} else {
				$perkiraanselesai = ' belum ditentukan prosedur pelayanannya (Pola 1 atau Pola 2)';
			}
?>
				<p>Pendaftaran <?php echo $objDetailPermohonan->dmhnTipe.' '.$objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : '').' '.$perkiraanselesai; ?></p>
<?php
		}
?>
			</td>
		</tr>
		</table>
		
		<br><br>
		
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width="50%" rowspan="5">
				<!-- <small>
				Catatan:
				<ol>
					<li>Pola 1 prosedur pelayanan tanpa survey</li>
					<li>Pola 2 prosedur pelayanan dengan survey yang lama penyelesaiannya terhitung setelah survey lapangan dan berkas lengkap diterima</li>
				</ol>
				</small> -->
			</td>
			<td width="50%" align="center">
				<p><?php echo $objSatuanKerja->wilIbukota; ?>,<br><?php echo $objPengguna->pnJabatan; ?></p>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td align="center">(<b><?php echo strtoupper($objPengguna->pnNama); ?></b>)</td>
		</tr>
		</table>
<?php		
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	static function readPermohonan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Permohonan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
<?php 
		switch ($_SESSION['sesipengguna']->levelAkses) {
			case 'Petugas Loket Pelayanan':
?>
				<div id="toolbarEntri">
					<a class="button-link inline dark-blue" href="index2.php?act=permohonan&task=print&id=<?php echo $obj->mhnID; ?>&html=0" target="_blank">Cetak Formulir Pendaftaran</a>
					<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<a class="button-link inline dark-blue" href="index2.php?act=permohonan&task=print&id=<?php echo $obj->mhnID; ?>&html=0" target="_blank">Cetak Formulir Pendaftaran</a>
					<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
					<br>
					<br>
				</div>
<?php
				break; 
			default:
?>
				<div id="toolbarEntri">
					<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
					<br>
					<br>
				</div>
<?php
		}

		$app->showMessage($success, $msg);
?>
			<br>
			<table class="readTable" border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td width="150">Tanggal</td><td>&nbsp;:&nbsp;</td>
				<td>
					<input type="hidden" id="mhnTgl" name="mhnTgl" value="<?php echo $obj->mhnTgl; ?>">
					<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
				</td>
			</tr>
			<tr>
				<td>No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
				<td>
					<input type="hidden" id="mhnNoUrut" name="mhnNoUrut" value="<?php echo $obj->mhnNoUrut; ?>">
					<input type="hidden" id="mhnNoUrutLengkap" name="mhnNoUrutLengkap" value="<?php echo $obj->mhnNoUrutLengkap; ?>">
					<?php echo $obj->mhnNoUrutLengkap; ?>
				</td>
			</tr>
			<tr>
				<td>Loket</td><td>&nbsp;:&nbsp;</td>
				<td>
					<input type="hidden" id="mhnLokID" name="mhnLokID" value="<?php echo $obj->mhnLokID; ?>">
					<?php echo $app->queryField("SELECT lokNama FROM loket WHERE lokID='".$obj->mhnLokID."'"); ?>
				</td>
			</tr>
			<tr>
				<td>Petugas</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnID > 0 ? $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->mhnDibuatOleh."'") : $_SESSION['sesipengguna']->nama); ?>
				</td>
			</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Pemohon</u></b></p></td>
				</tr>
			<tr>
				<td style="padding-left:20px;">No. KTP</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->mhnNoKtp; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->mhnNama; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->mhnAlamat; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;"></td><td></td>
				<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Tempat Lahir</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnTempatLahir != '') ? $obj->mhnTempatLahir : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Tanggal Lahir</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnTglLahir != '0000-00-00') ? $app->MySQLDateToNormal($obj->mhnTglLahir) : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Kewarganegaraan</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnKewarganegaraan != '') ? $obj->mhnKewarganegaraan : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Pekerjaan</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnPekerjaan != '') ? $obj->mhnPekerjaan : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnNoTelp != '') ? $obj->mhnNoTelp : '-'; ?>
				</td>
			</tr>
			<tr>
				<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
			</tr>
<?php 
		if ($obj->mhnPerID > 0) {
?>
			<tr>
				<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->perNama != '') ? $obj->perNama : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnPerusahaanKelurahanID > 0 ? $obj->mhnPerusahaanAlamat : (($obj->perAlamat != '') ? $obj->perAlamat : '-'));  ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
				<td>
<?php 
			$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
					FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
					WHERE wilayah.wilID='".($obj->mhnPerusahaanKelurahanID > 0 ? $obj->mhnPerusahaanKelurahanID : $obj->perKelurahanID)."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
			echo $app->queryField($sql); 
?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->perNoTelp != '') ? $obj->perNoTelp : '-'; ?>
				</td>
			</tr>
<?php 
		} else {
?>
			<tr>
				<td colspan="3" style="padding-left:20px;">Tidak ada</td>
			</tr>
<?php
		}
?>
			<tr>
				<td colspan="3">
					<p><b><u>Untuk Pengurusan Surat Izin</u></b></p>
					<p><i>Petunjuk: persyaratan dengan <span class="wajib">warna merah</span> adalah persyaratan minimal.</i></p>
				</td>
			</tr>
<?php 
		$sql = "SELECT *, dmhnID AS id 
				FROM detailpermohonan, jenissuratizin
				WHERE dmhnMhnID='".$obj->mhnID."' AND dmhnJsiID=jsiID";
		$detailpermohonan = $app->queryArrayOfObjects($sql);
		
		if (count($detailpermohonan) > 0) {
?>
			<tr>
				<td style="padding-left:20px;">Surat Izin</td><td>&nbsp;:&nbsp;</td>
				<td>
					<table class="readTable2" border="0" cellpadding="0" cellspacing="0" width="100%">
<?php
			foreach ($detailpermohonan as $objDetailPermohonan) {
				if ($objDetailPermohonan->dmhnPola == 2) {
					if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
						$perkiraanselesai = ' dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Belum ditentukan perkiraan selesainya';
					} else {
						$perkiraanselesai = ' dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai);
					}
				} else if ($objDetailPermohonan->dmhnPola == 1) {
					if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
						$perkiraanselesai = ' belum ditentukan perkiraan selesainya';
					} else {
						$perkiraanselesai = ' dengan perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai);
					}
				} else {
					$perkiraanselesai = ' belum ditentukan prosedur pelayanannya (Pola 1 atau Pola 2)';
				}
?>
					<tr style="background:#E7BD42;">
						<td colspan="2" style="padding:6px;">
<?php 
				switch ($objDetailPermohonan->dmhnTipe) {
					case 'Baru':
						echo '<span class="badge open">Baru</span>';
						break;
					case 'Perubahan':
						echo '<span class="badge info">Perubahan</span>';
						break;
					case 'Perpanjangan':
						echo '<span class="badge close">Perpanjangan</span>';
						break;
				}
?>
							<?php echo $objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : '').', '.$perkiraanselesai; ?>
						</td>
					</tr>
					<tr style="background-color:#E5E5E5;">
						<td colspan="2" style="padding-left:3px;">
							Administrasi oleh <b>
<?php 
				echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$objDetailPermohonan->dmhnDiadministrasikanOleh."'");
?>
							</b>
						</td>
					</tr>
<?php
				$syarat = array(
					'Umum' => array(),
					'Khusus' => array()
				);
				
				$sql = "SELECT * 
						FROM syaratsuratizin
						LEFT JOIN kelengkapandokumen ON kdDmhnID='".$objDetailPermohonan->dmhnID."' AND kdSsiID=ssiID 
						WHERE ssiJsiID='".$objDetailPermohonan->jsiID."'
						ORDER BY ssiNo";
				$rs = $app->query($sql);
				
				while(($objSyaratSuratIzin = mysql_fetch_object($rs)) == true){
					$syarat[$objSyaratSuratIzin->ssiUmumKhusus][] = $objSyaratSuratIzin;
				}
				
				if (count($syarat['Umum']) > 0) {
?>
					<tr style="background-color:#D0DAFD; color:#009;">
						<td colspan="2" style="padding-left:3px;"><b>Kelengkapan Dokumen berdasarkan Syarat Umum untuk <?php echo $objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : ''); ?></b></td>
					</tr>
<?php
					foreach ($syarat['Umum'] as $v) {
?>
					<tr style="background:#E5E5E5;">
						<td width="25">
<?php 
						if (isset($v->kdSsiID)) {
?>
							<input type="checkbox" id="syaratsuratizin[<?php echo $v->ssiID; ?>]" name="syaratsuratizin[<?php echo $v->ssiID; ?>]" value="<?php echo $objDetailPermohonan->jsiID; ?>" <?php echo isset($v->kdSsiID) ? 'checked' : ''; ?> disabled>
<?php 
						}
?>
						</td>	
						<td>
							<label for="syaratsuratizin[<?php echo $v->ssiID; ?>]" style="<?php echo ($v->ssiMinimal == 1) ? 'color:#FF0000;' : ''; ?>">
								<?php echo $v->ssiNama.($v->ssiJumlah > 1 ? ' sebanyak '.$v->ssiJumlah.' lembar' : ''); ?>
							</label><br>
						</td>
					</tr>
<?php 
					}
				}
				
				if (count($syarat['Khusus']) > 0) {
?>

					<tr style="background-color:#D0DAFD; color:#009;">
						<td colspan="2" style="padding-left:3px;"><b>Kelengkapan Dokumen berdasarkan Syarat Khusus untuk <?php echo $objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : ''); ?></b></td>
					</tr>
<?php
					foreach ($syarat['Khusus'] as $v) {
?>
					<tr style="background:#E5E5E5;">
						<td width="25">
<?php 
						if (isset($v->kdSsiID)) {
?>
							<input type="checkbox" id="syaratsuratizin[<?php echo $v->ssiID; ?>]" name="syaratsuratizin[<?php echo $v->ssiID; ?>]" value="<?php echo $objDetailPermohonan->jsiID; ?>" <?php echo isset($v->kdSsiID) ? 'checked' : ''; ?> read>
<?php 
						}
?>
						</td>	
						<td>
							<label for="syaratsuratizin[<?php echo $v->ssiID; ?>]" style="<?php echo ($v->ssiMinimal == 1) ? 'color:#FF0000;' : ''; ?>">
								<?php echo $v->ssiNama.($v->ssiJumlah > 1 ? ' sebanyak '.$v->ssiJumlah.' lembar' : ''); ?>
							</label><br>
						</td>
					</tr>
<?php
					}
				}
			}
		}
?>
					</table>
				</td>
			</tr>
			</table>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/permohonan/js/read.permohonan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function smsPermohonan($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=permohonan&task=sms&id=<?php echo $obj->mhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/email.png" border="0" /> Kirim SMS Pemberitahuan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<br>
			<table class="readTable" border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td width="150">Tanggal</td><td>&nbsp;:&nbsp;</td>
				<td>
					<input type="hidden" id="mhnTgl" name="mhnTgl" value="<?php echo $obj->mhnTgl; ?>">
					<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
				</td>
			</tr>
			<tr>
				<td>No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
				<td>
					<input type="hidden" id="mhnNoUrut" name="mhnNoUrut" value="<?php echo $obj->mhnNoUrut; ?>">
					<input type="hidden" id="mhnNoUrutLengkap" name="mhnNoUrutLengkap" value="<?php echo $obj->mhnNoUrutLengkap; ?>">
					<?php echo $obj->mhnNoUrutLengkap; ?>
				</td>
			</tr>
			<tr>
				<td>Loket</td><td>&nbsp;:&nbsp;</td>
				<td>
					<input type="hidden" id="mhnLokID" name="mhnLokID" value="<?php echo $obj->mhnLokID; ?>">
					<?php echo $app->queryField("SELECT lokNama FROM loket WHERE lokID='".$obj->mhnLokID."'"); ?>
				</td>
			</tr>
			<tr>
				<td>Petugas</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnID > 0 ? $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->mhnDibuatOleh."'") : $_SESSION['sesipengguna']->nama); ?>
				</td>
			</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Pemohon</u></b></p></td>
				</tr>
			<tr>
				<td style="padding-left:20px;">No. KTP</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->mhnNoKtp; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->mhnNama; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->mhnAlamat; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;"></td><td></td>
				<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Tempat Lahir</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnTempatLahir != '') ? $obj->mhnTempatLahir : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Tanggal Lahir</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnTglLahir != '0000-00-00') ? $app->MySQLDateToNormal($obj->mhnTglLahir) : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Kewarganegaraan</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnKewarganegaraan != '') ? $obj->mhnKewarganegaraan : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Pekerjaan</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnPekerjaan != '') ? $obj->mhnPekerjaan : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->mhnNoTelp != '') ? $obj->mhnNoTelp : '-'; ?>
				</td>
			</tr>
			<tr>
				<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
			</tr>
<?php 
		if ($obj->mhnPerID > 0) {
?>
			<tr>
				<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->perNama != '') ? $obj->perNama : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->perAlamat != '') ? $obj->perAlamat : '-'; ?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
				<td>
<?php 
			$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
					FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
					WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
			echo $app->queryField($sql);
?>
				</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo ($obj->perNoTelp != '') ? $obj->perNoTelp : '-'; ?>
				</td>
			</tr>
<?php 
		} else {
?>
			<tr>
				<td colspan="3" style="padding-left:20px;">Tidak ada</td>
			</tr>
<?php
		}
?>
			<tr>
				<td colspan="3">
					<p><b><u>Untuk Pengurusan Surat Izin</u></b></p>
					<p><i>Petunjuk: persyaratan dengan <span class="wajib">warna merah</span> adalah persyaratan minimal.</i></p>
				</td>
			</tr>
<?php 
		$sql = "SELECT *, dmhnID AS id 
				FROM detailpermohonan, jenissuratizin
				WHERE dmhnMhnID='".$obj->mhnID."' AND dmhnJsiID=jsiID";
		$detailpermohonan = $app->queryArrayOfObjects($sql);
		
		if (count($detailpermohonan) > 0) {
?>
			<tr>
				<td style="padding-left:20px;">Surat Izin</td><td>&nbsp;:&nbsp;</td>
				<td>
					<table class="readTable2" border="0" cellpadding="0" cellspacing="0" width="100%">
<?php
			foreach ($detailpermohonan as $objDetailPermohonan) {
				if ($objDetailPermohonan->dmhnPola == 2) {
					if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
						$perkiraanselesai = ' dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Belum ditentukan perkiraan selesainya';
					} else {
						$perkiraanselesai = ' dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai);
					}
				} else if ($objDetailPermohonan->dmhnPola == 1) {
					if ($objDetailPermohonan->dmhnTglTargetSelesai == '0000-00-00') {
						$perkiraanselesai = ' belum ditentukan perkiraan selesainya';
					} else {
						$perkiraanselesai = ' dengan perkiraan selesai '.$app->MySQLDateToIndonesia($objDetailPermohonan->dmhnTglTargetSelesai);
					}
				} else {
					$perkiraanselesai = ' belum ditentukan prosedur pelayanannya (Pola 1 atau Pola 2)';
				}
?>
					<tr style="background:#E7BD42;">
						<td colspan="2" style="padding:6px;">
<?php 
				switch ($objDetailPermohonan->dmhnTipe) {
					case 'Baru':
						echo '<span class="badge open">Baru</span>';
						break;
					case 'Perubahan':
						echo '<span class="badge info">Perubahan</span>';
						break;
					case 'Perpanjangan':
						echo '<span class="badge close">Perpanjangan</span>';
						break;
				}
?>
							<?php echo $objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : '').', '.$perkiraanselesai; ?>
						</td>
					</tr>
					<tr style="background-color:#E5E5E5;">
						<td colspan="2" style="padding-left:3px;">
							Administrasi oleh <b>
<?php 
				echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$objDetailPermohonan->dmhnDiadministrasikanOleh."'");
?>
							</b>
						</td>
					</tr>
<?php
				$syarat = array(
					'Umum' => array(),
					'Khusus' => array()
				);
				
				$sql = "SELECT * 
						FROM syaratsuratizin
						LEFT JOIN kelengkapandokumen ON kdDmhnID='".$objDetailPermohonan->dmhnID."' AND kdSsiID=ssiID 
						WHERE ssiJsiID='".$objDetailPermohonan->jsiID."'
						ORDER BY ssiNo";
				$rs = $app->query($sql);
				
				while(($objSyaratSuratIzin = mysql_fetch_object($rs)) == true){
					$syarat[$objSyaratSuratIzin->ssiUmumKhusus][] = $objSyaratSuratIzin;
				}
				
				if (count($syarat['Umum']) > 0) {
?>
					<tr style="background-color:#D0DAFD; color:#009;">
						<td colspan="2" style="padding-left:3px;"><b>Kelengkapan Dokumen berdasarkan Syarat Umum untuk <?php echo $objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : ''); ?></b></td>
					</tr>
<?php
					foreach ($syarat['Umum'] as $v) {
?>
					<tr style="background:#E5E5E5;">
						<td width="25">
<?php 
						if (isset($v->kdSsiID)) {
?>
							<input type="checkbox" id="syaratsuratizin[<?php echo $v->ssiID; ?>]" name="syaratsuratizin[<?php echo $v->ssiID; ?>]" value="<?php echo $objDetailPermohonan->jsiID; ?>" <?php echo isset($v->kdSsiID) ? 'checked' : ''; ?> disabled>
<?php 
						}
?>
						</td>	
						<td>
							<label for="syaratsuratizin[<?php echo $v->ssiID; ?>]" style="<?php echo ($v->ssiMinimal == 1) ? 'color:#FF0000;' : ''; ?>">
								<?php echo $v->ssiNama.($v->ssiJumlah > 1 ? ' sebanyak '.$v->ssiJumlah.' lembar' : ''); ?>
							</label><br>
						</td>
					</tr>
<?php 
					}
				}
				
				if (count($syarat['Khusus']) > 0) {
?>

					<tr style="background-color:#D0DAFD; color:#009;">
						<td colspan="2" style="padding-left:3px;"><b>Kelengkapan Dokumen berdasarkan Syarat Khusus untuk <?php echo $objDetailPermohonan->jsiNama.($objDetailPermohonan->jsiSubNama != '' ? ' ('.$objDetailPermohonan->jsiSubNama.')' : ''); ?></b></td>
					</tr>
<?php
					foreach ($syarat['Khusus'] as $v) {
?>
					<tr style="background:#E5E5E5;">
						<td width="25">
<?php 
						if (isset($v->kdSsiID)) {
?>
							<input type="checkbox" id="syaratsuratizin[<?php echo $v->ssiID; ?>]" name="syaratsuratizin[<?php echo $v->ssiID; ?>]" value="<?php echo $objDetailPermohonan->jsiID; ?>" <?php echo isset($v->kdSsiID) ? 'checked' : ''; ?> read>
<?php 
						}
?>
						</td>	
						<td>
							<label for="syaratsuratizin[<?php echo $v->ssiID; ?>]" style="<?php echo ($v->ssiMinimal == 1) ? 'color:#FF0000;' : ''; ?>">
								<?php echo $v->ssiNama.($v->ssiJumlah > 1 ? ' sebanyak '.$v->ssiJumlah.' lembar' : ''); ?>
							</label><br>
						</td>
					</tr>
<?php
					}
				}
			}
		}
?>
					</table>
				</td>
			</tr>
			</table>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/permohonan/js/read.permohonan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewPermohonan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=permohonan"><img src="images/icons/rosette.png" border="0" /> Daftar Permohonan</a></h1>
		<p>
			<i>
				Petunjuk: baris berwarna putih adalah permohonan, sedangkan baris berwarna biru muda adalah detail permohonan (alur penyelesaian) beserta surat izinnya.
				Surat izin harus dientri terlebih dahulu, baru dapat dilihat ataupun dicetak.
			</i>
		</p>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Loket Pelayanan') {
?>
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=permohonan&task=addPerseorangan">Permohonan Perseorangan</a>
				<a class="button-link inline" id="btnTambah" href="index2.php?act=permohonan&task=addPerusahaan">Permohonan Perusahaan</a>
			</div>
<?php
		}
		
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				<select class="box" id="filterfield" name="filterfield">
<?php 
		foreach (array('mhnNoUrutLengkap'=>'No. Pendaftaran', 'mhnNoKtp'=>'No. KTP', 'mhnNama'=>'Nama Pemohon', 'perNama'=>'Nama Perusahaan', 'jsiNama'=>'Jenis Surat Izin') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['filter']['field']) {
				echo 'selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filterstatus" name="filterstatus">
					<option value="">--Seluruh Status--</option>
<?php 
		foreach (array('Belum Ditentukan Polanya', 'Belum Survey', 'Ditunda', 'Batal', 'Belum Selesai', 'Belum Diambil', 'Sudah Diambil', 'Dikembalikan') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $arr['filter']['status']) {
				echo 'selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['status'] != $arr['default']['status']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 80),
			$app->setHeader('mhnTgl', 'Tanggal', true, 60),
			$app->setHeader('mhnNoUrutLengkap', 'Nomor<br>Pendaftaran', true),
			$app->setHeader('mhnNoKtp', 'Nomor KTP', true),
			$app->setHeader('', '', true),
			$app->setHeader('mhnNama', 'Pemohon/<br>Perlu Survey?', true),
			$app->setHeader('perNama', 'Perusahaan /<br>Tgl. Perkiraan Selesai', true),
			$app->setHeader('dibuatoleh', 'Didaftarkan Oleh/<br>Diadministrasikan Oleh', true),
			$app->setHeader('dmhnDiadministrasikanStatus', 'Status Pengurusan', false)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			
			foreach ($arr['data'] as $v) {
				$mhnSelesai = true;
				if (count($arr['subdata'][$v->mhnID]) > 0) {
					foreach ($arr['subdata'][$v->mhnID] as $v2) {
						if ($v2->dmhnTelahAmbil == 0) {
							$mhnSelesai = false;
						}
					}
				}
				
				echo '<tr>';
				switch ($_SESSION['sesipengguna']->levelAkses) {
					case 'Petugas Loket Pelayanan':
						echo '<td>';
						if ($v->mhnDibuatOleh == $_SESSION['sesipengguna']->ID) {
							if (!$mhnSelesai) {
								echo '<a href="index2.php?act=permohonan&task=edit&id='.$v->mhnID.'" title="Ubah Permohonan"><img src="images/icons/pencil.png" border="0" /></a> ';
								echo '<a href="javascript:hapus('.$v->mhnID.', '."'".$v->mhnNoUrutLengkap."'".');" title="Hapus Permohonan"><img src="images/icons/delete2.png" border="0" /></a> ';
							} else {
								echo '<a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'" title="Lihat Permohonan"><img src="images/icons/zoom.png" border="0" /></a> ';
							}
							echo '<a href="index2.php?act=permohonan&task=print&id='.$v->mhnID.'&html=0" title="Cetak Formulir Permohonan" target="_blank"><img src="images/icons/printer.png" border="0" /></a>';
						} else {
							echo '<a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'" title="Lihat Permohonan"><img src="images/icons/zoom.png" border="0" /></a> ';
						}
						echo '</td>';
						break;
					default:
						echo '<td>';
            echo '<a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'" title="Lihat Permohonan"><img src="images/icons/zoom.png" border="0" /></a> ';
            echo '&nbsp;<a target="_blank" href="' . rtrim(CFG_LIVE_PATH, "/") . ':1337/permohonan/' . $v->mhnID . '" title="Lihat Data Spasial Permohonan"><img src="images/icons/map.png" border="0" /></a> ';
						echo '</td>';
				}
				echo '<td>'.$app->MySQLDateToNormal($v->mhnTgl).'</td>';
				switch ($_SESSION['sesipengguna']->levelAkses) {
					case 'Petugas Loket Pelayanan':
						if ($v->mhnDibuatOleh == $_SESSION['sesipengguna']->ID) {
							if (!$mhnSelesai) {
								echo '<td><a href="index2.php?act=permohonan&task=edit&id='.$v->mhnID.'" title="Ubah Permohonan">'.$v->mhnNoUrutLengkap.'</a></td>';
							} else {
								echo '<td><a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'" title="Lihat Permohonan">'.$v->mhnNoUrutLengkap.'</a></td>';
							}
						} else {
							echo '<td><a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'" title="Lihat Permohonan">'.$v->mhnNoUrutLengkap.'</a></td>';
						}
						break;
					default:
						echo '<td><a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'" title="Lihat Permohonan">'.$v->mhnNoUrutLengkap.'</a></td>';
				}
				echo '<td>'.$v->mhnNoKtp.'</td>';
				echo '<td></td>';
				echo '<td>'.$v->mhnNama.'</td>';
				echo '<td>'.$v->perNama.'</td>';
				echo '<td>'.$v->dibuatoleh.'</td>';
				echo '<td></td>';
				echo "</tr>\n";
				
				$hasReklame = false;
				$textReklame = '';
				$idReklame = 0;
				
				$hasPemecahanIMB = false;
				$textPemecahanIMB = '';
				$idPemecahanIMB = 0;
				
				$hasPemecahanSITU = false;
				$textPemecahanSITU = '';
				$idPemecahanSITU = 0;
				
				$hasPemecahanSITUHO = false;
				$textPemecahanSITUHO = '';
				$idPemecahanSITUHO = 0;
				
				$hasPemecahanTDP = false;
				$textPemecahanTDP = '';
				$idPemecahanTDP = 0;
				
				$hasPemecahanTDI = false;
				$textPemecahanTDI = '';
				$idPemecahanTDI = 0;
				
				$hasPariwisata = false;
				$textPariwisata = '';
				$idPariwisata = 0;
				
				$hasPariwisata = false;
				$textPariwisata = '';
				$idPariwisata = 0;
				
				$hasTDUK = false;
				
				if (count($arr['subdata'][$v->mhnID]) > 0) {
					foreach ($arr['subdata'][$v->mhnID] as $v2) {
						if ($v2->jsiKode == 'reklame') {
							$hasReklame = true;
							$textReklame = $v2->jsiNama;
							$idReklame = $v2->dmhnID;
						}
						
						if ($v2->jsiKode == 'pemecahanimb') {
							$hasPemecahanIMB = true;
							$textPemecahanIMB = $v2->jsiNama;
							$idPemecahanIMB = $v2->dmhnID;
						}
						
						if ($v2->jsiKode == 'situ') {
							$hasPemecahanSITU = true;
							$textPemecahanSITU = $v2->jsiNama;
							$idPemecahanSITU = $v2->dmhnID;
						}
						
						if ($v2->jsiKode == 'situho') {
							$hasPemecahanSITUHO = true;
							$textPemecahanSITUHO = $v2->jsiNama;
							$idPemecahanSITUHO = $v2->dmhnID;
						} 
						
						if ($v2->jsiKode == 'tdp') {
							$hasPemecahanTDP = true;
							$textPemecahanTDP = $v2->jsiNama;
							$idPemecahanTDP = $v2->dmhnID;
						} 
						
						if ($v2->jsiKode == 'tdi') {
							$hasPemecahanTDI = true;
							$textPemecahanTDI = $v2->jsiNama;
							$idPemecahanTDI = $v2->dmhnID;
						} 
						
						if ($v2->jsiKode == 'pariwisata') {
							$hasPariwisata = true;
							$textPariwisata = 'TDUK';
							$idPariwisata = $v2->dmhnID;
						}
						
						if ($v2->jsiKode == 'tduk') {
							$hasTDUK = true;
						}
						
						echo '<tr class="odd">';
						switch ($_SESSION['sesipengguna']->levelAkses) {
							case 'Petugas Loket Pelayanan':
								echo '<td></td>';
								break;
							case 'Petugas Administrasi':
								echo '<td>';
								if ($v2->dmhnDiadministrasikanOleh == $_SESSION['sesipengguna']->ID) {
									echo '<a href="index2.php?act=dokumen&id='.$v2->dmhnID.'" title="Arsip Kelengkapan Dokumen"><img src="images/icons/book.png" border="0" /></a> ';
									echo '<a href="index2.php?act=detailpermohonan&task=edit&id='.$v2->dmhnID.'" title="Entri Detail Permohonan (Alur Penyelesaian)"><img src="images/icons/arrow_right.png" border="0" /></a> ';
								}
								if ($v2->dmhnDikembalikan == 1) {
									echo '<a href="index2.php?act=detailpermohonan&task=printpenolakan&id='.$v2->dmhnID.'&html=0" title="Cetak Surat Penolakan Permohonan" target="_blank"><img src="images/icons/printer_delete.png" border="0" /></a> ';
									echo '<a href="index2.php?act=detailpermohonan&task=printpengembalian&id='.$v2->dmhnID.'&html=0" title="Cetak Tanda Terima Pengembalian Dokumen Tidak Lengkap" target="_blank"><img src="images/icons/printer_error.png" border="0" /></a> ';
								}
								echo '</td>';
								break;
							case 'Surveyor':
								echo '<td>';
								echo '<a href="index2.php?act=detailpermohonan&task=read&id='.$v2->dmhnID.'" title="Lihat Detail Permohonan (Alur Penyelesaian)"><img src="images/icons/zoom.png" border="0" /></a>';
								if ($v2->dmhnPerluSurvey) {
									echo '<a href="index2.php?act=detailpermohonan&task=edit&id='.$v2->dmhnID.'" title="Entri Detail Permohonan (Alur Penyelesaian)"><img src="images/icons/arrow_right.png" border="0" /></a> ';
								}
								echo '</td>';
								break;
							case 'Petugas Loket Penyerahan':
								echo '<td>';
								echo '<a href="index2.php?act=detailpermohonan&task=read&id='.$v2->dmhnID.'" title="Lihat Detail Permohonan (Alur Penyelesaian)"><img src="images/icons/zoom.png" border="0" /></a>';
								if ($v2->dmhnTelahSelesai) {
									echo '<a href="index2.php?act=detailpermohonan&task=edit&id='.$v2->dmhnID.'" title="Entri Detail Permohonan (Alur Penyelesaian)"><img src="images/icons/arrow_right.png" border="0" /></a> ';
								}
								echo '</td>';
								break;
							default:
								echo '<td>';
								echo '<a href="index2.php?act=detailpermohonan&task=read&id='.$v2->dmhnID.'" title="Lihat Detail Permohonan (Alur Penyelesaian)"><img src="images/icons/zoom.png" border="0" /></a>';
								echo '</td>';
						}
						
						switch ($_SESSION['sesipengguna']->levelAkses) {
							case 'Petugas Administrasi':
								echo '<td>';
								if ($v2->dmhnDiadministrasikanOleh == $_SESSION['sesipengguna']->ID) {								
									if ($v2->dmhnDiadministrasikanPada != '0000-00-00 00:00:00') {
										echo '<a href="index2.php?act='.$v2->jsiKode.'&task=edit&id='.$v2->dmhnID.'" title="Entri Surat Izin"><img src="images/icons/pencil.png" border="0" /></a> ';
										
										if ($v2->dmhnTambahan == 1) {
											echo '<a href="javascript:hapusdetail('."'".$v2->jsiKode."'".','.$v2->dmhnID.');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a> ';
										}
										
										//TODO: belum dikopikan ke seluruh view
										echo '<a id="btnPrint-'.$v2->jsiKode.'-print-'.$v2->dmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
										//echo '<a class="btnPrint" href="frame.set.php?act='.$v2->jsiKode.'&task=print&id='.$v2->dmhnID.'" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
									} else {
										echo '<a href="index2.php?act='.$v2->jsiKode.'&task=edit&id='.$v2->dmhnID.'" title="Entri Surat Izin"><img src="images/icons/pencil.png" border="0" /></a> ';
										
										if ($v2->dmhnTambahan == 1) {
											echo '<a href="javascript:hapusdetail('."'".$v2->jsiKode."'".','.$v2->dmhnID.');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a> ';
										}
									}
								}
								echo '</td>';
								break;
							case 'Surveyor':
								echo '<td></td>';
								break;
							default:
								echo '<td>';
								if ($v2->dmhnDiadministrasikanPada != '0000-00-00 00:00:00') {
									echo '<a href="index2.php?act='.$v2->jsiKode.'&task=read&id='.$v2->dmhnID.'" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
								}
								echo '</td>';
						}
						
						echo '<td>'.$v2->dmhnTipe.'</td>';
						
						switch ($_SESSION['sesipengguna']->levelAkses) {
							case 'Petugas Administrasi':
								echo '<td><a href="index2.php?act='.$v2->jsiKode.'&task=edit&id='.$v2->dmhnID.'" title="Entri Surat Izin">'.$v2->jsiNama.($v2->jsiSubNama != '' ? ' ('.$v2->jsiSubNama.')' : '').'</a></td>';
								break;
							case 'Surveyor':
								echo '<td>'.$v2->jsiNama.($v2->jsiSubNama != '' ? ' ('.$v2->jsiSubNama.')' : '').'</td>';
								break;
							default:
								echo '<td>';
								if ($v2->dmhnDiadministrasikanPada != '0000-00-00 00:00:00') {
									echo '<a href="index2.php?act='.$v2->jsiKode.'&task=read&id='.$v2->dmhnID.'" title="Lihat Surat Izin">'.$v2->jsiNama.($v2->jsiSubNama != '' ? ' ('.$v2->jsiSubNama.')' : '').'</a>';
								} else {
									echo $v2->jsiNama.($v2->jsiSubNama != '' ? ' ('.$v2->jsiSubNama.')' : '');
								}
								echo '</td>';
						}
						
						if ($v->jsiKode == 'situho') {
							$lokasi = $app->queryField("SELECT wilNama FROM wilayah, situho WHERE situhoDmhnID='".$v->dmhnID."' AND situhoKelurahanID=wilID");
						} else if ($v->mhnLokasiUsahaKelurahanID > 0) {
							$lokasi = $v->mhnLokasiUsahaAlamat;
						} else if ($v->mhnPerusahaanKelurahanID > 0){
							$lokasi = $v->mhnPerusahaanAlamat;
						} else {
							$lokasi = $v->perAlamat;
						}
						echo '<td>'.$lokasi.'</td>';
						
						if ($v2->dmhnDikembalikan == 0) {
							if ($v2->dmhnPola > 0) {
								if ($v2->dmhnPerluSurvey) {
									echo '<td>Ada Survey</td>';
								} else {
									echo '<td>Tanpa Survey</td>';
								}
								if ($v2->dmhnTglTargetSelesai != '0000-00-00') {
									echo '<td>'.$app->MySQLDateToNormal($v2->dmhnTglTargetSelesai).'</td>';
								} else {
									echo '<td><span class="badge tidak">Belum Ditentukan</span></td>';
								}
							} else {
								echo '<td><span class="badge tidak">Belum Ditentukan</span></td>';
								echo '<td><span class="badge tidak">Belum Ditentukan</span></td>';
							}
						} else {
							if ($v2->dmhnPola > 0) {
								if ($v2->dmhnPerluSurvey) {
									echo '<td>Ada Survey</td>';
								} else {
									echo '<td>Tanpa Survey</td>';
								}
								if ($v2->dmhnTglTargetSelesai != '0000-00-00') {
									echo '<td>'.$app->MySQLDateToNormal($v2->dmhnTglTargetSelesai).'</td>';
								} else {
									echo '<td>Belum Ditentukan</td>';
								}
							} else {
								echo '<td>Belum Ditentukan</td>';
								echo '<td>Belum Ditentukan</td>';
							}
						}
						echo '<td>'.$v2->diadministrasikanoleh.'</td>';
						
						$badge = '';
					 	if ($v2->dmhnDikembalikan == 0) {
							if ($v2->dmhnPola > 0) {
						 		if ($v2->dmhnPerluSurvey == 1) {
									if ($v2->dmhnTelahSurvey == 0) {
										$badge = '<span class="badge warning">Belum Survey</span>';
									} else {
										if ($v2->dmhnHasilSurvey == 'Memenuhi Syarat') {
											if ($v2->dmhnTelahSelesai == 0) {
												$badge = '<span class="badge tidak">Belum Selesai</span>';
											} else {
												if ($v2->dmhnTelahAmbil == 0) { 
													$badge = '<span class="badge info">Belum Diambil</span>';
												} else {
													$badge = '<span class="badge ya">Sudah Diambil</span>';
												}
											}
										} else if ($v2->dmhnHasilSurvey == 'Ditunda') {
											$badge = '<span class="badge info">Hasil Survey: Ditunda</span>';
										} else if ($v2->dmhnHasilSurvey == 'Batal') {
											$badge = '<span class="badge info">Hasil Survey: Batal</span>';
										}
									}
								} else {
									if ($v2->dmhnTelahSelesai == 0) {
										$badge = '<span class="badge tidak">Belum Selesai</span>';
									} else {
										if ($v2->dmhnTelahAmbil == 0) { 
											$badge = '<span class="badge info">Belum Diambil</span>';
										} else {
											$badge = '<span class="badge ya">Sudah Diambil</span>';
										}
									}
								}
							} else {
								$badge = '<span class="badge info">Belum Ditentukan Polanya</span>';
							}
						} else {
							$badge = '<span class="badge inverse">Dikembalikan</span>';
						}
						echo '<td>'.$badge.'</td>';
						echo "</tr>\n";
					}
				}
						
				if ($hasReklame) {
					echo '<tr class="odd">';
					echo '<td></td>';
					echo '<td colspan="8"><a href="index2.php?act=reklame&task=add&id='.$idReklame.'" title="Tambah Surat Izin ('.$textReklame.')"><img src="images/icons/add.png" border="0" style="padding-right:5px;" />Tambah '.$textReklame.'</a></td>';
					echo "</tr>\n";
				}
				
				if ($hasPemecahanIMB) {
					echo '<tr class="odd">';
					echo '<td></td>';
					echo '<td colspan="8"><a href="index2.php?act=pemecahanimb&task=add&id='.$idPemecahanIMB.'" title="Tambah Surat Izin ('.$textPemecahanIMB.')"><img src="images/icons/add.png" border="0" style="padding-right:5px;" />Tambah '.$textPemecahanIMB.'</a></td>';
					echo "</tr>\n";
				}
				
				if ($hasPemecahanSITU) {
					echo '<tr class="odd">';
					echo '<td></td>';
					echo '<td colspan="8"><a href="index2.php?act=situ&task=add&id='.$idPemecahanSITU.'" title="Tambah Surat Izin ('.$textPemecahanSITU.')"><img src="images/icons/add.png" border="0" style="padding-right:5px;" />Tambah '.$textPemecahanSITU.'</a></td>';
					echo "</tr>\n";
				}
				
				if ($hasPemecahanSITUHO) {
					echo '<tr class="odd">';
					echo '<td></td>';
					echo '<td colspan="8"><a href="index2.php?act=situho&task=add&id='.$idPemecahanSITUHO.'" title="Tambah Surat Izin ('.$textPemecahanSITUHO.')"><img src="images/icons/add.png" border="0" style="padding-right:5px;" />Tambah '.$textPemecahanSITUHO.'</a></td>';
					echo "</tr>\n";
				}
				
				if ($hasPemecahanTDP) {
					echo '<tr class="odd">';
					echo '<td></td>';
					echo '<td colspan="8"><a href="index2.php?act=tdp&task=add&id='.$idPemecahanTDP.'" title="Tambah Surat Izin ('.$textPemecahanTDP.')"><img src="images/icons/add.png" border="0" style="padding-right:5px;" />Tambah '.$textPemecahanTDP.'</a></td>';
					echo "</tr>\n";
				}
				
				if ($hasPemecahanTDI) {
					echo '<tr class="odd">';
					echo '<td></td>';
					echo '<td colspan="8"><a href="index2.php?act=tdi&task=add&id='.$idPemecahanTDI.'" title="Tambah Surat Izin ('.$textPemecahanTDI.')"><img src="images/icons/add.png" border="0" style="padding-right:5px;" />Tambah '.$textPemecahanTDI.'</a></td>';
					echo "</tr>\n";
				}
						
				if ($hasPariwisata && !$hasTDUK) {
					echo '<tr class="odd">';
					echo '<td></td>';
					echo '<td colspan="8"><a href="index2.php?act=tduk&task=add&id='.$idPariwisata.'" title="Entri Surat Izin ('.$textPariwisata.')"><img src="images/icons/add.png" border="0" style="padding-right:5px;" />Entri '.$textPariwisata.'</a></td>';
					echo "</tr>\n";
				}
				
				$i++;
			}
		} else {
?>
			<tr><td colspan="10">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->addButton('imb', 'printlampiran', 'Lampiran');
		$objReport->addButton('imb', 'printretribusi', 'Setoran Retribusi');
		$objReport->addButton('pemecahanimb', 'printretribusi', 'Setoran Retribusi');
		$objReport->showDialog();
?> 
	<script type="text/javascript" src="acts/permohonan/js/permohonan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>