<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Administrasi']['all'] = true;
$acl['Petugas Loket Pelayanan']['all'] = true;
$acl['Petugas Loket Penyerahan']['all'] = true;
$acl['Petugas Loket Pengaduan']['all'] = true;
$acl['Pejabat']['read'] = true;
$acl['Pejabat']['find'] = true;
$acl['Surveyor']['read'] = true;

//Dengan pengecualian
if (!in_array($app->task, array('client'))) {
	if (!$app->checkACL($acl)) {
		die(CFG_RESTRICTED_ACCESS);
	}
}

//Include file(s)
require_once 'model.permohonan.php';
require_once 'view.permohonan.php';

switch ($app->task) {
	case 'addPerseorangan':
		editPermohonan($app->id, "Perseorangan");
		break;
	case 'addPerusahaan':
		editPermohonan($app->id, "Perusahaan");
		break;
	case 'add':
	case 'edit':
		editPermohonan($app->id);
		break;
	case 'client':
		clientPermohonan($app->id);
		break;
	case 'delete':
		deletePermohonan($app->id);
		break;
	case 'find':
		findPermohonan();
		break;
	case 'print':
		printPermohonan($app->id);
		break;
	case 'read':
		readPermohonan($app->id);
		break;
	case 'save':
		savePermohonan();
		break;
	case 'sms':
		smsPermohonan($app->id);
		break;
	default:
		viewPermohonan($app->success, $app->msg);
		break;
}

function clientPermohonan() {
	global $app;
	
	$find = $app->getStr('find');
	
	$result = new stdClass();
	
	if ($find != '') {
		switch ($find) {
			case 'nourutlengkap':
				$nourutlengkap = $app->getStr('nourutlengkap');
				
				$sql = "SELECT mhnID AS id, mhnNoUrutLengkap AS nourut, mhnTgl AS tgl, mhnNama AS pemohon, perNama AS perusahaan
						FROM permohonan 
						LEFT JOIN perusahaan ON mhnPerID=perID
						WHERE mhnNoUrutLengkap='".$nourutlengkap."'";
				$objPermohonan = $app->queryObject($sql);
				if ($objPermohonan) {
					$objPermohonan->suratizin = array();
					
					$sql = "SELECT *
							FROM detailpermohonan
							LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
							WHERE dmhnMhnID='".$objPermohonan->id."'";
					$rs = $app->query($sql);
					if ($rs) {
						while(($obj = mysql_fetch_object($rs)) == true){
							$objSuratIzin = new stdClass();
							$objSuratIzin->nama = $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : '');
							
							if ($obj->dmhnDikembalikan == 0) {
								if ($obj->dmhnPola > 0) {
							 		if ($obj->dmhnPerluSurvey == 1) {
										if ($obj->dmhnTelahSurvey == 0) {
											$objSuratIzin->status = 'Belum Survey';
										} else {
											if ($obj->dmhnTelahSelesai == 0) {
												$objSuratIzin->status = 'Belum Selesai';
											} else {
												if ($obj->dmhnTelahAmbil == 0) { 
													$objSuratIzin->status = 'Belum Diambil';
												} else {
													$objSuratIzin->status = 'Sudah Diambil';
												}
											}
										}
									} else {
										if ($obj->dmhnTelahSelesai == 0) {
											$objSuratIzin->status = 'Belum Selesai';
										} else {
											if ($obj->dmhnTelahAmbil == 0) { 
												$objSuratIzin->status = 'Belum Diambil';
											} else {
												$objSuratIzin->status = 'Sudah Diambil';
											}
										}
									}
								} else {
									$objSuratIzin->status = 'Belum Ditentukan Polanya';
								}
							} else {
								$objSuratIzin->status = 'Dikembalikan';
							}
							
							$objPermohonan->suratizin[] = $objSuratIzin;
						}
					}
					
					$result->obj = $objPermohonan;
					$result->success = true;
				} else {
					$result->success = false;
				}
				echo json_encode($result);
				break;
			default:
				$result->success = false;
				echo json_encode($result);
		}
	} else {
		$result->success = false;
		echo json_encode($result);
	}
}

function deletePermohonan($id) {
	global $app;
	
	//Get object
	$objPermohonan = $app->queryObject("SELECT * FROM permohonan WHERE mhnID='".$id."' AND mhnDibuatOleh='".$_SESSION['sesipengguna']->ID."'");
	if (!$objPermohonan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalSuratIzin = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnMhnID='".$objPermohonan->mhnID."' AND dmhnDiadministrasikanPada<>'0000-00-00'"));
	if ($totalSuratIzin > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalSuratIzin.' surat izin pada permohonan tersebut yang sudah diproses oleh Petugas Administrasi';
	}
	
	$totalDokumen = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan, dokumen WHERE dokDmhnID=dmhnID AND dmhnMhnID='".$objPermohonan->mhnID."'"));
	if ($totalDokumen > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalDokumen.' kelengkapan dokumen pada permohonan tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM permohonan WHERE mhnID='".$objPermohonan->mhnID."'");
		
		$arrDetailPermohonan = $app->queryArrayOfObjects("SELECT *, dmhnID AS id FROM detailpermohonan WHERE dmhnMhnID='".$objPermohonan->mhnID."'");
		if (count($arrDetailPermohonan) > 0) {
			foreach ($arrDetailPermohonan as $v) {
				$app->query("DELETE FROM kelengkapandokumen WHERE kdDmhnID='".$v->dmhnID."'");
			}
		}
		
		$app->query("DELETE FROM detailpermohonan WHERE dmhnMhnID='".$objPermohonan->mhnID."'");
		
		$success = true;
		$msg = 'Permohonan "'.$objPermohonan->mhnNoUrutLengkap.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'permohonan', $objPermohonan->mhnID, $objPermohonan->mhnNoUrutLengkap, $msg);
	} else {
		$success = false;
		$msg = 'Permohonan "'.$objPermohonan->mhnNoUrutLengkap.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewPermohonan($success, $msg);
}

function editPermohonan($id, $tipe = "") {
	global $app;
	
	//Query
	if ($id > 0) {
		$objPermohonan = $app->queryObject("SELECT * FROM permohonan WHERE mhnID='".$id."' AND mhnDibuatOleh='".$_SESSION['sesipengguna']->ID."'");
		if (!$objPermohonan) {
			$app->showPageError();
			exit();
		}
		
		//Data lainnya
		$objPermohonan->jenissuratizin = array();
		$objPermohonan->pola = array();
		$objPermohonan->perlusurvey = array();
		$objPermohonan->perkiraanselesai = array();
		$objPermohonan->syaratsuratizin = array();
		$objPermohonan->administrasioleh = array();
		
		$sql = "SELECT *
				FROM detailpermohonan
				LEFT JOIN kelengkapandokumen ON kdDmhnID=dmhnID
				LEFT JOIN syaratsuratizin ON kdSsiID=ssiID
				WHERE dmhnMhnID='".$objPermohonan->mhnID."'";
		$rs = $app->query($sql);
		
		while(($obj = mysql_fetch_object($rs)) == true){
			$objPermohonan->jenissuratizin[$obj->dmhnJsiID] = $obj->dmhnTipe;
			$objPermohonan->pola[$obj->dmhnJsiID] = $obj->dmhnPola;
			$objPermohonan->perlusurvey[$obj->dmhnJsiID] = $obj->dmhnPerluSurvey;
			$objPermohonan->perkiraanselesai[$obj->dmhnJsiID] = $obj->dmhnTglTargetSelesai;
			
			if (isset($obj->ssiID)) {
				$objPermohonan->syaratsuratizin[$obj->ssiID] = $obj;
			}
			
			$objPermohonan->administrasioleh[$obj->dmhnJsiID] = $obj->dmhnDiadministrasikanOleh;
		}
		
		$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perID='".$objPermohonan->mhnPerID."'");
		if ($objPerusahaan) {
			$objPermohonan->perID = $objPerusahaan->perID;
			$objPermohonan->perNama = $objPerusahaan->perNama;
			$objPermohonan->perAlamat = $objPerusahaan->perAlamat;
			$objPermohonan->perKelurahanID = $objPerusahaan->perKelurahanID;
			$objPermohonan->perNoTelp = $objPerusahaan->perNoTelp;
		} else {
			$objPermohonan->perID = 0;
			$objPermohonan->perNama = '';
			$objPermohonan->perAlamat = '';
			$objPermohonan->perKelurahanID = 0;
			$objPermohonan->perNoTelp = '';
		}
	} else {
		$objPermohonan = new Permohonan_Model();
		
		$objPermohonan->mhnLokID = $_SESSION['sesipengguna']->loketID;
		$objPermohonan->mhnTgl = date('Y-m-d');
		
		//Data lainnya
		$objPermohonan->jenissuratizin = array();
		$objPermohonan->pola = array();
		$objPermohonan->perlusurvey = array();
		$objPermohonan->perkiraanselesai = array();
		$objPermohonan->syaratsuratizin = array();
		$objPermohonan->administrasioleh = array();
		
		$objPermohonan->perID = 0;
		$objPermohonan->perNama = '';
		$objPermohonan->perAlamat = '';
		$objPermohonan->perKelurahanID = 0;
		$objPermohonan->perNoTelp = '';
	}
	
	Permohonan_View::editPermohonan(true, '', $objPermohonan, $tipe);
}

function findPermohonan() {
	global $app;
	
	$find = $app->getStr('find');
	
	$result = new stdClass();
	
	if ($find != '') {
		switch ($find) {
			case 'kelurahan':
				$query = $app->getStr('query');
				
				$arr = array(
					'query' => $query,
					'suggestions' => array()
				);
				
				//Field harus bernama data dan value
				if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Loket Pelayanan') {
					$sql = "SELECT w.wilID AS id, CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
							FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
							WHERE (w.wilTingkat='Kelurahan' OR w.wilTingkat='Desa') AND (w.wilNama LIKE '".$query."%' OR w.wilNama LIKE '% ".$query."%') AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID 
							ORDER BY w3.wilTingkat, w3.wilNama, w2.wilNama, w.wilNama";
				} else {
					//Kunci ke w3.wilID=7 Kabupaten Pelalawan
					$sql = "SELECT w.wilID AS id, CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
							FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
							WHERE (w.wilTingkat='Kelurahan' OR w.wilTingkat='Desa') AND (w.wilNama LIKE '".$query."%' OR w.wilNama LIKE '% ".$query."%') AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilID=7 AND w3.wilParentID=w4.wilID 
							ORDER BY w3.wilTingkat, w3.wilNama, w2.wilNama, w.wilNama";
				}
				$rs = $app->query($sql);
				if ($rs) {
					while(($obj = mysql_fetch_object($rs)) == true){
						$obj2 = new stdClass();
						$obj2->id = $obj->id;
						$obj2->kode = $obj->kode;
						
						$obj->data = $obj2;
						
						$arr['suggestions'][] = $obj;
					}
				}
				
				echo htmlspecialchars(json_encode($arr), ENT_NOQUOTES);
				break;
			case 'kelurahanAll':
				$query = $app->getStr('query');
				
				$arr = array(
					'query' => $query,
					'suggestions' => array()
				);
				
				$sql = "SELECT w.wilID AS id, CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
						FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
						WHERE (w.wilTingkat='Kelurahan' OR w.wilTingkat='Desa') AND (w.wilNama LIKE '".$query."%' OR w.wilNama LIKE '% ".$query."%') AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID 
						ORDER BY w3.wilTingkat, w3.wilNama, w2.wilNama, w.wilNama";
				$rs = $app->query($sql);
				if ($rs) {
					while(($obj = mysql_fetch_object($rs)) == true){
						$obj2 = new stdClass();
						$obj2->id = $obj->id;
						$obj2->kode = $obj->kode;
						
						$obj->data = $obj2;
						
						$arr['suggestions'][] = $obj;
					}
				}
				
				echo htmlspecialchars(json_encode($arr), ENT_NOQUOTES);
				break;
			case 'kecamatan':
				$query = $app->getStr('query');
				
				$arr = array(
					'query' => $query,
					'suggestions' => array()
				);
				
				//Field harus bernama data dan value
				$sql = "SELECT w.wilID AS id, CONCAT(w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
						FROM wilayah AS w, wilayah AS w2, wilayah AS w3
						WHERE w.wilTingkat='Kecamatan' AND (w.wilNama LIKE '".$query."%' OR w.wilNama LIKE '% ".$query."%') AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID 
						ORDER BY w3.wilTingkat, w3.wilNama, w2.wilNama, w.wilNama";
				$rs = $app->query($sql);
				if ($rs) {
					while(($obj = mysql_fetch_object($rs)) == true){
						$obj2 = new stdClass();
						$obj2->id = $obj->id;
						$obj2->kode = $obj->kode;
						
						$obj->data = $obj2;
						
						$arr['suggestions'][] = $obj;
					}
				}
				
				echo htmlspecialchars(json_encode($arr), ENT_NOQUOTES);
				break;
			case 'klui':
				//Hanya mencari nama dari tabel kblikelompok
				$query = $app->getStr('query');
				
				$arr = array(
					'query' => $query,
					'suggestions' => array()
				);
				
				//Field harus bernama data dan value
				$sql = "SELECT kkelID AS id, kkelKode AS kode, kkelNama AS value
						FROM kbli
						WHERE (kkelNama LIKE '".$query."%' OR kkelNama LIKE '% ".$query."%')
						ORDER BY kkelNama";
				$rs = $app->query($sql);
				if ($rs) {
					while(($obj = mysql_fetch_object($rs)) == true){
						$obj2 = new stdClass();
						$obj2->id = $obj->id;
						$obj2->kode = $obj->kode;
						
						$obj->data = $obj2;
						
						$arr['suggestions'][] = $obj;
					}
				}
				
				echo htmlspecialchars(json_encode($arr), ENT_NOQUOTES);
				break;
			case 'kbli':
				//Mencari kode dan nama dari tabel kblikelompok
				$query = $app->getStr('query');
				
				$arr = array(
					'query' => $query,
					'suggestions' => array()
				);
				
				//Field harus bernama data dan value
				$sql = "SELECT kkelID AS id, kkelKode AS kode, CONCAT(kkelKode,' - ',kkelNama) AS value
						FROM kbli
						WHERE (kkelKode LIKE '".$query."%' OR kkelNama LIKE '".$query."%' OR kkelNama LIKE '% ".$query."%')
						ORDER BY kkelNama";
				$rs = $app->query($sql);
				if ($rs) {
					while(($obj = mysql_fetch_object($rs)) == true){
						$obj2 = new stdClass();
						$obj2->id = $obj->id;
						$obj2->kode = $obj->kode;
						
						$obj->data = $obj2;
						
						$arr['suggestions'][] = $obj;
					}
				}
				
				echo htmlspecialchars(json_encode($arr), ENT_NOQUOTES);
				break;
			case 'kbli2':
				//Mencari kode dan nama dari tabel kblisubgolongan dan kblikelompok
				$query = $app->getStr('query');
				
				$arr = array(
					'query' => $query,
					'suggestions' => array()
				);
				
				//TODO: Modif untuk kpubPublikasi=1
				//Field harus bernama data dan value
				$sql = "SELECT ksubID AS id, ksubKode AS kode, ksubNama AS nama, CONCAT(ksubKode,' - ',ksubNama) AS value
						FROM kblisubgolongan
						WHERE (ksubKode LIKE '".$query."%' OR ksubNama LIKE '".$query."%' OR ksubNama LIKE '% ".$query."%')
						ORDER BY ksubNama";
				$rs = $app->query($sql);
				if ($rs) {
					while(($obj = mysql_fetch_object($rs)) == true){
						$obj2 = new stdClass();
						$obj2->tabel = "subgolongan";
						$obj2->id = $obj->id;
						$obj2->kode = $obj->kode;
						$obj2->nama = $obj->nama;
						
						$obj->data = $obj2;
						
						$arr['suggestions'][] = $obj;
					}
				}
				
				//Field harus bernama data dan value
				$sql = "SELECT kkelID AS id, kkelKode AS kode, kkelNama AS nama, CONCAT(kkelKode,' - ',kkelNama) AS value
						FROM kbli
						WHERE (kkelKode LIKE '".$query."%' OR kkelNama LIKE '".$query."%' OR kkelNama LIKE '% ".$query."%')
						ORDER BY kkelNama";
				$rs = $app->query($sql);
				if ($rs) {
					while(($obj = mysql_fetch_object($rs)) == true){
						$obj2 = new stdClass();
						$obj2->tabel = "kelompok";
						$obj2->id = $obj->id;
						$obj2->kode = $obj->kode;
						$obj2->nama = $obj->nama;
						
						$obj->data = $obj2;
						
						$arr['suggestions'][] = $obj;
					}
				}
				
				echo htmlspecialchars(json_encode($arr), ENT_NOQUOTES);
				break;
			case 'noktp':
				$noktp = $app->getStr('noktp');
				
				//Ambil informasi terakhir
				$objPermohonan = $app->queryObject("SELECT * FROM permohonan WHERE mhnNoKtp='".$noktp."' ORDER BY mhnID DESC LIMIT 1");
				if ($objPermohonan) {
					$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS nama
							FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
							WHERE w.wilID='".$objPermohonan->mhnKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
					$objKelurahan = $app->queryObject($sql);
					
					if ($objKelurahan) {
						$objPermohonan->mhnKelurahanKode = $objKelurahan->kode;
						$objPermohonan->mhnKelurahan = $objKelurahan->nama;
					} else {
						$objPermohonan->mhnKelurahanKode = '';
						$objPermohonan->mhnKelurahan = '';
					}
					$objPermohonan->mhnTglLahir = $app->MySQLDateToNormal($objPermohonan->mhnTglLahir);
					
					$result->obj = $objPermohonan;
					$result->success = true;
				} else {
					$result->success = false;
				}
				echo json_encode($result);
				break;
			case 'nourutlengkap':
				$nourutlengkap = $app->getStr('nourutlengkap');
				
				$objPermohonan = $app->queryObject("SELECT * FROM permohonan WHERE mhnNoUrutLengkap='".$nourutlengkap."'");
				if ($objPermohonan) {
					$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS nama
							FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
							WHERE w.wilID='".$objPermohonan->mhnKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
					$objKelurahan = $app->queryObject($sql);
					
					if ($objKelurahan) {
						$objPermohonan->mhnKelurahanKode = $objKelurahan->kode;
						$objPermohonan->mhnKelurahan = $objKelurahan->nama;
					} else {
						$objPermohonan->mhnKelurahanKode = '';
						$objPermohonan->mhnKelurahan = '';
					}
					$result->obj = $objPermohonan;
					$result->success = true;
				} else {
					$result->success = false;
				}
				echo json_encode($result);
				break;
			case 'perusahaan':
				$query = $app->getStr('query');
				
				$arr = array(
					'query' => $query,
					'suggestions' => array()
				);
				
				//Field harus bernama data dan value
				$sql = "SELECT perID AS data, perNama AS value
						FROM perusahaan
						WHERE perNama LIKE '".$query."%' OR perNama LIKE '% ".$query."%'
						ORDER BY perNama";
				$rs = $app->query($sql);
				if ($rs) {
					while(($obj = mysql_fetch_object($rs)) == true){
						$arr['suggestions'][] = $obj;
					}
				}
				
				echo htmlspecialchars(json_encode($arr), ENT_NOQUOTES);
				break;
			case 'perusahaanid':
				$perid = $app->getStr('perID');
				
				//Ambil informasi
				$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perID='".$perid."'");
				if ($objPerusahaan) {
					$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS nama
							FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
							WHERE w.wilID='".$objPerusahaan->perKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
					$objKelurahan = $app->queryObject($sql);
					if ($objKelurahan) {
						$objPerusahaan->perKelurahanKode = $objKelurahan->kode;
						$objPerusahaan->perKelurahan = $objKelurahan->nama;
					} else {
						$objPerusahaan->perKelurahanKode = '';
						$objPerusahaan->perKelurahan = '';
					}
					$result->obj = $objPerusahaan;
					$result->success = true;
				} else {
					$result->success = false;
				}
				echo json_encode($result);
				break;
			default:
				$result->success = false;
				echo json_encode($result);
		}
	} else {
		$result->success = false;
		echo json_encode($result);
	}
}

function printPermohonan($id) {
	global $app;
	
	$sql = "SELECT * 
			FROM permohonan 
			LEFT JOIN perusahaan ON mhnPerID=perID
			WHERE mhnID='".$id."'";
	$objPermohonan = $app->queryObject($sql);
	if (!$objPermohonan) {
		$app->showPageError();
		exit();
	}
	
	Permohonan_View::printPermohonan($objPermohonan);
}

function readPermohonan($id) {
	global $app;
	
	//Query
	$objPermohonan = $app->queryObject("SELECT * FROM permohonan WHERE mhnID='".$id."'");
	if (!$objPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perID='".$objPermohonan->mhnPerID."'");
	if ($objPerusahaan) {
		$objPermohonan->perNama = $objPerusahaan->perNama;
		$objPermohonan->perAlamat = $objPerusahaan->perAlamat;
		$objPermohonan->perKelurahanID = $objPerusahaan->perKelurahanID;
		$objPermohonan->perNoTelp = $objPerusahaan->perNoTelp;
	} else {
		$objPermohonan->perNama = '';
		$objPermohonan->perAlamat = '';
		$objPermohonan->perKelurahanID = 0;
		$objPermohonan->perNoTelp = '';
	}
	
	Permohonan_View::readPermohonan(true, '', $objPermohonan);
}

function savePermohonan() {
	global $app;
	
	//Check object
	if ($app->getInt('mhnID') > 0) {
		$objPermohonan = $app->queryObject("SELECT * FROM permohonan WHERE mhnID='".$app->getInt('mhnID')."' AND mhnDibuatOleh='".$_SESSION['sesipengguna']->ID."'");
		if (!$objPermohonan) {
			$app->showPageError();
			exit();
		}
		
		$totalBelumAmbil = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnMhnID='".$app->getInt('mhnID')."' AND dmhnTelahAmbil=0"));
		if ($totalBelumAmbil == 0) {
			//Sudah diambil semua atau tidak ada detail permohonan
			$app->showPageError();
			exit();
		}
	}
	
	//Create object
	$objPermohonan = new Permohonan_Model();
	$app->bind($objPermohonan);
	
	//Modify object (if necessary)
	$objPermohonan->mhnTgl = $app->NormalDateToMySQL($objPermohonan->mhnTgl);
	$objPermohonan->mhnTglLahir = $app->NormalDateToMySQL($objPermohonan->mhnTglLahir);
	
	$arrJenisSuratIzin = array();
	//Dibedakan karena pengurusan izin untuk perusahaan harus ada perusahaannya
	$perusahaan = array();
	$perseorangan = array();
	
	if (isset($_REQUEST['jenissuratizin'])) {
		foreach ($_REQUEST['jenissuratizin'] as $k=>$v) {
			if ($v != '') {
				$k2 = explode('-', $k);
				
				$arrJenisSuratIzin[$k2[1]] = $v;
				
				if ($k2[0] == 'Perusahaan') {
					$perusahaan[$k2[1]] = $v;
				} else if ($k2[0] == 'Perseorangan') {
					$perseorangan[$k2[1]] = $v;
				}
			}
		}
	}
	
	$arrPola = array();
	if (isset($_REQUEST['pola'])) {
		$arrPola = $_REQUEST['pola'];
	}
	
	$arrPerluSurvey = array();
	if (isset($_REQUEST['perlusurvey'])) {
		$arrPerluSurvey = $_REQUEST['perlusurvey'];
	}
	
	$arrPerkiraanSelesai = array();
	if (isset($_REQUEST['perkiraanselesai'])) {
		foreach ($_REQUEST['perkiraanselesai'] as $k=>$v) {
			if ($v == '') {
				$arrPerkiraanSelesai[$k] = $v;
			} else {
				$arrPerkiraanSelesai[$k] = $app->NormalDateToMySQL($v);
			}
		}
	}
	
	$arrSyaratSuratIzin = array();
	if (isset($_REQUEST['syaratsuratizin'])) {
		$arrSyaratSuratIzin = $_REQUEST['syaratsuratizin'];
	}
	
	$arrAdministrasiOleh = array();
	if (isset($_REQUEST['administrasioleh'])) {
		$arrAdministrasiOleh = $_REQUEST['administrasioleh'];
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objPermohonan->mhnID > 0) {
		if ($objPermohonan->mhnNoUrutLengkap == '') {
			$doSave = false;
			$msg[] = '- No. Pendaftaran belum diisi';
		} else {
			$isExist = $app->isExist("SELECT mhnID AS id FROM permohonan WHERE mhnNoUrutLengkap='".$objPermohonan->mhnNoUrutLengkap."'", $objPermohonan->mhnID);
			if (!$isExist) {
				$doSave = false;
				$msg[]  = '- No. Pendaftaran "'.$objPermohonan->mhnNoUrutLengkap.'" sudah ada';
			}
			
			if (strlen($objPermohonan->mhnNoUrutLengkap) != 10) {
				$doSave = false;
				$msg[]  = '- No. Pendaftaran harus 10 angka';
			}
			
			if (substr($objPermohonan->mhnNoUrutLengkap,0,6) != substr($objPermohonan->mhnTgl,0,4).substr($objPermohonan->mhnTgl,5,2)) {
				$doSave = false;
				$msg[]  = '- No. Pendaftaran harus diawali dengan "'.substr($objPermohonan->mhnTgl,0,4).substr($objPermohonan->mhnTgl,5,2)."'";
			}
		}
	}
	
	if ($objPermohonan->mhnNoKtp == '') {
		$doSave = false;
		$msg[] = '- No. KTP belum diisi';
	}
	
	if ($objPermohonan->mhnNama == '') {
		$doSave = false;
		$msg[] = '- Nama Pemohon belum diisi';
	}
	
	if ($objPermohonan->mhnAlamat == '') {
		$doSave = false;
		$msg[] = '- Alamat Pemohon belum diisi';
	}
	
	if ($objPermohonan->mhnKelurahanID == 0) {
		$doSave = false;
		$msg[] = '- Kelurahan Pemohon belum dipilih dari daftar yang ada';
	}
	
	if ($objPermohonan->mhnTempatLahir == '') {
		$doSave = false;
		$msg[] = '- Tempat Lahir belum diisi';
	}
	
	if ($objPermohonan->mhnTglLahir == '0000-00-00') {
		$doSave = false;
		$msg[] = '- Tanggal Lahir belum diisi';
	}
	
	if (count($perusahaan) == 0 && count($perseorangan) == 0) {
		$doSave = false;
		$msg[] = '- Surat Izin yang dimohonkan belum dipilih';
	}
	
	if (count($perusahaan) > 0) {
		if ($app->getStr('perNama') == '') {
			$doSave = false;
			$msg[] = '- Nama Perusahaan belum diisi';
		}
		
		if ($app->getStr('mhnPerusahaanAlamat') == '') {
			$doSave = false;
			$msg[] = '- Alamat Perusahaan belum diisi';
		}
		
		if ($app->getInt('mhnPerusahaanKelurahanID') == 0) {
			$doSave = false;
			$msg[] = '- Kelurahan untuk Data Perusahaan belum dipilih dari daftar yang ada';
		}
	}
	
	//Insert or update? Karena digunakan di luar if ($doSave) {}
	$doInsert = ($objPermohonan->mhnID == 0) ? true : false;
	
	//Karena setiap kali menyimpan tidak dilakukan penghapusan seluruh data terkait dari detailpermohonan dan kelengkapandokumen, 
	//maka dibuatkan lookup table untuk kepentingan update dan delete.
	//NOTE: permasalahnnya, form didesain agar maksimal satu pengurusan surat izin per jenis surat izin.
	//		sehingga apabila reklame dan pemecahan imb bisa ditambahkan oleh Administrasi, maka
	//		tidak bisa dihapus oleh Pelayanan, tetapi bisa dihapus oleh Administrasi
	$lookupDetailPermohonan = array();
	$lookupKelengkapanDokumen = array();
	
	$sql = "SELECT *
			FROM detailpermohonan
			LEFT JOIN kelengkapandokumen ON kdDmhnID=dmhnID
			WHERE dmhnMhnID='".$objPermohonan->mhnID."' AND dmhnTambahan=0";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$lookupDetailPermohonan[$obj->dmhnID] = false;
		$lookupKelengkapanDokumen[$obj->kdDmhnID][$obj->kdSsiID] = false;
	}
	
	//Cek permohonan yang sedang diurus perusahaan
	//NOTE: tidak ada pengecekan untuk yang diurus perseorangan
	/*if (count($perusahaan) > 0) {
		$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perNama='".$app->getStr('perNama')."'");
		if ($objPerusahaan) {
			foreach (array_keys($perusahaan) as $k) {
				//Cek apakah sedang mengurus surat izin yang sama dan belum diambil?
				$sql = "SELECT *
						FROM detailpermohonan, jenissuratizin 
						WHERE dmhnMhnID<>'".$objPermohonan->mhnID."' AND dmhnPerID='".$objPerusahaan->perID."' AND dmhnJsiID=jsiID AND jsiID='".$k."' AND dmhnTelahAmbil=0";
				$objPermohonanSebelumnya = $app->queryObject($sql);
				if ($objPermohonanSebelumnya) {
					$doSave = false;
					$msg[] = '- Perusahaan ini sedang mengurus "'.$objPermohonanSebelumnya->jsiNama.($objPermohonanSebelumnya->jsiSubNama != '' ? ' ('.$objPermohonanSebelumnya->jsiSubNama.')' : '').'"';
				}
			}
		}
	}*/
	
	//Query
	if ($doSave) {
		if ($doInsert) {
			//Ambil terakhir
			$objPermohonan->mhnNoUrut = intval($app->queryField("SELECT MAX(mhnNoUrut) AS nomor FROM permohonan WHERE YEAR(mhnTgl)='".substr($objPermohonan->mhnTgl,0,4)."' AND MONTH(mhnTgl)='".substr($objPermohonan->mhnTgl,5,2)."'")) + 1;
			$objPermohonan->mhnNoUrutLengkap = substr($objPermohonan->mhnTgl,0,4).substr($objPermohonan->mhnTgl,5,2).sprintf('%04d', $objPermohonan->mhnNoUrut);
			$objPermohonan->mhnDibuatOleh = $_SESSION['sesipengguna']->ID;
			$objPermohonan->mhnDibuatPada = date('Y-m-d H:i:s');
			
			$sql = $app->createSQLforInsert($objPermohonan);
		} else {
			$objPermohonan->mhnNoUrut = intval(substr($objPermohonan->mhnNoUrutLengkap,6));
			$objPermohonan->mhnNoUrutLengkap = substr($objPermohonan->mhnTgl,0,4).substr($objPermohonan->mhnTgl,5,2).sprintf('%04d', $objPermohonan->mhnNoUrut);
			$objPermohonan->mhnDiubahOleh = $_SESSION['sesipengguna']->ID;
			$objPermohonan->mhnDiubahPada = date('Y-m-d H:i:s');
			
			$arrExcludeField = array('mhnDibuatOleh', 'mhnDibuatPada');
			
			$sql = $app->createSQLforUpdate($objPermohonan, $arrExcludeField);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objPermohonan->mhnID = $app->queryField("SELECT LAST_INSERT_ID() FROM permohonan");
		}
			
		$success = true;
		$msg = 'Permohonan "'.$objPermohonan->mhnNoUrutLengkap.'" berhasil disimpan';
		
		$app->writeLog(EV_INFORMASI, 'permohonan', $objPermohonan->mhnID, $objPermohonan->mhnNoUrutLengkap, $msg);
		
		//Detail permohonan, dengan key adalah jsiID, value adalah dmhnID
		$detailpermohonan = array();
		
		//Data Perusahaan (tidak didasarkan pada $app->getInt('perID') melainkan $app->getInt('perNama'))
		//Update tabel perusahaan (perusahaan dan pemilik)
		$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perNama='".$app->getStr('perNama')."'");
		if ($objPerusahaan) {
			//Perusahaan lama
			$perID = $objPerusahaan->perID;
			
			//NOTE: tidak diperlukan lagi
			/*$sql = "UPDATE perusahaan 
					SET 
						perNama='".$app->getStr('perNama')."', 
						perAlamat='".$app->getStr('perAlamat')."', 
						perKelurahanID='".$app->getInt('perKelurahanID')."', 
						perNoTelp='".$app->getStr('perNoTelp')."',
						perNoKTPPemilik='".$objPermohonan->mhnNoKtp."',
						perNamaPemilik='".$objPermohonan->mhnNama."',
						perKewarganegaraanPemilik='".$objPermohonan->mhnKewarganegaraan."',
						perPekerjaanPemilik='".$objPermohonan->mhnPekerjaan."',
						perTempatLahirPemilik='".$objPermohonan->mhnTempatLahir."',
						perTglLahirPemilik='".$objPermohonan->mhnTglLahir."',
						perAlamatPemilik='".$objPermohonan->mhnAlamat."',
						perNoTelpPemilik='".$objPermohonan->mhnNoTelp."',
						perKelurahanPemilikID='".$objPermohonan->mhnKelurahanID."'
					WHERE perID='".$perID."'";
			$app->query($sql);*/
		} else {
			//Perusahaan baru
			$sql = "INSERT INTO perusahaan (
						perNama, 
						perAlamat, 
						perKelurahanID, 
						perNoTelp, 
						perNoKTPPemilik, 
						perNamaPemilik, 
						perKewarganegaraanPemilik, 
						perPekerjaanPemilik, 
						perTempatLahirPemilik, 
						perTglLahirPemilik, 
						perAlamatPemilik, 
						perNoTelpPemilik, 
						perKelurahanPemilikID
					) VALUES (
						'".$app->getStr('perNama')."', 
						'".$objPermohonan->mhnPerusahaanAlamat."', 
						'".$objPermohonan->mhnPerusahaanKelurahanID."', 
						'".$app->getStr('perNoTelp')."', 
						'".$objPermohonan->mhnNoKtp."', 
						'".$objPermohonan->mhnNama."', 
						'".$objPermohonan->mhnKewarganegaraan."', 
						'".$objPermohonan->mhnPekerjaan."', 
						'".$objPermohonan->mhnTempatLahir."', 
						'".$objPermohonan->mhnTglLahir."', 
						'".$objPermohonan->mhnAlamat."', 
						'".$objPermohonan->mhnNoTelp."', 
						'".$objPermohonan->mhnKelurahanID."'
					)";
			$app->query($sql);
			
			$perID = $app->queryField("SELECT LAST_INSERT_ID() FROM perusahaan");
		}
		
		$administrasioleh = array();
		
		//Jika mengurus surat izin untuk perusahaan
		if (count($perusahaan) > 0) {
			//Update permohonan
			$app->query("UPDATE permohonan SET mhnPerID='".$perID."' WHERE mhnID='".$objPermohonan->mhnID."'");
			$objPermohonan->mhnPerID = $perID;
			
			//Izin Perusahaan
			foreach ($perusahaan as $jsiID=>$tipe) {
				$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$jsiID."'");
				
				if ($objJenisSuratIzin) {
					$pola = $app->getInt2('pola', $jsiID);
					if ($pola == 2) {
						$perlusurvey = $objJenisSuratIzin->jsiPerluSurvey2;
						$tgltargetselesai = $app->NormalDateToMySQL($app->getStr2('perkiraanselesai', $jsiID));
					} else if ($pola == 1) {
						$perlusurvey = $objJenisSuratIzin->jsiPerluSurvey1;
						$tgltargetselesai = $app->NormalDateToMySQL($app->getStr2('perkiraanselesai', $jsiID));
					} else {
						$perlusurvey = 0;
						$tgltargetselesai = '0000-00-00';
					}
					
					$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnMhnID='".$objPermohonan->mhnID."' AND dmhnJsiID='".$jsiID."'");
					if ($objDetailPermohonan) {
						$sql = "UPDATE detailpermohonan
								SET dmhnPerID='".$perID."', dmhnTipe='".$tipe."', dmhnBiayaLeges='".$objJenisSuratIzin->jsiBiayaLeges."', dmhnBiayaAdministrasi='".$objJenisSuratIzin->jsiBiayaAdministrasi."', dmhnPola='".$pola."', dmhnPerluSurvey='".$perlusurvey."', dmhnTglTargetSelesai='".$tgltargetselesai."', dmhnDiadministrasikanOleh='".$arrAdministrasiOleh[$jsiID]."'
								WHERE dmhnID='".$objDetailPermohonan->dmhnID."'";
						$app->query($sql);
						
						$detailpermohonan[$jsiID] = $objDetailPermohonan->dmhnID;
					} else {
						$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh) 
								VALUES ('".$objPermohonan->mhnID."', '".$perID."', '".$jsiID."', '".$tipe."', '".$objJenisSuratIzin->jsiBiayaLeges."', '".$objJenisSuratIzin->jsiBiayaAdministrasi."', '".$pola."', '".$perlusurvey."', '".$tgltargetselesai."', '".$arrAdministrasiOleh[$jsiID]."')";
						$app->query($sql);
						
						$detailpermohonan[$jsiID] = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
					}
					
					$administrasioleh[] = $arrAdministrasiOleh[$jsiID];
					$lookupDetailPermohonan[$detailpermohonan[$jsiID]] = true;
				}
			}
		}
		
		if (count($perseorangan) > 0) {
			//Izin Perseorangan
			foreach ($perseorangan as $jsiID=>$tipe) {
				$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$jsiID."'");
				
				if ($objJenisSuratIzin) {
					$pola = $app->getInt2('pola', $jsiID);
					if ($pola == 2) {
						$perlusurvey = $objJenisSuratIzin->jsiPerluSurvey2;
						$tgltargetselesai = $app->NormalDateToMySQL($app->getStr2('perkiraanselesai', $jsiID));
					} else if ($pola == 1) {
						$perlusurvey = $objJenisSuratIzin->jsiPerluSurvey1;
						$tgltargetselesai = $app->NormalDateToMySQL($app->getStr2('perkiraanselesai', $jsiID));
					} else {
						$perlusurvey = 0;
						$tgltargetselesai = '0000-00-00';
					}
					
					$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnMhnID='".$objPermohonan->mhnID."' AND dmhnJsiID='".$jsiID."'");
					if ($objDetailPermohonan) {
						$sql = "UPDATE detailpermohonan
								SET dmhnPerID='0', dmhnTipe='".$tipe."', dmhnBiayaLeges='".$objJenisSuratIzin->jsiBiayaLeges."', dmhnBiayaAdministrasi='".$objJenisSuratIzin->jsiBiayaAdministrasi."', dmhnPola='".$pola."', dmhnPerluSurvey='".$perlusurvey."', dmhnTglTargetSelesai='".$tgltargetselesai."', dmhnDiadministrasikanOleh='".$arrAdministrasiOleh[$jsiID]."'
								WHERE dmhnID='".$objDetailPermohonan->dmhnID."'";
						$app->query($sql);
						
						$detailpermohonan[$jsiID] = $objDetailPermohonan->dmhnID;
					} else {
						$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh) 
								VALUES ('".$objPermohonan->mhnID."', '0', '".$jsiID."', '".$tipe."', '".$objJenisSuratIzin->jsiBiayaLeges."', '".$objJenisSuratIzin->jsiBiayaAdministrasi."', '".$pola."', '".$perlusurvey."', '".$tgltargetselesai."', '".$arrAdministrasiOleh[$jsiID]."')";
						$app->query($sql);
						
						$detailpermohonan[$jsiID] = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
					}
					
					$administrasioleh[] = $arrAdministrasiOleh[$jsiID];
					$lookupDetailPermohonan[$detailpermohonan[$jsiID]] = true;
				}
			}
		}
		
		if (count($lookupDetailPermohonan) > 0) {
			foreach ($lookupDetailPermohonan as $k=>$v) {
				if (!$v) {
					$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$k."'");
				}
			}
		}
		
		//Syarat surat izin
		if (count($arrSyaratSuratIzin) > 0) {
			foreach ($arrSyaratSuratIzin as $ssiID=>$jsiID) {
				//Hanya bisa disimpan jika surat izin dipilih baru/perubahan/perpanjangan
				if (isset($detailpermohonan[$jsiID])) {
					$objKelengkapanDokumen = $app->queryObject("SELECT * FROM kelengkapandokumen WHERE kdDmhnID='".$detailpermohonan[$jsiID]."' AND kdSsiID='".$ssiID."'");
					if ($objKelengkapanDokumen) {
						$lookupKelengkapanDokumen[$detailpermohonan[$jsiID]][$ssiID] = true;
					} else {
						$app->query("INSERT INTO kelengkapandokumen (kdDmhnID, kdSsiID) VALUES ('".$detailpermohonan[$jsiID]."', '".$ssiID."')");
					}
				}
			}
		}
		
		if (count($lookupKelengkapanDokumen) > 0) {
			foreach ($lookupKelengkapanDokumen as $k=>$v) {
				foreach ($v as $k2=>$v2) {
					if (!$v2) {
						$app->query("DELETE FROM kelengkapandokumen WHERE kdDmhnID='".$k."' AND kdSsiID='".$k2."'");
					}
				}
			}
		}
	} else {
		$success = false;
		if ($objPermohonan->mhnID > 0) {
			$msg = 'Permohonan "'.$objPermohonan->mhnNoUrutLengkap.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
		} else {
			$msg = 'Permohonan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
		}
	}
	
	//PASTIKAN TERBACA!
	if ($success) {
		$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perID='".$objPermohonan->mhnPerID."'");
		if ($objPerusahaan) {
			$objPermohonan->perNama = $objPerusahaan->perNama;
			$objPermohonan->perAlamat = $objPerusahaan->perAlamat;
			$objPermohonan->perKelurahanID = $objPerusahaan->perKelurahanID;
			$objPermohonan->perNoTelp = $objPerusahaan->perNoTelp;
		} else {
			$objPermohonan->perNama = '';
			$objPermohonan->perAlamat = '';
			$objPermohonan->perKelurahanID = 0;
			$objPermohonan->perNoTelp = '';
		}
		
		if ($doInsert) {
			Permohonan_View::readPermohonan($success, $msg, $objPermohonan);
		} else {
			viewPermohonan($success, $msg);
		}
	} else {
		//Data lainnya
		$objPermohonan->jenissuratizin = $arrJenisSuratIzin;
		$objPermohonan->pola = $arrPola;
		$objPermohonan->perlusurvey = $arrPerluSurvey;
		$objPermohonan->perkiraanselesai = $arrPerkiraanSelesai;
		$objPermohonan->syaratsuratizin = $arrSyaratSuratIzin;
		$objPermohonan->administrasioleh = $arrAdministrasiOleh;
		
		$objPermohonan->perID = $app->getInt('perID');
		$objPermohonan->perNama = $app->getStr('perNama');
		$objPermohonan->perAlamat = $app->getStr('perAlamat');
		$objPermohonan->perKelurahanID = $app->getInt('perKelurahanID');
		$objPermohonan->perNoTelp = $app->getStr('perNoTelp');
		
		Permohonan_View::editPermohonan($success, $msg, $objPermohonan);
	}
}

function smsPermohonan($id) {
	global $app;
	
	$obj = $app->queryObject("SELECT * FROM permohonan WHERE mhnID='".$id."'");
	if ($obj) {
		Permohonan_View::smsPermohonan($obj);
	}
}

function viewPermohonan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('permohonan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('permohonan', 'sort', 'mhnNoUrutLengkap');
	$dir   		= $app->pageVar('permohonan', 'dir', 'desc');
	
	//Add filter(s) and it's default value
	$default = array(
		'field' => '',
		'nama' => '',
		'status' => ''
	);
	
	$field 		= $app->pageVar('permohonan', 'filterfield', $default['field'], 'strval');
	$nama 		= $app->pageVar('permohonan', 'filternama', $default['nama'], 'strval');
	$status 	= $app->pageVar('permohonan', 'filterstatus', $default['status'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = $field." LIKE '%".$nama."%'";
	}
	
	switch ($status) {
		case 'Belum Ditentukan Polanya':
			$filter[] = "dmhnDikembalikan=0 AND dmhnPola=0";
			break;
		case 'Belum Survey':
			$filter[] = "dmhnDikembalikan=0 AND dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=0";
			break;
		case 'Ditunda':
			$filter[] = "dmhnDikembalikan=0 AND dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=1 AND dmhnHasilSurvey='Ditunda'";
			break;
		case 'Batal':
			$filter[] = "dmhnDikembalikan=0 AND dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=1 AND dmhnHasilSurvey='Batal'";
			break;
		case 'Belum Selesai':
			$filter[] = "dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=0";
			break;
		case 'Belum Diambil':
			$filter[] = "dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=0";
			break;
		case 'Sudah Diambil':
			$filter[] = "dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=1";
			break;
		case 'Dikembalikan':
			$filter[] = "dmhnDikembalikan=1";
			break;
		default:
	}
	if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Administrasi') {
		$filter[] = "dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."'";
	}
	//NOTE: specific
	//$filter[] = "YEAR(mhnTgl)>=2014";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'subdata' => array(), 
		'filter' => array(
			'field' => $field,
			'nama' => $nama,
			'status' => $status
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
		  		  FROM detailpermohonan
		  		  LEFT JOIN permohonan ON dmhnMhnID=mhnID
		  		  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN pengguna ON mhnDibuatOleh=pengguna.pnID
				  LEFT JOIN perusahaan ON mhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *, pengguna.pnNama AS dibuatoleh, pengguna2.pnNama AS diadministrasikanoleh
		  	FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN pengguna ON mhnDibuatOleh=pengguna.pnID
			LEFT JOIN pengguna AS pengguna2 ON dmhnDiadministrasikanOleh=pengguna2.pnID
			LEFT JOIN perusahaan ON mhnPerID=perID
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][$obj->mhnID] = $obj;
		$arr['subdata'][$obj->mhnID][$obj->dmhnID] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Permohonan_View::viewPermohonan($success, $msg, $arr);
}
?>