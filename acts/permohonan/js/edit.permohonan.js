//Variables
var form = $("#myForm");
var mhnNoKtp = $("#mhnNoKtp");
var mhnNama = $("#mhnNama");
var mhnAlamat = $("#mhnAlamat");
var mhnKelurahan = $("#mhnKelurahan");
var mhnTempatLahir = $("#mhnTempatLahir");
var mhnTglLahir = $("#mhnTglLahir");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'mhnNoKtp' || this.id === undefined) {
		if (mhnNoKtp.val().length == 0) {
			doSubmit = false;
			mhnNoKtp.addClass("error");		
		} else {
			mhnNoKtp.removeClass("error");
		}
	}
	
	if (this.id == 'mhnNama' || this.id === undefined) {
		if (mhnNama.val().length == 0) {
			doSubmit = false;
			mhnNama.addClass("error");		
		} else {
			mhnNama.removeClass("error");
		}
	}
	
	if (this.id == 'mhnAlamat' || this.id === undefined) {
		if (mhnAlamat.val().length == 0) {
			doSubmit = false;
			mhnAlamat.addClass("error");		
		} else {
			mhnAlamat.removeClass("error");
		}
	}
	
	if (this.id == 'mhnKelurahan' || this.id === undefined) {
		if (mhnKelurahan.val().length == 0) {
			doSubmit = false;
			mhnKelurahan.addClass("error");		
		} else {
			mhnKelurahan.removeClass("error");
		}
	}
	
	if (this.id == 'mhnTempatLahir' || this.id === undefined) {
		if (mhnTempatLahir.val().length == 0) {
			doSubmit = false;
			mhnTempatLahir.addClass("error");		
		} else {
			mhnTempatLahir.removeClass("error");
		}
	}
	
	if (this.id == 'mhnTglLahir' || this.id === undefined) {
		if (mhnTglLahir.val().length == 0) {
			doSubmit = false;
			mhnTglLahir.addClass("error");		
		} else {
			mhnTglLahir.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

mhnNoKtp.on("keyup", function(){
	if (mhnNoKtp.val().length > 12) {
		$.getJSON(
			CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=noktp',  
			{ noktp: mhnNoKtp.val() },
			function(json) { 
				if (json.success) {
					var obj = json.obj;
					$('#mhnNama').val(obj.mhnNama);
					$('#mhnAlamat').val(obj.mhnAlamat);
					$('#mhnKelurahanID').val(obj.mhnKelurahanID);
					$('#mhnKelurahanKode').val(obj.mhnKelurahanKode);
					$('#mhnKelurahan').val(obj.mhnKelurahan);
					$('#mhnTempatLahir').val(obj.mhnTempatLahir);
					if (obj.mhnTglLahir != '00-00-0000') {
						$('#mhnTglLahir').val(obj.mhnTglLahir);
					} else {
						$('#mhnTglLahir').val('');
					}
					$('#mhnKewarganegaraan').val(obj.mhnKewarganegaraan);
					$('#mhnPekerjaan').val(obj.mhnPekerjaan);
					$('#mhnNoTelp').val(obj.mhnNoTelp);
				} else {
					/*$('#mhnNama').val('');
					$('#mhnAlamat').val('');
					$('#mhnKelurahanID').val(0);
					$('#mhnKelurahanKode').val('');
					$('#mhnKelurahan').val('');
					$('#mhnTempatLahir').val('');
					$('#mhnTglLahir').val('');
					$('#mhnKewarganegaraan').val('Indonesia');
					$('#mhnPekerjaan').val('');
					$('#mhnNoTelp').val('');*/
				}
			}  
		);  
	}
});

$(".selectJenis").on("change", function(){
	var id = $(this).attr('id');
	var idSyarat = '.trSyarat\\[' + id.substring(15,id.length-1) + '\\]';
	var idJenis = '#trJenis\\[' + id.substring(15,id.length-1) + '\\]';
	
	if ($(this).val() == '') {
		$(idSyarat).hide();
		$(idJenis).removeClass('selected');
	} else {
		$(idSyarat).show();
		$(idJenis).addClass('selected');
	}
});

$(".selectPola").on("change", function(){
	var id = $(this).attr('id');
	var id2 = id.substring(5,id.length-1);
	
	if ($(this).val() == 1) {
		/*Pola 1*/
		$('#perlusurvey\\[' + id2 + '\\]').val($('#perlusurvey1\\[' + id2 + '\\]').val());
		$('#perkiraanselesai\\[' + id2 + '\\]').val($('#perkiraanselesai1\\[' + id2 + '\\]').val());
		$('#perkiraanselesai\\[' + id2 + '\\]').show();
		
		if ($('#perlusurvey1\\[' + id2 + '\\]').val() == 1) {
			$('#teksperkiraanselesai\\[' + id2 + '\\]').html('dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Perkiraan selesai ');
		} else {
			$('#teksperkiraanselesai\\[' + id2 + '\\]').html('dengan perkiraan selesai');
		} 
		$('#teksperkiraanselesai\\[' + id2 + '\\]').show();
	} else if ($(this).val() == 2) {
		/*Pola 2*/
		$('#perlusurvey\\[' + id2 + '\\]').val($('#perlusurvey2\\[' + id2 + '\\]').val());
		$('#perkiraanselesai\\[' + id2 + '\\]').val($('#perkiraanselesai2\\[' + id2 + '\\]').val());
		$('#perkiraanselesai\\[' + id2 + '\\]').show();
		
		if ($('#perlusurvey2\\[' + id2 + '\\]').val() == 1) {
			$('#teksperkiraanselesai\\[' + id2 + '\\]').html('dengan lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima. Perkiraan selesai ');
		} else {
			$('#teksperkiraanselesai\\[' + id2 + '\\]').html('dengan perkiraan selesai');
		} 
		$('#teksperkiraanselesai\\[' + id2 + '\\]').show();
	} else {
		/*Belum Ditentukan*/
		$('#perlusurvey\\[' + id2 + '\\]').val(0);
		$('#perkiraanselesai\\[' + id2 + '\\]').hide();
		$('#teksperkiraanselesai\\[' + id2 + '\\]').hide();
	}
});

mhnNoKtp.blur(validateForm);
mhnNama.blur(validateForm);
mhnAlamat.blur(validateForm);
mhnKelurahan.blur(validateForm);
mhnTempatLahir.blur(validateForm);
mhnTglLahir.blur(validateForm);

mhnNoKtp.keyup(validateForm);
mhnNama.keyup(validateForm);
mhnAlamat.keyup(validateForm);
mhnKelurahan.keyup(validateForm);
mhnTempatLahir.keyup(validateForm);
mhnTglLahir.keyup(validateForm);

form.submit(submitForm);

$('#mhnKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#mhnKelurahanID').val(suggestion.data.id);
		$('#mhnKelurahanKode').val(suggestion.data.kode);
		$('#mhnKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#perNama').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=perusahaan',
	onSelect: function(suggestion){
		$('#perID').val(suggestion.data);
		$('#perNama').val(suggestion.value);
		
		$.getJSON(
			CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=perusahaanid',  
			{ perID: suggestion.data },
			function(json) { 
				if (json.success) {
					var obj = json.obj;
					$('#perAlamat').val(obj.perAlamat);
					$('#perKelurahanID').val(obj.perKelurahanID);
					$('#perKelurahanKode').val(obj.perKelurahanKode);
					$('#perKelurahan').val(obj.perKelurahan);
					$('#perNoTelp').val(obj.perNoTelp);
				}
			}  
		);  
	}
});

$('#mhnPerusahaanKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#mhnPerusahaanKelurahanID').val(suggestion.data.id);
		$('#mhnPerusahaanKelurahanKode').val(suggestion.data.kode);
		$('#mhnPerusahaanKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#mhnLokasiUsahaKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#mhnLokasiUsahaKelurahanID').val(suggestion.data.id);
		$('#mhnLokasiUsahaKelurahanKode').val(suggestion.data.kode);
		$('#mhnLokasiUsahaKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

//Set focus
mhnNoKtp.focus();