//Functions
function hapus(id, nama) {
	if (confirm("Hapus No. Pendaftaran '" + nama + "'?")) {
		window.open('index2.php?act=permohonan&task=delete&id=' + id, '_self');
	}
}

function hapusdetail(act,id) {
	if (confirm("Hapus?")) {
		window.open('index2.php?act=' + act + '&task=delete&id=' + id, '_self');
	}
}

function lihat(page) {
	var url = 'index2.php?act=permohonan&page=' + page;
	url += '&filterfield=' + $('#filterfield').val();
	url += '&filternama=' + $('#filternama').val();
	url += '&filterstatus=' + $('#filterstatus').val();
	
	window.open(url, '_self');
}

function sort(field, direction) {
	var url = 'index2.php?act=permohonan';
	url += '&sort=' + field;
	url += '&dir=' + direction;
	
	window.open(url, '_self');
}

//Events
$("#page").change(function() {
	lihat(this.value);
});

$('#filternama').keypress(function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
		lihat(1);
	}
});

$("#find").click(function() {
	lihat(1);
});

$("#cancel").click(function() {
	$('#filterfield').val('mhnNoUrutLengkap');
	$('#filternama').val('');
	$('#filterstatus').val('');
	lihat(1);
});

//Set focus
$("#btnTambah").focus();