<?php
class Permohonan_Model {	
	public $tabel = 'permohonan';
	public $primaryKey = 'mhnID';
	
	public $mhnID = 0;
	public $mhnLokID = 0;
	public $mhnPerID = 0;	//Digunakan untuk nilai default, lainnya mengambil dari mhnPerusahaanAlamat, atau jika ada mhnLokasiUsaha
	public $mhnPerusahaanAlamat = '';
	public $mhnPerusahaanKelurahanID = 0;
	public $mhnLokasiUsahaAlamat = '';
	public $mhnLokasiUsahaKelurahanID = 0;
	public $mhnTgl = '0000-00-00';
	public $mhnNoUrut = 0;
	public $mhnNoUrutLengkap = 0;
	public $mhnNoKtp = '';
	public $mhnNama = '';
	public $mhnAlamat = '';
	public $mhnKelurahanID = 0;
	public $mhnTempatLahir = '';
	public $mhnTglLahir = '0000-00-00';
	public $mhnKewarganegaraan = 'Indonesia';
	public $mhnPekerjaan = '';
	public $mhnNoTelp = '';
	public $mhnDibuatOleh = 0;
	public $mhnDibuatPada = '0000-00-00 00:00:00';
	public $mhnDiubahOleh = 0;
	public $mhnDiubahPada = '0000-00-00 00:00:00';
	//TODO: cek field2 berikut
	/*public $mhnAdministrasiOleh = 0;	//Oleh Petugas Administrasi
	public $mhnAdministrasiStatus = 'Diproses';	//Diproses, Dikembalikan
	public $mhnDikembalikanAlasan = '';
	public $mhnDikembalikanNo = 0;
	public $mhnDikembalikanTgl = '0000-00-00';
	public $mhnDikembalikanMengetahui = 0;*/
}
?>