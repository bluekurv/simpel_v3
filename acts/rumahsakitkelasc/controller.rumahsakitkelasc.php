<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'rumahsakitkelasc';
$childTitle = 'Izin Operasional Rumah Sakit Kelas C';
$childKodeSurat = 'IOP-RSK-C';
$childTembusan = 'Kepala Dinas Kesehatan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>