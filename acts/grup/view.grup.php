<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Grup_View {
	static function editGrup($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=grup&task=edit&id=<?php echo $obj->grupID; ?>"><img src="images/icons/flag_red.png" border="0" /> <?php echo ($obj->grupID > 0) ? 'Ubah' : 'Tambah'; ?> Grup Surat Izin</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=grup&task=save" method="POST" >
				<input type="hidden" id="grupID" name="grupID" value="<?php echo $obj->grupID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=grup">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Berada di</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('grupParentID', $obj->grupParentID);
?>
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="grupNama" name="grupNama" maxlength="255" size="50" value="<?php echo $obj->grupNama; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/grup/js/edit.grup.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewGrup($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=grup"><img src="images/icons/flag_red.png" border="0" /> Grup Surat Izin</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=grup&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<p><i>Petunjuk: maksimal 3 tingkat</i></p>
			<table class="dataTable">
			<thead>
			<tr>
				<th width="40">Aksi</th>
				<th>Nama</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr[0]) > 0) {
			$i = 1;
			foreach ($arr[0] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=grup&task=edit&id='.$v->grupID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->grupID.', '."'".$v->grupNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=grup&task=edit&id='.$v->grupID.'">'.$v->grupNama.'</a></td>';
				echo "</tr>\n";
				$i++;
				
				if (isset($arr[$v->grupID])) {
					foreach ($arr[$v->grupID] as $v2) {
						echo ($i%2) ? '<tr>' : '<tr class="odd">';
						echo '<td><a href="index2.php?act=grup&task=edit&id='.$v2->grupID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v2->grupID.', '."'".$v2->grupNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
						echo '<td>--- <a href="index2.php?act=grup&task=edit&id='.$v2->grupID.'">'.$v2->grupNama.'</a></td>';
						echo "</tr>\n";
						$i++;
						
						if (isset($arr[$v2->grupID])) {
							foreach ($arr[$v2->grupID] as $v3) {
								echo ($i%2) ? '<tr>' : '<tr class="odd">';
								echo '<td><a href="index2.php?act=grup&task=edit&id='.$v3->grupID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v3->grupID.', '."'".$v3->grupNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
								echo '<td>--- --- <a href="index2.php?act=grup&task=edit&id='.$v3->grupID.'">'.$v3->grupNama.'</a></td>';
								echo "</tr>\n";
								$i++;
							}
						}
					}
				}
			}
		} else {
?>
			<tr><td colspan="3">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/grup/js/grup.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>