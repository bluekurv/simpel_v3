<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.grup.php';
require_once 'view.grup.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editGrup($app->id);
		break;
	case 'delete':
		deleteGrup($app->id);
		break;
	case 'save':
		saveGrup();
		break;
	default:
		viewGrup(true, '');
		break;
}

function deleteGrup($id) {
	global $app;
	
	//Get object
	$objGrup = $app->queryObject("SELECT * FROM grup WHERE grupID='".$id."'");
	if (!$objGrup) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalSubGrup = intval($app->queryField("SELECT COUNT(*) AS total FROM grup WHERE grupParentID='".$id."'"));
	if ($totalSubGrup > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalSubGrup.' grup di bawah grup tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM grup WHERE grupID='".$id."'");
		
		$success = true;
		$msg = '"'.$objGrup->grupNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'grup', $objGrup->grupID, $objGrup->grupNama, $msg);
	} else {
		$success = false;
		$msg = '"'.$objGrup->grupNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewGrup($success, $msg);
}

function editGrup($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objGrup = $app->queryObject("SELECT * FROM grup WHERE grupID='".$id."'");
		if (!$objGrup) {
			$app->showPageError();
			exit();
		}
	} else {
		$objGrup = new Grup_Model();
	}
	
	Grup_View::editGrup(true, '', $objGrup);
}

function saveGrup() {
	global $app;
	
	//Create object
	$objGrup = new Grup_Model();
	$app->bind($objGrup);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objGrup->grupNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objGrup->grupID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objGrup);
		} else {
			$sql = $app->createSQLforUpdate($objGrup);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objGrup->grupID = $app->queryField("SELECT LAST_INSERT_ID() FROM grup");
			
			$success = true;
			$msg = '"'.$objGrup->grupNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'grup', $objGrup->grupID, $objGrup->grupNama, $msg);
		} else {
			$success = true;
			$msg = '"'.$objGrup->grupNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'grup', $objGrup->grupID, $objGrup->grupNama, $msg);
		}
	} else {
		$success = false;
		$msg = '"'.$objGrup->grupNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewGrup($success, $msg);
	} else {
		Grup_View::editGrup($success, $msg, $objGrup);
	}
}

function viewGrup($success, $msg) {
	global $app;
	
	$arr = array();
	
	//Query
	$sql = "SELECT * FROM grup GROUP BY grupParentID, grupNama";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr[$obj->grupParentID][] = $obj;
	}
	
	Grup_View::viewGrup($success, $msg, $arr);
}
?>