//Variables
var form = $("#myForm");
var grupParentID = $("#grupParentID");
var grupNama = $("#grupNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'grupParentID' || this.id === undefined) {
		if (grupParentID.val == 0){
			doSubmit = false;
			grupParentID.addClass("error");		
		} else {
			grupParentID.removeClass("error");
		}
	}
	
	if (this.id == 'grupNama' || this.id === undefined) {
		if (grupNama.val().length == 0){
			doSubmit = false;
			grupNama.addClass("error");		
		} else {
			grupNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
grupParentID.blur(validateForm);
grupNama.blur(validateForm);

grupParentID.keyup(validateForm);
grupNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
grupParentID.focus();