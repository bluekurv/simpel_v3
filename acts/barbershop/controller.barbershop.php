<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'barbershop';
$childTitle = 'Izin Operasional Barber Shop';
$childKodeSurat = 'IOP-BS';
$childTembusan = 'Kepala Dinas Pendapatan Daerah Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>