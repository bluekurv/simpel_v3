<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'peternakan';
$childTitle = 'Izin Operasional Peternakan';
$childKodeSurat = 'IOP-P';
$childTembusan = 'Kepala Dinas Peternakan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>