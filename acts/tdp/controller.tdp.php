<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.tdp.php';
require_once 'view.tdp.php';

switch ($app->task) {
	case 'add':
		addTdp($app->id);
		break;
	case 'edit':
		editTdp($app->id);
		break;
	case 'delete':
		deleteTdp($app->id);
		break;
	case 'print':
		printTdp($app->id);
		break;
	case 'read':
		readTdp($app->id);
		break;
	case 'save':
		saveTdp();
		break;
	default:
		viewTdp(true, '');
		break;
}

function addTdp($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editTdp($id);
}

function editTdp($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objTdp = $app->queryObject("SELECT * FROM tdp WHERE tdpDmhnID='".$id."'");
	if (!$objTdp) {
		$objTdp = new Tdp_Model();
		
		//Ambil nilai default
		$objTdp->tdpPendaftaran = $objDetailPermohonan->dmhnTipe;
		
		$objTdp->tdpAlamat = $objDetailPermohonan->perAlamat;
		$objTdp->tdpKelurahanID = $objDetailPermohonan->perKelurahanID;
		
		$objTdp->tdpNoTelp = $objDetailPermohonan->perNoTelp;
		$objTdp->tdpNoFax = $objDetailPermohonan->perNoFax;
		$objTdp->tdpKegiatanUsahaPokok = $objDetailPermohonan->perKegiatanUsahaPokok;
		$objTdp->tdpNPWP = $objDetailPermohonan->perNPWP;
		$objTdp->tdpBentukBadan = $objDetailPermohonan->perBentukBadan;
		$objTdp->tdpStatus = $objDetailPermohonan->perStatus;
		$objTdp->tdpKluiTabel = $objDetailPermohonan->perKluiTabel;
		$objTdp->tdpKluiID = $objDetailPermohonan->perKluiID;
		
		//Ambil tdp sebelumnya
		$sql = "SELECT * 
				FROM tdp, detailpermohonan 
				WHERE tdpDmhnID=dmhnID AND dmhnPerID='".$objDetailPermohonan->perID."'";
		$objTdpSebelumnya = $app->queryObject($sql);
		if ($objTdpSebelumnya) {
			$objTdp->tdpNo = $objTdpSebelumnya->tdpNo;
		}
	} else {
		if ($objTdp->tdpAlamat == '') {
			$objTdp->tdpAlamat = $objDetailPermohonan->perAlamat;
		}
		if ($objTdp->tdpKelurahanID == 0) {
			$objTdp->tdpKelurahanID = $objDetailPermohonan->perKelurahanID;
		}
	}
	
	$objTdp->tdpDmhnID = $objDetailPermohonan->dmhnID;
	
	Tdp_View::editTdp(true, '', $objDetailPermohonan, $objTdp);
}

function deleteTdp($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM tdp WHERE tdpDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Tanda Daftar Perusahaan berhasil dihapus';
		
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Tanda Daftar Perusahaan', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function printTdp($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN tdp ON dmhnID=tdpDmhnID
			WHERE dmhnID='".$id."'";
	$objTdp = $app->queryObject($sql);
	if (!$objTdp) {
		$app->showPageError();
		exit();
	}
	
	if ($objTdp->tdpAlamat == '') {
		$objTdp->tdpAlamat = $objTdp->perAlamat;
	}
	if ($objTdp->tdpKelurahanID == 0) {
		$objTdp->tdpKelurahanID = $objTdp->perKelurahanID;
	}

	/*if ($objTdp->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	Tdp_View::printTdp($objTdp);
}

function readTdp($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN tdp ON dmhnID=tdpDmhnID
			WHERE dmhnID='".$id."'";
	$objTdp = $app->queryObject($sql);
	if (!$objTdp) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objTdp->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Tdp_View::readTdp($objTdp);
}

function saveTdp() {
	global $app;
	
	//Get object
	$objTdpSebelumnya = $app->queryObject("SELECT * FROM tdp WHERE tdpID='".$app->getInt('tdpID')."'");
	if (!$objTdpSebelumnya) {
		$objTdpSebelumnya = new Tdp_Model();
	}
	
	//Create object
	$objTdp = new Tdp_Model();
	$app->bind($objTdp);
	
	if ($objTdp->tdpKluiTabel != '') {
		switch ($objTdp->tdpKluiTabel) {
			case 'golongan':
				$sql = "SELECT kgolKode AS kode, kgolNama AS value 
						FROM kbligolongan
						WHERE kgolID='".$objTdp->tdpKluiID."'";
				break;
			case 'subgolongan':
				$sql = "SELECT ksubKode AS kode, ksubNama AS value 
						FROM kblisubgolongan
						WHERE ksubID='".$objTdp->tdpKluiID."'";
				break;
			case 'kelompok':
				$sql = "SELECT kkelKode AS kode, kkelNama AS value 
						FROM kblikelompok
						WHERE kkelID='".$objTdp->tdpKluiID."'";
				break;
		}
		$perKlui = $app->queryObject($sql);
		if ($perKlui) {
			$objTdp->tdpKluiKodePendek = substr($perKlui->kode,0,2);
		} else {
			$objTdp->tdpKluiKodePendek = '00';
		}
	} else {
		$objTdp->tdpKluiKodePendek = '00';
	}
	
	//Modify object (if necessary)
	$objTdp->tdpTglPengesahan = $app->NormalDateToMySQL($objTdp->tdpTglPengesahan);
	if ($objTdp->tdpTglPengesahan != '0000-00-00') {
		if ($objTdpSebelumnya->tdpNo == ''){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			if ($objTdp->tdpNo == '') {
				//Jika ada nomor
				$objTdp->tdpNo = intval($app->queryField("SELECT MAX(tdpNo) AS nomor FROM tdp WHERE tdpBentukBadan='".$objTdp->tdpBentukBadan."'")) + 1;
			} else {
				//Jika tidak ada nomor
			}
		}
		
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objTdp->tdpTglBerlaku = date('Y-m-d', strtotime($objTdp->tdpTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objTdp->tdpTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objTdp->tdpTglDaftarUlang = date('Y-m-d', strtotime($objTdp->tdpTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objTdp->tdpTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objTdp->tdpNo = '';
		
		$objTdp->tdpTglBerlaku = '0000-00-00';
		$objTdp->tdpTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objTdp->tdpNoLengkap = '0416'.$app->queryField("SELECT bperNilai FROM bentukperusahaan WHERE bperID='".$objTdp->tdpBentukBadan."'").$objTdp->tdpKluiKodePendek.sprintf('%05d', $objTdp->tdpNo);
		
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	$isExist = $app->isExist("SELECT tdpID AS id FROM tdp WHERE tdpNoLengkap='".$objTdp->tdpNoLengkap."'", $objTdp->tdpID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nomor Surat "'.$objTdp->tdpNoLengkap.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objTdp->tdpID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objTdp);
		} else {
			$sql = $app->createSQLforUpdate($objTdp);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objTdp->tdpID = $app->queryField("SELECT LAST_INSERT_ID() FROM tdp");
		}
		
		$success = true;
		$msg = 'Tanda Daftar Perusahaan berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'tdp', $objTdp->tdpID, $objTdp->tdpNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objTdp->tdpDmhnID."'");
		
		//Update data perusahaan
		$app->query("UPDATE perusahaan 
					 SET 
					 	 perNoTelp='".$objTdp->tdpNoTelp."', 
					 	 perNoFax='".$objTdp->tdpNoFax."', 
					 	 perKegiatanUsahaPokok='".$objTdp->tdpKegiatanUsahaPokok."', 
					 	 perNPWP='".$objTdp->tdpNPWP."', 
					 	 perBentukBadan='".$objTdp->tdpBentukBadan."', 
					 	 perStatus='".$objTdp->tdpStatus."', 
					 	 perKluiTabel='".$objTdp->tdpKluiTabel."', 
					 	 perKluiID='".$objTdp->tdpKluiID."' 
					 WHERE perID='".$app->getInt('perID')."'");
	} else {
		$success = false;
		$msg = 'Tanda Daftar Perusahaan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objTdp->tdpDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Tdp_View::editTdp($success, $msg, $objDetailPermohonan, $objTdp);
	}
}

function viewTdp($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('tdp', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('tdp', 'sort', 'tdpTglPengesahan');
	//$dir		= $app->pageVar('tdp', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'bentukperusahaan' => -1,
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('tdp', 'filternama', $default['nama'], 'strval');
	$bentukperusahaan 		= $app->pageVar('tdp', 'filterbentukperusahaan', $default['bentukperusahaan'], 'intval');
	$tahun 		= $app->pageVar('tdp', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(tdpNoLengkap LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($bentukperusahaan > $default['bentukperusahaan']) {
		$filter[] = "substring(tdpNoLengkap,5,1)='".$bentukperusahaan."'";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(tdpTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'bentukperusahaan' => $bentukperusahaan,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM tdp
				  LEFT JOIN detailpermohonan ON tdpDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM tdp
			LEFT JOIN detailpermohonan ON tdpDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(tdpTglPengesahan) DESC, tdpNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Tdp_View::viewTdp($success, $msg, $arr);
}
?>