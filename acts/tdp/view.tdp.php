<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Tdp_View {
	static function editTdp($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<link rel="stylesheet" type="text/css" href="js/jquery.treeview/jquery.treeview.css">
	<script type="text/javascript" src="js/jquery.treeview/jquery.treeview.js"></script>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdp&task=edit&id=<?php echo $obj->tdpDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Tanda Daftar Perusahaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=tdp&task=save" method="POST" >
				<input type="hidden" id="tdpID" name="tdpID" value="<?php echo $obj->tdpID; ?>" />
				<input type="hidden" id="tdpDmhnID" name="tdpDmhnID" value="<?php echo $obj->tdpDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" name="save" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" name="save" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->tdpImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('tdpGrupID', $obj->tdpGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=tdp&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=tdp&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdpAlamat" name="tdpAlamat" maxlength="500" size="100" value="<?php echo $obj->tdpAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
				WHERE w.wilID='".$obj->tdpKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$tdpKelurahan = $app->queryObject($sql);
		if (!$tdpKelurahan) {
			$tdpKelurahan = new stdClass();
			$tdpKelurahan->kode = '';
			$tdpKelurahan->value = '';
		}
?>
						<input type="hidden" id="tdpKelurahanID" name="tdpKelurahanID" value="<?php echo $obj->tdpKelurahanID; ?>" />
						<input class="box readonly" id="tdpKelurahanKode" name="tdpKelurahanKode" size="8" value="<?php echo $tdpKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="tdpKelurahan" name="tdpKelurahan" maxlength="500" size="70" value="<?php echo $tdpKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdpNoTelp" name="tdpNoTelp" maxlength="255" size="50" value="<?php echo $obj->tdpNoTelp; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Fax</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdpNoFax" name="tdpNoFax" maxlength="50" size="50" value="<?php echo $obj->tdpNoFax; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdpNPWP" name="tdpNPWP" maxlength="50" size="50" value="<?php echo $obj->tdpNPWP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bentuk Badan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		$app->createSelect('tdpBentukBadan', "SELECT bperID AS id, CONCAT(bperKode,' - ',bperNama) AS nama FROM bentukperusahaan ORDER BY bperKode, bperNama", $obj->tdpBentukBadan, array(0=>''));
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Status Perusahaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="tdpStatus" name="tdpStatus">
<?php 
		foreach (array('Tunggal', 'Pusat', 'Cabang', 'Pembantu', 'Perwakilan') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdpStatus) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kegiatan Usaha Pokok</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdpKegiatanUsahaPokok" name="tdpKegiatanUsahaPokok" maxlength="255" size="100" value="<?php echo $obj->tdpKegiatanUsahaPokok; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">KBLI</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdpKluiTabel != '') {
			switch ($obj->tdpKluiTabel) {
				case 'golongan':
					$sql = "SELECT kgolKode AS kode, kgolNama AS value 
							FROM kbligolongan
							WHERE kgolID='".$obj->tdpKluiID."'";
					break;
				case 'subgolongan':
					$sql = "SELECT ksubKode AS kode, ksubNama AS value 
							FROM kblisubgolongan
							WHERE ksubID='".$obj->tdpKluiID."'";
					break;
				case 'kelompok':
					$sql = "SELECT kkelKode AS kode, kkelNama AS value 
							FROM kblikelompok
							WHERE kkelID='".$obj->tdpKluiID."'";
					break;
			}
			$perKlui = $app->queryObject($sql);
			if ($perKlui) {
				$perKlui->tabel = $obj->tdpKluiTabel;
			} else {
				$perKlui = new stdClass();
				$perKlui->kode = '';
				$perKlui->value = '';
				$perKlui->tabel = '';
			}
		} else {
			$perKlui = new stdClass();
			$perKlui->kode = '';
			$perKlui->value = '';
			$perKlui->tabel = '';
		}
?>
						<input type="hidden" id="tdpKluiID" name="tdpKluiID" value="<?php echo $obj->tdpKluiID; ?>" />
						<input type="hidden" id="tdpKluiTabel" name="tdpKluiTabel" value="<?php echo $perKlui->tabel; ?>" />
						<input class="box readonly" id="tdpKluiKode" name="tdpKluiKode" size="8" value="<?php echo $perKlui->kode; ?>" tabindex="-1" readonly />
						<input class="box readonly" id="tdpKlui" name="tdpKlui" maxlength="500" size="100" value="<?php echo $perKlui->value; ?>" tabindex="-1" readonly />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
						<input class="box" id="txtBidangUsaha" name="txtBidangUsaha" maxlength="500" size="70" value="" />
					<td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan kode pada textbox di atas ini atau silahkan telusuri daftar di bawah ini, kemudian klik kode yang diinginkan</i>)</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
<?php 
		$arr = array(
			'golonganpokok' => array(),
			'golongan' => array(),
			'subgolongan' => array(),
			'kelompok' => array()
		);
		
		//Query
		$sql = "SELECT *, kblipublikasi.kpubID AS id FROM kblipublikasi WHERE kblipublikasi.kpubPublikasi=1 ORDER BY kpubNama";
		$arr['publikasi'] = $app->queryArrayOfObjects($sql);
		
		//Query
		$sql = "SELECT *, kblikategori.kkatID AS id, kblikategori.kkatKpubID AS parentid FROM kblikategori ORDER BY kkatKode, kkatNama";
		$arr['kategori'] = $app->queryArrayOfObjects($sql, 2);
		
		//Query
		$sql = "SELECT *, kbligolonganpokok.kgpokID AS id, kbligolonganpokok.kgpokKkatID AS parentid FROM kbligolonganpokok ORDER BY kgpokKode, kgpokNama";
		$arr['golonganpokok'] = $app->queryArrayOfObjects($sql, 2);
		
		//Query
		$sql = "SELECT *, kbligolongan.kgolID AS id, kbligolongan.kgolKgpokID AS parentid FROM kbligolongan ORDER BY kgolKode, kgolNama";
		$arr['golongan'] = $app->queryArrayOfObjects($sql, 2);
		
		//Query
		$sql = "SELECT *, kblisubgolongan.ksubID AS id, kblisubgolongan.ksubKgolID AS parentid FROM kblisubgolongan ORDER BY ksubKode, ksubNama";
		$arr['subgolongan'] = $app->queryArrayOfObjects($sql, 2);
		
		//Query
		$sql = "SELECT *, kblikelompok.kkelID AS id, kblikelompok.kkelKsubID AS parentid FROM kblikelompok ORDER BY kkelKode, kkelNama";
		$arr['kelompok'] = $app->queryArrayOfObjects($sql, 2);
		
		if (count($arr['publikasi']) > 0) {
?>
						<ul id="browser" class="filetree treeview">
<?php 
			foreach ($arr['publikasi'] as $pub) {
?>			
							<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $pub->kpubNama; ?></span>
								<ul>
<?php 
				if (isset($arr['kategori'][$pub->kpubID])) {
					foreach ($arr['kategori'][$pub->kpubID] as $kat) {
?>
									<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $kat->kkatKode.' - '. $kat->kkatNama; ?></span>
										<ul>
<?php 
						if (isset($arr['golonganpokok'][$kat->kkatID])) {
							foreach ($arr['golonganpokok'][$kat->kkatID] as $gpok) {
?>
											<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $gpok->kgpokKode.' - '. $gpok->kgpokNama; ?></span>
												<ul>
<?php 
								if (isset($arr['golongan'][$gpok->kgpokID])) {
									foreach ($arr['golongan'][$gpok->kgpokID] as $gol) {
?>
													<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><a href="javascript:pilihKlui('golongan', <?php echo $gol->kgolID; ?>, '<?php echo $gol->kgolKode; ?>', '<?php echo $gol->kgolNama; ?>')"><?php echo $gol->kgolKode; ?></a> - <?php echo $gol->kgolNama; ?></span>
														<ul>
<?php 
										if (isset($arr['subgolongan'][$gol->kgolID])) {
											foreach ($arr['subgolongan'][$gol->kgolID] as $sub) {
?>
															<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><a href="javascript:pilihKlui('subgolongan', <?php echo $sub->ksubID; ?>, '<?php echo $sub->ksubKode; ?>', '<?php echo $sub->ksubNama; ?>')"><?php echo $sub->ksubKode; ?></a> - <?php echo $sub->ksubNama; ?></span>
																<ul>
<?php 
												if (isset($arr['kelompok'][$sub->ksubID])) {
													foreach ($arr['kelompok'][$sub->ksubID] as $kel) {
?>
																	<li><span class="file"><a href="javascript:pilihKlui('kelompok', <?php echo $kel->kkelID; ?>, '<?php echo $kel->kkelKode; ?>', '<?php echo $kel->kkelNama; ?>')"><?php echo $kel->kkelKode; ?></a> - <?php echo $kel->kkelNama; ?></span></li>
<?php
													}
												}
?>
																</ul>
															</li>
<?php
											}
										}
?>
														</ul>
													</li>
<?php
									}
								}
?>										
												</ul>
											</li>
<?php
							}
						}
?>
										</ul>
									</li>
<?php
					}
				}
?>
								</ul>
							</li>
<?php 
			}
?>
						</ul>
<?php 
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdpKeterangan" name="tdpKeterangan" maxlength="255" size="50" value="<?php echo $obj->tdpKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="tdpPendaftaran" name="tdpPendaftaran">
<?php 
		foreach (array('Baru', 'Perubahan', 'Perpanjangan', 'Duplikat') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdpPendaftaran) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pembaharuan ke-</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdpPembaharuan" name="tdpPembaharuan" maxlength="5" size="5" value="<?php echo $obj->tdpPembaharuan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdpNo != '') {	//NOTE: Selain tdp, bernilai > 0
						//24-06-2014
						/*<span>0416<?php echo $app->queryField("SELECT bperNilai FROM bentukperusahaan WHERE bperID='".$objDetailPermohonan->perBentukBadan."'").$obj->tdpKluiKodePendek; ?></span>
						<input class="box" id="tdpNo" name="tdpNo" maxlength="5" size="5" value="<?php echo sprintf('%05d', $obj->tdpNo); ?>" />*/
?>
						<span>0416<?php echo $app->queryField("SELECT bperNilai FROM bentukperusahaan WHERE bperID='".$obj->tdpBentukBadan."'").$obj->tdpKluiKodePendek; ?></span>
						<input class="box" id="tdpNo" name="tdpNo" maxlength="5" size="5" value="<?php echo sprintf('%05d', $obj->tdpNo); ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="tdpNo" name="tdpNo" value="<?php echo $obj->tdpNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="tdpTglPengesahan" name="tdpTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdpTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT pnID AS id, pnNama AS nama 
				FROM pengguna 
				WHERE pnLevelAkses='Pejabat' 
				ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
		$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="tdpPejID" name="tdpPejID">
<?php 
		while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
			echo '<option value="'.$objPengesahanOleh->id.'"';
			if ($objPengesahanOleh->id == $obj->tdpPejID) {
				echo ' selected';	
			}
			echo '>'.$objPengesahanOleh->nama.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='tdp'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->tdpTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdpTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="tdpTglBerlaku" name="tdpTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->tdpTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdpTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdpTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="tdpTglDaftarUlang" name="tdpTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->tdpTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/tdp/js/edit.tdp.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printTdp($obj) {
		global $app;
		
		$objReport 		= new Report('Tanda Daftar Perusahaan', 'TDP-'.$obj->nomor.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->tdpPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<p align="center">
		<span style="font-size:16px;"><b>TANDA DAFTAR PERUSAHAAN</b></span><br>
		<span style="font-size:12px;"><b><?php echo strtoupper($app->queryField("SELECT bperNama FROM bentukperusahaan WHERE bperID='".$obj->tdpBentukBadan."'")); ?></b></span><br><br>
		<span style="font-size:12px;"><b>BERDASARKAN<br>UNDANG-UNDANG NOMOR 3 TAHUN 1982<br>TENTANG WAJIB DAFTAR PERUSAHAAN</b></span>
	</p>
	
	<table border="1" cellpadding="10" cellspacing="0" width="100%">
	<tr>
		<td width="33%" align="center">
			<b>NOMOR TDP<br><?php echo $obj->tdpNoLengkap; ?></b>
		</td>
		<td width="34%" align="center">
			<b>BERLAKU S/D TANGGAL<br><?php echo strtoupper($app->MySQLDateToIndonesia($obj->tdpTglBerlaku)); ?></b>
		</td>
		<td width="33%" align="center">
			<b>PENDAFTARAN <?php echo strtoupper($obj->tdpPendaftaran); ?><br>PEMBAHARUAN KE <?php echo $obj->tdpPembaharuan; ?></b>
		</td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="1" cellpadding="10" cellspacing="0" width="100%">
	<tr>
		<td width="67%">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="140"><b>NAMA PERUSAHAAN</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (0.67 * $objReport->width) - 140 - 10 - (2 * 10); ?>"><b><?php echo strtoupper($obj->perNama); ?></b></td>
			</tr>
			</table>
		</td>
		<td width="33%">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="45"><b>STATUS</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (0.33 * $objReport->width) - 45 - 10 - (2 * 10); ?>"><b><?php echo strtoupper($obj->tdpStatus); ?></b></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="140"><b>NAMA PENGURUS / PENANGGUNG JAWAB</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (1 * $objReport->width) - 140 - 10 - (2 * 10); ?>"><b><?php echo strtoupper($obj->mhnNama); ?></b></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
<?php 
		$sql = "SELECT CONCAT(IF(wilayah.wilTingkat='Kelurahan','Kel. ','Desa '),wilayah.wilNama,', Kec. ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->tdpKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
?>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="140"><b>ALAMAT PERUSAHAAN</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (1 * $objReport->width) - 140 - 10 - (2 * 10); ?>"><b><?php echo strtoupper($obj->tdpAlamat.', '.$app->queryField($sql)); ?></b></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="140"><b>NPWP</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (0.67 * $objReport->width) - 140 - 10 - (2 * 10); ?>"><b><?php echo ($obj->tdpNPWP != '') ? strtoupper($obj->tdpNPWP) : '-'; ?></b></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="140"><b>NOMOR TELEPON</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (0.67 * $objReport->width) - 140 - 10 - (2 * 10); ?>"><b><?php echo ($obj->tdpNoTelp != '') ? strtoupper($obj->tdpNoTelp) : '-'; ?></b></td>
			</tr>
			</table>
		</td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="45"><b>FAX</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (0.33 * $objReport->width) - 45 - 10 - (2 * 10); ?>"><b><?php echo ($obj->tdpNoFax != '') ? strtoupper($obj->tdpNoFax) : '-'; ?></b></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="140"><b>KEGIATAN USAHA POKOK</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (0.67 * $objReport->width) - 140 - 10 - (2 * 10); ?>"><b><?php echo ($obj->tdpKegiatanUsahaPokok != '') ? strtoupper($obj->tdpKegiatanUsahaPokok) : '-'; ?></b></td>
			</tr>
			</table>
		</td>
		<td>
<?php 
		if ($obj->tdpKluiTabel != '') {
			switch ($obj->tdpKluiTabel) {
				case 'golongan':
					$sql = "SELECT kgolKode AS kode, kgolNama AS value 
							FROM kbligolongan
							WHERE kgolID='".$obj->tdpKluiID."'";
					break;
				case 'subgolongan':
					$sql = "SELECT ksubKode AS kode, ksubNama AS value 
							FROM kblisubgolongan
							WHERE ksubID='".$obj->tdpKluiID."'";
					break;
				case 'kelompok':
					$sql = "SELECT kkelKode AS kode, kkelNama AS value 
							FROM kblikelompok
							WHERE kkelID='".$obj->tdpKluiID."'";
					break;
			}
			$tdpKlui = $app->queryObject($sql);
			if ($tdpKlui) {
				$tdpKlui->tabel = $obj->tdpKluiTabel;
			} else {
				$tdpKlui = new stdClass();
				$tdpKlui->kode = '';
				$tdpKlui->value = '';
				$perKlui->tabel = '';
			}
		} else {
			$perKlui = new stdClass();
			$perKlui->kode = '';
			$perKlui->value = '';
			$perKlui->tabel = '';
		}
?>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="45"><b>KBLI</b></td>
				<td width="10"><b>:</b></td>
				<td width="<?php echo (0.33 * $objReport->width) - 45 - 10 - (2 * 10); ?>"><b><?php echo $tdpKlui->kode; ?></b></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="48%">&nbsp;</td>
		<td width="52%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->tdpTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
?>
	<style>
		li {
			text-align: justify;
		}
	</style>
	
	<p><b>Perhatian</b><p>
	<ol>
		<li>Tanda Daftar Perusahaan Wajib dipasang di tempat yang mudah dilihat oleh umum.</li>
		<li>Nomor Tanda Daftar Perusahaan Wajib dicantumkan pada papan nama perusahaan dan dokumen-dokumen yang dipergunakan dalam kegiatan usaha.</li>
		<li>Apabila Tanda Daftar Perusahaan Hilang atau rusak, wajib mengajukan permintaan tertulis kepada kantor pendaftaran perusahaan untuk memperoleh pengantinya dalam waktu 3 (tiga) bulan setelah kehilangan atau rusak.</li>
		<li>Setiap perubahan atas hal-hal yang didaftarkan wajib dilaporkan kepada kantor pendaftaran perusahaan dengan menyebutkan alasan-alasannya, dalam waktu dalam waktu 3 (tiga) bulan setelah terjadinya perubahan.</li>
		<li>Daftar Perusahaan dihapus apabila terjadi hal-hal sebagai berikut : 
			<ol type="a">
				<li>Pembubaran bentuk perusahaan ; atau</li>
				<li>Pembubaran Perusahaan ; atau</li>
				<li>Perusahaan menghentikan segala kegiatan usahanya ; atau</li>
				<li>Perusahaan berhenti akibat Akta Pendiriannya kadaluwarsa atau berakhir ; atau</li>
				<li>Perusahaan menghentikan kegiatannya / bubar berdasarkan putusan pengadilan negeri yang telah mempuyai kekuatan hukum tetap.</li>
			</ol>
		</li>
		<li>Tanda Daftar Perusahaan Berlaku untuk jangka waktu 5 (lima) tahun sejak diterbitkan dan wajib didaftar ulang selambat-lambatnya 3 (tiga) bulan sebelum masa berlakunya berakhir.</li>
	</ol>
	
	<p>&nbsp;</p>
	
	<p><b>Ketentuan Pidana</b><p>
	<ol>
		<li>Barang siapa yang menurut undang-undang ini dan atau peraturan pelaksananya diwajibkan mendaftarkan perusahaannya dalam daftar perusahaan yang dengan sengaja atau karena kelalaiannya tidak memenuhi kewajibannya diancam dengan pidana penjara selama-lamanya 3 (tiga) bulan atau pidana denda setinggi-tingginya Rp. 3.000.000,- (tiga juta rupiah). Tindak pidana tersebut merupakan kejahatan. (pasal 32) UU-WDP.</li>
		<li>Barang siapa melakukan atau menyuruh melakukan pendaftaran secara keliru atau tidak lengkap dalam daftar perusahaan diancam dengan pidana kurungan selama-lamanya 3 (tiga) bulan atau pidana denda setinggi-tingginya Rp. 1.500.000,- (satu juta lima ratus ribu rupiah). Tindak pidana tersebut merupakan pelanggaran (pasal 33) UU-WDP.</li>
		<li>Barang siapa tidak memenuhi kewajibannya menurut undang-undang ini dan atau peraturan pelaksananya untuk menghadap atau menolak atau menyerahkan atau mengajukan sesuatu persyaratan dan atau keterangan lain untuk keperluan pendaftaran dalam daftar perusahaan diancam dengan pidana kurungan selama-lamanya 2 (dua) bulan atau pidana denda setinggi-tingginya Rp. 1.000.000,- (satu juta rupiah). Tindak pidana tersebut merupakan pelanggaran (pasal 34) UU-WDP.</li>
	</ol>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readTdp($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdp&task=read&id=<?php echo $obj->tdpDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Tanda Daftar Perusahaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=tdp&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=tdp&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpNoTelp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Fax</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpNoFax; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">NPWP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpNPWP; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bentuk Badan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT bperNama FROM bentukperusahaan WHERE bperID='".$obj->tdpBentukBadan."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Status Perusahaan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpStatus; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kegiatan Usaha Pokok</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpKegiatanUsahaPokok; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">KBLI</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdpKluiTabel != '') {
			switch ($obj->tdpKluiTabel) {
				case 'golongan':
					$sql = "SELECT kgolKode AS kode, kgolNama AS value 
							FROM kbligolongan
							WHERE kgolID='".$obj->tdpKluiID."'";
					break;
				case 'subgolongan':
					$sql = "SELECT ksubKode AS kode, ksubNama AS value 
							FROM kblisubgolongan
							WHERE ksubID='".$obj->tdpKluiID."'";
					break;
				case 'kelompok':
					$sql = "SELECT kkelKode AS kode, kkelNama AS value 
							FROM kblikelompok
							WHERE kkelID='".$obj->tdpKluiID."'";
					break;
			}
			$tdpKlui = $app->queryObject($sql);
			if ($tdpKlui) {
				$tdpKlui->tabel = $obj->tdpKluiTabel;
			} else {
				$tdpKlui = new stdClass();
				$tdpKlui->kode = '';
				$tdpKlui->value = '';
			$perKlui->tabel = '';
			}
		} else {
			$perKlui = new stdClass();
			$perKlui->kode = '';
			$perKlui->value = '';
			$perKlui->tabel = '';
		}
		echo $tdpKlui->kode.' - '.$tdpKlui->value;
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpPendaftaran; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pembaharuan ke-</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpPembaharuan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdpNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->tdpTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->tdpPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdpTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdpTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdpTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdpTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/tdp/js/read.tdp.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewTdp($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdp"><img src="images/icons/rosette.png" border="0" /> Buku Register Tanda Daftar Perusahaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
<?php
		$app->createSelect('filterbentukperusahaan', "SELECT bperNilai AS id, CONCAT(bperNilai,' - ',bperNama,' (',bperKode,')') AS nama FROM bentukperusahaan ORDER BY bperNilai, bperNama", $arr['filter']['bentukperusahaan'], array(-1=>'Seluruhnya'));
?>
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['bentukperusahaan'] != $arr['default']['bentukperusahaan'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('tdpNo', 'Nomor Surat', true),
			$app->setHeader('tdpTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('tdpTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('tdpTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=tdp&task=read&id='.$v->tdpDmhnID.'&fromact=tdp" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->tdpDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				
				echo '<td><a href="index2.php?act=tdp&task=read&id='.$v->tdpDmhnID.'&fromact=tdp" title="Lihat Surat Izin">'.$v->tdpNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->tdpTglPengesahan).'</td>';
				
				if ($v->tdpTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->tdpTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->tdpTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->tdpTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/tdp/js/tdp.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>