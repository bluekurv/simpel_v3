<?php
class Tdp_Model {	
	public $tabel = 'tdp';
	public $primaryKey = 'tdpID';
	
	public $tdpID = 0;
	public $tdpDmhnID = 0;
	public $tdpGrupID = 0;
	public $tdpNo = '';
	public $tdpNoLengkap = '';
	public $tdpPendaftaran = '';
	public $tdpPembaharuan = 0;
	public $tdpAlamat = '';
	public $tdpKelurahanID = 0;
	public $tdpNoTelp = '';
	public $tdpNoFax = '';
	public $tdpKegiatanUsahaPokok = '';
	public $tdpNPWP = '';
	public $tdpBentukBadan = 0;
	public $tdpStatus = '';
	public $tdpKluiTabel = '';
	public $tdpKluiKodePendek = '';
	public $tdpKluiID = 0;
	public $tdpKeterangan = '';
	public $tdpTglBerlaku = '0000-00-00';
	public $tdpTglDaftarUlang = '0000-00-00';
	public $tdpPejID = 0;
	public $tdpTglPengesahan = '0000-00-00'; 	 	 	 	 	
	public $tdpArsip = '';
}
?>