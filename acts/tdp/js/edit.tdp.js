//Objects
$("#browser").treeview();

//Variables
var form = $("#myForm");

//Functions
function pilihKlui(tabel, id, kode, nama) {
	$("#tdpKluiID").val(id);
	$("#tdpKluiTabel").val(tabel);
	$("#tdpKluiKode").val(kode);
	$("#tdpKlui").val(nama);
}

function validateForm() {
	var doSubmit = true;
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

form.submit(submitForm);

$('#tdpKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#tdpKelurahanID').val(suggestion.data.id);
		$('#tdpKelurahanKode').val(suggestion.data.kode);
		$('#tdpKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#txtBidangUsaha').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kbli2',
	onSelect: function(suggestion){
		pilihKlui(suggestion.data.tabel, suggestion.data.id, suggestion.data.kode, suggestion.data.nama);
	}
});

//Set focus
$("#tdpNoTelp").focus();