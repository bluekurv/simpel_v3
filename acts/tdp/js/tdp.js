//Functions
function lihat(page) {
	var url = 'index2.php?act=tdp&page=' + page;
	url += '&filternama=' + $('#filternama').val();
	url += '&filterbentukperusahaan=' + $('#filterbentukperusahaan').val();
	url += '&filtertahun=' + $('#filtertahun').val();
	
	window.open(url, '_self');
}

function sort(field, direction) {
	var url = 'index2.php?act=tdp';
	url += '&sort=' + field;
	url += '&dir=' + direction;
	
	window.open(url, '_self');
}

//Events
$("#page").change(function() {
	lihat(this.value);
});

$('#filternama').keypress(function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
		lihat(1);
	}
});

$("#find").click(function() {
	lihat(1);
});

$("#cancel").click(function() {
	$('#filternama').val('');
	$('#filterbentukperusahaan').val(-1);
	$('#filtertahun').val(0);
	lihat(1);
});

//Set focus
$("#btnTambah").focus();