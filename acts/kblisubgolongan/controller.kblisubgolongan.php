<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kblisubgolongan.php';
require_once 'view.kblisubgolongan.php';
require_once CFG_ABSOLUTE_PATH.'/acts/kbli/view.kbli.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKBLISubGolongan($app->id);
		break;
	case 'delete':
		deleteKBLISubGolongan($app->id);
		break;
	case 'save':
		saveKBLISubGolongan();
		break;
	default:
		viewKBLISubGolongan(true, '');
		break;
}

function deleteKBLISubGolongan($id) {
	global $app;
	
	//Get object
	$objKBLISubGolongan = $app->queryObject("SELECT * FROM kblisubgolongan WHERE ksubID='".$id."'");
	if (!$objKBLISubGolongan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalKBLKelompok = intval($app->queryField("SELECT COUNT(*) AS total FROM kblikelompok WHERE kkelKsubID='".$id."'"));
	if ($totalKBLKelompok > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLKelompok.' kelompok untuk sub golongan tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM kblisubgolongan WHERE ksubID='".$id."'");
		
		$success = true;
		$msg = 'Sub Golongan Lapangan Usaha "'.$objKBLISubGolongan->ksubNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'kblisubgolongan', $objKBLISubGolongan->ksubID, $objKBLISubGolongan->ksubNama, $msg);
	} else {
		$success = false;
		$msg = 'Sub Golongan Lapangan Usaha "'.$objKBLISubGolongan->ksubNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKBLISubGolongan($success, $msg);
}

function editKBLISubGolongan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKBLISubGolongan = $app->queryObject("SELECT * FROM kblisubgolongan WHERE ksubID='".$id."'");
		if (!$objKBLISubGolongan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKBLISubGolongan = new KBLISubGolongan_Model();
		
		$objKBLISubGolongan->ksubKgolID = $_SESSION['kblisubgolongan-filterparent'];
		$objKBLISubGolongan->ksubKpubID = intval($app->queryField("SELECT ksubKpubID FROM kblisubgolongan WHERE ksubID='".$_SESSION['kblisubgolongan-filterparent']."'"));
	}
	
	KBLISubGolongan_View::editKBLISubGolongan(true, '', $objKBLISubGolongan);
}

function saveKBLISubGolongan() {
	global $app;
	
	//Create object
	$objKBLISubGolongan = new KBLISubGolongan_Model();
	$app->bind($objKBLISubGolongan);
	
	//Modify object (if necessary)
	if (isset($_REQUEST['ksubTampil'])) {
		$objKBLISubGolongan->ksubTampil = ($_REQUEST['ksubTampil'] == 'on') ? 1 : 0;
	} else {
		$objKBLISubGolongan->ksubTampil = 0;
	}
	//TODO: specific
	//NOTE: karena sudah mysql_escape_string sebelumnya, maka bukan "\r\n" tetapi '\r\n'
	$objKBLISubGolongan->ksubNama = str_replace(array('\r\n', '\n', '\r'), ' ', ucwords(strtolower($objKBLISubGolongan->ksubNama)));
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKBLISubGolongan->ksubKgolID == 0) {
		$doSave = false;
		$msg[] = '- Kategori belum diisi';
	}
	
	if ($objKBLISubGolongan->ksubKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKBLISubGolongan->ksubNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objKBLISubGolongan->ksubKgolID == 0) {
		$doSave = false;
		$msg[] = '- Publikasi belum diisi';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKBLISubGolongan->ksubID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKBLISubGolongan);
		} else {
			$sql = $app->createSQLforUpdate($objKBLISubGolongan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKBLISubGolongan->ksubID = $app->queryField("SELECT LAST_INSERT_ID() FROM kblisubgolongan");
			
			$success = true;
			$msg = 'Sub Golongan Lapangan Usaha "'.$objKBLISubGolongan->ksubNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'kblisubgolongan', $objKBLISubGolongan->ksubID, $objKBLISubGolongan->ksubNama, $msg);
		} else {
			$success = true;
			$msg = 'Sub Golongan Lapangan Usaha "'.$objKBLISubGolongan->ksubNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'kblisubgolongan', $objKBLISubGolongan->ksubID, $objKBLISubGolongan->ksubNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Sub Golongan Lapangan Usaha "'.$objKBLISubGolongan->ksubNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKBLISubGolongan($success, $msg);
	} else {
		KBLISubGolongan_View::editKBLISubGolongan($success, $msg, $objKBLISubGolongan);
	}
}

function viewKBLISubGolongan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kblisubgolongan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kblisubgolongan', 'sort', 'ksubKode');
	$dir   		= $app->pageVar('kblisubgolongan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'parent' => 0
	);
	
	$nama 		= $app->pageVar('kblisubgolongan', 'filternama', $default['nama'], 'strval');
	$parent		= $app->pageVar('kblisubgolongan', 'filterparent', $default['parent'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(ksubKode LIKE '%".$nama."%' OR ksubNama LIKE '%".$nama."%')";
	}
	if ($parent != $default['parent']) {
		$filter[] = "ksubKgolID='".$parent."'";
	}
	$filter[] = "ksubKgolID=kgolID AND ksubKpubID=kpubID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'parent' => $parent
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM kblisubgolongan, kbligolongan, kblipublikasi
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM kblisubgolongan, kbligolongan, kblipublikasi
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	KBLISubGolongan_View::viewKBLISubGolongan($success, $msg, $arr);
}
?>