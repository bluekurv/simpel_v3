//Variables
var form = $("#myForm");
var ksubKgolID = $("#ksubKgolID");
var ksubKode = $("#ksubKode");
var ksubNama = $("#ksubNama");
var ksubKpubID = $("#ksubKpubID");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'ksubKgolID' || this.id === undefined) {
		if (ksubKgolID.val() == 0){
			doSubmit = false;
			ksubKgolID.addClass("error");		
		} else {
			ksubKgolID.removeClass("error");
		}
	}
	
	if (this.id == 'ksubKode' || this.id === undefined) {
		if (ksubKode.val().length == 0){
			doSubmit = false;
			ksubKode.addClass("error");		
		} else {
			ksubKode.removeClass("error");
		}
	}
	
	if (this.id == 'ksubNama' || this.id === undefined) {
		if (ksubNama.val().length == 0){
			doSubmit = false;
			ksubNama.addClass("error");		
		} else {
			ksubNama.removeClass("error");
		}
	}
	
	if (this.id == 'ksubKpubID' || this.id === undefined) {
		if (ksubKpubID.val() == 0){
			doSubmit = false;
			ksubKpubID.addClass("error");		
		} else {
			ksubKpubID.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
ksubKgolID.blur(validateForm);
ksubKode.blur(validateForm);
ksubNama.blur(validateForm);
ksubKpubID.blur(validateForm);

ksubKgolID.keyup(validateForm);
ksubKode.keyup(validateForm);
ksubNama.keyup(validateForm);
ksubKpubID.keyup(validateForm);

form.submit(submitForm);

//Set focus
ksubKgolID.focus();