<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.jeniskendaraan.php';
require_once 'view.jeniskendaraan.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editJenisKendaraan($app->id);
		break;
	case 'delete':
		deleteJenisKendaraan($app->id);
		break;
	case 'save':
		saveJenisKendaraan();
		break;
	default:
		viewJenisKendaraan(true, '');
		break;
}

function deleteJenisKendaraan($id) {
	global $app;
	
	//Get object
	$objJenisKendaraan = $app->queryObject("SELECT * FROM jeniskendaraan WHERE jkenID='".$id."'");
	if (!$objJenisKendaraan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM jeniskendaraan WHERE jkenID='".$id."'");
		
		$success = true;
		$msg = 'Jenis Kendaraan "'.$objJenisKendaraan->jkenNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'jeniskendaraan', $objJenisKendaraan->jkenID, $objJenisKendaraan->jkenNama, $msg);
	} else {
		$success = false;
		$msg = 'Jenis Kendaraan "'.$objJenisKendaraan->jkenNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewJenisKendaraan($success, $msg);
}

function editJenisKendaraan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objJenisKendaraan = $app->queryObject("SELECT * FROM jeniskendaraan WHERE jkenID='".$id."'");
		if (!$objJenisKendaraan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objJenisKendaraan = new JenisKendaraan_Model();
	}
	
	JenisKendaraan_View::editJenisKendaraan(true, '', $objJenisKendaraan);
}

function saveJenisKendaraan() {
	global $app;
	
	//Create object
	$objJenisKendaraan = new JenisKendaraan_Model();
	$app->bind($objJenisKendaraan);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objJenisKendaraan->jkenKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objJenisKendaraan->jkenNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT jkenID AS id FROM jeniskendaraan WHERE jkenKode='".$objJenisKendaraan->jkenKode."'", $objJenisKendaraan->jkenID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objJenisKendaraan->jkenKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT jkenID AS id FROM jeniskendaraan WHERE jkenNama='".$objJenisKendaraan->jkenNama."'", $objJenisKendaraan->jkenID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objJenisKendaraan->jkenNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objJenisKendaraan->jkenID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objJenisKendaraan);
		} else {
			$sql = $app->createSQLforUpdate($objJenisKendaraan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objJenisKendaraan->jkenID = $app->queryField("SELECT LAST_INSERT_ID() FROM jeniskendaraan");
			
			$success = true;
			$msg = 'Jenis Kendaraan "'.$objJenisKendaraan->jkenNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'jeniskendaraan', $objJenisKendaraan->jkenID, $objJenisKendaraan->jkenNama, $msg);
		} else {
			$success = true;
			$msg = 'Jenis Kendaraan "'.$objJenisKendaraan->jkenNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'jeniskendaraan', $objJenisKendaraan->jkenID, $objJenisKendaraan->jkenNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Jenis Kendaraan "'.$objJenisKendaraan->jkenNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewJenisKendaraan($success, $msg);
	} else {
		JenisKendaraan_View::editJenisKendaraan($success, $msg, $objJenisKendaraan);
	}
}

function viewJenisKendaraan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('jeniskendaraan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('jeniskendaraan', 'sort', 'jkenKode');
	$dir   		= $app->pageVar('jeniskendaraan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('jeniskendaraan', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(jkenKode LIKE '%".$nama."%' OR jkenNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM jeniskendaraan
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM jeniskendaraan
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	JenisKendaraan_View::viewJenisKendaraan($success, $msg, $arr);
}
?>