//Variables
var form = $("#myForm");
var jkenKode = $("#jkenKode");
var jkenNama = $("#jkenNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'jkenKode' || this.id === undefined) {
		if (jkenKode.val().length == 0){
			doSubmit = false;
			jkenKode.addClass("error");		
		} else {
			jkenKode.removeClass("error");
		}
	}
	
	if (this.id == 'jkenNama' || this.id === undefined) {
		if (jkenNama.val().length == 0){
			doSubmit = false;
			jkenNama.addClass("error");		
		} else {
			jkenNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
jkenKode.blur(validateForm);
jkenNama.blur(validateForm);

jkenKode.keyup(validateForm);
jkenNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
jkenKode.focus();