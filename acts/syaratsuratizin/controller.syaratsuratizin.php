<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.syaratsuratizin.php';
require_once 'view.syaratsuratizin.php';

switch ($app->task) {
	case 'add':
		addSyaratSuratIzin($app->id);
		break;
	case 'edit':
		editSyaratSuratIzin($app->id);
		break;
	case 'delete':
		deleteSyaratSuratIzin($app->id);
		break;
	case 'down':
		downSyaratSuratIzin($app->id);
		break;
	case 'up':
		upSyaratSuratIzin($app->id);
		break;
	case 'save':
		saveSyaratSuratIzin();
		break;
	default:
		viewSyaratSuratIzin(true, '', $app->id);
		break;
}

function addSyaratSuratIzin($id) {
	global $app;
	
	//Get object
	$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$id."'");
	if (!$objJenisSuratIzin) {
		$app->showPageError();
		exit();
	}

	$objSyaratSuratIzin = new SyaratSuratIzin_Model();
	$objSyaratSuratIzin->ssiJsiID = $id;
	
	SyaratSuratIzin_View::editSyaratSuratIzin(true, '', $objSyaratSuratIzin, $objJenisSuratIzin);
}

function deleteSyaratSuratIzin($id) {
	global $app;
	
	//Get object
	$objSyaratSuratIzin = $app->queryObject("SELECT * FROM syaratsuratizin WHERE ssiID='".$id."'");
	if (!$objSyaratSuratIzin) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM syaratsuratizin WHERE ssiID='".$id."'");
		
		$success = true;
		$msg = 'Syarat "'.$objSyaratSuratIzin->ssiNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'syaratsuratizin', $objSyaratSuratIzin->ssiID, $objSyaratSuratIzin->ssiNama, $msg);
	} else {
		$success = false;
		$msg = 'Syarat "'.$objSyaratSuratIzin->ssiNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewSyaratSuratIzin($success, $msg, $objSyaratSuratIzin->ssiJsiID);
}

function downSyaratSuratIzin($id) {
	global $app;
	
	//Get object
	$objSyaratSuratIzin = $app->queryObject("SELECT * FROM syaratsuratizin WHERE ssiID='".$id."'");
	if (!$objSyaratSuratIzin) {
		$app->showPageError();
		exit();
	}
	
	$max = intval($app->queryField("SELECT MAX(ssiNo) FROM syaratsuratizin WHERE ssijsiID='".$objSyaratSuratIzin->ssiJsiID."'"));
	
	if ($objSyaratSuratIzin->ssiNo == $max) {
		$app->showPageError();
		exit();
	}
	
	$obj2 = $app->queryObject("SELECT * FROM syaratsuratizin WHERE ssiNo='".($objSyaratSuratIzin->ssiNo + 1)."' AND ssiJsiID='".$objSyaratSuratIzin->ssiJsiID."'");
	if ($obj2) {
		$app->query("UPDATE syaratsuratizin SET ssiNo='".$obj2->ssiNo."' WHERE ssiID='".$objSyaratSuratIzin->ssiID."'");
		$app->query("UPDATE syaratsuratizin SET ssiNo='".$objSyaratSuratIzin->ssiNo."' WHERE ssiID='".$obj2->ssiID."'");
	} else {
		$app->query("UPDATE syaratsuratizin SET ssiNo='".($objSyaratSuratIzin->ssiNo + 1)."' WHERE ssiID='".$objSyaratSuratIzin->ssiID."'");
	}
	
	$success = true;
	$msg = 'Syarat "'.$objSyaratSuratIzin->ssiNama.'" berhasil digeser ke bawah';
	
	viewSyaratSuratIzin($success, $msg, $objSyaratSuratIzin->ssiJsiID);
}

function upSyaratSuratIzin($id) {
	global $app;
	
	//Get object
	$objSyaratSuratIzin = $app->queryObject("SELECT * FROM syaratsuratizin WHERE ssiID='".$id."'");
	if (!$objSyaratSuratIzin) {
		$app->showPageError();
		exit();
	}
	
	$min = 1;
	
	if ($objSyaratSuratIzin->ssiNo == $min) {
		$app->showPageError();
		exit();
	}
	
	$obj2 = $app->queryObject("SELECT * FROM syaratsuratizin WHERE ssiNo='".($objSyaratSuratIzin->ssiNo - 1)."' AND ssiJsiID='".$objSyaratSuratIzin->ssiJsiID."'");
	if ($obj2) {
		$app->query("UPDATE syaratsuratizin SET ssiNo='".$obj2->ssiNo."' WHERE ssiID='".$objSyaratSuratIzin->ssiID."'");
		$app->query("UPDATE syaratsuratizin SET ssiNo='".$objSyaratSuratIzin->ssiNo."' WHERE ssiID='".$obj2->ssiID."'");
	} else {
		$app->query("UPDATE syaratsuratizin SET ssiNo='".($objSyaratSuratIzin->ssiNo - 1)."' WHERE ssiID='".$objSyaratSuratIzin->ssiID."'");
	}
	
	$success = true;
	$msg = 'Syarat "'.$objSyaratSuratIzin->ssiNama.'" berhasil digeser ke atas';
	
	viewSyaratSuratIzin($success, $msg, $objSyaratSuratIzin->ssiJsiID);
}

function editSyaratSuratIzin($id) {
	global $app;
	
	//Query
	$objSyaratSuratIzin = $app->queryObject("SELECT * FROM syaratsuratizin WHERE ssiID='".$id."'");
	if (!$objSyaratSuratIzin) {
		$app->showPageError();
		exit();
	}
	
	//Get object
	$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$objSyaratSuratIzin->ssiJsiID."'");
	if (!$objJenisSuratIzin) {
		$app->showPageError();
		exit();
	}

	SyaratSuratIzin_View::editSyaratSuratIzin(true, '', $objSyaratSuratIzin, $objJenisSuratIzin);
}

function saveSyaratSuratIzin() {
	global $app;
	
	//Create object
	$objSyaratSuratIzin = new SyaratSuratIzin_Model();
	$app->bind($objSyaratSuratIzin);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objSyaratSuratIzin->ssiNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT ssiID AS id FROM syaratsuratizin WHERE ssiNama='".$objSyaratSuratIzin->ssiNama."' AND ssiJsiID='".$objSyaratSuratIzin->ssiJsiID."'", $objSyaratSuratIzin->ssiID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objSyaratSuratIzin->ssiNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objSyaratSuratIzin->ssiID == 0) ? true : false;
		
		if ($doInsert) {
			$objSyaratSuratIzin->ssiNo = intval($app->queryField("SELECT MAX(ssiNo) AS nomor FROM syaratsuratizin WHERE ssiJsiID='".$objSyaratSuratIzin->ssiJsiID."'")) + 1 ;
			
			$sql = $app->createSQLforInsert($objSyaratSuratIzin);
		} else {
			$sql = $app->createSQLforUpdate($objSyaratSuratIzin);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objSyaratSuratIzin->ssiID = $app->queryField("SELECT LAST_INSERT_ID() FROM syaratsuratizin");
			
			$success = true;
			$msg = 'Syarat "'.$objSyaratSuratIzin->ssiNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'syaratsuratizin', $objSyaratSuratIzin->ssiID, $objSyaratSuratIzin->ssiNama, $msg);
		} else {
			$success = true;
			$msg = 'Syarat "'.$objSyaratSuratIzin->ssiNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'syaratsuratizin', $objSyaratSuratIzin->ssiID, $objSyaratSuratIzin->ssiNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Syarat "'.$objSyaratSuratIzin->ssiNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewSyaratSuratIzin($success, $msg, $objSyaratSuratIzin->ssiJsiID);
	} else {
		//Get object
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$objSyaratSuratIzin->ssiJsiID."'");
		if (!$objJenisSuratIzin) {
			$app->showPageError();
			exit();
		}
		
		SyaratSuratIzin_View::editSyaratSuratIzin($success, $msg, $objSyaratSuratIzin, $objJenisSuratIzin);
	}
}

function viewSyaratSuratIzin($success, $msg, $id) {
	global $app;
	
	//Query
	$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$id."'");
	if (!$objJenisSuratIzin) {
		$app->showPageError();
		exit();
	}

	//Get variable(s)
	$page 		= $app->pageVar('syaratsuratizin', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('syaratsuratizin', 'sort', 'ssiNo');
	$dir   		= $app->pageVar('syaratsuratizin', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('syaratsuratizin', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "ssiNama LIKE '%".$nama."%'";
	}
	$filter[] = "ssiJsiID='".$id."'";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM syaratsuratizin
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM syaratsuratizin
			".$where." 
			ORDER BY ".$sort." ".$dir.", ssiNama ASC 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	SyaratSuratIzin_View::viewSyaratSuratIzin($success, $msg, $arr, $objJenisSuratIzin);
}
?>