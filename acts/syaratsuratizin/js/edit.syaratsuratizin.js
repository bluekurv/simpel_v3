//Variables
var form = $("#myForm");
var ssiNo = $("#ssiNo");
var ssiNama = $("#ssiNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'ssiNo' || this.id === undefined) {
		if (ssiNo.val() <= 0){
			doSubmit = false;
			ssiNo.addClass("error");		
		} else {
			ssiNo.removeClass("error");
		}
	}
	
	if (this.id == 'ssiNama' || this.id === undefined) {
		if (ssiNama.val().length == 0){
			doSubmit = false;
			ssiNama.addClass("error");		
		} else {
			ssiNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
ssiNama.blur(validateForm);

ssiNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
ssiNama.focus();