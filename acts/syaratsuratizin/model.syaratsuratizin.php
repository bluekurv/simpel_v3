<?php
class SyaratSuratIzin_Model {	
	public $tabel = 'syaratsuratizin';
	public $primaryKey = 'ssiID';
	
	public $ssiID = 0;
	public $ssiJsiID = 0;
	public $ssiNo = 0;
	public $ssiNama = '';
	public $ssiUmumKhusus = 'Umum';
	public $ssiBaru = 0;
	public $ssiPerubahan = 0;
	public $ssiPerpanjangan = 0;
	public $ssiMinimal = 0;
	public $ssiJumlah = 1;
}
?>