<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class SyaratSuratIzin_View {
	static function editSyaratSuratIzin($success, $msg, $obj, $objJenisSuratIzin) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=syaratsuratizin&task=edit&id=<?php echo $obj->ssiID; ?>"><img src="images/icons/tag_pink.png" border="0" /> <?php echo ($obj->ssiID > 0) ? 'Ubah' : 'Tambah'; ?> Syarat untuk <?php echo $objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : ''); ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=syaratsuratizin&task=save" method="POST" >
				<input type="hidden" id="ssiID" name="ssiID" value="<?php echo $obj->ssiID; ?>" />
				<input type="hidden" id="ssiJsiID" name="ssiJsiID" value="<?php echo $obj->ssiJsiID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=syaratsuratizin&id=<?php echo $obj->ssiJsiID; ?>">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="170">No</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->ssiID > 0) {
?>
						<input class="box" id="ssiNo" name="ssiNo" maxlength="3" size="3" value="<?php echo $obj->ssiNo; ?>" />
<?php 
		} else {
?>
						<p>Terakhir</p>
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ssiNama" name="ssiNama" maxlength="500" size="100" value="<?php echo $obj->ssiNama; ?>" />
					</td>
				</tr>
				<tr>
					<td>Jumlah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ssiJumlah" name="ssiJumlah" maxlength="3" size="3" value="<?php echo $obj->ssiJumlah; ?>" />
					</td>
				</tr>
				<tr>
					<td>Persyaratan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchRadio('ssiUmumKhusus', 'Umum', 'Umum', 'Khusus', 'Khusus', $obj->ssiUmumKhusus);
?>
					</td>
				</tr>
				<tr>
					<td>Pendaftaran Baru?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchRadio('ssiBaru', 1, 'Ya', 0, 'Tidak', $obj->ssiBaru);
?>
					</td>
				</tr>
				<tr>
					<td>Pendaftaran Perubahan?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchRadio('ssiPerubahan', 1, 'Ya', 0, 'Tidak', $obj->ssiPerubahan);
?>
					</td>
				</tr>
				<tr>
					<td>Pendaftaran Perpanjangan?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchRadio('ssiPerpanjangan', 1, 'Ya', 0, 'Tidak', $obj->ssiPerpanjangan);
?>
					</td>
				</tr>
				<tr>
					<td>Minimal?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchRadio('ssiMinimal', 1, 'Ya', 0, 'Tidak', $obj->ssiMinimal);
?>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/syaratsuratizin/js/edit.syaratsuratizin.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewSyaratSuratIzin($success, $msg, $arr, $objJenisSuratIzin) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=syaratsuratizin&id=<?php echo $objJenisSuratIzin->jsiID; ?>"><img src="images/icons/tag_pink.png" border="0" /> Syarat untuk <?php echo $objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : ''); ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<input type="hidden" id="id" name="id" value="<?php echo $objJenisSuratIzin->jsiID; ?>" />
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=syaratsuratizin&task=add&id=<?php echo $objJenisSuratIzin->jsiID; ?>">Tambah</a>
				<a class="button-link inline blue" href="index2.php?act=jenissuratizin">Kelola Jenis Surat Izin</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 80),
			$app->setHeader('ssiNo', 'No', true, 40, 'right'),
			$app->setHeader('ssiNama', 'Nama', true),
			$app->setHeader('ssiJumlah', 'Jumlah', true, 0, 'right'),
			$app->setHeader('ssiUmumKhusus', 'Persyaratan', true),
			$app->setHeader('ssiPendaftaran', 'Pendaftaran', false, 230),
			$app->setHeader('ssiMinimal', 'Minimal?', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=syaratsuratizin&task=edit&id='.$v->ssiID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> ';
				echo '<a href="javascript:hapus('.$v->ssiID.', '."'".$v->ssiNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a> ';
				if ($v->ssiNo == 1) {
					echo '<img src="images/icons/blank.png" border="0" /> ';
				} else {
					echo '<a href="index2.php?act=syaratsuratizin&task=up&id='.$v->ssiID.'" title="Geser ke atas"><img src="images/icons/arrow_up.png" border="0" /></a> ';
				}
				if ($arr['startData'] + $i - 1 == $arr['totalData']) {
					echo '<img src="images/icons/blank.png" border="0" /> ';
				} else {
					echo '<a href="index2.php?act=syaratsuratizin&task=down&id='.$v->ssiID.'" title="Geser ke bawah"><img src="images/icons/arrow_down.png" border="0" /></a> ';
				}
				echo '</td>';
				echo '<td align="right">'.$v->ssiNo.'</td>';
				echo '<td><a href="index2.php?act=syaratsuratizin&task=edit&id='.$v->ssiID.'">'.$v->ssiNama.'</a></td>';
				echo '<td align="right">'.$v->ssiJumlah.'</td>';
				echo '<td>'.($v->ssiUmumKhusus == 'Umum' ? '<span class="badge ya">Umum</span>' : '<span class="badge tidak">Khusus</span>').'</td>';
				$pendaftaran = array();
				if ($v->ssiBaru) {
					$pendaftaran[] = '<span class="badge open">Baru</span>';
				} else {
					$pendaftaran[] = '<span class="badge none">Baru</span>';
				}
				if ($v->ssiPerubahan) {
					$pendaftaran[] = '<span class="badge info">Perubahan</span>';
				} else {
					$pendaftaran[] = '<span class="badge none">Perubahan</span>';
				}
				if ($v->ssiPerpanjangan) {
					$pendaftaran[] = '<span class="badge close">Perpanjangan</span>';
				} else {
					$pendaftaran[] = '<span class="badge none">Perpanjangan</span>';
				}
				echo '<td>'.implode('', $pendaftaran).'</td>';
				echo '<td>'.($v->ssiMinimal == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="7">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/syaratsuratizin/js/syaratsuratizin.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>