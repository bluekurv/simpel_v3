<?php
class Pengguna_Model {	
	public $tabel = 'pengguna';
	public $primaryKey = 'pnID';
	
	public $pnID = 0;
	public $pnLokID = 0;
	public $pnUserName = '';
	public $pnPassword = '';
	public $pnLevelAkses = '';
	public $pnBolehAkses = 0;
	public $pnNIP = '';
	public $pnNama = '';
	public $pnJabatan = '';
	public $pnOrganisasi = 'satuankerja';	//satuankerja, bidang, subbidang
	public $pnOrganisasiID = 0;
	public $pnPgrID = 0;
	public $pnEmail = '';
	public $pnTeleponKantor = '';
	public $pnTeleponFax = '';
	public $pnTeleponMobile = '';
	public $pnTeleponRumah = '';
	public $pnLoginPada = '0000-00-00 00:00:00';
	public $pnAksesLaporan = 0;
	public $pnDefaultPengesahanLaporan = 0;
	public $pnAksesAdministrasi = '';
}
?>