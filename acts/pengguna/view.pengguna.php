<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Pengguna_View {
	static function editPasswordPengguna($success, $msg) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pengguna&task=editpassword"><img src="images/icons/key.png" border="0" /> Ubah Kata Kunci</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=pengguna&task=savepassword" method="POST">
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
				</div>
<?php
		$app->showMessage($success, $msg);

		if ($_SESSION['sesipengguna']->defaultPassword) {
?>
				<p><i>Petunjuk: <span style="color:#FF0000;">Anda masih menggunakan kata kunci default "<b>password</b>", harap diubah terlebih dahulu</span>. Isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
<?php 
		} else {
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
<?php 
		}
?>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Kata Kunci Lama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" type="password" id="passwordlama" name="passwordlama" maxlength="15" size="20" /></td>
				</tr>
				<tr>
					<td>Kata Kunci Baru <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" type="password" id="passwordbaru" name="passwordbaru" maxlength="15" size="20" /></td>
				</tr>
				<tr>
					<td>Ulangi Kata Kunci Baru <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" type="password" id="passwordbaru2" name="passwordbaru2" maxlength="15" size="20" /></td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pengguna/js/editpassword.pengguna.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function editPengguna($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pengguna&task=edit&id=<?php echo $obj->pnID; ?>"><img src="images/icons/user.png" border="0" /> <?php echo ($obj->pnID > 0) ? 'Ubah' : 'Tambah'; ?> Pengguna</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=pengguna&task=save" method="POST" >
				<input type="hidden" id="pnID" name="pnID" value="<?php echo $obj->pnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=pengguna">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="200">Nama Pengguna <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnUserName" name="pnUserName" maxlength="15" size="20" value="<?php echo $obj->pnUserName; ?>" /></td>
				</tr>
				<tr>
					<td>Kata Kunci</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" type="password" id="pnPassword" name="pnPassword" maxlength="15" size="20" /></td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Akses</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Level Akses</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		$app->createSelect('pnLevelAkses', "SELECT laNama AS id, laNama AS nama FROM levelakses ORDER BY laNama", $obj->pnLevelAkses);
?>
					</td>
				</tr>
				<tr id="trLokID">
					<td style="padding-left:20px;">Loket Pelayanan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelect('pnLokID', "SELECT lokID AS id, lokNama AS nama FROM loket ORDER BY lokNama", $obj->pnLokID, array(0=>''));
?>
					</td>
				</tr>
				<tr id="trOrganisasiID">
					<td style="padding-left:20px;">Organisasi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="pnOrganisasi" name="pnOrganisasi">
<?php
		foreach (array('satuankerja'=>'Satuan Kerja', 'bidang'=>'Bidang', 'subbidang'=>'Sub Bidang') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $obj->pnOrganisasi) {
				echo ' selected';
			}
			echo ">".$v."</option>\n";
		}
?>
						</select>
<?php 
		$app->createSelect('satuankerja', "SELECT skrID AS id, skrNama AS nama FROM satuankerja ORDER BY skrNama", ($obj->pnOrganisasi == 'satuankerja' ? $obj->pnOrganisasiID : 0));
		$app->createSelect('bidang', "SELECT bidID AS id, bidNama AS nama FROM bidang ORDER BY bidNama", ($obj->pnOrganisasi == 'bidang' ? $obj->pnOrganisasiID : 0));
		$app->createSelect('subbidang', "SELECT subID AS id, subNama AS nama FROM subbidang ORDER BY subNama", ($obj->pnOrganisasi == 'subbidang' ? $obj->pnOrganisasiID : 0));
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Boleh Akses?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('pnBolehAkses', 1, 'Ya', 0, 'Tidak', $obj->pnBolehAkses);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Akses Laporan?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('pnAksesLaporan', 1, 'Ya', 0, 'Tidak', $obj->pnAksesLaporan);
?>
						<div style="margin-left:85px; padding-top:8px;">(<i>Untuk level akses <b>Pejabat</b></i>)</div>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Default Pengesahan Laporan?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('pnDefaultPengesahanLaporan', 1, 'Ya', 0, 'Tidak', $obj->pnDefaultPengesahanLaporan);
?>
						<div style="margin-left:85px; padding-top:8px;">(<i>Untuk level akses <b>Pejabat</b></i>)</div>
					</td>
				</tr>
				<tr>
					<td valign="top" style="padding-left:20px;">Akses Administrasi?</td><td valign="top">&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pnAksesAdministrasi" name="pnAksesAdministrasi" maxlength="100" size="100" value="<?php echo $obj->pnAksesAdministrasi; ?>" /><br>
						(<i>Untuk level akses <b>Petugas Administrasi</b>, diisikan dengan jenis surat izin yang diadministrasikan olehnya. Hanya sebagai informasi saja</i>)
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Informasi Kontak</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NIP <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnNIP" name="pnNIP" maxlength="50" size="50" value="<?php echo $obj->pnNIP; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnNama" name="pnNama" maxlength="50" size="50" value="<?php echo $obj->pnNama; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jabatan</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnJabatan" name="pnJabatan" maxlength="500" size="100" value="<?php echo $obj->pnJabatan; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pangkat (Golongan/Ruang)</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelect('pnPgrID', "SELECT pgrID AS id, CONCAT(pgrPangkat,' (',pgrGolongan,'/',pgrRuang,')') AS nama FROM pangkatgolonganruang ORDER BY pgrID", $obj->pnPgrID);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Email</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnEmail" name="pnEmail" maxlength="50" size="50" value="<?php echo $obj->pnEmail; ?>" /></td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Nomor Telepon</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kantor</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnTeleponKantor" name="pnTeleponKantor" maxlength="50" size="50" value="<?php echo $obj->pnTeleponKantor; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Fax.</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnTeleponFax" name="pnTeleponFax" maxlength="50" size="50" value="<?php echo $obj->pnTeleponFax; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Mobile</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnTeleponMobile" name="pnTeleponMobile" maxlength="50" size="50" value="<?php echo $obj->pnTeleponMobile; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Rumah</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="pnTeleponRumah" name="pnTeleponRumah" maxlength="50" size="50" value="<?php echo $obj->pnTeleponRumah; ?>" /></td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pengguna/js/edit.pengguna.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewPengguna($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pengguna"><img src="images/icons/user.png" border="0" /> Pengguna</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=pengguna&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama Pengguna : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
<?php
		$app->createSelect('filterlevelakses', "SELECT laNama AS id, laNama AS nama FROM levelakses ORDER BY laNama", $arr['filter']['levelakses'], array('--Seluruh Level Akses--'=>'--Seluruh Level Akses--'));
?>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['levelakses'] != $arr['default']['levelakses']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('pnUserName', 'Nama Pengguna', true),
			$app->setHeader('pnNama', 'Nama', true),
			$app->setHeader('pnLevelAkses', 'Level Akses', true),
			$app->setHeader('lokNama', 'Loket', true),
			$app->setHeader('pnOrganisasi', 'Organisasi', false),
			$app->setHeader('pnBolehAkses', 'Boleh Akses', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=pengguna&task=edit&id='.$v->pnID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->pnID.', '."'".$v->pnUserName."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=pengguna&task=edit&id='.$v->pnID.'">'.$v->pnUserName.'</a></td>';
				echo '<td>'.$v->pnNama.'</td>';
				echo '<td>'.$v->pnLevelAkses.'</td>';
				echo '<td>'.$v->lokNama.'</td>';
				if ($v->pnOrganisasi == 'satuankerja') {
					echo '<td>'.$v->skrNama.'</td>';
				} else if ($v->pnOrganisasi == 'bidang') {
					echo '<td>'.$v->bidNama.'</td>';
				} else if ($v->pnOrganisasi == 'subbidang') {
					echo '<td>'.$v->subNama.'</td>';
				} else {
					echo '<td></td>';
				}
				echo '<td>'.($v->pnBolehAkses == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="<?php echo count($columns); ?>">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pengguna/js/pengguna.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>