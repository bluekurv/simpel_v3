<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

//Dengan pengecualian
if (!in_array($app->task, array('login', 'logout', 'editpassword', 'savepassword'))) {
	if (!$app->checkACL($acl)) {
		die(CFG_RESTRICTED_ACCESS);
	}
}

//Include file(s)
require_once 'model.pengguna.php';
require_once 'view.pengguna.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editPengguna($app->id);
		break;
	case 'delete':
		deletePengguna($app->id);
		break;
	case 'editpassword':
		editPasswordPengguna($app->msg);
		break;
	case 'login':
		loginPengguna();
		break;
	case 'logout':
		logoutPengguna();
		break;
	case 'save':
		savePengguna();
		break;
	case 'savepassword':
		savePasswordPengguna();
		break;
	default:
		viewPengguna(true, '');
		break;
}

function deletePengguna($id) {
	global $app;
	
	//Get object
	$objPengguna = $app->queryObject("SELECT * FROM pengguna WHERE pnID='".$id."'");
	if (!$objPengguna) {
		$app->showPageError();
		exit();
	}
	
	if ($_SESSION['sesipengguna']->ID == $id) {
		viewPengguna(false, 'Anda sedang login');
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalEventViewer = intval($app->queryField("SELECT COUNT(*) AS total FROM eventviewer WHERE evDibuatOlehID='".$id."'"));
	if ($totalEventViewer > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalEventViewer.' log aplikasi untuk pengguna tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM pengguna WHERE pnID='".$id."'");
		
		$success = true;
		$msg = 'Pengguna "'.$objPengguna->pnUserName.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'pengguna', $_SESSION['sesipengguna']->ID, $_SESSION['sesipengguna']->namaPengguna, $msg);
	} else {
		$success = false;
		$msg = 'Pengguna "'.$objPengguna->pnUserName.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewPengguna($success, $msg);
}

function editPasswordPengguna($msg) {
	Pengguna_View::editPasswordPengguna(true, $msg);
}

function editPengguna($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objPengguna = $app->queryObject("SELECT * FROM pengguna WHERE pnID='".$id."'");
		if (!$objPengguna) {
			$app->showPageError();
			exit();
		}
	} else {
		$objPengguna = new Pengguna_Model();
	}
	
	Pengguna_View::editPengguna(true, '', $objPengguna);
}

function loginPengguna() {
	global $app;
	
	//Get variable(s)
	$username = isset($_POST['username']) ? trim($_POST['username']) : '';
	$password = isset($_POST['password']) ? trim($_POST['password']) : '';
	
	if ($username != '' && $password != '') {
		//Query
		$sql = "SELECT * 
				FROM pengguna 
				LEFT JOIN loket ON pnLokID=lokID
				WHERE pnUserName='".mysql_real_escape_string($username, $app->connection)."' AND pnPassword='".md5(mysql_real_escape_string($password, $app->connection))."' AND pnBolehAkses=1";
		$objPengguna = $app->queryObject($sql);
		
		if ($objPengguna) {
			$_SESSION['sesipengguna'] = new UserSession();
			$_SESSION['sesipengguna']->ID = $objPengguna->pnID;
			$_SESSION['sesipengguna']->namaPengguna = $objPengguna->pnUserName;
			$_SESSION['sesipengguna']->nama = $objPengguna->pnNama;
			$_SESSION['sesipengguna']->levelAkses = $objPengguna->pnLevelAkses;
			$_SESSION['sesipengguna']->loginSebelumnya = $objPengguna->pnLoginPada;
			$_SESSION['sesipengguna']->loginSaatIni = date('Y-m-d H:i:s');
			$_SESSION['sesipengguna']->loketID = $objPengguna->pnLokID;
			$_SESSION['sesipengguna']->loketNama = $objPengguna->lokNama;
			$_SESSION['sesipengguna']->aksesLaporan = $objPengguna->pnAksesLaporan;
			
			if ($objPengguna->pnOrganisasi == 'satuankerja') {
				$objOrganisasi = $app->queryObject("SELECT skrID AS id, skrNama AS nama FROM satuankerja WHERE skrID='".$objPengguna->pnOrganisasiID."'");
			} else if ($objPengguna->pnOrganisasi == 'bidang') {
				$objOrganisasi = $app->queryObject("SELECT bidID AS id, bidNama AS nama FROM bidang WHERE bidID='".$objPengguna->pnOrganisasiID."'");
			} else if ($objPengguna->pnOrganisasi == 'bidang') {
				$objOrganisasi = $app->queryObject("SELECT subID AS id, subNama AS nama FROM subbidang WHERE subID='".$objPengguna->pnOrganisasiID."'");
			} else {
				$objOrganisasi = new stdClass();
				$objOrganisasi->id = 0;
				$objOrganisasi->nama = '';
			}
			
			$_SESSION['sesipengguna']->bidangID = $objOrganisasi->id;
			$_SESSION['sesipengguna']->bidangNama = $objOrganisasi->nama;
			
			if ($password == 'password') {
				$_SESSION['sesipengguna']->defaultPassword = true;
			} else {
				$_SESSION['sesipengguna']->defaultPassword = false;
			}
			
			$app->query("UPDATE pengguna SET pnLoginPada='".$_SESSION['sesipengguna']->loginSaatIni."' WHERE pnID='".$_SESSION['sesipengguna']->ID."'");
			
			//session_write_close();
			
			$app->writeLog(EV_INFORMASI, 'pengguna', $objPengguna->pnID, $username, 'Pengguna "'.$username.'" berhasil login');

			header('Location:index2.php');
		} else {
			$_SESSION['sesipengguna'] = new UserSession();
			
			//session_write_close();
			
			$app->writeLog(EV_KESALAHAN, 'pengguna', 0, $username, 'Pengguna "'.$username.'" tidak berhasil login');
			
			header('Location:index.php?login=0');
		}
	} else {
		header('Location:index.php?login=0');
	}
}

function logoutPengguna() {
	global $app;
	
	$app->writeLog(EV_INFORMASI, 'pengguna', $_SESSION['sesipengguna']->ID, $_SESSION['sesipengguna']->namaPengguna, 'Pengguna "'.$_SESSION['sesipengguna']->namaPengguna.'" berhasil logout');
	
	$_SESSION = array();
	$_SESSION['sesipengguna'] = new UserSession();
	
	header('Location:index.php');
}

function savePengguna() {
	global $app;
	
	//Create object
	$objPengguna = new Pengguna_Model();
	$app->bind($objPengguna);
	
	//Modify object (if necessary)
	if ($objPengguna->pnLevelAkses != 'Petugas Loket Pelayanan') {
		$objPengguna->pnLokID = 0;
	}

	$satuankerja = $app->getInt('satuankerja');
	$bidang = $app->getInt('bidang');
	$subbidang = $app->getInt('subbidang');
	
	if ($objPengguna->pnLevelAkses == 'Pejabat') {
		if ($objPengguna->pnOrganisasi == 'satuankerja') {
			$objPengguna->pnOrganisasiID = $satuankerja;
		} else if ($objPengguna->pnOrganisasi == 'bidang') {
			$objPengguna->pnOrganisasiID = $bidang;
		} else if ($objPengguna->pnOrganisasi == 'subbidang') {
			$objPengguna->pnOrganisasiID = $subbidang;
		} else {
			$objPengguna->pnOrganisasi = '';
			$objPengguna->pnOrganisasiID = 0;
		}
	} else {
		$objPengguna->pnOrganisasi = '';
		$objPengguna->pnOrganisasiID = 0;
	}

	if (isset($_REQUEST['pnBolehAkses'])) {
		$objPengguna->pnBolehAkses = ($_REQUEST['pnBolehAkses'] == 'on') ? 1 : 0;
	} else {
		$objPengguna->pnBolehAkses = 0;
	}
	if (isset($_REQUEST['pnAksesLaporan'])) {
		$objPengguna->pnAksesLaporan = ($_REQUEST['pnAksesLaporan'] == 'on') ? 1 : 0;
	} else {
		$objPengguna->pnAksesLaporan = 0;
	}
	if (isset($_REQUEST['pnDefaultPengesahanLaporan'])) {
		$objPengguna->pnDefaultPengesahanLaporan = ($_REQUEST['pnDefaultPengesahanLaporan'] == 'on') ? 1 : 0;
	} else {
		$objPengguna->pnDefaultPengesahanLaporan = 0;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objPengguna->pnUserName == '') {
		$doSave = false;
		$msg[] = '- Nama Pengguna belum diisi';
	}
	
	if ($objPengguna->pnLevelAkses == 'Petugas Loket Pelayanan' && $objPengguna->pnLokID == 0) {
		$doSave = false;
		$msg[] = '- Loket Pelayanan belum dipilih';
	}
	
	if ($objPengguna->pnNIP == '') {
		$doSave = false;
		$msg[] = '- NIP belum diisi';
	}
	
	if ($objPengguna->pnNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT pnID AS id FROM pengguna WHERE pnUserName='".$objPengguna->pnUserName."'", $objPengguna->pnID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama Pengguna "'.$objPengguna->pnUserName.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objPengguna->pnID == 0) ? true : false;
		
		if ($doInsert) {
			if ($objPengguna->pnPassword == '') {
				$objPengguna->pnPassword = md5('password');
			} else {
				$objPengguna->pnPassword = md5($objPengguna->pnPassword);
			}
			$sql = $app->createSQLforInsert($objPengguna);
		} else {
			$arrExcludeField = array('pnLoginPada');
			
			if ($objPengguna->pnPassword == '') {
				$arrExcludeField[] = 'pnPassword';
			} else {
				$objPengguna->pnPassword = md5($objPengguna->pnPassword);
			}
			$sql = $app->createSQLforUpdate($objPengguna, $arrExcludeField);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objPengguna->pnID = $app->queryField("SELECT LAST_INSERT_ID() FROM pengguna");
			
			$success = true;
			$msg = 'Pengguna "'.$objPengguna->pnUserName.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'pengguna', $objPengguna->pnID, $objPengguna->pnUserName, $msg);
		} else {
			$success = true;
			$msg = 'Pengguna "'.$objPengguna->pnUserName.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'pengguna', $objPengguna->pnID, $objPengguna->pnUserName, $msg);
		}
		
		//Hanya ada satu pengguna yang menjadi Default Pengesahan Laporan
		if ($objPengguna->pnDefaultPengesahanLaporan) {
			$app->query("UPDATE pengguna SET pnDefaultPengesahanLaporan=0 WHERE pnID<>'".$objPengguna->pnID."'");
		}
	} else {
		$success = false;
		$msg = 'Pengguna "'.$objPengguna->pnUserName.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewPengguna($success, $msg);
	} else {
		Pengguna_View::editPengguna($success, $msg, $objPengguna);
	}
}

function savePasswordPengguna() {
	global $app;
	
	$objPengguna = $app->queryObject("SELECT * FROM pengguna WHERE pnUserName='".$_SESSION['sesipengguna']->namaPengguna."' AND pnPassword='".md5(mysql_real_escape_string($_REQUEST['passwordlama']))."'");
	
	if ($objPengguna) {
		$app->query("UPDATE pengguna SET pnPassword='".md5(mysql_real_escape_string($_REQUEST['passwordbaru']))."' WHERE pnUserName='".$_SESSION['sesipengguna']->namaPengguna."'");
	
		if ($_REQUEST['passwordbaru'] == 'password') {
			$_SESSION['sesipengguna']->defaultPassword = true;
		} else {
			$_SESSION['sesipengguna']->defaultPassword = false;
		}
		
		$success = true;
		$msg = 'Kata Kunci berhasil diubah';
		
		$app->writeLog(EV_INFORMASI, 'pengguna', $_SESSION['sesipengguna']->ID, $_SESSION['sesipengguna']->namaPengguna, 'Kata Kunci untuk Pengguna "'.$_SESSION['sesipengguna']->namaPengguna.'" berhasil diubah');
	} else {
		$success = false;
		$msg = 'Kata Kunci tidak berhasil diubah';
	}
	
	if ($success) {
		//Handling menu
		header("Location:index2.php?act=pengguna&task=editpassword&msg=".$msg);
	} else {
		Pengguna_View::editPasswordPengguna($success, $msg);
	}
}

function viewPengguna($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('pengguna', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('pengguna', 'sort', 'pnUserName');
	$dir   		= $app->pageVar('pengguna', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'levelakses' => '--Seluruh Level Akses--'
	);
	
	$nama 		= $app->pageVar('pengguna', 'filternama', $default['nama'], 'strval');
	$levelakses = $app->pageVar('pengguna', 'filterlevelakses', $default['levelakses'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "pnUserName LIKE '%".$nama."%'";
	}
	if ($levelakses != $default['levelakses']) {
		$filter[] = "pnLevelAkses='".$levelakses."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'levelakses' => $levelakses
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM pengguna
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM pengguna
			LEFT JOIN loket ON pnLokID=lokID
			LEFT JOIN satuankerja ON pnOrganisasi='satuankerja' AND pnOrganisasiID=skrID
			LEFT JOIN bidang ON pnOrganisasi='bidang' AND pnOrganisasiID=bidID
			LEFT JOIN subbidang ON pnOrganisasi='subbidang' AND pnOrganisasiID=subID
			".$where." 
			ORDER BY ".$sort." ".$dir."
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Pengguna_View::viewPengguna($success, $msg, $arr);
}
?>