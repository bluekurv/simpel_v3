//Variables
var form = $("#myForm");
var pnNama = $("#pnNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'pnNama' || this.id === undefined) {
		if (pnNama.val().length == 0){
			doSubmit = false;
			pnNama.addClass("error");		
		} else {
			pnNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
pnNama.blur(validateForm);

pnNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
pnNama.focus();