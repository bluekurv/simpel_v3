//Variables
var form = $("#myForm");
var passwordlama = $("#passwordlama");
var passwordbaru = $("#passwordbaru");
var passwordbaru2 = $("#passwordbaru2");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'passwordlama' || this.id === undefined) {
		if (passwordlama.val().length == 0){
			doSubmit = false;
			passwordlama.addClass("error");		
		} else {
			passwordlama.removeClass("error");
		}
	}
	
	if (this.id == 'passwordbaru' || this.id === undefined) {
		if (passwordbaru.val().length == 0){
			doSubmit = false;
			passwordbaru.addClass("error");		
		} else {
			if (passwordbaru2.val().length > 0 && passwordbaru.val() != passwordbaru2.val()) {
				doSubmit = false;
				passwordbaru.addClass("error");	
			} else {
				passwordbaru.removeClass("error");
			}
		}
	}
	
	if (this.id == 'passwordbaru2' || this.id === undefined) {
		if (passwordbaru2.val().length == 0){
			doSubmit = false;
			passwordbaru2.addClass("error");
		} else {
			if (passwordbaru.val().length > 0 && passwordbaru.val() != passwordbaru2.val()) {
				doSubmit = false;
				passwordbaru2.addClass("error");
			} else {
				passwordbaru2.removeClass("error");
			}
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
passwordlama.blur(validateForm);
passwordbaru.blur(validateForm);
passwordbaru2.blur(validateForm);

passwordlama.keyup(validateForm);
passwordbaru.keyup(validateForm);
passwordbaru2.keyup(validateForm);

form.submit(submitForm);

//Set focus
passwordlama.focus();