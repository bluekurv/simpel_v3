//Variables
var form = $("#myForm");
var pnUserName = $("#pnUserName");
var pnNIP = $("#pnNIP");
var pnNama = $("#pnNama");

if ($("#pnLevelAkses").val() == 'Pejabat') {
	$("#trOrganisasiID").show();
} else {
	$("#trOrganisasiID").hide();
}

if ($("#pnOrganisasi").val() == 'satuankerja') {
	$("#satuankerja").show();
	$("#bidang").hide();
	$("#subbidang").hide();
} else if ($("#pnOrganisasi").val() == 'bidang') {
	$("#satuankerja").hide();
	$("#bidang").show();
	$("#subbidang").hide();
} else if ($("#pnOrganisasi").val() == 'subbidang') {
	$("#satuankerja").hide();
	$("#bidang").hide();
	$("#subbidang").show();
}

if ($("#pnLevelAkses").val() == 'Petugas Loket Pelayanan') {
	$("#trLokID").show();
} else {
	$("#trLokID").hide();
}

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'pnUserName' || this.id === undefined) {
		if (pnUserName.val().length == 0){
			doSubmit = false;
			pnUserName.addClass("error");		
		} else {
			pnUserName.removeClass("error");
		}
	}
	
	if (this.id == 'pnNIP' || this.id === undefined) {
		if (pnNIP.val().length == 0){
			doSubmit = false;
			pnNIP.addClass("error");		
		} else {
			pnNIP.removeClass("error");
		}
	}
	
	if (this.id == 'pnNama' || this.id === undefined) {
		if (pnNama.val().length == 0){
			doSubmit = false;
			pnNama.addClass("error");		
		} else {
			pnNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$("#pnLevelAkses").on("change", function(){
	if ($(this).val() == 'Pejabat') {
		$("#trOrganisasiID").show();
	} else {
		$("#trOrganisasiID").hide();
	}

	if ($(this).val() == 'Petugas Loket Pelayanan') {
		$("#trLokID").show();
	} else {
		$("#trLokID").hide();
	}
});

$("#pnOrganisasi").on("change", function(){
	if ($(this).val() == 'satuankerja') {
		$("#satuankerja").show();
		$("#bidang").hide();
		$("#subbidang").hide();
	} else if ($(this).val() == 'bidang') {
		$("#satuankerja").hide();
		$("#bidang").show();
		$("#subbidang").hide();
	} else if ($(this).val() == 'subbidang') {
		$("#satuankerja").hide();
		$("#bidang").hide();
		$("#subbidang").show();
	}
});

pnUserName.blur(validateForm);
pnNIP.blur(validateForm);
pnNama.blur(validateForm);

pnUserName.keyup(validateForm);
pnNIP.keyup(validateForm);
pnNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
pnUserName.focus();