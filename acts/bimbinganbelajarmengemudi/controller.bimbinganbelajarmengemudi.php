<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'bimbinganbelajarmengemudi';
$childTitle = 'Izin Operasional Bimbingan Belajar Mengemudi';
$childKodeSurat = 'IOP-BBM';
$childTembusan = 'Kepala Dinas Pendidikan Kabupaten Pelalawan';
$childTembusan2 = 'Kepala Dinas Perhubungan, Komunikasi dan Informasi Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>