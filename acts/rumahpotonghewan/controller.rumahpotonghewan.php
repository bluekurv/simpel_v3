<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'rumahpotonghewan';
$childTitle = 'Izin Operasional Rumah Potong Hewan';
$childKodeSurat = 'IOP-RPH';
$childTembusan = 'Kepala Dinas Peternakan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>