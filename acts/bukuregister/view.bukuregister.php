<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class BukuRegister_View {
	static function indexBukuRegister() {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=bukuregister"><img src="images/icons/book_addresses.png" border="0" /> Buku Register</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
<?php 
		$jenissuratizin = $app->queryArrayOfObjects("SELECT *, jsiID AS id FROM jenissuratizin WHERE jsiTampil=1 GROUP BY jsiKode ORDER BY jsiNama");
		if (count($jenissuratizin) > 0) {
?>
	<ul>
<?php
 			foreach ($jenissuratizin as $v) {
 				if (file_exists(CFG_ABSOLUTE_PATH.'/acts/'.$v->jsiKode)) {
?>
 		<li style="padding:5px;"><a href="index2.php?act=<?php echo $v->jsiKode; ?>&page=1"><?php echo $v->jsiNama; ?></a></li>
<?php
 				}
 			}
?>
	</ul>
<?php
 		}
?>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php
	}
}
?>