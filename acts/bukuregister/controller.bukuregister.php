<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Loket Pelayanan']['all'] = true;
$acl['Petugas Administrasi']['all'] = true;
$acl['Petugas Loket Penyerahan']['all'] = true;
$acl['Pejabat']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'view.bukuregister.php';

switch ($app->task) {
	default:
		indexBukuRegister();
		break;
}

function indexBukuRegister() {
	BukuRegister_View::indexBukuRegister();
}
?>