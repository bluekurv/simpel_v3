<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'pabrik';
$childTitle = 'Izin Operasional Pabrik';
$childKodeSurat = 'IOP-PBR';
$childTembusan = 'Kepala Dinas Perindustrian, Perdagangan dan Pasar Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>