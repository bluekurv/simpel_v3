<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Surveyor']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'view.survey.php';

switch ($app->task) {
	default:
		viewSurvey($app->success, $app->msg);
		break;
}

function viewSurvey($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('survey', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('survey', 'sort', 'mhnNoUrutLengkap');
	$dir   		= $app->pageVar('survey', 'dir', 'desc');
	
	//Add filter(s) and it's default value
	$default = array(
		'field' => '',
		'nama' => '',
		'status' => ''
	);
	
	$field 		= $app->pageVar('survey', 'filterfield', $default['field'], 'strval');
	$nama 		= $app->pageVar('survey', 'filternama', $default['nama'], 'strval');
	$status 	= $app->pageVar('survey', 'filterstatus', $default['status'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = $field." LIKE '%".$nama."%'";
	}
	
	switch ($status) {
		case 'Belum Survey':
			$filter[] = "dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=0";
			break;
		case 'Memenuhi Syarat':
			$filter[] = "dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat'";
			break;
		case 'Ditunda':
			$filter[] = "dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=1 AND dmhnHasilSurvey='Ditunda'";
			break;
		case 'Batal':
			$filter[] = "dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=1 AND dmhnHasilSurvey='Batal'";
			break;
		default:
			$filter[] = "dmhnPola>0 AND dmhnPerluSurvey=1";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'subdata' => array(), 
		'filter' => array(
			'field' => $field,
			'nama' => $nama,
			'status' => $status
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM detailpermohonan
				  LEFT JOIN permohonan ON dmhnMhnID=mhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN pengguna ON mhnDibuatOleh=pengguna.pnID
				  LEFT JOIN perusahaan ON mhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *, pengguna.pnNama AS dibuatoleh, pengguna2.pnNama AS diadministrasikanoleh
		  	FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN pengguna ON mhnDibuatOleh=pengguna.pnID
			LEFT JOIN pengguna AS pengguna2 ON dmhnDiadministrasikanOleh=pengguna2.pnID
			LEFT JOIN perusahaan ON mhnPerID=perID
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][$obj->mhnID] = $obj;
		$arr['subdata'][$obj->mhnID][$obj->dmhnID] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Status_View::viewStatus($success, $msg, $arr);
}
?>