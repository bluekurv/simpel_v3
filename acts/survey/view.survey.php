<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Status_View {
	static function viewStatus($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=survey"><img src="images/icons/rosette.png" border="0" /> Daftar Survey</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				<select class="box" id="filterfield" name="filterfield">
<?php 
		foreach (array('mhnNoUrutLengkap'=>'No. Pendaftaran', 'mhnNoKtp'=>'No. KTP', 'mhnNama'=>'Nama Pemohon', 'perNama'=>'Nama Perusahaan') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['filter']['field']) {
				echo 'selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filterstatus" name="filterstatus">
					<option value="">--Seluruh Status--</option>
<?php 
		foreach (array('Belum Survey', 'Memenuhi Syarat', 'Ditunda', 'Batal') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $arr['filter']['status']) {
				echo 'selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['status'] != $arr['default']['status']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 20),
			$app->setHeader('mhnTgl', 'Tanggal', true, 60),
			$app->setHeader('mhnNoUrutLengkap', 'Nomor<br>Pendaftaran', true),
			$app->setHeader('mhnNoKtp', 'Nomor KTP', true),
			$app->setHeader('mhnNama', 'Pemohon /<br>Status Survey', true),
			$app->setHeader('perNama', 'Perusahaan /<br>Status Pengurusan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo '<tr>';
        echo '<td>';
        echo '<a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'&fromact=survey" title="Lihat Permohonan"><img src="images/icons/zoom.png" border="0" /></a>';
        echo '&nbsp;<a target="_blank" href="' . rtrim(CFG_LIVE_PATH, "/") . ':1337/permohonan/' . $v->mhnID . '" title="Lihat Data Spasial Permohonan"><img src="images/icons/map.png" border="0" /></a> ';
        echo '</td>';
				echo '<td>'.$app->MySQLDateToNormal($v->mhnTgl).'</td>';
				echo '<td><a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'&fromact=survey" title="Lihat Permohonan">'.$v->mhnNoUrutLengkap.'</a></td>';
				echo '<td>'.$v->mhnNoKtp.'</td>';
				echo '<td>'.$v->mhnNama.'</td>';
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				
				if (count($arr['subdata'][$v->mhnID]) > 0) {
					foreach ($arr['subdata'][$v->mhnID] as $v2) {
						echo '<tr class="odd">';
						echo '<td><a href="index2.php?act=detailpermohonan&task=edit&id='.$v2->dmhnID.'&fromact=survey" title="Entri Detail Permohonan (Alur Penyelesaian)"><img src="images/icons/arrow_right.png" border="0" /></a></td>';
						echo '<td></td>';
						
						/*switch ($v2->dmhnTipe) {
							case 'Baru':
								echo '<td><span class="badge open">Baru</span></td>';
								break;
							case 'Perubahan':
								echo '<td><span class="badge info">Perubahan</span></td>';
								break;
							case 'Perpanjangan':
								echo '<td><span class="badge close">Perpanjangan</span></td>';
								break;
							default:
								echo '<td><span class="badge close">?</span></td>';
						}*/
						echo '<td>'.$v2->dmhnTipe.'</td>';
						echo '<td>'.$v2->jsiNama.($v2->jsiSubNama != '' ? ' ('.$v2->jsiSubNama.')' : '').'</td>';
						
						$badge = '';
						if ($v2->dmhnPerluSurvey == 1) {
							if ($v2->dmhnTelahSurvey == 0) {
								$badge = '<span class="badge warning">Belum Survey</span>';
							} else {
								switch ($v2->dmhnHasilSurvey) {
									case 'Memenuhi Syarat':
										$badge = '<span class="badge ya">Memenuhi Syarat</span>';
										break;
									case 'Ditunda':
										$badge = '<span class="badge inverse">Ditunda</span>';
										break;
									case 'Batal':
										$badge = '<span class="badge tidak">Batal</span>';
										break;
								}
							}
						}
						echo '<td>'.$badge.'</td>';
						
						$badge = '';
					 	if ($v2->dmhnDikembalikan == 0) {
							if ($v2->dmhnPola > 0) {
						 		if ($v2->dmhnPerluSurvey == 1) {
									if ($v2->dmhnTelahSurvey == 0) {
										$badge = '<span class="badge warning">Belum Survey</span>';
									} else {
										/*if ($v2->dmhnHasilSurvey == 'Memenuhi Syarat') {
											if ($v2->dmhnTelahSelesai == 0) {
												$badge = '<span class="badge tidak">Belum Selesai</span>';
											} else {
												if ($v2->dmhnTelahAmbil == 0) { 
													$badge = '<span class="badge info">Belum Diambil</span>';
												} else {
													$badge = '<span class="badge ya">Sudah Diambil</span>';
												}
											}
										} else if ($v2->dmhnHasilSurvey == 'Ditunda') {
											$badge = '<span class="badge tidak">Ditunda</span>';
										} else if ($v2->dmhnHasilSurvey == 'Batal') {
											$badge = '<span class="badge tidak">Batal</span>';
										}*/
										//Tidak mengacu ke view.permohonan.php, karena ini dari sudut pandang surveyor
										if ($v2->dmhnTelahSelesai == 0) {
											$badge = '<span class="badge tidak">Belum Selesai</span>';
										} else {
											if ($v2->dmhnTelahAmbil == 0) { 
												$badge = '<span class="badge info">Belum Diambil</span>';
											} else {
												$badge = '<span class="badge ya">Sudah Diambil</span>';
											}
										}
									}
								} else {
									if ($v2->dmhnTelahSelesai == 0) {
										$badge = '<span class="badge tidak">Belum Selesai</span>';
									} else {
										if ($v2->dmhnTelahAmbil == 0) { 
											$badge = '<span class="badge info">Belum Diambil</span>';
										} else {
											$badge = '<span class="badge ya">Sudah Diambil</span>';
										}
									}
								}
							} else {
								$badge = '<span class="badge tidak">Belum Ditentukan Polanya</span>';
							}
						} else {
							$badge = '<span class="badge inverse">Dikembalikan</span>';
						}
						echo '<td>'.$badge.'</td>';
						echo "</tr>\n";
					}
				}
				
				$i++;
			}
		} else {
?>
			<tr><td colspan="9">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/survey/js/survey.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>