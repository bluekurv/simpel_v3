//Functions
function lihat(page) {
	var url = 'index2.php?act=survey&page=' + page;
	url += '&filterfield=' + $('#filterfield').val();
	url += '&filternama=' + $('#filternama').val();
	url += '&filterstatus=' + $('#filterstatus').val();
	
	window.open(url, '_self');
}

function sort(field, direction) {
	var url = 'index2.php?act=survey';
	url += '&sort=' + field;
	url += '&dir=' + direction;
	
	window.open(url, '_self');
}

//Events
$("#page").change(function() {
	lihat(this.value);
});

$('#filternama').keypress(function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
		lihat(1);
	}
});

$("#find").click(function() {
	lihat(1);
});

$("#cancel").click(function() {
	$('#filterfield').val('mhnNoUrutLengkap');
	$('#filternama').val('');
	$('#filterstatus').val('');
	lihat(1);
});