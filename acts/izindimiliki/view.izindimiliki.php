<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class IzinDimiliki_View {
	static function editIzinDimiliki($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=izindimiliki&task=edit&id=<?php echo $obj->izID; ?>"><img src="images/icons/database_table.png" border="0" /> <?php echo ($obj->izID > 0) ? 'Ubah' : 'Tambah'; ?> Jenis Izin yang Dimiliki</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=izindimiliki&task=save" method="POST" >
				<input type="hidden" id="izID" name="izID" value="<?php echo $obj->izID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=izindimiliki">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Kode <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izKode" name="izKode" maxlength="5" size="5" value="<?php echo $obj->izKode; ?>" />
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izNama" name="izNama" maxlength="255" size="50" value="<?php echo $obj->izNama; ?>" />
					</td>
				</tr>
				<tr>
					<td>Dikeluarkan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="izDikeluarkanOleh" name="izDikeluarkanOleh" maxlength="255" size="50" value="<?php echo $obj->izDikeluarkanOleh; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/izindimiliki/js/edit.izindimiliki.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewIzinDimiliki($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=izindimiliki"><img src="images/icons/database_table.png" border="0" /> Jenis Izin yang Dimiliki</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=izindimiliki&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Kode/Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('izKode', 'Kode', true, 40),
			$app->setHeader('izNama', 'Nama', true),
			$app->setHeader('izDikeluarkanOleh', 'Dikeluarkan Oleh', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=izindimiliki&task=edit&id='.$v->izID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->izID.', '."'".$v->izNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=izindimiliki&task=edit&id='.$v->izID.'">'.$v->izKode.'</a></td>';
				echo '<td>'.$v->izNama.'</td>';
				echo '<td>'.$v->izDikeluarkanOleh.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="4">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/izindimiliki/js/izindimiliki.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>