//Variables
var form = $("#myForm");
var izKode = $("#izKode");
var izNama = $("#izNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'izKode' || this.id === undefined) {
		if (izKode.val().length == 0){
			doSubmit = false;
			izKode.addClass("error");		
		} else {
			izKode.removeClass("error");
		}
	}
	
	if (this.id == 'izNama' || this.id === undefined) {
		if (izNama.val().length == 0){
			doSubmit = false;
			izNama.addClass("error");		
		} else {
			izNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
izKode.blur(validateForm);
izNama.blur(validateForm);

izKode.keyup(validateForm);
izNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
izKode.focus();