<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.izindimiliki.php';
require_once 'view.izindimiliki.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editIzinDimiliki($app->id);
		break;
	case 'delete':
		deleteIzinDimiliki($app->id);
		break;
	case 'save':
		saveIzinDimiliki();
		break;
	default:
		viewIzinDimiliki(true, '');
		break;
}

function deleteIzinDimiliki($id) {
	global $app;
	
	//Get object
	$objIzinDimiliki = $app->queryObject("SELECT * FROM izindimiliki WHERE izID='".$id."'");
	if (!$objIzinDimiliki) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM izindimiliki WHERE izID='".$id."'");
		
		$success = true;
		$msg = 'Jenis Izin "'.$objIzinDimiliki->izNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'izin', $objIzinDimiliki->izID, $objIzinDimiliki->izNama, $msg);
	} else {
		$success = false;
		$msg = 'Jenis Izin "'.$objIzinDimiliki->izNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewIzinDimiliki($success, $msg);
}

function editIzinDimiliki($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objIzinDimiliki = $app->queryObject("SELECT * FROM izindimiliki WHERE izID='".$id."'");
		if (!$objIzinDimiliki) {
			$app->showPageError();
			exit();
		}
	} else {
		$objIzinDimiliki = new IzinDimiliki_Model();
	}
	
	IzinDimiliki_View::editIzinDimiliki(true, '', $objIzinDimiliki);
}

function saveIzinDimiliki() {
	global $app;
	
	//Create object
	$objIzinDimiliki = new IzinDimiliki_Model();
	$app->bind($objIzinDimiliki);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objIzinDimiliki->izKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objIzinDimiliki->izNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT izID AS id FROM izindimiliki WHERE izKode='".$objIzinDimiliki->izKode."'", $objIzinDimiliki->izID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objIzinDimiliki->izKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT izID AS id FROM izindimiliki WHERE izNama='".$objIzinDimiliki->izNama."'", $objIzinDimiliki->izID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objIzinDimiliki->izNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objIzinDimiliki->izID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objIzinDimiliki);
		} else {
			$sql = $app->createSQLforUpdate($objIzinDimiliki);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objIzinDimiliki->izID = $app->queryField("SELECT LAST_INSERT_ID() FROM izindimiliki");
			
			$success = true;
			$msg = 'Jenis Izin "'.$objIzinDimiliki->izNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'izin', $objIzinDimiliki->izID, $objIzinDimiliki->izNama, $msg);
		} else {
			$success = true;
			$msg = 'Jenis Izin "'.$objIzinDimiliki->izNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'izin', $objIzinDimiliki->izID, $objIzinDimiliki->izNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Jenis Izin "'.$objIzinDimiliki->izNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewIzinDimiliki($success, $msg);
	} else {
		IzinDimiliki_View::editIzinDimiliki($success, $msg, $objIzinDimiliki);
	}
}

function viewIzinDimiliki($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('izin', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('izin', 'sort', 'izKode');
	$dir   		= $app->pageVar('izin', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('izin', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(izKode LIKE '%".$nama."%' OR izNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM izindimiliki
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM izindimiliki
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	IzinDimiliki_View::viewIzinDimiliki($success, $msg, $arr);
}
?>