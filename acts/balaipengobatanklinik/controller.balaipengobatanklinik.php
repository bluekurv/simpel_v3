<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'balaipengobatanklinik';
$childTitle = 'Izin Operasional Balai Pengobatan / Klinik';
$childKodeSurat = 'IOP-KL';
$childTembusan = 'Kepala Dinas Kesehatan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>