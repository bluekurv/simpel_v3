//Objects
$("#tdiLuasBangunan").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
$("#tdiLuasTanah").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
$("#tdiInventaris").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#tdiKapasitasProduksiPerTahun").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#tdiTenagaKerjaIndonesiaLaki").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#tdiTenagaKerjaIndonesiaPerempuan").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#tdiTenagaKerjaAsingLaki").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#tdiTenagaKerjaAsingPerempuan").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });

//Variables
var form = $("#myForm");
var tdiKelurahanLokasi = $("#tdiKelurahanLokasi");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'tdiKelurahanLokasi' || this.id === undefined) {
		if (tdiKelurahanLokasi.val().length == 0) {
			doSubmit = false;
			tdiKelurahanLokasi.addClass("error");		
		} else {
			tdiKelurahanLokasi.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

$("#tdiKlasifikasi").change(function() {
	$("#spanKlasifikasi").html($(this).val());
});

form.submit(submitForm);

$('#tdiKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#tdiKelurahanID').val(suggestion.data.id);
		$('#tdiKelurahanKode').val(suggestion.data.kode);
		$('#tdiKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#tdiKelurahanLokasi').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#tdiKelurahanLokasiID').val(suggestion.data.id);
		$('#tdiKelurahanLokasiKode').val(suggestion.data.kode);
		$('#tdiKelurahanLokasi').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#tdiKlui').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=klui',
	onSelect: function(suggestion){
		$('#tdiKluiID').val(suggestion.data.id);
		$('#tdiKluiKode').val(suggestion.data.kode);
		$('#tdiKlui').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

//Set focus
$("#perNPWP").focus();