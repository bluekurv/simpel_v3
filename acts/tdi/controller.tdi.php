<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.tdi.php';
require_once 'view.tdi.php';

switch ($app->task) {
	case 'add':
		addTdi($app->id);
		break;
	case 'edit':
		editTdi($app->id);
		break;
	case 'delete':
		deleteTdi($app->id);
		break;
	case 'print':
		printTdi($app->id);
		break;
	case 'read':
		readTdi($app->id);
		break;
	case 'save':
		saveTdi();
		break;
	default:
		viewTdi(true, '');
		break;
}

function addTdi($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editTdi($id);
}

function editTdi($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objTdi = $app->queryObject("SELECT * FROM tdi WHERE tdiDmhnID='".$id."'");
	if (!$objTdi) {
		$objTdi = new Tdi_Model();
		
		//Ambil nilai default
		$objTdi->tdiAlamat = $objDetailPermohonan->perAlamat;
		$objTdi->tdiKelurahanID = $objDetailPermohonan->perKelurahanID;
		
		$objTdi->tdiNPWP = $objDetailPermohonan->perNPWP;
		$objTdi->tdiNIPIK = $objDetailPermohonan->perNIPIK;
		
		//
		$objTdi->tdiKluiID = $objDetailPermohonan->perKluiID;
		
		$objTdi->tdiKepemilikanPabrik = $objDetailPermohonan->perKepemilikanPabrik;
		$objTdi->tdiKelurahanLokasiID = $objDetailPermohonan->perKelurahanLokasiID;
		$objTdi->tdiLuasBangunan = $objDetailPermohonan->perLuasBangunan;
		$objTdi->tdiLuasTanah = $objDetailPermohonan->perLuasTanah;
		$objTdi->tdiTenagaPenggerak = $objDetailPermohonan->perTenagaPenggerak;
		$objTdi->tdiKlasifikasi = $objDetailPermohonan->perKlasifikasi;
		$objTdi->tdiKomoditiIndustri = $objDetailPermohonan->perKomoditiIndustri;
		$objTdi->tdiInventaris = $objDetailPermohonan->perInventaris;
		$objTdi->tdiKapasitasProduksiPerTahun = $objDetailPermohonan->perKapasitasProduksiPerTahun;
		$objTdi->tdiSatuanKapasitasProduksiPerTahun = $objDetailPermohonan->perSatuanKapasitasProduksiPerTahun;
		$objTdi->tdiTenagaKerjaIndonesiaLaki = $objDetailPermohonan->perTenagaKerjaIndonesiaLaki;
		$objTdi->tdiTenagaKerjaIndonesiaPerempuan = $objDetailPermohonan->perTenagaKerjaIndonesiaPerempuan;
		$objTdi->tdiTenagaKerjaAsingLaki = $objDetailPermohonan->perTenagaKerjaAsingLaki;
		$objTdi->tdiTenagaKerjaAsingPerempuan = $objDetailPermohonan->perTenagaKerjaAsingPerempuan;
	} else {
		if ($objTdi->tdiAlamat == '') {
			$objTdi->tdiAlamat = $objDetailPermohonan->perAlamat;
		}
		if ($objTdi->tdiKelurahanID == 0) {
			$objTdi->tdiKelurahanID = $objDetailPermohonan->perKelurahanID;
		}
	}
	
	$objTdi->tdiDmhnID = $objDetailPermohonan->dmhnID;
	
	Tdi_View::editTdi(true, '', $objDetailPermohonan, $objTdi);
}

function deleteTdi($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM tdi WHERE tdiDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Tanda Daftar Industri berhasil dihapus';
		
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Tanda Daftar Industri', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function printTdi($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN tdi ON dmhnID=tdiDmhnID
			WHERE dmhnID='".$id."'";
	$objTdi = $app->queryObject($sql);
	if (!$objTdi) {
		$app->showPageError();
		exit();
	}
	
	if ($objTdi->tdiAlamat == '') {
		$objTdi->tdiAlamat = $objTdi->perAlamat;
	}
	if ($objTdi->tdiKelurahanID == 0) {
		$objTdi->tdiKelurahanID = $objTdi->perKelurahanID;
	}

	/*if ($objTdi->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	Tdi_View::printTdi($objTdi);
}

function readTdi($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN tdi ON dmhnID=tdiDmhnID
			WHERE dmhnID='".$id."'";
	$objTdi = $app->queryObject($sql);
	if (!$objTdi) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objTdi->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Tdi_View::readTdi($objTdi);
}

function saveTdi() {
	global $app;
	
	//Get object
	$objTdiSebelumnya = $app->queryObject("SELECT * FROM tdi WHERE tdiID='".$app->getInt('tdiID')."'");
	if (!$objTdiSebelumnya) {
		$objTdiSebelumnya = new Tdi_Model();
	}
	
	//Create object
	$objTdi = new Tdi_Model();
	$app->bind($objTdi);
	
	//Modify object (if necessary)
	$objTdi->tdiLuasBangunan = $app->MoneyToMySQL($objTdi->tdiLuasBangunan);
	$objTdi->tdiLuasTanah = $app->MoneyToMySQL($objTdi->tdiLuasTanah);
	$objTdi->tdiInventaris = $app->MoneyToMySQL($objTdi->tdiInventaris);
	$objTdi->tdiKapasitasProduksiPerTahun = $app->MoneyToMySQL($objTdi->tdiKapasitasProduksiPerTahun);
	
	$objTdi->tdiTenagaKerjaIndonesiaLaki = $app->MoneyToMySQL($objTdi->tdiTenagaKerjaIndonesiaLaki);
	$objTdi->tdiTenagaKerjaIndonesiaPerempuan = $app->MoneyToMySQL($objTdi->tdiTenagaKerjaIndonesiaPerempuan);
	$objTdi->tdiTenagaKerjaAsingLaki = $app->MoneyToMySQL($objTdi->tdiTenagaKerjaAsingLaki);
	$objTdi->tdiTenagaKerjaAsingPerempuan = $app->MoneyToMySQL($objTdi->tdiTenagaKerjaAsingPerempuan);
	
	$objTdi->tdiTglPengesahan = $app->NormalDateToMySQL($objTdi->tdiTglPengesahan);
	if ($objTdi->tdiTglPengesahan != '0000-00-00') {
		if ($objTdiSebelumnya->tdiNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objTdi->tdiNo = intval($app->queryField("SELECT MAX(tdiNo) AS nomor FROM tdi WHERE YEAR(tdiTglPengesahan)='".substr($objTdi->tdiTglPengesahan,0,4)."' AND tdiKlasifikasi='".$objTdi->tdiKlasifikasi."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objTdi->tdiTglBerlaku = date('Y-m-d', strtotime($objTdi->tdiTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objTdi->tdiTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objTdi->tdiTglDaftarUlang = date('Y-m-d', strtotime($objTdi->tdiTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objTdi->tdiTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objTdi->tdiNo = 0;
		
		$objTdi->tdiTglBerlaku = '0000-00-00';
		$objTdi->tdiTglDaftarUlang = '0000-00-00';
	}
	
	if ($objTdi->tdiNIPIK == 0) {
		$objTdi->tdiNIPIK = intval($app->queryField("SELECT MAX(tdiNIPIK) AS nomor FROM tdi")) + 1;
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objTdi->tdiNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/TDI/'.$objTdi->tdiKlasifikasi.'/'.substr($objTdi->tdiTglPengesahan,0,4).'/'.$objTdi->tdiNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objTdi->tdiNo > 0){
		$isExist = $app->isExist("SELECT tdiID AS id FROM tdi WHERE tdiNo='".$objTdi->tdiNo."' AND YEAR(tdiTglPengesahan)='".substr($objTdi->tdiTglPengesahan,0,4)."' AND tdiKlasifikasi='".$objTdi->tdiKlasifikasi."'", $objTdi->tdiID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objTdi->tdiNo.'" sudah ada';
		}
	}
	
	if ($objTdi->tdiKelurahanLokasiID == 0) {
		$doSave = false;
		$msg[] = '- Kelurahan untuk Lokasi Pabrik belum dipilih dari daftar yang ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objTdi->tdiID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objTdi);
		} else {
			$sql = $app->createSQLforUpdate($objTdi);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objTdi->tdiID = $app->queryField("SELECT LAST_INSERT_ID() FROM tdi");
		}
		
		$success = true;
		$msg = 'Tanda Daftar Industri berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'tdi', $objTdi->tdiID, $objTdi->tdiNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objTdi->tdiDmhnID."'");
		
		//Update data perusahaan
		$app->query("UPDATE perusahaan 
					 SET 
					 	 perNPWP='".$objTdi->tdiNPWP."', 
					 	 perNIPIK='".$objTdi->tdiNIPIK."', 
					 	 perKluiID='".$objTdi->tdiKluiID."', 
					 	 perKelurahanLokasiID='".$objTdi->tdiKelurahanLokasiID."', 
					 	 perLuasBangunan='".$objTdi->tdiLuasBangunan."', 
					 	 perLuasTanah='".$objTdi->tdiLuasTanah."', 
					 	 perTenagaPenggerak='".$objTdi->tdiTenagaPenggerak."', 
					 	 perKlasifikasi='".$objTdi->tdiKlasifikasi."', 
					 	 perKomoditiIndustri='".$objTdi->tdiKomoditiIndustri."', 
					 	 perInventaris='".$objTdi->tdiInventaris."', 
					 	 perKapasitasProduksiPerTahun='".$objTdi->tdiKapasitasProduksiPerTahun."', 
					 	 perSatuanKapasitasProduksiPerTahun='".$objTdi->tdiSatuanKapasitasProduksiPerTahun."', 
					 	 perTenagaKerjaIndonesiaLaki='".$objTdi->tdiTenagaKerjaIndonesiaLaki."', 
					 	 perTenagaKerjaIndonesiaPerempuan='".$objTdi->tdiTenagaKerjaIndonesiaPerempuan."', 
					 	 perTenagaKerjaAsingLaki='".$objTdi->tdiTenagaKerjaAsingLaki."', 
					 	 perTenagaKerjaAsingPerempuan='".$objTdi->tdiTenagaKerjaAsingPerempuan."'
					 WHERE perID='".$app->getInt('perID')."'");
	} else {
		$success = false;
		$msg = 'Tanda Daftar Industri tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		if (isset($_REQUEST['save'])) {
			header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
		} else {
			header('Location:index2.php?act=mesin&id='.$objTdi->tdiID.'&success=true&msg='.$msg);
		}
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objTdi->tdiDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Tdi_View::editTdi($success, $msg, $objDetailPermohonan, $objTdi);
	}
}

function viewTdi($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('tdi', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('tdi', 'sort', 'tdiTglPengesahan');
	//$dir		= $app->pageVar('tdi', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('tdi', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('tdi', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(tdiNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(tdiTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM tdi
				  LEFT JOIN detailpermohonan ON tdiDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM tdi
			LEFT JOIN detailpermohonan ON tdiDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(tdiTglPengesahan) DESC, tdiNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Tdi_View::viewTdi($success, $msg, $arr);
}
?>