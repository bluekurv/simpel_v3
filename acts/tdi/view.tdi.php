<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Tdi_View {
	static function editTdi($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdi&task=edit&id=<?php echo $obj->tdiDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Tanda Daftar Industri</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=tdi&task=save" method="POST" >
				<input type="hidden" id="tdiID" name="tdiID" value="<?php echo $obj->tdiID; ?>" />
				<input type="hidden" id="tdiDmhnID" name="tdiDmhnID" value="<?php echo $obj->tdiDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" name="save" class="button-link inline dark-blue" value="Simpan" />
					<input type="submit" name="manage" class="button-link inline dark-blue" value="Kelola Mesin/Peralatan Produksi" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" name="save" class="button-link inline dark-blue" value="Simpan" />
					<input type="submit" name="manage" class="button-link inline dark-blue" value="Kelola Mesin/Peralatan Produksi" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->tdiImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('tdiGrupID', $obj->tdiGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=tdi&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=tdi&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiAlamat" name="tdiAlamat" maxlength="500" size="100" value="<?php echo $obj->tdiAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
				WHERE w.wilID='".$obj->tdiKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$tdiKelurahan = $app->queryObject($sql);
		if (!$tdiKelurahan) {
			$tdiKelurahan = new stdClass();
			$tdiKelurahan->kode = '';
			$tdiKelurahan->value = '';
		}
?>
						<input type="hidden" id="tdiKelurahanID" name="tdiKelurahanID" value="<?php echo $obj->tdiKelurahanID; ?>" />
						<input class="box readonly" id="tdiKelurahanKode" name="tdiKelurahanKode" size="8" value="<?php echo $tdiKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="tdiKelurahan" name="tdiKelurahan" maxlength="500" size="70" value="<?php echo $tdiKelurahan->value; ?>" />
											</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiNPWP" name="tdiNPWP" maxlength="50" size="50" value="<?php echo $obj->tdiNPWP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NIPIK</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdiNIPIK > 0) {
?>
						<input class="box readonly" size="5" value="04160" tabindex="-1" readonly />
						<input class="box" id="tdiNIPIK" name="tdiNIPIK" maxlength="4" size="4" value="<?php echo sprintf('%04d', $obj->tdiNIPIK); ?>" />
<?php 
		} else {
?>
						Terakhir 
						<input type="hidden" id="tdiNIPIK" name="tdiNIPIK" value="<?php echo $obj->tdiNIPIK; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Pabrik</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kepemilikan Pabrik</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="tdiKepemilikanPabrik" name="tdiKepemilikanPabrik">
<?php 
		foreach (array('Milik Sendiri', 'Sewa', 'Lainnya') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdiKepemilikanPabrik) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Pabrik <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->tdiKelurahanLokasiID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$tdiKelurahanLokasi = $app->queryObject($sql);
		if (!$tdiKelurahanLokasi) {
			$tdiKelurahanLokasi = new stdClass();
			$tdiKelurahanLokasi->kode = '';
			$tdiKelurahanLokasi->value = '';
		}
?>
						<input type="hidden" id="tdiKelurahanLokasiID" name="tdiKelurahanLokasiID" value="<?php echo $obj->tdiKelurahanLokasiID; ?>" />
						<input class="box readonly" id="tdiKelurahanLokasiKode" name="tdiKelurahanLokasiKode" size="8" value="<?php echo $tdiKelurahanLokasi->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="tdiKelurahanLokasi" name="tdiKelurahanLokasi" maxlength="500" size="70" value="<?php echo $tdiKelurahanLokasi->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, pergunakan sub menu yang ada di menu <b>Administrasi &gt; Lokasi</b> untuk mengelola data lokasi</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Bangunan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiLuasBangunan" name="tdiLuasBangunan" maxlength="25" size="25" value="<?php echo $app->MySQLToMoney($obj->tdiLuasBangunan,2); ?>" /> <span>m<sup>2</sup></span>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Tanah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiLuasTanah" name="tdiLuasTanah" maxlength="25" size="25" value="<?php echo $app->MySQLToMoney($obj->tdiLuasTanah,2); ?>" /> <span>m<sup>2</sup></span>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Industri (KLUI)</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT kkelKode AS kode, kkelNama AS value 
				FROM kbli
				WHERE kkelID='".$obj->tdiKluiID."'";
		$tdiKlui = $app->queryObject($sql);
		if (!$tdiKlui) {
			$tdiKlui = new stdClass();
			$tdiKlui->kode = '';
			$tdiKlui->value = '';
		}
?>
						<input type="hidden" id="tdiKluiID" name="tdiKluiID" value="<?php echo $obj->tdiKluiID; ?>" />
						<input class="box readonly" id="tdiKluiKode" name="tdiKluiKode" size="8" value="<?php echo $tdiKlui->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="tdiKlui" name="tdiKlui" maxlength="500" size="70" value="<?php echo $tdiKlui->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Klasifikasi TDI</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="tdiKlasifikasi" name="tdiKlasifikasi">
<?php 
		foreach (array('IKAHH', 'IKAIK', 'ILME') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdiKlasifikasi) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Komoditi Industri</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiKomoditiIndustri" name="tdiKomoditiIndustri" maxlength="255" size="50" value="<?php echo $obj->tdiKomoditiIndustri; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tenaga Penggerak</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiTenagaPenggerak" name="tdiTenagaPenggerak" maxlength="255" size="50" value="<?php echo $obj->tdiTenagaPenggerak; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nilai Inventaris (Mesin/Peralatan)</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiInventaris" name="tdiInventaris" maxlength="20" size="25" value="<?php echo $app->MySQLToMoney($obj->tdiInventaris); ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kapasitas Produksi Terpasang Per Tahun</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiKapasitasProduksiPerTahun" name="tdiKapasitasProduksiPerTahun" maxlength="20" size="25" value="<?php echo $app->MySQLToMoney($obj->tdiKapasitasProduksiPerTahun); ?>" />
						dalam satuan
						<input class="box" id="tdiSatuanKapasitasProduksiPerTahun" name="tdiSatuanKapasitasProduksiPerTahun" maxlength="255" size="50" value="<?php echo $obj->tdiSatuanKapasitasProduksiPerTahun; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiKeterangan" name="tdiKeterangan" maxlength="255" size="50" value="<?php echo $obj->tdiKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Tenaga Kerja Indonesia</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Laki-laki</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiTenagaKerjaIndonesiaLaki" name="tdiTenagaKerjaIndonesiaLaki" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->tdiTenagaKerjaIndonesiaLaki); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Perempuan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiTenagaKerjaIndonesiaPerempuan" name="tdiTenagaKerjaIndonesiaPerempuan" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->tdiTenagaKerjaIndonesiaPerempuan); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Tenaga Kerja Asing</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Laki-laki</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiTenagaKerjaAsingLaki" name="tdiTenagaKerjaAsingLaki" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->tdiTenagaKerjaAsingLaki); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Perempuan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdiTenagaKerjaAsingPerempuan" name="tdiTenagaKerjaAsingPerempuan" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->tdiTenagaKerjaAsingPerempuan); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdiNo > 0) {
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/TDI/<span id="spanKlasifikasi"><?php echo $obj->tdiKlasifikasi; ?>/<?php echo substr($obj->tdiTglPengesahan,0,4); ?>/</span>
						<input class="box" id="tdiNo" name="tdiNo" maxlength="5" size="5" value="<?php echo $obj->tdiNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="tdiNo" name="tdiNo" value="<?php echo $obj->tdiNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="tdiTglPengesahan" name="tdiTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdiTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT pnID AS id, pnNama AS nama 
				FROM pengguna 
				WHERE pnLevelAkses='Pejabat' 
				ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
		$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="tdiPejID" name="tdiPejID">
<?php 
		while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
			echo '<option value="'.$objPengesahanOleh->id.'"';
			if ($objPengesahanOleh->id == $obj->tdiPejID) {
				echo ' selected';	
			}
			echo '>'.$objPengesahanOleh->nama.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='tdi'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->tdiTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdiTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="tdiTglBerlaku" name="tdiTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->tdiTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdiTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdiTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="tdiTglDaftarUlang" name="tdiTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->tdiTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/tdi/js/edit.tdi.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printTdi($obj) {
		global $app;
		
		$objReport 		= new Report('Tanda Daftar Industri', 'TDI-'.$obj->tdiKlasifikasi.'-'.substr($obj->tdiTglPengesahan,0,4).'-'.$obj->tdiNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->tdiPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		p, li {
			text-align: justify;
		}
	</style>
	
	<p align="center">
		<span style="font-size:14px;"><b><u>TANDA DAFTAR INDUSTRI</u></b></span><br>
		<span><b>Nomor : <?php echo $obj->tdiNoLengkap; ?></b></span>
	</p>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="20" align="right" valign="top">1.</td>
		<td width="<?php echo ($objReport->width - 30) / 2; ?>" align="left" valign="top">a. Nama Perusahaan</td>
        <td width="10" align="left" valign="top">:</td>
		<td width="<?php echo ($objReport->width - 30) / 2; ?>" align="left" valign="top"><?php echo strtoupper($obj->perNama); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">b. Alamat dan Nomor Telepon</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo strtoupper($obj->tdiAlamat.' / '.$obj->perNoTelp); ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">2.</td>
		<td align="left" valign="top">Nomor Pokok Wajib Pajak (NPWP)</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->tdiNPWP; ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">3.</td>
		<td align="left" valign="top">Nomor Induk Pendaftaran Industri Kecil (NIPIK)</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top">04160<?php echo sprintf('%04d', $obj->tdiNIPIK); ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">4.</td>
		<td align="left" valign="top">a. Nama Pemilik</td>
        <td align="left" valign="top">:</td>
        <td align="left" valign="top"><?php echo strtoupper($obj->mhnNama); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">b. Alamat Pemilik</td>
        <td align="left" valign="top">:</td>
       <td align="left" valign="top"><?php echo strtoupper($obj->mhnAlamat); ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">5.</td>
		<td align="left" valign="top">Jenis Industri (KLUI)</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php $objKlui = $app->queryObject("SELECT * FROM kbli WHERE kkelID='".$obj->tdiKluiID."'"); echo ($objKlui) ? strtoupper($objKlui->kkelNama.' ( '.$objKlui->kkelKode.' )') : ''; ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">6.</td>
		<td align="left" valign="top">Komoditi Industri</td>
        <td align="left" valign="top">:</td>
       	<td align="left" valign="top"><?php echo strtoupper($obj->tdiKomoditiIndustri); ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">7.</td>
		<td align="left" valign="top">Lokasi Pabrik</td>
        <td align="left" valign="top"></td>
		<td align="left" valign="top"></td>
	</tr>
<?php 
		$sql = "SELECT wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilNama AS kabupaten , wilayah4.wilNama AS provinsi 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->tdiKelurahanLokasiID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasi = $app->queryObject($sql);
?>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">a. Desa / Kelurahan</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $objLokasi->kelurahan; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">b. Kecamatan</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $objLokasi->kecamatan; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">c. Kabupaten/Kota</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $objLokasi->kabupaten; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">d. Provinsi</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $objLokasi->provinsi; ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">8.</td>
		<td align="left" valign="top">Mesin / Peralatan Produksi</td>
        <td align="left" valign="top"></td>
		<td align="left" valign="top"></td>
	</tr>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">a. Mesin / Peralatan Produksi</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top">Terlampir</td>
	</tr>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">b. Mesin / Peralatan Utama</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top">Terlampir</td>
	</tr>
	<tr>
		<td align="left" valign="top"></td>
		<td align="left" valign="top">c. Tenaga Penggerak</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->tdiTenagaPenggerak; ?></td>
	</tr>
	<tr>
		<td align="right" valign="top">9.</td>
		<td align="left" valign="top">Nilai Inventaris (Mesin / Peralatan)</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top">Rp. <?php echo $app->MySQLToMoney($obj->tdiInventaris); ?>,-</td>
	</tr>
	<tr>
		<td align="right" valign="top">10.</td>
		<td align="left" valign="top">Kapasitas Produksi Terpasang Per Tahun</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $app->MySQLToMoney($obj->tdiKapasitasProduksiPerTahun).' '.$obj->tdiSatuanKapasitasProduksiPerTahun; ?>/Tahun</td>
	</tr>
	</table>
	
	<p>Pemegang Tanda Daftar Industri ini agar menyampaikan informasi dengan pengisian Formulir Pdf-III-IK pada setiap tahun paling lambat tanggal 31 Januari tahun berikutnya.</p>
	<br><br>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="48%">&nbsp;</td>
		<td width="52%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->tdiTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		$pdf->setCellHeightRatio(1.25); 
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->setCellHeightRatio($objReport->cellHeightRatio); 
		
		ob_start();
?>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="15%"><h3>LAMPIRAN</h3></td><td width="10"><h3>:</h3></td>
		<td width="85%"><h3>TANDA DAFTAR INDUSTRI</h3></td>
	</tr>
	<tr>
		<td><h3>NIPIK</h3></td><td><h3>:</h3></td>
		<td><h3>04160<?php echo sprintf('%04d', $obj->tdiNIPIK); ?></h3></td>
	</tr>
	</table>
	
	&nbsp;<br/>
	<table border="1" cellpadding="3" cellspacing="0" width="100%">
	<tr>
		<td width="30" align="center">No.</td>
		<td width="<?php echo ($objReport->width - 230) / 2; ?>" align="center">Jenis Mesin</td>
		<td width="40" align="center">Jumlah</td>
		<td width="<?php echo ($objReport->width - 230) / 2; ?>" align="center">Merek/Spesifikasi Negara Asal</td>
		<td width="80" align="center">Kapasitas Mesin</td>
		<td width="80" align="center">Harga<br>(Rp)</td>
	</tr>
<?php 
		$sql = "SELECT * FROM  mesin WHERE msnTdiID='".$obj->tdiID."'";
		$rs = $app->query($sql);
		$jumlah = 0;
		$i = 1;
		while(($v = mysql_fetch_object($rs)) == true){
?>
	<tr>
		<td align="right"><?php echo $i; ?>.</td>
		<td><?php echo strtoupper($v->msnJenis); ?></td>
		<td align="center"><?php echo $app->MySQLToMoney($v->msnJumlah); ?></td>
		<td><?php echo strtoupper(trim($v->msnMerek.' '.$v->msnSpesifikasiNegaraAsal)); ?></td>
		<td><?php echo strtoupper($app->MySQLToMoney($v->msnKapasitasMesin, 2).' '.$v->msnSatuanKapasitasMesin); ?></td>
		<td align="right"><?php echo $app->MySQLToMoney($v->msnHarga); ?></td>
	</tr>
<?php
			$jumlah = bcadd($jumlah, $v->msnHarga);
			$i++;
		}
?>
	<tr>
		<td colspan="5" align="right">JUMLAH</td>
		<td align="right"><?php echo $app->MySQLToMoney($jumlah); ?></td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="44%">&nbsp;</td>
		<td width="56%">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->tdiTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readTdi($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdi&task=read&id=<?php echo $obj->tdiDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Tanda Daftar Industri</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=tdi&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=tdi&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">NPWP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdiNPWP; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">NIPIK</td><td>&nbsp;:&nbsp;</td>
			<td>
				04160<?php echo sprintf('%04d', $obj->tdiNIPIK); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Pabrik</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kepemilikan Pabrik</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdiKepemilikanPabrik; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Lokasi Pabrik</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->tdiKelurahanLokasiID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Luas Bangunan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->tdiLuasBangunan,2); ?> m<sup>2</sup>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Luas Tanah</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->tdiLuasTanah,2); ?> m<sup>2</sup>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tenaga Penggerak</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdiTenagaPenggerak; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis Industri (KLUI)</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT kkelKode AS kode, kkelNama AS value 
				FROM kbli
				WHERE kkelID='".$obj->tdiKluiID."'";
		$tdiKlui = $app->queryObject($sql);
		if (!$tdiKlui) {
			$tdiKlui = new stdClass();
			$tdiKlui->kode = '';
			$tdiKlui->value = '';
		}
?>
				<?php echo $tdiKlui->kode.' '.$tdiKlui->value; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Klasifikasi TDI</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdiKlasifikasi; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Komoditi Industri</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdiKomoditiIndustri; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kapasitas Produksi Per Tahun</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->tdiKapasitasProduksiPerTahun); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Satuan Kapasitas Produksi Per Tahun</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdiSatuanKapasitasProduksiPerTahun; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Inventaris</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->tdiInventaris); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Tenaga Kerja</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Indonesia (Laki-laki)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->tdiTenagaKerjaIndonesiaLaki); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Indonesia (Perempuan)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->tdiTenagaKerjaIndonesiaPerempuan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Asing (Laki-laki)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->tdiTenagaKerjaAsingLaki); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Asing (Perempuan)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->tdiTenagaKerjaAsingPerempuan); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdiNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->tdiTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->tdiPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdiTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdiTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdiTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdiTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/tdi/js/read.tdi.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewTdi($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdi"><img src="images/icons/rosette.png" border="0" /> Buku Register Tanda Daftar Industri</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('tdiNo', 'Nomor Surat', true),
			$app->setHeader('tdiTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('tdiTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('tdiTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=tdi&task=read&id='.$v->tdiDmhnID.'&fromact=tdi" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->tdiDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=tdi&task=read&id='.$v->tdiDmhnID.'&fromact=tdi" title="Entri">'.$v->tdiNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->tdiTglPengesahan).'</td>';
				
				if ($v->tdiTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->tdiTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->tdiTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->tdiTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/tdi/js/tdi.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>