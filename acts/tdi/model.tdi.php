<?php
class Tdi_Model {	
	public $tabel = 'tdi';
	public $primaryKey = 'tdiID';
	
	public $tdiID = 0;
	public $tdiDmhnID = 0;
	public $tdiGrupID = 0;
	public $tdiNo = 0;
	public $tdiNoLengkap = '';
	public $tdiAlamat = '';
	public $tdiKelurahanID = 0;
	public $tdiNPWP = '';
	public $tdiNIPIK = 0;
	public $tdiKepemilikanPabrik = '';
	public $tdiKelurahanLokasiID = 0;	
	public $tdiLuasBangunan = 0.00;
	public $tdiLuasTanah = 0.00;
	public $tdiTenagaPenggerak = '';
	public $tdiKluiID = 0;
	public $tdiKlasifikasi = '';
	public $tdiKomoditiIndustri = ''; 
	public $tdiInventaris = 0;
	public $tdiKapasitasProduksiPerTahun = 0;
	public $tdiSatuanKapasitasProduksiPerTahun = '';
	public $tdiTenagaKerjaIndonesiaLaki = 0;
	public $tdiTenagaKerjaIndonesiaPerempuan = 0;
	public $tdiTenagaKerjaAsingLaki = 0;
	public $tdiTenagaKerjaAsingPerempuan = 0;
	public $tdiKeterangan = '';
	public $tdiTglBerlaku = '0000-00-00';
	public $tdiTglDaftarUlang = '0000-00-00';
	public $tdiPejID = 0;
	public $tdiTglPengesahan = '0000-00-00'; 	 	 	 	 	
	public $tdiArsip = '';
}
?>