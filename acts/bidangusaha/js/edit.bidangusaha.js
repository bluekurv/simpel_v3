//Variables
var form = $("#myForm");
var bushKode = $("#bushKode");
var bushNama = $("#bushNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'bushKode' || this.id === undefined) {
		if (bushKode.val().length == 0){
			doSubmit = false;
			bushKode.addClass("error");		
		} else {
			bushKode.removeClass("error");
		}
	}
	
	if (this.id == 'bushNama' || this.id === undefined) {
		if (bushNama.val().length == 0){
			doSubmit = false;
			bushNama.addClass("error");		
		} else {
			bushNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
bushKode.blur(validateForm);
bushNama.blur(validateForm);

bushKode.keyup(validateForm);
bushNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
bushKode.focus();