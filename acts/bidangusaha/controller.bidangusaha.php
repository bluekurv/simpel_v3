<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.bidangusaha.php';
require_once 'view.bidangusaha.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editBidangUsaha($app->id);
		break;
	case 'delete':
		deleteBidangUsaha($app->id);
		break;
	case 'save':
		saveBidangUsaha();
		break;
	default:
		viewBidangUsaha(true, '');
		break;
}

function deleteBidangUsaha($id) {
	global $app;
	
	//Get object
	$objBidangUsaha = $app->queryObject("SELECT * FROM bidangusaha WHERE bushID='".$id."'");
	if (!$objBidangUsaha) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM bidangusaha WHERE bushID='".$id."'");
		
		$success = true;
		$msg = 'Bidang Usaha "'.$objBidangUsaha->bushNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'bidangusaha', $objBidangUsaha->bushID, $objBidangUsaha->bushNama, $msg);
	} else {
		$success = false;
		$msg = 'Bidang Usaha "'.$objBidangUsaha->bushNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewBidangUsaha($success, $msg);
}

function editBidangUsaha($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objBidangUsaha = $app->queryObject("SELECT * FROM bidangusaha WHERE bushID='".$id."'");
		if (!$objBidangUsaha) {
			$app->showPageError();
			exit();
		}
	} else {
		$objBidangUsaha = new BidangUsaha_Model();
	}
	
	BidangUsaha_View::editBidangUsaha(true, '', $objBidangUsaha);
}

function saveBidangUsaha() {
	global $app;
	
	//Create object
	$objBidangUsaha = new BidangUsaha_Model();
	$app->bind($objBidangUsaha);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objBidangUsaha->bushKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objBidangUsaha->bushNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT bushID AS id FROM bidangusaha WHERE bushKode='".$objBidangUsaha->bushKode."'", $objBidangUsaha->bushID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objBidangUsaha->bushKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT bushID AS id FROM bidangusaha WHERE bushNama='".$objBidangUsaha->bushNama."'", $objBidangUsaha->bushID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objBidangUsaha->bushNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objBidangUsaha->bushID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objBidangUsaha);
		} else {
			$sql = $app->createSQLforUpdate($objBidangUsaha);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objBidangUsaha->bushID = $app->queryField("SELECT LAST_INSERT_ID() FROM bidangusaha");
			
			$success = true;
			$msg = 'Bidang Usaha "'.$objBidangUsaha->bushNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'bidangusaha', $objBidangUsaha->bushID, $objBidangUsaha->bushNama, $msg);
		} else {
			$success = true;
			$msg = 'Bidang Usaha "'.$objBidangUsaha->bushNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'bidangusaha', $objBidangUsaha->bushID, $objBidangUsaha->bushNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Bidang Usaha "'.$objBidangUsaha->bushNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewBidangUsaha($success, $msg);
	} else {
		BidangUsaha_View::editBidangUsaha($success, $msg, $objBidangUsaha);
	}
}

function viewBidangUsaha($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('bidangusaha', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('bidangusaha', 'sort', 'bushKode');
	$dir   		= $app->pageVar('bidangusaha', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('bidangusaha', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(bushKode LIKE '%".$nama."%' OR bushNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM bidangusaha
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM bidangusaha
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	BidangUsaha_View::viewBidangUsaha($success, $msg, $arr);
}
?>