<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Pariwisata_View {
	static function editPariwisata($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pariwisata&task=edit&id=<?php echo $obj->pariwisataDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Izin Peruntukan Bidang Usaha Kepariwisataan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=pariwisata&task=save" method="POST" >
				<input type="hidden" id="pariwisataID" name="pariwisataID" value="<?php echo $obj->pariwisataID; ?>" />
				<input type="hidden" id="pariwisataDmhnID" name="pariwisataDmhnID" value="<?php echo $obj->pariwisataDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->pariwisataImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('pariwisataGrupID', $obj->pariwisataGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=pariwisata&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=pariwisata&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->perAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pariwisataNPWPD" name="pariwisataNPWPD" maxlength="255" size="50" value="<?php echo $obj->pariwisataNPWPD; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pariwisataMerekUsahaPerusahaan" name="pariwisataMerekUsahaPerusahaan" maxlength="255" size="50" value="<?php echo $obj->pariwisataMerekUsahaPerusahaan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//$app->createSelect('pariwisataBushID', "SELECT bushID AS id, bushNama AS nama FROM bidangusaha ORDER BY bushNama", $obj->pariwisataBushID, array(0=>''));
?>
						<input class="box" id="pariwisataBidangUsaha" name="pariwisataBidangUsaha" maxlength="255" size="50" value="Rekreasi dan Hiburan Umum" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//$app->createSelect('pariwisataJparID', "SELECT jparID AS id, jparNama AS nama FROM jenispariwisata ORDER BY jparNama", $obj->pariwisataJparID, array(0=>''));
?>
						<input class="box" id="pariwisataJenisUsaha" name="pariwisataJenisUsaha" maxlength="255" size="50" value="<?php echo $obj->pariwisataJenisUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pariwisataLuasUsahaPanjang" name="pariwisataLuasUsahaPanjang" maxlength="11" size="11" value="<?php echo $obj->pariwisataLuasUsahaPanjang; ?>" />
						m x
						<input class="box" id="pariwisataLuasUsahaLebar" name="pariwisataLuasUsahaLebar" maxlength="11" size="11" value="<?php echo $obj->pariwisataLuasUsahaLebar; ?>" />
						m
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pariwisataLokasiUsaha" name="pariwisataLokasiUsaha" maxlength="255" size="50" value="<?php echo $obj->pariwisataLokasiUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->pariwisataKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$pariwisataKelurahan = $app->queryObject($sql);
		if (!$pariwisataKelurahan) {
			$pariwisataKelurahan = new stdClass();
			$pariwisataKelurahan->kode = '';
			$pariwisataKelurahan->value = '';
		}
?>
						<input type="hidden" id="pariwisataKelurahanID" name="pariwisataKelurahanID" value="<?php echo $obj->pariwisataKelurahanID; ?>" />
						<input class="box readonly" id="pariwisataKelurahanKode" name="pariwisataKelurahanKode" size="8" value="<?php echo $pariwisataKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="pariwisataKelurahan" name="pariwisataKelurahan" maxlength="500" size="70" value="<?php echo $pariwisataKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pariwisataKeterangan" name="pariwisataKeterangan" maxlength="255" size="50" value="<?php echo $obj->pariwisataKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->pariwisataNo > 0) {
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/IU-RH/<?php echo substr($obj->pariwisataTglPengesahan,0,4); ?>/</span>
						<input class="box" id="pariwisataNo" name="pariwisataNo" maxlength="5" size="5" value="<?php echo $obj->pariwisataNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="pariwisataNo" name="pariwisataNo" value="<?php echo $obj->pariwisataNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="pariwisataTglPengesahan" name="pariwisataTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->pariwisataTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="pariwisataPejID" name="pariwisataPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->pariwisataPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='pariwisata'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->pariwisataTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->pariwisataTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="pariwisataTglBerlaku" name="pariwisataTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->pariwisataTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->pariwisataTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->pariwisataTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="pariwisataTglDaftarUlang" name="pariwisataTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->pariwisataTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jspariwisata/edit.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printPariwisata($obj) {
		global $app;
		
		$objReport 		= new Report('Izin Pariwisata', 'IP-'.substr($obj->pariwisataTglPengesahan,0,4).'-'.$obj->pariwisataNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->pariwisataPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<h4 align="center">
		KEPUTUSAN KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?><br>
		NOMOR : <?php echo $obj->pariwisataNoLengkap; ?><br>
		TENTANG<br>
		IZIN USAHA REKREASI DAN HIBURAN UMUM<br>
		<br>
		KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?>,
	</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			Surat Permohonan dari Sdr <?php echo strtoupper($obj->mhnNama); ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> tentang untuk mendapatkan Izin Usaha Rekreasi dan Hiburan Umum.
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">
			<table>
				<tr><td width="4%">a.</td><td width="96%">Bahwa pengendalian, penertiban dan pengawasan bidang usaha perlu diatur oleh Pemerintah Daerah.</td></tr>
				<tr><td>b.</td><td>Bahwa untuk memenuhi persyaratan pengurusan selanjutnya dari usaha yang bersangkutan, maka perlu izin dari Kepala <?php echo CFG_COMPANY_SHORT_NAME; ?> Kabupaten Pelalawan.</td></tr>
				<tr><td>c.</td><td>Bahwa setelah Usaha yang bersangkutan telah memenuhi persyaratan untuk diberikan izin dalam usaha dimaksud.</td></tr>
				<tr><td>d.</td><td>Bahwa untuk maksud tersebut di atas perlu ditetapkan dalam suatu keputusan Kepala <?php echo CFG_COMPANY_SHORT_NAME; ?> Kabupaten Pelalawan.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
<?php 
		echo $konfigurasi['consideringPariwisata'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">PERTAMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Memberikan Izin Usaha Rekreasi dan Hiburan Umum kepada :</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">
			<table width="100px" height="100px" border="0">
			<tr>
				<td>
					<img src="<?php echo CFG_LIVE_PATH; ?>/images/report/3x4.png" width="93" height="122">
				</td>
			</tr>
			</table>
		</td>
		<td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0">
			<tr>
				<td align="left" valign="top" width="38%">Nama</td>
				<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="60%"><?php echo strtoupper($obj->mhnNama); ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">No. KTP</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnNoKtp; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Bidang Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pariwisataBidangUsaha; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Jenis Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pariwisataJenisUsaha; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Merek</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pariwisataMerekUsahaPerusahaan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Nomor NPWPD</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pariwisataNPWPD; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Tempat Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pariwisataLokasiUsaha; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Kelurahan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->kelurahan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Kecamatan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->kecamatan; ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEDUA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			Pemegang Izin Harus memenuhi ketentuan sebagai berikut :
		</td>
	</tr>
	<tr>
		<td width="20%"></td><td width="7"></td>
		<td width="75%">
			<table>
				<tr><td width="4%">1.</td><td width="96%">Pengusaha ybs harus menjaga ketertiban, keamanan, dan ketentraman umum.</td></tr>
				<tr><td>2.</td><td>Pengusaha ybs harus menjaga kebersihan tempat usaha.</td></tr>
				<tr><td>3.</td><td>Pengusaha ybs harus mematuhi ketentuan/petunjuk yang ada hubungannya dengan kegiatan usahanya.</td></tr>
				<tr><td>4.</td><td>Pengusaha ybs wajib menyampaikan laporan statistik pengunjung secara berkala sesuai ketentuan yang berlaku.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KETIGA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Izin ini berlaku sampai dengan <?php echo $app->MySQLDateToIndonesia(date('Y-m-d', strtotime($obj->pariwisataTglDaftarUlang." -1 days"))); ?>.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEEMPAT</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Surat Izin ini dicabut kembali, apabila yang bersangkutan tidak mentaati ketentuan yang berlaku.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KELIMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Keputusan ini mulai berlaku sejak tanggal ditetapkan.</td>
	</tr>
	</table>

	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="44%">&nbsp;</td>
		<td width="56%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->pariwisataTglPengesahan)); ?>
		</td>
	</tr>
	</table>

	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:8px">
	<tr>
		<td width="50">Tembusan :</td>
		<td>Disampaikan Kepada Yth. :</td>
	</tr>
	<tr>
		<td></td>
		<td>1. Sekda <?php echo $objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>2. Dispenda <?php echo $objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>3. Kepala BLH <?php echo $objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>4. Satpol PP <?php echo $objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?></td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readPariwisata($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pariwisata&task=read&id=<?php echo $obj->pariwisataDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Peruntukan Bidang Usaha Kepariwisataan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoUrutLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pariwisataNPWPD; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis Pariwisata</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		echo $app->queryField("SELECT jparNama FROM jenispariwisata WHERE jparID='".$obj->pariwisataJparID."'");
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		echo $app->queryField("SELECT bushNama FROM bidangusaha WHERE bushID='".$obj->pariwisataBushID."'");
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pariwisataMerekUsahaPerusahaan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Luas Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pariwisataLuasUsahaPanjang; ?> m x <?php echo $obj->pariwisataLuasUsahaLebar; ?> m
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Lokasi Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pariwisataLokasiUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->pariwisataKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pariwisataKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pariwisataNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->pariwisataTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->pariwisataPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->pariwisataTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->pariwisataTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->pariwisataTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->pariwisataTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jspariwisata/read.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewPariwisata($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pariwisata"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Usaha Pariwisata</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('pariwisataNo', 'Nomor Surat', true),
			$app->setHeader('pariwisataTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('pariwisataTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('pariwisataTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=pariwisata&task=read&id='.$v->pariwisataDmhnID.'&fromact=pariwisata" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->pariwisataDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=pariwisata&task=read&id='.$v->pariwisataDmhnID.'&fromact=pariwisata" title="Entri">'.$v->pariwisataNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->pariwisataTglPengesahan).'</td>';
				
				if ($v->pariwisataTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->pariwisataTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->pariwisataTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->pariwisataTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/jspariwisata/view.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>