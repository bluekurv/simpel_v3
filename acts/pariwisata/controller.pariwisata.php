<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.pariwisata.php';
require_once 'view.pariwisata.php';

switch ($app->task) {
	case 'edit':
		editPariwisata($app->id);
		break;
	case 'print':
		printPariwisata($app->id);
		break;
	case 'read':
		readPariwisata($app->id);
		break;
	case 'save':
		savePariwisata();
		break;
	default:
		viewPariwisata(true, '');
		break;
}

function editPariwisata($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objPariwisata = $app->queryObject("SELECT * FROM pariwisata WHERE pariwisataDmhnID='".$id."'");
	if (!$objPariwisata) {
		$objPariwisata = new Pariwisata_Model();
		
		//Ambil nilai default
		//Kecuali pariwisataMerekUsahaPerusahaan dan pariwisataKelurahanID
		$objPariwisata->pariwisataNPWPD = $objDetailPermohonan->perNPWPD;
	}
	
	$objPariwisata->pariwisataDmhnID = $objDetailPermohonan->dmhnID;
	
	Pariwisata_View::editPariwisata(true, '', $objDetailPermohonan, $objPariwisata);
}

function printPariwisata($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN pariwisata ON pariwisataDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objPariwisata = $app->queryObject($sql);
	if (!$objPariwisata) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objPariwisata->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objPariwisata->pariwisataKelurahanID."'");
	$objPariwisata->kelurahan = $objKelurahan->wilNama;
	
	$objKecamatan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");
	$objPariwisata->kecamatan = $objKecamatan->wilNama;
	
	$objPariwisata->kabupaten = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKecamatan->wilParentID."'");
	
	Pariwisata_View::printPariwisata($objPariwisata);
}

function readPariwisata($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN pariwisata ON pariwisataDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objPariwisata = $app->queryObject($sql);
	if (!$objPariwisata) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objPariwisata->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Pariwisata_View::readPariwisata($objPariwisata);
}

function savePariwisata() {
	global $app;
	
	//Get object
	$objPariwisataSebelumnya = $app->queryObject("SELECT * FROM pariwisata WHERE pariwisataID='".$app->getInt('pariwisataID')."'");
	if (!$objPariwisataSebelumnya) {
		$objPariwisataSebelumnya = new Pariwisata_Model();
	}
	
	//Create object
	$objPariwisata = new Pariwisata_Model();
	$app->bind($objPariwisata);
	
	//Modify object (if necessary)
	$objPariwisata->pariwisataTglPengesahan = $app->NormalDateToMySQL($objPariwisata->pariwisataTglPengesahan);
	if ($objPariwisata->pariwisataTglPengesahan != '0000-00-00') {
		if ($objPariwisataSebelumnya->pariwisataNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objPariwisata->pariwisataNo = intval($app->queryField("SELECT MAX(pariwisataNo) AS nomor FROM pariwisata WHERE YEAR(pariwisataTglPengesahan)='".substr($objPariwisata->pariwisataTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objPariwisata->pariwisataTglBerlaku = date('Y-m-d', strtotime($objPariwisata->pariwisataTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objPariwisata->pariwisataTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objPariwisata->pariwisataTglDaftarUlang = date('Y-m-d', strtotime($objPariwisata->pariwisataTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objPariwisata->pariwisataTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objPariwisata->pariwisataNo = 0;
		
		$objPariwisata->pariwisataTglBerlaku = '0000-00-00';
		$objPariwisata->pariwisataTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objPariwisata->pariwisataNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/IU-RH/'.substr($objPariwisata->pariwisataTglPengesahan,0,4).'/'.$objPariwisata->pariwisataNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objPariwisata->pariwisataNo > 0){
		$isExist = $app->isExist("SELECT pariwisataID AS id FROM pariwisata WHERE pariwisataNo='".$objPariwisata->pariwisataNo."' AND YEAR(pariwisataTglPengesahan)='".substr($objPariwisata->pariwisataTglPengesahan,0,4)."'", $objPariwisata->pariwisataID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objPariwisata->pariwisataNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objPariwisata->pariwisataID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objPariwisata);
		} else {
			$sql = $app->createSQLforUpdate($objPariwisata);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objPariwisata->pariwisataID = $app->queryField("SELECT LAST_INSERT_ID() FROM pariwisata");
		} 
		
		$success = true;
		$msg = 'Izin Peruntukan Bidang Usaha Kepariwisataan berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'pariwisata', $objPariwisata->pariwisataID, $objPariwisata->pariwisataNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objPariwisata->pariwisataDmhnID."'");
	} else {
		$success = false;
		$msg = 'Izin Peruntukan Bidang Usaha Kepariwisataan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objPariwisata->pariwisataDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Pariwisata_View::editPariwisata($success, $msg, $objDetailPermohonan, $objPariwisata);
	}
}

function viewPariwisata($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('pariwisata', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('pariwisata', 'sort', 'pariwisataTglPengesahan');
	//$dir		= $app->pageVar('pariwisata', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('pariwisata', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('pariwisata', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(pariwisataNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(pariwisataTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM pariwisata
				  LEFT JOIN detailpermohonan ON pariwisataDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM pariwisata
			LEFT JOIN detailpermohonan ON pariwisataDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(pariwisataTglPengesahan) DESC, pariwisataNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Pariwisata_View::viewPariwisata($success, $msg, $arr);
}
?>