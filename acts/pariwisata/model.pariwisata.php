<?php
class Pariwisata_Model {	
	public $tabel = 'pariwisata';
	public $primaryKey = 'pariwisataID';
	
	public $pariwisataID = 0;
	public $pariwisataDmhnID = 0;
	public $pariwisataGrupID = 0;
	public $pariwisataNo = 0;
	public $pariwisataNoLengkap = '';
	public $pariwisataNPWPD = '';
	public $pariwisataJparID = 0;
	public $pariwisataBushID = 0;
	public $pariwisataJenisUsaha = '';
	public $pariwisataBidangUsaha = '';
	public $pariwisataMerekUsahaPerusahaan = '';
	public $pariwisataLuasUsahaPanjang = 0;
	public $pariwisataLuasUsahaLebar = 0;
	public $pariwisataLokasiUsaha = '';
	public $pariwisataKelurahanID = 0;
	public $pariwisataKeterangan = '';
	public $pariwisataTglBerlaku = '0000-00-00'; 
	public $pariwisataTglDaftarUlang = '0000-00-00';
	public $pariwisataPejID = 0;
	public $pariwisataTglPengesahan = '0000-00-00'; 	 	 	 	 	
	public $pariwisataArsip = '';
}
?>