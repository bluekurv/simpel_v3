<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

switch ($app->task) {
	case 'sql':
		sqlKonfigurasi();
		break;
	case 'savemap':
		savemapKonfigurasi();
		break;
	case 'map':
		mapKonfigurasi();
		break;
	default:
		viewKonfigurasi();
		break;
}

function debug($v) {
	echo '<pre>';
	print_r($v);
	echo '</pre>';
}

function debux($v) {
	debug($v);
	exit();
}

function sqlKonfigurasi() {
	global $app;
	
	$map = array(
		'situ'=>'situ',
		'situho'=>'situho',
		'siup'=>'siup',
		'tdi'=>'tdi',
		'tdp'=>'tdp',
		'pariwisata'=>'pariwisata',
		'imb'=>'imb',
		'reklame'=>'reklame',
		'pemecahanimb'=>'pemecahanimb',
		'airtanah'=>'air',
		'airpermukaan'=>'air',
		'tduk'=>'tduk',
		'budidayaperkebunan'=>'bp',
		'tdg'=>'tdg'
	);

	//'iuhiburan'=>'iuhiburan',
	//'tduhotel'=>'tduhotel',
	$records = array();
	
	foreach ($map as $k=>$v) {
		$sql = "SELECT {$v}NoLengkap AS no, {$v}TglPengesahan AS tgl, {$v}DmhnID AS id
				FROM {$k} 
				WHERE YEAR({$v}TglPengesahan)>=2015";
		$records[$k] = $app->queryArrayOfObjects($sql);
	}
	
	//Ambil jenissuratizin yang perlu dipetakan
	$sql = "SELECT dmhnID, mhnNama, mhnTgl, jsiID, jsiNama, jsiKode, perNama
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON mhnPerID=perID
			WHERE dmhnTelahSelesai=1 AND YEAR(dmhnTglSelesai)>=2015";
	$result = $app->query($sql);
	$jenissuratizin = array();
	
	while(($obj = mysql_fetch_object($result)) == true){
		$jenissuratizin[] = $obj;
	}
	
	$sql = "SELECT *, jsiID AS id
			FROM jenissuratizintemporary";
	$jenissuratizin2 = $app->queryArrayOfObjects($sql);
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=impor2&task=sql"><img src="images/icons/rosette.png" border="0" /> Generate SQL</a></h1>
	</div>
	<div class="halamanTengah">
<?php	
	$sql = "INSERT INTO ptsp (ptspNama, ptspNamaPerusahaan, ptspBidang, ptspStatus, ptspIzinUsaha, ptspTgl, ptspTglPenerbitan, ptspNoReg) ";
	$arr = array();
	
	if (count($jenissuratizin) > 0) {
		foreach ($jenissuratizin as $v) {
			if (isset($map[$v->jsiKode]) && isset($records[$v->jsiKode][$v->dmhnID])) {
				$arr[] = "('".$v->mhnNama."', 
				'".$v->perNama."', 
				'".$jenissuratizin2[$v->jsiID]->bidang."', 
				'".$jenissuratizin2[$v->jsiID]->status."', 
				'".$v->jsiNama."', 
				'".$v->mhnTgl."', 
				'".$records[$v->jsiKode][$v->dmhnID]->tgl."', 
				'".$records[$v->jsiKode][$v->dmhnID]->no."')";
			}
		}
	}
	
	if (count($arr) > 0) {
		$sql .= " VALUES ".implode(",<br>", $arr);
		echo $sql.";";
	}
?>
	</div>
	<div class="halamanBawah"></div>
<?php
}

function savemapKonfigurasi() {
	global $app;
	
	$sql = "DROP TABLE IF EXISTS jenissuratizintemporary";
	$app->query($sql);
	
	$sql = "CREATE TABLE `jenissuratizintemporary` (
	  `jsiID` int(11) NOT NULL,
	  `bidang` varchar(500) NOT NULL,
	  `status` varchar(500) NOT NULL
	) ENGINE=InnoDB DEFAULT CHARSET=latin1";
	$app->query($sql);
	
	foreach ($_REQUEST['bidang'] as $k=>$v) {
		$bidang = $v;
		$status = $_REQUEST['status'][$k];
		
		$sql = "INSERT INTO jenissuratizintemporary (jsiID, bidang, status) VALUES ('".$k."', '".$bidang."', '".$status."')";
		$app->query($sql);
	}
	
	header('location:index2.php?act=impor2');
}

function mapKonfigurasi() {
	global $app;
	
	$sql = "SELECT *, jsiID AS id
			FROM jenissuratizin";
	$jenissuratizin = $app->queryArrayOfObjects($sql);
	
	//Ambil jenissuratizin yang perlu dipetakan
	$sql = "SELECT jsiID
			FROM permohonan, detailpermohonan, jenissuratizin
			WHERE dmhnMhnID=mhnID AND dmhnTelahSelesai=1 AND YEAR(dmhnTglSelesai)>=2015 AND dmhnJsiID=jsiID
			GROUP BY jsiID";
	$result = $app->query($sql);
	$jenissuratizin2 = array();
	
	while(($obj = mysql_fetch_object($result)) == true){
		$jenissuratizin2[] = $obj->jsiID;
	}
	
	$sql = "SELECT *, jsiID AS id
			FROM jenissuratizintemporary";
	$jenissuratizin3 = $app->queryArrayOfObjects($sql);
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=impor2&task=map"><img src="images/icons/rosette.png" border="0" /> Perbarui Data Mapping</a></h1>
	</div>
	<div class="halamanTengah">
		<form action="index2.php?act=impor2&task=savemap" method="post">
				<table border="1" style="margin-top:10px;">
				<tr>
					<th>No</th>
					<th>Jenis Surat Izin</th>
					<th>Bidang</th>
					<th>Jenis</th>
				</tr>
<?php 
	$bidang = array(
		"Tata Ruang",
		"Pertanian",
		"Pemadam Kebakaran",
		"Pendidikan",
		"Peternakan",
		"Perkebunan",
		"Pertanahan",
		"Perdagangan",
		"Pemerintah Daerah",
		"Perikanan",
		"Kesbangpol",
		"Ketenagakerjaan",
		"Kehutanan",
		"ESDM",
		"kesehatan",
		"Penanaman Modal",
		"Lingkungan Hidup",
		"Perhubungan",
		"Sosial",
		"Koperasi UKM",
		"Kominfo",
		"Bina Marga",
		"Budaya dan Pariwisata",
		"Cipta Karya"
	);

	$status = array(
		"perizinan",
		"nonperizinan"
	);
	
	if (count($jenissuratizin2) > 0) {
		$i = 1;
		foreach ($jenissuratizin2 as $v) {
?>
				<tr>
					<td align="right"><?php echo $i; ?></td>
					<td><?php echo $jenissuratizin[$v]->jsiNama; ?></td>
					<td>
						<select name="bidang[<?php echo $jenissuratizin[$v]->jsiID; ?>]" style="padding:0px; font-size:12px; font-family:Calibri;">
						<option></option>
<?php 
			foreach ($bidang as $v2) {
?>
						<option <?php echo isset($jenissuratizin3[$v]) ? ( $v2 == $jenissuratizin3[$v]->bidang ? 'selected' : '' ): ''; ?>><?php echo $v2; ?></option>
<?php
			}
?>
						</select>
					</td>
					<td>
						<select name="status[<?php echo $jenissuratizin[$v]->jsiID; ?>]" style="padding:0px; font-size:12px; font-family:Calibri;">
						<option></option>
<?php 
			foreach ($status as $v2) {
?>
						<option <?php echo isset($jenissuratizin3[$v]) ? ( $v2 == $jenissuratizin3[$v]->status ? 'selected' : '' ): ''; ?>><?php echo $v2; ?></option>
<?php
			}
?>
						</select>
					</td>
				</tr>
<?php
			$i++;
		}
	}
?>
				</table>
				<br>
				<input type="submit" value="Simpan">
		</form>
	</div>
	<div class="halamanBawah"></div>
<?php
}

function viewKonfigurasi() {
	global $app;
	
	$sql = "SELECT jenissuratizin.*, jenissuratizin.jsiID AS id, jenissuratizintemporary.bidang, jenissuratizintemporary.status
			FROM jenissuratizin, jenissuratizintemporary
			WHERE jenissuratizin.jsiID=jenissuratizintemporary.jsiID";
	$jenissuratizin = $app->queryArrayOfObjects($sql);
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=impor2"><img src="images/icons/rosette.png" border="0" /> Konversi Data</a></h1>
	</div>
	<div class="halamanTengah">
		<ol>
			<li>
				<a href="index2.php?act=impor2&task=map">Perbarui data mapping</a>
				<table border="1" style="margin:10px;">
				<tr>
					<th>No</th>
					<th>Jenis Surat Izin</th>
					<th>Kode Surat Izin</th>
					<th>Bidang</th>
					<th>Jenis</th>
				</tr>
<?php 
	if (count($jenissuratizin) > 0) {
		$i = 1;
		foreach ($jenissuratizin as $v) {
?>
				<tr>
					<td align="right"><?php echo $i; ?></td>
					<td><?php echo $v->jsiNama; ?></td>
					<td><?php echo $v->jsiKode; ?></td>
					<td><?php echo $v->bidang; ?></td>
					<td><?php echo $v->status; ?></td>
				</tr>
<?php
			$i++;
		}
	}
?>
				</table>
			</li>
			<li>
				<a href="index2.php?act=impor2&task=sql">Generate SQL</a>
			</li>
		</ol>
	</div>
	<div class="halamanBawah"></div>
<?php
}
?>