//Variables
var form = $("#myForm");
var imbKelurahan = $("#imbKelurahan");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'imbKelurahan' || this.id === undefined) {
		if (imbKelurahan.val().length == 0) {
			doSubmit = false;
			imbKelurahan.addClass("error");		
		} else {
			imbKelurahan.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

function hapusPungutan(){ 
	if (confirm("Hapus pungutan?")) {
		var par = $(this).parent().parent().parent(); 
		par.remove();
	}
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

imbKelurahan.blur(validateForm);

imbKelurahan.keyup(validateForm);

form.submit(submitForm);

$('#imbKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#imbKelurahanID').val(suggestion.data.id);
		$('#imbKelurahanKode').val(suggestion.data.kode);
		$('#imbKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$("#btnTambahPungutan").click(function() {
	$("#pungutan").append( 
			'<tr>' + 
			'<td style="padding-left:20px; border-bottom: dotted 1px #BBBBBB;">' + 
			'<div>' + 
			'<img src="images/icons/delete2.png" id="btnHapusPungutan' + elmCounter + '" class="btnHapusPungutan' + elmCounter + '" title="Hapus Pungutan" /> ' + 
			'<input class="box bulat" id="pimbNo\[' + elmCounter + '\]" name="pimbNo\[' + elmCounter + '\]" maxlength="2" size="2" value="' + (elmCounter + 1) + '" placeholder="No" /> ' + 
			'<input class="box" id="pimbKelompok\[' + elmCounter + '\]" name="pimbKelompok\[' + elmCounter + '\]" maxlength="500" size="8" value="" placeholder="Kelompok" />' + 
			'</div>' + 
			'<input class="box" id="pimbNama\[' + elmCounter + '\]" name="pimbNama\[' + elmCounter + '\]" maxlength="500" size="19" value="" placeholder="Nama Pungutan" />' + 
			'</td>' + 
			'<td style="border-bottom: dotted 1px #BBBBBB;">&nbsp;:&nbsp;</td>' + 
			'<td style="border-bottom: dotted 1px #BBBBBB;">' + 
			'<input class="box desimal1" id="pimbNilai1\[' + elmCounter + '\]" name="pimbNilai1\[' + elmCounter + '\]" maxlength="11" size="6" value="" placeholder="Nilai" /> ' + 
			'<input class="box" id="pimbSatuan1\[' + elmCounter + '\]" name="pimbSatuan1\[' + elmCounter + '\]" maxlength="11" size="4" value="" placeholder="Satuan" /> ' + 
			'x ' + 
			'<input class="box desimal1" id="pimbNilai2\[' + elmCounter + '\]" name="pimbNilai2\[' + elmCounter + '\]" maxlength="11" size="6" value="" placeholder="Nilai" /> ' + 
			'<input class="box" id="pimbSatuan2\[' + elmCounter + '\]" name="pimbSatuan2\[' + elmCounter + '\]" maxlength="11" size="4" value="" placeholder="Satuan" /> ' + 
			'x ' + 
			'<input class="box desimal1" id="pimbNilai3\[' + elmCounter + '\]" name="pimbNilai3\[' + elmCounter + '\]" maxlength="11" size="6" value="" placeholder="Nilai" /> ' + 
			'<input class="box" id="pimbSatuan3\[' + elmCounter + '\]" name="pimbSatuan3\[' + elmCounter + '\]" maxlength="11" size="4" value="" placeholder="Satuan" /> ' + 
			'= ' +  
			'<input class="box" id="pimbSatuanLuas\[' + elmCounter + '\]" name="pimbSatuanLuas\[' + elmCounter + '\]" maxlength="50" size="10" value="" placeholder="Satuan Luas" /> ' + 
			'x Rp. ' + 
			'<input class="box bulat" id="pimbNilaiRupiah\[' + elmCounter + '\]" name="pimbNilaiRupiah\[' + elmCounter + '\]" maxlength="11" size="11" value="" /> ' + 
			'x ' + 
			'<input class="box bulat" id="pimbUnit\[' + elmCounter + '\]" name="pimbUnit\[' + elmCounter + '\]" maxlength="11" size="6" value="1" placeholder="Unit" /> unit' + 
			'</td>' + 
			'</tr>');
	
	$('#btnHapusPungutan' + elmCounter).bind("click", hapusPungutan);
	elmCounter++;
});

$(".btnHapusPungutan").bind("click", hapusPungutan);

//Set focus
$("#imbNoSertifikat").focus();