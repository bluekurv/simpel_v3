<?php
class IMB_Model {	
	public $tabel = 'imb';
	public $primaryKey = 'imbID';
	
	public $imbID = 0;
	public $imbDmhnID = 0;
	public $imbGrupID = 0;
	public $imbNo = 0;
	public $imbNoLengkap = '';
	public $imbNoSertifikat = '';
	public $imbTglSertifikat = '';
	public $imbHakTanah = '';
	public $imbAtasNama = '';
	public $imbJabatan = '';
	public $imbUntukMembangun = '';
	public $imbRab = 0.00;
	public $imbNoBAP = '';
	public $imbTglBAP = '0000-00-00';
	public $imbNoIP = '';
	public $imbTglIP = '0000-00-00';
	public $imbLokasi = '';
	public $imbKelurahanID = 0;
	public $imbBatasUtara = '';
	public $imbBatasSelatan = '';
	public $imbBatasBarat = '';
	public $imbBatasTimur = '';
	public $imbJarakDariJalan = '';
	public $imbKonstruksiBangunan = '';
	public $imbBahanPondasi = '';
	public $imbKerangkaAtasBawah = '';
	public $imbBahanLantai = '';
	public $imbBahanDinding = '';
	public $imbBahanLoteng = '';
	public $imbBahanAtap = '';
	public $imbLuasBangunan = '';
	public $imbJumlahLantai = '';
	public $imbPanjangPagar = '';	 	 	
	public $imbJenisPagar = '';
	public $imbJumlahUnit = 0;
	public $imbJenis = 'Baru';		//'Baru','Pecah Baru','Lama','Pecah Lama'
	public $imbParentID = 0;		//IMB Induk
	public $imbRekomendasiCamat = '';
	public $imbKeterangan = '';
	public $imbTglBerlaku = '0000-00-00';
	public $imbTglDaftarUlang = '0000-00-00';
	public $imbPejID = 0;
	public $imbTglPengesahan = '0000-00-00'; 		 	 	 	 	
	public $imbArsip = '';
	public $imbPungutanNama = '';
	public $imbPungutanJumlah = 0;
	public $imbPungutanBendaharaID = 0;
	public $imbPungutanMengetahuiID = 0;
}

class PungutanIMB_Model {
	public $pimbImbID = '';
	public $pimbNama = '';
	public $pimbKelompok = '';
	public $pimbNilai1 = '';
	public $pimbSatuan1 = '';
	public $pimbNilai2 = '';
	public $pimbSatuan2 = '';
	public $pimbNilai3 = '';
	public $pimbSatuan3 = '';
	public $pimbSatuanLuas = '';
	public $pimbNilaiRupiah = '';
	public $pimbUnit = 1;
}
?>