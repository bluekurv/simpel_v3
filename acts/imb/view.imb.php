<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class IMB_View {
	static function editIMB($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=imb&task=edit&id=<?php echo $obj->imbDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Izin Mendirikan Bangunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=imb&task=save" method="POST" >
				<input type="hidden" id="imbID" name="imbID" value="<?php echo $obj->imbID; ?>" />
				<input type="hidden" id="imbDmhnID" name="imbDmhnID" value="<?php echo $obj->imbDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->imbImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table id="entriTable" class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('imbGrupID', $obj->imbGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Umum Bangunan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. dan Tgl. Sertifikat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="imbNoSertifikat" name="imbNoSertifikat" cols="80" rows="5"><?php echo $obj->imbNoSertifikat; ?></textarea>
					</td>
				</tr>
				<!-- <tr>
					<td style="padding-left:20px;">Tgl. Sertifikat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date" id="imbTglSertifikat" name="imbTglSertifikat" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->imbTglSertifikat); ?>">
					</td>
				</tr> -->
				<tr>
					<td style="padding-left:20px;">Hak Tanah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbHakTanah" name="imbHakTanah" maxlength="255" size="50" value="<?php echo $obj->imbHakTanah; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Atas Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbAtasNama" name="imbAtasNama" maxlength="255" size="50" value="<?php echo $obj->imbAtasNama; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jabatan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbJabatan" name="imbJabatan" maxlength="255" size="50" value="<?php echo $obj->imbJabatan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Untuk Membangun</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbUntukMembangun" name="imbUntukMembangun" maxlength="255" size="50" value="<?php echo $obj->imbUntukMembangun; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">RAB Pembangunan (Rp)</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbRab" name="imbRab" maxlength="255" size="50" value="<?php echo $obj->imbRab; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. BAP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbNoBAP" name="imbNoBAP" maxlength="255" size="50" value="<?php echo $obj->imbNoBAP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. BAP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date" id="imbTglBAP" name="imbTglBAP" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->imbTglBAP); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. IP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbNoIP" name="imbNoIP" maxlength="255" size="50" value="<?php echo $obj->imbNoIP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. IP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date" id="imbTglIP" name="imbTglIP" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->imbTglIP); ?>">
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Posisi Bangunan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbLokasi" name="imbLokasi" maxlength="255" size="50" value="<?php echo $obj->imbLokasi; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->imbKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$imbKelurahan = $app->queryObject($sql);
		if (!$imbKelurahan) {
			$imbKelurahan = new stdClass();
			$imbKelurahan->kode = '';
			$imbKelurahan->value = '';
		}
?>
						<input type="hidden" id="imbKelurahanID" name="imbKelurahanID" value="<?php echo $obj->imbKelurahanID; ?>" />
						<input class="box readonly" id="imbKelurahanKode" name="imbKelurahanKode" size="8" value="<?php echo $imbKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="imbKelurahan" name="imbKelurahan" maxlength="500" size="70" value="<?php echo $imbKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Batas Utara</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBatasUtara" name="imbBatasUtara" maxlength="255" size="50" value="<?php echo $obj->imbBatasUtara; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Batas Selatan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBatasSelatan" name="imbBatasSelatan" maxlength="255" size="50" value="<?php echo $obj->imbBatasSelatan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Batas Barat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBatasBarat" name="imbBatasBarat" maxlength="255" size="50" value="<?php echo $obj->imbBatasBarat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Batas Timur</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBatasTimur" name="imbBatasTimur" maxlength="255" size="50" value="<?php echo $obj->imbBatasTimur; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jarak dari Jalan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbJarakDariJalan" name="imbJarakDariJalan" maxlength="255" size="11" value="<?php echo $obj->imbJarakDariJalan; ?>" /> m
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Struktur Bangunan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Konstruksi Bangunan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbKonstruksiBangunan" name="imbKonstruksiBangunan" maxlength="255" size="50" value="<?php echo $obj->imbKonstruksiBangunan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bahan Pondasi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBahanPondasi" name="imbBahanPondasi" maxlength="255" size="50" value="<?php echo $obj->imbBahanPondasi; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kerangka Atas/Bawah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbKerangkaAtasBawah" name="imbKerangkaAtasBawah" maxlength="255" size="50" value="<?php echo $obj->imbKerangkaAtasBawah; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bahan Lantai</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBahanLantai" name="imbBahanLantai" maxlength="255" size="50" value="<?php echo $obj->imbBahanLantai; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bahan Dinding</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBahanDinding" name="imbBahanDinding" maxlength="255" size="50" value="<?php echo $obj->imbBahanDinding; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bahan Loteng</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBahanLoteng" name="imbBahanLoteng" maxlength="255" size="50" value="<?php echo $obj->imbBahanLoteng; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bahan Atap</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbBahanAtap" name="imbBahanAtap" maxlength="255" size="50" value="<?php echo $obj->imbBahanAtap; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Bangunan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbLuasBangunan" name="imbLuasBangunan" maxlength="255" size="50" value="<?php echo $obj->imbLuasBangunan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jumlah Lantai</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbJumlahLantai" name="imbJumlahLantai" maxlength="255" size="50" value="<?php echo $obj->imbJumlahLantai; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Panjang Pagar</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbPanjangPagar" name="imbPanjangPagar" maxlength="255" size="50" value="<?php echo $obj->imbPanjangPagar; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Pagar</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbJenisPagar" name="imbJenisPagar" maxlength="255" size="50" value="<?php echo $obj->imbJenisPagar; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jumlah Unit</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbJumlahUnit" name="imbJumlahUnit" maxlength="255" size="11" value="<?php echo $obj->imbJumlahUnit; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Rekomendasi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="imbRekomendasiCamat" name="imbRekomendasiCamat" cols="80" rows="5"><?php echo $obj->imbRekomendasiCamat; ?></textarea>
						<br><i>Setiap rekomendasi ditulis dan diakhiri dengan menekan tombol ENTER</i>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbKeterangan" name="imbKeterangan" maxlength="255" size="50" value="<?php echo $obj->imbKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->imbNo > 0) {
			//TODO: 271016: imb-s
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/IMB - S/<?php echo substr($obj->imbTglPengesahan,0,4); ?>/</span>
						<input class="box" id="imbNo" name="imbNo" maxlength="5" size="5" value="<?php echo $obj->imbNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="imbNo" name="imbNo" value="<?php echo $obj->imbNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="imbTglPengesahan" name="imbTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->imbTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="imbPejID" name="imbPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->imbPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='imb'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->imbTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->imbTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="imbTglBerlaku" name="imbTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->imbTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->imbTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->imbTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="imbTglDaftarUlang" name="imbTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->imbTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Retribusi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bendahara Penerimaan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Keuangan' 
			ORDER BY pnNama";
	$rsKeuangan = $app->query($sql);
?>
						<select class="box" id="imbPungutanBendaharaID" name="imbPungutanBendaharaID">
<?php 
	while(($objKeuangan = mysql_fetch_object($rsKeuangan)) == true){
		echo '<option value="'.$objKeuangan->id.'"';
		if ($objKeuangan->id == $obj->imbPungutanBendaharaID) {
			echo ' selected';	
		}
		echo '>'.$objKeuangan->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Mengetahui</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsMengetahui = $app->query($sql);
?>
						<select class="box" id="imbPungutanMengetahuiID" name="imbPungutanMengetahuiID">
<?php 
	while(($objMengetahui = mysql_fetch_object($rsMengetahui)) == true){
		echo '<option value="'.$objMengetahui->id.'"';
		if ($objMengetahui->id == $obj->imbPungutanMengetahuiID) {
			echo ' selected';	
		}
		echo '>'.$objMengetahui->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pembayaran Retribusi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="imbPungutanNama" name="imbPungutanNama" maxlength="500" size="50" value="<?php echo $obj->imbPungutanNama; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3" style="padding-left:20px;">dengan rincian sebagai berikut</td>
				</tr>
				<tr>
					<td colspan="3" style="padding-left:20px;"><input type="button" class="button-link inline dark-blue" id="btnTambahPungutan" value="Tambah Pungutan" /></td>
				</tr>
				<tr>
					<td colspan="3">
						<table id="entriTable2" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody id="pungutan">
<?php 
		$i = 0;
		
		if (count($obj->pungutan) > 0) {
			foreach ($obj->pungutan as $pungutan) {
?>
						<tr>
							<td style="padding-left:20px; border-bottom: dotted 1px #BBBBBB;">
								<div>
									<img src="images/icons/delete2.png" class="btnHapusPungutan" title="Hapus Pungutan" />
									<input class="box bulat" id="pimbNo[<?php echo $i; ?>]" name="pimbNo[<?php echo $i; ?>]" maxlength="2" size="2" value="<?php echo $pungutan->pimbNo; ?>" placeholder="No" />
									<input class="box" id="pimbKelompok[<?php echo $i; ?>]" name="pimbKelompok[<?php echo $i; ?>]" maxlength="500" size="8" value="<?php echo $pungutan->pimbKelompok; ?>" placeholder="Kelompok" />
								</div>
								<input class="box" id="pimbNama[<?php echo $i; ?>]" name="pimbNama[<?php echo $i; ?>]" maxlength="500" size="19" value="<?php echo $pungutan->pimbNama; ?>" placeholder="Nama Pungutan" />
							</td>
							<td style="border-bottom: dotted 1px #BBBBBB;">&nbsp;:&nbsp;</td>
							<td style="border-bottom: dotted 1px #BBBBBB;">
								<input class="box desimal2" id="pimbNilai1[<?php echo $i; ?>]" name="pimbNilai1[<?php echo $i; ?>]" maxlength="11" size="6" value="<?php echo $pungutan->pimbNilai1; ?>" placeholder="Nilai" />
								<input class="box" id="pimbSatuan1[<?php echo $i; ?>]" name="pimbSatuan1[<?php echo $i; ?>]" maxlength="50" size="4" value="<?php echo $pungutan->pimbSatuan1; ?>" placeholder="Satuan" />
								x
								<input class="box desimal2" id="pimbNilai2[<?php echo $i; ?>]" name="pimbNilai2[<?php echo $i; ?>]" maxlength="11" size="6" value="<?php echo $pungutan->pimbNilai2; ?>" placeholder="Nilai" />
								<input class="box" id="pimbSatuan2[<?php echo $i; ?>]" name="pimbSatuan2[<?php echo $i; ?>]" maxlength="50" size="4" value="<?php echo $pungutan->pimbSatuan2; ?>" placeholder="Satuan" />
								x
								<input class="box desimal2" id="pimbNilai3[<?php echo $i; ?>]" name="pimbNilai3[<?php echo $i; ?>]" maxlength="11" size="6" value="<?php echo $pungutan->pimbNilai3; ?>" placeholder="Nilai" />
								<input class="box" id="pimbSatuan3[<?php echo $i; ?>]" name="pimbSatuan3[<?php echo $i; ?>]" maxlength="50" size="4" value="<?php echo $pungutan->pimbSatuan3; ?>" placeholder="Satuan" />
								= 
								<input class="box" id="pimbSatuanLuas[<?php echo $i; ?>]" name="pimbSatuanLuas[<?php echo $i; ?>]" maxlength="50" size="10" value="<?php echo $pungutan->pimbSatuanLuas; ?>" placeholder="Satuan Luas" />
								x Rp.
								<input class="box bulat" id="pimbNilaiRupiah[<?php echo $i; ?>]" name="pimbNilaiRupiah[<?php echo $i; ?>]" maxlength="11" size="11" value="<?php echo $pungutan->pimbNilaiRupiah; ?>" />
								x 
								<input class="box bulat" id="pimbUnit[<?php echo $i; ?>]" name="pimbUnit[<?php echo $i; ?>]" maxlength="11" size="6" value="<?php echo $pungutan->pimbUnit; ?>" placeholder="Unit" /> unit
							</td>
						</tr>
<?php 
				$i++;
			}
		}
?>						
						</tbody>
						</table>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	<script type="text/javascript">
		var elmCounter = <?php echo $i; ?>;
	</script>
	<script type="text/javascript" src="acts/imb/js/edit.imb.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printIMB($obj) {
		global $app;
		
		$objReport 		= new Report('Izin Mendirikan Bangunan', 'IMB-'.substr($obj->imbTglPengesahan,0,4).'-'.$obj->imbNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->imbPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,0);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<?php //TODO: 271016: imb-s ?>
	<h4 align="center">
		KEPUTUSAN KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?><br>
		NOMOR : <?php echo $obj->imbNoLengkap; ?><br>
		TENTANG<br>
		IZIN MENDIRIKAN, MEMPERBAIKI DAN MEMBONGKAR BANGUNAN SEMENTARA<br>
		<br>
		KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?>,
	</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellpadding="0" cellspacing="0">
<?php 
		$imbRekomendasiCamat = explode("\n",$obj->imbRekomendasiCamat);
		$imbRekomendasiCamat2 = array();
		
		if (count($imbRekomendasiCamat) > 0) {
			foreach ($imbRekomendasiCamat as $rekomendasi) {
				if (trim($rekomendasi) != '') {
					$imbRekomendasiCamat2[] = trim($rekomendasi);
				}
			}
		}	
?>
				<tr><td width="4%">1.&nbsp;</td><td width="96%">Surat Permohonan dari Sdr <?php echo $obj->mhnNama; ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> untuk mendapatkan Izin Mendirikan, Memperbaiki dan Membongkar Bangunan.</td></tr>
				<tr><td>2.</td><td>Berita Acara Pemeriksaan dari <?php echo $objSatuanKerja->skrNama; ?> No. <?php echo $obj->imbNoBAP; ?> Tanggal <?php echo $app->MySQLDateToIndonesia($obj->imbTglBAP); ?></td></tr>
<?php
		$no = 3;
		if (count($imbRekomendasiCamat2) > 0) {	
			foreach ($imbRekomendasiCamat2 as $rekomendasi) {
?>
				<tr><td><?php echo $no; ?>.&nbsp;</td><td><?php echo trim($rekomendasi); ?></td></tr>
<?php
				$no++;
			}
		}
?>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="justify" valign="top">
			<table>
				<tr><td width="4%">a.</td><td width="96%">Bahwa untuk mencapai tertib pelaksanaan pembangunan, serta pengawasan pengendalian sesuai dengan Rencana Umum Tata Ruang Kota, maka untuk mendirikan, merubah, membongkar bangunan diperlukan izin dari Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu.</td></tr>
				<tr><td>b.</td><td>Bahwa permohonan izin telah memenuhi persyaratan dan telah sesuai dengan ketentuan yang berlaku.</td></tr>
				<tr><td>c.</td><td>Bahwa untuk maksud tersebut diatas perlu ditetapkan dalam suatu Keputusan Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
<?php 
		echo $konfigurasi['consideringIMB'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">PERTAMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Memberikan Izin kepada :</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top"></td>
		<td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0">
			<tr>
				<td align="left" valign="top" width="38%">Nama</td>
				<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="60%"><?php echo $obj->mhnNama; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">No KTP</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnNoKtp; ?></td>
			</tr>
			<tr>
			  	<td align="left" valign="top">Pekerjaan</td>
			  	<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnPekerjaan; ?></td>
			</tr>
<?php 
		$sql = "SELECT wilayah.wilTingkat AS tingkat, wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilTingkat AS kabupatenkota, wilayah3.wilNama AS kabupaten 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objAlamat = $app->queryObject($sql);
?>
			<tr>
				<td align="left" valign="top">Alamat</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnAlamat.', '.($objAlamat->kelurahan == 'Desa' ? 'Desa' : 'Kel.').' '.$objAlamat->kelurahan.', Kec. '.$objAlamat->kecamatan.', '.($objAlamat->kabupatenkota == 'Kabupaten' ? 'Kab.' : 'Kota').' '.$objAlamat->kabupaten; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Bertindak Atas Nama</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->imbAtasNama; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Jabatan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->imbJabatan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Untuk Mendirikan Bangunan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->imbUntukMembangun; ?></td>
			</tr>
			<tr>
				<td colspan="3">
					Diatas sebidang tanah kepunyaan/dikuasai kepada yang bersangkutan tercatat dengan Surat Tanah/Sertifikat <?php echo $obj->imbHakTanah; ?> <?php echo $obj->imbNoSertifikat; ?> Terletak di :
				</td>
			</tr>
			<tr>
				<td align="left" valign="top">Alamat</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->imbLokasi; ?></td>
			</tr>
<?php 
		$sql = "SELECT wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilNama AS kabupaten 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->imbKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasi = $app->queryObject($sql);
?>
			<tr>
				<td align="left" valign="top">Kelurahan/Desa</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $objLokasi->kelurahan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Kecamatan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $objLokasi->kecamatan; ?></td>
			</tr>
			<tr>
				<td align="left" colspan="3">Sesuai dengan permohonan yang telah disetujui dan ketentuan sebagaimana tercantum dalam Lampiran Keputusan ini.</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEDUA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Surat Izin ini dicabut kembali, apabila Pengusaha/Pemegang Izin tidak mentaati ketentuan-ketentuan tersebut di atas dan melanggar Peraturan Perundang-undangan yang berlaku.</td>
	</tr>
	<tr>
		<td align="left" colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KETIGA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Keputusan ini mulai berlaku sejak tanggal ditetapkan.</td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="48%">&nbsp;</td>
		<td width="52%">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->imbTglPengesahan)); ?>
		</td>
	</tr>
	</table>
	
	<br><br>
	
	<small>Kutipan Keputusan ini, aslinya di atas kertas bermaterai Rp. 6.000,- diberikan kepada yang bersangkutan untuk diketahui dan dipergunakan sebagaimana mestinya.</small>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	static function printLampiranIMB($obj) {
		global $app;
		
		$objReport 		= new Report('Izin Mendirikan Bangunan', 'IMB-'.substr($obj->imbTglPengesahan,0,4).'-'.$obj->imbNo.'.pdf');
		//$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->imbPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,0);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="50%" align="right">
			<b>LAMPIRAN : </b>
		</td>
		<td colspan="2" width="50%">
			<b>KEPUTUSAN KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
		</td>
	</tr>
	<tr>
		<td></td>
		<td width="15%"><b>NOMOR</b></td>
		<td width="35%"><b>: <?php echo $obj->imbNoLengkap; ?></b></td>
	</tr>
	<tr>
		<td></td>
		<td><b>TANGGAL</b></td>
		<td><b>: <?php echo $app->MySQLDateToIndonesia($obj->imbTglPengesahan); ?></b></td>
	</tr>
	<tr>
		<td style="<?php echo $app->border('b'); ?>" colspan="3">&nbsp;</td>
	</tr>
	</table>
<?php
		$pdf->Image(CFG_ABSOLUTE_PATH.'/images/bg_logo.jpg', $pdf->GetPageWidth() / 2 - 60, $pdf->getPageHeight() / 2 - 75, 120, 150, '', '', '', false, 300, '', false, false, 0);
?>	
	<p><b>I. UMUM</b></p>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="4%"></td>
		<td width="4%">1.</td>
		<td colspan="2" width="92%">Dalam pelaksanaan pekerjaan, pemegang izin harus mematuhi semua peraturan-peraturan bangunan yang berlaku dan petunjuk-petunjuk dari Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan melalui pengawasan lapangan.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td>2.</td>
		<td colspan="2">Setiap waktu selama dalam pelaksanaan pekerjaan "Surat Izin" ini berikut dengan gambar yang telah disahkan harus berada ditempat pekerjaan, untuk keperluan pemeriksaan oleh petugas yang berwenang.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td>3.</td>
		<td colspan="2">Setiap pemegang izin tidak boleh mempersulit/ menghalangi petugas yang berwenang untuk melakukan pemeriksaan.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td>4.</td>
		<td colspan="2">Setiap akan melakukan perubahan dari bentuk yang telah disahkan seperti dalam gambar, pemegang izin harus terlebih dahulu menyampaikan/ mengajukan permohonan perubahan secara tertulis kepada Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan.</td>
	</tr>
	</table>
	
	<p><b>II. ADMINISTRASI</b></p>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="4%"></td>
		<td width="4%">1.</td>
		<td colspan="2" width="92%">Masa pelaksanaan pekerjaan bangunan ini ditetapkan selama:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;bulan terhitung mulai Surat Izin ini dikeluarkan. Bilamana dalam jangka waktu yang telah ditetapkan tenyata pelaksanaan pekerjaan belum selesai. Pemegang izin harus memberitahukan kepada Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan untuk mendapatkan tambahan waktu yang diminta.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td>2.</td>
		<td colspan="2">Dalam masa 6 (enam) bulan setelah dikeluarkan Surat Izin ini, yang bersangkutan tidak melaksanakan kegiatannya, maka izin ini dinyatakan tidak berlaku lagi (batal) kecuali dapat perpanjangan waktu pelaksanaannya secara tertulis dari Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td>3.</td>
		<td colspan="2">Apabila ketentuan-ketentuan di atas dan peraturan-peraturan lain yang berkaitan dengan pelaksanaan pekerjaan bangunan ini ternyata ada diantaranya yang tidak dipatuhi, maka Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan berhak untuk menghentikan/ membongkar pekerjaan-pekerjaan yang salah atau bilamana perlu izin ini dapat dicabut kembali tanpa memperhitungkan kerugian-kerugian dari pemegang izin.</td>
	</tr>
	</table>
	
	<br pagebreak="true"/>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="4%"></td>
		<td width="4%">4.</td>
		<td colspan="2" width="92%">Kepada pemegang izin dikenakan pungutan sebagai berikut:</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td colspan="2">
			<table border="0" cellpadding="1" cellspacing="0" width="100%">
			<tr>
				<td width="4%" align="center" style="<?php echo $app->border(); ?>"><b>NO</b></td>
				<td width="26%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>JENIS BANGUNAN</b></td>
				<td width="10%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>UNIT</b></td>
				<td colspan="5" width="17%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>UKURAN</b></td>
				<td colspan="2" width="10%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>LUAS</b></td>
				<td colspan="2" width="15%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>RETRIBUSI</b></td>
				<td colspan="2" width="18%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>JUMLAH</b></td>
			</tr>
<?php 
		$jumlah = 0;
		$jumlah_dibulatkan = 0;
			
		if (count($obj->pungutan) > 0) {
			$j = 0;
			
			foreach ($obj->pungutan as $kelompok=>$pungutans) {
				$j++;
?>
	<tr>
		<td style="<?php echo $app->border('lr'); ?>"></td>
		<td style="<?php echo $app->border('r'); ?>"><b><u><?php echo ($j > 1) ? '<br>' : '';  ?><?php echo $kelompok; ?></u></b></td>
		<td style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="5" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
	</tr>
<?php
				$i = 1;
				
				foreach ($pungutans as $pungutan) {				
					$nilai = array();	//?
					$luas = 1;			//?
					
					if ($pungutan->pimbNilai1 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai1, -1).' '.$pungutan->pimbSatuan1); 
						$luas = bcmul($luas, $pungutan->pimbNilai1, 4);
						
						if ($pungutan->pimbSatuan1 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					if ($pungutan->pimbNilai2 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai2, -1).' '.$pungutan->pimbSatuan2);
						$luas = bcmul($luas, $pungutan->pimbNilai2, 4);
						
						if ($pungutan->pimbSatuan2 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					} 
					if ($pungutan->pimbNilai3 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai3, -1).' '.$pungutan->pimbSatuan3); 
						$luas = bcmul($luas, $pungutan->pimbNilai3, 4);
						
						if ($pungutan->pimbSatuan3 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					} 
					
					$luas = bcmul($luas, $pungutan->pimbUnit, 4);
					$hasil = bcmul($luas, $pungutan->pimbNilaiRupiah, 4);
					$jumlah = bcadd($jumlah, $hasil, 4);
	?>
		<tr>
			<td align="center" style="<?php echo $app->border('lr'); ?>"><?php echo $i.'.' ?></td>
			<td style="<?php echo $app->border('r'); ?>"><?php echo $pungutan->pimbNama; ?></td>
			<td align="center" style="<?php echo $app->border('r'); ?>"><?php echo $pungutan->pimbUnit; ?></td>
			<td colspan="5" align="right" style="<?php echo $app->border('r'); ?>"><?php echo (count($nilai) > 0) ? implode(' x ',$nilai) : ''; ?></td>
			<td colspan="2" align="right" style="<?php echo $app->border('r'); ?>"><?php echo trim($app->MySQLToMoney($luas, -1).' '.$pungutan->pimbSatuanLuas); ?></td>
			<td width="4%">Rp.</td>
			<td width="11%" align="right" style="<?php echo $app->border('r'); ?>"><?php echo $app->MySQLToMoney($pungutan->pimbNilaiRupiah); ?></td>
			<td width="4%">Rp.</td>
			<td width="14%" align="right" style="<?php echo $app->border('r'); ?>"><?php echo $app->MySQLToMoney($hasil); ?></td>
		</tr>
<?php 
					$i++;
				}
			}
		}
?>
			<tr>
				<td colspan="8" style="<?php echo $app->border('tbl'); ?>"></td>
				<td colspan="4" style="<?php echo $app->border('tbr'); ?>">JUMLAH</td>
				<td align="right" style="<?php echo $app->border('tb'); ?>">Rp.</td>
				<td align="right" style="<?php echo $app->border('tbr'); ?>"><?php echo $app->MySQLToMoney($jumlah); ?></td>
			</tr>
<?php 
		//Pembulatan ke ratusan atas
		$intpart = floor($jumlah % 100);
		
		if ($intpart > 0) {
			$jumlah = bcsub($jumlah, $intpart, 0);
			$jumlah = bcadd($jumlah, 100, 0);
		}
		
		$jumlah_dibulatkan = round($jumlah,0);
?>
			<tr>
				
				<td colspan="8" style="<?php echo $app->border('bl'); ?>"></td>
				<td colspan="4" style="<?php echo $app->border('br'); ?>">JUMLAH DIBULATKAN</td>
				<td align="right" style="<?php echo $app->border('b'); ?>">Rp.</td>
				<td align="right" style="<?php echo $app->border('br'); ?>"><?php echo $app->MySQLToMoney($jumlah_dibulatkan); ?></td>
			</tr>
			<tr>
				
				<td colspan="14" style="<?php echo $app->border('blr'); ?>">
					<b><i>TERBILANG : <?php echo ucwords($app->terbilang($jumlah_dibulatkan)); ?> Rupiah</i></b>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	
	<p><b>III. TEKNIS</b></p>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="4%"></td>
		<td width="4%">1.</td>
		<td colspan="2" width="92%">Pelaksanaan pekerjaan harus sesuai dengan perencanaan/gambar yang telah disahkan oleh Dinas Pekerjaan Umum dan Penataan Ruang Kabupaten Pelalawan.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td>2.</td>
		<td colspan="2">Bilamana pelaksanaan pekerjaan akan dimulai (pemasangan patok-patok) Baouw Plank dan penggalian pondasi, pemegang izin terlebih dahulu harus memberitahukan kepada Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan dan apabila telah diperiksa dan dibenarkan, pekerjaan tersebut oleh pemegang izin baru dapat dimulai.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td>3.</td>
		<td colspan="2">Guna menjaga kebersihan, menghindari penyakit dan untuk mengatur pengaliran air, maka kepada pemegang izin diwajibkan membuat:</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td width="4%">a. </td>
		<td width="88%">Saluran air kotoran dari bangunan ke pembuangan tertentu (septic tank) paling kurang 10 meter dari sumur.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td></td>
		<td>b.</td>
		<td>Saluran air bangunan (air mandi, air hujan, dan lain-lain) dari bangunan ke saluran-saluran air tertentu dilaksanakan dengan baik, sesuai petunjuk dari Dinas Pekerjaan Umum dan Penataan Ruang Kabupaten Pelalawan.</td>
	</tr>
	<tr>
		<td></td>
		<td>4.</td>
		<td colspan="2">Penutup<br>Lampiran Surat Keputusan ini adalah bagian yang tidak terpisahkan dari keputusan aslinya, untuk diketahui oleh yang berkepentingan dan dilaksanakan sebagaimana mestinya.</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
	</table>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%">
			<?php $objReport->signature2($konfigurasi, $objPengesahan); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
?>
	<p align="center"><b>Pendaftaran Balik Nama/ Pemecahan<br>Kutipan SK IMB</b></p>
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td align="center" style="<?php echo $app->border(); ?>">Sebab Perubahan</td>
		<td align="center" style="<?php echo $app->border('rtb'); ?>">Tanggal Pendaftaran</td>
		<td align="center" style="<?php echo $app->border('rtb'); ?>">Nama Pemegang Hak</td>
		<td align="center" style="<?php echo $app->border('rtb'); ?>">Tanda Tangan Kepala Dinas dan Cap Dinas</td>
	</tr>
	<tr>
		<td style="<?php echo $app->border('lrb'); ?>">
			<br><br><br><br><br><br><br><br><br><br>
			<br><br><br><br><br><br><br><br><br><br>
			<br><br><br><br><br><br><br><br><br><br>
			<br><br><br><br><br><br><br><br><br><br>
			<br><br><br><br><br><br><br><br><br><br>
		</td>
		<td style="<?php echo $app->border('rb'); ?>"></td>
		<td style="<?php echo $app->border('rb'); ?>"></td>
		<td style="<?php echo $app->border('rb'); ?>"></td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	static function printRetribusiIMB($obj) {
		global $app;
		
		$objReport 		= new Report('Setoran Retribusi Izin Mendirikan Bangunan', 'SetoranRetribusiIMB-'.substr($obj->imbTglPengesahan,0,4).'-'.$obj->imbNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		//$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->imbPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,0);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$sql = "SELECT wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilNama AS kabupaten 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->imbKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasi = $app->queryObject($sql);
		
		ob_start();
?>
	<p align="center">
		<b>SETORAN RETRIBUSI IJIN MENDIRIKAN BANGUNAN (IMB)<br>
		TAHUN <?php echo substr($obj->imbTglPengesahan,0,4); ?></b><br>
		NOMOR : <?php echo $obj->imbNo.'/IMB - S/'.$app->getRoman(intval(substr($obj->imbTglPengesahan,5,2))).'/'.substr($obj->imbTglPengesahan,0,4); ?>
	</p>
	
	<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td width="20%">NAMA</td>
		<td width="4%">:</td>
		<td width="76%"><b><?php echo strtoupper($obj->mhnNama); ?></b></td>
	</tr>
	<tr>
		<td>LOKASI</td>
		<td>:</td>
		<td><?php echo $obj->imbLokasi.', '.$objLokasi->kelurahan.', '.$objLokasi->kecamatan; ?></td>
	</tr>
	<tr>
		<td>KETERANGAN</td>
		<td>:</td>
		<td><?php echo 'Pembayaran Retribusi '.$obj->imbPungutanNama; ?></td>
	</tr>
	<tr>
		<td colspan="3"></td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;">Berdasarkan Peraturan Daerah Nomor 01 Tahun 2016 Lampiran XVIII Tentang Retribusi Daerah, dijelaskan bahwa struktur dan besarnya tarif Retribusi Izin Mendirikan Bangunan berdasarkan luas bangunan dan jenis bangunan yang didirikan dengan perincian sebagai berikut :</td>
	</tr>
	<tr>
		<td colspan="3"></td>
	</tr>
	</table>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="4%" align="center" style="<?php echo $app->border(); ?>"><b>NO</b></td>
		<td width="26%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>JENIS BANGUNAN</b></td>
		<td width="10%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>UNIT</b></td>
		<td colspan="5" width="17%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>UKURAN</b></td>
		<td colspan="2" width="10%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>LUAS</b></td>
		<td colspan="2" width="15%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>RETRIBUSI</b></td>
		<td colspan="2" width="18%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>JUMLAH</b></td>
	</tr>
<?php 
		$jumlah = 0;
		$jumlah_dibulatkan = 0;
			
		if (count($obj->pungutan) > 0) {
			$j = 0;
			
			foreach ($obj->pungutan as $kelompok=>$pungutans) {
				$j++;
?>
	<tr>
		<td style="<?php echo $app->border('lr'); ?>"></td>
		<td style="<?php echo $app->border('r'); ?>"><b><u><?php echo ($j > 1) ? '<br>' : '';  ?><?php echo $kelompok; ?></u></b></td>
		<td style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="5" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
	</tr>
<?php
				$i = 1;
				
				foreach ($pungutans as $pungutan) {				
					$nilai = array();	//?
					$luas = 1;			//?
					
					if ($pungutan->pimbNilai1 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai1, -1).' '.$pungutan->pimbSatuan1); 
						$luas = bcmul($luas, $pungutan->pimbNilai1, 4);
						
						if ($pungutan->pimbSatuan1 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					if ($pungutan->pimbNilai2 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai2, -1).' '.$pungutan->pimbSatuan2);
						$luas = bcmul($luas, $pungutan->pimbNilai2, 4);
						
						if ($pungutan->pimbSatuan2 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					} 
					if ($pungutan->pimbNilai3 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai3, -1).' '.$pungutan->pimbSatuan3); 
						$luas = bcmul($luas, $pungutan->pimbNilai3, 4);
						
						if ($pungutan->pimbSatuan3 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					} 
					
					$luas = bcmul($luas, $pungutan->pimbUnit, 4);
					$hasil = bcmul($luas, $pungutan->pimbNilaiRupiah, 4);
					$jumlah = bcadd($jumlah, $hasil, 4);
	?>
		<tr>
			<td align="center" style="<?php echo $app->border('lr'); ?>"><?php echo $i.'.' ?></td>
			<td style="<?php echo $app->border('r'); ?>"><?php echo $pungutan->pimbNama; ?></td>
			<td align="center" style="<?php echo $app->border('r'); ?>"><?php echo $pungutan->pimbUnit; ?></td>
			<td colspan="5" align="right" style="<?php echo $app->border('r'); ?>"><?php echo (count($nilai) > 0) ? implode(' x ',$nilai) : ''; ?></td>
			<td colspan="2" align="right" style="<?php echo $app->border('r'); ?>"><?php echo trim($app->MySQLToMoney($luas, -1).' '.$pungutan->pimbSatuanLuas); ?></td>
			<td width="4%">Rp.</td>
			<td width="11%" align="right" style="<?php echo $app->border('r'); ?>"><?php echo $app->MySQLToMoney($pungutan->pimbNilaiRupiah); ?></td>
			<td width="4%">Rp.</td>
			<td width="14%" align="right" style="<?php echo $app->border('r'); ?>"><?php echo $app->MySQLToMoney($hasil); ?></td>
		</tr>
<?php 
					$i++;
				}
			}
		}
?>
	<tr>
		<td colspan="8" style="<?php echo $app->border('tbl'); ?>"></td>
		<td colspan="4" style="<?php echo $app->border('tbr'); ?>">JUMLAH</td>
		<td align="right" style="<?php echo $app->border('tb'); ?>">Rp.</td>
		<td align="right" style="<?php echo $app->border('tbr'); ?>"><?php echo $app->MySQLToMoney($jumlah); ?></td>
	</tr>
<?php 
		//Pembulatan ke ratusan atas
		$intpart = floor($jumlah % 100);
		
		if ($intpart > 0) {
			$jumlah = bcsub($jumlah, $intpart, 0);
			$jumlah = bcadd($jumlah, 100, 0);
		}
		
		$jumlah_dibulatkan = round($jumlah, 0);
?>
	<tr>
		
		<td colspan="8" style="<?php echo $app->border('bl'); ?>"></td>
		<td colspan="4" style="<?php echo $app->border('br'); ?>">JUMLAH DIBULATKAN</td>
		<td align="right" style="<?php echo $app->border('b'); ?>">Rp.</td>
		<td align="right" style="<?php echo $app->border('br'); ?>"><?php echo $app->MySQLToMoney($jumlah_dibulatkan); ?></td>
	</tr>
	</table>
	
	<br><br>
<?php 
		$objBendahara 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->imbPungutanBendaharaID."' AND pnPgrID=pgrID");
		if (!is_object($objBendahara)) {
			$objBendahara = new stdClass();
			$objBendahara->pnNIP = '';
			$objBendahara->pnNama = '';
		}
		$objMengetahui 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->imbPungutanMengetahuiID."' AND pnPgrID=pgrID");
		if (!is_object($objMengetahui)) {
			$objMengetahui = new stdClass();
			$objMengetahui->pnNIP = '';
			$objMengetahui->pnNama = '';
		}
?>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="50%" align="center">
			<br><br><br>
			<b>BENDAHARA PENERIMAAN DPMPTSP<br>KABUPATEN PELALAWAN</b>
			<br><br><br><br><br>
			<b><u><?php echo strtoupper($objBendahara->pnNama); ?></u></b>
			<br>
			NIP. <?php echo $objBendahara->pnNIP; ?>
		</td>
		<td width="50%" align="center">
			<?php echo $objSatuanKerja->wilIbukota.', '.$app->MySQLDateToIndonesia($obj->imbTglPengesahan); ?>
			<br><br>
			<b>PEMOHON,</b>
			<br><br><br><br><br><br>
			<b><u><?php echo strtoupper($obj->mhnNama); ?></u></b>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<br><br>
			MENGETAHUI
			<br>
			<b>KASI PELAYANAN PERIZINAN DAN NON PERIZINAN B</b>
			<br><br><br><br><br>
			<b><u><?php echo strtoupper($objMengetahui->pnNama); ?></u></b>
			<br>
			NIP. <?php echo $objMengetahui->pnNIP; ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
				
		$pdf->Output($objReport->filename);
	}
	
	static function printRecapIMB($arr) {
		global $app;
		
		if ($arr['filter']['bulan'] == '') {
			$periode = $arr['filter']['tahun'];
		} else {
			$periode = strtoupper($app->bulan[$arr['filter']['bulan']]).' '.$arr['filter']['tahun'];
		}
		
		$objReport 		= new Report('Rekapitulasi Setoran Retribusi Izin Mendirikan Bangunan', 'RekapitulasiSetoranRetribusiIMB-'.$periode.'.pdf');
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,0);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<p align="center">
		<b>REKAPITULASI SETORAN RETRIBUSI IJIN MENDIRIKAN BANGUNAN (IMB)<br>
		PERIODE <?php echo $periode; ?></b>
	</p>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th width="4%" align="center" style="<?php echo $app->border(); ?>"><b>NO</b></th>
			<th width="17%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>NOMOR SURAT</b></th>
			<th width="9%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>TANGGAL<br>PENGESAHAN</b></th>
			<th width="15%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>NAMA</b></th>
			<th width="29%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>LOKASI</b></th>
			<th colspan="2" width="12%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>JUMLAH DIBULATKAN</b></th>
			<th width="15%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>KETERANGAN</b></th>
		</tr>
	</thead>
	<tbody>
<?php 
		$jumlah_dibulatkan = 0;
			
		if (count($arr['data']) > 0) {
			$i = 1;
			
			foreach ($arr['data'] as $pungutan) { 
				$jumlah_dibulatkan = bcadd($jumlah_dibulatkan, $pungutan->imbPungutanJumlah, 4);
?>
		<tr>
			<td width="4%" align="center" style="<?php echo $app->border('blr'); ?>"><?php echo $i.'.' ?></td>
			<td width="17%" style="<?php echo $app->border('br'); ?>"><?php echo $pungutan->imbNoLengkap; ?></td>
			<td width="9%" align="center" style="<?php echo $app->border('br'); ?>"><?php echo $app->MySQLDateToNormal($pungutan->imbTglPengesahan); ?></td>
			<td width="15%"style="<?php echo $app->border('br'); ?>"><?php echo $pungutan->mhnNama; ?></td>
			<td width="29%"style="<?php echo $app->border('br'); ?>"><?php echo $pungutan->imbLokasi.', '.$pungutan->kelurahan.', '.$pungutan->kecamatan; ?></td>
			<td width="4%" style="<?php echo $app->border('b'); ?>">Rp.</td>
			<td width="8%" align="right" style="<?php echo $app->border('br'); ?>"><?php echo $app->MySQLToMoney($pungutan->imbPungutanJumlah); ?></td>
			<td width="15%" style="<?php echo $app->border('br'); ?>"><?php echo $pungutan->imbPungutanNama; ?></td>
		</tr>
<?php 
				$i++;
			}
		}
?>
		<tr>
			<td colspan="5" align="center" style="<?php echo $app->border('blr'); ?>"><b>JUMLAH</b></td>
			<td style="<?php echo $app->border('b'); ?>" ><b>Rp.</b></td>
			<td align="right" style="<?php echo $app->border('br'); ?>"><b><?php echo $app->MySQLToMoney($jumlah_dibulatkan); ?></b></td>
			<td style="<?php echo $app->border('br'); ?>"></td>
		</tr>
	</tbody>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
				
		$pdf->Output($objReport->filename);
	}

	static function readIMB($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=imb&task=edit&id=<?php echo $obj->imbDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Mendirikan Bangunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=imb&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Umum Bangunan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;" valign="top">No. dan Tgl. Sertifikat</td><td valign="top">&nbsp;:&nbsp;</td>
			<td valign="top">
				<?php echo $obj->imbNoSertifikat; ?>
			</td>
		</tr>
		<!-- <tr>
			<td style="padding-left:20px;">Tgl. Sertifikat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->imbTglSertifikat); ?>
			</td>
		</tr> -->
		<tr>
			<td style="padding-left:20px;">Hak Tanah</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbHakTanah; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Atas Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbAtasNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jabatan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbJabatan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Untuk Membangun</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbUntukMembangun; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">RAB Pembangunan (Rp)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbRab; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. BAP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbNoBAP; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. BAP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->imbTglBAP); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. IP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbNoIP; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. IP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->imbTglIP); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Posisi Bangunan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbLokasi; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->imbKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Batas Utara</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBatasUtara; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Batas Selatan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBatasSelatan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Batas Barat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBatasBarat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Batas Timur</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBatasTimur; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jarak dari Jalan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbJarakDariJalan; ?> m
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Struktur Bangunan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Konstruksi Bangunan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbKonstruksiBangunan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bahan Pondasi</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBahanPondasi; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kerangka Atas/Bawah</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbKerangkaAtasBawah; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bahan Lantai</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBahanLantai; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bahan Dinding</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBahanDinding; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bahan Loteng</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBahanLoteng; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bahan Atap</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbBahanAtap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Luas Bangunan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbLuasBangunan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jumlah Lantai</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbJumlahLantai; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Panjang Pagar</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbPanjangPagar; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis Pagar</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbJenisPagar; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jumlah Unit</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbJumlahUnit; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->imbNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->imbTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->imbPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->imbTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->imbTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->imbTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->imbTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/imb/js/read.imb.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewIMB($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=imb"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Mendirikan Bangunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue btnPrint" id="btnPrintRekapitulasi-imb-printrecap-0" href="#printForm">Rekapitulasi Setoran Retribusi</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
<?php 
		$app->createSelectMonth('filterbulan', $arr['filter']['bulan'], array(0 => 'Seluruhnya'));
		$app->createSelectYear('filtertahun', $arr['filter']['tahun'], array(0 => 'Seluruhnya'));
?>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['bulan'] > 0 || $arr['filter']['tahun'] > 0) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('imbNo', 'Nomor Surat', true),
			$app->setHeader('imbTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('imbTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('imbTglDaftarUlang', 'Tgl. Daftar Berikutnya', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=imb&task=read&id='.$v->imbDmhnID.'&fromact=imb" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->imbDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=imb&task=read&id='.$v->imbDmhnID.'&fromact=imb" title="Entri">'.$v->imbNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->imbTglPengesahan).'</td>';
				
				if ($v->imbTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->imbTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->imbTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->imbTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="5">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->addButton('imb', 'printlampiran', 'Lampiran');
		$objReport->addButton('imb', 'printretribusi', 'Setoran Retribusi');
		$objReport->addButtonParameter('btnPrintRekapitulasi-imb-printrecap-0', array('filternama', 'filterbulan', 'filtertahun'));
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/imb/js/imb.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>