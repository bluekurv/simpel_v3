<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.imb.php';
require_once 'view.imb.php';

switch ($app->task) {
	case 'edit':
		editIMB($app->id);
		break;
	case 'print':
	case 'printlampiran':
	case 'printretribusi':
		printIMB($app->id);
		break;
	case 'printrecap':
		printRecapIMB($app->id);
		break;
	case 'read':
		readIMB($app->id);
		break;
	case 'save':
		saveIMB();
		break;
	default:
		viewIMB(true, '');
		break;
}

function editIMB($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objIMB = $app->queryObject("SELECT * FROM imb WHERE imbDmhnID='".$id."'");
	if (!$objIMB) {
		$objIMB = new IMB_Model();
	}
	
	$objIMB->pungutan = array();
	
	$sql = "SELECT * FROM pungutanimb WHERE pimbImbID='".$objIMB->imbID."' ORDER BY pimbNo";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$obj->pimbNilai1 = $app->MySQLToMoney($obj->pimbNilai1, 2);
		$obj->pimbNilai2 = $app->MySQLToMoney($obj->pimbNilai2, 2);
		$obj->pimbNilai3 = $app->MySQLToMoney($obj->pimbNilai3, 2);
		$obj->pimbNilaiRupiah = $app->MySQLToMoney($obj->pimbNilaiRupiah);
		$obj->pimbUnit = $app->MySQLToMoney($obj->pimbUnit);
		$objIMB->pungutan[] = $obj;
	}
	
	$objIMB->imbDmhnID = $objDetailPermohonan->dmhnID;
	
	IMB_View::editIMB(true, '', $objDetailPermohonan, $objIMB);
}

function printIMB($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN imb ON imbDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objIMB = $app->queryObject($sql);
	if (!$objIMB) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objIMB->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	$objIMB->pungutan = array();
	
	$sql = "SELECT * FROM pungutanimb WHERE pimbImbID='".$objIMB->imbID."' ORDER BY pimbNo";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		if ($obj->pimbKelompok == '') {
			$obj->pimbKelompok = 'Ukuran Bangunan';
		}
		
		$objIMB->pungutan[$obj->pimbKelompok][] = $obj;
	}
	
	switch ($app->task) {
		case 'print':
			IMB_View::printIMB($objIMB);
			break;
		case 'printlampiran':
			IMB_View::printLampiranIMB($objIMB);
			break;
		case 'printretribusi':
			IMB_View::printRetribusiIMB($objIMB);
			break;
	}
}

function printRecapIMB() {
	global $app;
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'bulan' => 0,
		'tahun' => 0
	);
	
	$nama 		= $app->getStr('filternama');
	$bulan 		= $app->getInt('filterbulan');
	$tahun 		= $app->getInt('filtertahun');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "imbNo LIKE '".$nama."%'";
	}
	if ($bulan > $default['bulan']) {
		$filter[] = "MONTH(imbTglPengesahan)='".$bulan."'";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(imbTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(),
		'filter' => array(
			'nama' => $nama,
			'bulan' => $bulan,
			'tahun' => $tahun
		)
	);
	
	//Query
	$sql = "SELECT *, wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilNama AS kabupaten
			FROM imb
			LEFT JOIN detailpermohonan ON imbDmhnID=dmhnID
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN wilayah ON wilayah.wilID=imbKelurahanID
			LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
			LEFT JOIN wilayah AS wilayah3 ON wilayah2.wilParentID=wilayah3.wilID
			LEFT JOIN wilayah AS wilayah4 ON wilayah3.wilParentID=wilayah4.wilID
			".$where." 
			ORDER BY YEAR(imbTglPengesahan) DESC, imbNo ASC";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	IMB_View::printRecapIMB($arr);
}

function readIMB($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN imb ON imbDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objIMB = $app->queryObject($sql);
	if (!$objIMB) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objIMB->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	IMB_View::readIMB($objIMB);
}

function saveIMB() {
	global $app;
	
	//Get object
	$objIMBSebelumnya = $app->queryObject("SELECT * FROM imb WHERE imbID='".$app->getInt('imbID')."'");
	if (!$objIMBSebelumnya) {
		$objIMBSebelumnya = new IMB_Model();
	}
	
	//Create object
	$objIMB = new IMB_Model();
	$app->bind($objIMB);
	
	//Modify object (if necessary)
	$objIMB->imbTglSertifikat = $app->NormalDateToMySQL($objIMB->imbTglSertifikat);
	$objIMB->imbTglBAP = $app->NormalDateToMySQL($objIMB->imbTglBAP);
	$objIMB->imbTglIP = $app->NormalDateToMySQL($objIMB->imbTglIP);
	$objIMB->imbTglPengesahan = $app->NormalDateToMySQL($objIMB->imbTglPengesahan);
	if ($objIMB->imbTglPengesahan != '0000-00-00') {
		if ($objIMB->imbNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objIMB->imbNo = intval($app->queryField("SELECT MAX(imbNo) AS nomor FROM imb WHERE YEAR(imbTglPengesahan)='".substr($objIMB->imbTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objIMB->imbTglBerlaku = date('Y-m-d', strtotime($objIMB->imbTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objIMB->imbTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objIMB->imbTglDaftarUlang = date('Y-m-d', strtotime($objIMB->imbTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objIMB->imbTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objIMB->imbNo = 0;
		
		$objIMB->imbTglBerlaku = '0000-00-00';
		$objIMB->imbTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	//TODO: 271016: imb-s
	$objIMB->imbNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/IMB - S/'.substr($objIMB->imbTglPengesahan,0,4).'/'.$objIMB->imbNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objIMB->imbNo > 0){
		$isExist = $app->isExist("SELECT imbID AS id FROM imb WHERE imbNo='".$objIMB->imbNo."' AND YEAR(imbTglPengesahan)='".substr($objIMB->imbTglPengesahan,0,4)."'", $objIMB->imbID);
		if (!$isExist) {
			$doSave = false;
			
			$objIMBExist = $app->queryObject("SELECT * FROM imb WHERE imbNo='".$objIMB->imbNo."' AND YEAR(imbTglPengesahan)='".substr($objIMB->imbTglPengesahan,0,4)."'");
			
			$msg[] = '- Nomor Surat "'.$objIMB->imbNo.'" sudah ada, dengan Nomor "'.$objIMBExist->imbNoLengkap.'", Tgl. '.$app->MySQLDateToNormal($objIMBExist->imbTglPengesahan);
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objIMB->imbID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objIMB);
		} else {
			$sql = $app->createSQLforUpdate($objIMB);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objIMB->imbID = $app->queryField("SELECT LAST_INSERT_ID() FROM imb");
		}

		$success = true;
		$msg = 'Izin Mendirikan Bangunan berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'imb', $objIMB->imbID, $objIMB->imbNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objIMB->imbDmhnID."'");
		
		//Update pungutan imb
		$app->query("DELETE FROM pungutanimb WHERE pimbImbID='".$objIMB->imbID."'");
		
		$jumlah = 0;
		$jumlah_dibulatkan = 0;
		
		if (count($_REQUEST['pimbNama']) > 0) {
			foreach (array_keys($_REQUEST['pimbNama']) as $k) {
				$pimbNo = mysql_escape_string(trim($_REQUEST['pimbNo'][$k]));
				$pimbKelompok = mysql_escape_string(trim($_REQUEST['pimbKelompok'][$k]));
				$pimbNama = mysql_escape_string(trim($_REQUEST['pimbNama'][$k]));
				$pimbNilai1 = $app->MoneyToMySQL($_REQUEST['pimbNilai1'][$k]);
				$pimbSatuan1 = mysql_escape_string(trim($_REQUEST['pimbSatuan1'][$k]));
				$pimbNilai2 = $app->MoneyToMySQL($_REQUEST['pimbNilai2'][$k]);
				$pimbSatuan2 = mysql_escape_string(trim($_REQUEST['pimbSatuan2'][$k]));
				$pimbNilai3 = $app->MoneyToMySQL($_REQUEST['pimbNilai3'][$k]);
				$pimbSatuan3 = mysql_escape_string(trim($_REQUEST['pimbSatuan3'][$k]));
				$pimbSatuanLuas = mysql_escape_string(trim($_REQUEST['pimbSatuanLuas'][$k]));
				$pimbNilaiRupiah = $app->MoneyToMySQL($_REQUEST['pimbNilaiRupiah'][$k]);
				$pimbUnit = $app->MoneyToMySQL($_REQUEST['pimbUnit'][$k]);
				
				if ($pimbNama != '') {
					$app->query("INSERT INTO pungutanimb (
						pimbImbID, 
						pimbNo, 
						pimbKelompok, 
						pimbNama, 
						pimbNilai1, 
						pimbSatuan1, 
						pimbNilai2, 
						pimbSatuan2, 
						pimbNilai3, 
						pimbSatuan3, 
						pimbSatuanLuas, 
						pimbNilaiRupiah, 
						pimbUnit
					) VALUES (
						'".$objIMB->imbID."', 
						'".$pimbNo."', 
						'".$pimbKelompok."', 
						'".$pimbNama."', 
						'".$pimbNilai1."', 
						'".$pimbSatuan1."',
						'".$pimbNilai2."', 
						'".$pimbSatuan2."',
						'".$pimbNilai3."', 
						'".$pimbSatuan3."',
						'".$pimbSatuanLuas."',
						'".$pimbNilaiRupiah."',
						'".$pimbUnit."'
					)");
					
					$luas = 1;
					if ($pimbNilai1 > 0) {
						$luas = bcmul($luas, $pimbNilai1, 4);
						
						if ($pimbSatuan1 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					if ($pimbNilai2 > 0) {
						$luas = bcmul($luas, $pimbNilai2, 4);
						
						if ($pimbSatuan2 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					if ($pimbNilai3 > 0) {
						$luas = bcmul($luas, $pimbNilai3, 4);
						
						if ($pimbSatuan3 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					$luas = bcmul($luas, $pimbUnit, 4);
					$hasil = bcmul($luas, $pimbNilaiRupiah, 4);
					$jumlah = bcadd($jumlah, $hasil, 4);
				}
			}
		}
		
		//Pembulatan ke ratusan atas
		if ($jumlah % 100 > 0) {
			$jumlah = $jumlah - ($jumlah % 100) + 100;
		}
		$jumlah_dibulatkan = round($jumlah);
		
		//Update imbPungutanJumlah (yang sudah dibulatkan)
		$app->query("UPDATE imb SET imbPungutanJumlah='".$jumlah_dibulatkan."' WHERE imbID='".$objIMB->imbID."'");
	} else {
		$success = false;
		$msg = 'Izin Mendirikan Bangunan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objIMB->imbDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		IMB_View::editIMB($success, $msg, $objDetailPermohonan, $objIMB);
	}
}

function viewIMB($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('imb', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('imb', 'sort', 'imbTglPengesahan');
	//$dir		= $app->pageVar('imb', 'dir', 'desc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'bulan' => date('n'),
		'tahun' => date('Y')
	);
	
	$nama 		= $app->pageVar('imb', 'filternama', $default['nama'], 'strval');
	$bulan 		= $app->pageVar('imb', 'filterbulan', $default['bulan'], 'intval');
	$tahun 		= $app->pageVar('imb', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "imbNo LIKE '".$nama."%'";
	}
	if ($bulan > 0) {
		$filter[] = "MONTH(imbTglPengesahan)='".$bulan."'";
	}
	if ($tahun > 0) {
		$filter[] = "YEAR(imbTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'bulan' => $bulan,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM imb
				  LEFT JOIN detailpermohonan ON imbDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM imb
			LEFT JOIN detailpermohonan ON imbDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(imbTglPengesahan) DESC, imbNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	IMB_View::viewIMB($success, $msg, $arr);
}
?>