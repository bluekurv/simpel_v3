<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.bentukperusahaan.php';
require_once 'view.bentukperusahaan.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editBentukPerusahaan($app->id);
		break;
	case 'delete':
		deleteBentukPerusahaan($app->id);
		break;
	case 'save':
		saveBentukPerusahaan();
		break;
	default:
		viewBentukPerusahaan(true, '');
		break;
}

function deleteBentukPerusahaan($id) {
	global $app;
	
	//Get object
	$objBentukPerusahaan = $app->queryObject("SELECT * FROM bentukperusahaan WHERE bperID='".$id."'");
	if (!$objBentukPerusahaan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM bentukperusahaan WHERE bperID='".$id."'");
		
		$success = true;
		$msg = 'Bentuk Perusahaan "'.$objBentukPerusahaan->bperNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'bentukperusahaan', $objBentukPerusahaan->bperID, $objBentukPerusahaan->bperNama, $msg);
	} else {
		$success = false;
		$msg = 'Bentuk Perusahaan "'.$objBentukPerusahaan->bperNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewBentukPerusahaan($success, $msg);
}

function editBentukPerusahaan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objBentukPerusahaan = $app->queryObject("SELECT * FROM bentukperusahaan WHERE bperID='".$id."'");
		if (!$objBentukPerusahaan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objBentukPerusahaan = new BentukPerusahaan_Model();
	}
	
	BentukPerusahaan_View::editBentukPerusahaan(true, '', $objBentukPerusahaan);
}

function saveBentukPerusahaan() {
	global $app;
	
	//Create object
	$objBentukPerusahaan = new BentukPerusahaan_Model();
	$app->bind($objBentukPerusahaan);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objBentukPerusahaan->bperKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objBentukPerusahaan->bperNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT bperID AS id FROM bentukperusahaan WHERE bperKode='".$objBentukPerusahaan->bperKode."'", $objBentukPerusahaan->bperID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objBentukPerusahaan->bperKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT bperID AS id FROM bentukperusahaan WHERE bperNama='".$objBentukPerusahaan->bperNama."'", $objBentukPerusahaan->bperID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objBentukPerusahaan->bperNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objBentukPerusahaan->bperID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objBentukPerusahaan);
		} else {
			$sql = $app->createSQLforUpdate($objBentukPerusahaan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objBentukPerusahaan->bperID = $app->queryField("SELECT LAST_INSERT_ID() FROM bentukperusahaan");
			
			$success = true;
			$msg = 'Bentuk Perusahaan "'.$objBentukPerusahaan->bperNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'bentukperusahaan', $objBentukPerusahaan->bperID, $objBentukPerusahaan->bperNama, $msg);
		} else {
			$success = true;
			$msg = 'Bentuk Perusahaan "'.$objBentukPerusahaan->bperNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'bentukperusahaan', $objBentukPerusahaan->bperID, $objBentukPerusahaan->bperNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Bentuk Perusahaan "'.$objBentukPerusahaan->bperNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewBentukPerusahaan($success, $msg);
	} else {
		BentukPerusahaan_View::editBentukPerusahaan($success, $msg, $objBentukPerusahaan);
	}
}

function viewBentukPerusahaan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('bentukperusahaan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('bentukperusahaan', 'sort', 'bperKode');
	$dir		= $app->pageVar('bentukperusahaan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('bentukperusahaan', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(bperKode LIKE '%".$nama."%' OR bperNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM bentukperusahaan
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM bentukperusahaan
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	BentukPerusahaan_View::viewBentukPerusahaan($success, $msg, $arr);
}
?>