//Variables
var form = $("#myForm");
var bperKode = $("#bperKode");
var bperNama = $("#bperNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'bperKode' || this.id === undefined) {
		if (bperKode.val().length == 0){
			doSubmit = false;
			bperKode.addClass("error");		
		} else {
			bperKode.removeClass("error");
		}
	}
	
	if (this.id == 'bperNama' || this.id === undefined) {
		if (bperNama.val().length == 0){
			doSubmit = false;
			bperNama.addClass("error");		
		} else {
			bperNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
bperKode.blur(validateForm);
bperNama.blur(validateForm);

bperKode.keyup(validateForm);
bperNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
bperKode.focus();