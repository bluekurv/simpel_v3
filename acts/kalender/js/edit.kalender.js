//Variables
var form = $("#myForm");
var kalTgl = $("#kalTgl");
var kalNama = $("#kalNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'kalTgl' || this.id === undefined) {
		if (kalTgl.val().length != 10){
			doSubmit = false;
			kalTgl.addClass("error");		
		} else {
			kalTgl.removeClass("error");
		}
	}
	
	if (this.id == 'kalNama' || this.id === undefined) {
		if (kalNama.val().length == 0){
			doSubmit = false;
			kalNama.addClass("error");		
		} else {
			kalNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
kalTgl.blur(validateForm);
kalNama.blur(validateForm);

kalTgl.keyup(validateForm);
kalNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
//Di-comment karena pop up tanggal tidak muncul jika di-focus
//kalTgl.focus();