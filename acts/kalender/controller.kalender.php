<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kalender.php';
require_once 'view.kalender.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKalender($app->id);
		break;
	case 'delete':
		deleteKalender($app->id);
		break;
	case 'save':
		saveKalender();
		break;
	default:
		viewKalender(true, '');
		break;
}

function deleteKalender($id) {
	global $app;
	
	//Get object
	$objKalender = $app->queryObject("SELECT * FROM kalender WHERE kalID='".$id."'");
	if (!$objKalender) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM kalender WHERE kalID='".$id."'");
		
		$success = true;
		$msg = 'Tanggal "'.$app->MySQLDateToNormal($objKalender->kalTgl).'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'kalender', $objKalender->kalID, $objKalender->kalTgl, $msg);
	} else {
		$success = false;
		$msg = 'Tanggal "'.$app->MySQLDateToNormal($objKalender->kalTgl).'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKalender($success, $msg);
}

function editKalender($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKalender = $app->queryObject("SELECT * FROM kalender WHERE kalID='".$id."'");
		if (!$objKalender) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKalender = new Kalender_Model();
		$objKalender->kalTgl = date('Y-m-d');
	}
	
	Kalender_View::editKalender(true, '', $objKalender);
}

function saveKalender() {
	global $app;
	
	//Create object
	$objKalender = new Kalender_Model();
	$app->bind($objKalender);
	
	//Modify object (if necessary)
	$objKalender->kalTgl = $app->NormalDateToMySQL($objKalender->kalTgl);
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKalender->kalTgl == '0000-00-00') {
		$doSave = false;
		$msg[] = '- Tanggal belum diisi';
	}
	
	if ($objKalender->kalNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT kalID AS id FROM kalender WHERE kalTgl='".$objKalender->kalTgl."'", $objKalender->kalID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Tanggal "'.$app->MySQLDateToNormal($objKalender->kalTgl).'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKalender->kalID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKalender);
		} else {
			$sql = $app->createSQLforUpdate($objKalender);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKalender->kalID = $app->queryField("SELECT LAST_INSERT_ID() FROM kalender");
			
			$success = true;
			$msg = 'Tanggal "'.$app->MySQLDateToNormal($objKalender->kalTgl).'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'kalender', $objKalender->kalID, $objKalender->kalTgl, $msg);
		} else {
			$success = true;
			$msg = 'Tanggal "'.$app->MySQLDateToNormal($objKalender->kalTgl).'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'kalender', $objKalender->kalID, $objKalender->kalTgl, $msg);
		}
	} else {
		$success = false;
		$msg = 'Tanggal "'.$app->MySQLDateToNormal($objKalender->kalTgl).'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKalender($success, $msg);
	} else {
		Kalender_View::editKalender($success, $msg, $objKalender);
	}
}

function viewKalender($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kalender', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kalender', 'sort', 'kalTgl');
	$dir   		= $app->pageVar('kalender', 'dir', 'desc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('kalender', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "kalNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM kalender
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM kalender
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Kalender_View::viewKalender($success, $msg, $arr);
}
?>