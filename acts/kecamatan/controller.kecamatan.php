<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Loket Pelayanan']['all'] = true;
$acl['Petugas Administrasi']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kecamatan.php';
require_once 'view.kecamatan.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKecamatan($app->id);
		break;
	case 'delete':
		deleteKecamatan($app->id);
		break;
	case 'save':
		saveKecamatan();
		break;
	default:
		viewKecamatan(true, '');
		break;
}

function deleteKecamatan($id) {
	global $app;
	
	//Get object
	$objKecamatan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$id."' AND wilTingkat='Kecamatan'");
	if (!$objKecamatan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalKelurahan = intval($app->queryField("SELECT COUNT(*) AS total FROM wilayah WHERE wilParentID='".$id."' AND (wilTingkat='Kelurahan' OR wilTingkat='Desa')"));
	if ($totalKelurahan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKelurahan.' kelurahan/desa pada kecamatan tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM wilayah WHERE wilID='".$id."'");
		
		$success = true;
		$msg = $objKecamatan->wilTingkat.' "'.$objKecamatan->wilNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'wilayah', $objKecamatan->wilID, $objKecamatan->wilNama, $msg);
	} else {
		$success = false;
		$msg = $objKecamatan->wilTingkat.' "'.$objKecamatan->wilNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKecamatan($success, $msg);
}

function editKecamatan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKecamatan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$id."' AND wilTingkat='Kecamatan'");
		if (!$objKecamatan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKecamatan = new Kecamatan_Model();
	}
	
	Kecamatan_View::editKecamatan(true, '', $objKecamatan);
}

function saveKecamatan() {
	global $app;
	
	//Create object
	$objKecamatan = new Kecamatan_Model();
	$app->bind($objKecamatan);
	
	//Modify object (if necessary)
	$objKecamatan->wilTingkat = 'Kecamatan';
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKecamatan->wilKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKecamatan->wilKodeRetribusi == '') {
		$doSave = false;
		$msg[] = '- Kode (pada SIMRetribusi) belum diisi';
	}
	
	if ($objKecamatan->wilNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objKecamatan->wilParentID == 0) {
		$doSave = false;
		$msg[] = '- Berada di bawah belum diisi';
	}
	
	if ($objKecamatan->wilKode != '00') {
		$isExist = $app->isExist("SELECT wilID AS id FROM wilayah WHERE wilKode='".$objKecamatan->wilKode."' AND wilTingkat='Kecamatan' AND wilParentID='".$objKecamatan->wilParentID."'", $objKecamatan->wilID);
		if (!$isExist) {
			$doSave = false;
			$msg[]  = '- Kode "'.$objKecamatan->wilKode.'" sudah ada';
		}
	}
	
	$isExist = $app->isExist("SELECT wilID AS id FROM wilayah WHERE wilNama='".$objKecamatan->wilNama."' AND wilTingkat='Kecamatan' AND wilParentID='".$objKecamatan->wilParentID."'", $objKecamatan->wilID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama "'.$objKecamatan->wilNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKecamatan->wilID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKecamatan);
		} else {
			$sql = $app->createSQLforUpdate($objKecamatan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKecamatan->wilID = $app->queryField("SELECT LAST_INSERT_ID() FROM wilayah");
			
			$success = true;
			$msg = $objKecamatan->wilTingkat.' "'.$objKecamatan->wilNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'wilayah', $objKecamatan->wilID, $objKecamatan->wilNama, $msg);
		} else {
			$success = true;
			$msg = $objKecamatan->wilTingkat.' "'.$objKecamatan->wilNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'wilayah', $objKecamatan->wilID, $objKecamatan->wilNama, $msg);
		}
	} else {
		$success = false;
		$msg = $objKecamatan->wilTingkat.' "'.$objKecamatan->wilNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKecamatan($success, $msg);
	} else {
		Kecamatan_View::editKecamatan($success, $msg, $objKecamatan);
	}
}

function viewKecamatan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kecamatan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kecamatan', 'sort', 'kode');
	$dir   		= $app->pageVar('kecamatan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'parent' => 0
	);
	
	$nama 		= $app->pageVar('kecamatan', 'filternama', $default['nama'], 'strval');
	$parent 	= $app->pageVar('kecamatan', 'filterparent', $default['parent'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "wilayah.wilNama LIKE '%".$nama."%'";
	}
	if ($parent > $default['parent']) {
		$filter[] = "wilayah.wilParentID='".$parent."'";
	}
	$filter[] = "wilayah.wilTingkat='Kecamatan'";
	$filter[] = "wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'parent' => $parent
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT 
				wilayah.*, 
				CONCAT(wilayah3.wilKode,'.',wilayah2.wilKode,'.',wilayah.wilKode) AS kode, CONCAT(wilayah2.wilTingkat,' ',wilayah2.wilNama) AS parent, 
				(SELECT COUNT(*) AS total FROM wilayah AS sub WHERE sub.wilParentID=wilayah.wilID AND (sub.wilTingkat='Kelurahan' OR sub.wilTingkat='Desa')) AS total
			FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Kecamatan_View::viewKecamatan($success, $msg, $arr);
}
?>