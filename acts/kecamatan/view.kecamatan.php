<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Kecamatan_View {
	static function editKecamatan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kecamatan&task=edit&id=<?php echo $obj->wilID; ?>"><img src="images/icons/flag_yellow.png" border="0" /> <?php echo ($obj->wilID > 0) ? 'Ubah' : 'Tambah'; ?> Kecamatan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=kecamatan&task=save" method="POST" >
				<input type="hidden" id="wilID" name="wilID" value="<?php echo $obj->wilID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=kecamatan">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Berada di</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		$provinsi = $app->queryArrayOfObjects("SELECT wilID AS id, wilayah.* FROM wilayah WHERE wilTingkat='Provinsi' ORDER BY wilKode");
		$kabupaten = $app->queryArrayOfObjects("SELECT wilID AS id, wilParentID AS parentid, wilayah.* FROM wilayah WHERE (wilTingkat='Kabupaten' OR wilTingkat='Kota') ORDER BY wilKode", 2);
?>
						<select class="box" id="wilParentID" name="wilParentID">
<?php
		if (count($provinsi) > 0) {
			foreach ($provinsi as $v) {
				echo '<optgroup label="'.$v->wilKode.' - '.$v->wilTingkat.' '.$v->wilNama.'">';
				
				if (isset($kabupaten[$v->wilID])) {
					if (count($kabupaten[$v->wilID]) > 0) {
						foreach ($kabupaten[$v->wilID] as $v2) {
							echo '<option value="'.$v2->wilID.'"';
							if ($v2->wilID == $obj->wilParentID) {
								echo ' selected';
							}
							echo '>'.$v->wilKode.'.'.$v2->wilKode.' - '.$v2->wilTingkat.' '.$v2->wilNama.'</option>';
						}
					}
				}
				
				echo '</optgroup>';
			}
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Kode <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="wilKode" name="wilKode" maxlength="5" size="5" value="<?php echo $obj->wilKode; ?>" />
						(<i>Jika belum diketahui, masukkan <b>00</b></i>)
					</td>
				</tr>
				<tr>
					<td>Kode (pada SIMRetribusi) <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="wilKodeRetribusi" name="wilKodeRetribusi" maxlength="5" size="5" value="<?php echo $obj->wilKodeRetribusi; ?>" />
						(<i>Jika belum diketahui, masukkan <b>00</b></i>)
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						Kecamatan <input class="box" id="wilNama" name="wilNama" maxlength="255" size="50" value="<?php echo $obj->wilNama; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kecamatan/js/edit.kecamatan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewKecamatan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kecamatan"><img src="images/icons/flag_yellow.png" border="0" /> Kecamatan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=kecamatan&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
<?php
		$provinsi = $app->queryArrayOfObjects("SELECT wilID AS id, wilayah.* FROM wilayah WHERE wilTingkat='Provinsi' ORDER BY wilNama");
		$kabupaten = $app->queryArrayOfObjects("SELECT wilID AS id, wilParentID AS parentid, wilayah.* FROM wilayah WHERE (wilTingkat='Kabupaten' OR wilTingkat='Kota') ORDER BY wilTingkat, wilNama", 2);
?>
				<select class="box" id="filterparent" name="filterparent">
					<option value="0">--Seluruh Kabupaten/Kota--</option>
<?php
		if (count($provinsi) > 0) {
			foreach ($provinsi as $v) {
				echo '<optgroup label="'.$v->wilTingkat.' '.$v->wilNama.'">';
				
				if (isset($kabupaten[$v->wilID])) {
					if (count($kabupaten[$v->wilID]) > 0) {
						foreach ($kabupaten[$v->wilID] as $v2) {
							echo '<option value="'.$v2->wilID.'"';
							if ($v2->wilID == $arr['filter']['parent']) {
								echo ' selected';
							}
							echo '>'.$v2->wilTingkat.' '.$v2->wilNama.'</option>';
						}
					}
				}
				
				echo '</optgroup>';
			}
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['parent'] != $arr['default']['parent']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('kode', 'Kode', true, 40),
			$app->setHeader('wilKodeRetribusi', 'Kode (pada SIMRetribusi)', true, 40),
			$app->setHeader('wilTingkat', 'Tingkat', true, 60),
			$app->setHeader('wilNama', 'Nama', true),
			$app->setHeader('parent', 'Berada di', true),
			$app->setHeader('total', 'Total Kelurahan/Desa', true, 0, 'right')
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=kecamatan&task=edit&id='.$v->wilID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->wilID.', '."'".$v->wilTingkat." ".$v->wilNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=kecamatan&task=edit&id='.$v->wilID.'">'.$v->kode.'</a></td>';
				echo '<td>'.$v->wilKodeRetribusi.'</td>';
				echo '<td>'.$v->wilTingkat.'</td>';
				echo '<td>'.$v->wilNama.'</td>';
				echo '<td>'.$v->parent.'</td>';
				echo '<td align="right">'.$v->total.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="7">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kecamatan/js/kecamatan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>