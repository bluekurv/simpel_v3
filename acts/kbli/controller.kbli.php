<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kbli.php';
require_once 'view.kbli.php';

switch ($app->task) {
	default:
		viewKBLI();
		break;
}

function viewKBLI() {
	global $app;
	
	$arr = array(
		'golonganpokok' => array(),
		'golongan' => array(),
		'subgolongan' => array(),
		'kelompok' => array()
	);
	
	//Query
	$sql = "SELECT *, kblipublikasi.kpubID AS id FROM kblipublikasi ORDER BY kpubNama";
	$arr['publikasi'] = $app->queryArrayOfObjects($sql);
	
	//Query
	$sql = "SELECT *, kblikategori.kkatID AS id, kblikategori.kkatKpubID AS parentid FROM kblikategori ORDER BY kkatKode, kkatNama";
	$arr['kategori'] = $app->queryArrayOfObjects($sql, 2);
	
	//Query
	$sql = "SELECT *, kbligolonganpokok.kgpokID AS id, kbligolonganpokok.kgpokKkatID AS parentid FROM kbligolonganpokok ORDER BY kgpokKode, kgpokNama";
	$arr['golonganpokok'] = $app->queryArrayOfObjects($sql, 2);
	
	//Query
	$sql = "SELECT *, kbligolongan.kgolID AS id, kbligolongan.kgolKgpokID AS parentid FROM kbligolongan ORDER BY kgolKode, kgolNama";
	$arr['golongan'] = $app->queryArrayOfObjects($sql, 2);
	$app->debug('golongan: '.count($arr['golongan']));
	//Query
	$sql = "SELECT *, kblisubgolongan.ksubID AS id, kblisubgolongan.ksubKgolID AS parentid FROM kblisubgolongan ORDER BY ksubKode, ksubNama";
	$arr['subgolongan'] = $app->queryArrayOfObjects($sql, 2);
	$app->debug('subgolongan: '.count($arr['subgolongan']));
	//Query
	$sql = "SELECT *, kblikelompok.kkelID AS id, kblikelompok.kkelKsubID AS parentid FROM kblikelompok ORDER BY kkelKode, kkelNama";
	$arr['kelompok'] = $app->queryArrayOfObjects($sql, 2);
	$app->debug('kelompok: '.count($arr['kelompok']));
	KBLI_View::viewKBLI($arr);
}
?>