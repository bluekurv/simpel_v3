<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class KBLI_View {
	static function menuKBLI($menu = '') {
?>
	<div id="toolbarEntri">
		<a class="button-link inline <?php echo ($menu == 'kbli' ? '' : 'grey'); ?>" href="index2.php?act=kbli">Lihat Semua</a>
		<span style="font-size:15px; font-weight:bold; font-family: 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;">atau kelola</span>
		<a class="button-link inline <?php echo ($menu == 'kblipublikasi' ? '' : 'grey'); ?>" href="index2.php?act=kblipublikasi&page=1">Publikasi</a>
		<a class="button-link inline <?php echo ($menu == 'kblikategori' ? '' : 'grey'); ?>" href="index2.php?act=kblikategori&page=1">Kategori</a>
		<a class="button-link inline <?php echo ($menu == 'kbligolonganpokok' ? '' : 'grey'); ?>" href="index2.php?act=kbligolonganpokok&page=1">Golongan Pokok</a>
		<a class="button-link inline <?php echo ($menu == 'kbligolongan' ? '' : 'grey'); ?>" href="index2.php?act=kbligolongan&page=1">Golongan</a>
		<a class="button-link inline <?php echo ($menu == 'kblisubgolongan' ? '' : 'grey'); ?>" href="index2.php?act=kblisubgolongan&page=1">Sub Golongan</a>
		<a class="button-link inline <?php echo ($menu == 'kblikelompok' ? '' : 'grey'); ?>" href="index2.php?act=kblikelompok&page=1">Kelompok</a>
	</div>
<?php
	}
	
	static function viewKBLI($arr) {
		global $app;
?>
	<link rel="stylesheet" type="text/css" href="js/jquery.treeview/jquery.treeview.css">
	<script type="text/javascript" src="js/jquery.treeview/jquery.treeview.js"></script>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kbli"><img src="images/icons/database_table.png" border="0" /> Klasifikasi Lapangan Usaha</a></h1>
		<?php KBLI_View::menuKBLI('kbli'); ?>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php 
		if (count($arr['publikasi']) > 0) {
?>
			<ul id="browser" class="filetree treeview">
<?php 
			foreach ($arr['publikasi'] as $pub) {
?>			
				<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $pub->kpubNama; ?></span>
					<ul>
<?php 
				if (isset($arr['kategori'][$pub->kpubID])) {
					foreach ($arr['kategori'][$pub->kpubID] as $kat) {
?>
						<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $kat->kkatKode.' - '. $kat->kkatNama; ?></span>
							<ul>
<?php 
						if (isset($arr['golonganpokok'][$kat->kkatID])) {
							foreach ($arr['golonganpokok'][$kat->kkatID] as $gpok) {
?>
								<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $gpok->kgpokKode.' - '. $gpok->kgpokNama; ?></span>
									<ul>
<?php 
								if (isset($arr['golongan'][$gpok->kgpokID])) {
									foreach ($arr['golongan'][$gpok->kgpokID] as $gol) {
?>
										<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $gol->kgolKode.' - '. $gol->kgolNama; ?></span>
											<ul>
<?php 
										if (isset($arr['subgolongan'][$gol->kgolID])) {
											foreach ($arr['subgolongan'][$gol->kgolID] as $sub) {
?>
												<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $sub->ksubKode.' - '. $sub->ksubNama; ?></span>
													<ul>
<?php 
												if (isset($arr['kelompok'][$sub->ksubID])) {
													foreach ($arr['kelompok'][$sub->ksubID] as $kel) {
?>
														<li><span class="file"><?php echo $kel->kkelKode.' - '.$kel->kkelNama; ?></span></li>
<?php
													}
												}
?>
													</ul>
												</li>
<?php
											}
										}
?>
											</ul>
										</li>
<?php
									}
								}
?>										
									</ul>
								</li>
<?php
							}
						}
?>
							</ul>
						</li>
<?php
					}
				}
?>
					</ul>
				</li>
<?php 
			}
?>
			</ul>
<?php 
		}
?>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kbli/js/kbli.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>