<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'perusahaanpenyediajasapekerja';
$childTitle = 'Izin Operasional Perusahaan Penyedia Jasa Pekerja atau Buruh';
$childKodeSurat = 'IOP-PJPB';
$childTembusan = 'Kepala Dinas Pendapatan Daerah Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>