<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Impor_View {
	static function daftarizinImpor($arr) {
		global $app;
		
		//NOTE: konek dengan simpel_v3
		$db = mysql_select_db("simpel_v3_110216", $app->connection);
		if (!$db) {
			die('Could not open database: simpel_v3');
		}
		$submit = false;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=impor&task=daftarizin"><img src="images/icons/calendar.png" border="0" /> Impor Daftar dan Izin</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<table class="dataTable">
			<tbody>
<?php
		$lokasi = 2306;	//NOT FOUND
		$notfound = array();

		if (count($arr['data']) > 0) {
			foreach ($arr['data'] as $v) {
				/*echo '<tr>';
				echo '<td>'.$v['pemohon']['m_pemohon.id_pemohon'].'</td>';
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_ktp'].'</td>';
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_nama'].'</td>';
				
				//Alamat yang mengandung nama kelurahan/kecamatan
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_alamat'].'</td>';
				
				//TODO: bagaimana jika tidak ada di tabel simpel_v3.kelurahan?
				$str = $v['pemohon']['m_pemohon.id_tree'].' - '.$v['pemohon']['m_tree.tree_desc'];
				$ok = true;
				if (isset($arr['regional'][$v['pemohon']['m_pemohon.id_tree']])) {
					$str .= '<br>--><br>'.$arr['regional'][$v['pemohon']['m_pemohon.id_tree']];
				} else {
					$str .= '<br>--><br>Tidak ditemukan di kppt2.regional';
					$ok = false;
				}
				if (isset($arr['kelurahan'][$v['pemohon']['m_pemohon.id_tree']])) {
					$str .= '<br>--><br>'.$arr['kelurahan'][$v['pemohon']['m_pemohon.id_tree']];
				} else {
					$str .= '<br>--><br>Tidak ditemukan di simpel_v3.kelurahan, gunakan '.$lokasi;
					$ok = false;
				}
				echo '<td '.($ok ? '' : 'style="background-color:#FF0000; color:#FFFFFF;"').'>'.$str.'</td>';
				
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_phone'].'</td>';
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_hp'].'</td>';
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_tgllahir'].'</td>';
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_tmplahir'].'</td>';
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_bangsa'].'</td>';
				echo '<td>'.$v['pemohon']['m_pemohon.pemohon_kerja'].'</td>';
				echo "</tr>\n";*/
				
				if (count($v['daftardanizin']) > 0) {
					/*echo '<tr>';
					echo '<td></td>';
					echo '<td colspan="6" style="background:#669; color:#FFFFFF;">trans_daftar:</td>';
					echo '<td colspan="23" style="background:#336; color:#FFFFFF;">trans_izin:</td>';
					echo '</tr>';
					
					echo '<tr style="background:#D0DAFD;">';
					echo '<td style="background:#FFFFFF;">&nbsp;</td>';
					echo '<td><b>ID Daftar</b></td>';
					echo '<td><b>ID Izin</b></td>';
					echo '<td><b>No. Daftar</b></td>';
					echo '<td><b>Tgl. Daftar</b></td>';
					echo '<td><b>Status</b></td>';
					echo '<td><b>Jenis</b></td>';
					echo '<td><b>ID Tree</b></td>';
					echo '<td><b>ID Master (jenis periwisata, status siup, jenis bangunan imb)</b></td>';
					echo '<td><b>Jenis Usaha</b></td>';
					echo '<td><b>Merek Usaha</b></td>';
					echo '<td><b>pngjawab</b></td>';
					echo '<td><b>alamatpngjawab</b></td>';
					echo '<td><b>npwpd</b></td>';
					echo '<td><b>panjang</b></td>';
					echo '<td><b>lebar</b></td>';
					echo '<td><b>tinggi</b></td>';
					echo '<td><b>lokasi</b></td>';
					echo '<td><b>timur</b></td>';
					echo '<td><b>barat</b></td>';
					echo '<td><b>utara</b></td>';
					echo '<td><b>selatan</b></td>';
					echo '<td><b>sk</b></td>';
					echo '<td><b>tgl</b></td>';
					echo '<td><b>id_user (yang menginput pendataan)</b></td>';
					echo '<td><b>tanah (status tanah pada situ)</b></td>';
					echo '<td><b>namaperusahaan (untuk siup, tdi)</b></td>';
					echo '<td><b>telp</b></td>';
					echo '<td><b>rekomendasi (untuk HO)</b></td>';
					echo '<td><b>fax</b></td>';
					echo '</tr>';*/
					
					foreach ($v['daftardanizin'] as $v2) {
						/*echo '<tr class="odd">';
						echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
						echo '<td>'.$v2['trans_daftar.id_daftar'].'</td>';
						echo '<td>'.$v2['trans_daftar.id_izin'].' - '.$v2['m_izin.izin_name'].'</td>';
						echo '<td>'.$v2['trans_daftar.daftar_no'].'</td>';
						echo '<td>'.substr($v2['trans_daftar.daftar_tgl'],0,10).'</td>';
						
						//TODO: bagaimana dengan status selain Selesai?
						echo '<td>'.$v2['trans_daftar.daftar_status'].'</td>';
						
						echo '<td>'.$v2['trans_daftar.daftar_jenis'].'</td>';
						
						//TODO: bagaimana jika tidak ada di tabel simpel_v3.kelurahan?
						$str = $v2['trans_izin.id_tree'].' - '.$v2['m_tree2.tree_desc'];
						$ok = true;
						if (isset($arr['regional'][$v2['trans_izin.id_tree']])) {
							$str .= '<br>--><br>'.$arr['regional'][$v2['trans_izin.id_tree']];
						} else {
							$str .= '<br>--><br>Tidak ditemukan di kppt2.regional';
							$ok = false;
						}
						if (isset($arr['kelurahan'][$v2['trans_izin.id_tree']])) {
							$str .= '<br>--><br>'.$arr['kelurahan'][$v2['trans_izin.id_tree']];
						} else {
							$str .= '<br>--><br>Tidak ditemukan di simpel_v3.kelurahan, gunakan '.$lokasi;
							$ok = false;
						}
						echo '<td '.($ok ? '' : 'style="background-color:#FF0000; color:#FFFFFF;"').'>'.$str.'</td>';
						
						echo '<td>'.$v2['trans_izin.id_master'].' - '.$v2['m_master.master_desc'].' - '.$v2['m_master.master_value'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_jnsusaha'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_merkusaha'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_pngjawab'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_alamatpngjawab'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_npwpd'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_panjang'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_lebar'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_tinggi'].'</td>';
						
						//Alamat yang mengandung nama kelurahan/kecamatan
						echo '<td>'.$v2['trans_izin.transizin_lokasi'].'</td>';
						
						echo '<td>'.$v2['trans_izin.transizin_timur'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_barat'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_utara'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_selatan'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_sk'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_tgl'].'</td>';
						echo '<td>'.$v2['trans_izin.id_user'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_tanah'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_namaperusahaan'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_telp'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_rekomendasi'].'</td>';
						echo '<td>'.$v2['trans_izin.transizin_fax'].'</td>';
						echo '</tr>';*/
						
						echo '<tr>';
						echo '<td colspan="30">';
						
						$perID = 0;
						$perNama = '';
						
						$merkusaha = trim(preg_replace('!\s+!', ' ', strtolower($v2['trans_izin.transizin_merkusaha'])));
						$namaperusahaan = trim(preg_replace('!\s+!', ' ', strtolower($v2['trans_izin.transizin_namaperusahaan'])));
						
						if (($perNama == "" || $perNama == "-") && isset($arr['perusahaan'][$merkusaha]->perID)) {
							$perID = $arr['perusahaan'][$merkusaha]->perID;
							$perNama = strtolower(preg_replace('!\s+!', ' ', $v2['trans_izin.transizin_merkusaha']));
						}
						if (($perNama == "" || $perNama == "-") && isset($arr['perusahaan'][$namaperusahaan]->perID)) {
							$perID = $arr['perusahaan'][$namaperusahaan]->perID;
							$perNama = strtolower(preg_replace('!\s+!', ' ', $v2['trans_izin.transizin_namaperusahaan']));
						}
						if ($perNama == "" || $perNama == "-") {
							$merkusaha = str_replace("'", '', $merkusaha);
							$merkusaha = trim(preg_replace('!\s+!', ' ', $merkusaha));
							
							$namaperusahaan = str_replace("'", '', $namaperusahaan);
							$namaperusahaan = trim(preg_replace('!\s+!', ' ', $namaperusahaan));
							
							if (($perNama == "" || $perNama == "-") && isset($arr['perusahaan'][$merkusaha]->perID)) {
								$perID = $arr['perusahaan'][$merkusaha]->perID;
								$perNama = $merkusaha;
							}
							if (($perNama == "" || $perNama == "-") && isset($arr['perusahaan'][$namaperusahaan]->perID)) {
								$perID = $arr['perusahaan'][$namaperusahaan]->perID;
								$perNama = $namaperusahaan;
							}
						}
						
						/*echo "{$merkusaha}<br>";
						echo var_dump($merkusaha)."<br>";
						echo "{$namaperusahaan}<br>";
						echo "perID : {$perID}<br>";
						echo "perNama : {$perNama}<br>";
						$app->debug($arr['perusahaan'][$merkusaha]);
						$app->debug($arr['perusahaan'][$namaperusahaan]);
						$app->debug($arr['perusahaan'][$perNama]);*/
						
						$mhnKelurahanID = 0;
						if (isset($arr['kelurahan'][$v2['m_pemohon.id_tree']])) {
							$mhnKelurahanID = $arr['kelurahan'][$v2['m_pemohon.id_tree']];
						}
						
						$hp = array();
						if (strlen($v2['m_pemohon.pemohon_phone']) > 1) {
							$hp[] = $v2['m_pemohon.pemohon_phone'];
						}
						if (strlen($v2['m_pemohon.pemohon_hp']) > 1) {
							$hp[] = $v2['m_pemohon.pemohon_hp'];
						}
						
						$sql = "INSERT INTO permohonan (
									mhnLokID,
									mhnPerID,
									mhnTgl,
									mhnNoUrut,
									mhnNoUrutLengkap,
									mhnNoKtp,
									mhnNama,
									mhnAlamat,
									mhnKelurahanID,
									mhnPekerjaan,
									mhnTempatLahir,
									mhnTglLahir,
									mhnKewarganegaraan,
									mhnNoTelp,
									mhnDibuatOleh,
									mhnDibuatPada,
									mhnImpor
								) VALUES (
									1,
									'".$perID."',
									'".substr($v2['trans_daftar.daftar_tgl'],0,10)."',
									'".intval(substr($v2['trans_daftar.daftar_no'],-4))."',
									'".$v2['trans_daftar.daftar_no']."',
									'".$v2['m_pemohon.pemohon_ktp']."',
									'".mysql_real_escape_string($v2['m_pemohon.pemohon_nama'])."',
									'".mysql_real_escape_string($v2['m_pemohon.pemohon_alamat'])."',
									'".$mhnKelurahanID."',
									'".$v2['m_pemohon.pemohon_kerja']."',
									'".$v2['m_pemohon.pemohon_tmplahir']."',
									'".$v2['m_pemohon.pemohon_tgllahir']."',
									'".$v2['m_pemohon.pemohon_bangsa']."',
									'".implode(', ',$hp)."',
									1,
									'".date('Y-m-d')."',
									'".$v2['trans_daftar.id_daftar']."'
								);";
						$app->debug($sql);
						if ($submit) {
							$app->query($sql);
						}
						echo '<br>';
						
						/*$sql = "SELECT LAST_INSERT_ID() FROM permohonan";
						$app->debug($sql);
						$mhnID = 0;
						if ($submit) {
							$mhnID = $app->queryField($sql);
						}
						echo '<br>';*/
						
						$sql = "INSERT INTO detailpermohonan (
									dmhnMhnID,
									dmhnPerID,
									dmhnJsiID,
									dmhnTipe,
									dmhnPola,
									dmhnTelahSelesai,
									dmhnTelahAmbil,
									dmhnDiadministrasikanOleh,
									dmhnDiadministrasikanPada,
									dmhnImpor
								) VALUES (
									(SELECT MAX(mhnID) FROM permohonan),
									'".$perID."',
									'".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiID."',
									'".$v2['trans_daftar.daftar_jenis']."',
									1,
									1,
									1,
									1,
									'".date('Y-m-d')."',
									".$v2['trans_izin.id_transizin']."
								);";
						$app->debug($sql);
						if ($submit) {
							$app->query($sql);
						}
						echo '<br>';
						
						/*$sql = "SELECT LAST_INSERT_ID() FROM detailpermohonan";
						$app->debug($sql);
						$dmhnID = 0;
						if ($submit) {
							$dmhnID = $app->queryField($sql);
						}
						echo '<br>';*/
						
						echo '</td>';
						echo '</tr>';
						
						switch ($v2['trans_daftar.id_izin']) {
							case 2: 
								//Izin Gangguan (HO)
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$arr_nolengkap = explode('/',$nolengkap);
								$no = $arr_nolengkap[count($arr_nolengkap) - 1];
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								//$app->debug($v2);
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku > 0) {
									$tglberlaku = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku." years"));
								} else {
									$tglberlaku = '0000-00-00';
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang > 0) {
									$tgldaftarulang = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang." years"));
								} else {
									$tgldaftarulang = '0000-00-00';
								}
								
								$sql = "INSERT INTO situho (
											situhoDmhnID,
											situhoNo,
											situhoNoLengkap,
											situhoNPWPD,
											situhoJenisUsaha,
											situhoBidangUsaha,
											situhoMerekUsaha,
											situhoLuasTempatUsaha,
											situhoStatusTempatUsaha,
											situhoRekomendasiCamat,
											situhoBatasUtara,
											situhoBatasSelatan,
											situhoBatasBarat,
											situhoBatasTimur,
											situhoKeterangan,
											situhoTglBerlaku,
											situhoTglDaftarUlang,
											situhoPejID,
											situhoTglPengesahan,
											situhoImpor 
										) VALUES (
											(SELECT MAX(dmhnID) FROM detailpermohonan),
											'".$no."',
											'".$nolengkap."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_npwpd'])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_jnsusaha'])."',
											'',
											'".mysql_real_escape_string($v2['trans_izin.transizin_merkusaha'])."',
											'',
											'".$v2['trans_izin.transizin_tanah']."',
											'".$v2['trans_izin.transizin_rekomendasi']."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_utara'])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_selatan'])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_barat'])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_timur'])."',
											'',
											'".$tglberlaku."',
											'".$tgldaftarulang."',
											'".$pejid."',
											'".$tglpengesahan."',
											1
										);";
								$app->debug($sql);
								if ($submit) {
									$app->query($sql);
								}
								echo '<br>';
								echo '</td>';
								echo '</tr>';
								break;
							case 3: 
								//Izin Pemasangan Reklame
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$arr_nolengkap = explode('/',$nolengkap);
								$no = $arr_nolengkap[count($arr_nolengkap) - 1];
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								
								if (isset($arr['kelurahan'][$v2['trans_izin.id_tree']])) {
									$kelurahan_id = $arr['kelurahan'][$v2['trans_izin.id_tree']];
								} else {
									$kelurahan_id = $lokasi;
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku > 0) {
									$tglberlaku = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku." years"));
								} else {
									$tglberlaku = '0000-00-00';
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang > 0) {
									$tgldaftarulang = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang." years"));
								} else {
									$tgldaftarulang = '0000-00-00';
								}
								
								$sql = "INSERT INTO reklame (
											reklameDmhnID,
											reklameNo,
											reklameNoLengkap,
											reklameBahan,
											reklameBunyi,
											reklamePanjang,
											reklameLebar,
											reklameJumlah,
											reklamePemasanganLokasi,
											reklamePemasanganKelurahanID,
											reklamePemasanganTglMulai,
											reklamePemasanganTglSampai,
											reklameKeterangan,
											reklameTglBerlaku,
											reklameTglDaftarUlang,
											reklamePejID,
											reklameTglPengesahan,
											reklameImpor 
										) VALUES (
											(SELECT MAX(dmhnID) FROM detailpermohonan),
											'".$no."',
											'".$nolengkap."',
											'".mysql_real_escape_string($arr['reklame'][$v2['trans_izin.id_transizin']]->transrek_bahan)."',
											'".mysql_real_escape_string($arr['reklame'][$v2['trans_izin.id_transizin']]->transrek_bunyi)."',
											'".$v2['trans_izin.transizin_panjang']."',
											'".$v2['trans_izin.transizin_lebar']."',
											'".$arr['reklame'][$v2['trans_izin.id_transizin']]->transrek_jumlah."',
											'".mysql_real_escape_string($arr['reklame'][$v2['trans_izin.id_transizin']]->transrek_lokasi)."',
											'".$kelurahan_id."',
											'".substr($arr['reklame'][$v2['trans_izin.id_transizin']]->transrek_mulai,0,10)."',
											'".substr($arr['reklame'][$v2['trans_izin.id_transizin']]->transrek_hingga,0,10)."',
											'',
											'".$tglberlaku."',
											'".$tgldaftarulang."',
											'".$pejid."',
											'".$tglpengesahan."',
											'".$arr['reklame'][$v2['trans_izin.id_transizin']]->id_transreklame."'
										);";
								$app->debug($sql);
								if ($submit) {
									$app->query($sql);
								}
								echo '<br>';
								echo '</td>';
								echo '</tr>';
								break;
							case 4: 
								//Surat Izin Usaha Perdagangan  (SIUP)
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$arr_nolengkap = explode('/',$nolengkap);
								$no = $arr_nolengkap[count($arr_nolengkap) - 1];
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								
								if (isset($arr['kelurahan'][$v2['trans_izin.id_tree']])) {
									$kelurahan_id = $arr['kelurahan'][$v2['trans_izin.id_tree']];
								} else {
									$kelurahan_id = $lokasi;
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku > 0) {
									$tglberlaku = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku." years"));
								} else {
									$tglberlaku = '0000-00-00';
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang > 0) {
									$tgldaftarulang = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang." years"));
								} else {
									$tgldaftarulang = '0000-00-00';
								}
								
								$sql = "INSERT INTO siup (
											siupDmhnID,
											siupNo,
											siupNoLengkap,
											siupMaksud,
											siupPembaharuan,
											siupBentukBadan,
											siupMerekUsaha,
											siupAlamat,
											siupKelurahanID,
											siupLokasi,
											siupNoTelp,
											siupNoFax,
											siupStatusTempatUsaha,
											siupNPWP,
											siupKodePos,
											siupNamaPemilik,
											siupJabatanPemilik,
											siupTmpLahirPemilik,
											siupTglLahirPemilik,
											siupAlamatPemilik,
											siupNoTelpPemilik,
											siupNoFaxPemilik,
											siupNoKtpPemilik,
											siupPassportPemilik,
											siupKewarganegaraanPemilik,
											siupBankDNNama1,
											siupBankDNAlamat1,
											siupBankDNNama2,
											siupBankDNAlamat2,
											siupBankDNNama3,
											siupBankDNAlamat3,
											siupBankLNNama1,
											siupBankLNAlamat1,
											siupBankLNNama2,
											siupBankLNAlamat2,
											siupBankLNNama3,
											siupBankLNAlamat3,
											siupJenis,
											siupModal,
											siupKegiatanUsaha,
											siupKelembagaan,
											siupBidangUsaha,
											siupJenisBarangJasa,
											siupKluiID,
											siupKluiID2,
											siupKluiID3,
											siupKluiKode,
											siupKluiKode2,
											siupKluiKode3,
											siupKluiNama,
											siupKluiNama2,
											siupKluiNama3,
											siupNoAktaPT,
											siupTglAktaPT,
											siupNoPengesahanPT,
											siupTglPengesahanPT,
											siupNoAktaKop,
											siupTglAktaKop,
											siupNoPengesahanKop,
											siupTglPengesahanKop,
											siupNoAktaCV,
											siupTglAktaCV,
											siupNoPengesahanCV,
											siupTglPengesahanCV,
											siupNoAktaFirma,
											siupTglAktaFirma,
											siupNoPengesahanFirma,
											siupTglPengesahanFirma,
											siupTotalSaham,
											siupSahamNasional,
											siupSahamAsing,
											siupNoAktaPTPerub,
											siupTglAktaPTPerub,
											siupNoPengesahanPTPerub,
											siupTglPengesahanPTPerub,
											siupNoAktaKopPerub,
											siupTglAktaKopPerub,
											siupNoPengesahanKopPerub,
											siupTglPengesahanKopPerub,
											siupNoAktaCVPerub,
											siupTglAktaCVPerub,
											siupNoPengesahanCVPerub,
											siupTglPengesahanCVPerub,
											siupNoAktaFirmaPerub,
											siupTglAktaFirmaPerub,
											siupNoPengesahanFirmaPerub,
											siupTglPengesahanFirmaPerub,
											siupTotalSahamPerub,
											siupSahamNasionalPerub,
											siupSahamAsingPerub,
											siupKeterangan,
											siupTglBerlaku,
											siupTglDaftarUlang,
											siupPejID,
											siupTglPengesahan,
											siupImpor 
										) VALUES (
											(SELECT MAX(dmhnID) FROM detailpermohonan),
											'".$no."',
											'".$nolengkap."',
											'0',
											'0',
											'".mysql_real_escape_string($arr['bentukperusahaan'][$arr['siup'][$v2['trans_izin.id_transizin']]->id_master])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_merkusaha'])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_lokasi'])."',
											'".$kelurahan_id."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_lokasi'])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_telp'])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_fax'])."',
											'',
											'".mysql_real_escape_string($v2['trans_izin.transizin_npwpd'])."',
											'',
											'".mysql_real_escape_string($v2['trans_izin.transizin_pngjawab'])."',
											'',
											'".mysql_real_escape_string($v2['m_pemohon.pemohon_tmplahir'])."',
											'".mysql_real_escape_string($v2['m_pemohon.pemohon_tgllahir'])."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_alamatpngjawab'])."',
											'".mysql_real_escape_string($v2['m_pemohon.pemohon_phone'])."',
											'',
											'".mysql_real_escape_string($v2['m_pemohon.pemohon_ktp'])."',
											'',
											'".mysql_real_escape_string($v2['m_pemohon.pemohon_bangsa'])."',
											'',
											'',
											'',
											'',
											'',
											'',
											'',
											'',
											'',
											'',
											'',
											'',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_jenispembantu)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_modal)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_kegiatan)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_lembaga)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_bidang)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_utama)."',
											'0',
											'0',
											'0',
											'',
											'',
											'',
											'',
											'',
											'',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_totalsaham)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_nasional)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_asing)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_nomorakta)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_tglakta)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_nomorsah)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_tglsah)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_nomorakta_ubah)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_tglakta_ubah)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_nomorsah_ubah)."',
											'".mysql_real_escape_string($arr['siup'][$v2['trans_izin.id_transizin']]->transsiup_tglsah_ubah)."',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'0000-00-00',
											'',
											'',
											'',
											'',
											'".$tglberlaku."',
											'".$tgldaftarulang."',
											'".$pejid."',
											'".$tglpengesahan."',
											'".$arr['siup'][$v2['trans_izin.id_transizin']]->id_transsiup."'
										);";
								$app->debug($sql);
								if ($submit) {
									$app->query($sql);
								}
								echo '<br>';
								echo '</td>';
								echo '</tr>';
								break;
							case 5: 
								//Izin Tanda Daftar Perusahaan  (TDP)
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$no = substr($nolengkap,-5);
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								if (isset($arr['tdp'][$v2['trans_izin.id_transizin']])) {
									$kode = '';
									
									//TODO: m_kbli atau m_tree => m_kbli2? bedanya adalah pada m_kbli, deleted=0, sementara pada m_kbli semuanya
									if (isset($arr['m_kbli2'][$arr['tdp'][$v2['trans_izin.id_transizin']]->id_tree])) {
										$kode = $arr['m_kbli2'][$arr['tdp'][$v2['trans_izin.id_transizin']]->id_tree]->kodefull;
									} else {
										echo '<p style="color:#FF0000;"><b>Notice</b>: id_tree '.$arr['tdp'][$v2['trans_izin.id_transizin']]->id_tree.' tidak ditemukan di tabel m_kbli (m_kbli2 ?), sehingga Kode tidak dapat dicari di tabel kbli*</p>';
									}
									
									//HACK: untuk yang tidak ada di kblikategori (tidak tercantum di kbli 2009 cetakan 3 dan kbli 2005 cetakan 3)
									//		dicari yang sama
									if ($kode == 56140) {
										//Kedai Makanan Dan Minuman
										$kode = 55240;
									}
									if ($kode == 56160) {
										//Jasa Boga (catering)
										$kode = 55260;
									}
									if ($kode == 56114) {
										//Restoran/ Rumah Makan Non Talam
										$kode = 55214;
									}
									if ($kode == 50500) {
										//Perdagangan eceran  bahan bakar kendaraan
										$kode = 50400;
									}
									if ($kode == 5614) {
										//Kedai Makanan Dan Minuman
										$kode = 5524;
									}
									if ($kode == 96111) {
										//Jasa Pangkas Rambut
										$kode = 93021;
									}
									if ($kode == 5611) {
										//Rumah Makan
										$kode = 5521;
									}
									if ($kode == 47232) {
										//Perdagangan eceran minuman tidak beralkohol
										$kode = 47222;
									}
									if ($kode == 47774) {
										//Perdagangan eceran tas, dompet, koper, ransel dan sejenisnya
										$kode = 47714;
									}
									if ($kode == 96112) {
										//Jasa Salon Kecantikan
										$kode = 93022;
									}
									if ($kode == 96129) {
										//JASA KEBUGARAN LAINNYA -> Kebugaran / Fitness
										$kode = 92417;
									}
									if ($kode == 9611) {
										//Jasa Salon Kecantikan
										$kode = 9302;
									}
									
									$kluitabel = '';
									$kluikodependek = '';
									$kluiid = 0;
									
									switch (strlen($kode)) {
										case 1:
											$kluitabel = 'kategori';
											$kluikodependek = substr($kode,0,2);
											if (isset($arr['kblikategori'][$kode])) {
												$kluiid = $arr['kblikategori'][$kode];
											} else {
												$notfound[$kode] = $arr['m_kbli2'][$arr['tdp'][$v2['trans_izin.id_transizin']]->id_tree];
										
												echo '<p style="color:#FF0000;"><b>Notice</b>: Kode '.$kode.' tidak ditemukan di tabel kblikategori</p>';
											}
											break;
										case 2:
											$kluitabel = 'golonganpokok';
											$kluikodependek = substr($kode,0,2);
											if (isset($arr['kbligolonganpokok'][$kode])) {
												$kluiid = $arr['kbligolonganpokok'][$kode];
											} else {
												$notfound[$kode] = $arr['m_kbli2'][$arr['tdp'][$v2['trans_izin.id_transizin']]->id_tree];
										
												echo '<p style="color:#FF0000;"><b>Notice</b>: Kode '.$kode.' tidak ditemukan di tabel kbligolonganpokok</p>';
											}
											break;
										case 3:
											$kluitabel = 'golongan';
											$kluikodependek = substr($kode,0,2);
											if (isset($arr['kbligolongan'][$kode])) {
												$kluiid = $arr['kbligolongan'][$kode];
											} else {
												$notfound[$kode] = $arr['m_kbli2'][$arr['tdp'][$v2['trans_izin.id_transizin']]->id_tree];
										
												echo '<p style="color:#FF0000;"><b>Notice</b>: Kode '.$kode.' tidak ditemukan di tabel kbligolongan</p>';
											}
											break;
										case 4:
											$kluitabel = 'subgolongan';
											$kluikodependek = substr($kode,0,2);
											if (isset($arr['kblisubgolongan'][$kode])) {
												$kluiid = $arr['kblisubgolongan'][$kode];
											} else {
												$notfound[$kode] = $arr['m_kbli2'][$arr['tdp'][$v2['trans_izin.id_transizin']]->id_tree];
										
												echo '<p style="color:#FF0000;"><b>Notice</b>: Kode '.$kode.' tidak ditemukan di tabel kblisubgolongan</p>';
											}
											break;
										case 5:
											$kluitabel = 'kelompok';
											$kluikodependek = substr($kode,0,2);
											if (isset($arr['kblikelompok'][$kode])) {
												$kluiid = $arr['kblikelompok'][$kode];
											} else {
												$notfound[$kode] = $arr['m_kbli2'][$arr['tdp'][$v2['trans_izin.id_transizin']]->id_tree];
										
												echo '<p style="color:#FF0000;"><b>Notice</b>: Kode '.$kode.' tidak ditemukan di tabel kblikelompok</p>';
											}
											break;
									}
									
									//kenapa tidak if? pergunakan $tglpengesahan?
									$tglberlaku = $arr['tdp'][$v2['trans_izin.id_transizin']]->transtdp_masabelaku;
									
									//kenapa tidak pergunakan $tglpengesahan?
									if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang > 0) {
										$tgldaftarulang = date('Y-m-d', strtotime($arr['tdp'][$v2['trans_izin.id_transizin']]->transtdp_masabelaku." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang." years"));
									} else {
										$tgldaftarulang = '0000-00-00';
									}
									
									$sql = "INSERT INTO tdp (
												tdpDmhnID,
												tdpNo,
												tdpNoLengkap,
												tdpPendaftaran,
												tdpPembaharuan,
												tdpNoTelp,
												tdpNoFax,
												tdpKegiatanUsahaPokok,
												tdpNPWP,
												tdpBentukBadan,
												tdpStatus,
												tdpKluiTabel,
												tdpKluiKodePendek,
												tdpKluiID,
												tdpKeterangan,
												tdpTglBerlaku,
												tdpTglDaftarUlang,
												tdpPejID,
												tdpTglPengesahan,
												tdpImpor
											) VALUES (
												(SELECT MAX(dmhnID) FROM detailpermohonan),
												'".$no."',
												'".$nolengkap."',
												'".ucwords(strtolower($arr['tdp'][$v2['trans_izin.id_transizin']]->transtdp_jnspendaftaran))."',
												'".$arr['tdp'][$v2['trans_izin.id_transizin']]->transtdp_perpanjang."',
												'".$arr['perusahaan'][$perNama]->perNoTelp."',
												'".$arr['perusahaan'][$perNama]->perNoFax."',
												'".$arr['tdp'][$v2['trans_izin.id_transizin']]->transtdp_bidang."',
												'".$arr['perusahaan'][$perNama]->perNPWP."',
												'".$arr['perusahaan'][$perNama]->perBentukBadan."',
												'".ucwords(strtolower($arr['tdp'][$v2['trans_izin.id_transizin']]->transtdp_status))."',
												'".$kluitabel."',
												'".$kluikodependek."',
												'".$kluiid."',
												'',
												'".$tglberlaku."',
												'".$tgldaftarulang."',
												'".$pejid."',
												'".$tglpengesahan."',
												'".$arr['tdp'][$v2['trans_izin.id_transizin']]->id_transtdp."'
											);";
									$app->debug($sql);
									if ($submit) {
										$app->query($sql);
									}
									echo '<br>';
								} else {
									//echo '<br>Tidak ada';
								}
								echo '</td>';
								echo '</tr>';
								break;
							case 6: 
								//Izin Tanda Daftar Industri (TDI)
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$arr_nolengkap = explode('/',$nolengkap);
								$no = $arr_nolengkap[count($arr_nolengkap) - 1];
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								
								if (isset($arr['kelurahan'][$v2['trans_izin.id_tree']])) {
									$kelurahan_id = $arr['kelurahan'][$v2['trans_izin.id_tree']];
								} else {
									$kelurahan_id = $lokasi;
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku > 0) {
									$tglberlaku = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku." years"));
								} else {
									$tglberlaku = '0000-00-00';
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang > 0) {
									$tgldaftarulang = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang." years"));
								} else {
									$tgldaftarulang = '0000-00-00';
								}
								
								if (isset($arr['tdi'][$v2['trans_izin.id_transizin']])) {
									$sql = "INSERT INTO tdi (
												tdiDmhnID,
												tdiNo,
												tdiNoLengkap,
												tdiNPWP,
												tdiNIPIK,
												tdiKepemilikanPabrik,
												tdiKelurahanLokasiID,
												tdiLuasBangunan,
												tdiLuasTanah,
												tdiTenagaPenggerak,
												tdiKluiID,
												tdiKlasifikasi,
												tdiKomoditiIndustri,
												tdiInventaris,
												tdiKapasitasProduksiPerTahun,
												tdiSatuanKapasitasProduksiPerTahun,
												tdiTenagaKerjaIndonesiaLaki,
												tdiTenagaKerjaIndonesiaPerempuan,
												tdiTenagaKerjaAsingLaki,
												tdiTenagaKerjaAsingPerempuan,
												tdiKeterangan,
												tdiTglBerlaku,
												tdiTglDaftarUlang,
												tdiPejID,
												tdiTglPengesahan,
												tdiImpor
											) VALUES (
												(SELECT MAX(dmhnID) FROM detailpermohonan),
												'".$no."',
												'".$nolengkap."',
												'".$v2['trans_izin.transizin_npwpd']."',
												'04160".substr($v2['trans_daftar.daftar_no'],6,4)."',
												'".$arr['kepemilikanpabrik'][$arr['tdi'][$v2['trans_izin.id_transizin']]->id_master]."',
												'".$kelurahan_id."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_bagunan."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_tanah."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_penggerak."',
												'".$arr['m_kbli2'][$arr['tdi'][$v2['trans_izin.id_transizin']]->id_tree]->kodefull."',
												'".$arr['tdiklasifikasi'][$arr['tdi'][$v2['trans_izin.id_transizin']]->id_klasifikasi]."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_komoditi."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_investasi."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_kapasitas."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_unit."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_tkil."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_tkip."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_tkal."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->transtdi_tkap."',
												'',
												'".$tglberlaku."',
												'".$tgldaftarulang."',
												'".$pejid."',
												'".$tglpengesahan."',
												'".$arr['tdi'][$v2['trans_izin.id_transizin']]->id_transtdi."'
											);";
								}
								$app->debug($sql);
								if ($submit) {
									$app->query($sql);
								}
								echo '<br>';
								echo '</td>';
								echo '</tr>';
								break;
							case 7: 
								//Izin Usaha Pariwisata
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$arr_nolengkap = explode('/',$nolengkap);
								$no = $arr_nolengkap[count($arr_nolengkap) - 1];
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								
								if (isset($arr['kelurahan'][$v2['trans_izin.id_tree']])) {
									$kelurahan_id = $arr['kelurahan'][$v2['trans_izin.id_tree']];
								} else {
									$kelurahan_id = $lokasi;
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku > 0) {
									$tglberlaku = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku." years"));
								} else {
									$tglberlaku = '0000-00-00';
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang > 0) {
									$tgldaftarulang = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang." years"));
								} else {
									$tgldaftarulang = '0000-00-00';
								}
								
								$sql = "INSERT INTO pariwisata (
											pariwisataDmhnID,
											pariwisataNo,
											pariwisataNoLengkap,
											pariwisataNPWPD,
											pariwisataJparID,
											pariwisataBushID,
											pariwisataMerekUsahaPerusahaan,
											pariwisataLuasUsahaPanjang,
											pariwisataLuasUsahaLebar,
											pariwisataLokasiUsaha,
											pariwisataKelurahanID,
											pariwisataKeterangan,
											pariwisataTglBerlaku,
											pariwisataTglDaftarUlang,
											pariwisataPejID,
											pariwisataTglPengesahan,
											pariwisataImpor
										) VALUES (
											(SELECT MAX(dmhnID) FROM detailpermohonan),
											'".$no."',
											'".$nolengkap."',
											'".$v2['trans_izin.transizin_npwpd']."',
											'".$arr['jenispariwisata'][$v2['trans_izin.id_master']]."',
											'".$arr['bidangusaha'][strtolower($v2['trans_izin.transizin_jnsusaha'])]."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_merkusaha'])."',
											'".$v2['trans_izin.transizin_panjang']."',
											'".$v2['trans_izin.transizin_lebar']."',
											'".mysql_real_escape_string($v2['trans_izin.transizin_lokasi'])."',
											'".$kelurahan_id."',
											'',
											'".$tglberlaku."',
											'".$tgldaftarulang."',
											'".$pejid."',
											'".$tglpengesahan."',
											1
										);";
								$app->debug($sql);
								if ($submit) {
									$app->query($sql);
								}
								echo '<br>';
								echo '</td>';
								echo '</tr>';
								break;
							case 11: 
								//Izin Mendirikan Bangunan (IMB)
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$arr_nolengkap = explode('/',$nolengkap);
								$no = $arr_nolengkap[count($arr_nolengkap) - 1];
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								
								if (isset($arr['imb'][$v2['trans_izin.id_transizin']])) {
									if (isset($arr['kelurahan'][$v2['trans_izin.id_tree']])) {
										$kelurahan_id = $arr['kelurahan'][$v2['trans_izin.id_tree']];
									} else {
										$kelurahan_id = $lokasi;
									}
									
									if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku > 0) {
										$tglberlaku = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku." years"));
									} else {
										$tglberlaku = '0000-00-00';
									}
									
									if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang > 0) {
										$tgldaftarulang = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang." years"));
									} else {
										$tgldaftarulang = '0000-00-00';
									}
									
									$sql = "INSERT INTO imb (
												imbDmhnID,
												imbNo,
												imbNoLengkap,
												imbNoSertifikat,
												imbTglSertifikat,
												imbHakTanah,
												imbAtasNama,
												imbJabatan,
												imbUntukMembangun,
												imbRab,
												imbNoBAP,
												imbTglBAP,
												imbNoIP,
												imbTglIP,
												imbLokasi,
												imbKelurahanID,
												imbBatasUtara,
												imbBatasSelatan,
												imbBatasBarat,
												imbBatasTimur,
												imbJarakDariJalan,
												imbKonstruksiBangunan,
												imbBahanPondasi,
												imbKerangkaAtasBawah,
												imbBahanLantai,
												imbBahanDinding,
												imbBahanLoteng,
												imbBahanAtap,
												imbLuasBangunan,
												imbJumlahLantai,
												imbPanjangPagar,
												imbJenisPagar,
												imbJumlahUnit,
												imbKeterangan,
												imbTglBerlaku,
												imbTglDaftarUlang,
												imbPejID,
												imbTglPengesahan,
												imbImpor
											) VALUES (
												(SELECT MAX(dmhnID) FROM detailpermohonan),
												'".$no."',
												'".$nolengkap."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_nosertifikat."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_tglsertifikat."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_haktanah."',
												'".mysql_real_escape_string($arr['imb'][$v2['trans_izin.id_transizin']]->transimb_atasnama)."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_jabatan."',
												'',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_rab."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_nobap."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_tglbap."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_noip."',
												'0000-00-00',
												'".$v2['trans_izin.transizin_lokasi']."',
												'".$kelurahan_id."',
												'',
												'',
												'',
												'',
												'',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_konstruksi."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_pondasi."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_kerangka."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_lantai."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_dinding."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_loteng."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_atap."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_luas."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_jumlantai."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_panjangpagar."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_jenispagar."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->transimb_jumlahunit."',
												'',
												'".$tglberlaku."',
												'".$tgldaftarulang."',
												'".$pejid."',
												'".$tglpengesahan."',
												'".$arr['imb'][$v2['trans_izin.id_transizin']]->id_transimb."'
											);";
									$app->debug($sql);
									if ($submit) {
										$app->query($sql);
									}
									echo '<br>';
								} else {
									//echo '<br>Tidak ada';
								}
								echo '</td>';
								echo '</tr>';
								break;
							case 14: 
								//Izin Operasional (SITU)
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$arr_nolengkap = explode('/',$nolengkap);
								$no = $arr_nolengkap[count($arr_nolengkap) - 1];
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku > 0) {
									$tglberlaku = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaBerlaku." years"));
								} else {
									$tglberlaku = '0000-00-00';
								}
								
								if ($arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang > 0) {
									$tgldaftarulang = date('Y-m-d', strtotime(substr($tglpengesahan,0,10)." +".$arr['jenissuratizin'][$v2['trans_daftar.id_izin']]->jsiMasaDaftarUlang." years"));
								} else {
									$tgldaftarulang = '0000-00-00';
								}
								
								$sql = "INSERT INTO situ (
											situDmhnID,
											situNo,
											situNoLengkap,
											situNPWPD,
											situJenisUsaha,
											situBidangUsaha,
											situMerekUsaha,
											situLuasTempatUsaha,
											situStatusTempatUsaha,
											situKeterangan,
											situTglBerlaku,
											situTglDaftarUlang,
											situPejID,
											situTglPengesahan,
											situImpor 
										) VALUES (
											(SELECT MAX(dmhnID) FROM detailpermohonan),
											'".$no."',
											'".$nolengkap."',
											'".$v2['trans_izin.transizin_npwpd']."',
											'".$v2['trans_izin.transizin_jnsusaha']."',
											'',
											'".mysql_real_escape_string($v2['trans_izin.transizin_merkusaha'])."',
											'',
											'".$v2['trans_izin.transizin_tanah']."',
											'',
											'".$tglberlaku."',
											'".$tgldaftarulang."',
											'".$pejid."',
											'".$tglpengesahan."',
											1
										);";
								$app->debug($sql);
								if ($submit) {
									$app->query($sql);
								}
								echo '<br>';
								echo '</td>';
								echo '</tr>';
								break;
							case 16: 
								//Pemecahan IMB (IMB)
								$nolengkap = $v2['trans_izin.transizin_sk'];
								$tglpengesahan = substr($v2['trans_izin.transizin_tgl'],0,10);
								$pejid = 5;
								
								echo '<tr class="odd">';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td style="background-color:#FFFFFF;">&nbsp;</td>';
								echo '<td colspan="28" style="color:#000000;">';
								//<b>'.strtoupper($v2['m_izin.izin_name']).'</b><br>';
								$app->debug($v2);
								
								echo '</td>';
								echo '</tr>';
								break;
							
						}
					}
				}
			}
		}
?>
			</tbody>
			</table>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/impor/js/daftarizin.impor.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function perusahaanImpor($arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=impor&task=perusahaan"><img src="images/icons/calendar.png" border="0" /> Impor Perusahaan</a></h1>
	</div>
	<div class="halamanTengah">
		<p>Ada <?php echo count($arr['data']); ?> perusahaan</p>
		<table class="dataTable">
		<tbody>
<?php 
		//NOTE: mengacu ke readme
		$lokasi = 2306;
		
		if (count($arr['data']) > 0) {
			foreach ($arr['data'] as $k=>$v) {
				echo '<tr>';
				echo '<td colspan="6">';
				
				$perKelurahanID = isset($arr['kelurahan'][$v->id_tree]) ? $arr['kelurahan'][$v->id_tree] : $lokasi;
				
				$sql = "INSERT INTO perusahaan (
							perNama, 
							perAlamat, 
							perKelurahanID, 
							perNoTelp, 
							perNoFax, 
							perJenisUsaha, 
							perMerekUsaha, 
							perNPWP,
							perNamaPemilik,
							perAlamatPemilik,
							perImpor
						) VALUES (
							'".mysql_real_escape_string($k)."',
							'".mysql_real_escape_string($v->transizin_lokasi)."',
							'".$perKelurahanID."',
							'".$v->transizin_telp."',
							'".$v->transizin_fax."',
							'".mysql_real_escape_string($v->transizin_jnsusaha)."',
							'".mysql_real_escape_string($v->transizin_merkusaha)."',
							'".$v->transizin_npwpd."',
							'".mysql_real_escape_string($v->transizin_pngjawab)."',
							'".mysql_real_escape_string($v->transizin_alamatpngjawab)."',
							1
						);";
				$app->debug($sql);
				echo '</td>';
				echo '</tr>';
			}
		}
?>
		</tbody>
		</table>
	</div>
	<div class="halamanBawah"></div>
<?php
	}
	
	static function viewImpor($arr) {
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=impor"><img src="images/icons/calendar.png" border="0" /> Impor</a></h1>
	</div>
	<div class="halamanTengah">
		<ul>
			<li><a href="index2.php?act=impor&task=perusahaan">Perusahaan</a></li>
<?php 
		if (count($arr) > 0) {
			foreach ($arr as $v) {
?>
			<li><a href="index2.php?act=impor&task=daftarizin&id=<?php echo $v->id_izin; ?>">Daftar dan Izin : <?php echo $v->izin_name; ?></a></li>
<?php
			}
		}
?>
		</ul>
		<h2>Editor</h2>
		<ul>
			<li><a href="index2.php?act=impor&task=list&id=2&id2=60">Daftar dan Izin : Izin Gangguan (HO)</a></li>
			<li><a href="index2.php?act=impor&task=list&id=3&id2=72">Daftar dan Izin : Izin Pemasangan Reklame</a></li>
			<li><a href="index2.php?act=impor&task=list&id=4&id2=61">Daftar dan Izin : Surat Izin Usaha Perdagangan (SIUP)</a></li>
			<li><a href="index2.php?act=impor&task=list&id=5&id2=63">Daftar dan Izin : Izin Tanda Daftar Perusahaan (TDP)</a></li>
			<li><a href="index2.php?act=impor&task=list&id=6&id2=62">Daftar dan Izin : Izin Tanda Daftar Industri (TDI)</a></li>
			<li><a href="index2.php?act=impor&task=list&id=7&id2=68">Daftar dan Izin : Izin Usaha Pariwisata</a></li>
			<li><a href="index2.php?act=impor&task=list&id=11&id2=70">Daftar dan Izin : Izin Mendirikan Bangunan (IMB)</a></li>
			<li><a href="index2.php?act=impor&task=list&id=14&id2=59">Daftar dan Izin : Izin Operasional (SITU)</a></li>
		</ul>
	</div>
	<div class="halamanBawah"></div>
<?php
	}
}
?>