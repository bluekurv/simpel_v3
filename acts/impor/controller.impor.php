<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app, $id;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'view.impor.php';

switch ($app->task) {
	case 'daftarizin':
		daftarizinImpor();
		break;
	case 'list':
		listImpor();
		break;
	case 'editperusahaan':
		editperusahaanImpor();
		break;
	case 'saveperusahaan':
		saveperusahaanImpor();
		break;
	case 'perusahaan':
		perusahaanImpor();
		break;
	default:
		viewImpor();
		break;
}

function saveperusahaanImpor() {
	global $app;

	$id = $app->getInt('id'); 	//id izin kppt
	$id2 = $app->getInt('id2');	//jsiID
	$id3 = $app->getInt('id3');	//dmhnID

	$perID = $app->getInt('perID');
	$perKelurahanID = $app->getInt('perKelurahanID'); 
	
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id3."'");

	$app->query("UPDATE permohonan SET mhnPerID='".$perID."' WHERE mhnID='".$objDetailPermohonan->mhnID."'");
	$app->query("UPDATE detailpermohonan SET dmhnPerID='".$perID."' WHERE dmhnID='".$objDetailPermohonan->dmhnID."'");
	
	switch ($id2) {
		case 60:
			$table = 'situho';
			break;
		case 72:
			$table = 'reklame';
			break;
		case 61:
			$table = 'siup';
			break;
		case 63:
			$table = 'tdp';
			break;
		case 62:
			$table = 'tdi';
			break;
		case 68:
			$table = 'pariwisata';
			break;
		case 70:
			$table = 'imb';
			break;
		case 59:
			$table = 'situ';
			break;
		case 79:
			$table = 'pemecahanimb';
			break;
	}
	
	header("Location:index2.php?act=impor&task=list&id={$id}&id2={$id2}#dmhnID{$id3}");
}

function editperusahaanImpor() {
	global $app;

	$id = $app->getInt('id'); 	//id izin kppt
	$id2 = $app->getInt('id2');	//jsiID
	$id3 = $app->getInt('id3');	//dmhnID
	
	$obj = $app->queryObject("SELECT * FROM detailpermohonan, permohonan WHERE dmhnID='".$id3."' AND dmhnMhnID=mhnID");
	if ($obj) {
		$objDaftar 	= $app->queryObject("SELECT * FROM kppt.trans_daftar WHERE id_daftar='".$obj->mhnImpor."'");
		
		$sql = "SELECT m_pemohon.*, m_tree.tree_desc 
				FROM kppt.m_pemohon
				LEFT JOIN kppt.m_tree ON m_pemohon.id_tree=m_tree.id_tree
				WHERE id_pemohon='".$objDaftar->id_pemohon."'";
		$objPemohon = $app->queryObject($sql);
		
		$sql = "SELECT trans_izin.*, m_tree.tree_desc 
				FROM kppt.trans_izin
				LEFT JOIN kppt.m_tree ON trans_izin.id_tree=m_tree.id_tree
				WHERE id_transizin='".$obj->dmhnImpor."'";
		$objIzin 	= $app->queryObject($sql);
		
		switch ($id) {
			case 2:
				break;
			case 3:
				$objSuratIzin = $app->queryObject("SELECT * FROM reklame WHERE reklameDmhnID='".$obj->dmhnID."'");
				$objSuratIzin2 = $app->queryObject("SELECT * FROM kppt.trans_reklame WHERE id_transreklame='".$objSuratIzin->reklameImpor."'");
				break;
			case 4:
				$objSuratIzin = $app->queryObject("SELECT * FROM siup WHERE siupDmhnID='".$obj->dmhnID."'");
				$objSuratIzin2 = $app->queryObject("SELECT * FROM kppt.trans_siup WHERE id_transsiup='".$objSuratIzin->siupImpor."'");
				break;
			case 5:
				break;
			case 6:
				break;
			case 7:
				break;
			case 11:
				break;
			case 14:
				break;
			case 16:
				break;
		}
		
		$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perID='".$obj->mhnPerID."'");
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=impor"><img src="images/icons/rosette.png" border="0" /> Perusahaan</a></h1>
	</div>
	<div class="halamanTengah">
		<table border="1" width="100%">
		<tr>
			<td valign="top" width="50%">
				<form id="myForm" name="myForm" action="index2.php?act=impor&task=saveperusahaan" method="POST">
				<input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
				<input type="hidden" id="id2" name="id2" value="<?php echo $id2; ?>" />
				<input type="hidden" id="id3" name="id3" value="<?php echo $id3; ?>" />
				
				<input type="submit" class="button-link inline dark-blue" value="Simpan" />
				<a class="button-link inline blue" href="index2.php?act=impor&task=list&id=<?php echo $id; ?>&id2=<?php echo $id2; ?>">Batal</a>
				
				<table class="editTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
				<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objPerusahaan->perID; ?>">
						<input class="box" id="perNama" name="perNama" maxlength="500" size="50" value="<?php echo $objPerusahaan->perNama; ?>" required autofocus/>
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama perusahaan untuk menampilkan pilihan yang ada, kemudian pilih perusahaan yang diinginkan atau masukkan nama perusahaan baru</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box readonly" id="perAlamat" name="perAlamat" maxlength="500" size="50" value="<?php echo $objPerusahaan->perAlamat; ?>" readonly/>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
				WHERE w.wilID='".$objPerusahaan->perKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$perKelurahan = $app->queryObject($sql);
		if (!$perKelurahan) {
			$perKelurahan = new stdClass();
			$perKelurahan->kode = '';
			$perKelurahan->value = '';
		}
?>
						<input type="hidden" id="perKelurahanID" name="perKelurahanID" value="<?php echo $objPerusahaan->perKelurahanID; ?>" />
						<input class="box readonly" id="perKelurahanKode" name="perKelurahanKode" size="8" value="<?php echo $perKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box readonly" id="perKelurahan" name="perKelurahan" maxlength="500" size="50" value="<?php echo $perKelurahan->value; ?>" readonly/>
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, pergunakan sub menu yang ada di menu <b>Pelayanan &gt; Lokasi</b> untuk mengelola data lokasi</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box readonly" id="perNoTelp" name="perNoTelp" maxlength="255" size="50" value="<?php echo $objPerusahaan->perNoTelp; ?>" readonly/>
					</td>
				</tr>
				</table>
				</form>
			</td>
			<td valign="top" width="50%">
				<h2>Data KPPT</h2>
<?php 
		show($objIzin, "trans_izin");
		if ($id == 3 && isset($objSuratIzin2)) {
			show($objSuratIzin2, "trans_reklame");
		}
		if ($id == 4 && isset($objSuratIzin2)) {
			show($objSuratIzin2, "trans_siup");
		}
		show($objDaftar, "trans_daftar");
		show($objPemohon, "m_pemohon");
	}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script>
	$('#perNama').autocomplete({
		serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=perusahaan',
		onSelect: function(suggestion){
			$('#perID').val(suggestion.data);
			$('#perNama').val(suggestion.value);
			
			$.getJSON(
				CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=perusahaanid',  
				{ perID: suggestion.data },
				function(json) { 
					if (json.success) {
						var obj = json.obj;
						$('#perAlamat').val(obj.perAlamat);
						$('#perKelurahanID').val(obj.perKelurahanID);
						$('#perKelurahanKode').val(obj.perKelurahanKode);
						$('#perKelurahan').val(obj.perKelurahan);
						$('#perNoTelp').val(obj.perNoTelp);
					}
				}  
			);  
		}
	});

	$('#perKelurahan').autocomplete({
		serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
		onSelect: function(suggestion){
			$('#perKelurahanID').val(suggestion.data.id);
			$('#perKelurahanKode').val(suggestion.data.kode);
			$('#perKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
		}
	});
	</script>
<?php
}

function show($obj, $name) {
?>
	<h3>Tabel <?php echo $name; ?></h3>
	<table class="dataTable">
<?php		
	if ($obj) {
		foreach (get_object_vars($obj) as $k=>$v) {
?>
	<tr>
		<td width="160"><?php echo $k; ?></td>
		<td><?php echo $v; ?></td>
	</tr>
<?php
		}
	} else {
?>
	<tr>
		<td colspan="2">Tidak ditemukan?</td>
	</tr>
<?php 
	}
?>
	</table>
<?php
}

function listImpor() {
	global $app;

	$id = $app->getInt('id');
	$id2 = $app->getInt('id2');
	
	switch ($id2) {
		case 60:
			$table = 'situho';
			break;
		case 72:
			$table = 'reklame';
			break;
		case 61:
			$table = 'siup';
			break;
		case 63:
			$table = 'tdp';
			break;
		case 62:
			$table = 'tdi';
			break;
		case 68:
			$table = 'pariwisata';
			break;
		case 70:
			$table = 'imb';
			break;
		case 59:
			$table = 'situ';
			break;
		case 79:
			$table = 'pemecahanimb';
			break;
	}
	
	$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$id2."'");
	
	$sql = "SELECT *
			FROM {$table}
			LEFT JOIN detailpermohonan ON {$table}DmhnID=dmhnID
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE {$table}Impor>0
			ORDER BY mhnNoUrutLengkap";
	$rs = $app->query($sql);
?>
	<style>
		.masalah {
			background: rgb(255,237,237);
		}
	</style>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=impor"><img src="images/icons/rosette.png" border="0" /> <?php echo $objJenisSuratIzin->jsiNama; ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<table class="dataTable">
			<thead>
			<tr>
				<th width="25">No.</th>
				<th width="70">No. Urut</th>
				<th width="65">Tgl.</th>
				<th width="150">Pemohon</th>
				<th width="150">Perusahaan</th>
				<th>Permasalahan</th>
				<th>Surat Izin</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php 
	$i = 1;
	while(($obj = mysql_fetch_object($rs)) == true){
		$masalah = array();
		if ($obj->mhnKelurahanID == 0) {
			$masalah[] = 'Kelurahan Pemohon belum ditentukan (mhnKelurahanID = 0)';
		}
		if ($obj->mhnPerID == 0) {
			$masalah[] = 'Perusahaan ada (perID = 0)';
		} else if ($obj->perNama == '' || $obj->perNama == '-') {
			$masalah[] = '<a href="index2.php?act=impor&task=editperusahaan&id='.$id.'&id2='.$id2.'&id3='.$obj->dmhnID.'">Nama Perusahaan belum ada (perNama = "")</a>';
		} else if ($obj->perKelurahanID == 0) {
			$masalah[] = 'Kelurahan Perusahaan belum ditentukan (perKelurahanID = 0)';
		}
		if (count($masalah) > 0) {
			$class = 'masalah';
		} else {
			$class = '';
		}
?>
			<tr class="<?php echo $class; ?>">
				<td><?php echo $i; ?></td>
				<td><?php echo $obj->mhnNoUrutLengkap; ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->mhnTgl); ?></td>
				<td><?php echo $obj->mhnNama; ?></td>
				<td><?php echo $obj->perNama; ?></td>
				<td><?php echo implode('<br>', $masalah); ?></td>
				<td><a name="dmhnID<?php echo $obj->dmhnID; ?>" href="index2.php?act=<?php echo $table; ?>&task=edit&id=<?php echo $obj->dmhnID; ?>">Edit</a></td>
				<td><a href="index2.php?act=impor&task=editperusahaan&id=<?php echo $id; ?>&id2=<?php echo $id2; ?>&id3=<?php echo $obj->dmhnID; ?>">Change</a></td>
			</tr>
<?php 
		$i++;
	}
?>
			</tbody>
			</table>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php
}

function perusahaanImpor() {
	global $app;
	
	$arr = array();
	$arr['data'] = array();
	
	$exclude = array('', '-', '--', );
	
	//SUDAH DIIMPOR
	//Query
	/*$sql = "SELECT * FROM trans_izin, trans_daftar 
			WHERE trans_izin.id_daftar=trans_daftar.id_daftar AND trans_daftar.daftar_status='Selesai'
			GROUP BY trans_izin.transizin_merkusaha
			ORDER BY trans_izin.transizin_merkusaha";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		if (!in_array(trim($obj->transizin_merkusaha), $exclude)) {
			$arr['data'][trim($obj->transizin_merkusaha)] = $obj;
		}
	}
	
	//Query
	$sql = "SELECT * FROM trans_izin, trans_daftar 
			WHERE trans_izin.id_daftar=trans_daftar.id_daftar AND trans_daftar.daftar_status='Selesai'
			GROUP BY trans_izin.transizin_namaperusahaan
			ORDER BY trans_izin.transizin_namaperusahaan";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		if (!in_array(trim($obj->transizin_namaperusahaan), $exclude)) {
			$arr['data'][trim($obj->transizin_namaperusahaan)] = $obj;
		}
	}*/
	
	//Query
	$sql = "SELECT * 
			FROM kppt.trans_izin, kppt.trans_daftar, kppt.trans_reklame
			WHERE trans_izin.id_daftar=trans_daftar.id_daftar AND trans_daftar.daftar_status='Selesai' AND trans_izin.id_transizin=trans_reklame.id_transizin
			GROUP BY trans_reklame.transrek_namainstansi
			ORDER BY trans_reklame.transrek_namainstansi";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		if (!in_array(trim($obj->transrek_namainstansi), $exclude)) {
			$arr['data'][trim($obj->transrek_namainstansi)] = $obj;
		}
	}
	
	ksort($arr['data']);
	
	//REGIONAL--------------------------------------------------------------------------
	$arr['regional'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.m_regional";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['regional'][$obj->id_tree] = $obj->Kelurahan.', '.$obj->Kecamatan.', '.$obj->Kabupaten.', '.$obj->Propinsi;
	}
	
	//KELURAHAN--------------------------------------------------------------------------
	$arr['kelurahan'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.kelurahan";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['kelurahan'][$obj->fromid] = $obj->toid;
	}
	
	Impor_View::perusahaanImpor($arr);
}

function daftarizinImpor() {
	global $app;
	
	$id = $app->id;
	
	$arr = array();
	
	//DATA--------------------------------------------------------------------------
	$arr['data'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.m_pemohon
			LEFT JOIN kppt.m_tree ON m_tree.id_tree=m_pemohon.id_tree
			LEFT JOIN kppt.trans_daftar ON trans_daftar.id_pemohon=m_pemohon.id_pemohon
			LEFT JOIN kppt.m_izin ON m_izin.id_izin=trans_daftar.id_izin
			LEFT JOIN kppt.trans_izin ON trans_izin.id_daftar=trans_daftar.id_daftar
			LEFT JOIN kppt.m_tree AS m_tree2 ON m_tree2.id_tree=trans_izin.id_tree
			LEFT JOIN kppt.m_master ON m_master.id_master=trans_izin.id_master
			WHERE trans_daftar.id_pemohon IS NOT NULL AND trans_daftar.daftar_status='Selesai' AND YEAR(trans_daftar.daftar_tgl)<2014 AND trans_daftar.id_izin=".$id."
			"; //LIMIT 0,200
	$rs = $app->query($sql);
	
	//Array
	//If two or more columns of the result have the same field names, the last column will take precedence. To access the other column(s) of the same name, you must use the numeric index of the column or make an alias for the column. For aliased columns, you cannot access the contents with the original column name.
	
	// Fetch the table name and then the field name
	$qualified_names = array();
	for ($i = 0; $i < mysql_num_fields($rs); ++$i) {
		$table = mysql_field_table($rs, $i);
		$field = mysql_field_name($rs, $i);
		array_push($qualified_names, "$table.$field");
	}
	
	while(($obj = mysql_fetch_row($rs)) == true){
		$combined = array_combine($qualified_names, $obj);
		
		$arr['data'][$obj[0]]['pemohon'] = $combined;
		if (!isset($arr[$obj[0]]['daftardanizin'])) {
			$arr['data'][$obj[0]]['daftardanizin'] = array();
		}
		
		$arr['data'][$obj[0]]['daftardanizin'][] = $combined;
		
	}
	
	//REGIONAL--------------------------------------------------------------------------
	$arr['regional'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.m_regional";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['regional'][$obj->id_tree] = $obj->Kelurahan.', '.$obj->Kecamatan.', '.$obj->Kabupaten.', '.$obj->Propinsi;
	}
	
	//KELURAHAN--------------------------------------------------------------------------
	$arr['kelurahan'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.kelurahan";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['kelurahan'][$obj->fromid] = $obj->toid;
	}
	
	//KBLI--------------------------------------------------------------------------
	$arr['kblikategori'] = array();
	$arr['kbligolonganpokok'] = array();
	$arr['kbligolongan'] = array();
	$arr['kblisubgolongan'] = array();
	$arr['kblikelompok'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.kblikategori";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['kblikategori'][$obj->kkatKode] = $obj->kkatID;
	}
	
	//Query
	$sql = "SELECT * FROM kppt.kbligolonganpokok";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['kbligolonganpokok'][$obj->kgpokKode] = $obj->kgpokID;
	}
	
	//Query
	$sql = "SELECT * FROM kppt.kbligolongan";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['kbligolongan'][$obj->kgolKode] = $obj->kgolID;
	}
	
	//Query
	$sql = "SELECT * FROM kppt.kblisubgolongan";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['kblisubgolongan'][$obj->ksubKode] = $obj->ksubID;
	}
	
	//Query
	$sql = "SELECT * FROM kppt.kblikelompok";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['kblikelompok'][$obj->kkelKode] = $obj->kkelID;
	}
	
	//KBLI dan TREE--------------------------------------------------------------------------
	//tabel m_kbli ada field kodefull
	$arr['m_kbli'] = array();
	//tabel m_tree tidak ada field kodefull
	$arr['m_tree'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.m_kbli";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['m_kbli'][$obj->id_tree] = $obj;
	}
	
	//Query
	$sql = "SELECT * FROM kppt.m_kbli2";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['m_kbli2'][$obj->id_tree] = $obj;
	}
	
	//Query
	$sql = "SELECT * FROM kppt.m_tree";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['m_tree'][$obj->id_tree] = $obj;
	}
	
	//PERUSAHAAN--------------------------------------------------------------------------
	$arr['perusahaan'] = array();
	
	//Query
	//Ambil dari lokal
	$sql = "SELECT perID, LOWER(perNama) AS perNama, perNoTelp, perNoFax, perNPWP, perBentukBadan FROM perusahaan";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['perusahaan'][$obj->perNama] = $obj;
	}
	
	$arr['bentukperusahaan'] = array(
		8 => 1,
		74 => 2,
		7 => 3,
		75=> 4,
		76 => 5,
		0 => 6,
		9 => 7
	);
	
	$arr['tdiklasifikasi'] = array(
		71 => 1,
		73 => 2,
		72 => 3
	);
	
	$arr['kepemilikanpabrik'] = array(
		49 => 'Milik Sendiri',
		50 => 'Sewa',
		51 => 'Lainnya'
	);
	
	$arr['jenispariwisata'] = array(
		24 => 1,
		25 => 2,
		26 => 3,
		27 => 4,
		28 => 5,
		0 => 6
	);
	
	$arr['bidangusaha'] = array(
		'perhotelan' => 4,
		'rumah makan' => 1,
		'wisma' => 6
	);
	
	//JENIS SURAT IZIN--------------------------------------------------------------------------
	$jenissuratizin = array();
	
	//Query
	$sql = "SELECT * FROM jenissuratizin";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$jenissuratizin[$obj->jsiID] = $obj;
	}
	
	$arr['jenissuratizin'] = array();
	
	$arr['jenissuratizin'][2]  = $jenissuratizin[60]; //Izin Gangguan (HO)
	$arr['jenissuratizin'][3]  = $jenissuratizin[72]; //Izin Pemasangan Reklame
	$arr['jenissuratizin'][4]  = $jenissuratizin[61]; //Surat Izin Usaha Perdagangan  (SIUP)
	$arr['jenissuratizin'][5]  = $jenissuratizin[63]; //Izin Tanda Daftar Perusahaan  (TDP)
	$arr['jenissuratizin'][6]  = $jenissuratizin[62]; //Izin Tanda Daftar Industri (TDI)
	$arr['jenissuratizin'][7]  = $jenissuratizin[68]; //Izin Usaha Pariwisata
	$arr['jenissuratizin'][11] = $jenissuratizin[70]; //Izin Mendirikan Bangunan (IMB)
	$arr['jenissuratizin'][14] = $jenissuratizin[59]; //Izin Operasional (SITU)
	$arr['jenissuratizin'][16] = $jenissuratizin[79]; //Pemecahan IMB (IMB)
	
	//HO--------------------------------------------------------------------------
	$arr['ho'] = array();
	
	//REKLAME--------------------------------------------------------------------------
	$arr['reklame'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.trans_reklame";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['reklame'][$obj->id_transizin] = $obj;
	}
	
	//SIUP--------------------------------------------------------------------------
	$arr['siup'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.trans_siup ORDER BY id_transsiup";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['siup'][$obj->id_transizin] = $obj;
		$i++;
	}
	
	//TDP--------------------------------------------------------------------------
	$arr['tdp'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.trans_tdp";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['tdp'][$obj->id_transizin] = $obj;
	}
	
	//TDI--------------------------------------------------------------------------
	$arr['tdi'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.trans_tdi";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['tdi'][$obj->id_transizin] = $obj;
	}
	
	//IP--------------------------------------------------------------------------
	$arr['ip'] = array();
	
	//IMB--------------------------------------------------------------------------
	$arr['imb'] = array();
	
	//Query
	$sql = "SELECT * FROM kppt.trans_imb";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['imb'][$obj->id_transizin] = $obj;
	}
	
	//SITU--------------------------------------------------------------------------
	$arr['situ'] = array();
	
	//Pemecahan IMB--------------------------------------------------------------------------
	$arr['pemecahanimb'] = array();
	
	Impor_View::daftarizinImpor($arr);
}

function viewImpor() {
	global $app;
	
	$arr = array();
	
	//Query
	$sql = "SELECT * FROM kppt.m_izin";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr[] = $obj;
	}
	
	Impor_View::viewImpor($arr);
}
?>