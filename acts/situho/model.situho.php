<?php
class SituHO_Model {	
	public $tabel = 'situho';
	public $primaryKey = 'situhoID';
	
	public $situhoID = 0;
	public $situhoDmhnID = 0;
	public $situhoGrupID = 0;
	public $situhoNo = 0;
	public $situhoNoLengkap = '';
	public $situhoNPWPD = '';
	public $situhoJenisUsaha = '';
	public $situhoBidangUsaha = '';
	public $situhoMerekUsaha = '';
	public $situhoAlamat = '';
	public $situhoKelurahanID = 0;
	public $situhoLuasTempatUsaha = '';
	public $situhoStatusTempatUsaha = '';
	public $situhoRekomendasiCamat = '';
	public $situhoBatasUtara = '';
	public $situhoBatasSelatan = '';
	public $situhoBatasBarat = '';
	public $situhoBatasTimur = '';
	public $situhoKeterangan = '';
	public $situhoTglBerlaku = '0000-00-00'; 	
	public $situhoTglDaftarUlang = '0000-00-00';
	public $situhoPejID = 0;
	public $situhoTglPengesahan = '0000-00-00'; 	 	 	 	 	
	public $situhoArsip = '';
}
?>