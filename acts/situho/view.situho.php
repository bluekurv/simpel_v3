<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class SituHO_View {
	static function editSituHO($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=situho&task=edit&id=<?php echo $obj->situhoDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Izin Gangguan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=situho&task=save" method="POST" >
				<input type="hidden" id="situhoID" name="situhoID" value="<?php echo $obj->situhoID; ?>" />
				<input type="hidden" id="situhoDmhnID" name="situhoDmhnID" value="<?php echo $obj->situhoDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->situhoImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('situhoGrupID', $obj->situhoGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=situho&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=situho&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->perAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//HACK
		if ($obj->situhoAlamat == '') {
			$obj->situhoAlamat = $objDetailPermohonan->perAlamat;
		}
?>
						<input class="box" id="situhoAlamat" name="situhoAlamat" maxlength="500" size="100" value="<?php echo $obj->situhoAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//HACK
		if ($obj->situhoKelurahanID == '') {
			$obj->situhoKelurahanID = $objDetailPermohonan->perKelurahanID;
		}
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->situhoKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$situhoKelurahan = $app->queryObject($sql);
		if (!$situhoKelurahan) {
			$situhoKelurahan = new stdClass();
			$situhoKelurahan->kode = '';
			$situhoKelurahan->value = '';
		}
?>
						<input type="hidden" id="situhoKelurahanID" name="situhoKelurahanID" value="<?php echo $obj->situhoKelurahanID; ?>" />
						<input class="box readonly" id="situhoKelurahanKode" name="situhoKelurahanKode" size="8" value="<?php echo $situhoKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="situhoKelurahan" name="situhoKelurahan" maxlength="500" size="70" value="<?php echo $situhoKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, informasikan kepada Administrator untuk menambahkannya</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
 						<!--<select class="box" id="situhoJenisUsaha" name="situhoJenisUsaha">
<?php 
		foreach (array('Perdagangan Barang', 'Perdagangan Jasa', 'Perdagangan Barang dan Jasa') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->situhoJenisUsaha) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>-->
						<input class="box" id="situhoJenisUsaha" name="situhoJenisUsaha" maxlength="255" size="50" value="<?php echo $obj->situhoJenisUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoBidangUsaha" name="situhoBidangUsaha" maxlength="255" size="50" value="<?php echo $obj->situhoBidangUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoMerekUsaha" name="situhoMerekUsaha" maxlength="255" size="50" value="<?php echo $obj->situhoMerekUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoNPWPD" name="situhoNPWPD" maxlength="255" size="50" value="<?php echo $obj->situhoNPWPD; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoLuasTempatUsaha" name="situhoLuasTempatUsaha" maxlength="255" size="50" value="<?php echo $obj->situhoLuasTempatUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Status Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="situhoStatusTempatUsaha" name="situhoStatusTempatUsaha">
<?php 
		foreach (array('Hak Milik', 'Hak Pakai', 'Sewa') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->situhoStatusTempatUsaha) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Rekomendasi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<!-- <input class="box" id="situhoRekomendasiCamat" name="situhoRekomendasiCamat" maxlength="255" size="50" value="<?php echo $obj->situhoRekomendasiCamat; ?>" /> -->
						<textarea class="box" id="situhoRekomendasiCamat" name="situhoRekomendasiCamat" cols="80" rows="5"><?php echo $obj->situhoRekomendasiCamat; ?></textarea>
						<br><i>Setiap rekomendasi ditulis dan diakhiri dengan menekan tombol ENTER</i>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Batas Utara</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoBatasUtara" name="situhoBatasUtara" maxlength="255" size="50" value="<?php echo $obj->situhoBatasUtara; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Batas Selatan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoBatasSelatan" name="situhoBatasSelatan" maxlength="255" size="50" value="<?php echo $obj->situhoBatasSelatan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Batas Barat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoBatasBarat" name="situhoBatasBarat" maxlength="255" size="50" value="<?php echo $obj->situhoBatasBarat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Batas Timur</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoBatasTimur" name="situhoBatasTimur" maxlength="255" size="50" value="<?php echo $obj->situhoBatasTimur; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situhoKeterangan" name="situhoKeterangan" maxlength="255" size="50" value="<?php echo $obj->situhoKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->situhoNo > 0) {
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/HO/<?php echo substr($obj->situhoTglPengesahan,0,4); ?>/</span>
						<input class="box" id="situhoNo" name="situhoNo" maxlength="5" size="5" value="<?php echo $obj->situhoNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="situhoNo" name="situhoNo" value="<?php echo $obj->situhoNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="situhoTglPengesahan" name="situhoTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->situhoTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="situhoPejID" name="situhoPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->situhoPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='situho'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->situhoTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->situhoTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="situhoTglBerlaku" name="situhoTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->situhoTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->situhoTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->situhoTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="situhoTglDaftarUlang" name="situhoTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->situhoTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/situho/js/edit.situho.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printSituHO($obj) {
		global $app;
		
		$objReport 		= new Report('Izin Gangguan', 'SITUHO-'.substr($obj->situhoTglPengesahan,0,4).'-'.$obj->situhoNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->situhoPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>
	
	<h4 align="center">
		KEPUTUSAN KEPALA <?php ECHO strtoupper($objSatuanKerja->skrNama); ?><br>
		NOMOR : <?php echo $obj->situhoNoLengkap; ?><br>
		TENTANG<br>
		IZIN USAHA GANGGUAN<br>
		<br>
		KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?>,
	</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellpadding="0" cellspacing="0">
<?php 
		$situhoRekomendasiCamat = explode("\n",$obj->situhoRekomendasiCamat);
		$situhoRekomendasiCamat2 = array();
		
		if (count($situhoRekomendasiCamat) > 0) {
			foreach ($situhoRekomendasiCamat as $rekomendasi) {
				if (trim($rekomendasi) != '') {
					$situhoRekomendasiCamat2[] = trim($rekomendasi);
				}
			}
		}
		
		
		
		if (count($situhoRekomendasiCamat2) > 0) {
?>
				<tr><td width="5%">1.&nbsp;</td><td width="96%">Surat Permohonan dari Sdr <?php echo $obj->mhnNama; ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> untuk memperoleh izin usaha berdasarkan Undang-undang Gangguan (Hinder Ordonatie).</td></tr>
<?php
			$no = 2;
			
			foreach ($situhoRekomendasiCamat2 as $rekomendasi) {
?>
				<tr><td><?php echo $no; ?>.&nbsp;</td><td><?php echo trim($rekomendasi); ?></td></tr>
<?php
				$no++;
			}
		} else {
?>
				<tr><td>Surat Permohonan dari Sdr <?php echo strtoupper($obj->mhnNama); ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> untuk memperoleh izin usaha berdasarkan Undang-undang Gangguan (Hinder Ordonatie).</td></tr>
<?php
		}
?>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" valign="top" align="justify">
			Bahwa Usaha yang bersangkutan telah memenuhi persyaratan yang telah ditetapkan, maka permohonan dapat disetujui, oleh karena itu perlu ditetapkan dengan suatu Surat Keputusan;
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
<?php 
		echo $konfigurasi['consideringHO'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">PERTAMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" valign="top" align="justify">Memberikan izin usaha berdasarkan Undang-undang Gangguan (Hinder Ordonatie) kepada:</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">
			<table width="100px" height="100px" border="0">
			<tr>
				<td>
					<img src="<?php echo CFG_LIVE_PATH; ?>/images/report/3x4.png" width="93" height="122">
				</td>
			</tr>
			</table>
		</td>
		<td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0">

			<tr>
            	<td align="left" colspan="4"></td>
			</tr>

			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top" width="30%">Nama</td>
            	<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="68%"><?php echo $obj->mhnNama; ?></td>
			</tr>
<?php 
		$diff = $app->_date_diff(strtotime(date('Y-m-d')), strtotime($obj->mhnTglLahir));	
?>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Umur</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $diff->y; ?> Tahun</td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Kewarganegaraan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnKewarganegaraan; ?></td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Pekerjaan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnPekerjaan; ?></td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">No KTP</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnNoKtp; ?></td>
			</tr>
<?php
	$objWilayah = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$obj->mhnKelurahanID."'");
	$objWilayah2 = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objWilayah->wilParentID."'");
?>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Alamat Tempat Tinggal</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnAlamat; ?>, <?php echo $objWilayah->wilTingkat.' '.$objWilayah->wilNama; ?>, <?php echo $objWilayah2->wilTingkat.' '.$objWilayah2->wilNama; ?></td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
			  	<td align="left" valign="top">Jenis Usaha</td>
				<td align="left" valign="top">:</td>
			  	<td align="left" valign="top"><?php echo $obj->situhoJenisUsaha; ?></td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Nomor NPWPD</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->situhoNPWPD; ?></td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Merek/Nama Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->situhoMerekUsaha; ?></td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Alamat Tempat Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->perAlamat; ?></td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Kelurahan/Desa</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->perKelurahan; ?></td>
			</tr>
			<tr>
            	<td align="left" valign="top" width="5%"></td>
				<td align="left" valign="top">Kecamatan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->perKecamatan; ?></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<?php 
		$pdf->writeHTML(ob_get_contents());

		$height=$pdf->getPageHeight() - $objReport->marginTop - $objReport->marginBottom;
		$pdf->writeHTMLCell(0,0,0,$height,'DENGAN KETENTUAN',0,0,false,true,'R');

		ob_clean();
		
		//As requested: 10
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, 10);
		$pdf->setCellHeightRatio(1.10);
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%"></td>
		<td width="75%">
			<u>DENGAN KETENTUAN-KETENTUAN SEBAGAI BERIKUT :</u>
			<ol>
				<li>Memelihara Ketertiban, Keamanan, dan Kebersihan Tempat Usaha di luar maupun di dalam ruangan.</li>
				<li>Menyediakan Alat Pemadam Kebakaran Racun Api yang bekerja baik.</li>
				<li>Tidak dibenarkan berbuat hiruk pikuk terutama malam hari.</li>
				<li>Bersedia menanggulangi pencemaran lingkungan yang disebabkan oleh limbah cair, kering, padat, beku, gas/udara dan suara atas usaha yang dilakukan untuk kepentingan umum.</li>
				<li>Jika Pemerintah mememerlukan dan menghendaki tempat tersebut, maka usaha dan bangunan harus dapat dipindahkan tanpa mendapat ganti rugi.</li>
				<li>Surat Izin Usaha Gangguan/Hinder Ordinatie (HO) berlaku selama masih dalam menjalankan usaha.</li>
				<li>Dalam rangka pengawasan dan pengendalian serta pembinaan, Izin Usaha Gangguan/Hinder Ordonatie (HO) setiap tahunnya membayar Retribusi sebagaimana yang ditentukan.</li>
				<li>Setiap kali pindah alamat/tempat usaha, memperluas usaha baru, harus mengajukan Surat Permohonan Izin Usaha yang baru kepada Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu (<?php echo CFG_COMPANY_SHORT_NAME; ?>) Kabupaten Pelalawan.</li>
				<li>Akan menggantung Surat Izin Usaha Gangguan/Hinder Ordonatie (HO) tersebut pada tempat yang mudah dilihat/dibaca di ruang tempat usaha.</li>
				<li>Akan mematuhi dan mentaati pada ketentuan/Peraturan Perundang-undangan serta petunjuk-petunjuk dari yang berwajib/Pemerintah Daerah Kabupaten Pelalawan sebagaimana yang ditentukan.</li>
				<li>Menyediakan Tong-tong sampah, membuang/memusnahkan ke tempat yang telah ditetapkan oleh Pemerintah Daerah Kabupaten Pelalawan tanpa mencemarkan lingkungan.</li>
				<li>Melaksanakan Pencegahan, upaya-upaya pengendalian pencemaran, menutup perusahaan selama pencemaran lingkungan sampai normal kembali dan melaporkan pencemaran kepada Pejabat Pemerintah yang berwenang.</li>
				<li>Surat Izin Usaha Gangguan/Hinder Ordonatie (HO) tersebut dapat dicabut bilamana ternyata tidak mengindahkan ketentuan diatas dan melaksanakan pendaftaran pada waktu yang telah ditentukan.</li>
			</ol>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEDUA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0">
			<tr>
				<td align="left" valign="top" colspan="3" align="justify">Usaha sebagaimana tersebut pada Diktum Pertama, dengan batas-batas sebagai berikut :</td>
			</tr>
            <tr>
				<td align="left" valign="top">Utara berbatas dengan</td>
                <td align="left" valign="top">: <?php echo $obj->situhoBatasUtara; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Selatan berbatas dengan</td> 
				<td align="left" valign="top">: <?php echo $obj->situhoBatasSelatan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Barat berbatas dengan</td> 
				<td align="left" valign="top">: <?php echo $obj->situhoBatasBarat; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Timur berbatas dengan</td> 
				<td align="left" valign="top">: <?php echo $obj->situhoBatasTimur; ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KETIGA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Izin Usaha berlaku selama Perusahaan masih berjalan/beroperasi dengan ketentuan Daftar Ulang 5 Tahun sekali.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEEMPAT</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Pemegang Izin/Pengusaha tetap melaksanakan pembayaran Retribusi, Pajak-pajak selama pemegang izin tidak mengajukan Permohonan Pemberhentian Usaha.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KELIMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Atas pemberian Izin Usaha ini dikenakan Retribusi Izin Usaha yang tetap dibayar setiap tahun, berdasarkan Peraturan Daerah Kabupaten Pelalawan.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEENAM</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Surat Izin ini dicabut kembali, apabila Pengusaha/Pemegang Izin tidak mentaati ketentuan-ketentuan tersebut di atas dan melanggar Peraturan Perundang-undangan yang berlaku.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KETUJUH</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Keputusan ini mulai berlaku sejak tanggal ditetapkan.</td>
	</tr>
	</table>
	
	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="48%">&nbsp;</td>
		<td width="52%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->situhoTglPengesahan)); ?>
		</td>
	</tr>
	</table>	

	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:8px">
	<tr>
		<td width="50">Tembusan :</td>
		<td>Disampaikan Kepada Yth. :</td>
	</tr>
	<tr>
		<td></td>
		<td>1. Kepala Dinas Pendapatan Daerah <?php echo $objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>2. Camat <?php echo $obj->perKecamatan; ?></td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readSituHO($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=situho&task=edit&id=<?php echo $obj->situhoDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Gangguan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=situho&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=situho&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoJenisUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoBidangUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoMerekUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoNPWPD; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Luas Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoLuasTempatUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Status Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoStatusTempatUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Rekomendasi</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		//echo $obj->situhoRekomendasiCamat; 
		$situhoRekomendasiCamat = explode("\n",$obj->situhoRekomendasiCamat);
		
		if (count($situhoRekomendasiCamat) > 0) {
			foreach ($situhoRekomendasiCamat as $rekomendasi) {
				if (trim($rekomendasi) != '') {
					echo trim($rekomendasi)."<br>";
				}
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Batas Utara</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoBatasUtara; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Batas Selatan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoBatasSelatan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Batas Barat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoBatasBarat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Batas Timur</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoBatasTimur; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situhoNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->situhoTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->situhoPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->situhoTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->situhoTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->situhoTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->situhoTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/situho/js/read.situho.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewSituHO($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=situho"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Gangguan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('situhoNo', 'Nomor Surat', true),
			$app->setHeader('situhoTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('situhoTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('situhoTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=situho&task=read&id='.$v->situhoDmhnID.'&fromact=situho" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->situhoDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=situho&task=read&id='.$v->situhoDmhnID.'&fromact=situho" title="Entri">'.$v->situhoNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->situhoTglPengesahan).'</td>';
				
				if ($v->situhoTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->situhoTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->situhoTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->situhoTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/situho/js/situho.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>