//Variables
var form = $("#myForm");

//Functions
function validateForm() {
	var doSubmit = true;
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

$('#situhoKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#situhoKelurahanID').val(suggestion.data.id);
		$('#situhoKelurahanKode').val(suggestion.data.kode);
		$('#situhoKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

form.submit(submitForm);

//Set focus
$("#situhoJenisUsahaPerusahaan").focus();