<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.situho.php';
require_once 'view.situho.php';

switch ($app->task) {
	case 'add':
		addSituHO($app->id);
		break;
	case 'edit':
		editSituHO($app->id);
		break;
	case 'delete':
		deleteSituHO($app->id);
		break;
	case 'print':
		printSituHO($app->id);
		break;
	case 'read':
		readSituHO($app->id);
		break;
	case 'save':
		saveSituHO();
		break;
	default:
		viewSituHO(true, '');
		break;
}

function addSituHO($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editSituHO($id);
}

function editSituHO($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objSituHO = $app->queryObject("SELECT * FROM situho WHERE situhoDmhnID='".$id."'");
	if (!$objSituHO) {
		$objSituHO = new SituHO_Model();
		
		//Ambil nilai default
		//$objSituHO->situhoNPWPD = $objDetailPermohonan->perNPWPD;
		$objSituHO->situhoJenisUsaha = $objDetailPermohonan->perJenisUsaha;
		$objSituHO->situhoBidangUsaha = $objDetailPermohonan->perBidangUsaha;
		$objSituHO->situhoMerekUsaha = $objDetailPermohonan->perMerekUsaha;
		$objSituHO->situhoLuasTempatUsaha = $objDetailPermohonan->perLuasTempatUsaha;
		$objSituHO->situhoStatusTempatUsaha = $objDetailPermohonan->perStatusTempatUsaha;
	}
	
	if ($objSituHO->situhoNPWPD == "") {
		$objSituHO->situhoNPWPD = $app->queryField("SELECT wprNoPokok FROM wajibpajakretribusi WHERE wprNama='".$objDetailPermohonan->perNama."' AND wprJenis='Pajak'");
	}
	
	$objSituHO->situhoDmhnID = $objDetailPermohonan->dmhnID;
	
	SituHO_View::editSituHO(true, '', $objDetailPermohonan, $objSituHO);
}

function deleteSituHO($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM situho WHERE situhoDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Izin Gangguan (HO) berhasil dihapus';
		
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Izin Gangguan (HO)', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function printSituHO($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN situho ON situhoDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objSituHO = $app->queryObject($sql);
	if (!$objSituHO) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objSituHO->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	//HACK
	if ($objSituHO->situhoAlamat != '') {
		$objSituHO->perAlamat = $objSituHO->situhoAlamat;
	}
	if ($objSituHO->situhoKelurahanID > 0) {
		$objSituHO->perKelurahanID = $objSituHO->situhoKelurahanID;
	}
	
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objSituHO->perKelurahanID."'");
	$objSituHO->perKelurahan = $objKelurahan->wilNama;
	$objSituHO->perKecamatan = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");
		
	SituHo_View::printSituHO($objSituHO);
}

function readSituHO($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN situho ON situhoDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objSituHO = $app->queryObject($sql);
	if (!$objSituHO) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objSituHO->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	SituHo_View::readSituHO($objSituHO);
}

function saveSituHO() {
	global $app;
	
	//Get object
	$objSituHOSebelumnya = $app->queryObject("SELECT * FROM situho WHERE situhoID='".$app->getInt('situhoID')."'");
	if (!$objSituHOSebelumnya) {
		$objSituHOSebelumnya = new SituHO_Model();
	}
	
	//Create object
	$objSituHO = new SituHO_Model();
	$app->bind($objSituHO);
	
	//Modify object (if necessary)
	$objSituHO->situhoTglPengesahan = $app->NormalDateToMySQL($objSituHO->situhoTglPengesahan);
	if ($objSituHO->situhoTglPengesahan != '0000-00-00') {
		if ($objSituHOSebelumnya->situhoNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objSituHO->situhoNo = intval($app->queryField("SELECT MAX(situhoNo) AS nomor FROM situho WHERE YEAR(situhoTglPengesahan)='".substr($objSituHO->situhoTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objSituHO->situhoTglBerlaku = date('Y-m-d', strtotime($objSituHO->situhoTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objSituHO->situhoTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objSituHO->situhoTglDaftarUlang = date('Y-m-d', strtotime($objSituHO->situhoTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objSituHO->situhoTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objSituHO->situhoNo = 0;
		
		$objSituHO->situhoTglBerlaku = '0000-00-00';
		$objSituHO->situhoTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objSituHO->situhoNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/HO/'.substr($objSituHO->situhoTglPengesahan,0,4).'/'.$objSituHO->situhoNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objSituHO->situhoNo > 0){
		$isExist = $app->isExist("SELECT situhoID AS id FROM situho WHERE situhoNo='".$objSituHO->situhoNo."' AND YEAR(situhoTglPengesahan)='".substr($objSituHO->situhoTglPengesahan,0,4)."'", $objSituHO->situhoID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objSituHO->situhoNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objSituHO->situhoID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objSituHO);
		} else {
			$sql = $app->createSQLforUpdate($objSituHO);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objSituHO->situhoID = $app->queryField("SELECT LAST_INSERT_ID() FROM situho");
		}
			
		$success = true;
		$msg = 'Izin Gangguan (HO) berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'situho', $objSituHO->situhoID, $objSituHO->situhoNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objSituHO->situhoDmhnID."'");
		
		$app->query("UPDATE perusahaan 
					 SET 
					 	 perJenisUsaha='".$objSituHO->situhoJenisUsaha."',
					 	 perBidangUsaha='".$objSituHO->situhoBidangUsaha."',
					 	 perMerekUsaha='".$objSituHO->situhoMerekUsaha."',
					 	 perNPWPD='".$objSituHO->situhoNPWPD."',
					 	 perLuasTempatUsaha='".$objSituHO->situhoLuasTempatUsaha."',
					 	 perStatusTempatUsaha='".$objSituHO->situhoStatusTempatUsaha."'
					 WHERE perID='".$app->getInt('perID')."'");
	} else {
		$success = false;
		$msg = 'Izin Gangguan (HO) tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objSituHO->situhoDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		SituHO_View::editSituHO($success, $msg, $objDetailPermohonan, $objSituHO);
	}
}

function viewSituHO($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('situho', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('situho', 'sort', 'situhoTglPengesahan');
	//$dir		= $app->pageVar('situho', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('situho', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('situho', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(situhoNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(situhoTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM situho
				  LEFT JOIN detailpermohonan ON situhoDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM situho
			LEFT JOIN detailpermohonan ON situhoDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(situhoTglPengesahan) DESC, situhoNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	SituHO_View::viewSituHO($success, $msg, $arr);
}
?>