//Variables
var form = $("#myForm");
var kkelKsubID = $("#kkelKsubID");
var kkelKode = $("#kkelKode");
var kkelNama = $("#kkelNama");
var kkelKpubID = $("#kkelKpubID");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'kkelKsubID' || this.id === undefined) {
		if (kkelKsubID.val() == 0){
			doSubmit = false;
			kkelKsubID.addClass("error");		
		} else {
			kkelKsubID.removeClass("error");
		}
	}
	
	if (this.id == 'kkelKode' || this.id === undefined) {
		if (kkelKode.val().length == 0){
			doSubmit = false;
			kkelKode.addClass("error");		
		} else {
			kkelKode.removeClass("error");
		}
	}
	
	if (this.id == 'kkelNama' || this.id === undefined) {
		if (kkelNama.val().length == 0){
			doSubmit = false;
			kkelNama.addClass("error");		
		} else {
			kkelNama.removeClass("error");
		}
	}
	
	if (this.id == 'kkelKpubID' || this.id === undefined) {
		if (kkelKpubID.val() == 0){
			doSubmit = false;
			kkelKpubID.addClass("error");		
		} else {
			kkelKpubID.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
kkelKsubID.blur(validateForm);
kkelKode.blur(validateForm);
kkelNama.blur(validateForm);
kkelKpubID.blur(validateForm);

kkelKsubID.keyup(validateForm);
kkelKode.keyup(validateForm);
kkelNama.keyup(validateForm);
kkelKpubID.keyup(validateForm);

form.submit(submitForm);

//Set focus
kkelKsubID.focus();