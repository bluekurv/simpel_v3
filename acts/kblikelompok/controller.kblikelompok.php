<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kblikelompok.php';
require_once 'view.kblikelompok.php';
require_once CFG_ABSOLUTE_PATH.'/acts/kbli/view.kbli.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKBLIKelompok($app->id);
		break;
	case 'delete':
		deleteKBLIKelompok($app->id);
		break;
	case 'save':
		saveKBLIKelompok();
		break;
	default:
		viewKBLIKelompok(true, '');
		break;
}

function deleteKBLIKelompok($id) {
	global $app;
	
	//Get object
	$objKBLIKelompok = $app->queryObject("SELECT * FROM kblikelompok WHERE kkelID='".$id."'");
	if (!$objKBLIKelompok) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM kblikelompok WHERE kkelID='".$id."'");
		
		$success = true;
		$msg = 'Kelompok Lapangan Usaha "'.$objKBLIKelompok->kkelNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'kblikelompok', $objKBLIKelompok->kkelID, $objKBLIKelompok->kkelNama, $msg);
	} else {
		$success = false;
		$msg = 'Kelompok Lapangan Usaha "'.$objKBLIKelompok->kkelNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKBLIKelompok($success, $msg);
}

function editKBLIKelompok($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKBLIKelompok = $app->queryObject("SELECT * FROM kblikelompok WHERE kkelID='".$id."'");
		if (!$objKBLIKelompok) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKBLIKelompok = new KBLIKelompok_Model();
		
		$objKBLIKelompok->kkelKsubID = $_SESSION['kblikelompok-filterparent'];
		$objKBLIKelompok->kkelKpubID = intval($app->queryField("SELECT ksubKpubID FROM kblisubgolongan WHERE ksubID='".$_SESSION['kblikelompok-filterparent']."'"));
	}
	
	KBLIKelompok_View::editKBLIKelompok(true, '', $objKBLIKelompok);
}

function saveKBLIKelompok() {
	global $app;
	
	//Create object
	$objKBLIKelompok = new KBLIKelompok_Model();
	$app->bind($objKBLIKelompok);
	
	//Modify object (if necessary)
	if (isset($_REQUEST['kkelTampil'])) {
		$objKBLIKelompok->kkelTampil = ($_REQUEST['kkelTampil'] == 'on') ? 1 : 0;
	} else {
		$objKBLIKelompok->kkelTampil = 0;
	}
	//TODO: specific
	//NOTE: karena sudah mysql_escape_string sebelumnya, maka bukan "\r\n" tetapi '\r\n'
	$objKBLIKelompok->kkelNama = str_replace(array('\r\n', '\n', '\r'), ' ', ucwords(strtolower($objKBLIKelompok->kkelNama)));
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKBLIKelompok->kkelKsubID == 0) {
		$doSave = false;
		$msg[] = '- Kategori belum diisi';
	}
	
	if ($objKBLIKelompok->kkelKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKBLIKelompok->kkelNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objKBLIKelompok->kkelKsubID == 0) {
		$doSave = false;
		$msg[] = '- Publikasi belum diisi';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKBLIKelompok->kkelID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKBLIKelompok);
		} else {
			$sql = $app->createSQLforUpdate($objKBLIKelompok);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKBLIKelompok->kkelID = $app->queryField("SELECT LAST_INSERT_ID() FROM kblikelompok");
			
			$success = true;
			$msg = 'Kelompok Lapangan Usaha "'.$objKBLIKelompok->kkelNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'kblikelompok', $objKBLIKelompok->kkelID, $objKBLIKelompok->kkelNama, $msg);
		} else {
			$success = true;
			$msg = 'Kelompok Lapangan Usaha "'.$objKBLIKelompok->kkelNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'kblikelompok', $objKBLIKelompok->kkelID, $objKBLIKelompok->kkelNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Kelompok Lapangan Usaha "'.$objKBLIKelompok->kkelNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKBLIKelompok($success, $msg);
	} else {
		KBLIKelompok_View::editKBLIKelompok($success, $msg, $objKBLIKelompok);
	}
}

function viewKBLIKelompok($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kblikelompok', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kblikelompok', 'sort', 'kkelKode');
	$dir   		= $app->pageVar('kblikelompok', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'parent' => 0
	);
	
	$nama 		= $app->pageVar('kblikelompok', 'filternama', $default['nama'], 'strval');
	$parent		= $app->pageVar('kblikelompok', 'filterparent', $default['parent'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(kkelKode LIKE '%".$nama."%' OR kkelNama LIKE '%".$nama."%')";
	}
	if ($parent != $default['parent']) {
		$filter[] = "kkelKsubID='".$parent."'";
	}
	$filter[] = "kkelKsubID=ksubID AND kkelKpubID=kpubID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'parent' => $parent
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM kblikelompok, kblisubgolongan, kblipublikasi
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM kblikelompok, kblisubgolongan, kblipublikasi
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	KBLIKelompok_View::viewKBLIKelompok($success, $msg, $arr);
}
?>