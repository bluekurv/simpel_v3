<?php
class IzinKerjaBidan_Model {	
	public $tabel = 'izinkerjabidan';
	public $primaryKey = 'ikbID';
	
	public $ikbID = 0;
	public $ikbDmhnID = 0;
	public $ikbNo = 0;
	public $ikbNoLengkap = '';
	
	public $ikbNama = '';
	public $ikbTempatLahir = '';
	public $ikbTglLahir = '0000-00-00';
	public $ikbAlamat = '';
	public $ikbNoSTR = '';
	public $ikbNoIBI = '';
	
	public $ikbTempatPraktekNama = '';
	public $ikbTempatPraktekAlamat = '';
	
	public $ikbKoordinatN = '';
	public $ikbKoordinatNDerajat = 0;
	public $ikbKoordinatNMenit = 0;
	public $ikbKoordinatNDetik = 0;
	public $ikbKoordinatE = '';
	public $ikbKoordinatEDerajat = 0;
	public $ikbKoordinatEMenit = 0;
	public $ikbKoordinatEDetik = 0;
	
	public $ikbTglBerlaku = '0000-00-00';	
	public $ikbTglDaftarUlang = '0000-00-00';
	public $ikbPejID = 0;
	public $ikbTglPengesahan = '0000-00-00'; 
	public $ikbArsip = '';
}
?>