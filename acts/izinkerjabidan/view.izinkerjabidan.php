<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class IzinKerjaBidan_View {
	static function editIzinKerjaBidan($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=izinkerjabidan&task=edit&id=<?php echo $obj->ikbDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Surat Izin Kerja Bidan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=izinkerjabidan&task=save" method="POST" >
				<input type="hidden" id="ikbID" name="ikbID" value="<?php echo $obj->ikbID; ?>" />
				<input type="hidden" id="ikbDmhnID" name="ikbDmhnID" value="<?php echo $obj->ikbDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=izinkerjabidan&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Identitas Bidan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ikbNama" name="ikbNama" maxlength="50" size="50" value="<?php echo $obj->ikbNama; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tempat Lahir</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ikbTempatLahir" name="ikbTempatLahir" maxlength="50" size="50" value="<?php echo $obj->ikbTempatLahir; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Lahir</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="ikbTglLahir" name="ikbTglLahir" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->ikbTglLahir); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ikbAlamat" name="ikbAlamat" maxlength="50" size="50" value="<?php echo $obj->ikbAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. STR</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ikbNoSTR" name="ikbNoSTR" maxlength="50" size="50" value="<?php echo $obj->ikbNoSTR; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. IBI</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ikbNoIBI" name="ikbNoIBI" maxlength="50" size="50" value="<?php echo $obj->ikbNoIBI; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Tempat Praktek</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ikbTempatPraktekNama" name="ikbTempatPraktekNama" maxlength="50" size="50" value="<?php echo $obj->ikbTempatPraktekNama; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="ikbTempatPraktekAlamat" name="ikbTempatPraktekAlamat" maxlength="50" size="50" value="<?php echo $obj->ikbTempatPraktekAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Titik Koordinat</td><td>&nbsp;:&nbsp;</td>
					<td>
						N 
						<input class="box" id="ikbKoordinatNDerajat" name="ikbKoordinatNDerajat" maxlength="10" size="3" value="<?php echo $obj->ikbKoordinatNDerajat; ?>" />&deg;
						<input class="box" id="ikbKoordinatNMenit" name="ikbKoordinatNMenit" maxlength="10" size="3" value="<?php echo $obj->ikbKoordinatNMenit; ?>" />'
						<input class="box" id="ikbKoordinatNDetik" name="ikbKoordinatNDetik" maxlength="10" size="5" value="<?php echo $obj->ikbKoordinatNDetik; ?>" />"
						<br>
						E 
						<input class="box" id="ikbKoordinatEDerajat" name="ikbKoordinatEDerajat" maxlength="10" size="3" value="<?php echo $obj->ikbKoordinatEDerajat; ?>" />&deg;
						<input class="box" id="ikbKoordinatEMenit" name="ikbKoordinatEMenit" maxlength="10" size="3" value="<?php echo $obj->ikbKoordinatEMenit; ?>" />'
						<input class="box" id="ikbKoordinatEDetik" name="ikbKoordinatEDetik" maxlength="10" size="5" value="<?php echo $obj->ikbKoordinatEDetik; ?>" />"
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->ikbNo > 0) {
?>
						<span>KPTS 540/<?php echo CFG_COMPANY_SHORT_NAME; ?>/<?php echo substr($obj->ikbTglPengesahan,0,4); ?>/</span>
						<input class="box" id="ikbNo" name="ikbNo" maxlength="5" size="5" value="<?php echo $obj->ikbNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="ikbNo" name="ikbNo" value="<?php echo $obj->ikbNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="ikbTglPengesahan" name="ikbTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->ikbTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="ikbPejID" name="ikbPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->ikbPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='izinkerjabidan'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->ikbTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->ikbTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="ikbTglBerlaku" name="ikbTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->ikbTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->ikbTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->ikbTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="ikbTglDaftarUlang" name="ikbTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->ikbTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/izinkerjabidan/js/edit.izinkerjabidan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printIzinKerjaBidan($obj) {
		global $app;
		
		$objReport 		= new Report('SIKB', 'IzinKerjaBidan-'.substr($obj->ikbTglPengesahan,0,4).'-'.$obj->ikbNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->ikbPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<p align="center">
		<b><u>SURAT IZIN KERJA BIDAN (SIKB)</u></b>
		<br>
		No. <?php echo $obj->ikbNoLengkap; ?>
	</p>
	
	<p align="justify">Berdasarkan Peraturan Menteri Kesehatan Republik Indonedia Nomor HK.02.02/Menkes/149/II/2010 tentang Izin dan Penyelenggaraan Praktek Bidan, 
	yang bertanda tangan di bawah ini, Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan memberikan Izin Praktek Bidan pada :</p>
	
	<h1 align="center"><u><?php echo $obj->ikbNama; ?></u></h1>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="45%" align="left" valign="top">Tempat / Tgl. Lahir</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="50%" align="left" valign="top"><?php echo $obj->ikbTempatLahir; ?> / <?php echo $app->MySQLDateToIndonesia($obj->ikbTglLahir); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Alamat</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->ikbAlamat; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Nomor Surat Keterangan STR</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->ikbNoSTR; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Nomor Rekomendasi IBI</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->ikbNoIBI; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Untuk Praktek Bidan</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->ikbTempatPraktekNama; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Alamat Tempat Praktek Bidan</td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->ikbTempatPraktekAlamat; ?></td>
	</tr>
	</table>	
	
	<p align="justify">Surat Izin Praktek Bidan (SIKB) berlaku sampai dengan tanggal : <b><?php echo $app->MySQLDateToIndonesia($obj->ikbTglBerlaku); ?></b>. 
	Surat Izin ini harus diperbaharui tiga bulan sebelum masa berakhir surat izin dan membawa SIKB asli.</p>
	
	<br><br>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%" align="center">
			<table border="0">
			<tr>
				<td align="center">
					<table border="0" cellpadding="1" cellspacing="0" width="100%">
					<tr>
						<td width="10%"></td> 
						<td width="35%" align="left">Dikeluarkan di</td><td width="7">:</td>
						<td width="50%" align="left">Pangkalan Kerinci</td>
					</tr>
					<tr>
						<td></td>
						<td align="left">Pada tanggal</td><td>:</td>
						<td align="left"><?php echo $app->MySQLDateToIndonesia($obj->ikbTglPengesahan); ?></td>
					</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<table border="0" cellpadding="1" cellspacing="0" width="100%">
					<tr>
						<td align="center">
<?php 
		if ($obj->ikbTandaTangan == 'atas nama') {		
?>
							<b>a.n. KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
							<br><b><?php echo $objPengesahan->pnJabatan; ?></b>
<?php 
		} else if ($obj->ikbTandaTangan == 'mewakili') {
?>
							<b>KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
							<br><b>Mewakili</b>
<?php 
		} else {
?>
							<b>KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
<?php 
		}
?>
							<br><br><br><br><br>
							<table width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td width="24%"></td>
								<td width="52%" align="center">
									<b><u><?php echo $objPengesahan->pnNama; ?></u></b><br>
									<?php echo $objPengesahan->pgrPangkat; ?><br>
									NIP. <?php echo $objPengesahan->pnNIP; ?>
								</td>
								<td width="24%"></td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	
	<p>
	<small>
	Tembusan :<br>
	1. Dinas Kesehatan Kabupaten Pelalawan<br>
	2. Pengurus Ikatan Bidan Indonesia (IBI)
	</small>
	</p>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readIzinKerjaBidan($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=izinkerjabidan&task=edit&id=<?php echo $obj->ikbDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Pengambilan Izin Kerja Bidan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=izinkerjabidan&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=izinkerjabidan&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->ikbNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->ikbTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->ikbPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->ikbTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->ikbTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->ikbTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->ikbTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/izinkerjabidan/js/read.izinkerjabidan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewIzinKerjaBidan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=izinkerjabidan"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Pengambilan IzinKerjaBidan yang Bersumber dari IzinKerjaBidan Permukaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('bpNo', 'Nomor Surat', true),
			$app->setHeader('bpTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('bpTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('bpTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=izinkerjabidan&task=read&id='.$v->ikbDmhnID.'&fromact=izinkerjabidan" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->ikbDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=izinkerjabidan&task=read&id='.$v->ikbDmhnID.'&fromact=izinkerjabidan" title="Entri">'.$v->ikbNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->ikbTglPengesahan).'</td>';
				
				if ($v->ikbTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->ikbTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->ikbTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->ikbTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/izinkerjabidan/js/izinkerjabidan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>