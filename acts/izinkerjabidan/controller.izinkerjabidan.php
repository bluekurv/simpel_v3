<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.izinkerjabidan.php';
require_once 'view.izinkerjabidan.php';

switch ($app->task) {
	case 'add':
		addIzinKerjaBidan($app->id);
		break;
	case 'edit':
		editIzinKerjaBidan($app->id);
		break;
	case 'delete':
		deleteIzinKerjaBidan($app->id);
		break;
	case 'print':
		printIzinKerjaBidan($app->id);
		break;
	case 'read':
		readIzinKerjaBidan($app->id);
		break;
	case 'save':
		saveIzinKerjaBidan();
		break;
	default:
		viewIzinKerjaBidan(true, '');
		break;
}

function addIzinKerjaBidan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editIzinKerjaBidan($id);
}

function deleteIzinKerjaBidan($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM izinkerjabidan WHERE ikbDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Izin Kerja Bidan berhasil dihapus';
	
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Izin Kerja Bidan', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function editIzinKerjaBidan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objIzinKerjaBidan = $app->queryObject("SELECT * FROM izinkerjabidan WHERE ikbDmhnID='".$id."'");
	if (!$objIzinKerjaBidan) {
		$objIzinKerjaBidan = new IzinKerjaBidan_Model();
	}
	
	$objIzinKerjaBidan->ikbDmhnID = $objDetailPermohonan->dmhnID;
	
	IzinKerjaBidan_View::editIzinKerjaBidan(true, '', $objDetailPermohonan, $objIzinKerjaBidan);
}

function printIzinKerjaBidan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN izinkerjabidan ON ikbDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objIzinKerjaBidan = $app->queryObject($sql);
	if (!$objIzinKerjaBidan) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objIzinKerjaBidan->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	//HACK
	/*if ($objIzinKerjaBidan->ikbAlamat != '') {
		$objIzinKerjaBidan->perAlamat = $objIzinKerjaBidan->ikbAlamat;
	}
	if ($objIzinKerjaBidan->ikbKelurahanID > 0) {
		$objIzinKerjaBidan->perKelurahanID = $objIzinKerjaBidan->ikbKelurahanID;
	}*/
	
	/*$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objIzinKerjaBidan->perKelurahanID."'");
	$objIzinKerjaBidan->perKelurahan = $objKelurahan->wilNama;
	$objIzinKerjaBidan->perKecamatan = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");*/
		
	IzinKerjaBidan_View::printIzinKerjaBidan($objIzinKerjaBidan);
}

function readIzinKerjaBidan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN izinkerjabidan ON ikbDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objIzinKerjaBidan = $app->queryObject($sql);
	if (!$objIzinKerjaBidan) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objIzinKerjaBidan->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	IzinKerjaBidan_View::readIzinKerjaBidan($objIzinKerjaBidan);
}

function saveIzinKerjaBidan() {
	global $app;
	
	//Get object
	$objIzinKerjaBidanSebelumnya = $app->queryObject("SELECT * FROM izinkerjabidan WHERE ikbID='".$app->getInt('ikbID')."'");
	if (!$objIzinKerjaBidanSebelumnya) {
		$objIzinKerjaBidanSebelumnya = new IzinKerjaBidan_Model();
	}
	
	//Create object
	$objIzinKerjaBidan = new IzinKerjaBidan_Model();
	$app->bind($objIzinKerjaBidan);
	
	//Modify object (if necessary)
	$objIzinKerjaBidan->ikbTglLahir = $app->NormalDateToMySQL($objIzinKerjaBidan->ikbTglLahir);
	$objIzinKerjaBidan->ikbTglPengesahan = $app->NormalDateToMySQL($objIzinKerjaBidan->ikbTglPengesahan);
	
	if ($objIzinKerjaBidan->ikbTglPengesahan != '0000-00-00') {
		if ($objIzinKerjaBidanSebelumnya->ikbNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objIzinKerjaBidan->ikbNo = intval($app->queryField("SELECT MAX(ikbNo) AS nomor FROM izinkerjabidan WHERE YEAR(ikbTglPengesahan)='".substr($objIzinKerjaBidan->ikbTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objIzinKerjaBidan->ikbTglBerlaku = date('Y-m-d', strtotime($objIzinKerjaBidan->ikbTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objIzinKerjaBidan->ikbTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objIzinKerjaBidan->ikbTglDaftarUlang = date('Y-m-d', strtotime($objIzinKerjaBidan->ikbTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objIzinKerjaBidan->ikbTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objIzinKerjaBidan->ikbNo = 0;
		
		$objIzinKerjaBidan->ikbTglBerlaku = '0000-00-00';
		$objIzinKerjaBidan->ikbTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objIzinKerjaBidan->ikbNoLengkap = '441/'.CFG_COMPANY_SHORT_NAME.'/'.substr($objIzinKerjaBidan->ikbTglPengesahan,0,4).'/'.$objIzinKerjaBidan->ikbNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objIzinKerjaBidan->ikbNo > 0){
		$isExist = $app->isExist("SELECT ikbID AS id FROM izinkerjabidan WHERE ikbNo='".$objIzinKerjaBidan->ikbNo."' AND YEAR(ikbTglPengesahan)='".substr($objIzinKerjaBidan->ikbTglPengesahan,0,4)."'", $objIzinKerjaBidan->ikbID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objIzinKerjaBidan->ikbNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objIzinKerjaBidan->ikbID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objIzinKerjaBidan);
		} else {
			$sql = $app->createSQLforUpdate($objIzinKerjaBidan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objIzinKerjaBidan->ikbID = $app->queryField("SELECT LAST_INSERT_ID() FROM izinkerjabidan");
		}
		
		$success = true;
		$msg = 'Izin Kerja Bidan berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'ikb', $objIzinKerjaBidan->ikbID, $objIzinKerjaBidan->ikbNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objIzinKerjaBidan->ikbDmhnID."'");
	} else {
		$success = false;
		$msg = 'Izin Kerja Bidan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objIzinKerjaBidan->ikbDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		IzinKerjaBidan_View::editIzinKerjaBidan($success, $msg, $objDetailPermohonan, $objIzinKerjaBidan);
	}
}

function viewIzinKerjaBidan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('ikb', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('ikb', 'sort', 'ikbTglPengesahan');
	//$dir		= $app->pageVar('ikb', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('ikb', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('ikb', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(ikbNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(ikbTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM izinkerjabidan
				  LEFT JOIN detailpermohonan ON ikbDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM izinkerjabidan
			LEFT JOIN detailpermohonan ON ikbDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(ikbTglPengesahan) DESC, ikbNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	IzinKerjaBidan_View::viewIzinKerjaBidan($success, $msg, $arr);
}
?>