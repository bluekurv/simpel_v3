//Variables
var form = $("#myForm");
var rekNomor = $("#rekNomor");
var rekNama = $("#rekNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'rekNomor' || this.id === undefined) {
		if (rekNomor.val().length == 0){
			doSubmit = false;
			rekNomor.addClass("error");		
		} else {
			rekNomor.removeClass("error");
		}
	}
	
	if (this.id == 'rekNama' || this.id === undefined) {
		if (rekNama.val().length == 0){
			doSubmit = false;
			rekNama.addClass("error");		
		} else {
			rekNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
rekNomor.blur(validateForm);
rekNama.blur(validateForm);

rekNomor.keyup(validateForm);
rekNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
rekNomor.focus();