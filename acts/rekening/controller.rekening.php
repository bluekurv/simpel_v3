<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.rekening.php';
require_once 'view.rekening.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editRekening($app->id);
		break;
	case 'delete':
		deleteRekening($app->id);
		break;
	case 'save':
		saveRekening();
		break;
	default:
		viewRekening(true, '');
		break;
}

function deleteRekening($id) {
	global $app;
	
	//Get object
	$objRekening = $app->queryObject("SELECT * FROM rekening WHERE rekID='".$id."'");
	if (!$objRekening) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalTarif = intval($app->queryField("SELECT COUNT(*) AS total FROM tarif WHERE trfRekID='".$id."'"));
	if ($totalTarif > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalTarif.' tarif untuk rekening retribusi tersebut';
	}
	
	$totalJenisSuratIzin = intval($app->queryField("SELECT COUNT(*) AS total FROM jenissuratizin WHERE jsiRekID='".$id."'"));
	if ($totalJenisSuratIzin > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalJenisSuratIzin.' jenis surat izin untuk rekening retribusi tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM rekening WHERE rekID='".$id."'");
		
		$success = true;
		$msg = 'Rekening retribusi "'.$objRekening->rekNomor.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'rekening', $objRekening->rekID, $objRekening->rekNomor, $msg);
	} else {
		$success = false;
		$msg = 'Rekening retribusi "'.$objRekening->rekNomor.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewRekening($success, $msg);
}

function editRekening($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objRekening = $app->queryObject("SELECT * FROM rekening WHERE rekID='".$id."'");
		if (!$objRekening) {
			$app->showPageError();
			exit();
		}
	} else {
		$objRekening = new Rekening_Model();
	}
	
	Rekening_View::editRekening(true, '', $objRekening);
}

function saveRekening() {
	global $app;
	
	//Create object
	$objRekening = new Rekening_Model();
	$app->bind($objRekening);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objRekening->rekNomor == '') {
		$doSave = false;
		$msg[] = '- Nomor belum diisi';
	}
	
	if ($objRekening->rekNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objRekening->rekNomor != '0.0.0.00.00') {
		$isExist = $app->isExist("SELECT rekID AS id FROM rekening WHERE rekNomor='".$objRekening->rekNomor."'", $objRekening->rekID);
		if (!$isExist) {
			$doSave = false;
			$msg[]  = '- Nomor "'.$objRekening->rekNama.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objRekening->rekID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objRekening);
		} else {
			$sql = $app->createSQLforUpdate($objRekening);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objRekening->rekID = $app->queryField("SELECT LAST_INSERT_ID() FROM rekening");
			
			$success = true;
			$msg = 'Rekening retribusi "'.$objRekening->rekNomor.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'rekening', $objRekening->rekID, $objRekening->rekNomor, $msg);
		} else {
			$success = true;
			$msg = 'Rekening retribusi "'.$objRekening->rekNomor.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'rekening', $objRekening->rekID, $objRekening->rekNomor, $msg);
		}
	} else {
		$success = false;
		$msg = 'Rekening "'.$objRekening->rekNomor.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewRekening($success, $msg);
	} else {
		Rekening_View::editRekening($success, $msg, $objRekening);
	}
}

function viewRekening($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('rekening', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('rekening', 'sort', 'rekNomor');
	$dir   		= $app->pageVar('rekening', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('rekening', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "rekNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM rekening
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *, (SELECT COUNT(*) AS total FROM tarif WHERE trfRekID=rekID) AS total_tarif
			FROM rekening
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Rekening_View::viewRekening($success, $msg, $arr);
}
?>