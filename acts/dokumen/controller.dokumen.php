<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app, $id;

//Implementing access control
$acl = array();
$acl['Petugas Administrasi']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.dokumen.php';
require_once 'view.dokumen.php';

switch ($app->task) {
	case 'delete':
		deleteDokumen($app->id);
		break;
	case 'save':
		saveDokumen($app->id);
		break;
	default:
		viewDokumen(true, '', $app->id);
		break;
}

function deleteDokumen($id) {
	global $app;
	
	//Get object
	$objDokumen = $app->queryObject("SELECT * FROM dokumen WHERE dokID='".$id."'");
	if (!$objDokumen) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM dokumen WHERE dokID='".$id."'");
		
		$uploadFolder = CFG_UPLOAD_PATH.'/files/'.md5('f'.$objDokumen->dokID);
		unlink($uploadFolder.'/'.$objDokumen->dokNamaFile.'.'.$objDokumen->dokEkstensiFile);
		unlink($uploadFolder.'/index.html');
		
		rmdir($uploadFolder);
		
		$success = true;
		$msg = 'Dokumen "'.$objDokumen->dokNamaFile.'.'.$objDokumen->dokEkstensiFile.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'dokumen', $objDokumen->dokID, $objDokumen->dokNamaFile.'.'.$objDokumen->dokEkstensiFile, $msg);
	} else {
		$success = false;
		$msg = 'Dokumen "'.$objDokumen->dokNamaFile.'.'.$objDokumen->dokEkstensiFile.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewDokumen($success, $msg, $objDokumen->dokDmhnID);
}

function saveDokumen($id) {
	global $app;
	
	//Get object
	$sql = "SELECT *
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	//Create object
	$objDokumen = new Dokumen_Model();
	$app->bind($objDokumen);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//File
	$pathparts = pathinfo($_FILES['file']['name']);
	$fileName = $pathparts['filename'];
	
	if ($fileName != '' && isset($pathparts['extension'])) {
		if (!preg_match('/^[A-Za-z0-9_-]+$/', $fileName)) {
			$doSave = false;
			$msg[] = '- Nama file berisikan karakter invalid';
		}
		
		$extension = strtolower($pathparts['extension']);
		if (in_array($extension, array('exe'))) {
			$doSave = false;
			$msg[] = "- Ekstensi file tidak diperbolehkan";
		}
		
		$tmpSize = filesize($_FILES['file']['tmp_name']);
		if ($tmpSize <= 0 || $tmpSize > CFG_UPLOAD_MAX_SIZE * 1024 * 1024) {
			$doSave = false;
			$msg[] = "- Ukuran file melebihi limit (".CFG_UPLOAD_MAX_SIZE." MB)";
		}
	} else {
		$doSave = false;
		$msg[] = "- Tidak ada file yang di-upload";
	}
	
	//Query
	if ($doSave) {
		$objDokumen->dokDmhnID = $id;
		$objDokumen->dokNamaFile = $fileName;
		$objDokumen->dokEkstensiFile = $extension;
		$objDokumen->dokUkuranFile = $tmpSize;
		$objDokumen->dokDiuploadOleh = $_SESSION['sesipengguna']->ID;
		$objDokumen->dokDiuploadPada = date('Y-m-d H:i:s');
		
		$sql = $app->createSQLforInsert($objDokumen);
		$app->query($sql);
		
		$objDokumen->dokID = $app->queryField("SELECT LAST_INSERT_ID() FROM dokumen");
		
		$uploadFolder = CFG_UPLOAD_PATH.'/files/'.md5('f'.$objDokumen->dokID);
		if (!file_exists($uploadFolder)){
			mkdir($uploadFolder);
			copy(CFG_UPLOAD_PATH.'/files/index.html', $uploadFolder.'/index.html');
		}
		
		copy($_FILES['file']['tmp_name'], $uploadFolder.'/'.$objDokumen->dokNamaFile.'.'.$objDokumen->dokEkstensiFile);
		
		$success = true;
		$msg = 'Dokumen "'.$objDokumen->dokNamaFile.'.'.$objDokumen->dokEkstensiFile.'" berhasil di-upload';
		
		$app->writeLog(EV_INFORMASI, 'dokumen', $objDokumen->dokID, $objDokumen->dokNamaFile.'.'.$objDokumen->dokEkstensiFile, $objDokumen->dokNamaFile.'.'.$objDokumen->dokEkstensiFile.'" berhasil di-upload');
	} else {
		$success = false;
		$msg = 'Dokumen tidak berhasil di-upload karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewDokumen($success, $msg, $objDokumen->dokDmhnID);
}

function viewDokumen($success, $msg, $id) {
	global $app;
	
	//Get object
	$sql = "SELECT *
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$arr = array();
	$arr['data'] = array();
	
	//Query
	$sql = "SELECT *
			FROM dokumen
			WHERE dokDmhnID='".$id."'
			ORDER BY dokNamaFile";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	Dokumen_View::viewDokumen($success, $msg, $arr, $objDetailPermohonan);
}
?>