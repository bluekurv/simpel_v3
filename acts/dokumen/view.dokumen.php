<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Dokumen_View {
	static function viewDokumen($success, $msg, $arr, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=dokumen&id=<?php echo $obj->dmhnID; ?>"><img src="images/icons/book.png" border="0" />  Dokumen untuk <?php echo $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : ''); ?> pada No. Pendaftaran <?php echo $obj->mhnNoUrutLengkap; ?>, tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl);  ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=dokumen&task=save" method="POST" enctype="multipart/form-data">
				<input type="hidden" id="id" name="id" value="<?php echo $obj->dmhnID; ?>" />
				<div id="toolbarEntri">
					<input class="box" type="file" id="file" name="file" value="" style="width:400px;">
					<input type="submit" class="button-link inline dark-blue" value="Upload" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Kembali</a>
				</div>
			</form>
<?php
		$app->showMessage($success, $msg);
?>
			<br>
			<table class="dataTable">
			<thead>
			<tr>
				<th width="20">Aksi</th>
				<th>Nama</th>
				<th>File</th>
				<th width="100">Download</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="javascript:hapus('.$v->dokID.', '."'".$v->dokNamaFile.'.'.$v->dokEkstensiFile."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td>'.$v->dokNama.'</td>';
				echo '<td>'.$v->dokNamaFile.'.'.$v->dokEkstensiFile.'</td>';
				echo '<td><a href="'.CFG_LIVE_PATH.'/files/'.md5('f'.$v->dokID).'/'.$v->dokNamaFile.'.'.$v->dokEkstensiFile.'" download>download</a></td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="4">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>		
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/dokumen/js/dokumen.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>