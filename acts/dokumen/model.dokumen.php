<?php
class Dokumen_Model {	
	public $tabel = 'dokumen';
	public $primaryKey = 'dokID';
	
	public $dokID = 0;
	public $dokDmhnID = 0;
	public $dokNama = '';
	public $dokNamaFile = '';
	public $dokEkstensiFile = '';
	public $dokUkuranFile = 0;
	public $dokDiuploadOleh = 0;
	public $dokDiuploadPada = '0000-00-00 00:00:00';
}
?>