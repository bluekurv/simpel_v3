<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.subbidang.php';
require_once 'view.subbidang.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editSubBidang($app->id);
		break;
	case 'delete':
		deleteSubBidang($app->id);
		break;
	case 'save':
		saveSubBidang();
		break;
	default:
		viewSubBidang(true, '');
		break;
}

function deleteSubBidang($id) {
	global $app;
	
	//Get object
	$objSubBidang = $app->queryObject("SELECT * FROM subbidang WHERE subID='".$id."'");
	if (!$objSubBidang) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalPengguna = intval($app->queryField("SELECT COUNT(*) AS total FROM pengguna WHERE pnOrganisasi='Sub Bidang' AND pnOrganisasi='".$id."'"));
	if ($totalPengguna > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalPengguna.' pengguna pada sub bidang tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM subbidang WHERE subID='".$id."'");
		
		$success = true;
		$msg = 'Sub Bidang "'.$objSubBidang->subNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'subbidang', $objSubBidang->subID, $objSubBidang->subNama, $msg);
	} else {
		$success = false;
		$msg = 'Sub Bidang "'.$objSubBidang->subNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewSubBidang($success, $msg);
}

function editSubBidang($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objSubBidang = $app->queryObject("SELECT * FROM subbidang WHERE subID='".$id."'");
		if (!$objSubBidang) {
			$app->showPageError();
			exit();
		}
	} else {
		$objSubBidang = new SubBidang_Model();
	}
	
	SubBidang_View::editSubBidang(true, '', $objSubBidang);
}

function saveSubBidang() {
	global $app;
	
	//Create object
	$objSubBidang = new SubBidang_Model();
	$app->bind($objSubBidang);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objSubBidang->subBidID == 0) {
		$doSave = false;
		$msg[] = '- Berada di belum diisi';
	}
	
	if ($objSubBidang->subNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT subID AS id FROM subbidang WHERE subNama='".$objSubBidang->subNama."'", $objSubBidang->subID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objSubBidang->subNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objSubBidang->subID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objSubBidang);
		} else {
			$sql = $app->createSQLforUpdate($objSubBidang);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objSubBidang->subID = $app->queryField("SELECT LAST_INSERT_ID() FROM subbidang");
			
			$success = true;
			$msg = 'Sub Bidang "'.$objSubBidang->subNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'subbidang', $objSubBidang->subID, $objSubBidang->subNama, $msg);
		} else {
			$success = true;
			$msg = 'Sub Bidang "'.$objSubBidang->subNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'subbidang', $objSubBidang->subID, $objSubBidang->subNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Sub Bidang "'.$objSubBidang->subNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewSubBidang($success, $msg);
	} else {
		SubBidang_View::editSubBidang($success, $msg, $objSubBidang);
	}
}

function viewSubBidang($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('subbidang', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('subbidang', 'sort', 'subNama');
	$dir   		= $app->pageVar('subbidang', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('subbidang', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "subNama LIKE '%".$nama."%'";
	}
	$filter[] = "subBidID=bidID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM subbidang, bidang
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM subbidang, bidang
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	SubBidang_View::viewSubBidang($success, $msg, $arr);
}
?>