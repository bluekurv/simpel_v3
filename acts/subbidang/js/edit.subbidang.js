//Variables
var form = $("#myForm");
var subNama = $("#subNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'subNama' || this.id === undefined) {
		if (subNama.val().length == 0){
			doSubmit = false;
			subNama.addClass("error");		
		} else {
			subNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
subNama.blur(validateForm);

subNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
subNama.focus();