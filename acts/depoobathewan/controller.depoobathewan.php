<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'depoobathewan';
$childTitle = 'Izin Operasional Depo Obat Hewan (Obat Unggas dan Vaksin)';
$childKodeSurat = 'IOP-DOH';
$childTembusan = 'Kepala Dinas Peternakan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>