<?php
class DetailBudidayaPerkebunan_Model {	
	public $tabel = 'detailbudidayaperkebunan';
	public $primaryKey = 'dbpID';
	
	public $dbpID = 0;
	public $dbpBpID = 0;
	public $dbpNo = 0;
	
	public $dbpKoordinat = '';
	/*public $dbpKoordinatN = '';
	public $dbpKoordinatNDerajat = '';
	public $dbpKoordinatNMenit = '';
	public $dbpKoordinatNDetik = '';
	public $dbpKoordinatNDerajat2 = '';
	public $dbpKoordinatNMenit2 = '';
	public $dbpKoordinatNDetik2 = '';
	public $dbpKoordinatE = '';
	public $dbpKoordinatEDerajat = '';
	public $dbpKoordinatEMenit = '';
	public $dbpKoordinatEDetik = '';
	public $dbpKoordinatEDerajat2 = '';
	public $dbpKoordinatEMenit2 = '';
	public $dbpKoordinatEDetik2 = '';*/
	
	public $dbpKelurahanID = 0;
	public $dbpStatusLahan = '';
	public $dbpNoLahan = '';
	public $dbpTglLahan = '0000-00-00';
	public $dbpLuasAreal = '';
	public $dbpJenisTanaman = '';
	public $dbpProduksiPerHaPerTahun = '';
	public $dbpAsalBenih = '';
	public $dbpJumlahPohon = '';
	public $dbpPolaTanam = '';
	public $dbpJenisPupuk = '';
	public $dbpMitraPengolahan = '';
	public $dbpJenisTanah = '';
	public $dbpTahunTanam = '';
	public $dbpUsahaLain = '';
}
?>