//Variables
var form = $("#myForm");

//Functions
function validateForm() {
	var doSubmit = true;
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

form.submit(submitForm);

//Set focus
$("#dbpKoordinatNDerajat").focus();