//Functions
function hapus(id, nama) {
	if (confirm("Hapus '" + nama + "'?")) {
		window.open('index2.php?act=detailbudidayaperkebunan&task=delete&id=' + id, '_self');
	}
}

function lihat(page) {
	var url = 'index2.php?act=detailbudidayaperkebunan&page=' + page;
	
	window.open(url, '_self');
}

function sort(field, direction) {
	var url = 'index2.php?act=detailbudidayaperkebunan';
	url += '&sort=' + field;
	url += '&dir=' + direction;
	
	window.open(url, '_self');
}

//Events
$("#page").change(function() {
	lihat(this.value);
});

//Set focus
$("#btnTambah").focus();