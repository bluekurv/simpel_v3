<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.detailbudidayaperkebunan.php';
require_once 'view.detailbudidayaperkebunan.php';

switch ($app->task) {
	case 'add':
		addDetailBudidayaPerkebunan($app->id);
		break;
	case 'edit':
		editDetailBudidayaPerkebunan($app->id);
		break;
	case 'delete':
		deleteDetailBudidayaPerkebunan($app->id);
		break;
	case 'save':
		saveDetailBudidayaPerkebunan();
		break;
	default:
		viewDetailBudidayaPerkebunan(true, '');
		break;
}

function addDetailBudidayaPerkebunan($id) {
	global $app;
	
	//Query
	$obj = new DetailBudidayaPerkebunan_Model();
	$obj->dbpBpID = $id;
	$obj->dbpNo = intval($app->queryField("SELECT MAX(dbpNo) FROM detailbudidayaperkebunan WHERE dbpBpID='".$id."'")) + 1;
	
	DetailBudidayaPerkebunan_View::editDetailBudidayaPerkebunan(true, '', $obj);
}

function deleteDetailBudidayaPerkebunan($id) {
	global $app;
	
	//Get object
	$obj = $app->queryObject("SELECT * FROM detailbudidayaperkebunan WHERE dbpID='".$id."'");
	if (!$obj) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailbudidayaperkebunan WHERE dbpID='".$id."'");
	
	$success = true;
	$msg = 'Detail Budidaya Perkebunan berhasil dihapus';
	
	$app->writeLog(EV_INFORMASI, 'detailbudidayaperkebunan', $obj->dbpID, 'Detail Budidaya Perkebunan', $msg);
	
	header('Location:index2.php?act=detailbudidayaperkebunan&id='.$obj->dbpBpID.'&success='.$success.'&msg='.$msg);
}

function editDetailBudidayaPerkebunan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * FROM detailbudidayaperkebunan WHERE dbpID='".$id."'";
	$obj = $app->queryObject($sql);
	if (!$obj) {
		$app->showPageError();
		exit();
	}
	
	DetailBudidayaPerkebunan_View::editDetailBudidayaPerkebunan(true, '', $obj);
}

function debug($v) {
	echo '<pre>';
	print_r($v);
	echo '</pre>';
}

function debux($v) {
	debug($v);
	exit();
}

function saveDetailBudidayaPerkebunan() {
	global $app;

	//Create object
	$objDetailBudidayaPerkebunan = new DetailBudidayaPerkebunan_Model();
	$app->bind($objDetailBudidayaPerkebunan);
	
	//Modify object (if necessary)
	$objDetailBudidayaPerkebunan->dbpTglLahan = $app->NormalDateToMySQL($objDetailBudidayaPerkebunan->dbpTglLahan);
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objDetailBudidayaPerkebunan->dbpID == 0) ? true : false;
		
		$arr = array();
		if (isset($_REQUEST['dbpKoordinatNDerajat'])) {
			foreach ($_REQUEST['dbpKoordinatNDerajat'] as $k=>$v) {
				$dbpKoordinatNDerajat = $_REQUEST['dbpKoordinatNDerajat'][$k];
				$dbpKoordinatNMenit = $_REQUEST['dbpKoordinatNMenit'][$k];
				$dbpKoordinatNDetik = $_REQUEST['dbpKoordinatNDetik'][$k];
				$dbpKoordinatEDerajat = $_REQUEST['dbpKoordinatEDerajat'][$k];
				$dbpKoordinatEMenit = $_REQUEST['dbpKoordinatEMenit'][$k];
				$dbpKoordinatEDetik = $_REQUEST['dbpKoordinatEDetik'][$k];
		
				$arr[] = $dbpKoordinatNDerajat.'|'.$dbpKoordinatNMenit.'|'.$dbpKoordinatNDetik.'|'.$dbpKoordinatEDerajat.'|'.$dbpKoordinatEMenit.'|'.$dbpKoordinatEDetik;
			}
		}
			
		$objDetailBudidayaPerkebunan->dbpKoordinat = "[".implode("][", $arr)."]";
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objDetailBudidayaPerkebunan);
		} else {
			$sql = $app->createSQLforUpdate($objDetailBudidayaPerkebunan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objDetailBudidayaPerkebunan->dbpID = $app->queryField("SELECT LAST_INSERT_ID() FROM detailbudidayaperkebunan");
		}
		
		$success = true;
		$msg = 'Detail Budidaya Perkebunan berhasil disimpan';
		
		$app->writeLog(EV_INFORMASI, 'dbp', $objDetailBudidayaPerkebunan->dbpID, $objDetailBudidayaPerkebunan->dbpNoLahan, $msg);
	} else {
		$success = false;
		$msg = 'Detail Budidaya Perkebunan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=detailbudidayaperkebunan&id='.$objDetailBudidayaPerkebunan->dbpBpID.'&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * FROM detailbudidayaperkebunan WHERE dbpID='".$objDetailBudidayaPerkebunan->dbpID."'";
		$objDetailBudidayaPerkebunan = $app->queryObject($sql);
		if (!$objDetailBudidayaPerkebunan) {
			$app->showPageError();
			exit();
		}
		
		DetailBudidayaPerkebunan_View::editDetailBudidayaPerkebunan($success, $msg, $objDetailBudidayaPerkebunan);
	}
}

function viewDetailBudidayaPerkebunan($success, $msg) {
	global $app;
	
	$id = $app->getInt('id');
	
	//Get variable(s)
	$page 		= $app->pageVar('dbp', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	
	//Add filter(s) and it's default value
	$filter = array();
	$filter[] = "dbpBpID='".$id."'";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$sort = "dbpNo";
	$dir = "ASC";
	
	$arr = array(
		'data' => array(), 
		'filter' => array(), 
		'default' => array(),
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM detailbudidayaperkebunan
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM detailbudidayaperkebunan
			".$where." 
			ORDER BY ".$sort." ".$dir."
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	DetailBudidayaPerkebunan_View::viewDetailBudidayaPerkebunan($success, $msg, $arr, $id);
}
?>