<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class DetailBudidayaPerkebunan_View {
	static function editDetailBudidayaPerkebunan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=detailbudidayaperkebunan&task=edit&id=<?php echo $obj->dbpID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Surat Tanda Daftar Usaha Budidaya Perkebunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=detailbudidayaperkebunan&task=save" method="POST" >
				<input type="hidden" id="dbpID" name="dbpID" value="<?php echo $obj->dbpID; ?>" />
				<input type="hidden" id="dbpBpID" name="dbpBpID" value="<?php echo $obj->dbpBpID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td colspan="3"><p><b><u>Data Kebun</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Nomor <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						Kebun <input class="box" id="dbpNo" name="dbpNo" maxlength="3" size="3" value="<?php echo $obj->dbpNo; ?>" required autofocus/>
					</td>
				</tr>
				<!-- <tr>
					<td style="padding-left:20px;">Titik Koordinat</td><td>&nbsp;:&nbsp;</td>
					<td>
						N 
						<input class="box" id="dbpKoordinatNDerajat" name="dbpKoordinatNDerajat" maxlength="10" size="3" value="<?php echo $obj->dbpKoordinatNDerajat; ?>" />&deg;
						<input class="box" id="dbpKoordinatNMenit" name="dbpKoordinatNMenit" maxlength="10" size="3" value="<?php echo $obj->dbpKoordinatNMenit; ?>" />'
						<input class="box" id="dbpKoordinatNDetik" name="dbpKoordinatNDetik" maxlength="10" size="5" value="<?php echo $obj->dbpKoordinatNDetik; ?>" />"
						s/d
						<input class="box" id="dbpKoordinatNDerajat2" name="dbpKoordinatNDerajat2" maxlength="10" size="3" value="<?php echo $obj->dbpKoordinatNDerajat2; ?>" />&deg;
						<input class="box" id="dbpKoordinatNMenit2" name="dbpKoordinatNMenit2" maxlength="10" size="3" value="<?php echo $obj->dbpKoordinatNMenit2; ?>" />'
						<input class="box" id="dbpKoordinatNDetik2" name="dbpKoordinatNDetik2" maxlength="10" size="5" value="<?php echo $obj->dbpKoordinatNDetik2; ?>" />"
						<br>
						E 
						<input class="box" id="dbpKoordinatEDerajat" name="dbpKoordinatEDerajat" maxlength="10" size="3" value="<?php echo $obj->dbpKoordinatEDerajat; ?>" />&deg;
						<input class="box" id="dbpKoordinatEMenit" name="dbpKoordinatEMenit" maxlength="10" size="3" value="<?php echo $obj->dbpKoordinatEMenit; ?>" />'
						<input class="box" id="dbpKoordinatEDetik" name="dbpKoordinatEDetik" maxlength="10" size="5" value="<?php echo $obj->dbpKoordinatEDetik; ?>" />"
						s/d
						<input class="box" id="dbpKoordinatEDerajat2" name="dbpKoordinatEDerajat2" maxlength="10" size="3" value="<?php echo $obj->dbpKoordinatEDerajat2; ?>" />&deg;
						<input class="box" id="dbpKoordinatEMenit2" name="dbpKoordinatEMenit2" maxlength="10" size="3" value="<?php echo $obj->dbpKoordinatEMenit2; ?>" />'
						<input class="box" id="dbpKoordinatEDetik2" name="dbpKoordinatEDetik2" maxlength="10" size="5" value="<?php echo $obj->dbpKoordinatEDetik2; ?>" />"
					</td>
				</tr> -->
				<!-- mulai dari 0 -->
<?php 
		$arr = explode('][', substr($obj->dbpKoordinat,1,strlen($obj->dbpKoordinat)-2));
		$arrKoordinat = array();
		foreach ($arr as $k=>$v) {
			$arrKoordinat[$k] = explode("|", $v);
		}
?>
				<tr>
					<td style="padding-left:20px;">Titik Koordinat</td><td>&nbsp;:&nbsp;</td>
					<td>
						N 
						<input class="box" name="dbpKoordinatNDerajat[0]" maxlength="10" size="3" value="<?php echo isset($arrKoordinat[0][0]) ? $arrKoordinat[0][0] : ''; ?>" />&deg;
						<input class="box" name="dbpKoordinatNMenit[0]" maxlength="10" size="3" value="<?php echo isset($arrKoordinat[0][1]) ? $arrKoordinat[0][1] : ''; ?>" />'
						<input class="box" name="dbpKoordinatNDetik[0]" maxlength="10" size="5" value="<?php echo isset($arrKoordinat[0][2]) ? $arrKoordinat[0][2] : ''; ?>" />"
						E 
						<input class="box" name="dbpKoordinatEDerajat[0]" maxlength="10" size="3" value="<?php echo isset($arrKoordinat[0][3]) ? $arrKoordinat[0][3] : ''; ?>" />&deg;
						<input class="box" name="dbpKoordinatEMenit[0]" maxlength="10" size="3" value="<?php echo isset($arrKoordinat[0][4]) ? $arrKoordinat[0][4] : ''; ?>" />'
						<input class="box" name="dbpKoordinatEDetik[0]" maxlength="10" size="5" value="<?php echo isset($arrKoordinat[0][5]) ? $arrKoordinat[0][5] : ''; ?>" />"
						<a href="javascript:addCoordinate();"><img src="images/icons/add.png"></a>
					</td>
				</tr>
<?php 
		$elIndex = 1;
		if (count($arrKoordinat) > 1) {
			foreach ($arrKoordinat as $k=>$v) {
				if ($k > 0) {
?>
				<tr id="trCoordinate-<?php echo $elIndex; ?>">
					<td></td><td>;</td>
					<td>
						N 
						<input class="box" name="dbpKoordinatNDerajat[<?php echo $elIndex; ?>]" maxlength="10" size="3" value="<?php echo isset($arrKoordinat[$elIndex][0]) ? $arrKoordinat[$elIndex][0] : ''; ?>" />&deg;
						<input class="box" name="dbpKoordinatNMenit[<?php echo $elIndex; ?>]" maxlength="10" size="3" value="<?php echo isset($arrKoordinat[$elIndex][1]) ? $arrKoordinat[$elIndex][1] : ''; ?>" />'
						<input class="box" name="dbpKoordinatNDetik[<?php echo $elIndex; ?>]" maxlength="10" size="5" value="<?php echo isset($arrKoordinat[$elIndex][2]) ? $arrKoordinat[$elIndex][2] : ''; ?>" />"
						E 
						<input class="box" name="dbpKoordinatEDerajat[<?php echo $elIndex; ?>]" maxlength="10" size="3" value="<?php echo isset($arrKoordinat[$elIndex][3]) ? $arrKoordinat[$elIndex][3] : ''; ?>" />&deg;
						<input class="box" name="dbpKoordinatEMenit[<?php echo $elIndex; ?>]" maxlength="10" size="3" value="<?php echo isset($arrKoordinat[$elIndex][4]) ? $arrKoordinat[$elIndex][4] : ''; ?>" />'
						<input class="box" name="dbpKoordinatEDetik[<?php echo $elIndex; ?>]" maxlength="10" size="5" value="<?php echo isset($arrKoordinat[$elIndex][5]) ? $arrKoordinat[$elIndex][5] : ''; ?>" />"
						<a href="javascript:removeCoordinate(<?php echo $elIndex; ?>);"><img src="images/icons/delete.png"></a>
					</td>
				</tr>
<?php
					$elIndex++;
				}
			}
		}
?>
				<tr id="trLast">
					<td colspan="3"></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan/Desa</td><td></td>
					<td>
<?php
		$sql = "SELECT w.wilID AS id, w.wilNama AS nama FROM wilayah AS w, wilayah AS w2, wilayah AS w3 WHERE w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilNama='Pelalawan' ORDER BY w.wilNama";
		$app->createSelect('bpKelurahanID', $sql, $obj->dbpKelurahanID);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Status Kepemilikan Lahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpStatusLahan" name="dbpStatusLahan" maxlength="50" size="50" value="<?php echo $obj->dbpStatusLahan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpNoLahan" name="dbpNoLahan" maxlength="50" size="50" value="<?php echo $obj->dbpNoLahan; ?>" required/>
						Tgl.
						<input class="box date" id="dbpTglLahan" name="dbpTglLahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->dbpTglLahan); ?>" required>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Areal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpLuasAreal" name="dbpLuasAreal" maxlength="50" size="50" value="<?php echo $obj->dbpLuasAreal; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Tanaman</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpJenisTanaman" name="dbpJenisTanaman" maxlength="50" size="50" value="<?php echo $obj->dbpJenisTanaman; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Produksi per Ha per Tahun</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpProduksiPerHaPerTahun" name="dbpProduksiPerHaPerTahun" maxlength="50" size="50" value="<?php echo $obj->dbpProduksiPerHaPerTahun; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Asal Benih</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpAsalBenih" name="dbpAsalBenih" maxlength="50" size="50" value="<?php echo $obj->dbpAsalBenih; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jumlah Pohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpJumlahPohon" name="dbpJumlahPohon" maxlength="50" size="50" value="<?php echo $obj->dbpJumlahPohon; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pola Tanam</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpPolaTanam" name="dbpPolaTanam" maxlength="50" size="50" value="<?php echo $obj->dbpPolaTanam; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Pupuk</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpJenisPupuk" name="dbpJenisPupuk" maxlength="50" size="50" value="<?php echo $obj->dbpJenisPupuk; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Mitra Pengolahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpMitraPengolahan" name="dbpMitraPengolahan" maxlength="50" size="50" value="<?php echo $obj->dbpMitraPengolahan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Tanah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpJenisTanah" name="dbpJenisTanah" maxlength="50" size="50" value="<?php echo $obj->dbpJenisTanah; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tahun Tanam</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpTahunTanam" name="dbpTahunTanam" maxlength="50" size="20" value="<?php echo $obj->dbpTahunTanam; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Usaha Lain di Lahan Kebun</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="dbpUsahaLain" name="dbpUsahaLain" maxlength="50" size="50" value="<?php echo $obj->dbpUsahaLain; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/detailbudidayaperkebunan/js/edit.detailbudidayaperkebunan.js?d=<?php echo date('dmYHis'); ?>"></script>
	<script>
		//sebelumnya ada 0
		var elIndex = <?php echo $elIndex; ?>;
		function addCoordinate() {
			$('#trLast').before('<tr id="trCoordinate-' + elIndex + '">' + 
				'<td></td><td></td>' + 
				'<td>' + 
				'N ' + 
				'<input class="box" name="dbpKoordinatNDerajat[' + elIndex + ']" maxlength="10" size="3" value="" />&deg; ' + 
				'<input class="box" name="dbpKoordinatNMenit[' + elIndex + ']" maxlength="10" size="3" value="" />&#39; ' + 
				'<input class="box" name="dbpKoordinatNDetik[' + elIndex + ']" maxlength="10" size="5" value="" />&#34; ' + 
				'E ' + 
				'<input class="box" name="dbpKoordinatEDerajat[' + elIndex + ']" maxlength="10" size="3" value="" />&deg; ' + 
				'<input class="box" name="dbpKoordinatEMenit[' + elIndex + ']" maxlength="10" size="3" value="" />&#39; ' + 
				'<input class="box" name="dbpKoordinatEDetik[' + elIndex + ']" maxlength="10" size="5" value="" />&#34; ' + 
				'<a href="javascript:removeCoordinate(' + elIndex + ');"><img src="images/icons/delete.png"></a>' + 
				'</td>' + 
				'</tr>');

			elIndex++;
		}

		function removeCoordinate(elIndex) {
			$('#trCoordinate-' + elIndex).remove();
		}
	</script>
<?php
	}
	
	static function viewDetailBudidayaPerkebunan($success, $msg, $arr, $id) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=detailbudidayaperkebunan&id=<?php echo $id; ?>"><img src="images/icons/rosette.png" border="0" /> Kelola Kebun untuk Surat Tanda Daftar Usaha Budidaya Perkebunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=detailbudidayaperkebunan&task=add&id=<?php echo $id; ?>">Tambah</a>
				<a class="button-link inline blue" href="index2.php?act=budidayaperkebunan&task=edit&id=<?php echo $id; ?>">Kembali</a>
			</div>
			<br>
<?php
		$app->showMessage($success, $msg);
?>
			<table class="dataTable">
			<thead>
			<tr>
				<th width="40">Aksi</th>
				<th width="80">Nomor</th>
				<th>Nomor Lahan</th>
				<th>Tgl. Lahan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=detailbudidayaperkebunan&task=edit&id='.$v->dbpID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->dbpID.', '."'Kebun ".$v->dbpNo."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=detailbudidayaperkebunan&task=edit&id='.$v->dbpID.'" title="Edit">Kebun '.$v->dbpNo.'</a></td>';
				echo '<td>'.$v->dbpNoLahan.'</td>';
				echo '<td>'.$app->MySQLDateToNormal($v->dbpTglLahan).'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="4">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/detailbudidayaperkebunan/js/detailbudidayaperkebunan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>