<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

backupTables(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);

//Backup the db OR just a table
function backupTables($host, $user, $pass, $name, $tables = '*') {  
	$link = mysql_connect($host,$user,$pass);
	mysql_select_db($name,$link);
	
	//Get all of the tables
  	if($tables == '*') {
		$tables = array();
		$result = mysql_query('SHOW TABLES');
		while(($row = mysql_fetch_row($result)) == true) {
		  $tables[] = $row[0];
		}
  	} else {
		$tables = is_array($tables) ? $tables : explode(',',$tables);
  	}
  	
  	//Cycle through
  	$return = '';
	foreach($tables as $table) {
		$result = mysql_query('SELECT * FROM '.$table); // or die(mysql_error($link));
		if ($result) {
			$num_fields = mysql_num_fields($result);
			
			$return.= 'DROP TABLE '.$table.';';
			$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
			$return.= "\n\n".$row2[1].";\n\n";
			
			for ($i = 0; $i < $num_fields; $i++) {
			  	while(($row = mysql_fetch_row($result)) == true) {
					$return.= 'INSERT INTO '.$table.' VALUES(';
					for($j=0; $j<$num_fields; $j++) {
						$row[$j] = addslashes($row[$j]);
			  			$row[$j] = str_replace("\n","\\n",$row[$j]);
			  			if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
			  			if ($j<($num_fields-1)) { $return.= ','; }
					}
					$return.= ");\n";
		  		}
			}
			$return.="\n\n\n";
		}
  	}
  
	$backupFolder = CFG_ABSOLUTE_PATH.'/backup';
	
	if (!file_exists($backupFolder)) {
		mkdir($backupFolder);
	}
  	
  	//Save file
  	//$filename = 'db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
  	//$handle = fopen($backupFolder.'/'.$filename,'w+');
  	$fileName = $app->database.'-'.time(); //.'-'.(md5(implode(',',$tables)));
	$sqlFileName = $fileName.'.sql';
	$handle = fopen($backupFolder.'/'.$sqlFileName,'w+');
	
	fwrite($handle,$return);
  	fclose($handle);
	
	$outputFileName = $sqlFileName;
	
	if (class_exists('ZipArchive')) {
		//Zip that file
		$zip = new ZipArchive();
		$zipFileName = $fileName.'.zip';
		if ($zip->open($backupFolder.'/'.$zipFileName, ZIPARCHIVE::CREATE) === TRUE) {
			$zip->addFile($backupFolder.'/'.$sqlFileName , $sqlFileName);
			$zip->close();
				
			//Delete the .sql file without any warning
			@unlink($backupFolder.'/'.$sqlFileName);
			
			$outputFileName = $zipFileName;
		} else {
			//Cannot open $zipFileName, use $sqlFileName
		}
	}
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php"><img src="images/icons/database.png" border="0" /> Backup Basisdata</a></h1>
	</div>
	<div class="halamanTengah">
  		Backup berhasil. Silahkan <a href="backup/<?php echo $outputFileName; ?>" download>download</a> ke komputer lokal.
  	</div>
  	<div class="halamanBawah"></div>
<?php
}
?>