<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.limbahcair.php';
require_once 'view.limbahcair.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editLimbahCair($app->id);
		break;
	case 'delete':
		deleteLimbahCair($app->id);
		break;
	case 'save':
		saveLimbahCair();
		break;
	default:
		viewLimbahCair(true, '');
		break;
}

function deleteLimbahCair($id) {
	global $app;
	
	//Get object
	$objLimbahCair = $app->queryObject("SELECT * FROM limbahcair WHERE lcID='".$id."'");
	if (!$objLimbahCair) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM limbahcair WHERE lcID='".$id."'");
		
		$success = true;
		$msg = 'Limbah Cair "'.$objLimbahCair->lcNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'limbahcair', $objLimbahCair->lcID, $objLimbahCair->lcNama, $msg);
	} else {
		$success = false;
		$msg = 'Limbah Cair "'.$objLimbahCair->lcNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewLimbahCair($success, $msg);
}

function editLimbahCair($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objLimbahCair = $app->queryObject("SELECT * FROM limbahcair WHERE lcID='".$id."'");
		if (!$objLimbahCair) {
			$app->showPageError();
			exit();
		}
	} else {
		$objLimbahCair = new LimbahCair_Model();
	}
	
	LimbahCair_View::editLimbahCair(true, '', $objLimbahCair);
}

function saveLimbahCair() {
	global $app;
	
	//Create object
	$objLimbahCair = new LimbahCair_Model();
	$app->bind($objLimbahCair);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objLimbahCair->lcKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objLimbahCair->lcNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT lcID AS id FROM limbahcair WHERE lcKode='".$objLimbahCair->lcKode."'", $objLimbahCair->lcID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objLimbahCair->lcKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT lcID AS id FROM limbahcair WHERE lcNama='".$objLimbahCair->lcNama."'", $objLimbahCair->lcID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objLimbahCair->lcNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objLimbahCair->lcID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objLimbahCair);
		} else {
			$sql = $app->createSQLforUpdate($objLimbahCair);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objLimbahCair->lcID = $app->queryField("SELECT LAST_INSERT_ID() FROM limbahcair");
			
			$success = true;
			$msg = 'Limbah Cair "'.$objLimbahCair->lcNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'limbahcair', $objLimbahCair->lcID, $objLimbahCair->lcNama, $msg);
		} else {
			$success = true;
			$msg = 'Limbah Cair "'.$objLimbahCair->lcNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'limbahcair', $objLimbahCair->lcID, $objLimbahCair->lcNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Limbah Cair "'.$objLimbahCair->lcNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewLimbahCair($success, $msg);
	} else {
		LimbahCair_View::editLimbahCair($success, $msg, $objLimbahCair);
	}
}

function viewLimbahCair($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('limbahcair', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('limbahcair', 'sort', 'lcKode');
	$dir   		= $app->pageVar('limbahcair', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('limbahcair', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(lcKode LIKE '%".$nama."%' OR lcNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM limbahcair
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM limbahcair
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	LimbahCair_View::viewLimbahCair($success, $msg, $arr);
}
?>