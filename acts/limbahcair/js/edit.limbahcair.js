//Variables
var form = $("#myForm");
var lcKode = $("#lcKode");
var lcNama = $("#lcNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'lcKode' || this.id === undefined) {
		if (lcKode.val().length == 0){
			doSubmit = false;
			lcKode.addClass("error");		
		} else {
			lcKode.removeClass("error");
		}
	}
	
	if (this.id == 'lcNama' || this.id === undefined) {
		if (lcNama.val().length == 0){
			doSubmit = false;
			lcNama.addClass("error");		
		} else {
			lcNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
lcKode.blur(validateForm);
lcNama.blur(validateForm);

lcKode.keyup(validateForm);
lcNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
lcKode.focus();