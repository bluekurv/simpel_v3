<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Perusahaan_View {
	static function editPerusahaan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=perusahaan&task=edit&id=<?php echo $obj->perID; ?>"><img src="images/icons/database_table.png" border="0" /> <?php echo ($obj->perID > 0) ? 'Ubah' : 'Tambah'; ?> Perusahaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=perusahaan&task=save" method="POST" >
				<input type="hidden" id="perID" name="perID" value="<?php echo $obj->perID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=perusahaan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=perusahaan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNama" name="perNama" maxlength="255" size="50" value="<?php echo $obj->perNama; ?>" />
					</td>
				</tr>
				<tr>
					<td>Masih Operasional?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('perOperasional', 1, 'Ya', 0, 'Tidak', $obj->perOperasional);
?>
					</td>
				</tr>
				<tr>
					<td>Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perAlamat" name="perAlamat" maxlength="500" size="100" value="<?php echo $obj->perAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td>Kelurahan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
				WHERE w.wilID='".$obj->perKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$perKelurahan = $app->queryObject($sql);
		if (!$perKelurahan) {
			$perKelurahan = new stdClass();
			$perKelurahan->kode = '';
			$perKelurahan->value = '';
		}
?>
						<input type="hidden" id="perKelurahanID" name="perKelurahanID" value="<?php echo $obj->perKelurahanID; ?>" />
						<input class="box readonly" id="perKelurahanKode" name="perKelurahanKode" size="8" value="<?php echo $perKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="perKelurahan" name="perKelurahan" maxlength="500" size="70" value="<?php echo $perKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, pergunakan sub menu yang ada di menu <b>Administrasi &gt; Lokasi</b> untuk mengelola data lokasi</i>)</td>
				</tr>
				<tr>
					<td>Kode Pos</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perKodePos" name="perKodePos" maxlength="5" size="5" value="<?php echo $obj->perKodePos; ?>" />
					</td>
				</tr>
				<tr>
					<td>No. Telepon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNoTelp" name="perNoTelp" maxlength="255" size="50" value="<?php echo $obj->perNoTelp; ?>" />
					</td>
				</tr>
				<tr>
					<td>No. Fax</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNoFax" name="perNoFax" maxlength="50" size="50" value="<?php echo $obj->perNoFax; ?>" />
					</td>
				</tr>
				<tr>
					<td>No. Telex</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNoTelex" name="perNoTelex" maxlength="50" size="50" value="<?php echo $obj->perNoTelex; ?>" />
					</td>
				</tr>
				<tr>
					<td>Email</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perEmail" name="perEmail" maxlength="50" size="50" value="<?php echo $obj->perEmail; ?>" />
					</td>
				</tr>
				<tr>
					<td>HomePage</td><td>&nbsp;:&nbsp;</td>
					<td>
						http://<input class="box" id="perHomePage" name="perHomePage" maxlength="255" size="100" value="<?php echo $obj->perHomePage; ?>" />
					</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td>Berbadan Hukum?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('perBerbadanHukum', 1, 'Ya', 0, 'Tidak', $obj->perBerbadanHukum);
?>
					</td>
				</tr>
				<tr>
					<td>Bentuk Badan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		$app->createSelect('perBentukBadan', "SELECT bperID AS id, CONCAT(bperKode,' - ',bperNama) AS nama FROM bentukperusahaan ORDER BY bperKode, bperNama", $obj->perBentukBadan, array(0=>''));
?>
					</td>
				</tr>
				<tr>
					<td>Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perJenisUsaha" name="perJenisUsaha" maxlength="255" size="50" value="<?php echo $obj->perJenisUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td>Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perBidangUsaha" name="perBidangUsaha" maxlength="255" size="50" value="<?php echo $obj->perBidangUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td>Status Perusahaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="perStatus" name="perStatus">
<?php 
		foreach (array('Tunggal', 'Pusat', 'Cabang', 'Pembantu', 'Perwakilan') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->perStatus) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Kegiatan Usaha Pokok</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perKegiatanUsahaPokok" name="perKegiatanUsahaPokok" maxlength="255" size="50" value="<?php echo $obj->perKegiatanUsahaPokok; ?>" />
					</td>
				</tr>
				<tr>
					<td>KBLI</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT kkelKode AS kode, kkelNama AS value 
				FROM kbli
				WHERE kkelID='".$obj->perKluiID."'";
		$perKlui = $app->queryObject($sql);
		if (!$perKlui) {
			$perKlui = new stdClass();
			$perKlui->kode = '';
			$perKlui->value = '';
		}
?>
						<input type="hidden" id="perKluiID" name="perKluiID" value="<?php echo $obj->perKluiID; ?>" />
						<input class="box readonly" id="perKluiKode" name="perKluiKode" size="8" value="<?php echo $perKlui->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="perKlui" name="perKlui" maxlength="500" size="70" value="<?php echo $perKlui->value; ?>" />
					</td>
				</tr>
				<tr>
					<td>Merek Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perMerekUsaha" name="perMerekUsaha" maxlength="255" size="50" value="<?php echo $obj->perMerekUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td>Luas Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perLuasTempatUsaha" name="perLuasTempatUsaha" maxlength="255" size="50" value="<?php echo $obj->perLuasTempatUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td>Status Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="perStatusTempatUsaha" name="perStatusTempatUsaha">
<?php 
		foreach (array(''=>'--Pilih--', 'Hak Milik'=>'Hak Milik', 'Hak Pakai'=>'Hak Pakai', 'Sewa'=>'Sewa') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $obj->perStatusTempatUsaha) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr><td colspan="3">&nbsp;</td></tr>
				<tr>
					<td>NPWP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNPWP" name="perNPWP" maxlength="50" size="50" value="<?php echo $obj->perNPWP; ?>" />
					</td>
				</tr>
				<tr>
					<td>NIPIK</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNIPIK" name="perNIPIK" maxlength="50" size="50" value="<?php echo $obj->perNIPIK; ?>" />
					</td>
				</tr>
				<tr>
					<td>NPWPD</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNPWPD" name="perNPWPD" maxlength="50" size="50" value="<?php echo $obj->perNPWPD; ?>" />
					</td>
				</tr>
				<tr>
					<td>NPWR</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNPWR" name="perNPWR" maxlength="50" size="50" value="<?php echo $obj->perNPWR; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Pemilik</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="perNoKTPPemilik" name="perNoKTPPemilik" maxlength="50" size="50" value="<?php echo $obj->perNoKTPPemilik; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="perNamaPemilik" name="perNamaPemilik" maxlength="50" size="50" value="<?php echo $obj->perNamaPemilik; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kewarganegaraan</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="perKewarganegaraanPemilik" name="perKewarganegaraanPemilik" maxlength="50" size="50" value="<?php echo $obj->perKewarganegaraanPemilik; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pekerjaan</td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="perPekerjaanPemilik" name="perPekerjaanPemilik" maxlength="50" size="50" value="<?php echo $obj->perPekerjaanPemilik; ?>" /></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tempat Lahir</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perTempatLahirPemilik" name="perTempatLahirPemilik" maxlength="255" size="50" value="<?php echo $obj->perTempatLahirPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tanggal Lahir</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date" id="perTglLahirPemilik" name="perTglLahirPemilik" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->perTglLahirPemilik); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perAlamatPemilik" name="perAlamatPemilik" maxlength="255" size="100" value="<?php echo $obj->perAlamatPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4
				WHERE w.wilID='".$obj->perKelurahanPemilikID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$perKelurahan = $app->queryObject($sql);
		if (!$perKelurahan) {
			$perKelurahan = new stdClass();
			$perKelurahan->kode = '';
			$perKelurahan->value = '';
		}
?>
						<input type="hidden" id="perKelurahanPemilikID" name="perKelurahanPemilikID" value="<?php echo $obj->perKelurahanPemilikID; ?>" />
						<input class="box readonly" id="perKelurahanPemilikKode" name="perKelurahanPemilikKode" size="8" value="<?php echo $perKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="perKelurahanPemilik" name="perKelurahanPemilik" maxlength="500" size="70" value="<?php echo $perKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, pergunakan sub menu yang ada di menu <b>Administrasi &gt; Lokasi</b> untuk mengelola data lokasi</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNoTelpPemilik" name="perNoTelpPemilik" maxlength="255" size="50" value="<?php echo $obj->perNoTelpPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Fax</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perNoFaxPemilik" name="perNoFaxPemilik" maxlength="255" size="50" value="<?php echo $obj->perNoFaxPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Pabrik</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kepemilikan Pabrik</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="perKepemilikanPabrik" name="tdiKepemilikanPabrik">
<?php 
		foreach (array('Milik Sendiri', 'Sewa', 'Lainnya') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->perKepemilikanPabrik) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Pabrik <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->perKelurahanLokasiID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$perKelurahanLokasi = $app->queryObject($sql);
		if (!$perKelurahanLokasi) {
			$perKelurahanLokasi = new stdClass();
			$perKelurahanLokasi->kode = '';
			$perKelurahanLokasi->value = '';
		}
?>
						<input type="hidden" id="perKelurahanLokasiID" name="perKelurahanLokasiID" value="<?php echo $obj->perKelurahanLokasiID; ?>" />
						<input class="box readonly" id="perKelurahanLokasiKode" name="perKelurahanLokasiKode" size="8" value="<?php echo $perKelurahanLokasi->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="perKelurahanLokasi" name="perKelurahanLokasi" maxlength="500" size="70" value="<?php echo $perKelurahanLokasi->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, pergunakan sub menu yang ada di menu <b>Administrasi &gt; Lokasi</b> untuk mengelola data lokasi</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Bangunan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perLuasBangunan" name="perLuasBangunan" maxlength="25" size="25" value="<?php echo $app->MySQLToMoney($obj->perLuasBangunan,2); ?>" /> <span>m<sup>2</sup></span>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Tanah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perLuasTanah" name="perLuasTanah" maxlength="25" size="25" value="<?php echo $app->MySQLToMoney($obj->perLuasTanah,2); ?>" /> <span>m<sup>2</sup></span>
					</td>
				</tr>
				<!-- <tr>
					<td style="padding-left:20px;">Jenis Industri (KLUI)</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		/*$sql = "SELECT kkelKode AS kode, kkelNama AS value 
				FROM kbli
				WHERE kkelID='".$obj->perKluiID."'";
		$perKlui = $app->queryObject($sql);
		if (!$perKlui) {
			$perKlui = new stdClass();
			$perKlui->kode = '';
			$perKlui->value = '';
		}*/
?>
						<input type="hidden" id="perKluiID" name="perKluiID" value="<?php echo $obj->perKluiID; ?>" />
						<input class="box readonly" id="perKluiKode" name="perKluiKode" size="8" value="<?php echo $perKlui->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="perKlui" name="perKlui" maxlength="500" size="70" value="<?php echo $perKlui->value; ?>" />
					</td>
				</tr> -->
				<tr>
					<td style="padding-left:20px;">Klasifikasi TDI</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="perKlasifikasi" name="perKlasifikasi">
<?php 
		foreach (array('IKAHH', 'IKAIK', 'ILME') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->perKlasifikasi) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Komoditi Industri</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perKomoditiIndustri" name="perKomoditiIndustri" maxlength="255" size="50" value="<?php echo $obj->perKomoditiIndustri; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tenaga Penggerak</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perTenagaPenggerak" name="perTenagaPenggerak" maxlength="255" size="50" value="<?php echo $obj->perTenagaPenggerak; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nilai Inventaris (Mesin/Peralatan)</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perInventaris" name="perInventaris" maxlength="20" size="25" value="<?php echo $app->MySQLToMoney($obj->perInventaris); ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kapasitas Produksi Terpasang Per Tahun</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perKapasitasProduksiPerTahun" name="perKapasitasProduksiPerTahun" maxlength="20" size="25" value="<?php echo $app->MySQLToMoney($obj->perKapasitasProduksiPerTahun); ?>" />
						dalam satuan
						<input class="box" id="perSatuanKapasitasProduksiPerTahun" name="perSatuanKapasitasProduksiPerTahun" maxlength="255" size="50" value="<?php echo $obj->perSatuanKapasitasProduksiPerTahun; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Tenaga Kerja Indonesia</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Laki-laki</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perTenagaKerjaIndonesiaLaki" name="perTenagaKerjaIndonesiaLaki" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->perTenagaKerjaIndonesiaLaki); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Perempuan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perTenagaKerjaIndonesiaPerempuan" name="perTenagaKerjaIndonesiaPerempuan" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->perTenagaKerjaIndonesiaPerempuan); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Tenaga Kerja Asing</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Laki-laki</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perTenagaKerjaAsingLaki" name="perTenagaKerjaAsingLaki" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->perTenagaKerjaAsingLaki); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Perempuan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="perTenagaKerjaAsingPerempuan" name="perTenagaKerjaAsingPerempuan" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->perTenagaKerjaAsingPerempuan); ?>" /> orang
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/perusahaan/js/edit.perusahaan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function readPerusahaan($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?><?php echo ($app->fromact != '') ? '&fromact='.$app->fromact.'&fromtask='.$app->fromtask.'&fromid='.$app->fromid : ''; ?>"><img src="images/icons/database_table.png" border="0" /> Lihat Perusahaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="#suratizin">Surat Izin yang Dimiliki</a>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=perusahaan' : 'index2.php?act='.$app->fromact.'&task='.$app->fromtask.'&id='.$app->fromid; ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="#suratizin">Surat Izin yang Dimiliki</a>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=perusahaan' : 'index2.php?act='.$app->fromact.'&task='.$app->fromtask.'&id='.$app->fromid; ?>">Selesai</a>
			<br>
			<br>
		</div>
		<br>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td width="150">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNama; ?>
			</td>
		</tr>
		<tr>
			<td>Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td>Kode Pos</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perKodePos; ?>
			</td>
		</tr>
		<tr>
			<td>No. Telepon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNoTelp; ?>
			</td>
		</tr>
		<tr>
			<td>No. Fax</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNoFax; ?>
			</td>
		</tr>
		<tr>
			<td>No. Telex</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNoTelex; ?>
			</td>
		</tr>
		<tr>
			<td>Email</td><td>&nbsp;:&nbsp;</td>
			<td>
				
			</td>
		</tr>
		<tr>
			<td>HomePage</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo ($obj->perHomePage != '') ? '<a href="http://'.$obj->perHomePage.'">'.$obj->perHomePage.'</a>' : ''; ?>
			</td>
		</tr>
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr>
			<td>Berbadan Hukum?</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo ($obj->perBerbadanHukum == 1) ? 'Ya' : 'Tidak'; ?>
			</td>
		</tr>
		<tr>
			<td>Bentuk Badan</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php
		echo $app->queryField("SELECT bperNama FROM bentukperusahaan WHERE bperID='".$obj->perBentukBadan."'");
?>
			</td>
		</tr>
		<tr>
			<td>Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perJenisUsaha; ?>
			</td>
		</tr>
		<tr>
			<td>Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perBidangUsaha; ?>
			</td>
		</tr>
		<tr>
			<td>Kegiatan Usaha Pokok</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perKegiatanUsahaPokok; ?>
			</td>
		</tr>
		<tr>
			<td>KBLI</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT kkelKode AS kode, kkelNama AS value 
				FROM kbli
				WHERE kkelID='".$obj->perKluiID."'";
		$perKlui = $app->queryObject($sql);
		if (!$perKlui) {
			$perKlui = new stdClass();
			$perKlui->kode = '';
			$perKlui->value = '';
		}
?>
				<?php echo $perKlui->kode.' '.$perKlui->value; ?>
			</td>
		</tr>
		<tr>
			<td>Merek Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perMerekUsaha; ?>
			</td>
		</tr>
		<tr>
			<td>Luas Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perLuasTempatUsaha; ?>
			</td>
		</tr>
		<tr>
			<td>Status Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perStatusTempatUsaha; ?>
			</td>
		</tr>
		<tr><td colspan="3">&nbsp;</td></tr>
		<tr>
			<td>NPWP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNPWP; ?>
			</td>
		</tr>
		<tr>
			<td>NIPIK</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNIPIK; ?>
			</td>
		</tr>
		<tr>
			<td>NPWPD</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNPWPD; ?>
			</td>
		</tr>
		<tr>
			<td>NPWR</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNPWR; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Pemilik</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNoKTPPemilik; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNamaPemilik; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kewarganegaraan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perKewarganegaraanPemilik; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pekerjaan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perPekerjaanPemilik; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tempat Lahir</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo ($obj->perTempatLahirPemilik != '') ? $obj->perTempatLahirPemilik : '-'; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tanggal Lahir</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo ($obj->perTglLahirPemilik != '0000-00-00') ? $app->MySQLDateToNormal($obj->perTglLahirPemilik) : '-'; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamatPemilik; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
			<td>

<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanPemilikID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Telepon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNoTelpPemilik; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Fax</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perNoFaxPemilik; ?>
			</td>
		</tr>
		</table>
		
		<a name="suratizin"></a>
		<p><b><u>Surat Izin yang Dimiliki</u></b></p>
		<table class="dataTable">
		<thead>
		<tr>
			<th>Jenis Surat Izin</th>
			<th>Nomor Surat</th>
			<th>Tgl. Pengesahan</th>
			<th>Berlaku s.d. Tgl</th>
			<th>Keterangan</th>
		</tr>
		</thead>
		<tbody>
<?php 
		$fromact = $app->getStr('fromact'); 
		$num = 0;
		
		switch ($fromact) {
			/*case 'imb':
				$num += Perusahaan_View::readImbPerusahaan($obj);
				break;
			case 'pariwisata':
				$num += Perusahaan_View::readPariwisataPerusahaan($obj);
				break;
			case 'situ':
				$num += Perusahaan_View::readSituPerusahaan($obj);
				break;
			case 'situho':
				$num += Perusahaan_View::readSituhoPerusahaan($obj);
				break;
			case 'siup':
				$num += Perusahaan_View::readSiupPerusahaan($obj);
				break;
			case 'tdi':
				$num += Perusahaan_View::readTdiPerusahaan($obj);
				break;
			case 'tdp':
				$num += Perusahaan_View::readTdpPerusahaan($obj);
				break;*/
			default:
				$num += Perusahaan_View::readImbPerusahaan($obj);
				$num += Perusahaan_View::readPariwisataPerusahaan($obj);
				$num += Perusahaan_View::readSituPerusahaan($obj);
				$num += Perusahaan_View::readSituhoPerusahaan($obj);
				$num += Perusahaan_View::readSiupPerusahaan($obj);
				$num += Perusahaan_View::readTdiPerusahaan($obj);
				$num += Perusahaan_View::readTdpPerusahaan($obj);
				break;
		}
		
		if ($num == 0) {
?>
		<tr>
			<td colspan="5">Tidak ada</td>
		</tr>
<?php
		}
?>
		</tbody>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/perusahaan/js/read.perusahaan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function readImbPerusahaan($objPerusahaan) {
		global $app;
		
		$sql = "SELECT *
				FROM detailpermohonan, imb
				WHERE dmhnID=imbDmhnID AND dmhnPerID='".$objPerusahaan->perID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=1
				ORDER BY imbTglDaftarUlang DESC";
		$rs = $app->query($sql);
		$num = mysql_num_rows($rs);
		
		if ($num > 0) {
			while(($obj = mysql_fetch_object($rs)) == true){
?>
			<tr>
				<td>Izin Mendirikan Bangunan</td>
				<td><?php echo $obj->imbNoLengkap; ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->imbTglPengesahan); ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->imbTglBerlaku); ?></td>
				<td></td>
			</tr>
<?php
			}
		}
		
		return $num;
	}
	
	static function readPariwisataPerusahaan($objPerusahaan) {
		global $app;
		
		$sql = "SELECT *
				FROM detailpermohonan, pariwisata
				WHERE dmhnID=pariwisataDmhnID AND dmhnPerID='".$objPerusahaan->perID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=1
				ORDER BY pariwisataTglDaftarUlang DESC";
		$rs = $app->query($sql);
		$num = mysql_num_rows($rs);
		
		if ($num > 0) {
			while(($obj = mysql_fetch_object($rs)) == true){
?>
			<tr>
				<td>Izin Usaha Pariwisata</td>
				<td><?php echo $obj->pariwisataNoLengkap; ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->pariwisataTglPengesahan); ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->pariwisataTglBerlaku); ?></td>
				<td></td>
			</tr>
<?php
			}
		}
		
		return $num;
	}
	
	static function readSituPerusahaan($objPerusahaan) {
		global $app;
		
		$sql = "SELECT *
				FROM detailpermohonan, situ
				WHERE dmhnID=situDmhnID AND dmhnPerID='".$objPerusahaan->perID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=1
				ORDER BY situTglDaftarUlang DESC";
		$rs = $app->query($sql);
		$num = mysql_num_rows($rs);
		
		if ($num > 0) {
			while(($obj = mysql_fetch_object($rs)) == true){
?>
			<tr>
				<td>Surat Izin Tempat Usaha</td>
				<td><?php echo $obj->situNoLengkap; ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->situTglPengesahan); ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->situTglBerlaku); ?></td>
				<td></td>
			</tr>
<?php
			}
		}
		
		return $num;
	}
	
	static function readSituhoPerusahaan($objPerusahaan) {
		global $app;
		
		$sql = "SELECT *
				FROM detailpermohonan, situho
				WHERE dmhnID=situhoDmhnID AND dmhnPerID='".$objPerusahaan->perID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=1
				ORDER BY situhoTglDaftarUlang DESC";
		$rs = $app->query($sql);
		$num = mysql_num_rows($rs);
		
		if ($num > 0) {
			while(($obj = mysql_fetch_object($rs)) == true){
?>
			<tr>
				<td>Izin Gangguan</td>
				<td><?php echo $obj->situhoNoLengkap; ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->situhoTglPengesahan); ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->situhoTglBerlaku); ?></td>
				<td></td>
			</tr>
<?php
			}
		}
		
		return $num;
	}
	
	static function readSiupPerusahaan($objPerusahaan) {
		global $app;
		
		$sql = "SELECT *
				FROM detailpermohonan, siup
				WHERE dmhnID=siupDmhnID AND dmhnPerID='".$objPerusahaan->perID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=1
				ORDER BY siupTglDaftarUlang DESC";
		$rs = $app->query($sql);
		$num = mysql_num_rows($rs);
		
		if ($num > 0) {
			while(($obj = mysql_fetch_object($rs)) == true){
?>
			<tr>
				<td>Surat Izin Usaha Perdagangan</td>
				<td><?php echo $obj->siupNoLengkap; ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->siupTglPengesahan); ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->siupTglBerlaku); ?></td>
				<td></td>
			</tr>
<?php
			}
		}
		
		return $num;
	}
	
	static function readTdiPerusahaan($objPerusahaan) {
		global $app;
		
		$sql = "SELECT *
				FROM detailpermohonan, tdi
				WHERE dmhnID=tdiDmhnID AND dmhnPerID='".$objPerusahaan->perID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=1
				ORDER BY tdiTglDaftarUlang DESC";
		$rs = $app->query($sql);
		$num = mysql_num_rows($rs);
		
		if ($num > 0) {
			while(($obj = mysql_fetch_object($rs)) == true){
?>
			<tr>
				<td>Tanda Daftar Industri</td>
				<td><?php echo $obj->tdiNoLengkap; ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->tdiTglPengesahan); ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->tdiTglBerlaku); ?></td>
				<td></td>
			</tr>
<?php
			}
		}
		
		return $num;
	}
	
	static function readTdpPerusahaan($objPerusahaan) {
		global $app;
		
		$sql = "SELECT *
				FROM detailpermohonan, tdp
				WHERE dmhnID=tdpDmhnID AND dmhnPerID='".$objPerusahaan->perID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=1
				ORDER BY tdpTglDaftarUlang DESC";
		$rs = $app->query($sql);
		$num = mysql_num_rows($rs);
		
		if ($num > 0) {
			while(($obj = mysql_fetch_object($rs)) == true){
				/*switch ($objPerusahaan->perKluiTabel) {
					case 'golongan':
						$sql = "SELECT kgolKode AS kode, kgolNama AS value 
								FROM kbligolongan
								WHERE kgolID='".$objPerusahaan->perKluiID."'";
						break;
					case 'subgolongan':
						$sql = "SELECT ksubKode AS kode, ksubNama AS value 
								FROM kblisubgolongan
								WHERE ksubID='".$objPerusahaan->perKluiID."'";
						break;
					case 'kelompok':
						$sql = "SELECT kkelKode AS kode, kkelNama AS value 
								FROM kblikelompok
								WHERE kkelID='".$objPerusahaan->perKluiID."'";
						break;
				}
				$perKlui = $app->queryObject($sql);
				if (!$perKlui) {
					$perKlui = new stdClass();
					$perKlui->kode = '';
					$perKlui->value = '';
				} else {
					$perKlui->tabel = $obj->perKluiTabel;
				}
				$nomor = '0416'.$app->queryField("SELECT bperNilai FROM bentukperusahaan WHERE bperID='".$objPerusahaan->perBentukBadan."'").substr($perKlui->kode,0,2).sprintf('%05d', $obj->tdpNo);*/
?>
			<tr>
				<td>Tanda Daftar Perusahaan</td>
				<td><?php echo $obj->tdpNoLengkap; ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->tdpTglPengesahan); ?></td>
				<td><?php echo $app->MySQLDateToNormal($obj->tdpTglBerlaku); ?></td>
				<td><?php echo 'Pendaftaran '.$obj->tdpPendaftaran.', Pembaharuan ke-'.$obj->tdpPembaharuan; ?></td>
			</tr>
<?php
			}
		}
	}
	
	static function viewPerusahaan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=perusahaan"><img src="images/icons/database_table.png" border="0" /> Perusahaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=perusahaan&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 60),
			$app->setHeader('perNama', 'Nama', true),
			$app->setHeader('perOperasional', 'Masih Operasional?', true, 150)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=perusahaan&task=edit&id='.$v->perID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> ';
				echo '<a href="index2.php?act=perusahaan&task=read&id='.$v->perID.'" title="Lihat"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a href="javascript:hapus('.$v->perID.', '."'".$v->perNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a>';
				echo '</td>';
				echo '<td><a href="index2.php?act=perusahaan&task=edit&id='.$v->perID.'">'.$v->perNama.'</a></td>';
				echo '<td>'.($v->perOperasional == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="3">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/perusahaan/js/perusahaan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>