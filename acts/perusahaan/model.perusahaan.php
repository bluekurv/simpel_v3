<?php
class Perusahaan_Model {	
	public $tabel = 'perusahaan';
	public $primaryKey = 'perID';
	
	public $perID = 0;
	public $perNama = '';
	public $perAlamat = '';
	public $perKelurahanID = 0;
	
	public $perKodePos = '';
	public $perNoTelp = '';
	public $perNoFax = '';
	public $perNoTelex = '';
	public $perEmail = '';
	public $perHomePage = '';
	
	public $perBerbadanHukum = 0;
	public $perBentukBadan = 0;
	public $perJenisUsaha = '';
	public $perBidangUsaha = '';
	public $perStatus = '';
	public $perKegiatanUsahaPokok = '';
	public $perKluiTabel = '';
	public $perKluiID = 0;
	public $perMerekUsaha = '';
	
	public $perNPWP = '';
	public $perNIPIK = '';
	public $perNPWPD = '';
	public $perNPWR = '';
	
	public $perNamaPemilik = '';
	public $perKewarganegaraanPemilik = 'Indonesia';
	public $perNoKTPPemilik = '';
	public $perPekerjaanPemilik = '';
	public $perTempatLahirPemilik = '';
	public $perTglLahirPemilik = '0000-00-00';
	public $perAlamatPemilik = '';
	public $perNoTelpPemilik = '';
	public $perNoFaxPemilik = '';
	public $perKelurahanPemilikID = 0;
	
	/*public $perNamaPenanggung = '';
	public $perAlamatPenanggung = '';
	public $perJabatanPenanggung = '';
	public $perDireksiNama = '';
	public $perKomisarisNama = '';*/
	
	public $perLuasTempatUsaha = '';
	public $perStatusTempatUsaha = '';
	
	/*public $perGrpID = 0;	 	 	 	 	 	
	public $perStatus = 0;	 	 	 	 	 	
	public $perAnak = 0;	 	 	 	 	 	
	public $perIndukNama = '';
	public $perIndukAlamat = '';
	public $perIndukKelurahan = 0;	 	 	 	 	 	
	public $perIndukKodePos = '';	 	 	 	 	 
	public $perIndukNoTelp = '';
	public $perIndukNoFax = '';
	public $perIndukNoTelex = '';
	public $perLokasiProduksi = 0;	 	 	 	 	 	
	public $perPenanamanModal = 0;	 	 	 	 	 	
	public $perWNIPria = 0;	 	 	 	 	 	 	
	public $perWNIWanita = 0;	 	 	 	 	 	 	
	public $perWNIJumlah = 0;	 	 	 	 	 	 	
	public $perWNAPria = 0;	 	 	 	 	 	 	
	public $perWNAWanita = 0;	 	 	 	 	 	 	
	public $perWNAJumlah = 0;	 	 	 	 	 	 	
	public $perKluiID = 0;	 	 	 	 	 	
	public $perKluiNama = '';
	public $perKluiKode = '';
	public $perProID = 0;	 	 	 	 	 	
	public $perOmset = 0;	 	 	 	 	 	 	
	public $perAset = 0;	 	 	 	 	 	 	
	public $perJenis = 0;	 	 	 	 	 	 	
	public $perProdusen = 0;	 	 	 	 	 	
	public $perEksportir = 0;	 	 	 	 	 	
	public $perImportir = 0;	 	 	 	 	 	
	public $perDistributor = 0;	 	 	 	 	 	
	public $perSubDistributor = 0;	 	 	 	 	 	
	public $perAgen = 0;	 	 	 	 	 	
	public $perPengecer = 0;	 	 	 	 	 	
	public $perKapasitasTerpasang = 0;	 	 	 	 	 	 	
	public $perSatuanTerpasang = 0;	 	 	 	 	 	
	public $perKapasitasProduksi = 0;	 	 	 	 	 	 	
	public $perSatuanProduksi = 0;	 	 	 	 	 	
	public $perKandunganLokal = 0;	 	 	 	 	 	 	
	public $perKandunganImpor = 0;	 	 	 	 	 	 	
	public $perJenisPengecer = 0;	 	 	 	 	 	
	public $perJenisPengecerKet = '';
	public $perTglPendirian = '0000-00-00';
	public $perTglMulaiKegiatan = '0000-00-00';
	public $perJangkaWaktu = 0;	 	 	 	 	 	
	public $perBentukKerjaSama = 0;	 	 	 	 	 	
	public $perMerekDagang = '';
	public $perNoMerekDagang = '';
	public $perHakPaten = '';
	public $perNoHakPaten = '';
	public $perHakCipta = '';
	public $perNoHakCipta = '';
	public $perAkteNo = '';
	public $perAkteTgl = '0000-00-00';
	public $perAkteNamaNotaris = '';
	public $perAkteAlamatNotaris = '';
	public $perAkteNoTelpNotaris = '';
	public $perAkteNoPengesahan = '';
	public $perAkteTglPengesahan = '0000-00-00';
	public $perAkteNoPersetujuan = '';
	public $perAkteTglPersetujuan = '0000-00-00';
	public $perAkteNoPenerimaan = '';
	public $perAkteTglPenerimaan = '0000-00-00';*/
	
	public $perKepemilikanPabrik = '';
	public $perKelurahanLokasiID = 0;
	public $perLuasBangunan = 0.00;
	public $perLuasTanah = 0.00;
	public $perTenagaPenggerak = '';
	public $perKlasifikasi = '';
	public $perKomoditiIndustri = '';
	public $perInventaris = 0;
	public $perKapasitasProduksiPerTahun = 0;
	public $perSatuanKapasitasProduksiPerTahun = '';
	public $perTenagaKerjaIndonesiaLaki = 0;
	public $perTenagaKerjaIndonesiaPerempuan = 0;
	public $perTenagaKerjaAsingLaki = 0;
	public $perTenagaKerjaAsingPerempuan = 0;
	
	public $perOperasional = 0;
}
?>