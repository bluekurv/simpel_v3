<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Administrasi']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.perusahaan.php';
require_once 'view.perusahaan.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editPerusahaan($app->id);
		break;
	case 'delete':
		deletePerusahaan($app->id);
		break;
	case 'read':
		readPerusahaan($app->id);
		break;
	case 'save':
		savePerusahaan();
		break;
	default:
		viewPerusahaan(true, '');
		break;
}

function deletePerusahaan($id) {
	global $app;
	
	//Get object
	$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perID='".$id."'");
	if (!$objPerusahaan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalPermohonan = intval($app->queryField("SELECT COUNT(*) AS total FROM permohonan WHERE mhnPerID='".$id."'"));
	if ($totalPermohonan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalPermohonan.' permohonan untuk perusahaan tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM perusahaan WHERE perID='".$id."'");
		
		$success = true;
		$msg = 'Perusahaan "'.$objPerusahaan->perNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'perusahaan', $objPerusahaan->perID, $objPerusahaan->perNama, $msg);
	} else {
		$success = false;
		$msg = 'Perusahaan "'.$objPerusahaan->perNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewPerusahaan($success, $msg);
}

function editPerusahaan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perID='".$id."'");
		if (!$objPerusahaan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objPerusahaan = new Perusahaan_Model();
	}
	
	Perusahaan_View::editPerusahaan(true, '', $objPerusahaan);
}

function readPerusahaan($id) {
	global $app;
	
	//Query
	$objPerusahaan = $app->queryObject("SELECT * FROM perusahaan WHERE perID='".$id."'");
	if (!$objPerusahaan) {
		$app->showPageError();
		exit();
	}
	
	Perusahaan_View::readPerusahaan($objPerusahaan);
}

function savePerusahaan() {
	global $app;
	
	//Create object
	$objPerusahaan = new Perusahaan_Model();
	$app->bind($objPerusahaan);
	
	//Modify object (if necessary)
	$objPerusahaan->perTglLahirPemilik = $app->NormalDateToMySQL($objPerusahaan->perTglLahirPemilik);
	
	if (isset($_REQUEST['perOperasional'])) {
		$objPerusahaan->perOperasional = ($_REQUEST['perOperasional'] == 'on') ? 1 : 0;
	} else {
		$objPerusahaan->perOperasional = 0;
	}
	if (isset($_REQUEST['perBerbadanHukum'])) {
		$objPerusahaan->perBerbadanHukum = ($_REQUEST['perBerbadanHukum'] == 'on') ? 1 : 0;
	} else {
		$objPerusahaan->perBerbadanHukum = 0;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objPerusahaan->perNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objPerusahaan->perStatusTempatUsaha == '') {
		$doSave = false;
		$msg[] = '- Status Tempat Usaha belum dipilih';
	}
	
	$isExist = $app->isExist("SELECT perID AS id FROM perusahaan WHERE perNama='".$objPerusahaan->perNama."'", $objPerusahaan->perID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objPerusahaan->perNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objPerusahaan->perID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objPerusahaan);
		} else {
			$sql = $app->createSQLforUpdate($objPerusahaan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objPerusahaan->perID = $app->queryField("SELECT LAST_INSERT_ID() FROM perusahaan");
			
			$success = true;
			$msg = 'Perusahaan "'.$objPerusahaan->perNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'perusahaan', $objPerusahaan->perID, $objPerusahaan->perNama, $msg);
		} else {
			$success = true;
			$msg = 'Perusahaan "'.$objPerusahaan->perNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'perusahaan', $objPerusahaan->perID, $objPerusahaan->perNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Perusahaan "'.$objPerusahaan->perNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewPerusahaan($success, $msg);
	} else {
		Perusahaan_View::editPerusahaan($success, $msg, $objPerusahaan);
	}
}

function viewPerusahaan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('perusahaan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('perusahaan', 'sort', 'perNama');
	$dir		= $app->pageVar('perusahaan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('perusahaan', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "perNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM perusahaan
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM perusahaan
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Perusahaan_View::viewPerusahaan($success, $msg, $arr);
}
?>