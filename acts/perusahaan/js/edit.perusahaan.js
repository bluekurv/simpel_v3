//Objects
$("#perLuasBangunan").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
$("#perLuasTanah").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
$("#perInventaris").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#perKapasitasProduksiPerTahun").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#perTenagaKerjaIndonesiaLaki").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#perTenagaKerjaIndonesiaPerempuan").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#perTenagaKerjaAsingLaki").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#perTenagaKerjaAsingPerempuan").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });

//Variables
var form = $("#myForm");
var perNama = $("#perNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'perNama' || this.id === undefined) {
		if (perNama.val().length == 0){
			doSubmit = false;
			perNama.addClass("error");		
		} else {
			perNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

perNama.blur(validateForm);

perNama.keyup(validateForm);

form.submit(submitForm);

$('#perKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#perKelurahanID').val(suggestion.data.id);
		$('#perKelurahanKode').val(suggestion.data.kode);
		$('#perKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#perKelurahanPemilik').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#perKelurahanPemilikID').val(suggestion.data.id);
		$('#perKelurahanPemilikKode').val(suggestion.data.kode);
		$('#perKelurahanPemilik').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#perKlui').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=klui',
	onSelect: function(suggestion){
		$('#perKluiID').val(suggestion.data.id);
		$('#perKluiKode').val(suggestion.data.kode);
		$('#perKlui').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#perKelurahanLokasi').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#perKelurahanLokasiID').val(suggestion.data.id);
		$('#perKelurahanLokasiKode').val(suggestion.data.kode);
		$('#perKelurahanLokasi').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

//Set focus
perNama.focus();