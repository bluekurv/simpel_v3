//Variables
var form = $("#myForm");
var sytNama = $("#sytNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'sytNama' || this.id === undefined) {
		if (sytNama.val().length == 0){
			doSubmit = false;
			sytNama.addClass("error");		
		} else {
			sytNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
sytNama.blur(validateForm);

sytNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
sytNama.focus();