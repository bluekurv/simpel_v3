<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.syarat.php';
require_once 'view.syarat.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editSyarat($app->id);
		break;
	case 'delete':
		deleteSyarat($app->id);
		break;
	case 'save':
		saveSyarat();
		break;
	default:
		viewSyarat(true, '');
		break;
}

function deleteSyarat($id) {
	global $app;
	
	//Get object
	$objSyarat = $app->queryObject("SELECT * FROM syarat WHERE sytID='".$id."'");
	if (!$objSyarat) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalSyaratSuratIzin = intval($app->queryField("SELECT COUNT(*) AS total FROM syaratsuratizin WHERE ssiSytID='".$id."'"));
	if ($totalSyaratSuratIzin > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalSyaratSuratIzin.' jenis surat izin dengan syarat tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM syarat WHERE sytID='".$id."'");
		
		$success = true;
		$msg = 'Syarat "'.$objSyarat->sytNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'syarat', $objSyarat->sytID, $objSyarat->sytNama, $msg);
	} else {
		$success = false;
		$msg = 'Syarat "'.$objSyarat->sytNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewSyarat($success, $msg);
}

function editSyarat($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objSyarat = $app->queryObject("SELECT * FROM syarat WHERE sytID='".$id."'");
		if (!$objSyarat) {
			$app->showPageError();
			exit();
		}
	} else {
		$objSyarat = new Syarat_Model();
	}
	
	Syarat_View::editSyarat(true, '', $objSyarat);
}

function saveSyarat() {
	global $app;
	
	//Create object
	$objSyarat = new Syarat_Model();
	$app->bind($objSyarat);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objSyarat->sytNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT sytID AS id FROM syarat WHERE sytNama='".$objSyarat->sytNama."'", $objSyarat->sytID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama "'.$objSyarat->sytNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objSyarat->sytID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objSyarat);
		} else {
			$sql = $app->createSQLforUpdate($objSyarat);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objSyarat->sytID = $app->queryField("SELECT LAST_INSERT_ID() FROM syarat");
			
			$success = true;
			$msg = 'Syarat "'.$objSyarat->sytNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'syarat', $objSyarat->sytID, $objSyarat->sytNama, $msg);
		} else {
			$success = true;
			$msg = 'Syarat "'.$objSyarat->sytNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'syarat', $objSyarat->sytID, $objSyarat->sytNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Syarat "'.$objSyarat->sytNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewSyarat($success, $msg);
	} else {
		Syarat_View::editSyarat($success, $msg, $objSyarat);
	}
}

function viewSyarat($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('syarat', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('syarat', 'sort', 'sytNama');
	$dir   		= $app->pageVar('syarat', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('syarat', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "sytNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM syarat
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM syarat
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Syarat_View::viewSyarat($success, $msg, $arr);
}
?>