<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'poultrishop';
$childTitle = 'Izin Operasional Poultri Shop (Pakan Unggas)';
$childKodeSurat = 'IOP-PU';
$childTembusan = 'Kepala Dinas Peternakan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>