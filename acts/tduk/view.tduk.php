<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Tduk_View {
	static function editTduk($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tduk&task=edit&id=<?php echo $obj->tdukDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri TDUK</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=tduk&task=save" method="POST" >
				<input type="hidden" id="tdukID" name="tdukID" value="<?php echo $obj->tdukID; ?>" />
				<input type="hidden" id="tdukDmhnID" name="tdukDmhnID" value="<?php echo $obj->tdukDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('tdukGrupID', $obj->tdukGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=tduk&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=tduk&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->perAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdukNPWPD" name="tdukNPWPD" maxlength="255" size="50" value="<?php echo $obj->tdukNPWPD; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//$app->createSelect('tdukBushID', "SELECT bushID AS id, bushNama AS nama FROM bidangusaha ORDER BY bushNama", $obj->tdukBushID, array(0=>''));
?>
						<input class="box" id="tdukBidangPariwisata" name="tdukBidangPariwisata" maxlength="255" size="50" value="Rekreasi dan Hiburan Umum" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//$app->createSelect('tdukJparID', "SELECT jparID AS id, jparNama AS nama FROM jenispariwisata ORDER BY jparNama", $obj->tdukJparID, array(0=>''));
?>
						<input class="box" id="tdukJenisPariwisata" name="tdukJenisPariwisata" maxlength="255" size="50" value="<?php echo $obj->tdukJenisPariwisata; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdukMerekUsahaPerusahaan" name="tdukMerekUsahaPerusahaan" maxlength="255" size="50" value="<?php echo $obj->tdukMerekUsahaPerusahaan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdukLokasiUsaha" name="tdukLokasiUsaha" maxlength="255" size="50" value="<?php echo $obj->tdukLokasiUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->tdukKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$tdukKelurahan = $app->queryObject($sql);
		if (!$tdukKelurahan) {
			$tdukKelurahan = new stdClass();
			$tdukKelurahan->kode = '';
			$tdukKelurahan->value = '';
		}
?>
						<input type="hidden" id="tdukKelurahanID" name="tdukKelurahanID" value="<?php echo $obj->tdukKelurahanID; ?>" />
						<input class="box readonly" id="tdukKelurahanKode" name="tdukKelurahanKode" size="8" value="<?php echo $tdukKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="tdukKelurahan" name="tdukKelurahan" maxlength="500" size="70" value="<?php echo $tdukKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdukKeterangan" name="tdukKeterangan" maxlength="255" size="50" value="<?php echo $obj->tdukKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdukNo > 0) {
?>
						<span>435/<?php echo CFG_COMPANY_SHORT_NAME; ?>/TDU-RH/<?php echo substr($obj->tdukTglPengesahan,0,4); ?>/</span>
						<input class="box" id="tdukNo" name="tdukNo" maxlength="5" size="5" value="<?php echo $obj->tdukNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="tdukNo" name="tdukNo" value="<?php echo $obj->tdukNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="tdukTglPengesahan" name="tdukTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdukTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="tdukPejID" name="tdukPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->tdukPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		/*$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='tduk'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->tdukTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdukTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}*/
?>
						<!-- <input type="hidden" id="tdukTglBerlaku" name="tdukTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->tdukTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>"> -->
						
						<input class="box date2" id="tdukTglBerlaku" name="tdukTglBerlaku" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdukTglBerlaku); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		/*if ($obj->tdukTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdukTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}*/
?>
						<!-- <input type="hidden" id="tdukTglDaftarUlang" name="tdukTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->tdukTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>"> -->
						
						<input class="box date2" id="tdukTglDaftarUlang" name="tdukTglDaftarUlang" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdukTglDaftarUlang); ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jstduk/edit.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printTduk($obj) {
		global $app;
		
		$objReport 		= new Report('Izin Pemasangan Tduk', 'IR-'.substr($obj->tdukTglPengesahan,0,4).'-'.$obj->tdukNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->tdukPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<h4 align="center">
		<u>TANDA DAFTAR USAHA REKREASI DAN HIBURAN UMUM</u><br>
		NOMOR : <?php echo $obj->tdukNoLengkap; ?>
	</h4>
	
	<p style="text-align:justify; text-indent: 40px;">Yang bertanda tangan di bawah ini, Kepala <?php echo strtoupper($objSatuanKerja->skrNama); ?> Kabupaten Pelalawan, berdasarkan kepada :</p>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="3%">1.</td><td width="97%">Undang-undang RI Nomor 32 Tahun 2014;</td>
	</tr>
	<tr>
		<td>2.</td><td>Undang-undang RI Nomor 10 Tahun 2009;</td>
	</tr>
	<tr>
		<td>3.</td><td>Peraturan Menteri Kebudayaan dan  Pariwisata Nomor : PM.91/HK.501/MKP/2010;</td>
	</tr>
	<tr>
		<td>4.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014;</td>
	</tr>
	<tr>
		<td>5.</td><td>Peraturan Bupati Pelalawan Nomor 33 Tahun 2014;</td>
	</tr>
	<tr>
		<td>6.</td><td>Keputusan Bupati Pelalawan Nomor : KPTS.821.2/BKD/2013/674;</td>
	</tr>
	</table>
	
	<p align="center">Dengan ini memberikan Tanda Daftar Usaha Rekreasi dan Hiburan Umum kepada  :</p>
	
	<p align="center" style="font-size:16px;"><b><i><?php echo strtoupper($obj->mhnNama); ?></i></b></p>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td align="left" valign="top" width="28%">Merek / Nama Usaha </td>
		<td align="left" valign="top" width="2%">:</td>
		<td align="left" valign="top" width="70%"><?php echo strtoupper($obj->perNama); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Bidang Usaha</td>
		<td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->tdukBidangPariwisata; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Jenis Usaha</td>
		<td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->tdukJenisPariwisata; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Alamat Usaha</td>
		<td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->tdukLokasiUsaha; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Kelurahan/Desa</td>
		<td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->kelurahan; ?></td>
	</tr>
	<tr>
		<td align="left" valign="top">Kecamatan</td>
		<td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo $obj->kecamatan; ?></td>
	</tr>
	</table>
	
	<p>Dengan Ketentuan sebagai :</p>
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="3%">a.</td><td width="97%">Pengusaha yang bersangkutan harus menjaga kebersihan, keindahan ketertiban tempat usaha dan menjaga keamanan lingkungan.</td>
	</tr>
	<tr>
		<td>b.</td><td>Pengusahan yang bersangkutan harus mematuhi ketentuan/petunjuk yang ada hubungannya dengan kegiatan usahanya.</td>
	</tr>
	<tr>
		<td>c.</td><td>Pengusaha yang bersangkutan wajib memelihara kelestarian lingkungan hidup yang serasi dan seimbang.</td>
	</tr>
	<tr>
		<td>d.</td><td>Pengusaha diharapkan dapat menerima tenaga tempatan minimal 60% dari jumlah tenaga kerja.</td>
	</tr>
	<tr>
		<td>e.</td><td>Penyimpangan / kelalaian terhadap ketentuan tersebut diatas, dapat mengakibatkan pembekuan sementara hingga pembatalan Tanda Daftar Usaha Pariwisata dan dan Penanggung Jawab usaha tersebut bertanggung jawab penuh jika terjadi penyimpangan terhadap peraturan perundang-undangan yang berlaku.</td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="45%">&nbsp;</td>
		<td width="55%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->tdukTglPengesahan)); ?>
		</td>
	</tr>
	</table>

	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:8px">
	<tr>
		<td>Tembusan disampaikan kepada :</td>
	</tr>
	<tr>
		<td>1. Dinas Pariwisata, Pemuda dan Olahraga Kabupaten Pelalawan</td>
	</tr>
	<tr>
		<td>2. Arsip</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readTduk($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tduk&task=edit&id=<?php echo $obj->tdukDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Pemasangan Tduk</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=tduk&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=tduk&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdukKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Tduk</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdukBahan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bunyi</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdukBunyi; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Ukuran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdukPanjang; ?> m x <?php echo $obj->tdukLebar; ?> m
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jumlah</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdukJumlah; ?> Unit
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Lokasi Pemasangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdukPemasanganLokasi; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->tdukPemasanganKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$tdukPemasanganKelurahan = $app->queryObject($sql);
		if (!$tdukPemasanganKelurahan) {
			$tdukPemasanganKelurahan = new stdClass();
			$tdukPemasanganKelurahan->kode = '';
			$tdukPemasanganKelurahan->value = '';
		}
?>
				<?php echo $tdukPemasanganKelurahan->value; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Lama Pemasangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->tdukPemasanganTglMulai); ?> s.d. <?php echo $app->MySQLDateToNormal($obj->tdukPemasanganTglSampai); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdukNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->tdukTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->tdukPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdukTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdukTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdukTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdukTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jstduk/read.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewTduk($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tduk"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Pemasangan Tduk</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('tdukNo', 'Nomor Surat', true),
			$app->setHeader('tdukTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('tdukTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('tdukTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=tduk&task=read&id='.$v->tdukDmhnID.'&fromact=tduk" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->tdukDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=tduk&task=read&id='.$v->tdukDmhnID.'&fromact=tduk" title="Lihat Surat Izin">'.$v->tdukNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->tdukTglPengesahan).'</td>';
				
				if ($v->tdukTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->tdukTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->tdukTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->tdukTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/jstduk/view.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>