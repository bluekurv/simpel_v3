<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.tduk.php';
require_once 'view.tduk.php';

switch ($app->task) {
	case 'add':
		addTduk($app->id);
		break;
	case 'edit':
		editTduk($app->id);
		break;
	case 'delete':
		deleteTduk($app->id);
		break;
	case 'print':
		printTduk($app->id);
		break;
	case 'read':
		readTduk($app->id);
		break;
	case 'save':
		saveTduk();
		break;
	default:
		viewTduk(true, '');
		break;
}

function addTduk($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	//Dengan catatan
	//dmhnMhnID sama
	//dmhnPerID sama
	//dmhnJsiID mengikuti jsiKode='tduk'
	$objTduk = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='tduk'");
	$objDetailPermohonan->dmhnJsiID = $objTduk->jsiID;
	//dmhnTipe sama
	//dmhnBiayaLeges sama
	//dmhnBiayaAdministrasi sama
	//dmhnPola sama
	//dmhnPerluSurvey sama
	//dmhnTglTargetSelesai sama
	//dmhnDiadministrasikanOleh sama
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editTduk($id);
}

function editTduk($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objTduk = $app->queryObject("SELECT * FROM tduk WHERE tdukDmhnID='".$id."'");
	if (!$objTduk) {
		$objTduk = new Tduk_Model();
		
		//Ambil dari pariwisata
		$objPariwisata = $app->queryObject("SELECT * FROM pariwisata, detailpermohonan WHERE pariwisataDmhnID=dmhnID AND dmhnMhnID='".$objDetailPermohonan->dmhnMhnID."'");
		if ($objPariwisata) {
			$objTduk->tdukNPWPD = $objPariwisata->pariwisataNPWPD;
			$objTduk->tdukJparID = $objPariwisata->pariwisataJparID;
			$objTduk->tdukBushID = $objPariwisata->pariwisataBushID;
			$objTduk->tdukMerekUsahaPerusahaan = $objPariwisata->pariwisataMerekUsahaPerusahaan;
			$objTduk->tdukLuasUsahaPanjang = $objPariwisata->pariwisataLuasUsahaPanjang;
			$objTduk->tdukLuasUsahaLebar = $objPariwisata->pariwisataLuasUsahaLebar;
			$objTduk->tdukLokasiUsaha = $objPariwisata->pariwisataLokasiUsaha;
			$objTduk->tdukKelurahanID = $objPariwisata->pariwisataKelurahanID;
			
			$objTduk->tdukKeterangan = $objPariwisata->pariwisataKeterangan;
			$objTduk->tdukTglBerlaku = $objPariwisata->pariwisataTglBerlaku;
			$objTduk->tdukTglDaftarUlang = $objPariwisata->pariwisataTglDaftarUlang;
			$objTduk->tdukPejID = $objPariwisata->pariwisataPejID;
			$objTduk->tdukTglPengesahan = $objPariwisata->pariwisataTglPengesahan;
			$objTduk->tdukArsip = $objPariwisata->pariwisataArsip;
		}
	}
	
	$objTduk->tdukDmhnID = $objDetailPermohonan->dmhnID;
	
	Tduk_View::editTduk(true, '', $objDetailPermohonan, $objTduk);
}

function deleteTduk($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM tduk WHERE tdukDmhnID='".$id."'");
		
	$success = true;
	$msg = 'TDUK berhasil dihapus';
		
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'TDUK', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function printTduk($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN tduk ON tdukDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objTduk = $app->queryObject($sql);
	if (!$objTduk) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objTduk->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objTduk->perKelurahanID."'");
	$objTduk->kelurahan = $objKelurahan->wilNama;
	
	$objKecamatan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");
	$objTduk->kecamatan = $objKecamatan->wilNama;
	
	$objTduk->kabupaten = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKecamatan->wilParentID."'");
	
	Tduk_View::printTduk($objTduk);
}

function readTduk($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN tduk ON tdukDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objTduk = $app->queryObject($sql);
	if (!$objTduk) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objTduk->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Tduk_View::readTduk($objTduk);
}

function saveTduk() {
	global $app;
	
	//Get object
	$objTdukSebelumnya = $app->queryObject("SELECT * FROM tduk WHERE tdukID='".$app->getInt('tdukID')."'");
	if (!$objTdukSebelumnya) {
		$objTdukSebelumnya = new Tduk_Model();
	}
	
	//Create object
	$objTduk = new Tduk_Model();
	$app->bind($objTduk);
	
	//Modify object (if necessary)
	$objTduk->tdukTglPengesahan = $app->NormalDateToMySQL($objTduk->tdukTglPengesahan);
	if ($objTduk->tdukTglPengesahan != '0000-00-00') {
		if ($objTdukSebelumnya->tdukNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objTduk->tdukNo = intval($app->queryField("SELECT MAX(tdukNo) AS nomor FROM tduk WHERE YEAR(tdukTglPengesahan)='".substr($objTduk->tdukTglPengesahan,0,4)."'")) + 1;
		}
			
		/*if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objTduk->tdukTglBerlaku = date('Y-m-d', strtotime($objTduk->tdukTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objTduk->tdukTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objTduk->tdukTglDaftarUlang = date('Y-m-d', strtotime($objTduk->tdukTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objTduk->tdukTglDaftarUlang = '0000-00-00';
		}*/
		$objTduk->tdukTglBerlaku = $app->NormalDateToMySQL($objTduk->tdukTglBerlaku);
		$objTduk->tdukTglDaftarUlang = $app->NormalDateToMySQL($objTduk->tdukTglDaftarUlang);
	} else {
		$objTduk->tdukNo = '';
		
		$objTduk->tdukTglBerlaku = '0000-00-00';
		$objTduk->tdukTglDaftarUlang = '0000-00-00';
	}
	 
	//Dapatkan nomor lengkap sebelum validasi
	$objTduk->tdukNoLengkap = '435/'.CFG_COMPANY_SHORT_NAME.'/TDU-RH/'.substr($objTduk->tdukTglPengesahan,0,4).'/'.$objTduk->tdukNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
			
	//Jika nomor urut bisa diubah
	if ($objTduk->tdukNo > 0){
		$isExist = $app->isExist("SELECT tdukID AS id FROM tduk WHERE tdukNo='".$objTduk->tdukNo."' AND YEAR(tdukTglPengesahan)='".substr($objTduk->tdukTglPengesahan,0,4)."'", $objTduk->tdukID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objTduk->tdukNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objTduk->tdukID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objTduk);
		} else {
			$sql = $app->createSQLforUpdate($objTduk);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objTduk->tdukID = $app->queryField("SELECT LAST_INSERT_ID() FROM tduk");
		}
		
		$success = true;
		$msg = 'TDUK berhasil disimpan';
		
		$app->writeLog(EV_INFORMASI, 'tduk', $objTduk->tdukID, $objTduk->tdukNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objTduk->tdukDmhnID."'");
	} else {
		$success = false;
		$msg = 'TDUK tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objTduk->tdukDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Tduk_View::editTduk($success, $msg, $objDetailPermohonan, $objTduk);
	}
}

function viewTduk($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('tduk', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('tduk', 'sort', 'tdukTglPengesahan');
	//$dir		= $app->pageVar('tduk', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('tduk', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('tduk', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(tdukNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(tdukTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM tduk
				  LEFT JOIN detailpermohonan ON tdukDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM tduk
			LEFT JOIN detailpermohonan ON tdukDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(tdukTglPengesahan) DESC, tdukNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Tduk_View::viewTduk($success, $msg, $arr);
}
?>