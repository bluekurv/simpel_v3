<?php
class Tduk_Model {	
	public $tabel = 'tduk';
	public $primaryKey = 'tdukID';
	
	public $tdukID = 0;
	public $tdukDmhnID = 0;
	public $tdukGrupID = 0;
	public $tdukNo = 0;
	public $tdukNoLengkap = '';

	public $tdukNPWPD = '';
	public $tdukJparID = 0;
	public $tdukBushID = 0;
	public $tdukJenisPariwisata = '';
	public $tdukBidangPariwisata = '';
	public $tdukMerekUsahaPerusahaan = '';
	public $tdukLuasUsahaPanjang = 0;
	public $tdukLuasUsahaLebar = 0;
	public $tdukLokasiUsaha = '';
	public $tdukKelurahanID = 0;
	
	public $tdukKeterangan = '';
	public $tdukTglBerlaku = '0000-00-00';
	public $tdukTglDaftarUlang = '0000-00-00';
	public $tdukPejID = 0;
	public $tdukTglPengesahan = '0000-00-00'; 	 	 	 	 	
	public $tdukArsip = '';
}
?>