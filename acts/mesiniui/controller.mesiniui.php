<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.mesiniui.php';
require_once 'view.mesiniui.php';

switch ($app->task) {
	case 'add':
		addMesinIui($app->id);
		break;
	case 'edit':
		editMesinIui($app->id);
		break;
	case 'delete':
		deleteMesinIui($app->id);
		break;
	case 'save':
		saveMesinIui();
		break;
	default:
		viewMesinIui($app->success, $app->msg, $app->id);
		break;
}

function addMesinIui($id) {
	global $app;
	
	//Get object
	$objIui = $app->queryObject("SELECT * FROM iui WHERE iuiID='".$id."'");
	if (!$objIui) {
		$app->showPageError();
		exit();
	}

	$objMesinIui = new MesinIui_Model();
	$objMesinIui->msnIuiID = $id;
	
	MesinIui_View::editMesinIui(true, '', $objMesinIui, $objIui);
}

function deleteMesinIui($id) {
	global $app;
	
	//Get object
	$objMesinIui = $app->queryObject("SELECT * FROM mesiniui WHERE msnID='".$id."'");
	if (!$objMesinIui) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM mesiniui WHERE msnID='".$id."'");
		
		$success = true;
		$msg = 'Mesin/Peralatan Produksi "'.$objMesinIui->msnJenis.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'mesiniui', $objMesinIui->msnID, $objMesinIui->msnJenis, $msg);
	} else {
		$success = false;
		$msg = 'Mesin/Peralatan Produksi "'.$objMesinIui->msnJenis.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewMesinIui($success, $msg, $objMesinIui->msnIuiID);
}

function editMesinIui($id) {
	global $app;
	
	//Query
	$objMesinIui = $app->queryObject("SELECT * FROM mesiniui WHERE msnID='".$id."'");
	if (!$objMesinIui) {
		$app->showPageError();
		exit();
	}
	
	//Get object
	$objIui = $app->queryObject("SELECT * FROM iui WHERE iuiID='".$objMesinIui->msnIuiID."'");
	if (!$objIui) {
		$app->showPageError();
		exit();
	}

	MesinIui_View::editMesinIui(true, '', $objMesinIui, $objIui);
}

function saveMesinIui() {
	global $app;
	
	//Create object
	$objMesinIui = new MesinIui_Model();
	$app->bind($objMesinIui);
	
	//Modify object (if necessary)
	$objMesinIui->msnJumlah = $app->MoneyToMySQL($objMesinIui->msnJumlah);
	$objMesinIui->msnKapasitasMesin = $app->MoneyToMySQL($objMesinIui->msnKapasitasMesin);
	$objMesinIui->msnHarga = $app->MoneyToMySQL($objMesinIui->msnHarga);
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objMesinIui->msnJenis == '') {
		$doSave = false;
		$msg[] = '- Jenis belum diisi';
	}
	
	$isExist = $app->isExist("SELECT msnID AS id FROM mesiniui WHERE msnJenis='".$objMesinIui->msnJenis."' AND msnIuiID='".$objMesinIui->msnIuiID."'", $objMesinIui->msnID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Jenis "'.$objMesinIui->msnJenis.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objMesinIui->msnID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objMesinIui);
		} else {
			$sql = $app->createSQLforUpdate($objMesinIui);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objMesinIui->msnID = $app->queryField("SELECT LAST_INSERT_ID() FROM mesiniui");
			
			$success = true;
			$msg = 'Mesin/Peralatan Produksi "'.$objMesinIui->msnJenis.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'mesiniui', $objMesinIui->msnID, $objMesinIui->msnJenis, $msg);
		} else {
			$success = true;
			$msg = 'Mesin/Peralatan Produksi "'.$objMesinIui->msnJenis.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'mesiniui', $objMesinIui->msnID, $objMesinIui->msnJenis, $msg);
		}
	} else {
		$success = false;
		$msg = 'Mesin/Peralatan Produksi "'.$objMesinIui->msnJenis.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewMesinIui($success, $msg, $objMesinIui->msnIuiID);
	} else {
		//Get object
		$objIui = $app->queryObject("SELECT * FROM iui WHERE iuiID='".$objMesinIui->msnIuiID."'");
		if (!$objIui) {
			$app->showPageError();
			exit();
		}
		
		MesinIui_View::editMesinIui($success, $msg, $objMesinIui, $objIui);
	}
}

function viewMesinIui($success, $msg, $id) {
	global $app;
	
	//Query
	$objIui = $app->queryObject("SELECT * FROM iui WHERE iuiID='".$id."'");
	if (!$objIui) {
		$app->showPageError();
		exit();
	}

	//Get variable(s)
	$page 		= $app->pageVar('mesiniui', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('mesiniui', 'sort', 'msnJenis');
	$dir   		= $app->pageVar('mesiniui', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('mesiniui', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "msnJenis LIKE '%".$nama."%'";
	}
	$filter[] = "msnIuiID='".$id."'";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM mesiniui
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM mesiniui
			".$where." 
			ORDER BY ".$sort." ".$dir.", msnJenis ASC 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	MesinIui_View::viewMesinIui($success, $msg, $arr, $objIui);
}
?>