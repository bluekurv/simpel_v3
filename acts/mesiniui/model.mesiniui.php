<?php
class MesinIui_Model {	
	public $tabel = 'mesiniui';
	public $primaryKey = 'msnID';
	
	public $msnID = 0;
	public $msnIuiID = 0;
	public $msnJenis = '';
	public $msnJumlah = 0;
	public $msnMerek = '';
	public $msnSpesifikasiNegaraAsal = '';
	public $msnKapasitasMesin = 0.00;
	public $msnSatuanKapasitasMesin = '';
	public $msnHarga = 0;
}
?>