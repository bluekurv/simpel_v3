<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'tanamanhias';
$childTitle = 'Izin Operasional Tanaman Hias';
$childKodeSurat = 'IOP-TH';
$childTembusan = 'Kepala Dinas Tata Kota, Pertamanan dan Kebersihan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>