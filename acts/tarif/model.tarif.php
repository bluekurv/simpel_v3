<?php
class Tarif_Model {	
	public $tabel = 'tarif';
	public $primaryKey = 'trfID';
	
	public $trfID = 0;
	public $trfRekID = 0;
	public $trfNama = '';
	public $trfBawah = 1;
	public $trfAtas = 1;
	public $trfFleksibel = 0;
	public $trfHargaSatuan = 0;
	public $trfFaktorPengali = 1.000;
	public $trfHitungPerVolume = 0;
}
?>