<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app, $id;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.tarif.php';
require_once 'view.tarif.php';

switch ($app->task) {
	case 'add':
		addTarif($app->id);
		break;
	case 'edit':
		editTarif($app->id);
		break;
	case 'delete':
		deleteTarif($app->id);
		break;
	case 'save':
		saveTarif();
		break;
	default:
		viewTarif(true, '', $app->id);
		break;
}

function deleteTarif($id) {
	global $app;
	
	//Get object
	$objTarif = $app->queryObject("SELECT * FROM tarif WHERE trfID='".$id."'");
	if (!$objTarif) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM tarif WHERE trfID='".$id."'");
		
		$success = true;
		$msg = 'Tarif "'.$objTarif->trfNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'tarif', $objTarif->trfID, $objTarif->trfNama, $msg);
	} else {
		$success = false;
		$msg = 'Tarif "'.$objTarif->trfNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewTarif($success, $msg, $objTarif->trfRekID);
}

function addTarif($id) {
	$objTarif = new Tarif_Model();
	$objTarif->trfRekID = $id;
	
	Tarif_View::editTarif(true, '', $objTarif);
}

function editTarif($id) {
	global $app;
	
	$objTarif = $app->queryObject("SELECT * FROM tarif WHERE trfID='".$id."'");
	if (!$objTarif) {
		$app->showPageError();
		exit();
	}

	Tarif_View::editTarif(true, '', $objTarif);
}

function saveTarif() {
	global $app;
	
	//Create object
	$objTarif = new Tarif_Model();
	$app->bind($objTarif);
	
	//Modify object (if necessary)
	$objTarif->trfBawah = $app->MoneyToMySQL($objTarif->trfBawah);
	if ($_REQUEST['sd'] == '') {
		$objTarif->trfAtas = $objTarif->trfBawah;
	} else {
		$objTarif->trfAtas = $app->MoneyToMySQL($objTarif->trfAtas);
		
		if ($objTarif->trfBawah > $objTarif->trfAtas) {
			$temp = $objTarif->trfBawah;
			$objTarif->trfBawah = $objTarif->trfAtas;
			$objTarif->trfAtas = $temp;
		}
	}
	$objTarif->trfHargaSatuan = $app->MoneyToMySQL($objTarif->trfHargaSatuan);
	if (isset($_REQUEST['trfFleksibel'])) {
		$objTarif->trfFleksibel = ($_REQUEST['trfFleksibel'] == 'on') ? 1 : 0;
	} else {
		$objTarif->trfFleksibel = 0;
	}
	if (isset($_REQUEST['trfHitungPerVolume'])) {
		$objTarif->trfHitungPerVolume = ($_REQUEST['trfHitungPerVolume'] == 'on') ? 1 : 0;
	} else {
		$objTarif->trfHitungPerVolume = 0;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objTarif->trfNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT trfID AS id FROM tarif WHERE trfNama='".$objTarif->trfNama."' AND trfRekID='".$objTarif->trfRekID."'", $objTarif->trfID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama "'.$objTarif->trfNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objTarif->trfID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objTarif);
		} else {
			$sql = $app->createSQLforUpdate($objTarif);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objTarif->trfID = $app->queryField("SELECT LAST_INSERT_ID() FROM tarif");
			
			$success = true;
			$msg = 'Tarif "'.$objTarif->trfNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'tarif', $objTarif->trfID, $objTarif->trfNama, $msg);
		} else {
			$success = true;
			$msg = 'Tarif "'.$objTarif->trfNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'tarif', $objTarif->trfID, $objTarif->trfNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Tarif "'.$objTarif->trfNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewTarif($success, $msg, $objTarif->trfRekID);
	} else {
		Tarif_View::editTarif($success, $msg, $objTarif);
	}
}

function viewTarif($success, $msg, $id) {
	global $app;
	
	$objRekening = $app->queryObject("SELECT * FROM rekening WHERE rekID='".$id."'");
	if (!$objRekening) {
		$app->showPageError();
		exit();
	}
	
	//Get variable(s)
	$page 		= $app->pageVar('tarif', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('tarif', 'sort', 'trfNama');
	$dir   		= $app->pageVar('tarif', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('tarif', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "trfNama LIKE '%".$nama."%'";
	}
	$filter[] = "trfRekID='".$id."'";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM tarif
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM tarif
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Tarif_View::viewTarif($success, $msg, $arr, $objRekening, $id);
}
?>