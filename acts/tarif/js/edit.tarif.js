//Variables
var form = $("#myForm");
var trfNama = $("#trfNama");
var trfBawah = $("#trfBawah");
var trfAtas = $("#trfAtas");
var sd = $("#sd");

if (trfBawah.val() == trfAtas.val()) {
	trfAtas.hide();
} else {
	sd.val('s.d.');
}

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'trfNama' || this.id === undefined) {
		if (trfNama.val().length == 0){
			doSubmit = false;
			trfNama.addClass("error");		
		} else {
			trfNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
trfNama.blur(validateForm);

trfNama.keyup(validateForm);

sd.on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    if (valueSelected == 's.d.') {
    	trfAtas.show();
    } else {
    	trfAtas.hide();
    }
});

form.submit(submitForm);

$("#trfBawah").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#trfAtas").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#trfJumlah").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });

$("#trfFaktorPengali").maskMoney({ thousands:'.', decimal:',', precision:3, defaultZero:true, allowZero:true });

//Set focus
trfNama.focus();