<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Tarif_View {
	static function editTarif($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
<?php 
		if ($app->task == 'add') {
?>
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tarif&task=add&id=<?php echo $obj->trfRekID; ?>"><img src="images/icons/money.png" border="0" /> Tambah Tarif Dasar</a></h1>
<?php 
		} else {
?>
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tarif&task=edit&id=<?php echo $obj->trfID; ?>"><img src="images/icons/money.png" border="0" /> Ubah Tarif Dasar</a></h1>
<?php 
		}
?>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=tarif&task=save" method="POST" >
				<input type="hidden" id="trfID" name="trfID" value="<?php echo $obj->trfID; ?>" />
				<input type="hidden" id="trfRekID" name="trfRekID" value="<?php echo $obj->trfRekID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=tarif&id=<?php echo $obj->trfRekID; ?>">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="170">Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="trfNama" name="trfNama" maxlength="255" size="100" value="<?php echo $obj->trfNama; ?>" />
					</td>
				</tr>
				<tr>
					<td>Jangkauan Volume</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="trfBawah" name="trfBawah" maxlength="255" size="11" value="<?php echo $app->MySQLToMoney($obj->trfBawah); ?>" />
						<select class="box" id="sd" name="sd">
<?php
		foreach (array('', 's.d.') as $v) {
			echo '<option value="'.$v.'">'.$v."</option>\n";
		}
?>
						</select>
						<input class="box" id="trfAtas" name="trfAtas" maxlength="255" size="11" value="<?php echo $app->MySQLToMoney($obj->trfAtas); ?>" />
					</td>
				</tr>
				<tr>
					<td>Hitung berdasarkan Volume</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('trfHitungPerVolume', 1, 'Ya', 0, 'Tidak', $obj->trfHitungPerVolume);
?>
						<div style="float:left; line-height: 30px;">&nbsp;(<i>Jika tidak, maka perhitungannya berdasarkan nilai 1</i>)</div>
					</td>
				</tr>
				<tr>
					<td>Harga Satuan (Rp)</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="trfHargaSatuan" name="trfHargaSatuan" maxlength="255" size="11" value="<?php echo $app->MySQLToMoney($obj->trfHargaSatuan); ?>" />
					</td>
				</tr>
				<tr>
					<td>Faktor Pengali</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="trfFaktorPengali" name="trfFaktorPengali" maxlength="255" size="11" value="<?php echo $app->MySQLToMoney($obj->trfFaktorPengali, 3); ?>" />
					</td>
				</tr>
				<tr>
					<td>Fleksibel</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('trfFleksibel', 1, 'Ya', 0, 'Tidak', $obj->trfFleksibel);
?>
						<div style="float:left; line-height: 30px;">&nbsp;(<i>Jika ya, maka <b>Harga Satuan (Rp)</b> dan <b>Faktor Pengali</b> dapat diubah</i>)</div>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/tarif/js/edit.tarif.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewTarif($success, $msg, $arr, $objRekening, $id) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tarif&id=<?php echo $id; ?>"><img src="images/icons/money.png" border="0" /> Tarif Dasar untuk Rekening Retribusi <?php echo $objRekening->rekNomor.' '.$objRekening->rekNama; ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<input type="hidden" id="id" name="id" value="<?php echo $id; ?>" />
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=tarif&task=add&id=<?php echo $id; ?>">Tambah</a>
				<a class="button-link inline blue" href="index2.php?act=rekening">Kelola Rekening Retribusi</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('trfNama', 'Nama', true),
			$app->setHeader('trfBawah', 'Jangkauan Volume', true, 0, 'right'),
			$app->setHeader('trfHitungPerVolume', 'HItung Volume', true),
			$app->setHeader('trfHargaSatuan', 'Harga Satuan (Rp)', true, 0, 'right'),
			$app->setHeader('trfFaktorPengali', 'Faktor Pengali', true, 0, 'right'),
			$app->setHeader('trfFleksibel', 'Fleksibel', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=tarif&task=edit&id='.$v->trfID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->trfID.', '."'".$v->trfNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=tarif&task=edit&id='.$v->trfID.'">'.$v->trfNama.'</a></td>';
				echo '<td align="right">'.($v->trfBawah == $v->trfAtas ? $app->MySQLToMoney($v->trfBawah) : $app->MySQLToMoney($v->trfBawah).' s.d. '.$app->MySQLToMoney($v->trfAtas)).'</td>';
				echo '<td>'.($v->trfHitungPerVolume == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo '<td align="right">'.$app->MySQLToMoney($v->trfHargaSatuan).'</td>';
				echo '<td align="right">'.$app->MySQLToMoney($v->trfFaktorPengali, 3).'</td>';
				echo '<td>'.($v->trfFleksibel == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="5">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/tarif/js/tarif.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>