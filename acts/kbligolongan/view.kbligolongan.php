<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class KBLIGolongan_View {
	static function editKBLIGolongan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kbligolongan&task=edit&id=<?php echo $obj->kgolID; ?>"><img src="images/icons/database_table.png" border="0" /> <?php echo ($obj->kgolID > 0) ? 'Ubah' : 'Tambah'; ?> Golongan Lapangan Usaha</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=kbligolongan&task=save" method="POST" >
				<input type="hidden" id="kgolID" name="kgolID" value="<?php echo $obj->kgolID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=kbligolongan">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Kategori</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$arrKbli = array();
		$sqlKbli = "SELECT kgpokID AS id, CONCAT(kgpokKode,' - ',kgpokNama) AS nama, kpubID, kpubNama AS publikasi FROM kbligolonganpokok, kblipublikasi WHERE kgpokKpubID=kpubID ORDER BY kpubNama, kgpokKode, kgpokNama";
		$rsKbli = $app->query($sqlKbli);
		if ($rsKbli) {
			while(($objKbli = mysql_fetch_object($rsKbli)) == true){
				$arrKbli[$objKbli->kpubID]['publikasi'] = $objKbli->publikasi;
				$arrKbli[$objKbli->kpubID]['kbli'][] = $objKbli;
			}
		}
?>
						<select class="box" id="kgolKgpokID" name="kgolKgpokID">
<?php 
		if (count($arrKbli) > 0) {
			foreach ($arrKbli as $objKbli) {
				echo '<optgroup label="'.$objKbli['publikasi'].'">';
				if (count($objKbli['kbli']) > 0) {
					foreach ($objKbli['kbli'] as $objKbli2) {
						echo '<option value="'.$objKbli2->id.'"';
						if ($objKbli2->id == $obj->kgolKgpokID) {
							echo ' selected';
						}
						echo ">".(strlen($objKbli2->nama) > 50 ? substr($objKbli2->nama, 0, 50).'...' : $objKbli2->nama)."</option>\n";
					}
				}
				echo '</optgroup>';
			}
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Kode <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="kgolKode" name="kgolKode" maxlength="10" size="10" value="<?php echo $obj->kgolKode; ?>" />
					</td>
				</tr>
				<tr>
					<td valign="top">Nama <span class="wajib">*</span></td><td valign="top">&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="kgolNama" name="kgolNama" cols="80" rows="5"><?php echo $obj->kgolNama; ?></textarea>
					</td>
				</tr>
				<tr>
					<td>Publikasi</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelect('kgolKpubID', "SELECT kpubID AS id, kpubNama AS nama FROM kblipublikasi ORDER BY kpubNama", $obj->kgolKpubID);
?>
					</td>
				</tr>
				<tr>
					<td>Tampil?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('kgolTampil', 1, 'Ya', 0, 'Tidak', $obj->kgolTampil);
?>
					</td>
				</tr>
				<tr>
					<td valign="top">Keterangan</td><td valign="top">&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="kgolKeterangan" name="kgolKeterangan" cols="80" rows="5"><?php echo $obj->kgolKeterangan; ?></textarea>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kbligolongan/js/edit.kbligolongan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewKBLIGolongan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kbligolongan"><img src="images/icons/database_table.png" border="0" /> Golongan Lapangan Usaha</a></h1>
		<?php KBLI_View::menuKBLI('kbligolongan'); ?>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=kbligolongan&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Kode/Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
<?php 
		$arrKbli = array();
		$sqlKbli = "SELECT kgpokID AS id, CONCAT(kgpokKode,' - ',kgpokNama) AS nama, kpubID, kpubNama AS publikasi FROM kbligolonganpokok, kblipublikasi WHERE kgpokKpubID=kpubID ORDER BY kpubNama, kgpokKode, kgpokNama";
		$rsKbli = $app->query($sqlKbli);
		if ($rsKbli) {
			while(($objKbli = mysql_fetch_object($rsKbli)) == true){
				$arrKbli[$objKbli->kpubID]['publikasi'] = $objKbli->publikasi;
				$arrKbli[$objKbli->kpubID]['kbli'][] = $objKbli;
			}
		}
?>
						<select class="box" id="filterparent" name="filterparent">
							<option value="0">--Seluruhnya--</option>
<?php 
		if (count($arrKbli) > 0) {
			foreach ($arrKbli as $objKbli) {
				echo '<optgroup label="'.$objKbli['publikasi'].'">';
				if (count($objKbli['kbli']) > 0) {
					foreach ($objKbli['kbli'] as $objKbli2) {
						echo '<option value="'.$objKbli2->id.'"';
						if ($objKbli2->id == $arr['filter']['parent']) {
							echo ' selected';
						}
						echo ">".(strlen($objKbli2->nama) > 50 ? substr($objKbli2->nama, 0, 50).'...' : $objKbli2->nama)."</option>\n";
					}
				}
				echo '</optgroup>';
			}
		}
?>
						</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['parent'] != $arr['default']['parent']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('kgolKode', 'Kode', true, 40),
			$app->setHeader('kgolNama', 'Nama', true),
			$app->setHeader('kgpokNama', 'Golongan Pokok', true),
			$app->setHeader('kpubNama', 'Publikasi', true),
			$app->setHeader('kgolTampil', 'Tampil', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=kbligolongan&task=edit&id='.$v->kgolID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->kgolID.', '."'".$v->kgolNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=kbligolongan&task=edit&id='.$v->kgolID.'">'.$v->kgolKode.'</a></td>';
				echo '<td>'.$v->kgolNama.'</td>';
				echo '<td>'.$v->kgpokNama.'</td>';
				echo '<td>'.$v->kpubNama.'</td>';
				echo '<td>'.($v->kgolTampil == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kbligolongan/js/kbligolongan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>