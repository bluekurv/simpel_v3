//Variables
var form = $("#myForm");
var kgolKgpokID = $("#kgolKgpokID");
var kgolKode = $("#kgolKode");
var kgolNama = $("#kgolNama");
var kgolKpubID = $("#kgolKpubID");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'kgolKgpokID' || this.id === undefined) {
		if (kgolKgpokID.val() == 0){
			doSubmit = false;
			kgolKgpokID.addClass("error");		
		} else {
			kgolKgpokID.removeClass("error");
		}
	}
	
	if (this.id == 'kgolKode' || this.id === undefined) {
		if (kgolKode.val().length == 0){
			doSubmit = false;
			kgolKode.addClass("error");		
		} else {
			kgolKode.removeClass("error");
		}
	}
	
	if (this.id == 'kgolNama' || this.id === undefined) {
		if (kgolNama.val().length == 0){
			doSubmit = false;
			kgolNama.addClass("error");		
		} else {
			kgolNama.removeClass("error");
		}
	}
	
	if (this.id == 'kgolKpubID' || this.id === undefined) {
		if (kgolKpubID.val() == 0){
			doSubmit = false;
			kgolKpubID.addClass("error");		
		} else {
			kgolKpubID.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
kgolKgpokID.blur(validateForm);
kgolKode.blur(validateForm);
kgolNama.blur(validateForm);
kgolKpubID.blur(validateForm);

kgolKgpokID.keyup(validateForm);
kgolKode.keyup(validateForm);
kgolNama.keyup(validateForm);
kgolKpubID.keyup(validateForm);

form.submit(submitForm);

//Set focus
kgolKgpokID.focus();