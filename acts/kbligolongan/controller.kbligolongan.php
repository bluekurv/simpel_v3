<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kbligolongan.php';
require_once 'view.kbligolongan.php';
require_once CFG_ABSOLUTE_PATH.'/acts/kbli/view.kbli.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKBLIGolongan($app->id);
		break;
	case 'delete':
		deleteKBLIGolongan($app->id);
		break;
	case 'save':
		saveKBLIGolongan();
		break;
	default:
		viewKBLIGolongan(true, '');
		break;
}

function deleteKBLIGolongan($id) {
	global $app;
	
	//Get object
	$objKBLIGolongan = $app->queryObject("SELECT * FROM kbligolongan WHERE kgolID='".$id."'");
	if (!$objKBLIGolongan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalKBLSubGolongan = intval($app->queryField("SELECT COUNT(*) AS total FROM kblisubgolongan WHERE ksubKgolID='".$id."'"));
	if ($totalKBLSubGolongan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLSubGolongan.' sub golongan untuk golongan tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM kbligolongan WHERE kgolID='".$id."'");
		
		$success = true;
		$msg = 'Golongan Lapangan Usaha "'.$objKBLIGolongan->kgolNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'kbligolongan', $objKBLIGolongan->kgolID, $objKBLIGolongan->kgolNama, $msg);
	} else {
		$success = false;
		$msg = 'Golongan Lapangan Usaha "'.$objKBLIGolongan->kgolNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKBLIGolongan($success, $msg);
}

function editKBLIGolongan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKBLIGolongan = $app->queryObject("SELECT * FROM kbligolongan WHERE kgolID='".$id."'");
		if (!$objKBLIGolongan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKBLIGolongan = new KBLIGolongan_Model();
		
		$objKBLIGolongan->kgolKgpokID = $_SESSION['kbligolongan-filterparent'];
		$objKBLIGolongan->kgolKpubID = intval($app->queryField("SELECT kgolKpubID FROM kbligolongan WHERE kgolID='".$_SESSION['kbligolongan-filterparent']."'"));
	}
	
	KBLIGolongan_View::editKBLIGolongan(true, '', $objKBLIGolongan);
}

function saveKBLIGolongan() {
	global $app;
	
	//Create object
	$objKBLIGolongan = new KBLIGolongan_Model();
	$app->bind($objKBLIGolongan);
	
	//Modify object (if necessary)
	if (isset($_REQUEST['kgolTampil'])) {
		$objKBLIGolongan->kgolTampil = ($_REQUEST['kgolTampil'] == 'on') ? 1 : 0;
	} else {
		$objKBLIGolongan->kgolTampil = 0;
	}
	//TODO: specific
	//NOTE: karena sudah mysql_escape_string sebelumnya, maka bukan "\r\n" tetapi '\r\n'
	$objKBLIGolongan->kgolNama = str_replace(array('\r\n', '\n', '\r'), ' ', ucwords(strtolower($objKBLIGolongan->kgolNama)));
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKBLIGolongan->kgolKgpokID == 0) {
		$doSave = false;
		$msg[] = '- Kategori belum diisi';
	}
	
	if ($objKBLIGolongan->kgolKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKBLIGolongan->kgolNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objKBLIGolongan->kgolKgpokID == 0) {
		$doSave = false;
		$msg[] = '- Publikasi belum diisi';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKBLIGolongan->kgolID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKBLIGolongan);
		} else {
			$sql = $app->createSQLforUpdate($objKBLIGolongan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKBLIGolongan->kgolID = $app->queryField("SELECT LAST_INSERT_ID() FROM kbligolongan");
			
			$success = true;
			$msg = 'Golongan Lapangan Usaha "'.$objKBLIGolongan->kgolNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'kbligolongan', $objKBLIGolongan->kgolID, $objKBLIGolongan->kgolNama, $msg);
		} else {
			$success = true;
			$msg = 'Golongan Lapangan Usaha "'.$objKBLIGolongan->kgolNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'kbligolongan', $objKBLIGolongan->kgolID, $objKBLIGolongan->kgolNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Golongan Lapangan Usaha "'.$objKBLIGolongan->kgolNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKBLIGolongan($success, $msg);
	} else {
		KBLIGolongan_View::editKBLIGolongan($success, $msg, $objKBLIGolongan);
	}
}

function viewKBLIGolongan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kbligolongan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kbligolongan', 'sort', 'kgolKode');
	$dir   		= $app->pageVar('kbligolongan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'parent' => 0
	);
	
	$nama 		= $app->pageVar('kbligolongan', 'filternama', $default['nama'], 'strval');
	$parent		= $app->pageVar('kbligolongan', 'filterparent', $default['parent'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(kgolKode LIKE '%".$nama."%' OR kgolNama LIKE '%".$nama."%')";
	}
	if ($parent != $default['parent']) {
		$filter[] = "kgolKgpokID='".$parent."'";
	}
	$filter[] = "kgolKgpokID=kgpokID AND kgolKpubID=kpubID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'parent' => $parent
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM kbligolongan, kbligolonganpokok, kblipublikasi
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM kbligolongan, kbligolonganpokok, kblipublikasi
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	KBLIGolongan_View::viewKBLIGolongan($success, $msg, $arr);
}
?>