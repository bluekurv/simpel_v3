//Variables
var form = $("#myForm");
var aduNoKtp = $("#aduNoKtp");
var aduNama = $("#aduNama");

//Functions
function cariNoKtp() {
	if (aduNoKtp.val().length > 0) {
		$.getJSON(
			CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=noktp',  
			{ noktp: aduNoKtp.val() },
			function(json) { 
				if (json.success) {
					var obj = json.obj;
					$('#aduNama').val(obj.mhnNama);
					$('#aduAlamat').val(obj.mhnAlamat);
					$('#aduNoTelp').val(obj.mhnNoTelp);
					
					//Karena keypress pada satu textbox memunculkan indikator error pada textbox2 lainnya, sementara textbox2 tsb tidak kosong, maka divalidasi ulang
					validateForm();
				} else {
					alert(json.msg);
				}
			}  
		);  
	}
}

function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'aduNoKtp' || this.id === undefined) {
		if (aduNoKtp.val().length == 0){
			doSubmit = false;
			aduNoKtp.addClass("error");		
		} else {
			aduNoKtp.removeClass("error");
		}
	}
	
	if (this.id == 'aduNama' || this.id === undefined) {
		if (aduNama.val().length == 0){
			doSubmit = false;
			aduNama.addClass("error");		
		} else {
			aduNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
aduNoKtp.blur(validateForm);
aduNama.blur(validateForm);

aduNoKtp.keyup(validateForm);
aduNama.keyup(validateForm);

aduNoKtp.keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if (keycode == '13'){
		cariNoKtp();	
	}
});

form.submit(submitForm);

//Set focus
aduNoKtp.focus();