<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Pejabat']['all'] = true;
$acl['Petugas Loket Pengaduan']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.pengaduan.php';
require_once 'view.pengaduan.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editPengaduan($app->id);
		break;
	case 'delete':
		deletePengaduan($app->id);
		break;
	case 'read':
		readPengaduan($app->id);
		break;
	case 'save':
		savePengaduan();
		break;
	case 'view':	//Additional task for default
	default:
		viewPengaduan(true, '');
		break;
}

function deletePengaduan($id) {
	global $app;
	
	//Get object
	$objPengaduan = $app->queryObject("SELECT * FROM pengaduan WHERE aduID='".$id."'");
	if (!$objPengaduan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM pengaduan WHERE aduID='".$id."'");
		
		$success = true;
		$msg = 'Pengaduan "'.$objPengaduan->aduNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'pengaduan', $objPengaduan->aduID, $objPengaduan->aduNama, $msg);
	} else {
		$success = false;
		$msg = 'Pengaduan "'.$objPengaduan->aduNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewPengaduan($success, $msg);
}

function editPengaduan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objPengaduan = $app->queryObject("SELECT * FROM pengaduan WHERE aduID='".$id."'");
		if (!$objPengaduan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objPengaduan = new Pengaduan_Model();
		$objPengaduan->aduTgl = date('Y-m-d');
	}
	
	Pengaduan_View::editPengaduan(true, '', $objPengaduan);
}

function readPengaduan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM pengaduan 
			LEFT JOIN permohonan ON mhnNoUrutLengkap=aduMhnNoUrutLengkap
			WHERE aduID='".$id."'";
	$objPengaduan = $app->queryObject($sql);
	if (!$objPengaduan) {
		$app->showPageError();
		exit();
	}
	
	Pengaduan_View::readPengaduan($objPengaduan);
}

function savePengaduan() {
	global $app;
	
	//Create object
	$objPengaduan = new Pengaduan_Model();
	$app->bind($objPengaduan);
	
	//Modify object (if necessary)
	$objPengaduan->aduTgl = $app->NormalDateToMySQL($objPengaduan->aduTgl);
	
	if (isset($_REQUEST['aduTelahSelesai'])) {
		$objPengaduan->aduTelahSelesai = ($_REQUEST['aduTelahSelesai'] == 'on') ? 1 : 0;
	} else {
		$objPengaduan->aduTelahSelesai = 0;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objPengaduan->aduNoKtp == '') {
		$doSave = false;
		$msg[] = '- No. KTP belum diisi';
	}
	
	if ($objPengaduan->aduNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objPengaduan->aduMhnNoUrutLengkap != '') {
		$objPermohonan = $app->queryObject("SELECT * FROM permohonan WHERE mhnNoUrutLengkap='".$objPengaduan->aduMhnNoUrutLengkap."'");
		if (!$objPermohonan) {
			$doSave = false;
			$msg[] = '- No. Urut Permohonan "'.$objPengaduan->aduMhnNoUrutLengkap.'" tidak ditemukan';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objPengaduan->aduID == 0) ? true : false;
		
		if ($doInsert) {
			$objPengaduan->aduDibuatOleh = $_SESSION['sesipengguna']->ID;
			$objPengaduan->aduDibuatPada = date('Y-m-d H:i:s');
			
			$sql = $app->createSQLforInsert($objPengaduan);
		} else {
			$objPengaduan->aduDiubahOleh = $_SESSION['sesipengguna']->ID;
			$objPengaduan->aduDiubahPada = date('Y-m-d H:i:s');
			
			$arrExcludeField = array('aduDibuatOleh', 'aduDibuatPada');
			
			$sql = $app->createSQLforUpdate($objPengaduan, $arrExcludeField);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objPengaduan->aduID = $app->queryField("SELECT LAST_INSERT_ID() FROM pengaduan");
			
			$success = true;
			$msg = 'Pengaduan "'.$objPengaduan->aduNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'pengaduan', $objPengaduan->aduID, $objPengaduan->aduNama, $msg);
		} else {
			$success = true;
			$msg = 'Pengaduan "'.$objPengaduan->aduNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'pengaduan', $objPengaduan->aduID, $objPengaduan->aduNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Pengaduan "'.$objPengaduan->aduNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewPengaduan($success, $msg);
	} else {
		Pengaduan_View::editPengaduan($success, $msg, $objPengaduan);
	}
}

function viewPengaduan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('pengaduan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('pengaduan', 'sort', 'aduTgl');
	$dir   		= $app->pageVar('pengaduan', 'dir', 'desc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'telahselesai' => -1
	);
	
	$nama 		= $app->pageVar('pengaduan', 'filternama', $default['nama'], 'strval');
	$telahselesai 	= $app->pageVar('pengaduan', 'filtertelahselesai', $default['telahselesai'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "aduNama LIKE '%".$nama."%'";
	}
	if ($telahselesai != $default['telahselesai']) {
		$filter[] = "aduTelahSelesai='".$telahselesai."'";
	}
	if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Loket Pengaduan') {
		
	} else if ($_SESSION['sesipengguna']->levelAkses == 'Pejabat') {
		$filter[] = "aduKepadaID='".$_SESSION['sesipengguna']->ID."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'telahselesai' => $telahselesai
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM pengaduan
				  LEFT JOIN permohonan ON aduMhnNoUrutLengkap=mhnNoUrutLengkap
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM pengaduan
			LEFT JOIN permohonan ON aduMhnNoUrutLengkap=mhnNoUrutLengkap
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Pengaduan_View::viewPengaduan2($success, $msg, $arr);
}
?>