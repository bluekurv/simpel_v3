<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Pengaduan_View {
	static function editPengaduan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pengaduan&task=edit&id=<?php echo $obj->aduID; ?>"><img src="images/icons/user_comment.png" border="0" /> <?php echo ($obj->aduID > 0) ? 'Ubah' : 'Tambah'; ?> Pengaduan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=pengaduan&task=save" method="POST" >
				<input type="hidden" id="aduID" name="aduID" value="<?php echo $obj->aduID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=pengaduan">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Tanggal <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date" id="aduTgl" name="aduTgl" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->aduTgl); ?>" />
					</td>
				</tr>
				<tr>
					<td>No. KTP <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="aduNoKtp" name="aduNoKtp" maxlength="50" size="50" value="<?php echo $obj->aduNoKtp; ?>" />
						<a class="button-link inline blue" href="javascript:cariNoKtp()">Cari</a>
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="aduNama" name="aduNama" maxlength="50" size="50" value="<?php echo $obj->aduNama; ?>" />
					</td>
				</tr>
				<tr>
					<td>Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="aduAlamat" name="aduAlamat" maxlength="500" size="100" value="<?php echo $obj->aduAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td>No. Telepon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="aduNoTelp" name="aduNoTelp" maxlength="255" size="50" value="<?php echo $obj->aduNoTelp; ?>" />
					</td>
				</tr>
				<tr>
					<td>No. Urut Permohonan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="aduMhnNoUrutLengkap" name="aduMhnNoUrutLengkap" maxlength="11" size="11" value="<?php echo $obj->aduMhnNoUrutLengkap; ?>" />
						(<i>Jika berkaitan dengan permohonan</i>)
					</td>
				</tr>
				<tr>
					<td valign="top">Isi</td><td valign="top">&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="aduIsi" name="aduIsi" cols="80" rows="5"><?php echo $obj->aduIsi; ?></textarea>
					</td>
				</tr>
				<tr>
					<td>Diteruskan Kepada</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="aduKepadaID" name="aduKepadaID">
		<?php 
				$sql = "SELECT *, pnID AS id 
						FROM pengguna 
						WHERE pnLevelAkses='Pejabat' 
						ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
				$pejabat = $app->queryArrayOfObjects($sql);
				foreach ($pejabat as $v) {
					echo '<option value="'.$v->pnID.'"';
					if ($v->pnID == $obj->aduKepadaID) {
						echo ' selected';
					}
					echo '>'.$v->pnNama.'</option>';
				}
		?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Selesai?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('aduTelahSelesai', 1, 'Sudah', 0, 'Belum', $obj->aduTelahSelesai);
?>
					</td>
				</tr>
				<tr>
					<td valign="top">Tanggapan</td><td valign="top">&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="aduTanggapan" name="aduTanggapan" cols="80" rows="5"><?php echo $obj->aduTanggapan; ?></textarea>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pengaduan/js/edit.pengaduan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function readPengaduan($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pengaduan&task=read&id=<?php echo $obj->aduID; ?>"><img src="images/icons/user_comment.png" border="0" /> Lihat Pengaduan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<table class="readTable" border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td width="150">Tanggal</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $app->MySQLDateToNormal($obj->aduTgl); ?>
				</td>
			</tr>
			<tr>
				<td>No. KTP</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->aduNoKtp; ?>
				</td>
			</tr>
			<tr>
				<td>Nama</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->aduNama; ?>
				</td>
			</tr>
			<tr>
				<td>Alamat</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->aduAlamat; ?>
				</td>
			</tr>
			<tr>
				<td>No. Telepon</td><td>&nbsp;:&nbsp;</td>
				<td>
					<?php echo $obj->aduNoTelp; ?>
				</td>
			</tr>
			<tr>
				<td>No. Urut Permohonan</td><td>&nbsp;:&nbsp;</td>
				<td>
<?php 
		if ($obj->aduMhnNoUrutLengkap != '') {
?>
					<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=pengaduan"><?php echo $obj->aduMhnNoUrutLengkap; ?></a>
<?php 
		} else {
?>
					-
<?php 
		}
?>
				</td>
			</tr>
			<tr>
				<td valign="top">Isi</td><td valign="top">&nbsp;:&nbsp;</td>
				<td valign="top">
					<?php echo $obj->aduIsi; ?>
				</td>
			</tr>
			<tr>
				<td>Selesai?</td><td>&nbsp;:&nbsp;</td>
				<td>
<?php 
		echo ($obj->aduTelahSelesai == 1) ? '<span class="badge ya">Sudah Selesai</span>' : '<span class="badge tidak">Belum Selesai</span>';
?>
				</td>
			</tr>
			<tr>
				<td valign="top">Tanggapan</td><td valign="top">&nbsp;:&nbsp;</td>
				<td valign="top">
					<?php echo ($obj->aduTanggapan != '') ? $obj->aduTanggapan : "-"; ?>
				</td>
			</tr>
			</table>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pengaduan/js/edit.pengaduan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewPengaduan2($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pengaduan"><img src="images/icons/user_comment.png" border="0" /> Pengaduan</a></h1>
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Loket Pengaduan') {
			$pengaduan = intval($app->queryField("SELECT COUNT(*) AS total FROM pengaduan WHERE aduTelahSelesai=0"));
		} else if ($_SESSION['sesipengguna']->levelAkses == 'Pejabat') {
			$pengaduan = intval($app->queryField("SELECT COUNT(*) AS total FROM pengaduan WHERE aduTelahSelesai=0 AND aduKepadaID='".$_SESSION['sesipengguna']->ID."'"));
		}
?>
		<p><?php echo ($pengaduan > 0) ? 'Ada <span style="color:#FF0000;">'.$pengaduan.' pengaduan</span>' : ' Tidak ada pengaduan'; ?> yang belum diselesaikan</p>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Loket Pengaduan') {
?>
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=pengaduan&task=add">Tambah</a>
			</div>
<?php
		}

		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertelahselesai" name="filtertelahselesai">
					<option value="-1">--Seluruhnya--</option>
<?php
		$telahselesai = array(0=>'Belum Selesai', 1=>'Sudah Selesai');
		foreach ($telahselesai as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['filter']['telahselesai']) {
				echo ' selected';
			}
			echo ">".$v."</option>\n";
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['telahselesai'] != $arr['default']['telahselesai']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<div style="padding:5px;">' : '<div class="odd" style="padding:5px;">';
				echo '<p>';
				if ($v->aduTelahSelesai) {
					echo '<span class="badge ya" style="font-size:12px;">Sudah Selesai</span> ';
				} else {
					echo '<span class="badge tidak" style="font-size:12px;">Belum Selesai</span> ';
				}
				if ($_SESSION['sesipengguna']->levelAkses == 'Pejabat' && $v->aduKepadaID == $_SESSION['sesipengguna']->ID) {
					echo '<a href="index2.php?act=pengaduan&task=edit&id='.$v->aduID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> ';
					//echo '<a href="javascript:hapus('.$v->aduID.', '."'No. KTP ".$v->aduNoKtp.', '.$v->aduNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a> ';
					echo '<a href="index2.php?act=pengaduan&task=edit&id='.$v->aduID.'">No. KTP '.$v->aduNoKtp.', '.$v->aduNama.'</a>';
				} else {
					echo '<a href="index2.php?act=pengaduan&task=read&id='.$v->aduID.'">No. KTP '.$v->aduNoKtp.', '.$v->aduNama.'</a>';
				}
				echo '</p>';
				$info = array();
				if ($v->aduAlamat != '') {
					$info[] = $v->aduAlamat;
				}
				if ($v->aduNoTelp != '') {
					$info[] = 'No.Telp '.$v->aduNoTelp;
				}
				if (count($info) > 0) {
					echo '<div>'.implode(', ',$info).'</div>';
				}
				echo '<div>Tgl. Pengaduan '.$app->MySQLDateToNormal($v->aduTgl).'</div>';
				if ($v->mhnNoUrutLengkap != '') {
					echo '<div>Terkait Permohonan No. <a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'&fromact=pengaduan">'.$v->mhnNoUrutLengkap.'</a></div>';
				}
				$kepada = $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$v->aduKepadaID."'");
				echo '<p><b>Isi Pengaduan :</b></p>';
				echo '<div>'.$v->aduIsi.'</div>';
				echo '<p><b>Diteruskan Kepada :</b> '.$kepada.'</p>';
				if ($v->aduTanggapan != '') {
					echo '<p><b>Tanggapan Atas Pengaduan :</b></p>';
					echo '<div>'.$v->aduTanggapan.'</div>';
				} else {
					echo '<p><b>Belum Ditanggapi</b></p>';					
				}
				echo "</div>\n";
				$i++;
			}
		} else {
?>
			<p>Tidak ada data</p>
<?php
		}
?>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pengaduan/js/pengaduan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewPengaduan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pengaduan"><img src="images/icons/user_comment.png" border="0" /> Pengaduan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php 
		if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Loket Pengaduan') {
?>
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=pengaduan&task=add">Tambah</a>
			</div>
<?php
		}
		
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertelahselesai" name="filtertelahselesai">
					<option value="-1">--Seluruhnya--</option>
<?php
		$telahselesai = array(0=>'Belum Selesai', 1=>'Sudah Selesai');
		foreach ($telahselesai as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['filter']['telahselesai']) {
				echo ' selected';
			}
			echo ">".$v."</option>\n";
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['telahselesai'] != $arr['default']['telahselesai']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('aduTgl', 'Tgl', true, 100),
			$app->setHeader('aduNoKtp', 'No. KTP', true),
			$app->setHeader('aduNama', 'Nama', true),
			$app->setHeader('aduAlamat', 'Alamat', true),
			$app->setHeader('aduNoTelp', 'No. Telepon', true),
			$app->setHeader('aduMhnNoUrutLengkap', 'No. Urut Permohonan', true),
			$app->setHeader('aduTelahSelesai', 'Telah Selesai?', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=pengaduan&task=edit&id='.$v->aduID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->aduID.', '."'".$v->aduNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->aduTgl).'</a></td>';
				echo '<td><a href="index2.php?act=pengaduan&task=edit&id='.$v->aduID.'">'.$v->aduNoKtp.'</a></td>';
				echo '<td>'.$v->aduNama.'</td>';
				echo '<td>'.$v->aduAlamat.'</td>';
				echo '<td>'.$v->aduNoTelp.'</td>';
				echo '<td>'.$v->aduMhnNoUrutLengkap.'</td>';
				echo '<td>'.($v->aduTelahSelesai == 1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="8">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pengaduan/js/pengaduan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>