<?php
class Pengaduan_Model {	
	public $tabel = 'pengaduan';
	public $primaryKey = 'aduID';
	
	public $aduID = 0;
	public $aduMhnNoUrutLengkap = '';
	public $aduTgl = '0000-00-00';
	public $aduNoKtp = '';
	public $aduNama = '';
	public $aduAlamat = '';
	public $aduNoTelp = '';
	public $aduIsi = '';
	public $aduKepadaID = 0;
	public $aduTelahSelesai = 0;
	public $aduTanggapan = '';
	public $aduDibuatOleh = 0;
	public $aduDibuatPada = '0000-00-00 00:00:00';
	public $aduDiubahOleh = 0;
	public $aduDiubahPada = '0000-00-00 00:00:00';
}
?>