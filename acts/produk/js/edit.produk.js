//Variables
var form = $("#myForm");
var proKode = $("#proKode");
var proNama = $("#proNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'proKode' || this.id === undefined) {
		if (proKode.val().length == 0){
			doSubmit = false;
			proKode.addClass("error");		
		} else {
			proKode.removeClass("error");
		}
	}
	
	if (this.id == 'proNama' || this.id === undefined) {
		if (proNama.val().length == 0){
			doSubmit = false;
			proNama.addClass("error");		
		} else {
			proNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
proKode.blur(validateForm);
proNama.blur(validateForm);

proKode.keyup(validateForm);
proNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
proKode.focus();