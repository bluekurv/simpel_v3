<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.produk.php';
require_once 'view.produk.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editProduk($app->id);
		break;
	case 'delete':
		deleteProduk($app->id);
		break;
	case 'save':
		saveProduk();
		break;
	default:
		viewProduk(true, '');
		break;
}

function deleteProduk($id) {
	global $app;
	
	//Get object
	$objProduk = $app->queryObject("SELECT * FROM produk WHERE proID='".$id."'");
	if (!$objProduk) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM produk WHERE proID='".$id."'");
		
		$success = true;
		$msg = 'Produk "'.$objProduk->proNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'produk', $objProduk->proID, $objProduk->proNama, $msg);
	} else {
		$success = false;
		$msg = 'Produk "'.$objProduk->proNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewProduk($success, $msg);
}

function editProduk($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objProduk = $app->queryObject("SELECT * FROM produk WHERE proID='".$id."'");
		if (!$objProduk) {
			$app->showPageError();
			exit();
		}
	} else {
		$objProduk = new Produk_Model();
	}
	
	Produk_View::editProduk(true, '', $objProduk);
}

function saveProduk() {
	global $app;
	
	//Create object
	$objProduk = new Produk_Model();
	$app->bind($objProduk);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objProduk->proKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objProduk->proNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT proID AS id FROM produk WHERE proKode='".$objProduk->proKode."'", $objProduk->proID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objProduk->proKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT proID AS id FROM produk WHERE proNama='".$objProduk->proNama."'", $objProduk->proID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objProduk->proNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objProduk->proID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objProduk);
		} else {
			$sql = $app->createSQLforUpdate($objProduk);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objProduk->proID = $app->queryField("SELECT LAST_INSERT_ID() FROM produk");
			
			$success = true;
			$msg = 'Produk "'.$objProduk->proNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'produk', $objProduk->proID, $objProduk->proNama, $msg);
		} else {
			$success = true;
			$msg = 'Produk "'.$objProduk->proNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'produk', $objProduk->proID, $objProduk->proNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Produk "'.$objProduk->proNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewProduk($success, $msg);
	} else {
		Produk_View::editProduk($success, $msg, $objProduk);
	}
}

function viewProduk($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('produk', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('produk', 'sort', 'proKode');
	$dir   		= $app->pageVar('produk', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('produk', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(proKode LIKE '%".$nama."%' OR proNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM produk
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM produk
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Produk_View::viewProduk($success, $msg, $arr);
}
?>