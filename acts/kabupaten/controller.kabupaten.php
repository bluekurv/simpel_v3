<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Loket Pelayanan']['all'] = true;
$acl['Petugas Administrasi']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kabupaten.php';
require_once 'view.kabupaten.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKabupaten($app->id);
		break;
	case 'delete':
		deleteKabupaten($app->id);
		break;
	case 'save':
		saveKabupaten();
		break;
	default:
		viewKabupaten(true, '');
		break;
}

function deleteKabupaten($id) {
	global $app;
	
	//Get object
	$objKabupaten = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$id."' AND (wilTingkat='Kabupaten' OR wilTingkat='Kota')");
	if (!$objKabupaten) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalKecamatan = intval($app->queryField("SELECT COUNT(*) AS total FROM wilayah WHERE wilParentID='".$id."' AND wilTingkat='Kecamatan'"));
	if ($totalKecamatan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKecamatan.' kecamatan pada kabupaten/kota tersebut';
	}
	
	$totalSatuanKerja = intval($app->queryField("SELECT COUNT(*) AS total FROM satuankerja WHERE skrWilID='".$id."'"));
	if ($totalSatuanKerja > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalSatuanKerja.' satuan kerja pada kabupaten/kota tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM wilayah WHERE wilID='".$id."'");
		
		$success = true;
		$msg = $objKabupaten->wilTingkat.' "'.$objKabupaten->wilNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'wilayah', $objKabupaten->wilID, $objKabupaten->wilNama, $msg);
	} else {
		$success = false;
		$msg = $objKabupaten->wilTingkat.' "'.$objKabupaten->wilNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKabupaten($success, $msg);
}

function editKabupaten($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKabupaten = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$id."' AND (wilTingkat='Kabupaten' OR wilTingkat='Kota')");
		if (!$objKabupaten) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKabupaten = new Kabupaten_Model();
	}
	
	Kabupaten_View::editKabupaten(true, '', $objKabupaten);
}

function saveKabupaten() {
	global $app;
	
	//Create object
	$objKabupaten = new Kabupaten_Model();
	$app->bind($objKabupaten);
	
	//Modify object (if necessary)
	if ($objKabupaten->wilTingkat == 'Kota') {
		$objKabupaten->wilIbukota = $objKabupaten->wilNama;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKabupaten->wilKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKabupaten->wilNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objKabupaten->wilParentID == 0) {
		$doSave = false;
		$msg[] = '- Berada di bawah belum diisi';
	}
	
	if ($objKabupaten->wilKode != '00') {
		$isExist = $app->isExist("SELECT wilID AS id FROM wilayah WHERE wilKode='".$objKabupaten->wilKode."' AND (wilTingkat='Kabupaten' OR wilTingkat='Kota') AND wilParentID='".$objKabupaten->wilParentID."'", $objKabupaten->wilID);
		if (!$isExist) {
			$doSave = false;
			$msg[]  = '- Kode "'.$objKabupaten->wilKode.'" sudah ada';
		}
	}
	
	/*$isExist = $app->isExist("SELECT wilID AS id FROM wilayah WHERE wilNama='".$objKabupaten->wilNama."' AND (wilTingkat='Kabupaten' OR wilTingkat='Kota') AND wilParentID='".$objKabupaten->wilParentID."'", $objKabupaten->wilID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama "'.$objKabupaten->wilNama.'" sudah ada';
	}*/
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKabupaten->wilID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKabupaten);
		} else {
			$sql = $app->createSQLforUpdate($objKabupaten);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKabupaten->wilID = $app->queryField("SELECT LAST_INSERT_ID() FROM wilayah");
			
			$success = true;
			$msg = $objKabupaten->wilTingkat.' "'.$objKabupaten->wilNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'wilayah', $objKabupaten->wilID, $objKabupaten->wilNama, $msg);
		} else {
			$success = true;
			$msg = $objKabupaten->wilTingkat.' "'.$objKabupaten->wilNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'wilayah', $objKabupaten->wilID, $objKabupaten->wilNama, $msg);
		}
	} else {
		$success = false;
		$msg = $objKabupaten->wilTingkat.' "'.$objKabupaten->wilNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKabupaten($success, $msg);
	} else {
		Kabupaten_View::editKabupaten($success, $msg, $objKabupaten);
	}
}

function viewKabupaten($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kabupaten', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kabupaten', 'sort', 'kode');
	$dir   		= $app->pageVar('kabupaten', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'parent' => 0
	);
	
	$nama 		= $app->pageVar('kabupaten', 'filternama', $default['nama'], 'strval');
	$parent 	= $app->pageVar('kabupaten', 'filterparent', $default['parent'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "wilayah.wilNama LIKE '%".$nama."%'";
	}
	if ($parent > $default['parent']) {
		$filter[] = "wilayah.wilParentID='".$parent."'";
	}
	$filter[] = "(wilayah.wilTingkat='Kabupaten' OR wilayah.wilTingkat='Kota')";
	$filter[] = "wilayah.wilParentID=wilayah2.wilID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'parent' => $parent
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM wilayah, wilayah AS wilayah2
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT 
				wilayah.*, 
				CONCAT(wilayah2.wilKode,'.',wilayah.wilKode) AS kode, CONCAT(wilayah2.wilTingkat,' ',wilayah2.wilNama) AS parent,
				(SELECT COUNT(*) AS total FROM wilayah AS sub WHERE sub.wilParentID=wilayah.wilID AND sub.wilTingkat='Kecamatan') AS total
			FROM wilayah, wilayah AS wilayah2
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Kabupaten_View::viewKabupaten($success, $msg, $arr);
}
?>