//Variables
var form = $("#myForm");
var wilParentID = $("#wilParentID");
var wilTingkat = $("#wilTingkat");
var wilNama = $("#wilNama");
var wilIbukota = $("#wilIbukota");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'wilParentID' || this.id === undefined) {
		if (wilParentID.val() == 0){
			doSubmit = false;
			wilParentID.addClass("error");		
		} else {
			wilParentID.removeClass("error");
		}
	}
	
	if (this.id == 'wilNama' || this.id === undefined) {
		if (wilNama.val().length == 0){
			doSubmit = false;
			wilNama.addClass("error");		
		} else {
			wilNama.removeClass("error");
		}
	}
	
	if (wilTingkat.val() == 'Kabupaten') {
		if (this.id == 'wilIbukota' || this.id === undefined) {
			if (wilIbukota.val().length == 0){
				doSubmit = false;
				wilIbukota.addClass("error");		
			} else {
				wilIbukota.removeClass("error");
			}
		}
	} else {
		wilIbukota.removeClass("error");
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
wilParentID.blur(validateForm);
wilNama.blur(validateForm);
wilIbukota.blur(validateForm);

wilParentID.keyup(validateForm);
wilNama.keyup(validateForm);
wilIbukota.keyup(validateForm);

wilTingkat.on('change', function (e) {
    var optionSelected = $("option:selected", this);
    var valueSelected = this.value;
    if (valueSelected == 'Kota') {
    	$('#trIbukota').hide();
    } else {
    	$('#trIbukota').show();
    }
});

form.submit(submitForm);

if (wilTingkat.val() == 'Kota') {
	$('#trIbukota').hide();
} else {
	$('#trIbukota').show();
}

//Set focus
wilParentID.focus();