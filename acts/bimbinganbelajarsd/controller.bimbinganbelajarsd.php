<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'bimbinganbelajarsd';
$childTitle = 'Izin Operasional Bimbingan Belajar SD';
$childKodeSurat = 'IOP-BIMSD';
$childTembusan = 'Kepala Dinas Pendidikan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>