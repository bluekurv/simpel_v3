<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Air_View {
	static function editAir($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=airpermukaan&task=edit&id=<?php echo $obj->airDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Izin Pengambilan Air yang Bersumber dari Air Permukaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=airpermukaan&task=save" method="POST" >
				<input type="hidden" id="airID" name="airID" value="<?php echo $obj->airID; ?>" />
				<input type="hidden" id="airDmhnID" name="airDmhnID" value="<?php echo $obj->airDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('airGrupID', $obj->airGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=airpermukaan&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Berdasarkan Surat Permohonan dari</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=airpermukaan&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->perAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				
				<tr>
					<td style="padding-left:20px;">No.</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airPermohonanNo" name="airPermohonanNo" maxlength="255" size="50" value="<?php echo $obj->airPermohonanNo; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl.</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="airPermohonanTgl" name="airPermohonanTgl" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->airPermohonanTgl); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Perihal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airPermohonanPerihal" name="airPermohonanPerihal" maxlength="255" size="100" value="<?php echo $obj->airPermohonanPerihal; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Surat Tugas dari Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan </u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No.</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airTugasNo" name="airTugasNo" maxlength="255" size="50" value="<?php echo $obj->airTugasNo; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl.</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="airTugasTgl" name="airTugasTgl" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->airTugasTgl); ?>">
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Untuk</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keperluan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airKeterangan" name="airKeterangan" maxlength="255" size="100" value="<?php echo $obj->airKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jumlah Titik Pengambilan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box bulat" id="airJumlahSumur" name="airJumlahSumur" maxlength="3" size="3" value="<?php echo $obj->airJumlahSumur; ?>" /> titik
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Sumber Air</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airSumber" name="airSumber" maxlength="255" size="100" value="<?php echo $obj->airSumber; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Titik Koordinat</td><td>&nbsp;:&nbsp;</td>
					<td>
						N 
						<input class="box" id="airKoordinatNDerajat" name="airKoordinatNDerajat" maxlength="10" size="3" value="<?php echo $obj->airKoordinatNDerajat; ?>" />&deg;
						<input class="box" id="airKoordinatNMenit" name="airKoordinatNMenit" maxlength="10" size="3" value="<?php echo $obj->airKoordinatNMenit; ?>" />'
						<input class="box" id="airKoordinatNDetik" name="airKoordinatNDetik" maxlength="10" size="5" value="<?php echo $obj->airKoordinatNDetik; ?>" />"
						<br>
						E 
						<input class="box" id="airKoordinatEDerajat" name="airKoordinatEDerajat" maxlength="10" size="3" value="<?php echo $obj->airKoordinatEDerajat; ?>" />&deg;
						<input class="box" id="airKoordinatEMenit" name="airKoordinatEMenit" maxlength="10" size="3" value="<?php echo $obj->airKoordinatEMenit; ?>" />'
						<input class="box" id="airKoordinatEDetik" name="airKoordinatEDetik" maxlength="10" size="5" value="<?php echo $obj->airKoordinatEDetik; ?>" />"
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Elevasi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airElevasi" name="airElevasi" maxlength="255" size="15" value="<?php echo $obj->airElevasi; ?>" /> meter
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kedalaman</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airKedalaman" name="airKedalaman" maxlength="255" size="15" value="<?php echo $obj->airKedalaman; ?>" /> meter
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Letak Pompa</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airLetakPompa" name="airLetakPompa" maxlength="255" size="50" value="<?php echo $obj->airLetakPompa; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pompa Yang Digunakan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<table border="0">
						<tr>
							<td>Nama Merek</td><td><input class="box" id="airMerekPompa" name="airMerekPompa" maxlength="255" size="50" value="<?php echo $obj->airMerekPompa; ?>" /></td>
						</tr>
						<tr>
							<td>Type</td><td><input class="box" id="airTipePompa" name="airTipePompa" maxlength="255" size="50" value="<?php echo $obj->airTipePompa; ?>" /></td>
						</tr>
						<tr>
							<td>Kapasitas</td>
							<td>
								<input class="box bulat" id="airKapasitasPompa" name="airKapasitasPompa" maxlength="11" size="5" value="<?php echo $obj->airKapasitasPompa; ?>" />
								<input class="box" id="airKapasitasPompaSatuan" name="airKapasitasPompaSatuan" maxlength="255" size="15" value="<?php echo $obj->airKapasitasPompaSatuan; ?>" />
							</td>
						</tr>
						<tr>
							<td>Head M</td>
							<td>
								<input class="box" id="airHeadPompa" name="airHeadPompa" maxlength="255" size="5" value="<?php echo $obj->airHeadPompa; ?>" />
								<input class="box" id="airHeadPompaSatuan" name="airHeadPompaSatuan" maxlength="255" size="15" value="<?php echo $obj->airHeadPompaSatuan; ?>" />
							</td>
						</tr>
						<!-- <tr>
							<td>Daya</td>
							<td>
								<input class="box" id="airDayaPompa" name="airDayaPompa" maxlength="255" size="5" value="<?php echo $obj->airDayaPompa; ?>" />
								<input class="box" id="airDayaPompaSatuan" name="airDayaPompaSatuan" maxlength="255" size="15" value="<?php echo $obj->airDayaPompaSatuan; ?>" />
							</td>
						</tr> -->
						</table>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pipa Hisap</td><td>&nbsp;:&nbsp;</td>
					<td>
						Diameter <input class="box" id="airDiameterPipaHisap" name="airDiameterPipaHisap" maxlength="255" size="5" value="<?php echo $obj->airDiameterPipaHisap; ?>" />
						<input class="box" id="airDiameterPipaHisapSatuan" name="airDiameterPipaHisapSatuan" maxlength="255" size="5" value="<?php echo $obj->airDiameterPipaHisapSatuan; ?>" /><br>
						Panjang <input class="box" id="airPanjangPipaHisap" name="airPanjangPipaHisap" maxlength="255" size="5" value="<?php echo $obj->airPanjangPipaHisap; ?>" />
						<input class="box" id="airPanjangPipaHisapSatuan" name="airPanjangPipaHisapSatuan" maxlength="255" size="5" value="<?php echo $obj->airPanjangPipaHisapSatuan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pipa Buang</td><td>&nbsp;:&nbsp;</td>
					<td>
						Diameter <input class="box" id="airDiameterPipaBuang" name="airDiameterPipaBuang" maxlength="255" size="5" value="<?php echo $obj->airDiameterPipaBuang; ?>" />
						<input class="box" id="airDiameterPipaBuangSatuan" name="airDiameterPipaBuangSatuan" maxlength="255" size="5" value="<?php echo $obj->airDiameterPipaBuangSatuan; ?>" /><br>
						Panjang <input class="box" id="airPanjangPipaBuang" name="airPanjangPipaBuang" maxlength="255" size="5" value="<?php echo $obj->airPanjangPipaBuang; ?>" />
						<input class="box" id="airPanjangPipaBuangSatuan" name="airPanjangPipaBuangSatuan" maxlength="255" size="5" value="<?php echo $obj->airPanjangPipaBuangSatuan; ?>" />
					</td>
				</tr>
				<!-- <tr>
					<td style="padding-left:20px;">Debit yang Dipompa</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airDebitPompa" name="airDebitPompa" maxlength="255" size="5" value="<?php echo $obj->airDebitPompa; ?>" />
						<input class="box" id="airDebitPompaSatuan" name="airDebitPompaSatuan" maxlength="255" size="15" value="<?php echo $obj->airDebitPompaSatuan; ?>" />
					</td>
				</tr> -->
				<tr>
					<td style="padding-left:20px;">Debit Air yang Dipergunakan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airDebitGuna" name="airDebitGuna" maxlength="255" size="5" value="<?php echo $obj->airDebitGuna; ?>" />
						<input class="box" id="airDebitGunaSatuan" name="airDebitGunaSatuan" maxlength="255" size="15" value="<?php echo $obj->airDebitGunaSatuan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lama Pemompaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airLamaPompa" name="airLamaPompa" maxlength="255" size="5" value="<?php echo $obj->airLamaPompa; ?>" />
						<input class="box" id="airLamaPompaSatuan" name="airLamaPompaSatuan" maxlength="255" size="15" value="<?php echo $obj->airLamaPompaSatuan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Mutu Air</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airMutu" name="airMutu" maxlength="255" size="50" value="<?php echo $obj->airMutu; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Analisa Kualitas Air</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airKualitas" name="airKualitas" maxlength="255" size="50" value="<?php echo $obj->airKualitas; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Studi UKP, UPL</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airStudi" name="airStudi" maxlength="255" size="50" value="<?php echo $obj->airStudi; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keluhan Masyarakat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airKeluhan" name="airKeluhan" maxlength="255" size="50" value="<?php echo $obj->airKeluhan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jumlah Karyawan Operator</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box bulat" id="airOperator" name="airOperator" maxlength="3" size="3" value="<?php echo $obj->airOperator; ?>" /> orang
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kesediaan Memasang Alat Ukur</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="airKesediaan" name="airKesediaan" maxlength="255" size="50" value="<?php echo $obj->airKesediaan; ?>" />
					</td>
				</tr>
				
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->airNo > 0) {
?>
						<span>KPTS 540/<?php echo CFG_COMPANY_SHORT_NAME; ?>/<?php echo substr($obj->airTglPengesahan,0,4); ?>/</span>
						<input class="box" id="airNo" name="airNo" maxlength="5" size="5" value="<?php echo $obj->airNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="airNo" name="airNo" value="<?php echo $obj->airNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="airTglPengesahan" name="airTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->airTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="airPejID" name="airPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->airPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='air'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->airTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->airTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="airTglBerlaku" name="airTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->airTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->airTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->airTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="airTglDaftarUlang" name="airTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->airTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/airpermukaan/js/edit.airpermukaan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printAir($obj) {
		global $app;
		
		$objReport 		= new Report('Izin Pengambilan Air yang Bersumber dari Air Permukaan', 'AirPermukaan-'.substr($obj->airTglPengesahan,0,4).'-'.$obj->airNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->airPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<h4 align="center">
		KEPUTUSAN KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?><br>
		KABUPATEN PELALAWAN<br>
		NOMOR : <?php echo $obj->airNoLengkap; ?><br><br>
		TENTANG<br><br>
		IZIN PENGAMBILAN AIR<br>
		YANG BERSUMBER DARI AIR PERMUKAAN (AP)<br>
		An. <?php echo strtoupper($obj->perNama); ?><br>
		<br>
		KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?> KABUPATEN PELALAWAN,
	</h4>
	
		<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr><td>Surat Permohonan dari <?php echo strtoupper($obj->perNama); ?> Nomor : <?php echo $obj->airPermohonanNo; ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->airPermohonanTgl); ?>, Perihal : <?php echo $obj->airPermohonanPerihal; ?>.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">
			<table>
				<tr><td width="4%">a.</td><td width="96%">Surat Tugas dari Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan No: <?php echo $obj->airTugasNo; ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->airTugasTgl); ?>.</td></tr>
				<tr><td>b.</td><td>Bahwa berdasarkan hasil peninjauan/ pemeriksaan dilapangan atas Pemohonan <?php echo ($obj->dmhnTipe != 'Baru' ? $obj->dmhnTipe.' ' : ''); ?>Izin Pengambilan Air Permukaan An. <?php echo strtoupper($obj->perNama); ?> yang dituangkan dalam berita acara pemeriksaan dapat diberikan Izin Pengambilan Air (SIPA) yang besumber dari Air Permukaan.</td></tr>
				<tr><td>c.</td><td>Bahwa berdasarkan pertimbangan sebagaimana maksud dalam huruf a dan b, perlu menetapkan keputusan Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan tentang Pengambilan Izin Air Permukaan An. <?php echo strtoupper($obj->perNama); ?>.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
<?php 
		echo $konfigurasi['consideringAT'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">PERTAMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Memberikan Izin Pengambilan Air yang bersumber dari Air Permukaan kepada :</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top"></td>
		<td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0">
			<tr>
				<td align="left" valign="top" colspan="3">&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td align="left" valign="top" width="25%">- Nama Perusahaan</td>
				<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="73%"><?php echo strtoupper($obj->perNama); ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">- Nama Pimpinan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo strtoupper($obj->mhnNama); ?></td>
			</tr>
			<tr>
			  	<td align="left" valign="top">- Alamat</td>
			  	<td align="left" valign="top">:</td>
				<td align="left" valign="top">
<?php 
		$sql = "SELECT CONCAT(wilayah.wilTingkat,' ',wilayah.wilNama) AS kelurahan, CONCAT(wilayah2.wilTingkat,' ',wilayah2.wilNama) AS kecamatan, CONCAT(wilayah3.wilTingkat,' ',wilayah3.wilNama) AS kabupaten  
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objWilayah = $app->queryObject($sql);
?>
					<?php echo $objWilayah->kelurahan."<br>".$objWilayah->kecamatan."<br>".$objWilayah->kabupaten; ?>
				</td>
			</tr>
			</table>
			Untuk keperluan <?php echo $obj->airKeterangan; ?>, dengan ketentuan sebagai berikut : 
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top"></td><td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0" width="100%">
			<tr>
				<td width="5%">1.</td>
				<td width="45%">Jumlah Titik Pengambilan</td>
				<td width="5%">:</td>
				<td width="45%"><?php echo $obj->airJumlahSumur; ?> (<?php echo $app->terbilang($obj->airJumlahSumur); ?>) titik</td>
			</tr>
			<tr>
				<td>2.</td>
				<td>Sumber Air</td>
				<td>:</td>
				<td><?php echo $obj->airSumber; ?></td>
			</tr>
			<tr>
				<td>3.</td>
				<td>Titik Koordinat</td>
				<td>:</td>
				<td>N <?php echo $obj->airKoordinatNDerajat; ?>&deg; <?php echo $obj->airKoordinatNMenit; ?>' <?php echo $obj->airKoordinatNDetik; ?>"</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td>E <?php echo $obj->airKoordinatEDerajat; ?>&deg; <?php echo $obj->airKoordinatEMenit; ?>' <?php echo $obj->airKoordinatEDetik; ?>"</td>
			</tr>
			<tr>
				<td>4.</td>
				<td>Elevasi</td>
				<td>:</td>
				<td><?php echo $obj->airElevasi; ?> meter</td>
			</tr>
			<tr>
				<td>5.</td>
				<td>Kedalaman Sumber Air</td>
				<td>:</td>
				<td><?php echo $obj->airKedalaman; ?> meter</td>
			</tr>
			<tr>
				<td>6.</td>
				<td>Letak Pompa</td>
				<td>:</td>
				<td><?php echo $obj->airLetakPompa; ?></td>
			</tr>
			<tr>
				<td>7.</td>
				<td>Pompa yang digunakan</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>- Nama Merek</td>
				<td>:</td>
				<td><?php echo $obj->airMerekPompa; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>- Type</td>
				<td>:</td>
				<td><?php echo $obj->airTipePompa; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>- Kapasitas</td>
				<td>:</td>
				<td><?php echo $app->MySQLToMoney($obj->airKapasitasPompa); ?> <?php echo $obj->airKapasitasPompaSatuan; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>- Head M</td>
				<td>:</td>
				<td><?php echo $obj->airHeadPompa; ?> <?php echo $obj->airHeadPompaSatuan; ?></td>
			</tr>
			<!-- <tr>
				<td></td>
				<td>- Daya</td>
				<td>:</td>
				<td><?php echo $obj->airDayaPompa; ?> <?php echo $obj->airDayaPompaSatuan; ?></td>
			</tr> -->
			<tr>
				<td>8.</td>
				<td>Pipa Hisap</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>- Diameter</td>
				<td>:</td>
				<td><?php echo $obj->airDiameterPipaHisap; ?> <?php echo $obj->airDiameterPipaHisapSatuan; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>- Panjang</td>
				<td>:</td>
				<td><?php echo $obj->airPanjangPipaHisap; ?> <?php echo $obj->airPanjangPipaHisapSatuan; ?></td>
			</tr>
			<tr>
				<td>9.</td>
				<td>Pipa Buang</td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td>- Diameter</td>
				<td>:</td>
				<td><?php echo $obj->airDiameterPipaBuang; ?> <?php echo $obj->airDiameterPipaBuangSatuan; ?></td>
			</tr>
			<tr>
				<td></td>
				<td>- Panjang</td>
				<td>:</td>
				<td><?php echo $obj->airPanjangPipaBuang; ?> <?php echo $obj->airPanjangPipaBuangSatuan; ?></td>
			</tr>
			<tr>
				<td>10.</td>
				<td>Debit Air yang digunakan</td>
				<td>:</td>
				<td><?php echo $obj->airDebitGuna; ?> <?php echo $obj->airDebitGunaSatuan; ?></td>
			</tr>
			<tr>
				<td>11.</td>
				<td>Lama Pemompaan</td>
				<td>:</td>
				<td><?php echo $obj->airLamaPompa; ?> <?php echo $obj->airLamaPompaSatuan; ?></td>
			</tr>
			<tr>
				<td>12.</td>
				<td>Mutu Air</td>
				<td>:</td>
				<td><?php echo $obj->airMutu; ?></td>
			</tr>
			<tr>
				<td>13.</td>
				<td>Analisa Kualitas Air</td>
				<td>:</td>
				<td><?php echo $obj->airKualitas; ?></td>
			</tr>
			<tr>
				<td>14.</td>
				<td>Study UKL, UPL</td>
				<td>:</td>
				<td><?php echo $obj->airStudi; ?></td>
			</tr>
			<tr>
				<td>15.</td>
				<td>Keluhan Masyarakat</td>
				<td>:</td>
				<td><?php echo $obj->airKeluhan; ?></td>
			</tr>
			<tr>
				<td>16.</td>
				<td>Jumlah Karyawan Operator</td>
				<td>:</td>
				<td><?php echo $obj->airOperator; ?> orang</td>
			</tr>
			<tr>
				<td>17.</td>
				<td>Kesedian memasang alat ukur </td>
				<td>:</td>
				<td><?php echo $obj->airKesediaan; ?></td>
			</tr>
			<tr>
				<td>18.</td>
				<td colspan="3">Memberikan sebagian airnya kepada masyarakat lingkungan sekitarnya secara cuma-cuma apabila diperlukan.</td>
			</tr>
			<tr>
				<td>19.</td>
				<td colspan="3">Selambat-lambatnya melaporkan 3 bulan sebelum masa berlaku izin habis.</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEDUA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0" width="100%">
			<tr>
				<td width="5%"></td>
				<td width="95%">Setiap bulan, triwulan dan tahunan melaporkan secara tertulis jumlah pengambilan dan pemanfaatan Air  Permukaan dengan melampirkan bukti pelunasan pajak/ retribusi kepada Bupati Pelalawan Cq. Dinas Pertambangan dan Energi Kabupaten Pelalawan.</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KETIGA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0" width="100%">
			<tr>
				<td width="5%"></td>
				<td width="95%">Setiap pengambilan dan pemanfaatan air yang tidak memenuhi ketentuan diatas, dapat dikenakan tindakan berupa penyegelan titik pengambilan air dan atas pencabutan izin pengambilan air.</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEEMPAT</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0" width="100%">
			<tr>
				<td width="5%"></td>
				<td width="95%">Keputusan ini mulai berlaku pada tanggal ditetapkan berlaku untuk jangka waktu selama 2 (dua)tahun.</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>

	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="48%">&nbsp;</td>
		<td width="52%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->airTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readAir($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=airpermukaan&task=edit&id=<?php echo $obj->airDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Pengambilan Air yang Bersumber dari Air Permukaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=airpermukaan&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=airpermukaan&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->airNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->airTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->airPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->airTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->airTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->airTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->airTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/airpermukaan/js/read.airpermukaan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewAir($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=airpermukaan"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Pengambilan Air yang Bersumber dari Air Permukaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('airNo', 'Nomor Surat', true),
			$app->setHeader('airTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('airTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('airTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=airpermukaan&task=read&id='.$v->airDmhnID.'&fromact=airpermukaan" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->airDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=airpermukaan&task=read&id='.$v->airDmhnID.'&fromact=airpermukaan" title="Entri">'.$v->airNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->airTglPengesahan).'</td>';
				
				if ($v->airTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->airTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->airTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->airTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/airpermukaan/js/airpermukaan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>