<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.airpermukaan.php';
require_once 'view.airpermukaan.php';

switch ($app->task) {
	case 'add':
		addAir($app->id);
		break;
	case 'edit':
		editAir($app->id);
		break;
	case 'delete':
		deleteAir($app->id);
		break;
	case 'print':
		printAir($app->id);
		break;
	case 'read':
		readAir($app->id);
		break;
	case 'save':
		saveAir();
		break;
	default:
		viewAir(true, '');
		break;
}

function addAir($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editAir($id);
}

function deleteAir($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM airpermukaan WHERE airDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Izin Air Permukaan berhasil dihapus';
	
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Izin Air Permukaan', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function editAir($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objAir = $app->queryObject("SELECT * FROM airpermukaan WHERE airDmhnID='".$id."'");
	if (!$objAir) {
		$objAir = new Air_Model();
		
		//Ambil nilai default
		$objAir->airNPWPD = $objDetailPermohonan->perNPWPD;
		$objAir->airJenisUsaha = $objDetailPermohonan->perJenisUsaha;
		$objAir->airBidangUsaha = $objDetailPermohonan->perBidangUsaha;
		$objAir->airMerekUsaha = $objDetailPermohonan->perMerekUsaha;
		$objAir->airLuasTempatUsaha = $objDetailPermohonan->perLuasTempatUsaha;
		$objAir->airStatusTempatUsaha = $objDetailPermohonan->perStatusTempatUsaha;
	}
	
	$objAir->airDmhnID = $objDetailPermohonan->dmhnID;
	
	Air_View::editAir(true, '', $objDetailPermohonan, $objAir);
}

function printAir($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN airpermukaan ON airDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objAir = $app->queryObject($sql);
	if (!$objAir) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objAir->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	//HACK
	if ($objAir->airAlamat != '') {
		$objAir->perAlamat = $objAir->airAlamat;
	}
	if ($objAir->airKelurahanID > 0) {
		$objAir->perKelurahanID = $objAir->airKelurahanID;
	}
	
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objAir->perKelurahanID."'");
	$objAir->perKelurahan = $objKelurahan->wilNama;
	$objAir->perKecamatan = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");
		
	Air_View::printAir($objAir);
}

function readAir($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN airpermukaan ON airDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objAir = $app->queryObject($sql);
	if (!$objAir) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objAir->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Air_View::readAir($objAir);
}

function saveAir() {
	global $app;
	
	//Get object
	$objAirSebelumnya = $app->queryObject("SELECT * FROM airpermukaan WHERE airID='".$app->getInt('airID')."'");
	if (!$objAirSebelumnya) {
		$objAirSebelumnya = new Air_Model();
	}
	
	//Create object
	$objAir = new Air_Model();
	$app->bind($objAir);
	
	//Modify object (if necessary)
	$objAir->airKapasitasPompa = $app->MoneyToMySQL($objAir->airKapasitasPompa);
	$objAir->airPermohonanTgl = $app->NormalDateToMySQL($objAir->airPermohonanTgl);
	$objAir->airTugasTgl = $app->NormalDateToMySQL($objAir->airTugasTgl);
	$objAir->airTglPengesahan = $app->NormalDateToMySQL($objAir->airTglPengesahan);
	
	if ($objAir->airTglPengesahan != '0000-00-00') {
		if ($objAirSebelumnya->airNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objAir->airNo = intval($app->queryField("SELECT MAX(airNo) AS nomor FROM airpermukaan WHERE YEAR(airTglPengesahan)='".substr($objAir->airTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objAir->airTglBerlaku = date('Y-m-d', strtotime($objAir->airTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objAir->airTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objAir->airTglDaftarUlang = date('Y-m-d', strtotime($objAir->airTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objAir->airTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objAir->airNo = 0;
		
		$objAir->airTglBerlaku = '0000-00-00';
		$objAir->airTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objAir->airNoLengkap = 'KPTS 540/'.CFG_COMPANY_SHORT_NAME.'/'.substr($objAir->airTglPengesahan,0,4).'/'.$objAir->airNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objAir->airNo > 0){
		$isExist = $app->isExist("SELECT airID AS id FROM airpermukaan WHERE airNo='".$objAir->airNo."' AND YEAR(airTglPengesahan)='".substr($objAir->airTglPengesahan,0,4)."'", $objAir->airID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objAir->airNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objAir->airID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objAir);
		} else {
			$sql = $app->createSQLforUpdate($objAir);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objAir->airID = $app->queryField("SELECT LAST_INSERT_ID() FROM airpermukaan");
		}
		
		$success = true;
		$msg = 'Izin Air Permukaan berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'air', $objAir->airID, $objAir->airNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objAir->airDmhnID."'");
	} else {
		$success = false;
		$msg = 'Izin Air Permukaan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objAir->airDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Air_View::editAir($success, $msg, $objDetailPermohonan, $objAir);
	}
}

function viewAir($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('air', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('air', 'sort', 'airTglPengesahan');
	//$dir		= $app->pageVar('air', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('air', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('air', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(airNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(airTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM airpermukaan
				  LEFT JOIN detailpermohonan ON airDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM airpermukaan
			LEFT JOIN detailpermohonan ON airDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(airTglPengesahan) DESC, airNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Air_View::viewAir($success, $msg, $arr);
}
?>