<?php
class Air_Model {	
	public $tabel = 'airpermukaan';
	public $primaryKey = 'airID';
	
	public $airID = 0;
	public $airDmhnID = 0;
	public $airGrupID = 0;
	public $airNo = 0;
	public $airNoLengkap = '';
	
	public $airPermohonanNo = '';
	public $airPermohonanTgl = '0000-00-00';
	public $airPermohonanPerihal = '';
	public $airTugasNo = '';
	public $airTugasTgl = '0000-00-00';
	
	public $airKeterangan = '';
	public $airJumlahSumur = 0;    //Jumlah Titik Pengambilan
	public $airSumber = '';
	public $airKoordinatN = '';
	public $airKoordinatNDerajat = 0;
	public $airKoordinatNMenit = 0;
	public $airKoordinatNDetik = 0;
	public $airKoordinatE = '';
	public $airKoordinatEDerajat = 0;
	public $airKoordinatEMenit = 0;
	public $airKoordinatEDetik = 0;
	public $airElevasi = '';
	public $airKedalaman = '';
	public $airLetakPompa = '';
	public $airMerekPompa = '';
	public $airTipePompa = '';
	public $airKapasitasPompa = 0;
	public $airKapasitasPompaSatuan = 'liter/menit';
	public $airHeadPompa = '';
	public $airHeadPompaSatuan = 'meter';
	public $airDayaPompa = '';
	public $airDayaPompaSatuan = 'HP';
	public $airDiameterPipaHisap = '';
	public $airDiameterPipaHisapSatuan = 'inchi';
	public $airPanjangPipaHisap = '';
	public $airPanjangPipaHisapSatuan = 'meter';
	public $airDiameterPipaBuang = '';
	public $airDiameterPipaBuangSatuan = 'inchi';
	public $airPanjangPipaBuang = '';
	public $airPanjangPipaBuangSatuan = 'meter';
	public $airDebitPompa = '';
	public $airDebitPompaSatuan = 'liter/detik';
	public $airDebitGuna = '';
	public $airDebitGunaSatuan = 'liter/hari';
	public $airLamaPompa = '';
	public $airLamaPompaSatuan = 'jam/hari';
	public $airMutu = '';
	public $airKualitas = '';
	public $airStudi = '';
	public $airKeluhan = '';
	public $airOperator = '';
	public $airKesediaan = '';
	
	public $airTglBerlaku = '0000-00-00';	
	public $airTglDaftarUlang = '0000-00-00';
	public $airPejID = 0;
	public $airTglPengesahan = '0000-00-00'; 
	public $airArsip = '';
}
?>