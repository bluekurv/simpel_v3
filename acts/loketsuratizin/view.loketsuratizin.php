<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class LoketSuratIzin_View {
	static function editLoketSuratIzin($success, $msg, $arr, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=loketsuratizin&id=<?php echo $obj->lokID; ?>"><img src="images/icons/office_building.png" border="0" /> Pilih Pelayanan untuk Loket "<?php echo $obj->lokNama; ?>"</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=loketsuratizin&task=save" method="POST" >
				<input type="hidden" id="id" name="id" value="<?php echo $obj->lokID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=loket">Kembali</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<br>
				<table border="0" cellpadding="1" cellspacing="1">
<?php 
		if (count($arr) > 0) {
			foreach ($arr as $v) {
?>
				<tr>
					<td><input type="checkbox" id="jenissuratizin[<?php echo $v->jsiID; ?>]" name="jenissuratizin[<?php echo $v->jsiID; ?>]" value="<?php echo $v->jsiID; ?>" <?php echo ($v->jsiID == $v->lsiJsiID) ? 'checked' : ''; ?>><label for="jenissuratizin[<?php echo $v->jsiID; ?>]"><?php echo $v->jsiNama.($v->jsiSubNama != '' ? ' ('.$v->jsiSubNama.')' : ''); ?></label></td>
				</tr>
<?php 
			}
		}
?>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php
	}
}
?>