<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app, $id;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'view.loketsuratizin.php';

switch ($app->task) {
	case 'save':
		saveLoketSuratIzin($app->id);
		break;
	default:
		editLoketSuratIzin(true, '', $app->id);
		break;
}

function saveLoketSuratIzin($id) {
	global $app;
	
	//Get object
	$objLoket = $app->queryObject("SELECT * FROM loket WHERE lokID='".$id."'");
	if (!$objLoket) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM loketsuratizin WHERE lsiLokID='".$id."'");
	
	$arr = array();
	
	if (isset($_REQUEST['jenissuratizin'])) {
		foreach ($_REQUEST['jenissuratizin'] as $v) {
			$arr[] = "('".$id."', '".$v."')";
		}
		
		$app->query("INSERT INTO loketsuratizin (lsiLokID, lsiJsiID) VALUES ".implode(', ', $arr)); 
		
		$msg = count($_REQUEST['jenissuratizin']).' Pelayanan berhasil disimpan';
		
		$app->writeLog(EV_INFORMASI, 'loket', $objLoket->lokID, $objLoket->lokNama, $msg);
		
		editLoketSuratIzin(true, $msg, $id);
	} else {
		editLoketSuratIzin(true, '', $id);
	}
}

function editLoketSuratIzin($success, $msg, $id) {
	global $app;
	
	//Get object
	$objLoket = $app->queryObject("SELECT * FROM loket WHERE lokID='".$id."'");
	if (!$objLoket) {
		$app->showPageError();
		exit();
	}
	
	$arr = array();
	
	//Query
	$sql = "SELECT *
			FROM jenissuratizin
			LEFT JOIN loketsuratizin ON lsiLokID='".$id."' AND lsiJsiID=jsiID
			ORDER BY jsiNama";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr[] = $obj;
	}
	
	LoketSuratIzin_View::editLoketSuratIzin($success, $msg, $arr, $objLoket);
}
?>