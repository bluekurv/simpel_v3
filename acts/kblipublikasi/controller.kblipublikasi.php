<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kblipublikasi.php';
require_once 'view.kblipublikasi.php';
require_once CFG_ABSOLUTE_PATH.'/acts/kbli/view.kbli.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKBLIPublikasi($app->id);
		break;
	case 'delete':
		deleteKBLIPublikasi($app->id);
		break;
	case 'save':
		saveKBLIPublikasi();
		break;
	default:
		viewKBLIPublikasi(true, '');
		break;
}

function deleteKBLIPublikasi($id) {
	global $app;
	
	//Get object
	$objKBLIPublikasi = $app->queryObject("SELECT * FROM kblipublikasi WHERE kpubID='".$id."'");
	if (!$objKBLIPublikasi) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalKBLIKategori = intval($app->queryField("SELECT COUNT(*) AS total FROM kblikategori WHERE kkatKpubID='".$id."'"));
	if ($totalKBLIKategori > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLIKategori.' kategori untuk publikasi tersebut';
	}
	
	$totalKBLIGolonganPokok = intval($app->queryField("SELECT COUNT(*) AS total FROM kbligolonganpokok WHERE kgpokKpubID='".$id."'"));
	if ($totalKBLIGolonganPokok > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLIGolonganPokok.' golongan pokok untuk publikasi tersebut';
	}
	
	$totalKBLIGolongan = intval($app->queryField("SELECT COUNT(*) AS total FROM kbligolongan WHERE kgolKpubID='".$id."'"));
	if ($totalKBLIGolongan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLIGolongan.' golongan untuk publikasi tersebut';
	}
	
	$totalKBLISubGolongan = intval($app->queryField("SELECT COUNT(*) AS total FROM kblisubgolongan WHERE ksubKpubID='".$id."'"));
	if ($totalKBLISubGolongan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLISubGolongan.' sub golongan untuk publikasi tersebut';
	}
	
	$totalKBLIKelompok = intval($app->queryField("SELECT COUNT(*) AS total FROM kblikelompok WHERE kkelKpubID='".$id."'"));
	if ($totalKBLIKelompok > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLIKelompok.' kelompok untuk publikasi tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM kblipublikasi WHERE kpubID='".$id."'");
		
		$success = true;
		$msg = 'Publikasi Lapangan Usaha "'.$objKBLIPublikasi->kpubNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'kblipublikasi', $objKBLIPublikasi->kpubID, $objKBLIPublikasi->kpubNama, $msg);
	} else {
		$success = false;
		$msg = 'Publikasi Lapangan Usaha "'.$objKBLIPublikasi->kpubNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKBLIPublikasi($success, $msg);
}

function editKBLIPublikasi($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKBLIPublikasi = $app->queryObject("SELECT * FROM kblipublikasi WHERE kpubID='".$id."'");
		if (!$objKBLIPublikasi) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKBLIPublikasi = new KBLIPublikasi_Model();
	}
	
	KBLIPublikasi_View::editKBLIPublikasi(true, '', $objKBLIPublikasi);
}

function saveKBLIPublikasi() {
	global $app;
	
	//Create object
	$objKBLIPublikasi = new KBLIPublikasi_Model();
	$app->bind($objKBLIPublikasi);
	
	//TODO: Modify object (if necessary)
	if (isset($_REQUEST['kpubPublikasi'])) {
		$objKBLIPublikasi->kpubPublikasi = ($_REQUEST['kpubPublikasi'] == 'on') ? 1 : 0;
	} else {
		$objKBLIPublikasi->kpubPublikasi = 0;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKBLIPublikasi->kpubNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT kpubID AS id FROM kblipublikasi WHERE kpubNama='".$objKBLIPublikasi->kpubNama."'", $objKBLIPublikasi->kpubID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objKBLIPublikasi->kpubNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKBLIPublikasi->kpubID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKBLIPublikasi);
		} else {
			$sql = $app->createSQLforUpdate($objKBLIPublikasi);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKBLIPublikasi->kpubID = $app->queryField("SELECT LAST_INSERT_ID() FROM kblipublikasi");
			
			$success = true;
			$msg = 'Publikasi Lapangan Usaha "'.$objKBLIPublikasi->kpubNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'kblipublikasi', $objKBLIPublikasi->kpubID, $objKBLIPublikasi->kpubNama, $msg);
		} else {
			$success = true;
			$msg = 'Publikasi Lapangan Usaha "'.$objKBLIPublikasi->kpubNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'kblipublikasi', $objKBLIPublikasi->kpubID, $objKBLIPublikasi->kpubNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Publikasi Lapangan Usaha "'.$objKBLIPublikasi->kpubNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKBLIPublikasi($success, $msg);
	} else {
		KBLIPublikasi_View::editKBLIPublikasi($success, $msg, $objKBLIPublikasi);
	}
}

function viewKBLIPublikasi($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kblipublikasi', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kblipublikasi', 'sort', 'kpubNama');
	$dir   		= $app->pageVar('kblipublikasi', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('kblipublikasi', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "kpubNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM kblipublikasi
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM kblipublikasi
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	KBLIPublikasi_View::viewKBLIPublikasi($success, $msg, $arr);
}
?>