//Variables
var form = $("#myForm");
var kpubNama = $("#kpubNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'kpubNama' || this.id === undefined) {
		if (kpubNama.val().length == 0){
			doSubmit = false;
			kpubNama.addClass("error");		
		} else {
			kpubNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
kpubNama.blur(validateForm);

kpubNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
kpubNama.focus();