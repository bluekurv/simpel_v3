<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Provinsi_View {
	static function editProvinsi($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=provinsi&task=edit&id=<?php echo $obj->wilID; ?>"><img src="images/icons/flag_red.png" border="0" /> <?php echo ($obj->wilID > 0) ? 'Ubah' : 'Tambah'; ?> Provinsi</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=provinsi&task=save" method="POST" >
				<input type="hidden" id="wilID" name="wilID" value="<?php echo $obj->wilID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=provinsi">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Berada di</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		$app->createSelect('wilParentID', "SELECT negID AS id, negNama AS nama FROM negara ORDER BY negNama", $obj->wilParentID);
?>
					</td>
				</tr>
				<tr>
					<td>Kode <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="wilKode" name="wilKode" maxlength="5" size="5" value="<?php echo $obj->wilKode; ?>" />
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						Provinsi <input class="box" id="wilNama" name="wilNama" maxlength="255" size="50" value="<?php echo $obj->wilNama; ?>" />
					</td>
				</tr>
				<tr id="trIbukota">
					<td>Ibukota <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="wilIbukota" name="wilIbukota" maxlength="50" size="50" value="<?php echo $obj->wilIbukota; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/provinsi/js/edit.provinsi.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewProvinsi($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=provinsi"><img src="images/icons/flag_red.png" border="0" /> Provinsi</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=provinsi&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('wilKode', 'Kode', true, 40),
			$app->setHeader('wilTingkat', 'Tingkat', true, 60),
			$app->setHeader('wilNama', 'Nama', true),
			$app->setHeader('wilIbukota', 'Ibukota', true),
			$app->setHeader('negNama', 'Berada di', true),
			$app->setHeader('total', 'Total Kabupaten/Kota', true, 0, 'right')
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=provinsi&task=edit&id='.$v->wilID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->wilID.', '."'".$v->wilTingkat." ".$v->wilNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=provinsi&task=edit&id='.$v->wilID.'">'.$v->wilKode.'</a></td>';
				echo '<td>'.$v->wilTingkat.'</td>';
				echo '<td>'.$v->wilNama.'</td>';
				echo '<td>'.$v->wilIbukota.'</td>';
				echo '<td>'.$v->negNama.'</td>';
				echo '<td align="right">'.$v->total.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="7">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/provinsi/js/provinsi.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>