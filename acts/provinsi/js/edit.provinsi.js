//Variables
var form = $("#myForm");
var wilParentID = $("#wilParentID");
var wilKode = $("#wilKode");
var wilNama = $("#wilNama");
var wilIbukota = $("#wilIbukota");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'wilParentID' || this.id === undefined) {
		if (wilParentID.val == 0){
			doSubmit = false;
			wilParentID.addClass("error");		
		} else {
			wilParentID.removeClass("error");
		}
	}
	
	if (this.id == 'wilKode' || this.id === undefined) {
		if (wilKode.val().length == 0){
			doSubmit = false;
			wilKode.addClass("error");		
		} else {
			wilKode.removeClass("error");
		}
	}
	
	if (this.id == 'wilNama' || this.id === undefined) {
		if (wilNama.val().length == 0){
			doSubmit = false;
			wilNama.addClass("error");		
		} else {
			wilNama.removeClass("error");
		}
	}
	
	if (this.id == 'wilIbukota' || this.id === undefined) {
		if (wilIbukota.val().length == 0){
			doSubmit = false;
			wilIbukota.addClass("error");		
		} else {
			wilIbukota.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
wilParentID.blur(validateForm);
wilKode.blur(validateForm);
wilNama.blur(validateForm);
wilIbukota.blur(validateForm);

wilParentID.keyup(validateForm);
wilKode.keyup(validateForm);
wilNama.keyup(validateForm);
wilIbukota.keyup(validateForm);

form.submit(submitForm);

//Set focus
wilParentID.focus();