<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Loket Pelayanan']['all'] = true;
$acl['Petugas Administrasi']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.provinsi.php';
require_once 'view.provinsi.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editProvinsi($app->id);
		break;
	case 'delete':
		deleteProvinsi($app->id);
		break;
	case 'save':
		saveProvinsi();
		break;
	default:
		viewProvinsi(true, '');
		break;
}

function deleteProvinsi($id) {
	global $app;
	
	//Get object
	$objProvinsi = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$id."' AND wilTingkat='Provinsi'");
	if (!$objProvinsi) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalKabupaten = intval($app->queryField("SELECT COUNT(*) AS total FROM wilayah WHERE wilParentID='".$id."' AND (wilTingkat='Kabupaten' OR wilTingkat='Kota')"));
	if ($totalKabupaten > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKabupaten.' kabupaten/kota pada provinsi tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM wilayah WHERE wilID='".$id."'");
		
		$success = true;
		$msg = $objProvinsi->wilTingkat.' "'.$objProvinsi->wilNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'wilayah', $objProvinsi->wilID, $objProvinsi->wilNama, $msg);
	} else {
		$success = false;
		$msg = $objProvinsi->wilTingkat.' "'.$objProvinsi->wilNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewProvinsi($success, $msg);
}

function editProvinsi($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objProvinsi = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$id."' AND wilTingkat='Provinsi'");
		if (!$objProvinsi) {
			$app->showPageError();
			exit();
		}
	} else {
		$objProvinsi = new Provinsi_Model();
	}
	
	Provinsi_View::editProvinsi(true, '', $objProvinsi);
}

function saveProvinsi() {
	global $app;
	
	//Create object
	$objProvinsi = new Provinsi_Model();
	$app->bind($objProvinsi);
	
	//Modify object (if necessary)
	$objProvinsi->wilTingkat = 'Provinsi';
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objProvinsi->wilKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objProvinsi->wilNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objProvinsi->wilIbukota == '') {
		$doSave = false;
		$msg[] = '- Ibukota belum diisi';
	}
	
	if ($objProvinsi->wilKode != '00') {
		$isExist = $app->isExist("SELECT wilID AS id FROM wilayah WHERE wilKode='".$objProvinsi->wilKode."' AND wilTingkat='Provinsi' AND wilParentID='".$objProvinsi->wilParentID."'", $objProvinsi->wilID);
		if (!$isExist) {
			$doSave = false;
			$msg[]  = '- Kode "'.$objProvinsi->wilKode.'" sudah ada';
		}
	}
	
	$isExist = $app->isExist("SELECT wilID AS id FROM wilayah WHERE wilNama='".$objProvinsi->wilNama."' AND wilTingkat='Provinsi' AND wilParentID='".$objProvinsi->wilParentID."'", $objProvinsi->wilID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama "'.$objProvinsi->wilNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objProvinsi->wilID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objProvinsi);
		} else {
			$sql = $app->createSQLforUpdate($objProvinsi);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objProvinsi->wilID = $app->queryField("SELECT LAST_INSERT_ID() FROM wilayah");
			
			$success = true;
			$msg = $objProvinsi->wilTingkat.' "'.$objProvinsi->wilNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'wilayah', $objProvinsi->wilID, $objProvinsi->wilNama, $msg);
		} else {
			$success = true;
			$msg = $objProvinsi->wilTingkat.' "'.$objProvinsi->wilNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'wilayah', $objProvinsi->wilID, $objProvinsi->wilNama, $msg);
		}
	} else {
		$success = false;
		$msg = $objProvinsi->wilTingkat.' "'.$objProvinsi->wilNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewProvinsi($success, $msg);
	} else {
		Provinsi_View::editProvinsi($success, $msg, $objProvinsi);
	}
}

function viewProvinsi($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('provinsi', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('provinsi', 'sort', 'wilKode');
	$dir   		= $app->pageVar('provinsi', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('provinsi', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "wilNama LIKE '%".$nama."%'";
	}
	$filter[] = "wilTingkat='Provinsi' AND wilParentID=negID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM wilayah, negara
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *, (SELECT COUNT(*) AS total FROM wilayah AS sub WHERE sub.wilParentID=wilayah.wilID AND (sub.wilTingkat='Kabupaten' OR sub.wilTingkat='Kota')) AS total
			FROM wilayah, negara
			".$where." 
			ORDER BY ".$sort." ".$dir."
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Provinsi_View::viewProvinsi($success, $msg, $arr);
}
?>