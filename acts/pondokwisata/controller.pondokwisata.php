<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'pondokwisata';
$childTitle = 'Izin Operasional Pondok Wisata';
$childKodeSurat = 'IOP-PW';
$childTembusan = 'Kepala Dinas Kebudayaan, Pariwisata, Pemuda dan Olahraga Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>