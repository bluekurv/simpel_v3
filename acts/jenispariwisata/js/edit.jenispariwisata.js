//Variables
var form = $("#myForm");
var jparNama = $("#jparNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'jparNama' || this.id === undefined) {
		if (jparNama.val().length == 0){
			doSubmit = false;
			jparNama.addClass("error");		
		} else {
			jparNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
jparNama.blur(validateForm);

jparNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
jparNama.focus();