<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.jenispariwisata.php';
require_once 'view.jenispariwisata.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editJenisPariwisata($app->id);
		break;
	case 'delete':
		deleteJenisPariwisata($app->id);
		break;
	case 'save':
		saveJenisPariwisata();
		break;
	default:
		viewJenisPariwisata(true, '');
		break;
}

function deleteJenisPariwisata($id) {
	global $app;
	
	//Get object
	$objJenisPariwisata = $app->queryObject("SELECT * FROM jenispariwisata WHERE jparID='".$id."'");
	if (!$objJenisPariwisata) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM jenispariwisata WHERE jparID='".$id."'");
		
		$success = true;
		$msg = 'Jenis Pariwisata "'.$objJenisPariwisata->jparNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'jenispariwisata', $objJenisPariwisata->jparID, $objJenisPariwisata->jparNama, $msg);
	} else {
		$success = false;
		$msg = 'Jenis Pariwisata "'.$objJenisPariwisata->jparNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewJenisPariwisata($success, $msg);
}

function editJenisPariwisata($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objJenisPariwisata = $app->queryObject("SELECT * FROM jenispariwisata WHERE jparID='".$id."'");
		if (!$objJenisPariwisata) {
			$app->showPageError();
			exit();
		}
	} else {
		$objJenisPariwisata = new JenisPariwisata_Model();
	}
	
	JenisPariwisata_View::editJenisPariwisata(true, '', $objJenisPariwisata);
}

function saveJenisPariwisata() {
	global $app;
	
	//Create object
	$objJenisPariwisata = new JenisPariwisata_Model();
	$app->bind($objJenisPariwisata);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objJenisPariwisata->jparNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT jparID AS id FROM jenispariwisata WHERE jparNama='".$objJenisPariwisata->jparNama."'", $objJenisPariwisata->jparID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objJenisPariwisata->jparNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objJenisPariwisata->jparID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objJenisPariwisata);
		} else {
			$sql = $app->createSQLforUpdate($objJenisPariwisata);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objJenisPariwisata->jparID = $app->queryField("SELECT LAST_INSERT_ID() FROM jenispariwisata");
			
			$success = true;
			$msg = 'Jenis Pariwisata "'.$objJenisPariwisata->jparNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'jenispariwisata', $objJenisPariwisata->jparID, $objJenisPariwisata->jparNama, $msg);
		} else {
			$success = true;
			$msg = 'Jenis Pariwisata "'.$objJenisPariwisata->jparNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'jenispariwisata', $objJenisPariwisata->jparID, $objJenisPariwisata->jparNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Jenis Pariwisata "'.$objJenisPariwisata->jparNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewJenisPariwisata($success, $msg);
	} else {
		JenisPariwisata_View::editJenisPariwisata($success, $msg, $objJenisPariwisata);
	}
}

function viewJenisPariwisata($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('jenispariwisata', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('jenispariwisata', 'sort', 'jparNama');
	$dir		= $app->pageVar('jenispariwisata', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('jenispariwisata', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "jparNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM jenispariwisata
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM jenispariwisata
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	JenisPariwisata_View::viewJenisPariwisata($success, $msg, $arr);
}
?>