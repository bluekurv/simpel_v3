//Variables
var form = $("#myForm");
var konfKey = $("#konfKey");
var konfValue = $("#konfValue");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'konfKey' || this.id === undefined) {
		if (konfKey.val().length == 0){
			doSubmit = false;
			konfKey.addClass("error");		
		} else {
			konfKey.removeClass("error");
		}
	}
	
	if (this.id == 'konfValue' || this.id === undefined) {
		if (konfValue.val().length == 0){
			doSubmit = false;
			konfValue.addClass("error");		
		} else {
			konfValue.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
konfKey.blur(validateForm);
konfValue.blur(validateForm);

konfKey.keyup(validateForm);
konfValue.keyup(validateForm);

form.submit(submitForm);

//Set focus
konfKey.focus();