<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.konfigurasi.php';
require_once 'view.konfigurasi.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKonfigurasi($app->id);
		break;
	case 'delete':
		deleteKonfigurasi($app->id);
		break;
	case 'save':
		saveKonfigurasi();
		break;
	default:
		viewKonfigurasi(true, '');
		break;
}

function deleteKonfigurasi($id) {
	global $app;
	
	//Get object
	$objKonfigurasi = $app->queryObject("SELECT * FROM konfigurasi WHERE konfID='".$id."'");
	if (!$objKonfigurasi) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM konfigurasi WHERE konfID='".$id."'");
		
		$success = true;
		$msg = 'Konfigurasi "'.$objKonfigurasi->konfKey.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'konfigurasi', $objKonfigurasi->konfID, $objKonfigurasi->konfKey, $msg);
	} else {
		$success = false;
		$msg = 'Konfigurasi "'.$objKonfigurasi->konfKey.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKonfigurasi($success, $msg);
}

function editKonfigurasi($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKonfigurasi = $app->queryObject("SELECT * FROM konfigurasi WHERE konfID='".$id."'");
		if (!$objKonfigurasi) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKonfigurasi = new Konfigurasi_Model();
	}
	
	Konfigurasi_View::editKonfigurasi(true, '', $objKonfigurasi);
}

function saveKonfigurasi() {
	global $app;
	
	//Create object
	$objKonfigurasi = new Konfigurasi_Model();
	//$app->bind($objKonfigurasi);
	$objKonfigurasi->konfID = intval($_REQUEST['konfID']);
	$objKonfigurasi->konfKey = trim($_REQUEST['konfKey']);
	$objKonfigurasi->konfValue = trim($_REQUEST['konfValue']);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKonfigurasi->konfKey == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKonfigurasi->konfValue == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT konfID AS id FROM konfigurasi WHERE konfKey='".$objKonfigurasi->konfKey."'", $objKonfigurasi->konfID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objKonfigurasi->konfKey.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT konfID AS id FROM konfigurasi WHERE konfValue='".$objKonfigurasi->konfValue."'", $objKonfigurasi->konfID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objKonfigurasi->konfValue.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKonfigurasi->konfID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKonfigurasi);
		} else {
			$sql = $app->createSQLforUpdate($objKonfigurasi);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKonfigurasi->konfID = $app->queryField("SELECT LAST_INSERT_ID() FROM konfigurasi");
			
			$success = true;
			$msg = 'Konfigurasi "'.$objKonfigurasi->konfKey.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'konfigurasi', $objKonfigurasi->konfID, $objKonfigurasi->konfValue, $msg);
		} else {
			$success = true;
			$msg = 'Konfigurasi "'.$objKonfigurasi->konfKey.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'konfigurasi', $objKonfigurasi->konfID, $objKonfigurasi->konfValue, $msg);
		}
	} else {
		$success = false;
		$msg = 'Konfigurasi "'.$objKonfigurasi->konfKey.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKonfigurasi($success, $msg);
	} else {
		Konfigurasi_View::editKonfigurasi($success, $msg, $objKonfigurasi);
	}
}

function viewKonfigurasi($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('konfigurasi', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('konfigurasi', 'sort', 'konfKey');
	$dir		= $app->pageVar('konfigurasi', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('konfigurasi', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(konfKey LIKE '%".$nama."%' OR konfValue LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM konfigurasi
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM konfigurasi
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Konfigurasi_View::viewKonfigurasi($success, $msg, $arr);
}
?>