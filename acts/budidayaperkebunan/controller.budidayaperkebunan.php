<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.budidayaperkebunan.php';
require_once 'view.budidayaperkebunan.php';

switch ($app->task) {
	case 'add':
		addBudidayaPerkebunan($app->id);
		break;
	case 'edit':
		editBudidayaPerkebunan($app->id);
		break;
	case 'delete':
		deleteBudidayaPerkebunan($app->id);
		break;
	case 'print':
		printBudidayaPerkebunan($app->id);
		break;
	case 'read':
		readBudidayaPerkebunan($app->id);
		break;
	case 'save':
		saveBudidayaPerkebunan();
		break;
	default:
		viewBudidayaPerkebunan(true, '');
		break;
}

function addBudidayaPerkebunan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editBudidayaPerkebunan($id);
}

function deleteBudidayaPerkebunan($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM budidayaperkebunan WHERE bpDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Izin BudidayaPerkebunan Permukaan berhasil dihapus';
	
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Izin BudidayaPerkebunan Permukaan', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function editBudidayaPerkebunan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objBudidayaPerkebunan = $app->queryObject("SELECT * FROM budidayaperkebunan WHERE bpDmhnID='".$id."'");
	if (!$objBudidayaPerkebunan) {
		$objBudidayaPerkebunan = new BudidayaPerkebunan_Model();
	}
	
	$objBudidayaPerkebunan->bpDmhnID = $objDetailPermohonan->dmhnID;
	
	BudidayaPerkebunan_View::editBudidayaPerkebunan(true, '', $objDetailPermohonan, $objBudidayaPerkebunan);
}

function printBudidayaPerkebunan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN budidayaperkebunan ON bpDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objBudidayaPerkebunan = $app->queryObject($sql);
	if (!$objBudidayaPerkebunan) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objBudidayaPerkebunan->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	//HACK
	if ($objBudidayaPerkebunan->bpAlamat != '') {
		$objBudidayaPerkebunan->perAlamat = $objBudidayaPerkebunan->bpAlamat;
	}
	if ($objBudidayaPerkebunan->bpKelurahanID > 0) {
		$objBudidayaPerkebunan->perKelurahanID = $objBudidayaPerkebunan->bpKelurahanID;
	}
	
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objBudidayaPerkebunan->perKelurahanID."'");
	$objBudidayaPerkebunan->perKelurahan = $objKelurahan->wilNama;
	$objBudidayaPerkebunan->perKecamatan = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");
	
	$objBudidayaPerkebunan->kebun = $app->queryArrayOfObjects("SELECT dbpID AS id, detailbudidayaperkebunan.* FROM detailbudidayaperkebunan WHERE dbpBpID='".$objBudidayaPerkebunan->bpID."' ORDER BY dbpNo");
	
	BudidayaPerkebunan_View::printBudidayaPerkebunan($objBudidayaPerkebunan);
}

function readBudidayaPerkebunan($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN budidayaperkebunan ON bpDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objBudidayaPerkebunan = $app->queryObject($sql);
	if (!$objBudidayaPerkebunan) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objBudidayaPerkebunan->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	BudidayaPerkebunan_View::readBudidayaPerkebunan($objBudidayaPerkebunan);
}

function saveBudidayaPerkebunan() {
	global $app;
	
	//Get object
	$objBudidayaPerkebunanSebelumnya = $app->queryObject("SELECT * FROM budidayaperkebunan WHERE bpID='".$app->getInt('bpID')."'");
	if (!$objBudidayaPerkebunanSebelumnya) {
		$objBudidayaPerkebunanSebelumnya = new BudidayaPerkebunan_Model();
	}
	
	//Create object
	$objBudidayaPerkebunan = new BudidayaPerkebunan_Model();
	$app->bind($objBudidayaPerkebunan);
	
	//Modify object (if necessary)
	$objBudidayaPerkebunan->bpTglLahir = $app->NormalDateToMySQL($objBudidayaPerkebunan->bpTglLahir);
	$objBudidayaPerkebunan->bpTglPengesahan = $app->NormalDateToMySQL($objBudidayaPerkebunan->bpTglPengesahan);
	
	if ($objBudidayaPerkebunan->bpTglPengesahan != '0000-00-00') {
		if ($objBudidayaPerkebunanSebelumnya->bpNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objBudidayaPerkebunan->bpNo = intval($app->queryField("SELECT MAX(bpNo) AS nomor FROM budidayaperkebunan WHERE YEAR(bpTglPengesahan)='".substr($objBudidayaPerkebunan->bpTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objBudidayaPerkebunan->bpTglBerlaku = date('Y-m-d', strtotime($objBudidayaPerkebunan->bpTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objBudidayaPerkebunan->bpTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objBudidayaPerkebunan->bpTglDaftarUlang = date('Y-m-d', strtotime($objBudidayaPerkebunan->bpTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objBudidayaPerkebunan->bpTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objBudidayaPerkebunan->bpNo = 0;
		
		$objBudidayaPerkebunan->bpTglBerlaku = '0000-00-00';
		$objBudidayaPerkebunan->bpTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objBudidayaPerkebunan->bpNoLengkap = '504/'.CFG_COMPANY_SHORT_NAME.'/'.substr($objBudidayaPerkebunan->bpTglPengesahan,0,4).'/'.$objBudidayaPerkebunan->bpNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objBudidayaPerkebunan->bpNo > 0){
		$isExist = $app->isExist("SELECT bpID AS id FROM budidayaperkebunan WHERE bpNo='".$objBudidayaPerkebunan->bpNo."' AND YEAR(bpTglPengesahan)='".substr($objBudidayaPerkebunan->bpTglPengesahan,0,4)."'", $objBudidayaPerkebunan->bpID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objBudidayaPerkebunan->bpNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objBudidayaPerkebunan->bpID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objBudidayaPerkebunan);
		} else {
			$sql = $app->createSQLforUpdate($objBudidayaPerkebunan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objBudidayaPerkebunan->bpID = $app->queryField("SELECT LAST_INSERT_ID() FROM budidayaperkebunan");
		}
		
		$success = true;
		$msg = 'Izin BudidayaPerkebunan Permukaan berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'bp', $objBudidayaPerkebunan->bpID, $objBudidayaPerkebunan->bpNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objBudidayaPerkebunan->bpDmhnID."'");
	} else {
		$success = false;
		$msg = 'Izin BudidayaPerkebunan Permukaan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		if (isset($_REQUEST['btnSave'])) {
			header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
		} else {
			header("Location:index2.php?act=detailbudidayaperkebunan&id=".$objBudidayaPerkebunan->bpID);
		}
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objBudidayaPerkebunan->bpDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		BudidayaPerkebunan_View::editBudidayaPerkebunan($success, $msg, $objDetailPermohonan, $objBudidayaPerkebunan);
	}
}

function viewBudidayaPerkebunan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('bp', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('bp', 'sort', 'bpTglPengesahan');
	//$dir		= $app->pageVar('bp', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('bp', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('bp', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(bpNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(bpTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM budidayaperkebunan
				  LEFT JOIN detailpermohonan ON bpDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM budidayaperkebunan
			LEFT JOIN detailpermohonan ON bpDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(bpTglPengesahan) DESC, bpNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	BudidayaPerkebunan_View::viewBudidayaPerkebunan($success, $msg, $arr);
}
?>