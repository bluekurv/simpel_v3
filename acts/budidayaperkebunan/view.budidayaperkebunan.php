<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class BudidayaPerkebunan_View {
	static function editBudidayaPerkebunan($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=budidayaperkebunan&task=edit&id=<?php echo $obj->bpDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Surat Tanda Daftar Usaha Budidaya Perkebunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=budidayaperkebunan&task=save" method="POST" >
				<input type="hidden" id="bpID" name="bpID" value="<?php echo $obj->bpID; ?>" />
				<input type="hidden" id="bpDmhnID" name="bpDmhnID" value="<?php echo $obj->bpDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" name="btnSave" class="button-link inline dark-blue" value="Simpan" />
					<input type="submit" name="btnSaveAndManage" class="button-link inline dark-blue" value="Simpan dan Kelola Kebun" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" name="btnSave" class="button-link inline dark-blue" value="Simpan" />
					<input type="submit" name="btnSaveAndManage" class="button-link inline dark-blue" value="Simpan dan Kelola Kebun" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('bpGrupID', $obj->bpGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=budidayaperkebunan&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Keterangan Pemilik</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="bpNama" name="bpNama" maxlength="50" size="50" value="<?php echo $obj->bpNama; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tempat Lahir</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="bpTempatLahir" name="bpTempatLahir" maxlength="50" size="50" value="<?php echo $obj->bpTempatLahir; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Lahir</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="bpTglLahir" name="bpTglLahir" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->bpTglLahir); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="bpNoKTP" name="bpNoKTP" maxlength="50" size="50" value="<?php echo $obj->bpNoKTP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="bpAlamat" name="bpAlamat" maxlength="255" size="50" value="<?php echo $obj->bpAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan/Desa</td><td></td>
					<td>
<?php
		$sql = "SELECT w.wilID AS id, w.wilNama AS nama FROM wilayah AS w, wilayah AS w2, wilayah AS w3 WHERE w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilNama='Pelalawan' ORDER BY w.wilNama";
		$app->createSelect('bpKelurahanID', $sql, $obj->bpKelurahanID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->bpNo > 0) {
?>
						<span>KPTS 540/<?php echo CFG_COMPANY_SHORT_NAME; ?>/<?php echo substr($obj->bpTglPengesahan,0,4); ?>/</span>
						<input class="box" id="bpNo" name="bpNo" maxlength="5" size="5" value="<?php echo $obj->bpNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="bpNo" name="bpNo" value="<?php echo $obj->bpNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="bpTglPengesahan" name="bpTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->bpTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="bpPejID" name="bpPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->bpPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='budidayaperkebunan'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->bpTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->bpTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="bpTglBerlaku" name="bpTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->bpTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->bpTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->bpTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="bpTglDaftarUlang" name="bpTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->bpTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/budidayaperkebunan/js/edit.budidayaperkebunan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printBudidayaPerkebunan($obj) {
		global $app;
		
		$objReport 		= new Report('Budidaya Perkebunan', 'BudidayaPerkebunan-'.substr($obj->bpTglPengesahan,0,4).'-'.$obj->bpNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->bpPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<h3 align="center">SURAT TANDA DAFTAR USAHA BUDIDAYA PERKEBUNAN (STD-B)</h3>
<?php 
		$sql = "SELECT w.wilNama AS kelurahan, w2.wilNama AS kecamatan, w3.wilNama AS kabupatenkota, w4.wilNama AS provinsi 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->bpKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$objWilayah = $app->queryObject($sql);
		
		if (!$objWilayah) {
			$objWilayah->kelurahan = '';
			$objWilayah->kecamatan = '';
			$objWilayah->kabupatenkota = '';
			$objWilayah->provinsi = '';
		}
?>
	<p>Kabupaten : <?php echo $objWilayah->kabupatenkota; ?><br>Kecamatan : <?php echo $objWilayah->kecamatan; ?></p>
	<hr>
	<p>Nomor : <?php echo $obj->bpNoLengkap; ?></p>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="5%" align="left" valign="top">A.</td>
        <td width="30%" colspan="2" align="left" valign="top">Keterangan Pemilik</td>
        <td width="7" align="left" valign="top"></td>
		<td width="60%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%" align="left" valign="top">1.</td>
        <td width="30%">Nama</td>
        <td width="7">:</td>
        <td width="60%"><?php echo strtoupper($obj->bpNama); ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%" align="left" valign="top">2.</td>
        <td width="30%">Tempat / Tanggal Lahir</td>
        <td width="7">:</td>
        <td width="60%"><?php echo strtoupper($obj->bpTempatLahir).' / '.$app->MySQLDateToNormal($obj->bpTglLahir); ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%" align="left" valign="top">3.</td>
        <td width="30%">Nomor KTP</td>
        <td width="7">:</td>
        <td width="60%"><?php echo strtoupper($obj->bpNoKTP); ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%" align="left" valign="top">4.</td>
        <td width="30%">Alamat</td>
        <td width="7">:</td>
        <td width="60%"><?php echo strtoupper($obj->bpAlamat); ?></td>
	</tr>
	<tr>
		<td colspan="5"></td>
	</tr>
	<tr>
		<td width="5%" align="left" valign="top">B.</td>
        <td width="30%" colspan="2" align="left" valign="top">Data Kebun</td>
        <td width="7" align="left" valign="top"></td>
		<td width="60%" align="left" valign="top"></td>
	</tr>
	</table>
	
	<table border="0" cellpadding="0" cellspacing="0">
<?php 
		$i = 1;
		foreach ($obj->kebun as $v) {
			if ($i > 1) {
?>
	<tr>
		<td colspan="5">&nbsp;</td>
	</tr>
<?php 
			}
?>
	<tr>
		<td width="5%"></td>
		<td width="5%" align="left" valign="top"><?php echo $app->getRoman($i); ?>.</td>
        <td width="30%">Kebun <?php echo $i; ?></td>
        <td width="7"></td>
        <td width="60%"></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Lokasi / Titik Koordinat Kebun</td>
        <td width="7">:</td>
        <td width="60%">
<?php 
			$arr = explode('][', substr($v->dbpKoordinat,1,strlen($v->dbpKoordinat)-2));
			$arrKoordinat = array();
			foreach ($arr as $k2=>$v2) {
				$arrKoordinat[$k2] = explode("|", $v2);
			}
			
			foreach ($arrKoordinat as $k2=>$v2) {
				echo 'N '.$v2[0].'&deg; '.$v2[1].'&#39; '.$v2[2].'&#34; ';
				echo 'E '.$v2[3].'&deg; '.$v2[4].'&#39; '.$v2[5].'&#34;<br>';
			}
?>
        </td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Desa/Kecamatan</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $objWilayah->kelurahan; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Status Kepemilikan Lahan</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpStatusLahan; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Nomor</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpNoLahan; ?> Tgl. <?php echo $app->MySQLDateToIndonesia($v->dbpTglLahan); ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Luas Areal</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpLuasAreal; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Jenis Tanaman</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpJenisTanaman; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Produksi per Ha per Tahun</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpProduksiPerHaPerTahun; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Asal Benih</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpAsalBenih; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Jumlah Pohon</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpJumlahPohon; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Pola Tanam</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpPolaTanam; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Jenis Pupuk</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpJenisPupuk; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Mitra Pengolahan</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpMitraPengolahan; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Jenis Tanah</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpJenisTanah; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Tahun Tanam</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpTahunTanam; ?></td>
	</tr>
	<tr>
		<td width="5%"></td>
		<td width="5%"></td>
        <td width="30%">- Usaha Lain di Lahan Kebun</td>
        <td width="7">:</td>
        <td width="60%"><?php echo $v->dbpUsahaLain; ?></td>
	</tr>
<?php 
			$i++;
		}
?>
	</table>
	
	<p>STD-B ini tidak berlaku apabila terjadi perubahan terhadap informasi tersebut di atas.</p>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%" align="center">
			<table>
			<tr>
				<td align="center"><b>Pangkalan Kerinci, <?php echo $app->MySQLDateToIndonesia($obj->bpTglPengesahan); ?></b></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<table border="0" cellpadding="1" cellspacing="0" width="100%">
					<tr>
						<td align="center">
<?php 
		if ($obj->bpTandaTangan == 'atas nama') {		
?>
							<b>a.n. KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
							<br><b><?php echo $objPengesahan->pnJabatan; ?></b>
<?php 
		} else if ($obj->bpTandaTangan == 'mewakili') {
?>
							<b>KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
							<br><b>Mewakili</b>
<?php 
		} else {
?>
							<b>KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
<?php 
		}
?>
							<br><br><br><br><br>
							<table width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td width="24%"></td>
								<td width="52%" align="center">
									<b><u><?php echo $objPengesahan->pnNama; ?></u></b><br>
									<?php echo $objPengesahan->pgrPangkat; ?><br>
									NIP. <?php echo $objPengesahan->pnNIP; ?>
								</td>
								<td width="24%"></td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readBudidayaPerkebunan($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=budidayaperkebunan&task=edit&id=<?php echo $obj->bpDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Pengambilan BudidayaPerkebunan yang Bersumber dari BudidayaPerkebunan Permukaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=budidayaperkebunan&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=budidayaperkebunan&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->bpNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->bpTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->bpPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->bpTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->bpTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->bpTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->bpTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/budidayaperkebunan/js/read.budidayaperkebunan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewBudidayaPerkebunan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=budidayaperkebunan"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Pengambilan BudidayaPerkebunan yang Bersumber dari BudidayaPerkebunan Permukaan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('bpNo', 'Nomor Surat', true),
			$app->setHeader('bpTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('bpTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('bpTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=budidayaperkebunan&task=read&id='.$v->bpDmhnID.'&fromact=budidayaperkebunan" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->bpDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=budidayaperkebunan&task=read&id='.$v->bpDmhnID.'&fromact=budidayaperkebunan" title="Entri">'.$v->bpNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->bpTglPengesahan).'</td>';
				
				if ($v->bpTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->bpTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->bpTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->bpTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/budidayaperkebunan/js/budidayaperkebunan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>