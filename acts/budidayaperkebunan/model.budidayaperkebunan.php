<?php
class BudidayaPerkebunan_Model {	
	public $tabel = 'budidayaperkebunan';
	public $primaryKey = 'bpID';
	
	public $bpID = 0;
	public $bpDmhnID = 0;
	public $bpGrupID = 0;
	public $bpNo = 0;
	public $bpNoLengkap = '';
	
	public $bpNama = '';
	public $bpTempatLahir = '';
	public $bpTglLahir = '0000-00-00';
	public $bpNoKTP = '';
	public $bpAlamat = '';
	public $bpKelurahanID = 0;
	
	public $bpTglBerlaku = '0000-00-00';	
	public $bpTglDaftarUlang = '0000-00-00';
	public $bpPejID = 0;
	public $bpTglPengesahan = '0000-00-00'; 
	public $bpArsip = '';
}
?>