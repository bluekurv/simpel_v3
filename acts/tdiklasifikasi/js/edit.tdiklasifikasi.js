//Variables
var form = $("#myForm");
var tklaKode = $("#tklaKode");
var tklaNama = $("#tklaNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'tklaKode' || this.id === undefined) {
		if (tklaKode.val().length == 0){
			doSubmit = false;
			tklaKode.addClass("error");		
		} else {
			tklaKode.removeClass("error");
		}
	}
	
	if (this.id == 'tklaNama' || this.id === undefined) {
		if (tklaNama.val().length == 0){
			doSubmit = false;
			tklaNama.addClass("error");		
		} else {
			tklaNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
tklaKode.blur(validateForm);
tklaNama.blur(validateForm);

tklaKode.keyup(validateForm);
tklaNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
tklaKode.focus();