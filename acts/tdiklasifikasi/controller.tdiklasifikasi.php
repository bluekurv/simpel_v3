<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.tdiklasifikasi.php';
require_once 'view.tdiklasifikasi.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editTDIKlasifikasi($app->id);
		break;
	case 'delete':
		deleteTDIKlasifikasi($app->id);
		break;
	case 'save':
		saveTDIKlasifikasi();
		break;
	default:
		viewTDIKlasifikasi(true, '');
		break;
}

function deleteTDIKlasifikasi($id) {
	global $app;
	
	//Get object
	$objTDIKlasifikasi = $app->queryObject("SELECT * FROM tdiklasifikasi WHERE tklaID='".$id."'");
	if (!$objTDIKlasifikasi) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM tdiklasifikasi WHERE tklaID='".$id."'");
		
		$success = true;
		$msg = 'TDIKlasifikasi "'.$objTDIKlasifikasi->tklaNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'tdiklasifikasi', $objTDIKlasifikasi->tklaID, $objTDIKlasifikasi->tklaNama, $msg);
	} else {
		$success = false;
		$msg = 'TDIKlasifikasi "'.$objTDIKlasifikasi->tklaNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewTDIKlasifikasi($success, $msg);
}

function editTDIKlasifikasi($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objTDIKlasifikasi = $app->queryObject("SELECT * FROM tdiklasifikasi WHERE tklaID='".$id."'");
		if (!$objTDIKlasifikasi) {
			$app->showPageError();
			exit();
		}
	} else {
		$objTDIKlasifikasi = new TDIKlasifikasi_Model();
	}
	
	TDIKlasifikasi_View::editTDIKlasifikasi(true, '', $objTDIKlasifikasi);
}

function saveTDIKlasifikasi() {
	global $app;
	
	//Create object
	$objTDIKlasifikasi = new TDIKlasifikasi_Model();
	$app->bind($objTDIKlasifikasi);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objTDIKlasifikasi->tklaKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objTDIKlasifikasi->tklaNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT tklaID AS id FROM tdiklasifikasi WHERE tklaKode='".$objTDIKlasifikasi->tklaKode."'", $objTDIKlasifikasi->tklaID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objTDIKlasifikasi->tklaKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT tklaID AS id FROM tdiklasifikasi WHERE tklaNama='".$objTDIKlasifikasi->tklaNama."'", $objTDIKlasifikasi->tklaID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objTDIKlasifikasi->tklaNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objTDIKlasifikasi->tklaID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objTDIKlasifikasi);
		} else {
			$sql = $app->createSQLforUpdate($objTDIKlasifikasi);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objTDIKlasifikasi->tklaID = $app->queryField("SELECT LAST_INSERT_ID() FROM tdiklasifikasi");
			
			$success = true;
			$msg = 'TDIKlasifikasi "'.$objTDIKlasifikasi->tklaNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'tdiklasifikasi', $objTDIKlasifikasi->tklaID, $objTDIKlasifikasi->tklaNama, $msg);
		} else {
			$success = true;
			$msg = 'TDIKlasifikasi "'.$objTDIKlasifikasi->tklaNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'tdiklasifikasi', $objTDIKlasifikasi->tklaID, $objTDIKlasifikasi->tklaNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'TDIKlasifikasi "'.$objTDIKlasifikasi->tklaNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewTDIKlasifikasi($success, $msg);
	} else {
		TDIKlasifikasi_View::editTDIKlasifikasi($success, $msg, $objTDIKlasifikasi);
	}
}

function viewTDIKlasifikasi($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('tdiklasifikasi', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('tdiklasifikasi', 'sort', 'tklaKode');
	$dir		= $app->pageVar('tdiklasifikasi', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('tdiklasifikasi', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(tklaKode LIKE '%".$nama."%' OR tklaNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM tdiklasifikasi
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM tdiklasifikasi
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	TDIKlasifikasi_View::viewTDIKlasifikasi($success, $msg, $arr);
}
?>