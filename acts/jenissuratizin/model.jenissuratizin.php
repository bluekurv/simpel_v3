<?php
class JenisSuratIzin_Model {	
	public $tabel = 'jenissuratizin';
	public $primaryKey = 'jsiID';
	
	public $jsiID = 0;
	public $jsiBidID = 0;
	public $jsiNama = '';
	public $jsiNamaSingkat = '';
	public $jsiSubNama = '';
	public $jsiBiayaLeges = 0;
	public $jsiRekID = 0;
	public $jsiLamaPengurusan1 = 0;
	public $jsiPerluSurvey1 = 0;
	public $jsiLamaPengurusan2 = 0;
	public $jsiPerluSurvey2 = 0;
	public $jsiMasaBerlaku = 0;
	public $jsiMasaDaftarUlang = 0;
	public $jsiTampil = 0;
	public $jsiKode = '';
	public $jsiNomor = 0;
	public $jsiTipe = '00';
}
?>