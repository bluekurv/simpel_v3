//Variables
var form = $("#myForm");
var jsiNama = $("#jsiNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'jsiNama' || this.id === undefined) {
		if (jsiNama.val().length == 0){
			doSubmit = false;
			jsiNama.addClass("error");		
		} else {
			jsiNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
jsiNama.blur(validateForm);

jsiNama.keyup(validateForm);

form.submit(submitForm);

$("#jsiBiayaLeges").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#jsiBiayaAdministrasi").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });

//Set focus
jsiNama.focus();