<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Administrasi']['list'] = true;
$acl['Petugas Administrasi']['read'] = true;
$acl['Petugas Loket Informasi']['list'] = true;
$acl['Petugas Loket Informasi']['read'] = true;
$acl['Petugas Loket Pelayanan']['list'] = true;
$acl['Petugas Loket Pelayanan']['read'] = true;
$acl['Petugas Loket Pengaduan']['list'] = true;
$acl['Petugas Loket Pengaduan']['read'] = true;
$acl['Petugas Loket Penyerahan']['list'] = true;
$acl['Petugas Loket Penyerahan']['read'] = true;
$acl['Surveyor']['list'] = true;
$acl['Surveyor']['read'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.jenissuratizin.php';
require_once 'view.jenissuratizin.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editJenisSuratIzin($app->id);
		break;
	case 'delete':
		deleteJenisSuratIzin($app->id);
		break;
	case 'list':
		listJenisSuratIzin();
		break;
	case 'read':
		readJenisSuratIzin($app->id);
		break;
	case 'save':
		saveJenisSuratIzin();
		break;
	default:
		viewJenisSuratIzin(true, '');
		break;
}

function deleteJenisSuratIzin($id) {
	global $app;
	
	//Get object
	$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$id."'");
	if (!$objJenisSuratIzin) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalSyaratSuratIzin = intval($app->queryField("SELECT COUNT(*) AS total FROM syaratsuratizin WHERE ssiJsiID='".$id."'"));
	if ($totalSyaratSuratIzin > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalSyaratSuratIzin.' syarat untuk jenis surat izin tersebut';
	}
	
	$totalLoketSuratIzin = intval($app->queryField("SELECT COUNT(*) AS total FROM loketsuratizin WHERE lsiJsiID='".$id."'"));
	if ($totalLoketSuratIzin > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalLoketSuratIzin.' loket untuk jenis surat izin tersebut';
	}
	
	//Query
	if ($doDelete) {
		//$app->query("DELETE FROM jenissuratizin WHERE jsiID='".$id."'");
		
		$success = true;
		$msg = 'Jenis Surat Izin "'.$objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : '').'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'jenissuratizin', $objJenisSuratIzin->jsiID, $objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : ''), $msg);
	} else {
		$success = false;
		$msg = 'Jenis Surat Izin "'.$objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : '').'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewJenisSuratIzin($success, $msg);
}

function editJenisSuratIzin($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$id."'");
		if (!$objJenisSuratIzin) {
			$app->showPageError();
			exit();
		}
	} else {
		$objJenisSuratIzin = new JenisSuratIzin_Model();
	}
	
	JenisSuratIzin_View::editJenisSuratIzin(true, '', $objJenisSuratIzin);
}

function listJenisSuratIzin() {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('jenissuratizin', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('jenissuratizin', 'sort', 'jsiNama');
	$dir   		= $app->pageVar('jenissuratizin', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('jenissuratizin', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "jsiNama LIKE '%".$nama."%'";
	}
	$filter[] = "jsiTampil=1";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM jenissuratizin
				  LEFT JOIN rekening ON jsiRekID=rekID
				  LEFT JOIN bidang ON jsiBidID=bidID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *, (SELECT COUNT(*) AS total FROM syaratsuratizin WHERE ssiJsiID=jsiID) AS total_syarat
			FROM jenissuratizin
			LEFT JOIN rekening ON jsiRekID=rekID
			LEFT JOIN bidang ON jsiBidID=bidID
			".$where." 
			ORDER BY ".$sort." ".$dir.", jsiNama ASC 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	JenisSuratIzin_View::listJenisSuratIzin($arr);
}

function readJenisSuratIzin($id) {
	global $app;
	
	//Query
	$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiID='".$id."'");
	if (!$objJenisSuratIzin) {
		$app->showPageError();
		exit();
	}
	
	$objJenisSuratIzin->syarat = array(
		'Umum' => array(),
		'Khusus' => array()
	);
	
	$sql = "SELECT * 
			FROM syaratsuratizin
			WHERE ssiJsiID='".$id."'
			ORDER BY ssiNo";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$objJenisSuratIzin->syarat[$obj->ssiUmumKhusus][] = $obj;
	}
	
	JenisSuratIzin_View::readJenisSuratIzin($objJenisSuratIzin);
}

function saveJenisSuratIzin() {
	global $app;
	
	//Create object
	$objJenisSuratIzin = new JenisSuratIzin_Model();
	$app->bind($objJenisSuratIzin);
	
	//Modify object (if necessary)
	$objJenisSuratIzin->jsiBiayaLeges = $app->MoneyToMySQL($objJenisSuratIzin->jsiBiayaLeges);
	if (isset($_REQUEST['jsiTampil'])) {
		$objJenisSuratIzin->jsiTampil = ($_REQUEST['jsiTampil'] == 'on') ? 1 : 0;
	} else {
		$objJenisSuratIzin->jsiTampil = 0;
	}
	if (isset($_REQUEST['jsiPerluSurvey1'])) {
		$objJenisSuratIzin->jsiPerluSurvey1 = ($_REQUEST['jsiPerluSurvey1'] == 'on') ? 1 : 0;
	} else {
		$objJenisSuratIzin->jsiPerluSurvey1 = 0;
	}
	if (isset($_REQUEST['jsiPerluSurvey2'])) {
		$objJenisSuratIzin->jsiPerluSurvey2 = ($_REQUEST['jsiPerluSurvey2'] == 'on') ? 1 : 0;
	} else {
		$objJenisSuratIzin->jsiPerluSurvey2 = 0;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objJenisSuratIzin->jsiNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objJenisSuratIzin->jsiBidID == 0) {
		$doSave = false;
		$msg[] = '- Bidang belum diisi';
	}
	
	/*$isExist = $app->isExist("SELECT jsiID AS id FROM jenissuratizin WHERE jsiNama='".$objJenisSuratIzin->jsiNama."'", $objJenisSuratIzin->jsiID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama "'.$objJenisSuratIzin->jsiNama.'" sudah ada';
	}*/
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objJenisSuratIzin->jsiID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objJenisSuratIzin);
		} else {
			$sql = $app->createSQLforUpdate($objJenisSuratIzin);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objJenisSuratIzin->jsiID = $app->queryField("SELECT LAST_INSERT_ID() FROM jenissuratizin");
			
			$success = true;
			$msg = 'Jenis Surat Izin "'.$objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : '').'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'jenissuratizin', $objJenisSuratIzin->jsiID, $objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : ''), $msg);
		} else {
			$success = true;
			$msg = 'Jenis Surat Izin "'.$objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : '').'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'jenissuratizin', $objJenisSuratIzin->jsiID, $objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : ''), $msg);
		}
	} else {
		$success = false;
		$msg = 'Jenis Surat Izin "'.$objJenisSuratIzin->jsiNama.($objJenisSuratIzin->jsiSubNama != '' ? ' ('.$objJenisSuratIzin->jsiSubNama.')' : '').'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewJenisSuratIzin($success, $msg);
	} else {
		JenisSuratIzin_View::editJenisSuratIzin($success, $msg, $objJenisSuratIzin);
	}
}

function viewJenisSuratIzin($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('jenissuratizin', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('jenissuratizin', 'sort', 'jsiNama');
	$dir   		= $app->pageVar('jenissuratizin', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('jenissuratizin', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "jsiNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM jenissuratizin
				  LEFT JOIN rekening ON jsiRekID=rekID
				  LEFT JOIN bidang ON jsiBidID=bidID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *, (SELECT COUNT(*) AS total FROM syaratsuratizin WHERE ssiJsiID=jsiID) AS total_syarat
			FROM jenissuratizin
			LEFT JOIN rekening ON jsiRekID=rekID
			LEFT JOIN bidang ON jsiBidID=bidID
			".$where." 
			ORDER BY ".$sort." ".$dir.", jsiNama ASC 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	JenisSuratIzin_View::viewJenisSuratIzin($success, $msg, $arr);
}
?>