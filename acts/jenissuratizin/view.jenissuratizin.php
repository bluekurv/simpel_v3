<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class JenisSuratIzin_View {
	static function editJenisSuratIzin($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=jenissuratizin&task=edit&id=<?php echo $obj->jsiID; ?>"><img src="images/icons/page_white_edit.png" border="0" /> <?php echo ($obj->jsiID > 0) ? 'Ubah' : 'Tambah'; ?> Jenis Surat Izin</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=jenissuratizin&task=save" method="POST" >
				<input type="hidden" id="jsiID" name="jsiID" value="<?php echo $obj->jsiID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=jenissuratizin">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="jsiNama" name="jsiNama" maxlength="500" size="100" value="<?php echo $obj->jsiNama; ?>" />
					</td>
				</tr>
				<tr>
					<td>Nama Singkat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="jsiNamaSingkat" name="jsiNamaSingkat" maxlength="50" size="50" value="<?php echo $obj->jsiNamaSingkat; ?>" />
					</td>
				</tr>
				<tr>
					<td>Sub Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="jsiSubNama" name="jsiSubNama" maxlength="500" size="100" value="<?php echo $obj->jsiSubNama; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
						(<i><b>Sub Nama</b> diisi jika terdapat surat izin dengan <b>Nama</b> yang sama namun memiliki persyaratan yang berbeda</i>)
					</td>
				</tr>
				<tr>
					<td>Rekening Retribusi</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelect('jsiRekID', "SELECT rekID AS id, CONCAT(rekNomor,' ',rekNama) AS nama FROM rekening ORDER BY rekNomor, rekNama", $obj->jsiRekID, array(0=>'--Tidak Ditentukan--'));
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Pola 1</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lama Penyelesaian</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="jsiLamaPengurusan1" name="jsiLamaPengurusan1">
<?php 
		for ($i=0; $i<=30; $i++) {
			echo '<option value="'.$i.'"';
			if ($i == $obj->jsiLamaPengurusan1) {
				echo ' selected';
			}
			echo ">".$i."</option>\n";
		}
?>
						</select>
						Hari Kerja (<i>Jika bernilai 0, berarti tidak menggunakan pola ini</i>)
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Ada Survey?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('jsiPerluSurvey1', 1, 'Ya', 0, 'Tidak', $obj->jsiPerluSurvey1);
?>
						<div style="margin-left:85px; padding-top:8px;">(<i>Jika ada survey, berarti lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima</i>)</div>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Pola 2</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lama Penyelesaian</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="jsiLamaPengurusan2" name="jsiLamaPengurusan2">
<?php 
		for ($i=0; $i<=30; $i++) {
			echo '<option value="'.$i.'"';
			if ($i == $obj->jsiLamaPengurusan2) {
				echo ' selected';
			}
			echo ">".$i."</option>\n";
		}
?>
						</select>
						Hari Kerja (<i>Jika bernilai 0, berarti tidak menggunakan pola ini</i>)
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Ada Survey?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('jsiPerluSurvey2', 1, 'Ya', 0, 'Tidak', $obj->jsiPerluSurvey2);
?>
						<div style="margin-left:85px; padding-top:8px;">(<i>Jika ada survey, berarti lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima</i>)</div>
					</td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<td>Masa Berlaku</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="jsiMasaBerlaku" name="jsiMasaBerlaku">
							<option value="0">Selama Ada</option>
<?php 
		for ($i=1; $i<=30; $i++) {
			echo '<option value="'.$i.'"';
			if ($i == $obj->jsiMasaBerlaku) {
				echo ' selected';
			}
			echo ">".$i." Tahun</option>\n";
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Daftar Ulang</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang">
							<option value="0">Tidak Ada</option>
<?php 
		for ($i=1; $i<=30; $i++) {
			echo '<option value="'.$i.'"';
			if ($i == $obj->jsiMasaDaftarUlang) {
				echo ' selected';
			}
			echo ">".$i." Tahun sekali</option>\n";
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Tipe Permohonan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="jsiTipe" name="jsiTipe">
<?php
		foreach (array('Perusahaan', 'Perseorangan') as $v) {
			echo '<option';
			if ($v == $obj->jsiTipe) {
				echo ' selected';
			}
			echo ">".$v."</option>\n";
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Bidang</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelect('jsiBidID', "SELECT bidID AS id, bidNama AS nama FROM bidang ORDER BY bidNama", $obj->jsiBidID);
?>
					</td>
				</tr>
				<tr>
					<td>Tampil?</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSwitchCheckBox('jsiTampil', 1, 'Ya', 0, 'Tidak', $obj->jsiTampil);
?>
					</td>
				</tr>
				<tr>
					<td>Kode</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="jsiKode" name="jsiKode" maxlength="500" size="50" value="<?php echo $obj->jsiKode; ?>" /> (<i>Dimasukkan oleh programmer</i>)
					</td>
				</tr>
				<tr>
					<td>Nomor</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="jsiNomor" name="jsiNomor" maxlength="50" size="50" value="<?php echo $obj->jsiNomor; ?>" /> (<i>Dimasukkan oleh programmer</i>)
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jenissuratizin/js/edit.jenissuratizin.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function listJenisSuratIzin($arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=jenissuratizin&task=list"><img src="images/icons/page_white_edit.png" border="0" /> Jenis Surat Izin</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 20),
			$app->setHeader('jsiNama', 'Nama', true),
			$app->setHeader('rekNomor', 'Rekening Retribusi', true),
			$app->setHeader('jsiPola1', 'Pola 1<br>(Hari Kerja)', false, 0, '', 2),
			$app->setHeader('jsiPola2', 'Pola 2<br>(Hari Kerja)', false, 0, '', 2),
			$app->setHeader('jsiMasaBerlaku', 'Masa Berlaku', false, 0, 'right'),
			$app->setHeader('jsiTipe', 'Tipe Permohonan', true),
			$app->setHeader('bidNama', 'Bidang', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=jenissuratizin&task=read&id='.$v->jsiID.'" title="Lihat"><img src="images/icons/zoom.png" border="0"></a></td>';
				echo '<td><a href="index2.php?act=jenissuratizin&task=read&id='.$v->jsiID.'">'.$v->jsiNama.($v->jsiSubNama != '' ? ' ('.$v->jsiSubNama.')' : '').'</a></td>';
				echo '<td>'.($v->rekNomor != '' ? $v->rekNomor : '0.0.0.00.00').'</td>';
				if ($v->jsiLamaPengurusan1 > 0) {
					echo '<td align="right">'.$v->jsiLamaPengurusan1.'</td>';
					echo '<td>'.($v->jsiPerluSurvey1 ? '<span class="badge ya">Ada Survey</span>' : '<span class="badge tidak">Tanpa Survey</span>').'</td>';
				} else {
					echo '<td colspan="2">-</td>';
				}
				if ($v->jsiLamaPengurusan2 > 0) {
					echo '<td align="right">'.$v->jsiLamaPengurusan2.'</td>';
					echo '<td>'.($v->jsiPerluSurvey2 ? '<span class="badge ya">Ada Survey</span>' : '<span class="badge tidak">Tanpa Survey</span>').'</td>';
				} else {
					echo '<td colspan="2">-</td>';
				}
				echo '<td align="right">'.($v->jsiMasaBerlaku == 0 ? 'Selama Ada' : $v->jsiMasaBerlaku.' Tahun').'</td>';
				echo '<td>'.$v->jsiTipe.'</td>';
				echo '<td>'.$v->bidNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="8">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jenissuratizin/js/list.jenissuratizin.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function readJenisSuratIzin($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=jenissuratizin&task=read&id=<?php echo $obj->jsiID; ?>"><img src="images/icons/page_white_edit.png" border="0" /> <?php echo $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : ''); ?></a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<table class="readTable" border="0" cellpadding="1" cellspacing="1">
			<tr>
				<td width="150">Nama</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : ''); ?></td>
			</tr>
			<tr>
				<td>Nama Singkat</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $obj->jsiNamaSingkat; ?></td>
			</tr>
			<!-- <tr>
				<td>Biaya Leges (Rp)</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $app->MySQLToMoney($obj->jsiBiayaLeges); ?></td>
			</tr>
			<tr>
				<td>Biaya Administrasi (Rp)</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $app->MySQLToMoney($obj->jsiBiayaAdministrasi); ?></td>
			</tr> -->
			<tr>
				<td>Rekening Retribusi</td><td>&nbsp;:&nbsp;</td>
				<td>
<?php 
		echo $app->queryField("SELECT CONCAT(rekNomor,' - ',rekNama) AS nama FROM rekening wHERE rekID='".$obj->jsiRekID."'");
?>
				</td>
			</tr>
			<tr>
				<td colspan="3"><b><u>Pola 1</u></b></td>
			</tr>
<?php 	if ($obj->jsiLamaPengurusan1 > 0) { ?>
			<tr>
				<td style="padding-left:20px;">Lama Penyelesaian</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $obj->jsiLamaPengurusan1; ?> Hari Kerja</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Ada Survey?</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $obj->jsiPerluSurvey1 ? 'Ya. Lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima' : 'Tidak'; ?></td>
			</tr>
<?php 	} else { ?>
			<tr>
				<td colspan="3" style="color:#FF0000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak menggunakan pola ini</td>
			</tr>
<?php 	} ?>
			<tr>
				<td colspan="3"><b><u>Pola 2</u></b></td>
			</tr>
<?php 	if ($obj->jsiLamaPengurusan2 > 0) { ?>
			<tr>
				<td style="padding-left:20px;">Lama Penyelesaian</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $obj->jsiLamaPengurusan2; ?> Hari Kerja</td>
			</tr>
			<tr>
				<td style="padding-left:20px;">Ada Survey?</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $obj->jsiPerluSurvey2 ? 'Ya. Lama penyelesaian terhitung setelah survey lapangan dan berkas lengkap diterima' : 'Tidak'; ?></td>
			</tr>
<?php 	} else { ?>
			<tr>
				<td colspan="3" style="color:#FF0000;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tidak menggunakan pola ini</td>
			</tr>
<?php 	} ?>
			<tr>
				<td>Masa Berlaku</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo ($obj->jsiMasaBerlaku == 0) ? 'Selama Ada' : $obj->jsiMasaBerlaku.' Tahun'; ?>
				</td>
			</tr>
			<tr>
				<td>Tipe Permohonan</td><td>&nbsp;:&nbsp;</td>
				<td><?php echo $obj->jsiTipe; ?></td>
			</tr>
			<tr>
				<td>Bidang</td><td>&nbsp;:&nbsp;</td>
				<td>
<?php 
		echo $app->queryField("SELECT bidNama AS nama FROM bidang wHERE bidID='".$obj->jsiBidID."'");
?>
				</td>
			</tr>
			</table>
<?php 
		if (count($obj->syarat['Umum']) > 0) {
			echo '<h2>Persyaratan Umum</h2>';
			echo '<table class="dataTable">';
			echo '<tr>';
			echo '<th></th>';
			echo '<th>Syarat</th>';
			echo '<th>Minimal</th>';
			echo '<th>Pembuatan</th>';
			echo '</tr>';
			
			$i = 1;
			foreach ($obj->syarat['Umum'] as $v) {
				echo '<tr>';
				echo '<td width="40" align="right">'.$i.'.</td>';
				echo '<td width="60%">'.$v->ssiNama.($v->ssiJumlah > 1 ? ' sebanyak '.$v->ssiJumlah.' lembar' : '').'</td>';
				echo '<td>'.($v->ssiMinimal ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				$pembuatan = array();
				if ($v->ssiBaru) {
					$pembuatan[] = '<span class="badge open">Baru</span>';
				} else {
					$pembuatan[] = '<span class="badge none">Baru</span>';
				}
				if ($v->ssiPerpanjangan) {
					$pembuatan[] = '<span class="badge close">Perpanjangan</span>';
				} else {
					$pembuatan[] = '<span class="badge none">Perpanjangan</span>';
				}
				if ($v->ssiPerubahan) {
					$pembuatan[] = '<span class="badge info">Perubahan</span>';
				} else {
					$pembuatan[] = '<span class="badge none">Perubahan</span>';
				}
				echo '<td>'.implode('', $pembuatan).'</td>';
				echo '</tr>';
				$i++;
			}
			echo '</table>';
		}

		if (count($obj->syarat['Khusus']) > 0) {
			echo '<h2>Persyaratan Khusus</h2>';
			echo '<table class="dataTable">';
			echo '<tr>';
			echo '<th></th>';
			echo '<th>Syarat</th>';
			echo '<th>Minimal</th>';
			echo '<th>Pembuatan</th>';
			echo '</tr>';
			
			$i = 1;
			foreach ($obj->syarat['Khusus'] as $v) {
				echo '<tr>';
				echo '<td width="40" align="right">'.$i.'.</td>';
				echo '<td width="60%">'.$v->ssiNama.($v->ssiJumlah > 1 ? ' sebanyak '.$v->ssiJumlah.' lembar' : '').'</td>';
				echo '<td>'.($v->ssiMinimal ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				$pembuatan = array();
				if ($v->ssiBaru) {
					$pembuatan[] = '<span class="badge open">Baru</span>';
				} else {
					$pembuatan[] = '<span class="badge none">Baru</span>';
				}
				if ($v->ssiPerpanjangan) {
					$pembuatan[] = '<span class="badge close">Perpanjangan</span>';
				} else {
					$pembuatan[] = '<span class="badge none">Perpanjangan</span>';
				}
				if ($v->ssiPerubahan) {
					$pembuatan[] = '<span class="badge info">Perubahan</span>';
				} else {
					$pembuatan[] = '<span class="badge none">Perubahan</span>';
				}
				echo '<td>'.implode('', $pembuatan).'</td>';
				echo '</tr>';
				$i++;
			}
			echo '</table>';
		}
?>			
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php
	}
	
	static function viewJenisSuratIzin($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=jenissuratizin"><img src="images/icons/page_white_edit.png" border="0" /> Jenis Surat Izin</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=jenissuratizin&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('syarat', 'Syarat', false, 40, 'right'),
			$app->setHeader('jsiNama', 'Nama Lengkap', true),
			$app->setHeader('jsiPola1', 'Pola 1<br>(Hari Kerja, Survey)', false, 0, '', 2),
			$app->setHeader('jsiPola2', 'Pola 2<br>(Hari Kerja, Survey)', false, 0, '', 2),
			$app->setHeader('jsiMasaBerlaku', 'Masa Berlaku', true, 0, 'right'),
			$app->setHeader('jsiMasaDaftarUlang', 'Daftar Ulang', true, 0, 'right'),
			$app->setHeader('jsiTipe', 'Tipe Permohonan', true),
			$app->setHeader('jsiTampil', 'Tampil', true),
			$app->setHeader('jsiKode', 'Kode', true),
			$app->setHeader('jsiNomor', 'Nomor', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=jenissuratizin&task=edit&id='.$v->jsiID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->jsiID.', '."'".$v->jsiNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td style="text-align:right;"><a href="index2.php?act=syaratsuratizin&id='.$v->jsiID.'" title="Syarat Surat Izin">'.$v->total_syarat.'<img src="images/icons/table.png" border="0" style="margin-left:5px; vertical-align:middle;"/></a></td>';
				echo '<td><a href="index2.php?act=jenissuratizin&task=edit&id='.$v->jsiID.'">'.$v->jsiNama.($v->jsiSubNama != '' ? ' ('.$v->jsiSubNama.')' : '').'</a></td>';
				if ($v->jsiLamaPengurusan1 > 0) {
					echo '<td align="right">'.$v->jsiLamaPengurusan1.'</td>';
					echo '<td>'.($v->jsiPerluSurvey1 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				} else {
					echo '<td colspan="2">-</td>';
				}
				if ($v->jsiLamaPengurusan2 > 0) {
					echo '<td align="right">'.$v->jsiLamaPengurusan2.'</td>';
					echo '<td>'.($v->jsiPerluSurvey2 ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				} else {
					echo '<td colspan="2">-</td>';
				}
				echo '<td align="right">'.($v->jsiMasaBerlaku == 0 ? 'Selama Ada' : $v->jsiMasaBerlaku.' Tahun').'</td>';
				echo '<td align="right">'.($v->jsiMasaDaftarUlang == 0 ? 'Tidak Ada' : $v->jsiMasaDaftarUlang.' Tahun sekali').'</td>';
				echo '<td>'.$v->jsiTipe.'</td>';
				echo '<td>'.($v->jsiTampil ? '<span class="badge ya">Ya</span>' : '<span class="badge tidak">Tidak</span>').'</td>';
				echo '<td>'.$v->jsiKode.'</td>';
				echo '<td>'.$v->jsiNomor.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="13">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/jenissuratizin/js/jenissuratizin.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>