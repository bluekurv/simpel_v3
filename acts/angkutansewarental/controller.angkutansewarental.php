<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'angkutansewarental';
$childTitle = 'Izin Operasional Angkutan Sewa Rental';
$childKodeSurat = 'IOP-ASR';
$childTembusan = 'Kepala Dinas Perhubungan, Komunikasi dan Informasi Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>