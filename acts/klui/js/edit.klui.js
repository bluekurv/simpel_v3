//Variables
var form = $("#myForm");
var kluiKode = $("#kluiKode");
var kluiNama = $("#kluiNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'kluiKode' || this.id === undefined) {
		if (kluiKode.val().length == 0){
			doSubmit = false;
			kluiKode.addClass("error");		
		} else {
			kluiKode.removeClass("error");
		}
	}
	
	if (this.id == 'kluiNama' || this.id === undefined) {
		if (kluiNama.val().length == 0){
			doSubmit = false;
			kluiNama.addClass("error");		
		} else {
			kluiNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
kluiKode.blur(validateForm);
kluiNama.blur(validateForm);

kluiKode.keyup(validateForm);
kluiNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
kluiKode.focus();