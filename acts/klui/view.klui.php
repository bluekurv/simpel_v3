<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class KLUI_View {
	static function editKLUI($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=klui&task=edit&id=<?php echo $obj->kluiID; ?>"><img src="images/icons/database_table.png" border="0" /> <?php echo ($obj->kluiID > 0) ? 'Ubah' : 'Tambah'; ?> KLUI</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=klui&task=save" method="POST" >
				<input type="hidden" id="kluiID" name="kluiID" value="<?php echo $obj->kluiID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=klui">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Kode <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="kluiKode" name="kluiKode" maxlength="5" size="5" value="<?php echo $obj->kluiKode; ?>" />
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="kluiNama" name="kluiNama" maxlength="255" size="50" value="<?php echo $obj->kluiNama; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/klui/js/edit.klui.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewKLUI($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=klui"><img src="images/icons/database_table.png" border="0" /> KLUI</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=klui&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Kode/Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('kluiKode', 'Kode', true, 40),
			$app->setHeader('kluiNama', 'Nama', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=klui&task=edit&id='.$v->kluiID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->kluiID.', '."'".$v->kluiNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=klui&task=edit&id='.$v->kluiID.'">'.$v->kluiKode.'</a></td>';
				echo '<td>'.$v->kluiNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="3">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/klui/js/klui.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>