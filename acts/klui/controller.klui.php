<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.klui.php';
require_once 'view.klui.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKLUI($app->id);
		break;
	case 'delete':
		deleteKLUI($app->id);
		break;
	case 'save':
		saveKLUI();
		break;
	default:
		viewKLUI(true, '');
		break;
}

function deleteKLUI($id) {
	global $app;
	
	//Get object
	$objKLUI = $app->queryObject("SELECT * FROM klui WHERE kluiID='".$id."'");
	if (!$objKLUI) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM klui WHERE kluiID='".$id."'");
		
		$success = true;
		$msg = 'KLUI "'.$objKLUI->kluiNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'klui', $objKLUI->kluiID, $objKLUI->kluiNama, $msg);
	} else {
		$success = false;
		$msg = 'KLUI "'.$objKLUI->kluiNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKLUI($success, $msg);
}

function editKLUI($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKLUI = $app->queryObject("SELECT * FROM klui WHERE kluiID='".$id."'");
		if (!$objKLUI) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKLUI = new KLUI_Model();
	}
	
	KLUI_View::editKLUI(true, '', $objKLUI);
}

function saveKLUI() {
	global $app;
	
	//Create object
	$objKLUI = new KLUI_Model();
	$app->bind($objKLUI);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKLUI->kluiKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKLUI->kluiNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT kluiID AS id FROM klui WHERE kluiKode='".$objKLUI->kluiKode."'", $objKLUI->kluiID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objKLUI->kluiKode.'" sudah ada';
	}
	
	/*$isExist = $app->isExist("SELECT kluiID AS id FROM klui WHERE kluiNama='".$objKLUI->kluiNama."'", $objKLUI->kluiID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objKLUI->kluiNama.'" sudah ada';
	}*/
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKLUI->kluiID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKLUI);
		} else {
			$sql = $app->createSQLforUpdate($objKLUI);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKLUI->kluiID = $app->queryField("SELECT LAST_INSERT_ID() FROM klui");
			
			$success = true;
			$msg = 'KLUI "'.$objKLUI->kluiNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'klui', $objKLUI->kluiID, $objKLUI->kluiNama, $msg);
		} else {
			$success = true;
			$msg = 'KLUI "'.$objKLUI->kluiNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'klui', $objKLUI->kluiID, $objKLUI->kluiNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'KLUI "'.$objKLUI->kluiNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKLUI($success, $msg);
	} else {
		KLUI_View::editKLUI($success, $msg, $objKLUI);
	}
}

function viewKLUI($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('klui', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('klui', 'sort', 'kluiKode');
	$dir   		= $app->pageVar('klui', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('klui', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(kluiKode LIKE '%".$nama."%' OR kluiNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM klui
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM klui
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	KLUI_View::viewKLUI($success, $msg, $arr);
}
?>