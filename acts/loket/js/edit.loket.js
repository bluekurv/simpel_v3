//Variables
var form = $("#myForm");
var lokNama = $("#lokNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'lokNama' || this.id === undefined) {
		if (lokNama.val().length == 0){
			doSubmit = false;
			lokNama.addClass("error");		
		} else {
			lokNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
lokNama.blur(validateForm);

lokNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
lokNama.focus();