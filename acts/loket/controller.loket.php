<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.loket.php';
require_once 'view.loket.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editLoket($app->id);
		break;
	case 'delete':
		deleteLoket($app->id);
		break;
	case 'save':
		saveLoket();
		break;
	default:
		viewLoket(true, '');
		break;
}

function deleteLoket($id) {
	global $app;
	
	//Get object
	$objLoket = $app->queryObject("SELECT * FROM loket WHERE lokID='".$id."'");
	if (!$objLoket) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalPelayanan = intval($app->queryField("SELECT COUNT(*) AS total FROM loketsuratizin WHERE lsiLokID='".$id."'"));
	if ($totalPelayanan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalPelayanan.' pelayanan untuk loket tersebut';
	}
	
	$totalPengguna = intval($app->queryField("SELECT COUNT(*) AS total FROM pengguna WHERE pnLokID='".$id."'"));
	if ($totalPengguna > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalPengguna.' pengguna untuk loket tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM loket WHERE lokID='".$id."'");
		
		$success = true;
		$msg = 'Loket "'.$objLoket->lokNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'loket', $objLoket->lokID, $objLoket->lokNama, $msg);
	} else {
		$success = false;
		$msg = 'Loket "'.$objLoket->lokNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewLoket($success, $msg);
}

function editLoket($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objLoket = $app->queryObject("SELECT * FROM loket WHERE lokID='".$id."'");
		if (!$objLoket) {
			$app->showPageError();
			exit();
		}
	} else {
		$objLoket = new Loket_Model();
	}
	
	Loket_View::editLoket(true, '', $objLoket);
}

function saveLoket() {
	global $app;
	
	//Create object
	$objLoket = new Loket_Model();
	$app->bind($objLoket);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objLoket->lokNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT lokID AS id FROM loket WHERE lokNama='".$objLoket->lokNama."'", $objLoket->lokID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama "'.$objLoket->lokNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objLoket->lokID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objLoket);
		} else {
			$sql = $app->createSQLforUpdate($objLoket);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objLoket->lokID = $app->queryField("SELECT LAST_INSERT_ID() FROM loket");
			
			$success = true;
			$msg = 'Loket "'.$objLoket->lokNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'loket', $objLoket->lokID, $objLoket->lokNama, $msg);
		} else {
			$success = true;
			$msg = 'Loket "'.$objLoket->lokNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'loket', $objLoket->lokID, $objLoket->lokNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Loket "'.$objLoket->lokNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewLoket($success, $msg);
	} else {
		Loket_View::editLoket($success, $msg, $objLoket);
	}
}

function viewLoket($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('loket', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('loket', 'sort', 'lokNama');
	$dir   		= $app->pageVar('loket', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('loket', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "lokNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total 
				  FROM loket
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *, (SELECT COUNT(*) AS total FROM loketsuratizin WHERE lsiLokID=lokID) AS total_suratizin
			FROM loket
			".$where." 
			ORDER BY ".$sort." ".$dir."
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Loket_View::viewLoket($success, $msg, $arr);
}
?>