<?php
class Tdg_Model {	
	public $tabel = 'tdg';
	public $primaryKey = 'tdgID';
	
	public $tdgID = 0;
	public $tdgDmhnID = 0;
	public $tdgGrupID = 0;
	public $tdgNo = 0;
	public $tdgNoLengkap = '';
	public $tdgNama = '';
	public $tdgAlamat = '';
	public $tdgKelurahanID = 0;
	public $tdgNoTelp = '';
	public $tdgNoFax = '';
	public $tdgJenisPemilik = '';	//Pemilik, Penanggung Jawab
	public $tdgNamaPemilik = '';
	public $tdgAlamatPemilik = '';
	public $tdgKelurahanPemilikID = 0;
	public $tdgJenisIdentitasPemilik = '';
	public $tdgNoIdentitasPemilik = '';
	public $tdgNoTelpPemilik = '';
	public $tdgNoFaxPemilik = '';
	public $tdgEmailPemilik = '';
	public $tdgNoSIUP = '';
	public $tdgTglSIUP = '0000-00-00';
	public $tdgTempatKeluarSIUP = '';
	public $tdgNoIMB = '';
	public $tdgTglIMB = '0000-00-00';
	public $tdgNoIUI = '';
	public $tdgNPWP = '';
	public $tdgAlamatGudang = '';
	public $tdgKelurahanGudangID = 0;
	public $tdgRTGudang = 0;
	public $tdgRWGudang = 0;
	public $tdgNoTelpGudang = '';
	public $tdgNoFaxGudang = '';
	public $tdgEmailGudang = '';
	public $tdgLuasGudang = 0;
	public $tdgKapasitasGudang = 0;
	public $tdgSatuanKapasitasGudang = '';
	public $tdgNilaiGudang = 0;
	public $tdgKoordinatN = '';
	public $tdgKoordinatE = '';
	public $tdgKomposisiNasional = 0.0;
	public $tdgKomposisiAsing = 0.0;
	public $tdgKelengkapanGudang = '';
	public $tdgListrik = '';
	public $tdgAir = '';
	public $tdgPendingin = '';
	public $tdgForklif = '';
	public $tdgKomputer = '';
	public $tdgKeterangan = '';
	public $tdgTandaTangan = '';
	public $tdgPejID = 0;
	public $tdgTglPengesahan = '0000-00-00'; 
	public $tdgTglBerlaku = '0000-00-00';
	public $tdgTglDaftarUlang = '0000-00-00';	 	 	 	 	
	public $tdgArsip = '';
}
?>