<?php
global $app;

if (!$app) {
	exit();
}

class Tdg_View {
	static function editTdg($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<link rel="stylesheet" type="text/css" href="js/jquery.treeview/jquery.treeview.css">
	<script type="text/javascript" src="js/jquery.treeview/jquery.treeview.js"></script>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdg&task=edit&id=<?php echo $obj->tdgDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Tanda Daftar Gudang</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=tdg&task=save" method="POST" >
				<input type="hidden" id="tdgID" name="tdgID" value="<?php echo $obj->tdgID; ?>" />
				<input type="hidden" id="tdgDmhnID" name="tdgDmhnID" value="<?php echo $obj->tdgDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('tdgGrupID', $obj->tdgGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=tdg&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<input type="hidden" id="tdgNama" name="tdgNama" value="<?php echo $obj->tdgNama; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=tdg&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="tdgAlamat" name="tdgAlamat" value="<?php echo $obj->tdgAlamat; ?>" />
						<?php echo $objDetailPermohonan->perAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Telp</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgNoTelp" name="tdgNoTelp" maxlength="500" size="50" value="<?php echo $obj->tdgNoTelp; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Fax</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgNoFax" name="tdgNoFax" maxlength="500" size="50" value="<?php echo $obj->tdgNoFax; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<p><span style="text-decoration: underline; text-weight:bold;">Data</span>
						<select class="box" id="tdgJenisPemilik" name="tdgJenisPemilik">
<?php 
		foreach (array('Pemilik Gudang', 'Penanggung Jawab Gudang') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdgJenisPemilik) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
						</p>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgNamaPemilik" name="tdgNamaPemilik" maxlength="500" size="50" value="<?php echo $obj->tdgNamaPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgAlamatPemilik" name="tdgAlamatPemilik" maxlength="500" size="50" value="<?php echo $obj->tdgAlamatPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		//HACK
		if ($obj->tdgKelurahanPemilikID == '') {
			$obj->tdgKelurahanPemilikID = $objDetailPermohonan->perKelurahanPemilikID;
		}
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->tdgKelurahanPemilikID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$tdgKelurahanPemilik = $app->queryObject($sql);
		if (!$tdgKelurahanPemilik) {
			$tdgKelurahanPemilik = new stdClass();
			$tdgKelurahanPemilik->kode = '';
			$tdgKelurahanPemilik->value = '';
		}
?>
						<input type="hidden" id="tdgKelurahanPemilikID" name="tdgKelurahanPemilikID" value="<?php echo $obj->tdgKelurahanPemilikID; ?>" />
						<input class="box readonly" id="tdgKelurahanPemilikKode" name="tdgKelurahanPemilikKode" size="8" value="<?php echo $tdgKelurahanPemilik->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="tdgKelurahanPemilik" name="tdgKelurahanPemilik" maxlength="500" size="70" value="<?php echo $tdgKelurahanPemilik->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">
						No. 
						<select class="box" id="tdgJenisIdentitasPemilik" name="tdgJenisIdentitasPemilik">
<?php 
		foreach (array('KTP', 'Paspor', 'KITAS') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdgJenisIdentitasPemilik) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgNoIdentitasPemilik" name="tdgNoIdentitasPemilik" maxlength="500" size="50" value="<?php echo $obj->tdgNoIdentitasPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Telp</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgNoTelpPemilik" name="tdgNoTelpPemilik" maxlength="500" size="50" value="<?php echo $obj->tdgNoTelpPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Fax</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgNoFaxPemilik" name="tdgNoFaxPemilik" maxlength="500" size="50" value="<?php echo $obj->tdgNoFaxPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Email</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgEmailPemilik" name="tdgEmailPemilik" maxlength="50" size="50" value="<?php echo $obj->tdgEmailPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Identitas Gudang (Per Unit)</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Gudang</td><td>:</td>
					<td>
						<input class="box" id="tdgAlamatGudang" name="tdgAlamatGudang" maxlength="500" size="50" value="<?php echo $obj->tdgAlamatGudang; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
						RT
						<input class="box bulat" id="tdgRTGudang" name="tdgRTGudang" maxlength="11" size="11" value="<?php echo $obj->tdgRTGudang; ?>" />
						/ RW
						<input class="box bulat" id="tdgRWGudang" name="tdgRWGudang" maxlength="11" size="11" value="<?php echo $obj->tdgRWGudang; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
						Kelurahan
<?php
		$sql = "SELECT w.wilID AS id, w.wilNama AS nama FROM wilayah AS w, wilayah AS w2, wilayah AS w3 WHERE w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilNama='Pelalawan' ORDER BY w.wilNama";
		$app->createSelect('tdgKelurahanGudangID', $sql, $obj->tdgKelurahanGudangID);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Telp</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgNoTelpGudang" name="tdgNoTelpGudang" maxlength="500" size="50" value="<?php echo $obj->tdgNoTelpGudang; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Fax</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgNoFaxGudang" name="tdgNoFaxGudang" maxlength="500" size="50" value="<?php echo $obj->tdgNoFaxGudang; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Email</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="tdgEmailGudang" name="tdgEmailGudang" maxlength="50" size="50" value="<?php echo $obj->tdgEmailGudang; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Gudang</td><td>:</td>
					<td>
						<input class="box bulat" id="tdgLuasGudang" name="tdgLuasGudang" maxlength="11" size="11" value="<?php echo $obj->tdgLuasGudang; ?>" />
						m<sup>2</sup>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kapasitas Gudang</td><td>:</td>
					<td>
						<input class="box bulat" id="tdgKapasitasGudang" name="tdgKapasitasGudang" maxlength="11" size="11" value="<?php echo $obj->tdgKapasitasGudang; ?>" />
						<select class="box" id="tdgSatuanKapasitasGudang" name="tdgSatuanKapasitasGudang">
<?php 
		foreach (array('m3', 'ton') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdgSatuanKapasitasGudang) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nilai Gudang (Rp)</td><td>:</td>
					<td>
						<input class="box" id="tdgNilaiGudang" name="tdgNilaiGudang" maxlength="255" size="50" value="<?php echo $app->MySQLToMoney($obj->tdgNilaiGudang); ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Titik Koordinat</td><td>:</td>
					<td>
						N <input class="box" id="tdgKoordinatN" name="tdgKoordinatN" maxlength="255" size="50" value="<?php echo $obj->tdgKoordinatN; ?>" /><br>
						E <input class="box" id="tdgKoordinatE" name="tdgKoordinatE" maxlength="255" size="50" value="<?php echo $obj->tdgKoordinatE; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Komposisi Kepemilikan</td>
				</tr>
				<tr>
					<td style="padding-left:20px; text-align:right;">Nasional</td><td>:</td>
					<td>
						<input class="box" id="tdgKomposisiNasional" name="tdgKomposisiNasional" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->tdgKomposisiNasional, 2); ?>" />
						%
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px; text-align:right;">Asing</td><td>:</td>
					<td>
						<input class="box" id="tdgKomposisiAsing" name="tdgKomposisiAsing" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->tdgKomposisiAsing, 2); ?>" />
						%
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelengkapan Gudang</td><td>:</td>
					<td>
						<select class="box" id="tdgKelengkapanGudang" name="tdgKelengkapanGudang">
<?php 
		foreach (array('Berpendingin', 'Tidak Berpendingin', 'Keduanya') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdgKelengkapanGudang) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Listrik</td><td>:</td>
					<td>
						<input class="box" id="tdgListrik" name="tdgListrik" maxlength="50" size="20" value="<?php echo $obj->tdgListrik; ?>" />
						Kwh
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Air</td><td>:</td>
					<td>
						<select class="box" id="tdgAir" name="tdgAir">
<?php 
		foreach (array('PAM', 'Sumur Bor') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->tdgAir) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Fasilitas Pendingin</td><td>:</td>
					<td>
						<input class="box" id="tdgPendingin" name="tdgPendingin" maxlength="50" size="20" value="<?php echo $obj->tdgPendingin; ?>" />
						<sup>o</sup>C
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Forklif</td><td>:</td>
					<td>
						<input class="box" id="tdgForklif" name="tdgForklif" maxlength="50" size="20" value="<?php echo $obj->tdgForklif; ?>" />
						Unit
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Komputer</td><td>:</td>
					<td>
						<input class="box" id="tdgKomputer" name="tdgKomputer" maxlength="50" size="20" value="<?php echo $obj->tdgKomputer; ?>" />
						Unit
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Surat-Surat</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor SIUP</td><td>:</td>
					<td>
						<input class="box" id="tdgNoSIUP" name="tdgNoSIUP" maxlength="500" size="50" value="<?php echo $obj->tdgNoSIUP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. SIUP</td><td>:</td>
					<td><input class="box date" id="tdgTglSIUP" name="tdgTglSIUP" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdgTglSIUP); ?>"></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tempat Dikeluarkannya SIUP</td><td>:</td>
					<td>
						<input class="box" id="tdgTempatKeluarSIUP" name="tdgTempatKeluarSIUP" maxlength="500" size="50" value="<?php echo $obj->tdgTempatKeluarSIUP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor IMB</td><td>:</td>
					<td>
						<input class="box" id="tdgNoIMB" name="tdgNoIMB" maxlength="500" size="50" value="<?php echo $obj->tdgNoIMB; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. IMB</td><td>:</td>
					<td><input class="box date" id="tdgTglIMB" name="tdgTglIMB" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdgTglIMB); ?>"></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Surat Izin Usaha Industri</td><td>:</td>
					<td>
						<input class="box" id="tdgNoIUI" name="tdgNoIUI" maxlength="500" size="50" value="<?php echo $obj->tdgNoIUI; ?>" />
					<td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWP</td><td>:</td>
					<td>
						<input class="box" id="tdgNPWP" name="tdgNPWP" maxlength="255" size="50" value="<?php echo $obj->tdgNPWP; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdgNo > 0) {
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/TDG/<?php echo $app->getRoman(intval(substr($obj->tdgTglPengesahan,5,2))); ?>/<?php echo substr($obj->tdgTglPengesahan,0,4); ?>/</span>
						<input class="box" id="tdgNo" name="tdgNo" maxlength="5" size="5" value="<?php echo $obj->tdgNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="tdgNo" name="tdgNo" value="<?php echo $obj->tdgNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="tdgTglPengesahan" name="tdgTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdgTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="tdgPejID" name="tdgPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->tdgPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='tdg'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->tdgTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdgTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="tdgTglBerlaku" name="tdgTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->tdgTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->tdgTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdgTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="tdgTglDaftarUlang" name="tdgTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->tdgTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/tdg/js/edit.tdg.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}

	static function printTdg($obj) {
		global $app;
		
		$objReport 		= new Report('Tanda Daftar Gudang', 'TDG-'.substr($obj->tdgTglPengesahan,0,4).'-'.$obj->tdgNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->tdgPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: left;	/* Bukan justify */
		}
	</style>

	<h2 align="center"><u>TANDA DAFTAR GUDANG (TDG)</u></h2>
	<h4 align="center">NOMOR : <?php echo $obj->tdgNoLengkap; ?></h4>
	
	<p>Yang bertanda tangan di bawah ini menerangkan bahwa :</p>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="3%">1.</td>
		<td width="3%">a.</td>
		<td width="39%">Nama <?php echo $obj->tdgJenisPemilik; ?></td>
		<td width="3%">:</td>
		<td width="52%"><?php echo strtoupper($obj->tdgNamaPemilik); ?></td>
	</tr>
	<tr>
		<td></td>
		<td>b.</td>
		<td>No. <?php echo $obj->tdgJenisIdentitasPemilik; ?></td>
		<td>:</td>
		<td colspan="2"><?php echo strtoupper($obj->tdgNoIdentitasPemilik); ?></td>
	</tr>
	<tr>
		<td></td>
		<td>c.</td>
		<td>Alamat <?php echo $obj->tdgJenisPemilik; ?></td>
		<td>:</td>
		<td colspan="2"><?php echo strtoupper($obj->tdgAlamat); ?></td>
	</tr>
	<tr>
		<td></td>
		<td>d.</td>
		<td>Telepon, Fax dan Email</td>
		<td>:</td>
		<td colspan="2">
<?php 
		$kontak = array();
		if ($obj->tdgNoTelpPemilik != '') {
			$kontak[] = 'TELP. '.strtoupper($obj->tdgNoTelpPemilik);
		}
		if ($obj->tdgNoFaxPemilik != '') {
			$kontak[] = 'FAX. '.strtoupper($obj->tdgNoFaxPemilik);
		}
		if ($obj->tdgEmailPemilik != '') {
			$kontak[] = $obj->tdgEmailPemilik;
		}
		echo implode(', ', $kontak); 
?>
		</td>
	</tr>
	<tr>
		<td colspan="6">&nbsp;</td>
	</tr>
<?php 
		$alamat = array();
		
		if ($obj->tdgAlamatGudang != '') {
			$alamat[] = $obj->tdgAlamatGudang;
		}
		
		if ($obj->tdgRTGudang != '') {
			$alamat[] = 'RT.'.sprintf('%03d', $obj->tdgRTGudang).' / RW.'.sprintf('%03d', $obj->tdgRWGudang);
		}
		
		$sql = "SELECT CONCAT(w.wilTingkat,' ',w.wilNama) AS kelurahan, CONCAT(w2.wilTingkat,' ',w2.wilNama) AS kecamatan, CONCAT(w3.wilTingkat,' ',w3.wilNama) AS kabupatenkota, CONCAT(w4.wilTingkat,' ',w4.wilNama) AS provinsi 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->tdgKelurahanGudangID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$objWilayah = $app->queryObject($sql);
		
		if ($objWilayah) {
			$alamat[] = $objWilayah->kelurahan;
			$alamat[] = $objWilayah->kecamatan;
			$alamat[] = $objWilayah->kabupatenkota;
			$alamat[] = $objWilayah->provinsi;
		}
?>
	<tr>
		<td>2.</td>
		<td colspan="2">Alamat Gudang</td>
		<td>:</td>
		<td colspan="2"><?php echo strtoupper(implode(', ', $alamat)); ?></td>
	</tr>
	<tr>
		<td>3.</td>
		<td colspan="2">Titik Koordinat</td>
		<td>:</td>
		<td colspan="2">N : <?php echo strtoupper($obj->tdgKoordinatN); ?>, E : <?php echo strtoupper($obj->tdgKoordinatE); ?></td>
	</tr>
	<tr>
		<td>4.</td>
		<td colspan="2">Telepon, Fax dan Email</td>
		<td>:</td>
		<td colspan="2">
<?php 
		$kontak = array();
		if ($obj->tdgNoTelpGudang != '') {
			$kontak[] = 'TELP. '.strtoupper($obj->tdgNoTelpGudang);
		}
		if ($obj->tdgNoFaxGudang != '') {
			$kontak[] = 'FAX. '.strtoupper($obj->tdgNoFaxGudang);
		}
		if ($obj->tdgEmailGudang != '') {
			$kontak[] = $obj->tdgEmailGudang;
		}
		echo implode(', ', $kontak); 
?>
		</td>
	</tr>
	<tr>
		<td>5.</td>
		<td colspan="2">Luas dan Kapasitas Gudang</td>
		<td>:</td>
		<td colspan="2">
			<?php echo strtoupper($obj->tdgLuasGudang); ?> m<sup>2</sup> (<?php echo strtoupper($app->terbilang($obj->tdgLuasGudang)); ?>)
		</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="2"></td>
		<td></td>
		<td colspan="2">
			<?php echo strtoupper($obj->tdgKapasitasGudang); ?> <?php echo $obj->tdgSatuanKapasitasGudang == 'm3' ? 'm<sup>3</sup>' : 'ton'; ?> (<?php echo strtoupper($app->terbilang($obj->tdgKapasitasGudang)); ?>)
		</td>
	</tr>
	<tr>
		<td>6.</td>
		<td colspan="2">Golongan Gudang</td>
		<td>:</td>
		<td colspan="2">
			<?php echo strtoupper($obj->tdgKelengkapanGudang); ?>
		</td>
	</tr>
	</table>
	
	<p>Tanda Daftar Gudang (TDG) ini hanya berlaku untuk gudang sebagaimana tercantum pada point 2 (dua) di atas untuk menyimpan barang-barang yang ditujukan untuk diperdagangkan, wajib di daftar ulang setiap 5 (lima) tahun dan wajib diperbaharui apabila terjadi perubahan data dan informasi di dalam TDG.</p>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%" align="center">
			<table>
			<tr>
				<td align="center"><b>Pangkalan Kerinci, <?php echo $app->MySQLDateToIndonesia($obj->tdgTglPengesahan); ?></b></td>
			</tr>
			<tr>
				<td align="center" colspan="2">
					<table border="0" cellpadding="1" cellspacing="0" width="100%">
					<tr>
						<td align="center">
<?php 
		if ($obj->tdgTandaTangan == 'atas nama') {		
?>
							<b>an. KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
							<br><b><?php echo $objPengesahan->pnJabatan; ?></b>
<?php 
		} else if ($obj->tdgTandaTangan == 'mewakili') {
?>
							<b>KEPALA DINAS PENANAMAN MODAL DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
							<br><b>Mewakili</b>
<?php 
		} else {
?>
							<b><br>KEPALA DINAS PENANAMAN MODAL <br>DAN PELAYANAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b>
<?php 
		}
?>
							<br><br><br><br><br>
							<table width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td width="24%"></td>
								<td width="52%">
									<b><u><?php echo $objPengesahan->pnNama; ?></u></b><br>
									<?php echo $objPengesahan->pgrPangkat; ?><br>
									NIP. <?php echo $objPengesahan->pnNIP; ?>
								</td>
								<td width="24%"></td>
							</tr>
							</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	
	<p>
	<small>Tembusan :<br>
	Dinas Perindustrian dan Perdagangan Kabupaten Pelalawan
	</small>
	</p>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	static function readTdg($obj) {
		global $app;
		
		//TODO: belum disesuaikan dengan editTdg
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdg&task=edit&id=<?php echo $obj->tdgDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Tanda Daftar Gudang</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=tdg&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jabatan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdgJabatanPemilik; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=tdg&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//HACK
		if ($obj->tdgAlamat == '') {
			$obj->tdgAlamat = $objDetailPermohonan->perAlamat;
		}
?>
						<input class="box" id="tdgAlamat" name="tdgAlamat" maxlength="500" size="100" value="<?php echo $obj->tdgAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//HACK
		if ($obj->tdgKelurahanID == '') {
			$obj->tdgKelurahanID = $objDetailPermohonan->perKelurahanID;
		}
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->tdgKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$tdgKelurahan = $app->queryObject($sql);
		if (!$tdgKelurahan) {
			$tdgKelurahan = new stdClass();
			$tdgKelurahan->kode = '';
			$tdgKelurahan->value = '';
		}
?>
						<input type="hidden" id="tdgKelurahanID" name="tdgKelurahanID" value="<?php echo $obj->tdgKelurahanID; ?>" />
						<input class="box readonly" id="tdgKelurahanKode" name="tdgKelurahanKode" size="8" value="<?php echo $tdgKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="tdgKelurahan" name="tdgKelurahan" maxlength="500" size="70" value="<?php echo $tdgKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, informasikan kepada Administrator untuk menambahkannya</i>)</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Surat-Surat</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor SIUP</td><td>:</td>
					<td>
						<input id="tdgNoSIUP" name="tdgNoSIUP" maxlength="500" size="50" value="<?php echo $obj->tdgNoSIUP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. SIUP</td><td>:</td>
					<td><input class="date" id="tdgTglSIUP" name="tdgTglSIUP" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->tdgTglSIUP); ?>"></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tempat Dikeluarkannya SIUP</td><td>:</td>
					<td>
						<input id="tdgTempatKeluarSIUP" name="tdgTempatKeluarSIUP" maxlength="500" size="50" value="<?php echo $obj->tdgTempatKeluarSIUP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Surat Izin Usaha Industri</td><td>:</td>
					<td>
						<input id="tdgNoIUI" name="tdgNoIUI" maxlength="500" size="50" value="<?php echo $obj->tdgNoIUI; ?>" />
					<td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWP</td><td>:</td>
					<td>
						<input id="tdgNPWP" name="tdgNPWP" maxlength="255" size="50" value="<?php echo $obj->tdgNPWP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Gudang (Per Unit)</td><td>:</td>
					<td>
						<input id="tdgAlamatGudang" name="tdgAlamatGudang" maxlength="500" size="50" value="<?php echo $obj->tdgAlamatGudang; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
						RT
						<input class="bulat" id="tdgRTGudang" name="tdgRTGudang" maxlength="11" size="11" value="<?php echo $obj->tdgRTGudang; ?>" />
						/ RW
						<input class="bulat" id="tdgRWGudang" name="tdgRWGudang" maxlength="11" size="11" value="<?php echo $obj->tdgRWGudang; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
						Kelurahan
<?php
		$this->createSelect('tdgKelurahanGudangID', $kelurahan, $obj->tdgKelurahanGudangID, array(0 => ''));
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Gudang (Per Unit)</td><td>:</td>
					<td>
						<input class="bulat" id="tdgLuasGudang" name="tdgLuasGudang" maxlength="11" size="11" value="<?php echo $obj->tdgLuasGudang; ?>" />
						m<sup>2</sup>
					</td>
				</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdgKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->tdgNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->tdgTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->tdgPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdgTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdgTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->tdgTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->tdgTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/tdg/js/read.tdg.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewTdg($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=tdg"><img src="images/icons/rosette.png" border="0" /> Buku Register Tanda Daftar Gudang</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('tdgNo', 'Nomor Surat', true),
			$app->setHeader('tdgTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('tdgTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('tdgTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=tdg&task=read&id='.$v->tdgDmhnID.'&fromact=tdg" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->tdgDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=tdg&task=read&id='.$v->tdgDmhnID.'&fromact=tdg" title="Entri">'.$v->tdgNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->tdgTglPengesahan).'</td>';
				
				if ($v->tdgTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->tdgTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->tdgTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->tdgTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/tdg/js/tdg.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>