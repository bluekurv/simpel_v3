<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.tdg.php';
require_once 'view.tdg.php';

switch ($app->task) {
	case 'add':
		addTdg($app->id);
		break;
	case 'edit':
		editTdg($app->id);
		break;
	case 'delete':
		deleteTdg($app->id);
		break;
	case 'print':
		printTdg($app->id);
		break;
	case 'read':
		readTdg($app->id);
		break;
	case 'save':
		saveTdg();
		break;
	default:
		viewTdg(true, '');
		break;
}

function addTdg($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editTdg($id);
}

function deleteTdg($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM tdg WHERE tdgDmhnID='".$id."'");
		
	$success = true;
	$msg = 'TDG berhasil dihapus';
		
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'TDG', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function editTdg($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objTdg = $app->queryObject("SELECT * FROM tdg WHERE tdgDmhnID='".$id."'");
	if (!$objTdg) {
		$objTdg = new Tdg_Model();
		
		//Ambil nilai default
		$objTdg->tdgNama 				= $objDetailPermohonan->perNama;
		$objTdg->tdgAlamat 				= $objDetailPermohonan->perAlamat;
		$objTdg->tdgKelurahanID 		= $objDetailPermohonan->perKelurahanID;
		$objTdg->tdgNoTelp 				= $objDetailPermohonan->perNoTelp;
		$objTdg->tdgNoFax 				= $objDetailPermohonan->perNoFax;
		$objTdg->tdgNPWP 				= $objDetailPermohonan->perNPWP;
		$objTdg->tdgNamaPemilik 		= $objDetailPermohonan->perNamaPemilik;
		$objTdg->tdgAlamatPemilik 		= $objDetailPermohonan->perAlamatPemilik;
		$objTdg->tdgKelurahanPemilikID 	= $objDetailPermohonan->perKelurahanPemilikID;
		$objTdg->tdgNoIdentitasPemilik  = $objDetailPermohonan->perNoKTPPemilik;
		$objTdg->tdgNoTelpPemilik 		= $objDetailPermohonan->perNoTelpPemilik;
		$objTdg->tdgNoFaxPemilik 		= $objDetailPermohonan->perNoFaxPemilik;
		
		$objTdg->tdgTempatKeluarSIUP 	= 'Pelalawan';
	}
	
	if ($objTdg->tdgNPWP == "") {
		$objTdg->tdgNPWP = $app->queryField("SELECT wprNoPokok FROM wajibpajakretribusi WHERE wprNama='".$objDetailPermohonan->perNama."' AND wprJenis='Pajak'");
	}
	
	$objTdg->tdgDmhnID = $objDetailPermohonan->dmhnID;
	
	Tdg_View::editTdg(true, '', $objDetailPermohonan, $objTdg);
}

function printTdg($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN tdg ON dmhnID=tdgDmhnID
			WHERE dmhnID='".$id."'";
	$objTdg = $app->queryObject($sql);
	if (!$objTdg) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objTdg->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	Tdg_View::printTdg($objTdg);
}

function readTdg($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN tdg ON dmhnID=tdgDmhnID
			WHERE dmhnID='".$id."'";
	$objTdg = $app->queryObject($sql);
	if (!$objTdg) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objTdg->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Tdg_View::readTdg($objTdg);
}

function saveTdg() {
	global $app;
	
	//Get object
	$objTdgSebelumnya = $app->queryObject("SELECT * FROM tdg WHERE tdgID='".$app->getInt('tdgID')."'");
	if (!$objTdgSebelumnya) {
		$objTdgSebelumnya = new Tdg_Model();
	}
	
	//Create object
	$objTdg = new Tdg_Model();
	$app->bind($objTdg);
	
	//Modify object (if necessary)
	$objTdg->tdgRTGudang = $app->MoneyToMySQL($objTdg->tdgRTGudang);
	$objTdg->tdgRWGudang = $app->MoneyToMySQL($objTdg->tdgRWGudang);
	$objTdg->tdgLuasGudang = $app->MoneyToMySQL($objTdg->tdgLuasGudang);
	$objTdg->tdgKapasitasGudang = $app->MoneyToMySQL($objTdg->tdgKapasitasGudang);
	$objTdg->tdgNilaiGudang = $app->MoneyToMySQL($objTdg->tdgNilaiGudang);
	$objTdg->tdgKomposisiNasional = $app->MoneyToMySQL($objTdg->tdgKomposisiNasional);
	$objTdg->tdgKomposisiAsing = $app->MoneyToMySQL($objTdg->tdgKomposisiAsing);

	$objTdg->tdgTglSIUP = $app->NormalDateToMySQL($objTdg->tdgTglSIUP);
	$objTdg->tdgTglIMB = $app->NormalDateToMySQL($objTdg->tdgTglIMB);
	
	$objTdg->tdgTglPengesahan = $app->NormalDateToMySQL($objTdg->tdgTglPengesahan);
	if ($objTdg->tdgTglPengesahan != '0000-00-00') {
		if ($objTdgSebelumnya->tdgNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objTdg->tdgNo = intval($app->queryField("SELECT MAX(tdgNo) AS nomor FROM tdg WHERE YEAR(tdgTglPengesahan)='".substr($objTdg->tdgTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objTdg->tdgTglBerlaku = date('Y-m-d', strtotime($objTdg->tdgTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objTdg->tdgTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objTdg->tdgTglDaftarUlang = date('Y-m-d', strtotime($objTdg->tdgTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objTdg->tdgTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objTdg->tdgNo = 0;
		
		$objTdg->tdgTglBerlaku = '0000-00-00';
		$objTdg->tdgTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	//137/BPMP2T/TDG/II/2016/*
	$objTdg->tdgNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/TDG/'.$app->getRoman(intval(substr($objTdg->tdgTglPengesahan,5,2))).'/'.substr($objTdg->tdgTglPengesahan,0,4).'/'.$objTdg->tdgNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objTdg->tdgNo > 0){
		$isExist = $app->isExist("SELECT tdgID AS id FROM tdg WHERE tdgNo='".$objTdg->tdgNo."' AND YEAR(tdgTglPengesahan)='".substr($objTdg->tdgTglPengesahan,0,4)."'", $objTdg->tdgID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objTdg->tdgNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objTdg->tdgID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objTdg);
		} else {
			$sql = $app->createSQLforUpdate($objTdg);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objTdg->tdgID = $app->queryField("SELECT LAST_INSERT_ID() FROM tdg");
		}
		
		$success = true;
		$msg = 'Tanda Daftar Gudang berhasil disimpan';
		
		$app->writeLog(EV_INFORMASI, 'tdg', $objTdg->tdgID, $objTdg->tdgNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objTdg->tdgDmhnID."'");
		
		/*$app->query("UPDATE perusahaan 
					 SET 
					 	 perMerekUsaha='".$objTdg->tdgMerekUsaha."', 
					 	 perNPWP='".$objTdg->tdgNPWP."', 
					 	 perStatusTempatUsaha='".$objTdg->tdgStatusTempatUsaha."' 
					 WHERE perID='".$app->getInt('perID')."'");*/
	} else {
		$success = false;
		$msg = 'Tanda Daftar Gudang tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objTdg->tdgDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Tdg_View::editTdg($success, $msg, $objDetailPermohonan, $objTdg);
	}
}

function viewTdg($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('tdg', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('tdg', 'sort', 'tdgTglPengesahan');
	//$dir		= $app->pageVar('tdg', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('tdg', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('tdg', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(tdgNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(tdgTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM tdg
				  LEFT JOIN detailpermohonan ON tdgDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM tdg
			LEFT JOIN detailpermohonan ON tdgDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(tdgTglPengesahan) DESC, tdgNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Tdg_View::viewTdg($success, $msg, $arr);
}
?>