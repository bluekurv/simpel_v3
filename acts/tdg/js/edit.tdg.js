//Objects
$("#browser").treeview();

$("#tdgNilaiGudang").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#tdgKomposisiNasional").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
$("#tdgKomposisiAsing").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });

//Variables
var form = $("#myForm");

//Functions
function pilihKlui(tabel, id, kode, nama) {
	var str = '<div class="kbli" id="kbli-' + id + '">';
	str += '<input type="hidden" id="tdgBidangUsaha[' + id + ']" name="tdgBidangUsaha[' + id + ']" value="' + tabel + '-' + kode + '">';
	str += '<input type="hidden" id="tdgBidangUsahaNama[' + id + ']" name="tdgBidangUsahaNama[' + id + ']" value="' + nama + '">';
	str += kode + ' - ' + nama + ' ';
	str += '<a href="javascript:removeKlui(\'' + tabel + '\', \'' + id + '\');" title="Hapus Kode"><b>x</b></a>';
	str += '</div>';
	
	$("#bidangusaha").append(str);
}

function removeKlui(tabel, id) {
	$('#kbli-' + id).remove();
}

function validateForm() {
	var doSubmit = true;
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

$('#txtBidangUsaha').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kbli2',
	onSelect: function(suggestion){
		pilihKlui(suggestion.data.tabel, suggestion.data.id, suggestion.data.kode, suggestion.data.nama);
	}
});

$('#tdgKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#tdgKelurahanID').val(suggestion.data.id);
		$('#tdgKelurahanKode').val(suggestion.data.kode);
		$('#tdgKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#tdgKelurahanPemilik').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#tdgKelurahanPemilikID').val(suggestion.data.id);
		$('#tdgKelurahanPemilikKode').val(suggestion.data.kode);
		$('#tdgKelurahanPemilik').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

form.submit(submitForm);

//Set focus
$("#tdgMerekUsaha").focus();