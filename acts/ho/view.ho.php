<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Ho_View {
	static function editHo() {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php"><img src="images/icons/application_home.png" border="0" /> Entri Izin Gangguan (HO)</a></h1>
	</div>
	<div class="halamanTengah">
		
	</div>
	<div class="halamanBawah"></div>
<?php
	}
	
	static function printHo() {
		global $app;
		
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");

		$pageOrientation = 'P';
		$pageFormat = 'A4';
		$pageTitle = 'Izin Gangguan (HO)';
		
		$pdf = new TCPDF($pageOrientation, PDF_UNIT, $pageFormat, true, 'UTF-8', false);
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($pageTitle);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins(PDF_MARGIN_LEFT, 15, PDF_MARGIN_RIGHT);
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont('helvetica', '', 12, '', true);
		
		$pdf->AddPage();
		
		ob_clean();
		ob_start();
?>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="15%"><img src="<?php echo CFG_LIVE_PATH; ?>/images/report/pelalawan.png"></td>
		<td width="85%" align="center">
			<h5>PEMERINTAH <?php echo strtoupper($objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama); ?></h5>
			<h4 style="font-family:Times New Roman;"><?php echo strtoupper($objSatuanKerja->skrNama); ?></h4>
			<h5 style="font-family:Times New Roman;"><?php echo strtoupper($objSatuanKerja->skrAlamat); ?></h5>
		</td>
	</tr>
	</table>
	<hr/>

	<table width="650" border="0" align="center" cellspacing="0">
	<tr>
		<td width="97" align="left" valign="top">Membaca</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top">
			<table width="504" border="0" align="center" cellspacing="0">
			<tr>
				<td width="12" align="left" valign="top">1.</td> <td width="488" align="left" valign="top">Surat Permohonan dari Saudara $saudara Tanggal : '.$date.' untuk memperoleh izin Usaha berdasarkan Undang-undang Gangguan (Hinder Ordonatie).</td>
			</tr>
			<tr>
				<td align="left" valign="top">2.</td> <td align="left" valign="top">Rekomendasi Camat Nomor : $nomor Perihal Perizinan Gangguan.</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">Menimbang</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top">
			Bahwa Usaha Ybs telah memenuhi persyaratan yang telah di tetapkan, maka permohonan dapat disetujui, oleh karena itu perlu di tetapkan dengan suatu Surat Keputusan.
		</td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">Mengingat</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top">
			<ol>
				<li>Undang-undang Gangguan (Hinder Ordonatie) stablat Tahun 1926 Nomor 226 yang di rubah dengan Stablat Tahun 1949 Nomor 14 dan 450</li>
				<li>Undang-undang Nomor 53 Tahun 1999</li>
				<li>Undang-undang Nomor 32 Tahun 2004</li>
				<li>Undang-undang Nomor 25 Tahun 2007</li>
				<li>Undang-undang Nomor 28 Tahun 2009</li>
				<li>Undang-undang Nomor 25 Tahun 2007</li>
				<li>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</li>
				<li>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</li>
				<li>Peraturan Menteri Dalam Negeri Nomor 27 Tahun 2009</li>
				<li>Peraturan Daerah Kabupaten Pelalawan Nomor 04 Tahun 2012</li>
				<li>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</li>
				<li>Keputusan Bupati Pelalawan Nomor KPTS.503/KPPT/2009/136</li>
				<li>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/21</li>
			</ol>
		</td>
	</tr>
	<tr align="left" valign="top">
		<td colspan="3" align="center"><b>MEMUTUSKAN</b></td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">Menetapkan</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">PERTAMA <br/>
			<table width="100px" height="100px" border="0">
			<tr>
				<td width="" align="center">
					<img src="<?php echo CFG_LIVE_PATH; ?>/images/report/3x4.png" width="200px" height="240px">
				</td>
			</tr>
			</table>
		</td>
		<td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top">
			<table width="504" border="0" align="center" cellspacing="0">
			<tr>
				<td width="504" align="left" valign="top" colspan="2">Memberikan izin Usaha berdasarkan Undang-undang Gangguan (Hinder Ordonatie) Kepada :</td>
			</tr>
			<tr>
				<td width="177" align="left" valign="top">Nama</td> <td width="323" align="left" valign="top"> '.$nama.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Umur</td> <td align="left" valign="top"> '.$umur.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Kewarganegaraan</td> <td align="left" valign="top"> '.$warga-negara.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Pekerjaan</td> <td align="left" valign="top"> '.$kerja.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">No KTP</td> <td align="left" valign="top"> '.$noktp.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Alamat Tempat Tinggal </td> <td align="left" valign="top"> '.$alamat.' </td>
			</tr>
			<tr>
			  	<td align="left" valign="top">Jenis Usaha</td> <td align="left" valign="top"> '.$jenisPariwisata.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Nomor NPWPD</td> <td align="left" valign="top"> '.$npwpd.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Merek/Nama Usaha</td> <td align="left" valign="top"> '.$merek.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Alamat Tempat Usaha</td> <td align="left" valign="top"> '.$jln.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Keluruhan/Desa</td> <td align="left" valign="top"> '.$kel-desa.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Kecamatan</td> <td align="left" valign="top"> '.$kec.' </td>
			</tr>
			</table>
		<u>DENGAN KETENTUAN-KETENTUAN SEBAGAI BERIKUT :</u>
		<ol>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
			<li>Pengusaha ybs harus menjaga kebersihan tempat usaha.</li>
		</ol>
	</td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">KEDUA</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top">
			Usaha sebagaimana tersebut pada Diktum Pertama, dengan batas-batas sebagai berikut :
			<table width="504" border="0" align="center" cellspacing="0">
			<tr>
				<td width="177" align="left" valign="top">Utara berbatas dengan</td> <td width="323" align="left" valign="top"> '.$utara.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Selatan berbatas dengan</td> 
				<td align="left" valign="top"> '.$selatan.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Barat berbatas dengan</td> 
				<td align="left" valign="top"> '.$barat.' </td>
			</tr>
			<tr>
				<td align="left" valign="top">Timur berbatas dengan</td> 
				<td align="left" valign="top"> '.$timur.' </td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">KETIGA</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top">
			Izin Usaha berlaku selama Perusahaan masih berjalan/beroperasi dengan ketentuan Daftar Ulang 5 Tahun sekali
		</td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">KEEMPAT</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top"> Pemegang Izin/Pengusaha tetap melaksanakan pembayaran Retribusi, Pajak-pajak selama pemegang izin tidak mengajukan Permohonan Pemberentian Usaha</td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">KELIMA</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top">
			Atas pemberian Izin Usaha ini dikenakan Retribusi Izin Usaha yang tetap dibayarb setiap tahu, bersdasarkan Peraturan DaerahKabupaten Pelalawan.
		</td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">KEENAM</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top"> Surat Izin ini dicabut kembali, Apabila Pengusaha/Pemegang Izin tidak mentaati ketentuan-ketentuan tersebut di atas dan melanggar Peraturan Perundang-undangan yang berlaku .</td>
	</tr>
	<tr>
		<td width="97" align="left" valign="top">KETUJUH</td><td width="6" align="left" valign="top">:</td>
		<td width="550" align="left" valign="top"> Keputusan ini mulai berlaku sejak tanggal di tetapkan.</td>
	</tr>
	</table>
	<table border="1" cellpadding="0" cellspacing="0" width="100%">
		<tr>
			<td width="60%"></td>
			<td width="40%" align="center">
				<p><?php echo $objSatuanKerja->wilIbukota; ?>, Jabatan</p>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td></td>
			<td align="center"><b><u>Nama</u></b></td>
		</tr>
</table>
<?php
		$html = ob_get_contents();
		
		ob_clean();
		
		//CSS diambil karena CSS tidak boleh eksternal
		$pdf->writeHTML($html);
		
		$pdf->Output('IzinGangguan(HO).pdf', 'I');
	}
	
	static function defaultHo() {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php"><img src="images/icons/application_home.png" border="0" /> Izin Gangguan (HO)</a></h1>
	</div>
	<div class="halamanTengah">
		
	</div>
	<div class="halamanBawah"></div>
<?php
	}
}
?>