<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'view.ho.php';

switch ($app->task) {
	case 'edit':
		editHo($app->id);
		break;
	case 'print':
		printHo($app->id);
		break;
	default:
		defaultHo();
		break;
}

function editHo($id) {
	global $app;
	
	Ho_View::editHo();
}

function printHo($id) {
	global $app;
	
	Ho_View::printHo();
}

function defaultHo() {
	Ho_View::defaultHo();
}
?>