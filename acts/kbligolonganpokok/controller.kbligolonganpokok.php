<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kbligolonganpokok.php';
require_once 'view.kbligolonganpokok.php';
require_once CFG_ABSOLUTE_PATH.'/acts/kbli/view.kbli.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editKBLIGolonganPokok($app->id);
		break;
	case 'delete':
		deleteKBLIGolonganPokok($app->id);
		break;
	case 'save':
		saveKBLIGolonganPokok();
		break;
	default:
		viewKBLIGolonganPokok(true, '');
		break;
}

function deleteKBLIGolonganPokok($id) {
	global $app;
	
	//Get object
	$objKBLIGolonganPokok = $app->queryObject("SELECT * FROM kbligolonganpokok WHERE kgpokID='".$id."'");
	if (!$objKBLIGolonganPokok) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalKBLIGolongan = intval($app->queryField("SELECT COUNT(*) AS total FROM kbligolongan WHERE kgolKgpokID='".$id."'"));
	if ($totalKBLIGolongan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalKBLIGolongan.' golongan untuk golongan pokok tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM kbligolonganpokok WHERE kgpokID='".$id."'");
		
		$success = true;
		$msg = 'Golongan Pokok Lapangan Usaha "'.$objKBLIGolonganPokok->kgpokNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'kbligolonganpokok', $objKBLIGolonganPokok->kgpokID, $objKBLIGolonganPokok->kgpokNama, $msg);
	} else {
		$success = false;
		$msg = 'Golongan Pokok Lapangan Usaha "'.$objKBLIGolonganPokok->kgpokNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKBLIGolonganPokok($success, $msg);
}

function editKBLIGolonganPokok($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKBLIGolonganPokok = $app->queryObject("SELECT * FROM kbligolonganpokok WHERE kgpokID='".$id."'");
		if (!$objKBLIGolonganPokok) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKBLIGolonganPokok = new KBLIGolonganPokok_Model();
		
		$objKBLIGolonganPokok->kgpokKkatID = $app->pageVar('kbligolonganpokok', 'filterparent', 0, 'intval');
		$objKBLIGolonganPokok->kgpokKpubID = intval($app->queryField("SELECT kkatKpubID FROM kblikategori WHERE kkatID='".$objKBLIGolonganPokok->kgpokKkatID."'"));
	}
	
	KBLIGolonganPokok_View::editKBLIGolonganPokok(true, '', $objKBLIGolonganPokok);
}

function saveKBLIGolonganPokok() {
	global $app;
	
	//Create object
	$objKBLIGolonganPokok = new KBLIGolonganPokok_Model();
	$app->bind($objKBLIGolonganPokok);
	
	//Modify object (if necessary)
	if (isset($_REQUEST['kgpokTampil'])) {
		$objKBLIGolonganPokok->kgpokTampil = ($_REQUEST['kgpokTampil'] == 'on') ? 1 : 0;
	} else {
		$objKBLIGolonganPokok->kgpokTampil = 0;
	}
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKBLIGolonganPokok->kgpokKkatID == 0) {
		$doSave = false;
		$msg[] = '- Kategori belum diisi';
	}
	
	if ($objKBLIGolonganPokok->kgpokKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKBLIGolonganPokok->kgpokNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objKBLIGolonganPokok->kgpokKpubID == 0) {
		$doSave = false;
		$msg[] = '- Publikasi belum diisi';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKBLIGolonganPokok->kgpokID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKBLIGolonganPokok);
		} else {
			$sql = $app->createSQLforUpdate($objKBLIGolonganPokok);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKBLIGolonganPokok->kgpokID = $app->queryField("SELECT LAST_INSERT_ID() FROM kbligolonganpokok");
			
			$success = true;
			$msg = 'Golongan Pokok Lapangan Usaha "'.$objKBLIGolonganPokok->kgpokNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'kbligolonganpokok', $objKBLIGolonganPokok->kgpokID, $objKBLIGolonganPokok->kgpokNama, $msg);
		} else {
			$success = true;
			$msg = 'Golongan Pokok Lapangan Usaha "'.$objKBLIGolonganPokok->kgpokNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'kbligolonganpokok', $objKBLIGolonganPokok->kgpokID, $objKBLIGolonganPokok->kgpokNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Golongan Pokok Lapangan Usaha "'.$objKBLIGolonganPokok->kgpokNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKBLIGolonganPokok($success, $msg);
	} else {
		KBLIGolonganPokok_View::editKBLIGolonganPokok($success, $msg, $objKBLIGolonganPokok);
	}
}

function viewKBLIGolonganPokok($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kbligolonganpokok', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kbligolonganpokok', 'sort', 'kgpokKode');
	$dir   		= $app->pageVar('kbligolonganpokok', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'parent' => 0
	);
	
	$nama 		= $app->pageVar('kbligolonganpokok', 'filternama', $default['nama'], 'strval');
	$parent		= $app->pageVar('kbligolonganpokok', 'filterparent', $default['parent'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(kgpokKode LIKE '%".$nama."%' OR kgpokNama LIKE '%".$nama."%')";
	}
	if ($parent != $default['parent']) {
		$filter[] = "kgpokKkatID='".$parent."'";
	}
	$filter[] = "kgpokKkatID=kkatID AND kgpokKpubID=kpubID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'parent' => $parent
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM kbligolonganpokok, kblikategori, kblipublikasi
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM kbligolonganpokok, kblikategori, kblipublikasi
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	KBLIGolonganPokok_View::viewKBLIGolonganPokok($success, $msg, $arr);
}
?>