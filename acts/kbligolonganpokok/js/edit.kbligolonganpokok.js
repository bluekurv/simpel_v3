//Variables
var form = $("#myForm");
var kgpokKkatID = $("#kgpokKkatID");
var kgpokKode = $("#kgpokKode");
var kgpokNama = $("#kgpokNama");
var kgpokKpubID = $("#kgpokKpubID");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'kgpokKkatID' || this.id === undefined) {
		if (kgpokKkatID.val() == 0){
			doSubmit = false;
			kgpokKkatID.addClass("error");		
		} else {
			kgpokKkatID.removeClass("error");
		}
	}
	
	if (this.id == 'kgpokKode' || this.id === undefined) {
		if (kgpokKode.val().length == 0){
			doSubmit = false;
			kgpokKode.addClass("error");		
		} else {
			kgpokKode.removeClass("error");
		}
	}
	
	if (this.id == 'kgpokNama' || this.id === undefined) {
		if (kgpokNama.val().length == 0){
			doSubmit = false;
			kgpokNama.addClass("error");		
		} else {
			kgpokNama.removeClass("error");
		}
	}
	
	if (this.id == 'kgpokKpubID' || this.id === undefined) {
		if (kgpokKpubID.val() == 0){
			doSubmit = false;
			kgpokKpubID.addClass("error");		
		} else {
			kgpokKpubID.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
kgpokKkatID.blur(validateForm);
kgpokKode.blur(validateForm);
kgpokNama.blur(validateForm);
kgpokKpubID.blur(validateForm);

kgpokKkatID.keyup(validateForm);
kgpokKode.keyup(validateForm);
kgpokNama.keyup(validateForm);
kgpokKpubID.keyup(validateForm);

form.submit(submitForm);

//Set focus
kgpokKkatID.focus();