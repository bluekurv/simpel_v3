//Variables
var form = $("#myForm");
var pariwisataKelurahan = $("#pariwisataKelurahan");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'pariwisataKelurahan' || this.id === undefined) {
		if (pariwisataKelurahan.val().length == 0) {
			doSubmit = false;
			pariwisataKelurahan.addClass("error");		
		} else {
			pariwisataKelurahan.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

form.submit(submitForm);

$('#pariwisataKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#pariwisataKelurahanID').val(suggestion.data.id);
		$('#pariwisataKelurahanKode').val(suggestion.data.kode);
		$('#pariwisataKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

//Set focus
$("#pariwisataPerNPWPD").focus();