<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Home_View {
	static function dashboardHome($arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php"><img src="images/icons/application_home.png" border="0" /> Dashboard</a></h1>
	</div>
<?php
		if (count($arr['message']) > 0) {
?>
	<div class="halamanTengah">
<?php
			foreach ($arr['message'] as $v) {
				$app->showMessage($v->success, $v->message);
			}
?>
	</div>
<?php 
		}
		
		switch ($_SESSION['sesipengguna']->levelAkses) {
			case 'Petugas Administrasi':
?>
	<div class="halamanTengah">
		<table border="0">
		<tr>
			<td valign="top"><img src="images/icons-lg/daftar_permohonan.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<h2><?php echo ($arr['pengurusan'] > 0) ? 'Ada <span style="color:#FF0000;">'.$arr['pengurusan'].' pengurusan surat izin</span>' : ' Tidak ada pengurusan surat izin'; ?> yang belum diselesaikan</h2>
				<p>Lihat pengurusan surat izin yang diarahkan kepada saya.</p>
				<a class="button-link inline dark-blue" href="index2.php?act=permohonan&page=1&filterfield=mhnNoUrutLengkap&filternama=&filterstatus=">Daftar Permohonan</a>
				<a class="button-link inline blue" href="index2.php?act=jenissuratizin&task=list&page=1">Jenis Surat Izin dan Persyaratannya</a>
			</td>
		</tr>
		<tr>
			<td valign="top"></td>
			<td valign="top">
<?php 
				if ($arr['belumditentukanpolanya'] > 0) {
?>
				<a class="button-link inline grey" href="index2.php?act=permohonan&page=1&filterfield=mhnNoUrutLengkap&filternama=&filterstatus=Belum%20Ditentukan%20Polanya">
					<div>
						<div style="color:#FF0000; font-size:36px; margin-top:10px; text-align:center;"><?php echo $arr['belumditentukanpolanya']; ?></div>
					</div><br>
					Belum Ditentukan Polanya
				</a>
<?php 
				}
				if ($arr['belumsurvey'] > 0) {
?>
				<a class="button-link inline grey" href="index2.php?act=permohonan&page=1&filterfield=mhnNoUrutLengkap&filternama=&filterstatus=Belum%20Survey">
					<div>
						<div style="color:#FF0000; font-size:36px; margin-top:10px; text-align:center;"><?php echo $arr['belumsurvey']; ?></div>
					</div><br>
					Belum Survey
				</a>
<?php 
				}
				if ($arr['hasilsurveyditunda'] > 0) {
?>
				<a class="button-link inline grey" href="index2.php?act=permohonan&page=1&filterfield=mhnNoUrutLengkap&filternama=&filterstatus=Ditunda">
					<div>
						<div style="color:#FF0000; font-size:36px; margin-top:10px; text-align:center;"><?php echo $arr['hasilsurveyditunda']; ?></div>
					</div><br>
					Hasil Survey: Ditunda
				</a>
<?php 
				}
				if ($arr['hasilsurveybatal'] > 0) {
?>
				<a class="button-link inline grey" href="index2.php?act=permohonan&page=1&filterfield=mhnNoUrutLengkap&filternama=&filterstatus=Batal">
					<div>
						<div style="color:#FF0000; font-size:36px; margin-top:10px; text-align:center;"><?php echo $arr['hasilsurveybatal']; ?></div>
					</div><br>
					Hasil Survey: Batal
				</a>
<?php 
				}
				if ($arr['belumselesai'] > 0) {
?>
				<a class="button-link inline grey" href="index2.php?act=permohonan&page=1&filterfield=mhnNoUrutLengkap&filternama=&filterstatus=Belum%20Selesai">
					<div>
						<div style="color:#FF0000; font-size:36px; margin-top:10px; text-align:center;"><?php echo $arr['belumselesai']; ?></div>
					</div><br>
					Belum Selesai
				</a>
<?php 
				}
?>
			</td>
		</tr>
		<tr>
			<td valign="top"><img src="images/icons-lg/cari_permohonan.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<h2>Cari Permohonan</h2>
				<form id="myForm" name="myForm" action="index2.php?act=permohonan&page=1" method="POST" >
					<select class="box" id="filterfield" name="filterfield">
						<option value="mhnNoUrutLengkap">No. Pendaftaran</option>
						<option value="mhnNoKtp">No. KTP</option>
						<option value="mhnNama">Nama Pemohon</option>
						<option value="perNama">Nama Perusahaan</option>
					</select>
					<input class="box" id="filternama" name="filternama">
					<input type="submit" class="button-link inline blue" value="Cari">
				</form>
			</td>
		</tr>
		</table>
	</div>
<?php 
				break;
			case 'Petugas Loket Pelayanan':
?>
	<div class="halamanTengah">
		<table border="0">
		<tr>
			<td valign="top"><img src="images/icons-lg/permohonan_baru.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<!--<p>Masukkan permohonan baru sesuai dengan isi formulir dan kelengkapan dokumen yang diajukan oleh Pemohon.</p>/-->
        <a class="button-link inline green" target="_blank" href="<?php echo rtrim(CFG_LIVE_PATH, "/") . ':1337'; ?>">Cek Lokasi</a>
				<a class="button-link inline dark-blue" id="btnPermohonanAdd" href="index2.php?act=permohonan&task=addPerseorangan">Permohonan Perseorangan</a>
				<a class="button-link inline" id="btnPermohonanAdd" href="index2.php?act=permohonan&task=addPerusahaan">Permohonan Perusahaan</a>
				<a class="button-link inline blue" href="index2.php?act=jenissuratizin&task=list&page=1">Jenis Surat Izin dan Persyaratannya</a>
			</td>
		</tr>
		<tr>
			<td valign="top"><img src="images/icons-lg/daftar_permohonan.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<p>Lihat permohonan yang telah dimasukkan sebelumnya.</p>
				<a class="button-link inline blue" href="index2.php?act=permohonan&page=1">Daftar Permohonan</a>
			</td>
		</tr>
		<tr>
			<td valign="top"><img src="images/icons-lg/cari_permohonan.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<p>Cari permohonan</p>
				<form id="myForm" name="myForm" action="index2.php?act=permohonan&page=1" method="POST" >
					<select class="box" id="filterfield" name="filterfield">
						<option value="mhnNoUrutLengkap">No. Pendaftaran</option>
						<option value="mhnNoKtp">No. KTP</option>
						<option value="mhnNama">Nama Pemohon</option>
						<option value="perNama">Nama Perusahaan</option>
					</select>
					<input class="box" id="filternama" name="filternama">
					<select class="box" id="filterstatus" name="filterstatus">
						<option value="">--Seluruh Status--</option>
<?php 
		foreach (array('Belum Selesai', 'Dikembalikan', 'Sudah Selesai') as $v) {
			echo '<option value="'.$v.'">'.$v.'</option>';
		}
?>
					</select>
					<input type="submit" class="button-link inline blue" value="Cari">
				</form>
			</td>
		</tr>
		</table>
	</div>
<?php 
				break;
			case 'Petugas Loket Pengaduan':
?>
	<div class="halamanTengah">
		<table border="0">
		<tr>
			<td valign="top"><img src="images/icons-lg/accessibility_warning.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<h2><?php echo ($arr['pengaduan'] > 0) ? 'Ada <span style="color:#FF0000;">'.$arr['pengaduan'].' pengaduan</span>' : ' Tidak ada pengaduan'; ?> yang belum diselesaikan</h2>
				<a class="button-link inline dark-blue" href="index2.php?act=pengaduan&task=add">Pengaduan Baru</a>
				<a class="button-link inline blue" href="index2.php?act=pengaduan&page=1">Daftar Pengaduan</a>
			</td>
		</tr>
		</table>
	</div>
<?php 
				break;
			case 'Petugas Loket Penyerahan':
?>
	<div class="halamanTengah">
		<table border="0">
		<tr>
			<td valign="top"><img src="images/icons-lg/daftar_permohonan.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<h2><?php echo ($arr['pengurusan'] > 0) ? 'Ada <span style="color:#FF0000;">'.$arr['pengurusan'].' surat izin</span>' : ' Tidak ada surat izin'; ?> yang belum diserahkan</h2>
				<p>Lihat surat izin yang belum diserahkan.</p>
				<a class="button-link inline blue" href="index2.php?act=penyerahan&page=1">Daftar Penyerahan</a>
				<a class="button-link inline blue" href="index2.php?act=home&task=barcode&html=0">Gunakan Barcode Scanner</a>
			</td>
		</tr>
		<tr>
			<td valign="top"><img src="images/icons-lg/cari_permohonan.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<h2>Cari Permohonan</h2>
				<form id="myForm" name="myForm" action="index2.php?act=permohonan&page=1" method="POST" >
					<select class="box" id="filterfield" name="filterfield">
						<option value="mhnNoUrutLengkap">No. Pendaftaran</option>
						<option value="mhnNoKtp">No. KTP</option>
						<option value="mhnNama">Nama Pemohon</option>
						<option value="perNama">Nama Perusahaan</option>
					</select>
					<input class="box" id="filternama" name="filternama">
					<select class="box" id="filterstatus" name="filterstatus">
						<option value="">--Seluruh Status--</option>
<?php 
		foreach (array('Belum Selesai', 'Dikembalikan', 'Sudah Selesai') as $v) {
			echo '<option value="'.$v.'">'.$v.'</option>';
		}
?>
					</select>
					<input type="submit" class="button-link inline blue" value="Cari">
				</form>
			</td>
		</tr>
		</table>
	</div>
<?php 
				break;
			case 'Surveyor':
?>
	<div class="halamanTengah">
		<table border="0">
		<tr>
			<td valign="top"><img src="images/icons-lg/daftar_permohonan.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<h2><?php echo ($arr['pengurusan'] > 0) ? 'Ada <span style="color:#FF0000;">'.$arr['pengurusan'].' pengurusan surat izin</span>' : ' Tidak ada pengurusan surat izin'; ?> yang perlu disurvey</h2>
				<p>Lihat pengurusan surat izin yang perlu disurvey.</p>
				<a class="button-link inline blue" href="index2.php?act=survey&page=1">Daftar Survey</a>
			</td>
		</tr>
		<tr>
			<td valign="top"><img src="images/icons-lg/cari_permohonan.png" width="128" height="128" border="0"></td>
			<td valign="top">
				<h2>Cari Permohonan</h2>
				<form id="myForm" name="myForm" action="index2.php?act=permohonan&page=1" method="POST" >
					<select class="box" id="filterfield" name="filterfield">
						<option value="mhnNoUrutLengkap">No. Pendaftaran</option>
						<option value="mhnNoKtp">No. KTP</option>
						<option value="mhnNama">Nama Pemohon</option>
						<option value="perNama">Nama Perusahaan</option>
					</select>
					<input class="box" id="filternama" name="filternama">
					<select class="box" id="filterstatus" name="filterstatus">
						<option value="">--Seluruh Status--</option>
<?php 
		foreach (array('Belum Selesai', 'Dikembalikan', 'Sudah Selesai') as $v) {
			echo '<option value="'.$v.'">'.$v.'</option>';
		}
?>
					</select>
					<input type="submit" class="button-link inline blue" value="Cari">
				</form>
			</td>
		</tr>
		</table>
	</div>
<?php 
				break;
			default:
?>
	<div class="halamanTengah" style="text-align:center;">
		<img src="images/logo.jpg"/>
	</div>
<?php 
		}
?>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/home/js/dashboard.home.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>