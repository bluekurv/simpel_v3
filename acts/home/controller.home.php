<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'view.home.php';

switch ($app->task) {
	case 'scanner':
		scannerHome();
		break;
	case 'barcode':
		barcodeHome();
		break;
	default:
		defaultHome();
		break;
}

function scannerHome() {
	global $app;
	
	$barcode = $_REQUEST['barcode'];
	
	$sql = "SELECT * 
			FROM permohonan, detailpermohonan, jenissuratizin
			WHERE dmhnID='".$barcode."' AND dmhnMhnID=mhnID AND dmhnJsiID=jsiID";
	$obj = $app->queryObject($sql);
	
	$sql = "SELECT * 
			FROM ".$obj->jsiKode."
			WHERE ".$obj->jsiKode."DmhnID='".$obj->dmhnID."'";
	$obj2 = $app->queryObject($sql);
	
	if ($obj2) {
	$sql = "UPDATE detailpermohonan SET dmhnTelahSelesai=1, dmhnTglSelesai='".date('Y-m-d')."', dmhnTelahAmbil=1, dmhnPengambil='', dmhnNoIdentitas='', dmhnTglAmbil='".date('Y-m-d')."', 
		dmhnDiserahkanOleh='".$_SESSION['sesipengguna']->ID."' WHERE dmhnID='".$barcode."'";
	//$app->query($sql);
?>
<div class="halamanAtas">
	<h1 class="bTitle"><a class="bTitle" href="index2.php"><img src="images/icons/user.png" border="0" /> Surat Izin untuk Barcode No. <?php echo $barcode; ?></a></h1>
	<a class="button-link inline blue" href="index2.php?act=home&task=barcode&html=0">Ulangi Barcode Scanner</a>
</div>
<div class="halamanTengah">
	<table class="readTable" border="0" cellpadding="1" cellspacing="1">
	<tr>
		<td width="150">Jenis Surat Izin</td><td>:</td>
		<td><?php echo $obj->jsiNama; ?></td>
	</tr>
	<tr>
		<td>No. Permohonan</td><td>:</td>
		<td><?php echo $obj->mhnNoUrutLengkap; ?></td>
	</tr>
	<tr>
		<td>No. Surat</td><td>:</td>
		<td><?php echo $obj2->{$obj->jsiKode."NoLengkap"}; ?></td>
	</tr>
	</table>
<?php
echo $sql;
?>
</div>
<div class="halamanBawah"></div>
<?php
	}
}

function barcodeHome() {
	global $app;
?>
<form action="index2.php" method="get">
<input type="hidden" name="act" value="home">
<input type="hidden" name="task" value="scanner">
<input id="barcode" name="barcode" placeholder="Untuk menggunakan barcode scanner, pastikan background berwarna biru muda. Apabila tidak, klik pada tulisan ini" autofocus>
</form>
<style>
body {
  margin: 0;
}
#barcode {
  width: 100%;
  height: 100%;
  font-size: 20px;
}
#barcode:focus {
  background-color: lightskyblue;
}
</style>
<?php
}

function defaultHome() {
	global $app;
	
	$arr = array();
	$arr['message'] = array();
	
	//Query
	$kalender = intval($app->queryField("SELECT COUNT(*) AS total FROM kalender WHERE YEAR(kalTgl)='".date('Y')."'"));
	if ($kalender == 0) {
		$obj = new stdClass();
		$obj->success = false;
		$obj->message = 'Data hari libur untuk tahun '.date('Y').' belum dimasukkan. Beritahukan hal ini kepada Administrator.';
		$arr['message'][] = $obj;
	}
	
	switch ($_SESSION['sesipengguna']->levelAkses) {
		case 'Petugas Administrasi':
			$arr['pengurusan'] = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."' AND dmhnDikembalikan=0 AND dmhnTelahSelesai=0"));
			$arr['belumditentukanpolanya'] = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."' AND dmhnDikembalikan=0 AND dmhnPola=0"));
			$arr['belumsurvey'] = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=0"));
			$arr['hasilsurveyditunda'] = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=1 AND dmhnHasilSurvey='Ditunda'"));
			$arr['hasilsurveybatal'] = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=1 AND dmhnHasilSurvey='Batal'"));
			$arr['belumselesai'] = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."' AND dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnHasilSurvey='Memenuhi Syarat' AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=0"));
			break;
		case 'Petugas Loket Pengaduan':
			$arr['pengaduan'] = intval($app->queryField("SELECT COUNT(*) AS total FROM pengaduan WHERE aduTelahSelesai=0"));
			break;
		case 'Petugas Loket Penyerahan':
			$arr['pengurusan'] = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDikembalikan=0 AND dmhnPola>0 AND (dmhnPerluSurvey=0 OR (dmhnPerluSurvey=1 AND dmhnTelahSurvey=1)) AND dmhnTelahSelesai=1 AND dmhnTelahAmbil=0"));
			break;
		case 'Surveyor':
			$arr['pengurusan'] = intval($app->queryField("SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDikembalikan=0 AND dmhnPola>0 AND dmhnPerluSurvey=1 AND dmhnTelahSurvey=0"));
			break;
	}
	
	Home_View::dashboardHome($arr);
}
?>