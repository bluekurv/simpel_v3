<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.iui.php';
require_once 'view.iui.php';

switch ($app->task) {
	case 'edit':
		editIui($app->id);
		break;
	case 'print':
		printIui($app->id);
		break;
	case 'read':
		readIui($app->id);
		break;
	case 'save':
		saveIui();
		break;
	default:
		viewIui(true, '');
		break;
}

function editIui($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objIui = $app->queryObject("SELECT * FROM iui WHERE iuiDmhnID='".$id."'");
	if (!$objIui) {
		$objIui = new Iui_Model();
		
		//Ambil nilai default
		$objIui->iuiKluiID = $objDetailPermohonan->perKluiID;
		$objIui->iuiKepemilikanPabrik = $objDetailPermohonan->perKepemilikanPabrik;
		$objIui->iuiKelurahanLokasiID = $objDetailPermohonan->perKelurahanLokasiID;
		$objIui->iuiLuasBangunan = $objDetailPermohonan->perLuasBangunan;
		$objIui->iuiLuasTanah = $objDetailPermohonan->perLuasTanah;
		$objIui->iuiTenagaPenggerak = $objDetailPermohonan->perTenagaPenggerak;
		$objIui->iuiKomoditiIndustri = $objDetailPermohonan->perKomoditiIndustri;
		$objIui->iuiInventaris = $objDetailPermohonan->perInventaris;
		$objIui->iuiKapasitasProduksiPerTahun = $objDetailPermohonan->perKapasitasProduksiPerTahun;
		$objIui->iuiSatuanKapasitasProduksiPerTahun = $objDetailPermohonan->perSatuanKapasitasProduksiPerTahun;
		$objIui->iuiTenagaKerjaIndonesiaLaki = $objDetailPermohonan->perTenagaKerjaIndonesiaLaki;
		$objIui->iuiTenagaKerjaIndonesiaPerempuan = $objDetailPermohonan->perTenagaKerjaIndonesiaPerempuan;
		$objIui->iuiTenagaKerjaAsingLaki = $objDetailPermohonan->perTenagaKerjaAsingLaki;
		$objIui->iuiTenagaKerjaAsingPerempuan = $objDetailPermohonan->perTenagaKerjaAsingPerempuan;
	}
	
	$objIui->iuiDmhnID = $objDetailPermohonan->dmhnID;
	
	Iui_View::editIui(true, '', $objDetailPermohonan, $objIui);
}

function printIui($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN iui ON dmhnID=iuiDmhnID
			WHERE dmhnID='".$id."'";
	$objIui = $app->queryObject($sql);
	if (!$objIui) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objIui->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	Iui_View::printIui($objIui);
}

function readIui($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN iui ON dmhnID=iuiDmhnID
			WHERE dmhnID='".$id."'";
	$objIui = $app->queryObject($sql);
	if (!$objIui) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objIui->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Iui_View::readIui($objIui);
}

function saveIui() {
	global $app;
	
	//Get object
	$objIuiSebelumnya = $app->queryObject("SELECT * FROM iui WHERE iuiID='".$app->getInt('iuiID')."'");
	if (!$objIuiSebelumnya) {
		$objIuiSebelumnya = new Iui_Model();
	}
	
	//Create object
	$objIui = new Iui_Model();
	$app->bind($objIui);
	
	//Modify object (if necessary)
	$objIui->iuiLuasBangunan = $app->MoneyToMySQL($objIui->iuiLuasBangunan);
	$objIui->iuiLuasTanah = $app->MoneyToMySQL($objIui->iuiLuasTanah);
	$objIui->iuiInventaris = $app->MoneyToMySQL($objIui->iuiInventaris);
	$objIui->iuiKapasitasProduksiPerTahun = $app->MoneyToMySQL($objIui->iuiKapasitasProduksiPerTahun);
	
	$objIui->iuiTenagaKerjaIndonesiaLaki = $app->MoneyToMySQL($objIui->iuiTenagaKerjaIndonesiaLaki);
	$objIui->iuiTenagaKerjaIndonesiaPerempuan = $app->MoneyToMySQL($objIui->iuiTenagaKerjaIndonesiaPerempuan);
	$objIui->iuiTenagaKerjaAsingLaki = $app->MoneyToMySQL($objIui->iuiTenagaKerjaAsingLaki);
	$objIui->iuiTenagaKerjaAsingPerempuan = $app->MoneyToMySQL($objIui->iuiTenagaKerjaAsingPerempuan);
	
	$objIui->iuiTglPengesahan = $app->NormalDateToMySQL($objIui->iuiTglPengesahan);
	if ($objIui->iuiTglPengesahan != '0000-00-00') {
		if ($objIuiSebelumnya->iuiNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objIui->iuiNo = intval($app->queryField("SELECT MAX(iuiNo) AS nomor FROM iui WHERE YEAR(iuiTglPengesahan)='".substr($objIui->iuiTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objIui->iuiTglBerlaku = date('Y-m-d', strtotime($objIui->iuiTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objIui->iuiTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objIui->iuiTglDaftarUlang = date('Y-m-d', strtotime($objIui->iuiTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objIui->iuiTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objIui->iuiNo = 0;
		
		$objIui->iuiTglBerlaku = '0000-00-00';
		$objIui->iuiTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objIui->iuiNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/IUI/'.substr($objIui->iuiTglPengesahan,0,4).'/'.$objIui->iuiNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objIui->iuiNo > 0){
		$isExist = $app->isExist("SELECT iuiID AS id FROM iui WHERE iuiNo='".$objIui->iuiNo."' AND YEAR(iuiTglPengesahan)='".substr($objIui->iuiTglPengesahan,0,4)."'", $objIui->iuiID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objIui->iuiNo.'" sudah ada';
		}
	}
	
	if ($objIui->iuiKelurahanLokasiID == 0) {
		$doSave = false;
		$msg[] = '- Kelurahan untuk Lokasi Pabrik belum dipilih dari daftar yang ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objIui->iuiID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objIui);
		} else {
			$sql = $app->createSQLforUpdate($objIui);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objIui->iuiID = $app->queryField("SELECT LAST_INSERT_ID() FROM iui");
		}
		
		$success = true;
		$msg = 'Tanda Daftar Industri berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'iui', $objIui->iuiID, $objIui->iuiNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objIui->iuiDmhnID."'");
		
		//Update data perusahaan
		$app->query("UPDATE perusahaan 
					 SET 
					 	 perKluiID='".$objIui->iuiKluiID."', 
					 	 perKelurahanLokasiID='".$objIui->iuiKelurahanLokasiID."', 
					 	 perLuasBangunan='".$objIui->iuiLuasBangunan."' , 
					 	 perLuasTanah='".$objIui->iuiLuasTanah."' , 
					 	 perTenagaPenggerak='".$objIui->iuiTenagaPenggerak."' , 
					 	 perKomoditiIndustri='".$objIui->iuiKomoditiIndustri."' , 
					 	 perInventaris='".$objIui->iuiInventaris."' , 
					 	 perKapasitasProduksiPerTahun='".$objIui->iuiKapasitasProduksiPerTahun."' , 
					 	 perSatuanKapasitasProduksiPerTahun='".$objIui->iuiSatuanKapasitasProduksiPerTahun."' , 
					 	 perTenagaKerjaIndonesiaLaki='".$objIui->iuiTenagaKerjaIndonesiaLaki."' , 
					 	 perTenagaKerjaIndonesiaPerempuan='".$objIui->iuiTenagaKerjaIndonesiaPerempuan."' , 
					 	 perTenagaKerjaAsingLaki='".$objIui->iuiTenagaKerjaAsingLaki."' , 
					 	 perTenagaKerjaAsingPerempuan='".$objIui->iuiTenagaKerjaAsingPerempuan."'
					 WHERE perID='".$app->getInt('perID')."'");
	} else {
		$success = false;
		$msg = 'Tanda Daftar Industri tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		if (isset($_REQUEST['save'])) {
			header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
		} else {
			header('Location:index2.php?act=mesiniui&id='.$objIui->iuiID.'&success=true&msg='.$msg);
		}
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objIui->iuiDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Iui_View::editIui($success, $msg, $objDetailPermohonan, $objIui);
	}
}

function viewIui($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('iui', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('iui', 'sort', 'iuiTglPengesahan');
	//$dir		= $app->pageVar('iui', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('iui', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('iui', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(iuiNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(iuiTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM iui
				  LEFT JOIN detailpermohonan ON iuiDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM iui
			LEFT JOIN detailpermohonan ON iuiDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(iuiTglPengesahan) DESC, iuiNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Iui_View::viewIui($success, $msg, $arr);
}
?>