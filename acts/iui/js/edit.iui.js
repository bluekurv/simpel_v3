//Objects
$("#iuiLuasBangunan").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
$("#iuiLuasTanah").maskMoney({ thousands:'.', decimal:',', precision:2, defaultZero:true, allowZero:true });
$("#iuiInventaris").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#iuiKapasitasProduksiPerTahun").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#iuiTenagaKerjaIndonesiaLaki").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#iuiTenagaKerjaIndonesiaPerempuan").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#iuiTenagaKerjaAsingLaki").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });
$("#iuiTenagaKerjaAsingPerempuan").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });

//Variables
var form = $("#myForm");
var iuiKelurahanLokasi = $("#iuiKelurahanLokasi");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'iuiKelurahanLokasi' || this.id === undefined) {
		if (iuiKelurahanLokasi.val().length == 0) {
			doSubmit = false;
			iuiKelurahanLokasi.addClass("error");		
		} else {
			iuiKelurahanLokasi.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

$("#iuiKlasifikasi").change(function() {
	$("#spanKlasifikasi").html($(this).val());
});

form.submit(submitForm);

$('#iuiKelurahanLokasi').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#iuiKelurahanLokasiID').val(suggestion.data.id);
		$('#iuiKelurahanLokasiKode').val(suggestion.data.kode);
		$('#iuiKelurahanLokasi').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

$('#iuiKlui').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=klui',
	onSelect: function(suggestion){
		$('#iuiKluiID').val(suggestion.data.id);
		$('#iuiKluiKode').val(suggestion.data.kode);
		$('#iuiKlui').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

//Set focus
$("#perNPWP").focus();