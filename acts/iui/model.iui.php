<?php
class Iui_Model {	
	public $tabel = 'iui';
	public $primaryKey = 'iuiID';
	
	public $iuiID = 0;
	public $iuiDmhnID = 0;
	public $iuiNo = 0;
	public $iuiNoLengkap = '';
	public $iuiKepemilikanPabrik = '';
	public $iuiKelurahanLokasiID = 0;	
	public $iuiLuasBangunan = 0.00;
	public $iuiLuasTanah = 0.00;
	public $iuiTenagaPenggerak = '';
	public $iuiKluiID = 0;
	public $iuiKomoditiIndustri = ''; 
	public $iuiInventaris = 0;
	public $iuiKapasitasProduksiPerTahun = 0;
	public $iuiSatuanKapasitasProduksiPerTahun = '';
	public $iuiTenagaKerjaIndonesiaLaki = 0;
	public $iuiTenagaKerjaIndonesiaPerempuan = 0;
	public $iuiTenagaKerjaAsingLaki = 0;
	public $iuiTenagaKerjaAsingPerempuan = 0;
	public $iuiKeterangan = '';
	public $iuiTglBerlaku = '0000-00-00';
	public $iuiTglDaftarUlang = '0000-00-00';
	public $iuiPejID = 0;
	public $iuiTglPengesahan = '0000-00-00'; 	 	 	 	 	
	public $iuiArsip = '';
}
?>