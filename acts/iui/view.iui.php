<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Iui_View {
	static function editIui($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=iui&task=edit&id=<?php echo $obj->iuiDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Izin Usaha Industri</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=iui&task=save" method="POST" >
				<input type="hidden" id="iuiID" name="iuiID" value="<?php echo $obj->iuiID; ?>" />
				<input type="hidden" id="iuiDmhnID" name="iuiDmhnID" value="<?php echo $obj->iuiDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" name="save" class="button-link inline dark-blue" value="Simpan" />
					<input type="submit" name="manage" class="button-link inline dark-blue" value="Kelola Mesin/Peralatan Produksi" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" name="save" class="button-link inline dark-blue" value="Simpan" />
					<input type="submit" name="manage" class="button-link inline dark-blue" value="Kelola Mesin/Peralatan Produksi" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=iui&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=iui&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->perAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Pabrik</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kepemilikan Pabrik</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="iuiKepemilikanPabrik" name="iuiKepemilikanPabrik">
<?php 
		foreach (array('Milik Sendiri', 'Sewa', 'Lainnya') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->iuiKepemilikanPabrik) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Pabrik <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->iuiKelurahanLokasiID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$iuiKelurahanLokasi = $app->queryObject($sql);
		if (!$iuiKelurahanLokasi) {
			$iuiKelurahanLokasi = new stdClass();
			$iuiKelurahanLokasi->kode = '';
			$iuiKelurahanLokasi->value = '';
		}
?>
						<input type="hidden" id="iuiKelurahanLokasiID" name="iuiKelurahanLokasiID" value="<?php echo $obj->iuiKelurahanLokasiID; ?>" />
						<input class="box readonly" id="iuiKelurahanLokasiKode" name="iuiKelurahanLokasiKode" size="8" value="<?php echo $iuiKelurahanLokasi->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="iuiKelurahanLokasi" name="iuiKelurahanLokasi" maxlength="500" size="70" value="<?php echo $iuiKelurahanLokasi->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, pergunakan sub menu yang ada di menu <b>Administrasi &gt; Lokasi</b> untuk mengelola data lokasi</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Bangunan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiLuasBangunan" name="iuiLuasBangunan" maxlength="25" size="25" value="<?php echo $app->MySQLToMoney($obj->iuiLuasBangunan,2); ?>" /> <span>m<sup>2</sup></span>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Tanah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiLuasTanah" name="iuiLuasTanah" maxlength="25" size="25" value="<?php echo $app->MySQLToMoney($obj->iuiLuasTanah,2); ?>" /> <span>m<sup>2</sup></span>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Industri (KLUI)</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT kkelKode AS kode, kkelNama AS value 
				FROM kbli
				WHERE kkelID='".$obj->iuiKluiID."'";
		$iuiKlui = $app->queryObject($sql);
		if (!$iuiKlui) {
			$iuiKlui = new stdClass();
			$iuiKlui->kode = '';
			$iuiKlui->value = '';
		}
?>
						<input type="hidden" id="iuiKluiID" name="iuiKluiID" value="<?php echo $obj->iuiKluiID; ?>" />
						<input class="box readonly" id="iuiKluiKode" name="iuiKluiKode" size="8" value="<?php echo $iuiKlui->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="iuiKlui" name="iuiKlui" maxlength="500" size="70" value="<?php echo $iuiKlui->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Komoditi Industri</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiKomoditiIndustri" name="iuiKomoditiIndustri" maxlength="255" size="50" value="<?php echo $obj->iuiKomoditiIndustri; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tenaga Penggerak</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiTenagaPenggerak" name="iuiTenagaPenggerak" maxlength="255" size="50" value="<?php echo $obj->iuiTenagaPenggerak; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nilai Inventaris (Mesin/Peralatan)</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiInventaris" name="iuiInventaris" maxlength="20" size="25" value="<?php echo $app->MySQLToMoney($obj->iuiInventaris); ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kapasitas Produksi Terpasang Per Tahun</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiKapasitasProduksiPerTahun" name="iuiKapasitasProduksiPerTahun" maxlength="20" size="25" value="<?php echo $app->MySQLToMoney($obj->iuiKapasitasProduksiPerTahun); ?>" />
						dalam satuan
						<input class="box" id="iuiSatuanKapasitasProduksiPerTahun" name="iuiSatuanKapasitasProduksiPerTahun" maxlength="255" size="50" value="<?php echo $obj->iuiSatuanKapasitasProduksiPerTahun; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiKeterangan" name="iuiKeterangan" maxlength="255" size="50" value="<?php echo $obj->iuiKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Tenaga Kerja Indonesia</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Laki-laki</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiTenagaKerjaIndonesiaLaki" name="iuiTenagaKerjaIndonesiaLaki" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->iuiTenagaKerjaIndonesiaLaki); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Perempuan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiTenagaKerjaIndonesiaPerempuan" name="iuiTenagaKerjaIndonesiaPerempuan" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->iuiTenagaKerjaIndonesiaPerempuan); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Tenaga Kerja Asing</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Laki-laki</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiTenagaKerjaAsingLaki" name="iuiTenagaKerjaAsingLaki" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->iuiTenagaKerjaAsingLaki); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Perempuan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="iuiTenagaKerjaAsingPerempuan" name="iuiTenagaKerjaAsingPerempuan" maxlength="11" size="11" value="<?php echo $app->MySQLToMoney($obj->iuiTenagaKerjaAsingPerempuan); ?>" /> orang
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->iuiNo > 0) {
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/IUI/<?php echo substr($obj->iuiTglPengesahan,0,4); ?>/</span>
						<input class="box" id="iuiNo" name="iuiNo" maxlength="5" size="5" value="<?php echo $obj->iuiNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="iuiNo" name="iuiNo" value="<?php echo $obj->iuiNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="iuiTglPengesahan" name="iuiTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->iuiTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT pnID AS id, pnNama AS nama 
				FROM pengguna 
				WHERE pnLevelAkses='Pejabat' 
				ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
		$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="iuiPejID" name="iuiPejID">
<?php 
		while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
			echo '<option value="'.$objPengesahanOleh->id.'"';
			if ($objPengesahanOleh->id == $obj->iuiPejID) {
				echo ' selected';	
			}
			echo '>'.$objPengesahanOleh->nama.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='iui'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->iuiTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->iuiTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="iuiTglBerlaku" name="iuiTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->iuiTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->iuiTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->iuiTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="iuiTglDaftarUlang" name="iuiTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->iuiTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/iui/js/edit.iui.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printIui($obj) {
		global $app;
		
		$objReport 		= new Report('Izin Usaha Industri', 'IUI-'.substr($obj->iuiTglPengesahan,0,4).'-'.$obj->iuiNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->iuiPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		p, li {
			text-align: justify;
		}
	</style>
	
	<p align="center">
		<span style="font-size:14px;"><b><u>IJIN USAHA INDUSTRI TANPA MELALUI PERSETUJUAN PRINSIP</u></b></span><br>
		<span><b>Nomor : <?php echo $obj->iuiNoLengkap; ?></b></span>
	</p>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="75%" align="justify" valign="top">
			Surat Permohonan dari Sdr <?php echo strtoupper($obj->mhnNama); ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> untuk memperoleh izin usaha industri;
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">
			<table>
				<tr><td width="4%">a.</td><td width="96%">Bahwa berdasarkan penilaian dan penelitian terhadap surat permohonan dan kelengkapan dokumen yang dilampirkan untuk melakukan kegiatan Industri yang disampaikan oleh <?php echo $obj->perNama; ?>, telah memenuhi syarat yang diperlukan, sehingga kepada perusahaan dapat diberikan Izin Usaha Industri;</td></tr>
				<tr><td>b.</td><td>Bahwa berdasarkan pertimbangan sebagaimana dimaksud pada huruf a, perlu dikeluarkan Keputusan Kepala <?php echo $objSatuanKerja->skrNama.' '.$objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?>;</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">	
<?php 
		echo $konfigurasi['consideringIUI'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" valign="top"></td>
	</tr>
	<tr>
		<td width="20%" valign="top">PERTAMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" valign="top" align="justify">Memberikan Izin Usaha Industri kepada:</td>
	</tr>
	<tr>
		<td width="20%" valign="top"></td>
		<td width="7" valign="top"></td>
		<td width="75%" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td valign="top" width="30%">Perusahaan</td>
				<td valign="top" width="2%">:</td>
				<td valign="top" width="68%"><?php echo strtoupper($obj->perNama); ?></td>
			</tr>
			<tr>
				<td valign="top">NPWP</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $obj->perNPWP; ?></td>
			</tr>
			<tr>
				<td colspan="3">Untuk menjalankan Perusahaan Industri</td>
			</tr>
			<tr>
				<td valign="top">1. Jenis Industri (KBLI)</td>
				<td valign="top">:</td>
				<td valign="top"><?php $objKlui = $app->queryObject("SELECT * FROM kbli WHERE kkelID='".$obj->iuiKluiID."'"); echo ($objKlui) ? strtoupper($objKlui->kkelNama.' ('.$objKlui->kkelKode.')') : ''; ?></td>
			</tr>
			<tr>
				<td valign="top">2. Lokasi Perusahaan</td>
				<td valign="top">:</td>
				<td valign="top"></td>
			</tr>
<?php 
		$sql = "SELECT wilayah2.wilNama AS kecamatan 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasiPerusahaan = $app->queryObject($sql);
?>
			<tr>
				<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a. Alamat Perusahaan</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $obj->perAlamat.', '.$objLokasiPerusahaan->kecamatan; ?></td>
			</tr>
<?php 
		$sql = "SELECT wilayah2.wilNama AS kecamatan 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanLokasiID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasiPabrik = $app->queryObject($sql);
?>
			<tr>
				<td valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b. Alamat Pabrik</td>
				<td valign="top">:</td>
				<td valign="top"><?php echo $objLokasiPabrik->kecamatan; ?></td>
			</tr>
			<tr>
				<td colspan="3">Dengan ketentuan persyaratan sebagaimana dimaksud pada lampiran Izin Usaha Industri ini.</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td valign="top">KEDUA</td><td valign="top">:</td>
		<td valign="top" align="justify">Izin Usaha Industri ini berlaku selama perusahaan industri ini beroperasi /melakukan kegiatan dan apabila dalam jangka waktu 3 (tiga) tahun terhitung mulai tanggal Surat Pernyataan dibuat tidak merealisasikan pembangunan pabrik dan sarana produksi serta tidak memenuhi kesanggupan yang tercantum dalam Surat Pernyataan, Izin Usaha Industri ini dinyatakan batal demi hukum.</td>
	</tr>
	<tr>
		<td valign="top">KETIGA</td><td valign="top">:</td>
		<td valign="top" align="justify">Izin Usaha Industri ini merupakan bagian yang tidak terpisahkan dengan Surat Pernyataan tertanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> yang dibuat oleh perusahaan yang bersangkutan</td>
	</tr>
	<tr>
		<td valign="top">KEEMPAT</td><td valign="top">:</td>
		<td valign="top" align="justify">Izin Usaha Industri ini terlepas dari izin-izin yang diharuskan berdasarkan peraturan perundang-undangan lain;</td>
	</tr>
	<tr>
		<td valign="top">KELIMA</td><td valign="top">:</td>
		<td valign="top" align="justify">Izin Usaha Industri ini mulai berlaku pada tanggal ditetapkan.</td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->iuiTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		$pdf->setCellHeightRatio(1.25); 
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->setCellHeightRatio($objReport->cellHeightRatio); 
		
		ob_start();
?>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="15%"><h3>LAMPIRAN</h3></td><td width="10"><h3>:</h3></td>
		<td width="85%"><h3>IZIN USAHA INDUSTRI</h3></td>
	</tr>
	<tr>
		<td><h3>NOMOR</h3></td><td width="10"><h3>:</h3></td>
		<td><h3><?php echo $obj->iuiNoLengkap; ?></h3></td>
	</tr>
	</table>
	
	&nbsp;<br/>
	<table border="1" cellpadding="3" cellspacing="0" width="100%">
	<tr>
		<td width="30" align="center">No.</td>
		<td width="<?php echo ($objReport->width - 230) / 2; ?>" align="center">Jenis Mesin</td>
		<td width="40" align="center">Jumlah</td>
		<td width="<?php echo ($objReport->width - 230) / 2; ?>" align="center">Merek/Spesifikasi Negara Asal</td>
		<td width="80" align="center">Kapasitas Mesin</td>
		<td width="80" align="center">Harga<br>(Rp)</td>
	</tr>
<?php 
		$sql = "SELECT * FROM  mesiniui WHERE msnIuiID='".$obj->iuiID."'";
		$rs = $app->query($sql);
		$jumlah = 0;
		$i = 1;
		while(($v = mysql_fetch_object($rs)) == true){
?>
	<tr>
		<td align="right"><?php echo $i; ?>.</td>
		<td><?php echo strtoupper($v->msnJenis); ?></td>
		<td align="center"><?php echo $app->MySQLToMoney($v->msnJumlah); ?></td>
		<td><?php echo strtoupper(trim($v->msnMerek.' '.$v->msnSpesifikasiNegaraAsal)); ?></td>
		<td><?php echo strtoupper($app->MySQLToMoney($v->msnKapasitasMesin, 2).' '.$v->msnSatuanKapasitasMesin); ?></td>
		<td align="right"><?php echo $app->MySQLToMoney($v->msnHarga); ?></td>
	</tr>
<?php
			$jumlah = bcadd($jumlah, $v->msnHarga);
			$i++;
		}
?>
	<tr>
		<td colspan="5" align="right">JUMLAH</td>
		<td align="right"><?php echo $app->MySQLToMoney($jumlah); ?></td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="50%">&nbsp;</td>
		<td width="50%">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->iuiTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readIui($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=iui&task=read&id=<?php echo $obj->iuiDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Izin Usaha Industri</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=iui&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=iui&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Pabrik</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kepemilikan Pabrik</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->iuiKepemilikanPabrik; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Lokasi Pabrik</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->iuiKelurahanLokasiID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Luas Bangunan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->iuiLuasBangunan,2); ?> m<sup>2</sup>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Luas Tanah</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->iuiLuasTanah,2); ?> m<sup>2</sup>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tenaga Penggerak</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->iuiTenagaPenggerak; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis Industri (KLUI)</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT kkelKode AS kode, kkelNama AS value 
				FROM kbli
				WHERE kkelID='".$obj->iuiKluiID."'";
		$iuiKlui = $app->queryObject($sql);
		if (!$iuiKlui) {
			$iuiKlui = new stdClass();
			$iuiKlui->kode = '';
			$iuiKlui->value = '';
		}
?>
				<?php echo $iuiKlui->kode.' '.$iuiKlui->value; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Komoditi Industri</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->iuiKomoditiIndustri; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kapasitas Produksi Per Tahun</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->iuiKapasitasProduksiPerTahun); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Satuan Kapasitas Produksi Per Tahun</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->iuiSatuanKapasitasProduksiPerTahun; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Inventaris</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->iuiInventaris); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Tenaga Kerja</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Indonesia (Laki-laki)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->iuiTenagaKerjaIndonesiaLaki); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Indonesia (Perempuan)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->iuiTenagaKerjaIndonesiaPerempuan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Asing (Laki-laki)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->iuiTenagaKerjaAsingLaki); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Asing (Perempuan)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->iuiTenagaKerjaAsingPerempuan); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->iuiNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->iuiTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->iuiPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->iuiTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->iuiTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->iuiTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->iuiTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/iui/js/read.iui.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewIui($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=iui"><img src="images/icons/rosette.png" border="0" /> Buku Register Izin Usaha Industri</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('iuiNo', 'Nomor Surat', true),
			$app->setHeader('iuiTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('iuiTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('iuiTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=iui&task=read&id='.$v->iuiDmhnID.'&fromact=iui" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->iuiDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=iui&task=read&id='.$v->iuiDmhnID.'&fromact=iui" title="Entri">'.$v->iuiNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->iuiTglPengesahan).'</td>';
				
				if ($v->iuiTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->iuiTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->iuiTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->iuiTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/iui/js/iui.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>