<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.bidang.php';
require_once 'view.bidang.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editBidang($app->id);
		break;
	case 'delete':
		deleteBidang($app->id);
		break;
	case 'save':
		saveBidang();
		break;
	default:
		viewBidang(true, '');
		break;
}

function deleteBidang($id) {
	global $app;
	
	//Get object
	$objBidang = $app->queryObject("SELECT * FROM bidang WHERE bidID='".$id."'");
	if (!$objBidang) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalSubBidang = intval($app->queryField("SELECT COUNT(*) AS total FROM subbidang WHERE subBidID='".$id."'"));
	if ($totalSubBidang > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalSubBidang.' sub bidang di bawah bidang tersebut';
	}
	
	$totalPengguna = intval($app->queryField("SELECT COUNT(*) AS total FROM pengguna WHERE pnOrganisasi='Bidang' AND pnOrganisasiID='".$id."'"));
	if ($totalPengguna > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalPengguna.' pengguna pada bidang tersebut';
	}
	
	$totalJenisSuratIzin = intval($app->queryField("SELECT COUNT(*) AS total FROM jenissuratizin WHERE jsiBidID='".$id."'"));
	if ($totalJenisSuratIzin > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalJenisSuratIzin.' jenis surat izin untuk bidang tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM bidang WHERE bidID='".$id."'");
		
		$success = true;
		$msg = 'Bidang "'.$objBidang->bidNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'bidang', $objBidang->bidID, $objBidang->bidNama, $msg);
	} else {
		$success = false;
		$msg = 'Bidang "'.$objBidang->bidNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewBidang($success, $msg);
}

function editBidang($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objBidang = $app->queryObject("SELECT * FROM bidang WHERE bidID='".$id."'");
		if (!$objBidang) {
			$app->showPageError();
			exit();
		}
	} else {
		$objBidang = new Bidang_Model();
	}
	
	Bidang_View::editBidang(true, '', $objBidang);
}

function saveBidang() {
	global $app;
	
	//Create object
	$objBidang = new Bidang_Model();
	$app->bind($objBidang);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objBidang->bidNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT bidID AS id FROM bidang WHERE bidNama='".$objBidang->bidNama."'", $objBidang->bidID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objBidang->bidNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objBidang->bidID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objBidang);
		} else {
			$sql = $app->createSQLforUpdate($objBidang);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objBidang->bidID = $app->queryField("SELECT LAST_INSERT_ID() FROM bidang");
			
			$success = true;
			$msg = 'Bidang "'.$objBidang->bidNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'bidang', $objBidang->bidID, $objBidang->bidNama, $msg);
		} else {
			$success = true;
			$msg = 'Bidang "'.$objBidang->bidNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'bidang', $objBidang->bidID, $objBidang->bidNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Bidang "'.$objBidang->bidNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewBidang($success, $msg);
	} else {
		Bidang_View::editBidang($success, $msg, $objBidang);
	}
}

function viewBidang($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('bidang', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('bidang', 'sort', 'bidNama');
	$dir   		= $app->pageVar('bidang', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('bidang', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "bidNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM bidang
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM bidang
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Bidang_View::viewBidang($success, $msg, $arr);
}
?>