//Variables
var form = $("#myForm");
var bidNama = $("#bidNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'bidNama' || this.id === undefined) {
		if (bidNama.val().length == 0){
			doSubmit = false;
			bidNama.addClass("error");		
		} else {
			bidNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
bidNama.blur(validateForm);

bidNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
bidNama.focus();