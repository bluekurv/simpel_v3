<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.groupperusahaan.php';
require_once 'view.groupperusahaan.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editGroupPerusahaan($app->id);
		break;
	case 'delete':
		deleteGroupPerusahaan($app->id);
		break;
	case 'save':
		saveGroupPerusahaan();
		break;
	default:
		viewGroupPerusahaan(true, '');
		break;
}

function deleteGroupPerusahaan($id) {
	global $app;
	
	//Get object
	$objGroupPerusahaan = $app->queryObject("SELECT * FROM groupperusahaan WHERE grpID='".$id."'");
	if (!$objGroupPerusahaan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM groupperusahaan WHERE grpID='".$id."'");
		
		$success = true;
		$msg = 'Group Perusahaan "'.$objGroupPerusahaan->grpNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'groupperusahaan', $objGroupPerusahaan->grpID, $objGroupPerusahaan->grpNama, $msg);
	} else {
		$success = false;
		$msg = 'Group Perusahaan "'.$objGroupPerusahaan->grpNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewGroupPerusahaan($success, $msg);
}

function editGroupPerusahaan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objGroupPerusahaan = $app->queryObject("SELECT * FROM groupperusahaan WHERE grpID='".$id."'");
		if (!$objGroupPerusahaan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objGroupPerusahaan = new GroupPerusahaan_Model();
	}
	
	GroupPerusahaan_View::editGroupPerusahaan(true, '', $objGroupPerusahaan);
}

function saveGroupPerusahaan() {
	global $app;
	
	//Create object
	$objGroupPerusahaan = new GroupPerusahaan_Model();
	$app->bind($objGroupPerusahaan);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objGroupPerusahaan->grpKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objGroupPerusahaan->grpNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT grpID AS id FROM groupperusahaan WHERE grpKode='".$objGroupPerusahaan->grpKode."'", $objGroupPerusahaan->grpID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objGroupPerusahaan->grpKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT grpID AS id FROM groupperusahaan WHERE grpNama='".$objGroupPerusahaan->grpNama."'", $objGroupPerusahaan->grpID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objGroupPerusahaan->grpNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objGroupPerusahaan->grpID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objGroupPerusahaan);
		} else {
			$sql = $app->createSQLforUpdate($objGroupPerusahaan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objGroupPerusahaan->grpID = $app->queryField("SELECT LAST_INSERT_ID() FROM groupperusahaan");
			
			$success = true;
			$msg = 'Group Perusahaan "'.$objGroupPerusahaan->grpNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'groupperusahaan', $objGroupPerusahaan->grpID, $objGroupPerusahaan->grpNama, $msg);
		} else {
			$success = true;
			$msg = 'Group Perusahaan "'.$objGroupPerusahaan->grpNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'groupperusahaan', $objGroupPerusahaan->grpID, $objGroupPerusahaan->grpNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Group Perusahaan "'.$objGroupPerusahaan->grpNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewGroupPerusahaan($success, $msg);
	} else {
		GroupPerusahaan_View::editGroupPerusahaan($success, $msg, $objGroupPerusahaan);
	}
}

function viewGroupPerusahaan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('groupperusahaan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('groupperusahaan', 'sort', 'grpKode');
	$dir   		= $app->pageVar('groupperusahaan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('groupperusahaan', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(grpKode LIKE '%".$nama."%' OR grpNama LIKE '%".$nama."%')";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM groupperusahaan
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM groupperusahaan
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	GroupPerusahaan_View::viewGroupPerusahaan($success, $msg, $arr);
}
?>