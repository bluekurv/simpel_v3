//Variables
var form = $("#myForm");
var grKode = $("#grKode");
var grpNama = $("#grpNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'grKode' || this.id === undefined) {
		if (grKode.val().length == 0){
			doSubmit = false;
			grKode.addClass("error");		
		} else {
			grKode.removeClass("error");
		}
	}
	
	if (this.id == 'grpNama' || this.id === undefined) {
		if (grpNama.val().length == 0){
			doSubmit = false;
			grpNama.addClass("error");		
		} else {
			grpNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
grKode.blur(validateForm);
grpNama.blur(validateForm);

grKode.keyup(validateForm);
grpNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
grKode.focus();