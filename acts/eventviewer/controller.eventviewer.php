<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'view.eventviewer.php';

switch ($app->task) {
	default:
		viewEventViewer();
		break;
}

function viewEventViewer() {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('eventviewer', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('eventviewer', 'sort', 'evDibuatPada');
	$dir   		= $app->pageVar('eventviewer', 'dir', 'desc');
	
	//Add filter(s)
	$waktu 		= $app->getStr('filterwaktu', date('Y-m-d'));
		
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'waktu' => $waktu
		), 
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT * 
				  FROM eventviewer
				  WHERE DAY(evDibuatPada)='".substr($waktu,8,2)."' AND MONTH(evDibuatPada)='".substr($waktu,5,2)."' AND YEAR(evDibuatPada)='".substr($waktu,0,4)."'";
	$rs_count = $app->query($count_sql);
	
	$totaldata = mysql_num_rows($rs_count);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = $count_sql." LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	EventViewer_View::viewEventViewer($arr);
}
?>