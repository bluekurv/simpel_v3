//Functions
function lihat(page) {
	var url = 'index2.php?act=eventviewer&page=' + page;
	url += '&filterwaktu=' + $('#filterwaktu2').val();
	
	window.open(url, '_self');
}

function sort(field, direction) {
	var url = 'index2.php?act=eventviewer';
	url += '&sort=' + field;
	url += '&dir=' + direction;
	
	window.open(url, '_self');
}

//Events
$("#page").change(function() {
	lihat(this.value);
});

$("#filterwaktu").datepicker({
	altField: "#filterwaktu2",
	altFormat: "yy-mm-dd",
	dateFormat: "dd-mm-yy"
});

$("#filterwaktu").change(function() {
	lihat(1);
});

$('#filterwaktu').keypress(function(e) {
	var code = (e.keyCode ? e.keyCode : e.which);
	if (code == 13) {
		lihat(1);
	}
});