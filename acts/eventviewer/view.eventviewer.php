<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class EventViewer_View {
	static function viewEventViewer($arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=eventviewer"><img src="images/icons/calendar.png" border="0" /> Event Viewer</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="pageFilter">
				Tanggal : 
				<input class="box" id="filterwaktu" name="filterwaktu" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($arr['filter']['waktu']); ?>" style="color:#808080;" />
				<input type="hidden" id="filterwaktu2" name="filterwaktu2" value="<?php echo $arr['filter']['waktu']; ?>" />
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('evTipe', '', true, 16),
			$app->setHeader('evDibuatPada', 'Waktu', true, 110),
			$app->setHeader('evDibuatOlehNama', 'Pengguna', true),
			$app->setHeader('evAksi', 'Deskripsi', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				switch ($v->evTipe) {
					case 0:
						echo '<td><img src="images/icons/information.png" /></td>';
						break;
					case 1:
						echo '<td><img src="images/icons/error.png" /></td>';
						break;
					case 2:
						echo '<td><img src="images/icons/exclamation.png" /></td>';
						break;
				}
				echo '<td>'.$app->MySQLDateToNormal($v->evDibuatPada).'</td>';
				echo '<td>'.$v->evDibuatOlehNama.'</td>';
				echo '<td>'.$v->evAksi.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="4">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/eventviewer/js/eventviewer.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>