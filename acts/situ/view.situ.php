<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Situ_View {
	static function editSitu($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=situ&task=edit&id=<?php echo $obj->situDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Surat Izin Tempat Usaha</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=situ&task=save" method="POST" >
				<input type="hidden" id="situID" name="situID" value="<?php echo $obj->situID; ?>" />
				<input type="hidden" id="situDmhnID" name="situDmhnID" value="<?php echo $obj->situDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->situImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('situGrupID', $obj->situGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=situ&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=situ&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->perAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//HACK
		if ($obj->situAlamat == '') {
			$obj->situAlamat = $objDetailPermohonan->perAlamat;
		}
?>
						<input class="box" id="situAlamat" name="situAlamat" maxlength="500" size="100" value="<?php echo $obj->situAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//HACK
		if ($obj->situKelurahanID == '') {
			$obj->situKelurahanID = $objDetailPermohonan->perKelurahanID;
		}
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->situKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$situKelurahan = $app->queryObject($sql);
		if (!$situKelurahan) {
			$situKelurahan = new stdClass();
			$situKelurahan->kode = '';
			$situKelurahan->value = '';
		}
?>
						<input type="hidden" id="situKelurahanID" name="situKelurahanID" value="<?php echo $obj->situKelurahanID; ?>" />
						<input class="box readonly" id="situKelurahanKode" name="situKelurahanKode" size="8" value="<?php echo $situKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="situKelurahan" name="situKelurahan" maxlength="500" size="70" value="<?php echo $situKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, informasikan kepada Administrator untuk menambahkannya</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situJenisUsaha" name="situJenisUsaha" maxlength="255" size="50" value="<?php echo $obj->situJenisUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situBidangUsaha" name="situBidangUsaha" maxlength="255" size="50" value="<?php echo $obj->situBidangUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situMerekUsaha" name="situMerekUsaha" maxlength="255" size="50" value="<?php echo $obj->situMerekUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situNPWPD" name="situNPWPD" maxlength="255" size="50" value="<?php echo $obj->situNPWPD; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Luas Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situLuasTempatUsaha" name="situLuasTempatUsaha" maxlength="255" size="50" value="<?php echo $obj->situLuasTempatUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Status Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="situStatusTempatUsaha" name="situStatusTempatUsaha">
<?php 
		foreach (array('Hak Milik', 'Hak Pakai', 'Sewa') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->situStatusTempatUsaha) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Rekomendasi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<!-- <input class="box" id="situRekomendasiCamat" name="situRekomendasiCamat" maxlength="255" size="50" value="<?php echo $obj->situRekomendasiCamat; ?>" /> -->
						<textarea class="box" id="situRekomendasiCamat" name="situRekomendasiCamat" cols="80" rows="5"><?php echo $obj->situRekomendasiCamat; ?></textarea>
						<br><i>Setiap rekomendasi ditulis dan diakhiri dengan menekan tombol ENTER</i>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="situKeterangan" name="situKeterangan" maxlength="255" size="50" value="<?php echo $obj->situKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->situNo > 0) {
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/SITU/<?php echo substr($obj->situTglPengesahan,0,4); ?>/</span>
						<input class="box" id="situNo" name="situNo" maxlength="5" size="5" value="<?php echo $obj->situNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="situNo" name="situNo" value="<?php echo $obj->situNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="situTglPengesahan" name="situTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->situTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="situPejID" name="situPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->situPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='situ'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->situTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->situTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="situTglBerlaku" name="situTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->situTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->situTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->situTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="situTglDaftarUlang" name="situTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->situTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/situ/js/edit.situ.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printSitu($obj) {
		global $app;
		
		$objReport 		= new Report('Surat Izin Tempat Usaha', 'SITU-'.substr($obj->situTglPengesahan,0,4).'-'.$obj->situNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->situPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<h4 align="center">
		KEPUTUSAN KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?><br>
		NOMOR : <?php echo $obj->situNoLengkap; ?><br>
		TENTANG<br>
		SURAT IZIN TEMPAT USAHA<br>
		<br>
		KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?>,
	</h4>
	
		<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellpadding="0" cellspacing="0">
<?php 
		$situRekomendasiCamat = explode("\n",$obj->situRekomendasiCamat);
		$situRekomendasiCamat2 = array();
		
		if (count($situRekomendasiCamat) > 0) {
			foreach ($situRekomendasiCamat as $rekomendasi) {
				if (trim($rekomendasi) != '') {
					$situRekomendasiCamat2[] = trim($rekomendasi);
				}
			}
		}
		
		
		
		if (count($situRekomendasiCamat2) > 0) {
?>
				<tr><td width="4%">1.&nbsp;</td><td width="96%">Surat Permohonan dari Sdr <?php echo strtoupper($obj->mhnNama); ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> untuk mendapatkan Izin Tempat Usaha.</td></tr>
<?php
			$no = 2;
			
			foreach ($situRekomendasiCamat2 as $rekomendasi) {
?>
				<tr><td><?php echo $no; ?>.&nbsp;</td><td><?php echo trim($rekomendasi); ?></td></tr>
<?php
				$no++;
			}
		} else {
?>
				<tr><td>Surat Permohonan dari Sdr <?php echo strtoupper($obj->mhnNama); ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> untuk mendapatkan Izin Tempat Usaha.</td></tr>
<?php
		}
?>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">
			<table>
				<tr><td width="4%">a.</td><td width="96%">Bahwa pengendalian, penertiban dan pengawasan bidang usaha perlu di atur oleh Pemerintah Daerah.</td></tr>
				<tr><td>b.</td><td>Bahwa untuk memenuhi persyaratan pengurusan selanjutnya dari usaha yang bersangkutan, maka perlu izin dari Kepala <?php echo $objSatuanKerja->skrNama; ?>.</td></tr>
				<tr><td>c.</td><td>Bahwa berdasarkan pertimbangan sebagaimana dimaksud Huruf a, Huruf b perlu menetapkan Izin Tempat Usaha.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
<?php 
		echo $konfigurasi['consideringSITU'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20%" align="left" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">PERTAMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Memberikan Izin Tempat Usaha kepada :</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">
			<table width="100px" height="100px" border="0">
			<tr>
				<td>
					<img src="<?php echo CFG_LIVE_PATH; ?>/images/report/3x4.png" width="93" height="122">
				</td>
			</tr>
			</table>
		</td>
		<td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table border="0" cellspacing="0">
			<tr>
				<td align="left" valign="top" colspan="3">&nbsp;&nbsp;</td>
			</tr>
			<tr>
				<td align="left" valign="top" width="25%">Nama</td>
				<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="73%"><?php echo strtoupper($obj->mhnNama); ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">No KTP</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->mhnNoKtp; ?></td>
			</tr>
			<tr>
			  	<td align="left" valign="top">Jenis Usaha</td>
			  	<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->situJenisUsaha; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Merek/Nama Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->situMerekUsaha; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Nomor NPWPD</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->situNPWPD; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Tempat Usaha</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->perAlamat; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Kelurahan/Desa</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->perKelurahan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Kecamatan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->perKecamatan; ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEDUA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top">
			Pemegang Izin ini harus memenuhi ketentuan sebagai berikut :
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top"></td><td width="7" align="left" valign="top"></td>
		<td width="75%" align="left" valign="top">
			<table>
				<tr><td width="4%">1.</td><td width="95%">Memasang Papan Merek sebagaimana tersebut pada Diktum pertama.</td></tr>
				<tr><td width="4%">2.</td><td width="95%">Memelihara Ketertiban / Keamanan dan Kebersihan lingkungan.</td></tr>
				<tr><td width="4%">3.</td><td width="95%">Memenuhi dan mentaati ketentuan Peraturan Perundang-undangan yang berlaku.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KETIGA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Izin Tempat Usaha ini berlaku selama 5 Tahun.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KEEMPAT</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Surat Izin ini dicabut kembali, apabila Pengusaha/Pemegang Izin tidak mentaati ketentuan-ketentuan tersebut di atas dan melanggar Peraturan Perundang-undangan yang berlaku.</td>
	</tr>
	<tr>
		<td width="20%" align="left" valign="top">KELIMA</td><td width="7" align="left" valign="top">:</td>
		<td width="75%" align="left" valign="top" align="justify">Keputusan ini mulai berlaku sejak tanggal ditetapkan.</td>
	</tr>
	</table>

	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="48%">&nbsp;</td>
		<td width="52%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->situTglPengesahan)); ?>
		</td>
	</tr>
	</table>	

	<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:8px">
	<tr>
		<td width="50">Tembusan :</td>
		<td>Disampaikan Kepada Yth. :</td>
	</tr>
	<tr>
		<td></td>
		<td>1. Kepala BPKAD <?php echo $objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama; ?></td>
	</tr>
	<tr>
		<td></td>
		<td>2. Camat <?php echo $obj->perKecamatan; ?></td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	static function readSitu($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=situ&task=edit&id=<?php echo $obj->situDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Surat Izin Tempat Usaha</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=situ&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=situ&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situJenisUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situBidangUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situMerekUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">NPWPD</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situNPWPD; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Luas Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situLuasTempatUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Status Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situStatusTempatUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Rekomendasi</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		//echo $obj->situRekomendasiCamat; 
		$situRekomendasiCamat = explode("\n",$obj->situRekomendasiCamat);
		
		if (count($situRekomendasiCamat) > 0) {
			foreach ($situRekomendasiCamat as $rekomendasi) {
				if (trim($rekomendasi) != '') {
					echo trim($rekomendasi)."<br>";
				}
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->situNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->situTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->situPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->situTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->situTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->situTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->situTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/situ/js/read.situ.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewSitu($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=situ"><img src="images/icons/rosette.png" border="0" /> Buku Register Surat Izin Tempat Usaha</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('situNo', 'Nomor Surat', true),
			$app->setHeader('situTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('situTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('situTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=situ&task=read&id='.$v->situDmhnID.'&fromact=situ" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->situDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=situ&task=read&id='.$v->situDmhnID.'&fromact=situ" title="Entri">'.$v->situNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->situTglPengesahan).'</td>';
				
				if ($v->situTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->situTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->situTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->situTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/situ/js/situ.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>