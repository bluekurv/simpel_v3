<?php
class Situ_Model {	
	public $tabel = 'situ';
	public $primaryKey = 'situID';
	
	public $situID = 0;
	public $situDmhnID = 0;
	public $situGrupID = 0;
	public $situNo = 0;
	public $situNoLengkap = '';
	public $situNPWPD = '';
	public $situJenisUsaha = '';
	public $situBidangUsaha = '';
	public $situMerekUsaha = '';
	public $situAlamat = '';
	public $situKelurahanID = 0;
	public $situLuasTempatUsaha = '';
	public $situStatusTempatUsaha = '';
	public $situRekomendasiCamat = '';
	public $situKeterangan = '';
	public $situTglBerlaku = '0000-00-00';	
	public $situTglDaftarUlang = '0000-00-00';
	public $situPejID = 0;
	public $situTglPengesahan = '0000-00-00'; 
	public $situArsip = '';
}
?>