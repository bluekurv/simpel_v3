<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.situ.php';
require_once 'view.situ.php';

switch ($app->task) {
	case 'add':
		addSitu($app->id);
		break;
	case 'edit':
		editSitu($app->id);
		break;
	case 'delete':
		deleteSitu($app->id);
		break;
	case 'print':
		printSitu($app->id);
		break;
	case 'read':
		readSitu($app->id);
		break;
	case 'save':
		saveSitu();
		break;
	default:
		viewSitu(true, '');
		break;
}

function addSitu($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editSitu($id);
}

function deleteSitu($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM situ WHERE situDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Izin Tempat Usaha berhasil dihapus';
	
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Izin Tempat Usaha', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function editSitu($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objSitu = $app->queryObject("SELECT * FROM situ WHERE situDmhnID='".$id."'");
	if (!$objSitu) {
		$objSitu = new Situ_Model();
		
		//Ambil nilai default
		//$objSitu->situNPWPD = $objDetailPermohonan->perNPWPD;
		$objSitu->situJenisUsaha = $objDetailPermohonan->perJenisUsaha;
		$objSitu->situBidangUsaha = $objDetailPermohonan->perBidangUsaha;
		$objSitu->situMerekUsaha = $objDetailPermohonan->perMerekUsaha;
		$objSitu->situLuasTempatUsaha = $objDetailPermohonan->perLuasTempatUsaha;
		$objSitu->situStatusTempatUsaha = $objDetailPermohonan->perStatusTempatUsaha;
	}
	
	if ($objSitu->situNPWPD == "") {
		$objSitu->situNPWPD = $app->queryField("SELECT wprNoPokok FROM wajibpajakretribusi WHERE wprNama='".$objDetailPermohonan->perNama."' AND wprJenis='Pajak'");
	}
	
	$objSitu->situDmhnID = $objDetailPermohonan->dmhnID;
	
	Situ_View::editSitu(true, '', $objDetailPermohonan, $objSitu);
}

function printSitu($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN situ ON situDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objSitu = $app->queryObject($sql);
	if (!$objSitu) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objSitu->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	//HACK
	if ($objSitu->situAlamat != '') {
		$objSitu->perAlamat = $objSitu->situAlamat;
	}
	if ($objSitu->situKelurahanID > 0) {
		$objSitu->perKelurahanID = $objSitu->situKelurahanID;
	}
	
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$objSitu->perKelurahanID."'");
	$objSitu->perKelurahan = $objKelurahan->wilNama;
	$objSitu->perKecamatan = $app->queryField("SELECT wilNama FROM wilayah WHERE wilID='".$objKelurahan->wilParentID."'");
		
	Situ_View::printSitu($objSitu);
}

function readSitu($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN situ ON situDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objSitu = $app->queryObject($sql);
	if (!$objSitu) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objSitu->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Situ_View::readSitu($objSitu);
}

function saveSitu() {
	global $app;
	
	//Get object
	$objSituSebelumnya = $app->queryObject("SELECT * FROM situ WHERE situID='".$app->getInt('situID')."'");
	if (!$objSituSebelumnya) {
		$objSituSebelumnya = new Situ_Model();
	}
	
	//Create object
	$objSitu = new Situ_Model();
	$app->bind($objSitu);
	
	//Modify object (if necessary)
	$objSitu->situTglPengesahan = $app->NormalDateToMySQL($objSitu->situTglPengesahan);
	if ($objSitu->situTglPengesahan != '0000-00-00') {
		if ($objSituSebelumnya->situNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objSitu->situNo = intval($app->queryField("SELECT MAX(situNo) AS nomor FROM situ WHERE YEAR(situTglPengesahan)='".substr($objSitu->situTglPengesahan,0,4)."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objSitu->situTglBerlaku = date('Y-m-d', strtotime($objSitu->situTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objSitu->situTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objSitu->situTglDaftarUlang = date('Y-m-d', strtotime($objSitu->situTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objSitu->situTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objSitu->situNo = 0;
		
		$objSitu->situTglBerlaku = '0000-00-00';
		$objSitu->situTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objSitu->situNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/SITU/'.substr($objSitu->situTglPengesahan,0,4).'/'.$objSitu->situNo;
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objSitu->situNo > 0){
		$isExist = $app->isExist("SELECT situID AS id FROM situ WHERE situNo='".$objSitu->situNo."' AND YEAR(situTglPengesahan)='".substr($objSitu->situTglPengesahan,0,4)."'", $objSitu->situID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objSitu->situNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objSitu->situID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objSitu);
		} else {
			$sql = $app->createSQLforUpdate($objSitu);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objSitu->situID = $app->queryField("SELECT LAST_INSERT_ID() FROM situ");
		}
		
		$success = true;
		$msg = 'Izin Tempat Usaha berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'situ', $objSitu->situID, $objSitu->situNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objSitu->situDmhnID."'");
	
		$app->query("UPDATE perusahaan 
					 SET 
					 	 perJenisUsaha='".$objSitu->situJenisUsaha."',
					 	 perBidangUsaha='".$objSitu->situBidangUsaha."',
					 	 perMerekUsaha='".$objSitu->situMerekUsaha."',
					 	 perNPWPD='".$objSitu->situNPWPD."',
					 	 perLuasTempatUsaha='".$objSitu->situLuasTempatUsaha."',
					 	 perStatusTempatUsaha='".$objSitu->situStatusTempatUsaha."'
					 WHERE perID='".$app->getInt('perID')."'");
	} else {
		$success = false;
		$msg = 'Izin Tempat Usaha tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objSitu->situDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Situ_View::editSitu($success, $msg, $objDetailPermohonan, $objSitu);
	}
}

function viewSitu($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('situ', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('situ', 'sort', 'situTglPengesahan');
	//$dir		= $app->pageVar('situ', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('situ', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('situ', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(situNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(situTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM situ
				  LEFT JOIN detailpermohonan ON situDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM situ
			LEFT JOIN detailpermohonan ON situDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(situTglPengesahan) DESC, situNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Situ_View::viewSitu($success, $msg, $arr);
}
?>