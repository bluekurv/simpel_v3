//Variables
var form = $("#myForm");

//Functions
function validateForm() {
	var doSubmit = true;
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

$('#situKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#situKelurahanID').val(suggestion.data.id);
		$('#situKelurahanKode').val(suggestion.data.kode);
		$('#situKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

form.submit(submitForm);

//Set focus
$("#situJenisUsahaPerusahaan").focus();