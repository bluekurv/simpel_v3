<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Siup_View {
	static function editSiup($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<link rel="stylesheet" type="text/css" href="js/jquery.treeview/jquery.treeview.css">
	<script type="text/javascript" src="js/jquery.treeview/jquery.treeview.js"></script>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=siup&task=edit&id=<?php echo $obj->siupDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Surat Izin Usaha Perdagangan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=siup&task=save" method="POST" >
				<input type="hidden" id="siupID" name="siupID" value="<?php echo $obj->siupID; ?>" />
				<input type="hidden" id="siupDmhnID" name="siupDmhnID" value="<?php echo $obj->siupDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
		if ($obj->siupImpor > 0) {
			$app->showMessage(true, 'Informasi<h2>Data perizinan hasil impor</h2>');
		}
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('siupGrupID', $obj->siupGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=siup&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jabatan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="siupJabatanPemilik" name="siupJabatanPemilik" maxlength="255" size="50" value="<?php echo $obj->siupJabatanPemilik; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input type="hidden" id="perID" name="perID" value="<?php echo $objDetailPermohonan->perID; ?>" />
						<a href="index2.php?act=perusahaan&task=read&id=<?php echo $objDetailPermohonan->perID; ?>&fromact=siup&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->perNama; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->perAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Lokasi Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//HACK
		if ($obj->siupAlamat == '') {
			$obj->siupAlamat = $objDetailPermohonan->perAlamat;
		}
?>
						<input class="box" id="siupAlamat" name="siupAlamat" maxlength="500" size="100" value="<?php echo $obj->siupAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		//HACK
		if ($obj->siupKelurahanID == '') {
			$obj->siupKelurahanID = $objDetailPermohonan->perKelurahanID;
		}
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->siupKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$siupKelurahan = $app->queryObject($sql);
		if (!$siupKelurahan) {
			$siupKelurahan = new stdClass();
			$siupKelurahan->kode = '';
			$siupKelurahan->value = '';
		}
?>
						<input type="hidden" id="siupKelurahanID" name="siupKelurahanID" value="<?php echo $obj->siupKelurahanID; ?>" />
						<input class="box readonly" id="siupKelurahanKode" name="siupKelurahanKode" size="8" value="<?php echo $siupKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="siupKelurahan" name="siupKelurahan" maxlength="500" size="70" value="<?php echo $siupKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan nama kelurahan untuk menampilkan pilihan dalam format Kabupaten/Kota &gt; Kecamatan &gt; Kelurahan/Desa, kemudian pilih kelurahan yang diinginkan. Jika tidak tercantum atau tidak sesuai, informasikan kepada Administrator untuk menambahkannya</i>)</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="siupMerekUsaha" name="siupMerekUsaha" maxlength="255" size="50" value="<?php echo $obj->siupMerekUsaha; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Status Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="siupStatusTempatUsaha" name="siupStatusTempatUsaha">
<?php 
		foreach (array('Hak Milik', 'Hak Pakai', 'Sewa') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->siupStatusTempatUsaha) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">NPWP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="siupNPWP" name="siupNPWP" maxlength="255" size="50" value="<?php echo $obj->siupNPWP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis SIUP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="siupJenis" name="siupJenis">
<?php 
		foreach (array('Kecil', 'Menengah', 'Besar') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->siupJenis) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Modal Perusahaan (Rp)</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="siupModal" name="siupModal" maxlength="255" size="50" value="<?php echo $app->MySQLToMoney($obj->siupModal); ?>" /><br>
						<i>(Nilai modal dan kekayaan bersih perusahaan seluruhnya tidak termasuk Tanah dan Bangunan Tempat Usaha)</i>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelembagaan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="siupKelembagaan" name="siupKelembagaan" maxlength="255" size="50" value="<?php echo $obj->siupKelembagaan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kegiatan Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="siupKegiatanUsaha" name="siupKegiatanUsaha">
<?php 
		foreach (array('Perdagangan Barang', 'Perdagangan Jasa', 'Perdagangan Barang dan Jasa') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $obj->siupKegiatanUsaha) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
					<td>
						<div class="box readonly" id="bidangusaha" name="bidangusaha" style="padding:5px;">
<?php 
		$bidangusaha = array();
		
		$sqlBidangUsaha = "SELECT * FROM siupbidangusaha WHERE sbuSiupID='".$obj->siupID."'";
		$rsBidangUsaha = $app->query($sqlBidangUsaha);
		
		while(($objBidangUsaha = mysql_fetch_object($rsBidangUsaha)) == true){
			$bidangusaha[] = $objBidangUsaha;
		}
		//BEGIN FIK:
		if (count($bidangusaha) > 0) {
			foreach ($bidangusaha as $v) {
				$str = '<div class="kbli" id="kbli-'.$v->sbuKbliID.'">';
				$str .= '<input type="hidden" id="siupBidangUsaha['.$v->sbuKbliID.']" name="siupBidangUsaha['.$v->sbuKbliID.']" value="'.$v->sbuTabel.'-'.$v->sbuKbliKode.'">';
				$str .= '<input type="hidden" id="siupBidangUsahaNama['.$v->sbuKbliID.']" name="siupBidangUsahaNama['.$v->sbuKbliID.']" value="'.$v->sbuKbliNama.'">';
				$str .= $v->sbuKbliKode.' - '.$v->sbuKbliNama.' ';
				$str .= '<a href="javascript:removeKlui('."'".$v->sbuTabel."','".$v->sbuKbliID."'".');" title="Hapus Kode"><b>x</b></a>';
				$str .= '</div>';
				
				echo $str;
			}
		}
		//END FIK:
?>
						</div>
					</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
						<input class="box" id="txtBidangUsaha" name="txtBidangUsaha" maxlength="500" size="70" value="" />
					<td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>(<i>Ketikkan kode pada textbox di atas ini atau silahkan telusuri daftar di bawah ini, kemudian klik kode yang diinginkan</i>)</td>
				</tr>
				<tr>
					<td></td><td></td>
					<td>
<?php 
		$arr = array(
			'golonganpokok' => array(),
			'golongan' => array(),
			'subgolongan' => array(),
			'kelompok' => array()
		);
		
		//Query
		$sql = "SELECT *, kblipublikasi.kpubID AS id FROM kblipublikasi WHERE kblipublikasi.kpubPublikasi=1 ORDER BY kpubNama";
		$arr['publikasi'] = $app->queryArrayOfObjects($sql);
		
		//Query
		$sql = "SELECT *, kblikategori.kkatID AS id, kblikategori.kkatKpubID AS parentid FROM kblikategori ORDER BY kkatKode, kkatNama";
		$arr['kategori'] = $app->queryArrayOfObjects($sql, 2);
		
		//Query
		$sql = "SELECT *, kbligolonganpokok.kgpokID AS id, kbligolonganpokok.kgpokKkatID AS parentid FROM kbligolonganpokok ORDER BY kgpokKode, kgpokNama";
		$arr['golonganpokok'] = $app->queryArrayOfObjects($sql, 2);
		
		//Query
		$sql = "SELECT *, kbligolongan.kgolID AS id, kbligolongan.kgolKgpokID AS parentid FROM kbligolongan ORDER BY kgolKode, kgolNama";
		$arr['golongan'] = $app->queryArrayOfObjects($sql, 2);
		
		//Query
		$sql = "SELECT *, kblisubgolongan.ksubID AS id, kblisubgolongan.ksubKgolID AS parentid FROM kblisubgolongan ORDER BY ksubKode, ksubNama";
		$arr['subgolongan'] = $app->queryArrayOfObjects($sql, 2);
		
		//Query
		$sql = "SELECT *, kblikelompok.kkelID AS id, kblikelompok.kkelKsubID AS parentid FROM kblikelompok ORDER BY kkelKode, kkelNama";
		$arr['kelompok'] = $app->queryArrayOfObjects($sql, 2);
?>

<?php
		if (count($arr['publikasi']) > 0) {
?>
						<ul id="browser" class="filetree treeview">
<?php 
			foreach ($arr['publikasi'] as $pub) {
?>			
							<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $pub->kpubNama; ?></span>
								<ul>
<?php 
				if (isset($arr['kategori'][$pub->kpubID])) {
					foreach ($arr['kategori'][$pub->kpubID] as $kat) {
?>
									<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $kat->kkatKode.' - '. $kat->kkatNama; ?></span>
										<ul>
<?php 
						if (isset($arr['golonganpokok'][$kat->kkatID])) {
							foreach ($arr['golonganpokok'][$kat->kkatID] as $gpok) {
?>
											<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><?php echo $gpok->kgpokKode.' - '. $gpok->kgpokNama; ?></span>
												<ul>
<?php 
								if (isset($arr['golongan'][$gpok->kgpokID])) {
									foreach ($arr['golongan'][$gpok->kgpokID] as $gol) {
?>
													<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><a href="javascript:pilihKlui('golongan', <?php echo $gol->kgolID; ?>, '<?php echo $gol->kgolKode; ?>', '<?php echo $gol->kgolNama; ?>')"><?php echo $gol->kgolKode; ?></a> - <?php echo $gol->kgolNama; ?></span>
														<ul>
<?php 
										if (isset($arr['subgolongan'][$gol->kgolID])) {
											foreach ($arr['subgolongan'][$gol->kgolID] as $sub) {
?>
															<li class="closed expandable"><div class="hitarea closed-hitarea expandable-hitarea"></div><span class="folder"><a href="javascript:pilihKlui('subgolongan', <?php echo $sub->ksubID; ?>, '<?php echo $sub->ksubKode; ?>', '<?php echo $sub->ksubNama; ?>')"><?php echo $sub->ksubKode; ?></a> - <?php echo $sub->ksubNama; ?></span>
																<ul>
<?php 
												if (isset($arr['kelompok'][$sub->ksubID])) {
													foreach ($arr['kelompok'][$sub->ksubID] as $kel) {
?>
																	<li><span class="file"><a href="javascript:pilihKlui('kelompok', <?php echo $kel->kkelID; ?>, '<?php echo $kel->kkelKode; ?>', '<?php echo $kel->kkelNama; ?>')"><?php echo $kel->kkelKode; ?></a> - <?php echo $kel->kkelNama; ?></span></li>
<?php
													}
												}
?>
																</ul>
															</li>
<?php
											}
										}
?>
														</ul>
													</li>
<?php
									}
								}
?>										
												</ul>
											</li>
<?php
							}
						}
?>
										</ul>
									</li>
<?php
					}
				}
?>
								</ul>
							</li>
<?php 
			}
?>
						</ul>
<?php 
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Jenis Barang/Jasa Dagangan Utama</td><td>&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="siupJenisBarangJasa" name="siupJenisBarangJasa" cols="80" rows="5"><?php echo $obj->siupJenisBarangJasa; ?></textarea>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="siupKeterangan" name="siupKeterangan" maxlength="255" size="50" value="<?php echo $obj->siupKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->siupNo > 0) {
?>
						<span>137/<?php echo CFG_COMPANY_SHORT_NAME; ?>/SIUP-<?php echo strtoupper(substr($obj->siupJenis,0,1)); ?>/<?php echo substr($obj->siupTglPengesahan,0,4); ?>/</span>
						<input class="box" id="siupNo" name="siupNo" maxlength="5" size="5" value="<?php echo $obj->siupNo; ?>" />
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="siupNo" name="siupNo" value="<?php echo $obj->siupNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="siupTglPengesahan" name="siupTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->siupTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="siupPejID" name="siupPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->siupPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='siup'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->siupTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->siupTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="siupTglBerlaku" name="siupTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->siupTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->siupTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->siupTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="siupTglDaftarUlang" name="siupTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->siupTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/siup/js/edit.siup.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	//VERSI BARU
	static function printSiup($obj) {
		global $app;
		
		$objReport 		= new Report('Surat Izin Usaha Perdagangan', 'SIUP-'.substr($obj->siupTglPengesahan,0,4).'-'.$obj->siupNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->siupPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: left;	/* Bukan justify */
		}
	</style>

	<table border="1" cellpadding="10" cellspacing="0" width="100%">
	<tr>
		<td align="center">
			<span style="font-size:14px;"><b>SURAT IZIN USAHA PERDAGANGAN <?php //echo strtoupper($obj->siupJenis); ?></b></span><br>
			<span style="font-size:12px;">Nomor : <?php echo $obj->siupNoLengkap; ?></span>
		</td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="1" cellpadding="5" cellspacing="0" width="100%">
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">NAMA PERUSAHAAN</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->perNama); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">NAMA PENANGGUNG JAWAB & JABATAN</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top">
					<?php echo strtoupper($obj->mhnNama); ?> (<?php echo strtoupper($obj->siupJabatanPemilik); ?>)
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
<?php 
		if ($obj->siupAlamat == '') {
			$obj->siupAlamat = $obj->perAlamat;
		}
		if ($obj->siupKelurahanID == 0) {
			$obj->siupKelurahanID = $obj->perKelurahanID;
		}

		$sql = "SELECT wilayah2.wilNama AS kecamatan 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->siupKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasiPerusahaan = $app->queryObject($sql);
?>
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">ALAMAT PERUSAHAAN</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->siupAlamat.', Kec. '.$objLokasiPerusahaan->kecamatan); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="70%">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="196" align="left" valign="top">NOMOR TELEPON</td>
        		<td width="10" align="left" valign="top">:</td>
				<td width="124" align="left" valign="top"><?php echo strtoupper($obj->perNoTelp); ?></td>
			</tr>
			</table>
		</td>
		<td width="30%">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="32%" align="left" valign="top">FAX. </td>
        		<td width="6%" align="left" valign="top">:</td>
				<td width="62%" align="left" valign="top"><?php echo strtoupper($obj->perNoFax); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">MODAL DAN KEKAYAAN BERSIH PERUSAHAAN<br><span style="font-size:8px;">(TIDAK TERMASUK TANAH DAN BANGUNAN)</span></td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top">Rp. <?php echo $app->MySQLToMoney($obj->siupModal,0); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">KELEMBAGAAN</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->siupKelembagaan); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">KEGIATAN USAHA (KBLI)</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->siupBidangUsaha); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">BARANG/JASA DAGANGAN UTAMA</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->siupJenisBarangJasa); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			IZIN INI BERLAKU UNTUK MELAKUKAN KEGIATAN USAHA PERDAGANGAN DI SELURUH WILAYAH REPUBLIK INDONESIA SELAMA PERUSAHAAN MASIH MENJALANKAN USAHANYA.
		</td>
	</tr>
	<!-- <tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">PENDAFTARAN ULANG PADA TANGGAL</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($app->MySQLDateToIndonesia($obj->siupTglDaftarUlang)); ?></td>
			</tr>
			</table>
		</td>
	</tr> -->
	</table>
	
	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="48%">
			<table width="100px" height="100px" border="0">
			<tr>
				<td>
					<img src="<?php echo CFG_LIVE_PATH; ?>/images/report/3x4.png" width="93" height="122">
				</td>
			</tr>
			</table>
		</td>
		<td width="52%" align="center">
			<?php //$objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->siupTglPengesahan)); ?>
			<table border="0" cellpadding="1" cellspacing="0" width="100%">
			<tr>
				<td colspan="2" align="center">
					<?php echo strtoupper($objSatuanKerja->wilIbukota.', '.$app->MySQLDateToIndonesia($obj->siupTglPengesahan)); ?>
					<br>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<b>KEPALA DINAS<br>PENANAMAN MODAL DAN<br>PELAYANAN PERIZINAN TERPADU SATU PINTU<br>KABUPATEN PELALAWAN</b><br>
					<br><br><br>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<table width="100%">
					<tr>
						<td align="left" width="50%" nowrap>
							<b><u><?php echo $objPengesahan->pnNama; ?></u></b><br>
							<?php echo $objPengesahan->pgrPangkat; ?><br>
							NIP. <?php echo $objPengesahan->pnNIP; ?>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	//VERSI LAMA
	static function printSiupLama($obj) {
		global $app;
		
		$objReport 		= new Report('Surat Izin Usaha Perdagangan', 'SIUP-'.substr($obj->siupTglPengesahan,0,4).'-'.$obj->siupNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->siupPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: left;	/* Bukan justify */
		}
	</style>

	<table border="1" cellpadding="10" cellspacing="0" width="100%">
	<tr>
		<td align="center">
			<span style="font-size:14px;"><b>SURAT IZIN USAHA PERDAGANGAN <?php echo strtoupper($obj->siupJenis); ?></b></span><br>
			<span style="font-size:12px;"><b>NOMOR : <?php echo $obj->siupNoLengkap; ?></b></span>
		</td>
	</tr>
	</table>
	
	<br><br>
	
	<table border="1" cellpadding="5" cellspacing="0" width="100%">
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">NAMA PERUSAHAAN</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->perNama); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">NAMA PENANGGUNG JAWAB & JABATAN</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top">
					<?php echo strtoupper($obj->mhnNama); ?> (<?php echo strtoupper($obj->siupJabatanPemilik); ?>)
				</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
<?php 
		if ($obj->siupAlamat == '') {
			$obj->siupAlamat = $obj->perAlamat;
		}
		if ($obj->siupKelurahanID == 0) {
			$obj->siupKelurahanID = $obj->perKelurahanID;
		}

		$sql = "SELECT wilayah2.wilNama AS kecamatan 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->siupKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasiPerusahaan = $app->queryObject($sql);
?>
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">ALAMAT PERUSAHAAN</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->siupAlamat.', Kec. '.$objLokasiPerusahaan->kecamatan); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="70%">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="196" align="left" valign="top">NOMOR TELEPON</td>
        		<td width="10" align="left" valign="top">:</td>
				<td width="124" align="left" valign="top"><?php echo strtoupper($obj->perNoTelp); ?></td>
			</tr>
			</table>
		</td>
		<td width="30%">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="32%" align="left" valign="top">FAX. </td>
        		<td width="6%" align="left" valign="top">:</td>
				<td width="62%" align="left" valign="top"><?php echo strtoupper($obj->perNoFax); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">KEKAYAAN BERSIH PERUSAHAAN (TIDAK TERMASUK TANAH DAN BANGUNAN)</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top">Rp. <?php echo $app->MySQLToMoney($obj->siupModal,0); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">KELEMBAGAAN</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->siupKelembagaan); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">KEGIATAN USAHA (KBLI)</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->siupBidangUsaha); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">BARANG/JASA DAGANGAN UTAMA</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($obj->siupJenisBarangJasa); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			IZIN INI BERLAKU UNTUK MELAKUKAN KEGIATAN USAHA PERDAGANGAN DI SELURUH WILAYAH REPUBLIK INDONESIA, SELAMA PERUSAHAAN MASIH MENJALANKAN USAHANYA, DAN WAJIB DIDAFTAR ULANG SETIAP 5 (LIMA) TAHUN SEKALI.
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table border="0" cellspacing="0" cellspacing="0" width="100%">
			<tr>
				<td width="38%" align="left" valign="top">PENDAFTARAN ULANG PADA TANGGAL</td>
        		<td width="2%" align="left" valign="top">:</td>
				<td width="60%" align="left" valign="top"><?php echo strtoupper($app->MySQLDateToIndonesia($obj->siupTglDaftarUlang)); ?></td>
			</tr>
			</table>
		</td>
	</tr>
	</table>
	
	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="48%">
			<table width="100px" height="100px" border="0">
			<tr>
				<td>
					<img src="<?php echo CFG_LIVE_PATH; ?>/images/report/3x4.png" width="93" height="122">
				</td>
			</tr>
			</table>
		</td>
		<td width="52%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->siupTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}

	//VERSI PALING LAMA
	static function printSiupPalingLama($obj) {
		global $app;
		
		$objReport 		= new Report('Surat Izin Usaha Perdagangan', 'SIUP-'.substr($obj->siupTglPengesahan,0,4).'-'.$obj->siupNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->siupPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: left;	/* Bukan justify */
		}
	</style>

	<p align="center">
		<span style="font-size:14px;"><b><u>SURAT IJIN USAHA PERDAGANGAN (SIUP) <?php echo strtoupper($obj->siupJenis); ?></u></b></span><br>
		<span style="font-size:12px;"><b>Nomor : <?php echo $obj->siupNoLengkap; ?></b></span>
	</p>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="44%" align="left" valign="top"><ol start="1"><li>Nama Perusahaan</li></ol></td>
        <td width="2%" align="left" valign="top">:</td>
		<td width="54%" align="left" valign="top"><?php echo strtoupper($obj->perNama); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"><ol start="2"><li>Merek (milik sendiri/lisensi)</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo strtoupper($obj->siupMerekUsaha); ?></td>
	</tr>
<?php 
		$sql = "SELECT wilayah2.wilNama AS kecamatan 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasiPerusahaan = $app->queryObject($sql);
?>
	<tr>
		<td align="left" valign="top"><ol start="3"><li>Alamat Kantor Perusahaan</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo strtoupper($obj->perAlamat.', '.$objLokasiPerusahaan->kecamatan); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"><ol start="4"><li>Nama Pemilik / Penanggung Jawab</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo strtoupper($obj->mhnNama); ?></td>
	</tr>
<?php 
		$sql = "SELECT wilayah2.wilNama AS kecamatan 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasiPemilik = $app->queryObject($sql);
?>
	<tr>
		<td align="left" valign="top"><ol start="5"><li>Alamat Pemilik / Penanggung Jawab</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo strtoupper($obj->mhnAlamat).', '.strtoupper($objLokasiPemilik->kecamatan); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"><ol start="6"><li>Nomor Pokok Wajib Pajak (NPWP)</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo strtoupper($obj->siupNPWP); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"><ol start="7"><li>Nilai Modal dan Kekayaan Bersih Perusahaan seluruhnya tidak termasuk Tanah dan Bangunan Tempat Usaha</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top">Rp. <?php echo $app->MySQLToMoney($obj->siupModal); ?>,-</td>
	</tr>
	<tr>
		<td align="left" valign="top"><ol start="8"><li>Kelembagaan</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo strtoupper($obj->siupKelembagaan); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"><ol start="9"><li>Kegiatan Usaha</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top"><?php echo strtoupper($obj->siupKegiatanUsaha); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"><ol start="10"><li>Bidang Usaha (KBLI)</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top" align="justify"><?php echo strtoupper($obj->siupBidangUsaha); ?></td>
	</tr>
	<tr>
		<td align="left" valign="top"><ol start="11"><li>Jenis Barang / Jasa Dagangan Utama</li></ol></td>
        <td align="left" valign="top">:</td>
		<td align="left" valign="top" align="justify"><?php echo strtoupper($obj->siupJenisBarangJasa); ?></td>
	</tr>
	</table>
	
	<p>Surat Izin Usaha Perdagangan (SIUP) ini diterbitkan dengan ketentuan : </p>
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="20%" align="left" valign="top">PERTAMA</td><td width="2%" align="left" valign="top">:</td>
		<td width="78%" align="left" valign="top" align="justify">
			Surat Izin Usaha Perdagangan (SIUP) ini berlaku untuk melakukan kegiatan usaha perdagangan di seluruh wilayah Republik Indonesia selama Perusahaan masih menjalankan Kegiatan Usaha Perdagangan dan Wajib Melakukan Pendaftaran Ulang sekali dalam 5 (lima) tahun yaitu pada tanggal <?php echo $app->MySQLDateToIndonesia($obj->siupTglDaftarUlang); ?>.
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">KEDUA</td><td align="left" valign="top">:</td>
		<td align="left" valign="top" align="justify">
			Pemilik / Penanggung Jawab wajib menyampaikan laporan kegiatan usaha perdagangannya satu kali dalam setahun selambat-lambatnya 31 Januari tahun berikutnya.
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">KETIGA</td><td align="left" valign="top">:</td>
		<td align="left" valign="top" align="justify">
			Tidak berlaku untuk kegiatan Perdagangan berjangka komoditi.
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">KEEMPAT</td><td align="left" valign="top">:</td>
		<td align="left" valign="top" align="justify">
			Tidak untuk melakukan kegiatan usaha selain yang tercantum dalam Surat Izin Usaha Perdagangan (SIUP) ini.
		</td>
	</tr>
	</table>

	<br><br>

	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="50%">
			<table width="100px" height="100px" border="0">
			<tr>
				<td>
					<img src="<?php echo CFG_LIVE_PATH; ?>/images/report/3x4.png" width="93" height="122">
				</td>
			</tr>
			</table>
		</td>
		<td width="50%" align="center">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->siupTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	static function readSiup($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=siup&task=edit&id=<?php echo $obj->siupDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Surat Izin Usaha Perdagangan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=siup&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jabatan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupJabatanPemilik; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Perusahaan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=perusahaan&task=read&id=<?php echo $obj->perID; ?>&fromact=siup&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->perNama; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->perAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->perKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Merek Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupMerekUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Status Tempat Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupStatusTempatUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">NPWP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupNPWP; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis SIUP</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupJenis; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Modal Perusahaan (Rp)</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLToMoney($obj->siupModal); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kelembagaan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupKelembagaan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kegiatan Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupKegiatanUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bidang Usaha</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupBidangUsaha; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Jenis Barang/Jasa Dagangan Utama</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupJenisBarangJasa; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->siupNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->siupTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->siupPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->siupTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->siupTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->siupTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->siupTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/siup/js/read.siup.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewSiup($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=siup"><img src="images/icons/rosette.png" border="0" /> Buku Register Surat Izin Usaha Perdagangan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat/Nama Perusahaan : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filtertahun" name="filtertahun">
					<option value="0">Seluruhnya</option>
<?php 
		for ($i=2008;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['filter']['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['tahun'] != $arr['default']['tahun']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('siupNo', 'Nomor Surat', true),
			$app->setHeader('siupTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('siupTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('siupTglDaftarUlang', 'Tgl. Daftar Berikutnya', true),
			$app->setHeader('perNama', 'Perusahaan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
				<th>Perusahaan</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=siup&task=read&id='.$v->siupDmhnID.'&fromact=siup" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->siupDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=siup&task=read&id='.$v->siupDmhnID.'&fromact=siup" title="Entri">'.$v->siupNoLengkap.'</a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->siupTglPengesahan).'</td>';
				
				if ($v->siupTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->siupTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->siupTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->siupTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo '<td>'.$v->perNama.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/siup/js/siup.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>