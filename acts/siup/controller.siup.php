<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.siup.php';
require_once 'view.siup.php';

switch ($app->task) {
	case 'add':
		addSiup($app->id);
		break;
	case 'edit':
		editSiup($app->id);
		break;
	case 'delete':
		deleteSiup($app->id);
		break;
	case 'print':
		printSiup($app->id);
		break;
	case 'read':
		readSiup($app->id);
		break;
	case 'save':
		saveSiup();
		break;
	default:
		viewSiup(true, '');
		break;
}

function addSiup($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editSiup($id);
}

function deleteSiup($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM siup WHERE siupDmhnID='".$id."'");
		
	$success = true;
	$msg = 'SIUP berhasil dihapus';
		
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'SIUP', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function editSiup($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objSiup = $app->queryObject("SELECT * FROM siup WHERE siupDmhnID='".$id."'");
	if (!$objSiup) {
		$objSiup = new Siup_Model();
		
		//Ambil nilai default
		$objSiup->siupMerekUsaha = $objDetailPermohonan->perMerekUsaha;
		//$objSiup->siupNPWP = $objDetailPermohonan->perNPWP;
		$objSiup->siupStatusTempatUsaha = $objDetailPermohonan->perStatusTempatUsaha;
	}
	
	if ($objSiup->siupNPWP == "") {
		$objSiup->siupNPWP = $app->queryField("SELECT wprNoPokok FROM wajibpajakretribusi WHERE wprNama='".$objDetailPermohonan->perNama."' AND wprJenis='Pajak'");
	}
	
	$objSiup->siupDmhnID = $objDetailPermohonan->dmhnID;
	
	Siup_View::editSiup(true, '', $objDetailPermohonan, $objSiup);
}

function printSiup($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN siup ON dmhnID=siupDmhnID
			WHERE dmhnID='".$id."'";
	$objSiup = $app->queryObject($sql);
	if (!$objSiup) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objSiup->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	Siup_View::printSiup($objSiup);
}

function readSiup($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN siup ON dmhnID=siupDmhnID
			WHERE dmhnID='".$id."'";
	$objSiup = $app->queryObject($sql);
	if (!$objSiup) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objSiup->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	Siup_View::readSiup($objSiup);
}

function saveSiup() {
	global $app;
	
	//Get object
	$objSiupSebelumnya = $app->queryObject("SELECT * FROM siup WHERE siupID='".$app->getInt('siupID')."'");
	if (!$objSiupSebelumnya) {
		$objSiupSebelumnya = new Siup_Model();
	}
	
	//Create object
	$objSiup = new Siup_Model();
	$app->bind($objSiup);
	
	//Modify object (if necessary)
	$objSiup->siupModal = $app->MoneyToMySQL($objSiup->siupModal);
	$objSiup->siupBidangUsaha = '';
	//$objSiup->siupJenisBarangJasa = '';
	
	$objSiup->siupTglPengesahan = $app->NormalDateToMySQL($objSiup->siupTglPengesahan);
	if ($objSiup->siupTglPengesahan != '0000-00-00') {
		if ($objSiupSebelumnya->siupNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objSiup->siupNo = intval($app->queryField("SELECT MAX(siupNo) AS nomor FROM siup WHERE YEAR(siupTglPengesahan)='".substr($objSiup->siupTglPengesahan,0,4)."' AND siupJenis='".$objSiup->siupJenis."'")) + 1;
		}
	
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objSiup->siupTglBerlaku = date('Y-m-d', strtotime($objSiup->siupTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objSiup->siupTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objSiup->siupTglDaftarUlang = date('Y-m-d', strtotime($objSiup->siupTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objSiup->siupTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objSiup->siupNo = 0;
		
		$objSiup->siupTglBerlaku = '0000-00-00';
		$objSiup->siupTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	$objSiup->siupNoLengkap = '137/'.CFG_COMPANY_SHORT_NAME.'/SIUP-'.strtoupper(substr($objSiup->siupJenis,0,1)).'/'.substr($objSiup->siupTglPengesahan,0,4).'/'.$objSiup->siupNo;
		
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objSiup->siupNo > 0){
		$isExist = $app->isExist("SELECT siupID AS id FROM siup WHERE siupNo='".$objSiup->siupNo."' AND YEAR(siupTglPengesahan)='".substr($objSiup->siupTglPengesahan,0,4)."' AND siupJenis='".$objSiup->siupJenis."'", $objSiup->siupID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objSiup->siupNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objSiup->siupID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objSiup);
		} else {
			$sql = $app->createSQLforUpdate($objSiup);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objSiup->siupID = $app->queryField("SELECT LAST_INSERT_ID() FROM siup");
		}
		
		$app->query("DELETE FROM siupbidangusaha WHERE sbuSiupID='".$objSiup->siupID."'");
		
		$siupBidangUsaha = array();
		//$siupJenisBarangJasa = array();
		
		$sbuKbliID = array();
		//BEGIN FIK:
		if (count($_REQUEST['siupBidangUsaha']) > 0) {
			foreach ($_REQUEST['siupBidangUsaha'] as $key=>$value) {
				$data = explode('-',$value);
				
				$sbuKbliID[] = "('".$objSiup->siupID."', '".$data[0]."', '".$key."', '".$data[1]."', '".$_REQUEST['siupBidangUsahaNama'][$key]."')";
				$siupBidangUsaha[] = $data[1];
				//$siupJenisBarangJasa[] = $_REQUEST['siupBidangUsahaNama'][$key];
			}
		}
		//END FIK:
		if (count($sbuKbliID) > 0) {
			$app->query("INSERT INTO siupbidangusaha (sbuSiupID, sbuTabel, sbuKbliID, sbuKbliKode, sbuKbliNama) VALUES ".implode(', ',$sbuKbliID));
		}
		
		//update siupBidangUsaha dan siupJenisBarangJasa
		//$app->query("UPDATE siup SET siupBidangUsaha='".implode(', ',$siupBidangUsaha)."', siupJenisBarangJasa='".implode(', ',$siupJenisBarangJasa)."' WHERE siupID='".$objSiup->siupID."'");
		//HANYA update siupBidangUsaha
		$app->query("UPDATE siup SET siupBidangUsaha='".implode(', ',$siupBidangUsaha)."' WHERE siupID='".$objSiup->siupID."'");
		
		$success = true;
		$msg = 'Izin Usaha Perdagangan berhasil disimpan';
		
		$app->writeLog(EV_INFORMASI, 'siup', $objSiup->siupID, $objSiup->siupNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objSiup->siupDmhnID."'");
		
		$app->query("UPDATE perusahaan 
					 SET 
					 	 perMerekUsaha='".$objSiup->siupMerekUsaha."', 
					 	 perNPWP='".$objSiup->siupNPWP."', 
					 	 perStatusTempatUsaha='".$objSiup->siupStatusTempatUsaha."' 
					 WHERE perID='".$app->getInt('perID')."'");
	} else {
		$success = false;
		$msg = 'Izin Usaha Perdagangan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objSiup->siupDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		Siup_View::editSiup($success, $msg, $objDetailPermohonan, $objSiup);
	}
}

function viewSiup($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('siup', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('siup', 'sort', 'siupTglPengesahan');
	//$dir		= $app->pageVar('siup', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'tahun' => 0
	);
	
	$nama 		= $app->pageVar('siup', 'filternama', $default['nama'], 'strval');
	$tahun 		= $app->pageVar('siup', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "(siupNo LIKE '".$nama."%' OR perNama LIKE '%".$nama."%')";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(siupTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM siup
				  LEFT JOIN detailpermohonan ON siupDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM siup
			LEFT JOIN detailpermohonan ON siupDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(siupTglPengesahan) DESC, siupNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Siup_View::viewSiup($success, $msg, $arr);
}
?>