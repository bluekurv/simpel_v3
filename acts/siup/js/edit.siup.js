//Objects
$("#browser").treeview();

$("#siupModal").maskMoney({ thousands:'.', decimal:',', precision:0, defaultZero:true, allowZero:true });

//Variables
var form = $("#myForm");

//Functions
//BEGIN FIK:
function pilihKlui(tabel, id, kode, nama) {
	var str = '<div class="kbli" id="kbli-' + id + '">';
	str += '<input type="hidden" id="siupBidangUsaha[' + id + ']" name="siupBidangUsaha[' + id + ']" value="' + tabel + '-' + kode + '">';
	str += '<input type="hidden" id="siupBidangUsahaNama[' + id + ']" name="siupBidangUsahaNama[' + id + ']" value="' + nama + '">';
	str += kode + ' - ' + nama + ' ';
	str += '<a href="javascript:removeKlui(\'' + tabel + '\', \'' + id + '\');" title="Hapus Kode"><b>x</b></a>';
	str += '</div>';
	
	$("#bidangusaha").append(str);
}

function removeKlui(tabel, id) {
	$('#kbli-' + id).remove();
}
//END FIK:

function validateForm() {
	var doSubmit = true;
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
$(window).scroll(function() {
	if ($(this).scrollTop() < 125) {
		$("#toolbarEntri2").hide();
	} else {
		$("#toolbarEntri2").show();
	}
});

$('#txtBidangUsaha').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kbli2',
	onSelect: function(suggestion){
		pilihKlui(suggestion.data.tabel, suggestion.data.id, suggestion.data.kode, suggestion.data.nama);
	}
});

$('#siupKelurahan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kelurahan',
	onSelect: function(suggestion){
		$('#siupKelurahanID').val(suggestion.data.id);
		$('#siupKelurahanKode').val(suggestion.data.kode);
		$('#siupKelurahan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});

form.submit(submitForm);

//Set focus
$("#siupMerekUsaha").focus();