<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Status_View {
	static function viewStatus($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=penyerahan"><img src="images/icons/rosette.png" border="0" /> Daftar Penyerahan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				<select class="box" id="filterfield" name="filterfield">
<?php 
		foreach (array('mhnNoUrutLengkap'=>'No. Pendaftaran', 'mhnNoKtp'=>'No. KTP', 'mhnNama'=>'Nama Pemohon', 'perNama'=>'Nama Perusahaan') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['filter']['field']) {
				echo 'selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<select class="box" id="filterstatus" name="filterstatus">
<?php 
		foreach (array('Belum Selesai', 'Belum Diserahkan', 'Sudah Diserahkan') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $arr['filter']['status']) {
				echo 'selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['status'] != $arr['default']['status']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 60),
			$app->setHeader('mhnTgl', 'Tanggal', true, 60),
			$app->setHeader('mhnNoUrutLengkap', 'Nomor<br>Pendaftaran', true),
			$app->setHeader('mhnNoKtp', 'Nomor KTP', true),
			$app->setHeader('', '', true),
			$app->setHeader('mhnNama', 'Pemohon /<br>Perlu Penyerahan?', true),
			$app->setHeader('perNama', 'Perusahaan /<br>Tgl. Perkiraan Selesai', true),
			$app->setHeader('dibuatoleh', 'Didaftarkan Oleh /<br>Diadministrasikan Oleh', true),
			$app->setHeader('mhnAdministrasiStatus', 'Status Penyerahan', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo '<tr>';
				echo '<td><a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'&fromact=penyerahan" title="Lihat Permohonan"><img src="images/icons/zoom.png" border="0" /></a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->mhnTgl).'</td>';
				echo '<td><a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'&fromact=penyerahan" title="Lihat Permohonan">'.$v->mhnNoUrutLengkap.'</a></td>';
				echo '<td>'.$v->mhnNoKtp.'</td>';
				echo '<td></td>';
				echo '<td>'.$v->mhnNama.'</td>';
				echo '<td>'.$v->perNama.'</td>';
				echo '<td>'.$v->dibuatoleh.'</td>';
				echo '<td></td>';
				echo "</tr>\n";
				
				if (count($arr['subdata'][$v->mhnID]) > 0) {
					foreach ($arr['subdata'][$v->mhnID] as $v2) {
						echo '<tr class="odd">';
						echo '<td><a href="index2.php?act=detailpermohonan&task=edit&id='.$v2->dmhnID.'&fromact=penyerahan" title="Entri Detail Permohonan (Alur Penyelesaian)"><img src="images/icons/arrow_right.png" border="0" /></a></td>';
						echo '<td></td>';
						
						/*switch ($v2->dmhnTipe) {
							case 'Baru':
								echo '<td><span class="badge open">Baru</span></td>';
								break;
							case 'Perubahan':
								echo '<td><span class="badge info">Perubahan</span></td>';
								break;
							case 'Perpanjangan':
								echo '<td><span class="badge close">Perpanjangan</span></td>';
								break;
							default:
								echo '<td><span class="badge close">?</span></td>';
						}*/
						echo '<td>'.$v2->dmhnTipe.'</td>';
						echo '<td>'.$v2->jsiNama.($v2->jsiSubNama != '' ? ' ('.$v2->jsiSubNama.')' : '').'</td>';
						
						switch ($v2->jsiKode) {
							case 'reklame':
								$fieldPart = 'reklamePemasangan';
								break;
							default:
								$fieldPart = $v2->jsiKode;
								break;
						}
						$exclude = array('airtanah', 'airpermukaan');
						if (!in_array($v2->jsiKode, $exclude)) {
							$lokasi = $app->queryField("SELECT wilNama FROM wilayah, ".$v2->jsiKode." WHERE ".$v2->jsiKode."DmhnID='".$v2->dmhnID."' AND ".$fieldPart."KelurahanID=wilID");
							echo '<td>'.$lokasi.'</td>';
						} else {
							echo '<td></td>';
						}
						if ($v2->dmhnPola > 0) {
							if ($v2->dmhnPerluSurvey) {
								echo '<td>Ada Penyerahan</td>';
							} else {
								echo '<td>Tanpa Penyerahan</td>';
							}
							if ($v2->dmhnTglTargetSelesai != '0000-00-00') {
								echo '<td>'.$app->MySQLDateToNormal($v2->dmhnTglTargetSelesai).'</td>';
							} else {
								echo '<td><span class="badge tidak">Belum Ditentukan</span></td>';
							}
						} else {
							echo '<td><span class="badge tidak">Belum Ditentukan</span></td>';
							echo '<td><span class="badge tidak">Belum Ditentukan</span></td>';
						}
						echo '<td>'.$v2->diadministrasikanoleh.'</td>';
						
						//NOTE: lebih sederhana karena mengacu kepada query
						$badge = '';
						if ($v2->dmhnTelahSelesai == 0) {
							$badge = '<span class="badge tidak">Belum Selesai</span>';
						} else {
							if ($v2->dmhnTelahAmbil == 0) { 
								$badge = '<span class="badge info">Belum Diambil</span>';
							} else {
								$badge = '<span class="badge ya">Sudah Diambil</span>';
							}
						}
						echo '<td>'.$badge.'</td>';
						echo "</tr>\n";
					}
				}
				
				$i++;
			}
		} else {
?>
			<tr><td colspan="9">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/penyerahan/js/penyerahan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>