//Variables
var form = $("#myForm");
var wilParentID = $("#wilParentID");
var wilKode = $("#wilKode");
var wilKodeRetribusi = $("#wilKodeRetribusi");
var wilNama = $("#wilNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'wilParentID' || this.id === undefined) {
		if (wilParentID.val() == 0){
			doSubmit = false;
			wilParentID.addClass("error");		
		} else {
			wilParentID.removeClass("error");
		}
	}
	
	if (this.id == 'wilKode' || this.id === undefined) {
		if (wilKode.val().length == 0){
			doSubmit = false;
			wilKode.addClass("error");		
		} else {
			wilKode.removeClass("error");
		}
	}
	
	if (this.id == 'wilKodeRetribusi' || this.id === undefined) {
		if (wilKodeRetribusi.val().length == 0){
			doSubmit = false;
			wilKodeRetribusi.addClass("error");		
		} else {
			wilKodeRetribusi.removeClass("error");
		}
	}
	
	if (this.id == 'wilNama' || this.id === undefined) {
		if (wilNama.val().length == 0){
			doSubmit = false;
			wilNama.addClass("error");		
		} else {
			wilNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
wilKode.blur(validateForm);
wilKodeRetribusi.blur(validateForm);
wilNama.blur(validateForm);

wilKode.keyup(validateForm);
wilKodeRetribusi.keyup(validateForm);
wilNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
wilParentID.focus();