<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Loket Pelayanan']['all'] = true;
$acl['Petugas Administrasi']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.kelurahan.php';
require_once 'view.kelurahan.php';

switch ($app->task) {
	case 'add2':
		add2Kelurahan();
		break;
	case 'add':
	case 'edit':
		editKelurahan($app->id);
		break;
	case 'delete':
		deleteKelurahan($app->id);
		break;
	case 'save':
		saveKelurahan();
		break;
	case 'save2':
		save2Kelurahan();
		break;
	default:
		viewKelurahan(true, '');
		break;
}

function deleteKelurahan($id) {
	global $app;
	
	//Get object
	$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$id."' AND (wilTingkat='Kelurahan' OR wilTingkat='Desa')");
	if (!$objKelurahan) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalPermohonan = intval($app->queryField("SELECT COUNT(*) AS total FROM permohonan WHERE mhnKelurahanID='".$id."'"));
	if ($totalPermohonan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalPermohonan.' permohonan pada kelurahan/desa tersebut';
	}
	
	$totalPerusahaan = intval($app->queryField("SELECT COUNT(*) AS total FROM perusahaan WHERE perKelurahanID='".$id."'"));
	if ($totalPerusahaan > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalPerusahaan.' perusahaan pada kelurahan/desa tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM wilayah WHERE wilID='".$id."'");
		
		$success = true;
		$msg = $objKelurahan->wilTingkat.' "'.$objKelurahan->wilNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'wilayah', $objKelurahan->wilID, $objKelurahan->wilNama, $msg);
	} else {
		$success = false;
		$msg = $objKelurahan->wilTingkat.' "'.$objKelurahan->wilNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewKelurahan($success, $msg);
}

function editKelurahan($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objKelurahan = $app->queryObject("SELECT * FROM wilayah WHERE wilID='".$id."' AND (wilTingkat='Kelurahan' OR wilTingkat='Desa')");
		if (!$objKelurahan) {
			$app->showPageError();
			exit();
		}
	} else {
		$objKelurahan = new Kelurahan_Model();
	}
	
	Kelurahan_View::editKelurahan(true, '', $objKelurahan);
}

function add2Kelurahan() {
	global $app;
	
	$parent 	= $app->pageVar('kelurahan', 'filterparent', 0, 'intval');
	
	Kelurahan_View::add2Kelurahan($parent);
}

function saveKelurahan() {
	global $app;
	
	//Create object
	$objKelurahan = new Kelurahan_Model();
	$app->bind($objKelurahan);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objKelurahan->wilKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objKelurahan->wilKodeRetribusi == '') {
		$doSave = false;
		$msg[] = '- Kode (pada SIMRetribusi) belum diisi';
	}
	
	if ($objKelurahan->wilNama == '') {
		$doSave = false;
		$msg[] = '- Nama Kelurahan belum diisi';
	}
	
	if ($objKelurahan->wilParentID == 0) {
		$doSave = false;
		$msg[] = '- Berada di bawah belum diisi';
	}
	
	if ($objKelurahan->wilKode != '00') {
		$isExist = $app->isExist("SELECT wilID AS id FROM wilayah WHERE wilKode='".$objKelurahan->wilKode."' AND wilTingkat='Kecamatan' AND wilParentID='".$objKelurahan->wilParentID."'", $objKelurahan->wilID);
		if (!$isExist) {
			$doSave = false;
			$msg[]  = '- Kode "'.$objKelurahan->wilKode.'" sudah ada';
		}
	}
	
	$isExist = $app->isExist("SELECT wilID AS id FROM wilayah WHERE wilNama='".$objKelurahan->wilNama."' AND (wilTingkat='Kelurahan' OR wilTingkat='Desa') AND wilParentID='".$objKelurahan->wilParentID."'", $objKelurahan->wilID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objKelurahan->wilNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objKelurahan->wilID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objKelurahan);
		} else {
			$sql = $app->createSQLforUpdate($objKelurahan);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objKelurahan->wilID = $app->queryField("SELECT LAST_INSERT_ID() FROM wilayah");
			
			$success = true;
			$msg = $objKelurahan->wilTingkat.' "'.$objKelurahan->wilNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'wilayah', $objKelurahan->wilID, $objKelurahan->wilNama, $msg);
		} else {
			$success = true;
			$msg = $objKelurahan->wilTingkat.' "'.$objKelurahan->wilNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'wilayah', $objKelurahan->wilID, $objKelurahan->wilNama, $msg);
		}
	} else {
		$success = false;
		$msg = $objKelurahan->wilTingkat.' "'.$objKelurahan->wilNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewKelurahan($success, $msg);
	} else {
		Kelurahan_View::editKelurahan($success, $msg, $objKelurahan);
	}
}

function save2Kelurahan() {
	global $app;
	
	$parentid = $app->getInt('wilParentID');
	$nama = $app->getStr('wilNama');
	$nama = str_replace('- Kelurahan/Desa ', '', $nama);
	
	$arr = explode('\r\n', $nama);
	
	if (count($arr) > 0) {
		$i = 1;
		foreach ($arr as $v) {
			$sql = "INSERT INTO wilayah (wilParentID, wilKode, wilTingkat, wilNama) 
					VALUES (".$parentid.", '".sprintf('%02d', $i)."', 'Kelurahan', '".$v."')";
			$app->query($sql);
			$i++;
		}
	}
	
	viewKelurahan(true, count($arr)." Kelurahan telah ditambahkan");
}

function viewKelurahan($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('kelurahan', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('kelurahan', 'sort', 'kode');
	$dir   		= $app->pageVar('kelurahan', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'parent' => 0
	);
	
	$nama 		= $app->pageVar('kelurahan', 'filternama', $default['nama'], 'strval');
	$parent 	= $app->pageVar('kelurahan', 'filterparent', $default['parent'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "wilayah.wilNama LIKE '%".$nama."%'";
	}
	if ($parent > $default['parent']) {
		$filter[] = "wilayah.wilParentID='".$parent."'";
	}
	$filter[] = "(wilayah.wilTingkat='Kelurahan' OR wilayah.wilTingkat='Desa')";
	$filter[] = "wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'parent' => $parent
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT wilayah.*, CONCAT(wilayah4.wilKode,'.',wilayah3.wilKode,'.',wilayah2.wilKode,'.',wilayah.wilKode) AS kode, CONCAT(wilayah2.wilTingkat,' ',wilayah2.wilNama) AS parent
			FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4
			".$where." 
			ORDER BY ".$sort." ".$dir.", wilayah.wilNama 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Kelurahan_View::viewKelurahan($success, $msg, $arr);
}
?>