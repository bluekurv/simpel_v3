<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Kelurahan_View {
	static function add2Kelurahan($parent) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kelurahan&task=add2"><img src="images/icons/flag_green.png" border="0" /> Tambah Kelurahan/Desa</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=kelurahan&task=save2" method="POST" >
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=kelurahan">Batal</a>
				</div>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Berada di</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		$provinsi = $app->queryArrayOfObjects("SELECT wilayah.wilID AS id, CONCAT(wilayah2.wilKode,'.',wilayah.wilKode) AS kode, wilayah.* FROM wilayah, wilayah AS wilayah2 WHERE (wilayah.wilTingkat='Kabupaten' OR wilayah.wilTingkat='Kota') AND wilayah.wilParentID=wilayah2.wilID ORDER BY kode");
		$kabupaten = $app->queryArrayOfObjects("SELECT wilID AS id, wilParentID AS parentid, wilayah.* FROM wilayah WHERE wilTingkat='Kecamatan' ORDER BY wilKode", 2);
?>
						<select class="box" id="wilParentID" name="wilParentID">
<?php
		if (count($provinsi) > 0) {
			foreach ($provinsi as $v) {
				echo '<optgroup label="'.$v->kode.' - '.$v->wilTingkat.' '.$v->wilNama.'">';
				
				if (isset($kabupaten[$v->wilID])) {
					if (count($kabupaten[$v->wilID]) > 0) {
						foreach ($kabupaten[$v->wilID] as $v2) {
							echo '<option value="'.$v2->wilID.'"';
							if ($v2->wilID == $parent) {
								echo ' selected';
							}
							echo '>'.$v->kode.'.'.$v2->wilKode.' - '.$v2->wilTingkat.' '.$v2->wilNama.'</option>';
						}
					}
				}
				
				echo '</optgroup>';
			}
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td valign="top">Nama</td><td valign="top">&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="wilNama" name="wilNama" cols="80" rows="5"></textarea>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php
	}
	
	static function editKelurahan($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kelurahan&task=edit&id=<?php echo $obj->wilID; ?>"><img src="images/icons/flag_green.png" border="0" /> <?php echo ($obj->wilID > 0) ? 'Ubah' : 'Tambah'; ?> Kelurahan/Desa</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=kelurahan&task=save" method="POST" >
				<input type="hidden" id="wilID" name="wilID" value="<?php echo $obj->wilID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=kelurahan">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Berada di</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		$provinsi = $app->queryArrayOfObjects("SELECT wilayah.wilID AS id, CONCAT(wilayah2.wilKode,'.',wilayah.wilKode) AS kode, wilayah.* FROM wilayah, wilayah AS wilayah2 WHERE (wilayah.wilTingkat='Kabupaten' OR wilayah.wilTingkat='Kota') AND wilayah.wilParentID=wilayah2.wilID ORDER BY kode");
		$kabupaten = $app->queryArrayOfObjects("SELECT wilID AS id, wilParentID AS parentid, wilayah.* FROM wilayah WHERE wilTingkat='Kecamatan' ORDER BY wilKode", 2);
?>
						<select class="box" id="wilParentID" name="wilParentID">
<?php
		if (count($provinsi) > 0) {
			foreach ($provinsi as $v) {
				echo '<optgroup label="'.$v->kode.' - '.$v->wilTingkat.' '.$v->wilNama.'">';
				
				if (isset($kabupaten[$v->wilID])) {
					if (count($kabupaten[$v->wilID]) > 0) {
						foreach ($kabupaten[$v->wilID] as $v2) {
							echo '<option value="'.$v2->wilID.'"';
							if ($v2->wilID == $obj->wilParentID) {
								echo ' selected';
							}
							echo '>'.$v->kode.'.'.$v2->wilKode.' - '.$v2->wilTingkat.' '.$v2->wilNama.'</option>';
						}
					}
				}
				
				echo '</optgroup>';
			}
		}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td>Kode <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="wilKode" name="wilKode" maxlength="5" size="5" value="<?php echo $obj->wilKode; ?>" />
						(<i>Jika belum diketahui, masukkan <b>00</b></i>)
					</td>
				</tr>
				<tr>
					<td>Kode (pada SIMRetribusi) <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="wilKodeRetribusi" name="wilKodeRetribusi" maxlength="5" size="5" value="<?php echo $obj->wilKodeRetribusi; ?>" />
						(<i>Jika belum diketahui, masukkan <b>00</b></i>)
					</td>
				</tr>
				<tr>
					<td>Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<select class="box" id="wilTingkat" name="wilTingkat">
<?php
		foreach (array('Kelurahan', 'Desa') as $v) {
			echo '<option';
			if ($v == $obj->wilTingkat) {
				echo ' selected';
			}
			echo ">".$v."</option>\n";
		}
?>
						</select>
						<input class="box" id="wilNama" name="wilNama" maxlength="255" size="50" value="<?php echo $obj->wilNama; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kelurahan/js/edit.kelurahan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewKelurahan($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=kelurahan"><img src="images/icons/flag_green.png" border="0" /> Kelurahan/Desa</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=kelurahan&task=add">Tambah</a>
				<!-- <a class="button-link inline dark-blue" id="btnTambah2" href="index2.php?act=kelurahan&task=add2">Tambah 2</a> -->
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
<?php
		$provinsi = $app->queryArrayOfObjects("SELECT wilID AS id, wilayah.* FROM wilayah WHERE (wilTingkat='Kabupaten' OR wilTingkat='Kota') ORDER BY wilTingkat, wilNama");
		$kabupaten = $app->queryArrayOfObjects("SELECT wilID AS id, wilParentID AS parentid, wilayah.* FROM wilayah WHERE wilTingkat='Kecamatan' ORDER BY wilTingkat, wilNama", 2);
?>
				<select class="box" id="filterparent" name="filterparent">
					<option value="0">--Seluruh Kecamatan--</option>
<?php
		if (count($provinsi) > 0) {
			foreach ($provinsi as $v) {
				echo '<optgroup label="'.$v->wilTingkat.' '.$v->wilNama.'">';
				
				if (isset($kabupaten[$v->wilID])) {
					if (count($kabupaten[$v->wilID]) > 0) {
						foreach ($kabupaten[$v->wilID] as $v2) {
							echo '<option value="'.$v2->wilID.'"';
							if ($v2->wilID == $arr['filter']['parent']) {
								echo ' selected';
							}
							echo '>'.$v2->wilTingkat.' '.$v2->wilNama.'</option>';
						}
					}
				}
				
				echo '</optgroup>';
			}
		}
?>
				</select>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['parent'] != $arr['default']['parent']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('kode', 'Kode', true, 40),
			$app->setHeader('wilKodeRetribusi', 'Kode (pada SIMRetribusi)', true, 40),
			$app->setHeader('wilTingkat', 'Tingkat', true, 60),
			$app->setHeader('wilNama', 'Nama', true),
			$app->setHeader('parent', 'Berada di', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=kelurahan&task=edit&id='.$v->wilID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->wilID.', '."'".$v->wilTingkat." ".$v->wilNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=kelurahan&task=edit&id='.$v->wilID.'">'.$v->kode.'</a></td>';
				echo '<td>'.$v->wilKodeRetribusi.'</td>';
				echo '<td>'.$v->wilTingkat.'</td>';
				echo '<td>'.$v->wilNama.'</td>';
				echo '<td>'.$v->parent.'</td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/kelurahan/js/kelurahan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>