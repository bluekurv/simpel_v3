<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

//Dengan pengecualian
if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.mahasiswa.php';
require_once 'view.mahasiswa.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editMahasiswa($app->id);
		break;
	case 'delete':
		deleteMahasiswa($app->id);
		break;
	case 'save':
		saveMahasiswa();
		break;
	default:
		viewMahasiswa(true, '');
		break;
}

function deleteMahasiswa($id) {
	global $app;
	
	//Get object
	$objMahasiswa = $app->queryObject("SELECT * FROM mahasiswa WHERE mhsID='".$id."'");
	if (!$objMahasiswa) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM mahasiswa WHERE mhsID='".$id."'");
		
		$success = true;
		$msg = 'Mahasiswa "'.$objMahasiswa->mhsNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'mahasiswa', $_SESSION['sesipengguna']->ID, $_SESSION['sesipengguna']->namaPengguna, $msg);
	} else {
		$success = false;
		$msg = 'Mahasiswa "'.$objMahasiswa->mhsNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewMahasiswa($success, $msg);
}

function editPasswordMahasiswa($msg) {
	Mahasiswa_View::editPasswordMahasiswa(true, $msg);
}

function editMahasiswa($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objMahasiswa = $app->queryObject("SELECT * FROM mahasiswa WHERE mhsID='".$id."'");
		if (!$objMahasiswa) {
			$app->showPageError();
			exit();
		}
	} else {
		$objMahasiswa = new Mahasiswa_Model();
	}
	
	Mahasiswa_View::editMahasiswa(true, '', $objMahasiswa);
}

function loginMahasiswa() {
	global $app;
	
	//Get variable(s)
	$username = isset($_POST['username']) ? trim($_POST['username']) : '';
	$password = isset($_POST['password']) ? trim($_POST['password']) : '';
	
	if ($username != '' && $password != '') {
		//Query
		$sql = "SELECT * 
				FROM mahasiswa 
				LEFT JOIN loket ON pnLokID=lokID
				WHERE mhsNama='".mysql_real_escape_string($username, $app->connection)."' AND pnPassword='".md5(mysql_real_escape_string($password, $app->connection))."' AND pnBolehAkses=1";
		$objMahasiswa = $app->queryObject($sql);
		
		if ($objMahasiswa) {
			$_SESSION['sesimahasiswa'] = new UserSession();
			$_SESSION['sesimahasiswa']->ID = $objMahasiswa->mhsID;
			$_SESSION['sesimahasiswa']->namaMahasiswa = $objMahasiswa->mhsNama;
			$_SESSION['sesimahasiswa']->nama = $objMahasiswa->pnNama;
			$_SESSION['sesimahasiswa']->levelAkses = $objMahasiswa->pnLevelAkses;
			$_SESSION['sesimahasiswa']->loginSebelumnya = $objMahasiswa->pnLoginPada;
			$_SESSION['sesimahasiswa']->loginSaatIni = date('Y-m-d H:i:s');
			$_SESSION['sesimahasiswa']->loketID = $objMahasiswa->pnLokID;
			$_SESSION['sesimahasiswa']->loketNama = $objMahasiswa->lokNama;
			$_SESSION['sesimahasiswa']->aksesLaporan = $objMahasiswa->pnAksesLaporan;
			
			if ($objMahasiswa->pnOrganisasi == 'satuankerja') {
				$objOrganisasi = $app->queryObject("SELECT skrID AS id, skrNama AS nama FROM satuankerja WHERE skrID='".$objMahasiswa->pnOrganisasiID."'");
			} else if ($objMahasiswa->pnOrganisasi == 'bidang') {
				$objOrganisasi = $app->queryObject("SELECT bidID AS id, bidNama AS nama FROM bidang WHERE bidID='".$objMahasiswa->pnOrganisasiID."'");
			} else if ($objMahasiswa->pnOrganisasi == 'bidang') {
				$objOrganisasi = $app->queryObject("SELECT subID AS id, subNama AS nama FROM subbidang WHERE subID='".$objMahasiswa->pnOrganisasiID."'");
			} else {
				$objOrganisasi = new stdClass();
				$objOrganisasi->id = 0;
				$objOrganisasi->nama = '';
			}
			
			$_SESSION['sesimahasiswa']->bidangID = $objOrganisasi->id;
			$_SESSION['sesimahasiswa']->bidangNama = $objOrganisasi->nama;
			
			if ($password == 'password') {
				$_SESSION['sesimahasiswa']->defaultPassword = true;
			} else {
				$_SESSION['sesimahasiswa']->defaultPassword = false;
			}
			
			$app->query("UPDATE mahasiswa SET pnLoginPada='".$_SESSION['sesimahasiswa']->loginSaatIni."' WHERE mhsID='".$_SESSION['sesimahasiswa']->ID."'");
			
			//session_write_close();
			
			$app->writeLog(EV_INFORMASI, 'mahasiswa', $objMahasiswa->mhsID, $username, 'Mahasiswa "'.$username.'" berhasil login');

			header('Location:index2.php');
		} else {
			$_SESSION['sesimahasiswa'] = new UserSession();
			
			//session_write_close();
			
			$app->writeLog(EV_KESALAHAN, 'mahasiswa', 0, $username, 'Mahasiswa "'.$username.'" tidak berhasil login');
			
			header('Location:index.php?login=0');
		}
	} else {
		header('Location:index.php?login=0');
	}
}

function logoutMahasiswa() {
	global $app;
	
	$app->writeLog(EV_INFORMASI, 'mahasiswa', $_SESSION['sesimahasiswa']->ID, $_SESSION['sesimahasiswa']->namaMahasiswa, 'Mahasiswa "'.$_SESSION['sesimahasiswa']->namaMahasiswa.'" berhasil logout');
	
	$_SESSION = array();
	$_SESSION['sesimahasiswa'] = new UserSession();
	
	header('Location:index.php');
}

function saveMahasiswa() {
	global $app;
	
	//Create object
	$objMahasiswa = new Mahasiswa_Model();
	$app->bind($objMahasiswa);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objMahasiswa->mhsNama == '') {
		$doSave = false;
		$msg[] = '- Nama Mahasiswa belum diisi';
	}
	
	$isExist = $app->isExist("SELECT mhsID AS id FROM mahasiswa WHERE mhsNama='".$objMahasiswa->mhsNama."'", $objMahasiswa->mhsID);
	if (!$isExist) {
		$doSave = false;
		$msg[]  = '- Nama Mahasiswa "'.$objMahasiswa->mhsNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objMahasiswa->mhsID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objMahasiswa);
		} else {
			$sql = $app->createSQLforUpdate($objMahasiswa);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objMahasiswa->mhsID = $app->queryField("SELECT LAST_INSERT_ID() FROM mahasiswa");
			
			$success = true;
			$msg = 'Mahasiswa "'.$objMahasiswa->mhsNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'mahasiswa', $objMahasiswa->mhsID, $objMahasiswa->mhsNama, $msg);
		} else {
			$success = true;
			$msg = 'Mahasiswa "'.$objMahasiswa->mhsNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'mahasiswa', $objMahasiswa->mhsID, $objMahasiswa->mhsNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Mahasiswa "'.$objMahasiswa->mhsNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewMahasiswa($success, $msg);
	} else {
		Mahasiswa_View::editMahasiswa($success, $msg, $objMahasiswa);
	}
}

function savePasswordMahasiswa() {
	global $app;
	
	$objMahasiswa = $app->queryObject("SELECT * FROM mahasiswa WHERE mhsNama='".$_SESSION['sesimahasiswa']->namaMahasiswa."' AND pnPassword='".md5(mysql_real_escape_string($_REQUEST['passwordlama']))."'");
	
	if ($objMahasiswa) {
		$app->query("UPDATE mahasiswa SET pnPassword='".md5(mysql_real_escape_string($_REQUEST['passwordbaru']))."' WHERE mhsNama='".$_SESSION['sesimahasiswa']->namaMahasiswa."'");
	
		if ($_REQUEST['passwordbaru'] == 'password') {
			$_SESSION['sesimahasiswa']->defaultPassword = true;
		} else {
			$_SESSION['sesimahasiswa']->defaultPassword = false;
		}
		
		$success = true;
		$msg = 'Kata Kunci berhasil diubah';
		
		$app->writeLog(EV_INFORMASI, 'mahasiswa', $_SESSION['sesimahasiswa']->ID, $_SESSION['sesimahasiswa']->namaMahasiswa, 'Kata Kunci untuk Mahasiswa "'.$_SESSION['sesimahasiswa']->namaMahasiswa.'" berhasil diubah');
	} else {
		$success = false;
		$msg = 'Kata Kunci tidak berhasil diubah';
	}
	
	if ($success) {
		//Handling menu
		header("Location:index2.php?act=mahasiswa&task=editpassword&msg=".$msg);
	} else {
		Mahasiswa_View::editPasswordMahasiswa($success, $msg);
	}
}

function viewMahasiswa($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('mahasiswa', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('mahasiswa', 'sort', 'mhsNama');
	$dir   		= $app->pageVar('mahasiswa', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('mahasiswa', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "mhsNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM mahasiswa
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM mahasiswa
			".$where." 
			ORDER BY ".$sort." ".$dir."
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Mahasiswa_View::viewMahasiswa($success, $msg, $arr);
}
?>