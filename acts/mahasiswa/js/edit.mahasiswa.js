//Variables
var form = $("#myForm");
var mhsNama = $("#mhsNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'mhsNama' || this.id === undefined) {
		if (mhsNama.val().length == 0){
			doSubmit = false;
			mhsNama.addClass("error");		
		} else {
			mhsNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
mhsNama.blur(validateForm);

mhsNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
mhsNama.focus();