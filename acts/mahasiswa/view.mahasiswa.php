<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Mahasiswa_View {
	static function editPasswordMahasiswa($success, $msg) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=mahasiswa&task=editpassword"><img src="images/icons/key.png" border="0" /> Ubah Kata Kunci</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=mahasiswa&task=savepassword" method="POST">
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
				</div>
<?php
		$app->showMessage($success, $msg);

		if ($_SESSION['sesimahasiswa']->defaultPassword) {
?>
				<p><i>Petunjuk: <span style="color:#FF0000;">Anda masih menggunakan kata kunci default "<b>password</b>", harap diubah terlebih dahulu</span>. Isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
<?php 
		} else {
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
<?php 
		}
?>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Kata Kunci Lama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" type="password" id="passwordlama" name="passwordlama" maxlength="15" size="20" /></td>
				</tr>
				<tr>
					<td>Kata Kunci Baru <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" type="password" id="passwordbaru" name="passwordbaru" maxlength="15" size="20" /></td>
				</tr>
				<tr>
					<td>Ulangi Kata Kunci Baru <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" type="password" id="passwordbaru2" name="passwordbaru2" maxlength="15" size="20" /></td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/mahasiswa/js/editpassword.mahasiswa.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function editMahasiswa($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=mahasiswa&task=edit&id=<?php echo $obj->mhsID; ?>"><img src="images/icons/user.png" border="0" /> <?php echo ($obj->mhsID > 0) ? 'Ubah' : 'Tambah'; ?> Mahasiswa</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=mahasiswa&task=save" method="POST" >
				<input type="hidden" id="mhsID" name="mhsID" value="<?php echo $obj->mhsID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=mahasiswa">Batal</a>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="200">Nama Mahasiswa <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td><input class="box" id="mhsNama" name="mhsNama" maxlength="15" size="20" value="<?php echo $obj->mhsNama; ?>" /></td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/mahasiswa/js/edit.mahasiswa.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewMahasiswa($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=mahasiswa"><img src="images/icons/user.png" border="0" /> Mahasiswa</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue" id="btnTambah" href="index2.php?act=mahasiswa&task=add">Tambah</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nama Mahasiswa : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('pnNama', 'Nama', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td><a href="index2.php?act=mahasiswa&task=edit&id='.$v->mhsID.'" title="Ubah"><img src="images/icons/pencil.png" border="0" /></a> <a href="javascript:hapus('.$v->mhsID.', '."'".$v->mhsNama."'".');" title="Hapus"><img src="images/icons/delete2.png" border="0" /></a></td>';
				echo '<td><a href="index2.php?act=mahasiswa&task=edit&id='.$v->mhsID.'">'.$v->mhsNama.'</a></td>';
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="<?php echo count($columns); ?>">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/mahasiswa/js/mahasiswa.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>