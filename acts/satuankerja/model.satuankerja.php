<?php
class SatuanKerja_Model {	
	public $tabel = 'satuankerja';
	public $primaryKey = 'skrID';
	
	public $skrID = 0;
	public $skrWilID = 0;
	public $skrNama = '';
	public $skrNamaSingkat = '';
	public $skrAlamat = '';
	public $skrNoTelp = '';
	public $skrNoFax = '';
}
?>