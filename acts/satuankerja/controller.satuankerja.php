<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.satuankerja.php';
require_once 'view.satuankerja.php';

switch ($app->task) {
	case 'save':
		saveSatuanKerja();
		break;
	default:
		editSatuanKerja(true, '');
		break;
}

function editSatuanKerja($success, $msg) {
	global $app;
	
	$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja WHERE skrID=1");
	if (!$objSatuanKerja) {
		$app->showPageError();
		exit();
	}
	
	SatuanKerja_View::editSatuanKerja($success, $msg, $objSatuanKerja);
}

function saveSatuanKerja() {
	global $app;
	
	//Create object
	$objSatuanKerja = new SatuanKerja_Model();
	$app->bind($objSatuanKerja);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objSatuanKerja->skrNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	if ($objSatuanKerja->skrNamaSingkat == '') {
		$doSave = false;
		$msg[] = '- Nama Singkat belum diisi';
	}
	
	if ($objSatuanKerja->skrAlamat == '') {
		$doSave = false;
		$msg[] = '- Alamat belum diisi';
	}
	
	if ($objSatuanKerja->skrWilID == 0) {
		$doSave = false;
		$msg[] = '- Berada di belum diisi';
	}
	
	//Query
	if ($doSave) {
		$sql = $app->createSQLforUpdate($objSatuanKerja);
		$app->query($sql);
		
		$success = true;
		$msg = 'Satuan Kerja "'.$objSatuanKerja->skrNama.'" berhasil diubah';
		
		$app->writeLog(EV_INFORMASI, 'satuankerja', $objSatuanKerja->skrID, $objSatuanKerja->skrNama, $msg);
	} else {
		$success = false;
		$msg = 'Satuan Kerja "'.$objSatuanKerja->skrNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	SatuanKerja_View::editSatuanKerja($success, $msg, $objSatuanKerja);
}
?>