<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class SatuanKerja_View {
	static function editSatuanKerja($success, $msg, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=satuankerja"><img src="images/icons/office_building.png" border="0" /> Satuan Kerja</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=satuankerja&task=save" method="POST" >
				<input type="hidden" id="skrID" name="skrID" value="<?php echo $obj->skrID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td width="150">Nama <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="skrNama" name="skrNama" maxlength="500" size="100" value="<?php echo $obj->skrNama; ?>" />
					</td>
				</tr>
				<tr>
					<td>Nama Singkat <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="skrNamaSingkat" name="skrNamaSingkat" maxlength="50" size="20" value="<?php echo $obj->skrNamaSingkat; ?>" />
					</td>
				</tr>
				<tr>
					<td>Alamat <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="skrAlamat" name="skrAlamat" maxlength="500" size="100" value="<?php echo $obj->skrAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td>Berada di</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php
		$app->createSelect('skrWilID', "SELECT wilID AS id, CONCAT(wilTingkat,' ',wilNama) AS nama FROM wilayah WHERE (wilTingkat='Kabupaten' OR wilTingkat='Kota') ORDER BY wilTingkat, wilNama", $obj->skrWilID);
?>
					</td>
				</tr>
				<tr>
					<td>No. Telepon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="skrNoTelp" name="skrNoTelp" maxlength="50" size="50" value="<?php echo $obj->skrNoTelp; ?>" />
					</td>
				</tr>
				<tr>
					<td>No. Fax</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="skrNoFax" name="skrNoFax" maxlength="50" size="50" value="<?php echo $obj->skrNoFax; ?>" />
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/satuankerja/js/edit.satuankerja.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>