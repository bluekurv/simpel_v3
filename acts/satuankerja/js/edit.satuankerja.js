//Variables
var form = $("#myForm");
var skrNama = $("#skrNama");
var skrNamaSingkat = $("#skrNamaSingkat");
var skrAlamat = $("#skrAlamat");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'skrNama' || this.id === undefined) {
		if (skrNama.val().length == 0){
			doSubmit = false;
			skrNama.addClass("error");		
		} else {
			skrNama.removeClass("error");
		}
	}
	
	if (this.id == 'skrNamaSingkat' || this.id === undefined) {
		if (skrNamaSingkat.val().length == 0){
			doSubmit = false;
			skrNamaSingkat.addClass("error");		
		} else {
			skrNamaSingkat.removeClass("error");
		}
	}
	
	if (this.id == 'skrAlamat' || this.id === undefined) {
		if (skrAlamat.val().length == 0){
			doSubmit = false;
			skrAlamat.addClass("error");		
		} else {
			skrAlamat.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
skrNama.blur(validateForm);
skrNamaSingkat.blur(validateForm);
skrAlamat.blur(validateForm);

skrNama.keyup(validateForm);
skrNamaSingkat.keyup(validateForm);
skrAlamat.keyup(validateForm);

form.submit(submitForm);

//Set focus
skrNama.focus();