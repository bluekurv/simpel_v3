<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Pengembalian_View {
	static function viewPengembalian($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pengembalian"><img src="images/icons/rosette.png" border="0" /> Daftar Pengembalian</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
<?php 
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				<select class="box" id="filterfield" name="filterfield">
<?php 
		foreach (array('mhnNoUrutLengkap'=>'No. Pendaftaran', 'mhnNoKtp'=>'No. KTP', 'mhnNama'=>'Nama Pemohon', 'perNama'=>'Nama Perusahaan') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['filter']['field']) {
				echo 'selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama']) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 60),
			$app->setHeader('mhnTgl', 'Tanggal', true, 60),
			$app->setHeader('mhnNoUrutLengkap', 'No. Pendaftaran', true),
			$app->setHeader('mhnNoKtp', 'No. KTP', true),
			$app->setHeader('mhnNama', 'Pemohon', true),
			$app->setHeader('perNama', 'Perusahaan', true),
			$app->setHeader('mhnDikembalikanTgl', 'Tgl. Pengembalian', true),
			$app->setHeader('mhnDikembalikanNo', 'No. Pengembalian', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);
?>
			</thead>
			<tbody>
<?php
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo '<tr>';
				echo '<td><a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'" title="Lihat"><img src="images/icons/zoom.png" border="0" /></a></td>';
				echo '<td>'.$app->MySQLDateToNormal($v->mhnTgl).'</td>';
				echo '<td><a href="index2.php?act=permohonan&task=read&id='.$v->mhnID.'">'.$v->mhnNoUrutLengkap.'</a></td>';
				echo '<td>'.$v->mhnNoKtp.'</td>';
				echo '<td>'.$v->mhnNama.'</td>';
				echo '<td>'.$v->perNama.'</td>';
				echo '<td>'.$app->MySQLDateToNormal($v->mhnDikembalikanTgl).'</td>';
				echo '<td>'.sprintf('%06d', $v->mhnDikembalikanNo).'/'.$objSatuanKerja->skrNamaSingkat.'/TU-SP/'.$app->getRoman(intval(substr($v->mhnDikembalikanTgl,5,2))).'/'.substr($v->mhnDikembalikanTgl,0,4).'</td>';
				echo "</tr>\n";
				
				$i++;
			}
		} else {
?>
			<tr><td colspan="8">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pengembalian/js/pengembalian.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>