<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
//$acl['Keuangan']['all'] = true;
$acl['Pejabat']['read'] = true;
$acl['Petugas Administrasi']['all'] = true;	//TODO: all?
//$acl['Petugas Loket Informasi']['all'] = true;
$acl['Petugas Loket Pelayanan']['all'] = true;	//TODO: all?
$acl['Petugas Loket Pengaduan']['read'] = true;
$acl['Petugas Loket Pengaduan']['find'] = true;
//$acl['Petugas Loket Penyerahan']['all'] = true;
//$acl['Surveyor']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'view.pengembalian.php';

switch ($app->task) {
	default:
		viewPengembalian(true, '');
		break;
}

function viewPengembalian($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('pengembalian', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('pengembalian', 'sort', 'mhnNoUrutLengkap');
	$dir   		= $app->pageVar('pengembalian', 'dir', 'desc');
	
	//Add filter(s) and it's default value
	$default = array(
		'field' => '',
		'nama' => ''
	);
	
	$field 		= $app->pageVar('pengembalian', 'filterfield', $default['field'], 'strval');
	$nama 		= $app->pageVar('pengembalian', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = $field." LIKE '%".$nama."%'";
	}
	if ($_SESSION['sesipengguna']->levelAkses == 'Petugas Administrasi') {
		$filter[] = "mhnAdministrasiOleh='".$_SESSION['sesipengguna']->ID."'";
	}
	$filter[] = "mhnAdministrasiStatus='Dikembalikan'";
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'field' => $field,
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM permohonan
				  LEFT JOIN perusahaan ON mhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM permohonan
			LEFT JOIN perusahaan ON mhnPerID=perID
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Pengembalian_View::viewPengembalian($success, $msg, $arr);
}
?>