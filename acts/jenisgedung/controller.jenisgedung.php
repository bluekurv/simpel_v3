<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.jenisgedung.php';
require_once 'view.jenisgedung.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editJenisGedung($app->id);
		break;
	case 'delete':
		deleteJenisGedung($app->id);
		break;
	case 'save':
		saveJenisGedung();
		break;
	default:
		viewJenisGedung(true, '');
		break;
}

function deleteJenisGedung($id) {
	global $app;
	
	//Get object
	$objJenisGedung = $app->queryObject("SELECT * FROM jenisgedung WHERE jgedID='".$id."'");
	if (!$objJenisGedung) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM jenisgedung WHERE jgedID='".$id."'");
		
		$success = true;
		$msg = 'Jenis Gedung "'.$objJenisGedung->jgedNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'jenisgedung', $objJenisGedung->jgedID, $objJenisGedung->jgedNama, $msg);
	} else {
		$success = false;
		$msg = 'Jenis Gedung "'.$objJenisGedung->jgedNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewJenisGedung($success, $msg);
}

function editJenisGedung($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objJenisGedung = $app->queryObject("SELECT * FROM jenisgedung WHERE jgedID='".$id."'");
		if (!$objJenisGedung) {
			$app->showPageError();
			exit();
		}
	} else {
		$objJenisGedung = new JenisGedung_Model();
	}
	
	JenisGedung_View::editJenisGedung(true, '', $objJenisGedung);
}

function saveJenisGedung() {
	global $app;
	
	//Create object
	$objJenisGedung = new JenisGedung_Model();
	$app->bind($objJenisGedung);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objJenisGedung->jgedNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT jgedID AS id FROM jenisgedung WHERE jgedNama='".$objJenisGedung->jgedNama."'", $objJenisGedung->jgedID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objJenisGedung->jgedNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objJenisGedung->jgedID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objJenisGedung);
		} else {
			$sql = $app->createSQLforUpdate($objJenisGedung);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objJenisGedung->jgedID = $app->queryField("SELECT LAST_INSERT_ID() FROM jenisgedung");
			
			$success = true;
			$msg = 'Jenis Gedung "'.$objJenisGedung->jgedNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'jenisgedung', $objJenisGedung->jgedID, $objJenisGedung->jgedNama, $msg);
		} else {
			$success = true;
			$msg = 'Jenis Gedung "'.$objJenisGedung->jgedNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'jenisgedung', $objJenisGedung->jgedID, $objJenisGedung->jgedNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Jenis Gedung "'.$objJenisGedung->jgedNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewJenisGedung($success, $msg);
	} else {
		JenisGedung_View::editJenisGedung($success, $msg, $objJenisGedung);
	}
}

function viewJenisGedung($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('jenisgedung', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('jenisgedung', 'sort', 'jgedNama');
	$dir		= $app->pageVar('jenisgedung', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('jenisgedung', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "jgedNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM jenisgedung
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM jenisgedung
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	JenisGedung_View::viewJenisGedung($success, $msg, $arr);
}
?>