//Variables
var form = $("#myForm");
var jgedNama = $("#jgedNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'jgedNama' || this.id === undefined) {
		if (jgedNama.val().length == 0){
			doSubmit = false;
			jgedNama.addClass("error");		
		} else {
			jgedNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
jgedNama.blur(validateForm);

jgedNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
jgedNama.focus();