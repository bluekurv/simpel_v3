//Variables
var form = $("#myForm");
var negKode = $("#negKode");
var negNama = $("#negNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'negKode' || this.id === undefined) {
		if (negKode.val().length == 0){
			doSubmit = false;
			negKode.addClass("error");		
		} else {
			negKode.removeClass("error");
		}
	}
	
	if (this.id == 'negNama' || this.id === undefined) {
		if (negNama.val().length == 0){
			doSubmit = false;
			negNama.addClass("error");		
		} else {
			negNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
negKode.blur(validateForm);
negNama.blur(validateForm);

negKode.keyup(validateForm);
negNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
negKode.focus();