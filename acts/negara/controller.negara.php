<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Petugas Loket Pelayanan']['all'] = true;
$acl['Petugas Administrasi']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.negara.php';
require_once 'view.negara.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editNegara($app->id);
		break;
	case 'delete':
		deleteNegara($app->id);
		break;
	case 'save':
		saveNegara();
		break;
	default:
		viewNegara(true, '');
		break;
}

function deleteNegara($id) {
	global $app;
	
	//Get object
	$objNegara = $app->queryObject("SELECT * FROM negara WHERE negID='".$id."'");
	if (!$objNegara) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	$totalProvinsi = intval($app->queryField("SELECT COUNT(*) AS total FROM wilayah WHERE wilParentID='".$id."' AND wilTingkat='Provinsi'"));
	if ($totalProvinsi > 0) {
		$doDelete = false;
		$msg[] = '- Terdapat '.$totalProvinsi.' provinsi pada negara tersebut';
	}
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM negara WHERE negID='".$id."'");
		
		$success = true;
		$msg = 'Negara "'.$objNegara->negNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'negara', $objNegara->negID, $objNegara->negNama, $msg);
	} else {
		$success = false;
		$msg = 'Negara "'.$objNegara->negNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewNegara($success, $msg);
}

function editNegara($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objNegara = $app->queryObject("SELECT * FROM negara WHERE negID='".$id."'");
		if (!$objNegara) {
			$app->showPageError();
			exit();
		}
	} else {
		$objNegara = new Negara_Model();
	}
	
	Negara_View::editNegara(true, '', $objNegara);
}

function saveNegara() {
	global $app;
	
	//Create object
	$objNegara = new Negara_Model();
	$app->bind($objNegara);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objNegara->negKode == '') {
		$doSave = false;
		$msg[] = '- Kode belum diisi';
	}
	
	if ($objNegara->negNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT negID AS id FROM negara WHERE negKode='".$objNegara->negKode."'", $objNegara->negID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Kode "'.$objNegara->negKode.'" sudah ada';
	}
	
	$isExist = $app->isExist("SELECT negID AS id FROM negara WHERE negNama='".$objNegara->negNama."'", $objNegara->negID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objNegara->negNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objNegara->negID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objNegara);
		} else {
			$sql = $app->createSQLforUpdate($objNegara);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objNegara->negID = $app->queryField("SELECT LAST_INSERT_ID() FROM negara");
			
			$success = true;
			$msg = 'Negara "'.$objNegara->negNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'negara', $objNegara->negID, $objNegara->negNama, $msg);
		} else {
			$success = true;
			$msg = 'Negara "'.$objNegara->negNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'negara', $objNegara->negID, $objNegara->negNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Negara "'.$objNegara->negNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewNegara($success, $msg);
	} else {
		Negara_View::editNegara($success, $msg, $objNegara);
	}
}

function viewNegara($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('negara', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort   	= $app->pageVar('negara', 'sort', 'negKode');
	$dir   		= $app->pageVar('negara', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('negara', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "negNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM negara
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM negara
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	Negara_View::viewNegara($success, $msg, $arr);
}
?>