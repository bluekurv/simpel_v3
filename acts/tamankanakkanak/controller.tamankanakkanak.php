<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'tamankanakkanak';
$childTitle = 'Izin Operasional Taman Kanak-Kanak';
$childKodeSurat = 'IOP-TK';
$childTembusan = 'Kepala Dinas Pendidikan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>