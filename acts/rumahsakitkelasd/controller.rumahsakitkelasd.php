<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

$childAct = 'rumahsakitkelasd';
$childTitle = 'Izin Operasional Rumah Sakit Kelas D';
$childKodeSurat = 'IOP-RSK-D';
$childTembusan = 'Kepala Dinas Kesehatan Kabupaten Pelalawan';
?>

<script>
	var childAct = '<?php echo $childTitle; ?>';
</script>

<?php
//Include file(s)
require_once 'acts/izinoperasional/controller.izinoperasional.php';
?>