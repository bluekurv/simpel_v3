<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class PemecahanIMB_View {
	static function editPemecahanIMB($success, $msg, $objDetailPermohonan, $obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pemecahanimb&task=edit&id=<?php echo $obj->pemecahanimbDmhnID; ?>"><img src="images/icons/rosette.png" border="0" /> Entri Pemecahan Izin Mendirikan Bangunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataEntri">
			<form id="myForm" name="myForm" action="index2.php?act=pemecahanimb&task=save" method="POST" >
				<input type="hidden" id="pemecahanimbID" name="pemecahanimbID" value="<?php echo $obj->pemecahanimbID; ?>" />
				<input type="hidden" id="pemecahanimbDmhnID" name="pemecahanimbDmhnID" value="<?php echo $obj->pemecahanimbDmhnID; ?>" />
				<div id="toolbarEntri">
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
				</div>
				<div id="toolbarEntri2" style="display:none;">
					<br>
					<input type="submit" class="button-link inline dark-blue" value="Simpan" />
					<a class="button-link inline blue" href="index2.php?act=permohonan">Batal</a>
					<br>
					<br>
				</div>
<?php
		$app->showMessage($success, $msg);
?>
				<p><i>Petunjuk: isian dengan tanda bintang (<span class="wajib">*</span>) wajib diisi.</i></p>
				<table id="entriTable" class="readTable" border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Grup Surat Izin</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$app->createSelectGroup('pemecahanimbGrupID', $obj->pemecahanimbGrupID);
?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
				</tr>
				<tr>
					<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $app->MySQLDateToIndonesia($objDetailPermohonan->mhnTgl); ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
					<td>
						<a href="index2.php?act=permohonan&task=read&id=<?php echo $objDetailPermohonan->mhnID; ?>&fromact=pemecahanimb&fromtask=edit&fromid=<?php echo $objDetailPermohonan->dmhnID; ?>"><?php echo $objDetailPermohonan->mhnNoUrutLengkap; ?></a>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNoKtp; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnNama; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->mhnAlamat; ?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;"></td><td></td>
					<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$objDetailPermohonan->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengurusan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<?php echo $objDetailPermohonan->dmhnTipe; ?>
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Data Umum Bangunan</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. IMB Induk</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbNoIMBInduk" name="pemecahanimbNoIMBInduk" maxlength="255" size="50" value="<?php echo $obj->pemecahanimbNoIMBInduk; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nama Pemilik</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbNama" name="pemecahanimbNama" maxlength="500" size="50" value="<?php echo $obj->pemecahanimbNama; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Umur</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbUmur" name="pemecahanimbUmur" maxlength="5" size="2" value="<?php echo $obj->pemecahanimbUmur; ?>" /> Tahun
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pekerjaan Pemilik</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbPekerjaan" name="pemecahanimbPekerjaan" maxlength="500" size="50" value="<?php echo $obj->pemecahanimbPekerjaan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat Pemilik</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbAlamat" name="pemecahanimbAlamat" maxlength="500" size="50" value="<?php echo $obj->pemecahanimbAlamat; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->pemecahanimbKelurahanID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$imbKelurahan = $app->queryObject($sql);
		if (!$imbKelurahan) {
			$imbKelurahan = new stdClass();
			$imbKelurahan->kode = '';
			$imbKelurahan->value = '';
		}
?>
						<input type="hidden" id="pemecahanimbKelurahanID" name="pemecahanimbKelurahanID" value="<?php echo $obj->pemecahanimbKelurahanID; ?>" />
						<input class="box readonly" id="pemecahanimbKelurahanKode" name="pemecahanimbKelurahanKode" size="8" value="<?php echo $imbKelurahan->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="pemecahanimbKelurahan" name="pemecahanimbKelurahan" maxlength="500" size="70" value="<?php echo $imbKelurahan->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. Surat Tanah</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbNoSuratTanah" name="pemecahanimbNoSuratTanah" maxlength="255" size="50" value="<?php echo $obj->pemecahanimbNoSuratTanah; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bangunan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbBangunan" name="pemecahanimbBangunan" maxlength="255" size="50" value="<?php echo $obj->pemecahanimbBangunan; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbAlamatTanah" name="pemecahanimbAlamatTanah" maxlength="255" size="50" value="<?php echo $obj->pemecahanimbAlamatTanah; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Kelurahan <span class="wajib">*</span></td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$sql = "SELECT CONCAT(w4.wilKode,'.',w3.wilKode,'.',w2.wilKode,'.',w.wilKode) AS kode, CONCAT(w3.wilTingkat,' ',w3.wilNama,' > ',w2.wilNama,' > ',w.wilNama) AS value 
				FROM wilayah AS w, wilayah AS w2, wilayah AS w3, wilayah AS w4 
				WHERE w.wilID='".$obj->pemecahanimbKelurahanTanahID."' AND w.wilParentID=w2.wilID AND w2.wilParentID=w3.wilID AND w3.wilParentID=w4.wilID";
		$imbKelurahanTanah = $app->queryObject($sql);
		if (!$imbKelurahanTanah) {
			$imbKelurahanTanah = new stdClass();
			$imbKelurahanTanah->kode = '';
			$imbKelurahanTanah->value = '';
		}
?>
						<input type="hidden" id="pemecahanimbKelurahanTanahID" name="pemecahanimbKelurahanTanahID" value="<?php echo $obj->pemecahanimbKelurahanTanahID; ?>" />
						<input class="box readonly" id="pemecahanimbKelurahanTanahKode" name="pemecahanimbKelurahanTanahKode" size="8" value="<?php echo $imbKelurahanTanah->kode; ?>" tabindex="-1" readonly />
						<input class="box" id="pemecahanimbKelurahanTanah" name="pemecahanimbKelurahanTanah" maxlength="500" size="70" value="<?php echo $imbKelurahanTanah->value; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">No. BAP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbNoBAP" name="pemecahanimbNoBAP" maxlength="255" size="50" value="<?php echo $obj->pemecahanimbNoBAP; ?>" />
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. BAP</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date" id="pemecahanimbTglBAP" name="pemecahanimbTglBAP" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->pemecahanimbTglBAP); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Rekomendasi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<textarea class="box" id="pemecahanimbRekomendasiCamat" name="pemecahanimbRekomendasiCamat" cols="80" rows="5"><?php echo $obj->pemecahanimbRekomendasiCamat; ?></textarea>
						<br><i>Setiap rekomendasi ditulis dan diakhiri dengan menekan tombol ENTER</i>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbKeterangan" name="pemecahanimbKeterangan" maxlength="255" size="50" value="<?php echo $obj->pemecahanimbKeterangan; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->pemecahanimbNo > 0) {
?>
						<input class="box" id="pemecahanimbNo" name="pemecahanimbNo" maxlength="5" size="5" value="<?php echo $obj->pemecahanimbNo; ?>" />
<?php 
			$arrNoIMBInduk = explode('/', $obj->pemecahanimbNoIMBInduk);
			if (count($arrNoIMBInduk) > 0) {
				$noIMBInduk = $arrNoIMBInduk[count($arrNoIMBInduk) - 1];
			} else {
				$noIMBInduk = 0;
			}
?>
						<span>/<?php echo $noIMBInduk; ?>/<?php echo CFG_COMPANY_SHORT_NAME; ?>/<?php echo $app->getRoman(intval(substr($obj->pemecahanimbTglPengesahan,5,2))); ?>/<?php echo substr($obj->pemecahanimbTglPengesahan,0,4); ?></span>
<?php
		} else {
?>
						Terakhir
						<input type="hidden" id="pemecahanimbNo" name="pemecahanimbNo" value="<?php echo $obj->pemecahanimbNo; ?>" />
<?php
		}
?>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box date2" id="pemecahanimbTglPengesahan" name="pemecahanimbTglPengesahan" maxlength="10" size="10" value="<?php echo $app->MySQLDateToNormal($obj->pemecahanimbTglPengesahan); ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsPengesahanOleh = $app->query($sql);
?>
						<select class="box" id="pemecahanimbPejID" name="pemecahanimbPejID">
<?php 
	while(($objPengesahanOleh = mysql_fetch_object($rsPengesahanOleh)) == true){
		echo '<option value="'.$objPengesahanOleh->id.'"';
		if ($objPengesahanOleh->id == $obj->pemecahanimbPejID) {
			echo ' selected';	
		}
		echo '>'.$objPengesahanOleh->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='imb'");
		if ($objJenisSuratIzin) {
			$jsiMasaBerlaku = $objJenisSuratIzin->jsiMasaBerlaku;
			$jsiMasaDaftarUlang = $objJenisSuratIzin->jsiMasaDaftarUlang;
		} else {
			$jsiMasaBerlaku = 0;			//Selama Ada
			$jsiMasaDaftarUlang = 0;		//Tidak Ada
		}
		
		if ($obj->pemecahanimbTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->pemecahanimbTglBerlaku);
		} else {
			if ($jsiMasaBerlaku > 0) {
				echo $jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
						<input type="hidden" id="pemecahanimbTglBerlaku" name="pemecahanimbTglBerlaku" value="<?php echo $app->MySQLDateToNormal($obj->pemecahanimbTglBerlaku); ?>" />
						<input type="hidden" id="jsiMasaBerlaku" name="jsiMasaBerlaku" value="<?php echo $jsiMasaBerlaku; ?>">
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
		if ($obj->pemecahanimbTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->pemecahanimbTglDaftarUlang);
		} else {
			if ($jsiMasaDaftarUlang > 0) {
				echo $jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
						<input type="hidden" id="pemecahanimbTglDaftarUlang" name="pemecahanimbTglDaftarUlang" value="<?php echo $app->MySQLDateToNormal($obj->pemecahanimbTglDaftarUlang); ?>" />
						<input type="hidden" id="jsiMasaDaftarUlang" name="jsiMasaDaftarUlang" value="<?php echo $jsiMasaDaftarUlang; ?>">
					</td>
				</tr>
				<tr>
					<td colspan="3"><p><b><u>Retribusi</u></b></p></td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Bendahara Penerimaan</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Keuangan' 
			ORDER BY pnNama";
	$rsKeuangan = $app->query($sql);
?>
						<select class="box" id="pemecahanimbPungutanBendaharaID" name="pemecahanimbPungutanBendaharaID">
<?php 
	while(($objKeuangan = mysql_fetch_object($rsKeuangan)) == true){
		echo '<option value="'.$objKeuangan->id.'"';
		if ($objKeuangan->id == $obj->pemecahanimbPungutanBendaharaID) {
			echo ' selected';	
		}
		echo '>'.$objKeuangan->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Mengetahui</td><td>&nbsp;:&nbsp;</td>
					<td>
<?php 
	$sql = "SELECT pnID AS id, pnNama AS nama 
			FROM pengguna 
			WHERE pnLevelAkses='Pejabat' 
			ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
	$rsMengetahui = $app->query($sql);
?>
						<select class="box" id="pemecahanimbPungutanMengetahuiID" name="pemecahanimbPungutanMengetahuiID">
<?php 
	while(($objMengetahui = mysql_fetch_object($rsMengetahui)) == true){
		echo '<option value="'.$objMengetahui->id.'"';
		if ($objMengetahui->id == $obj->pemecahanimbPungutanMengetahuiID) {
			echo ' selected';	
		}
		echo '>'.$objMengetahui->nama.'</option>';
	}
?>
						</select>
					</td>
				</tr>
				<tr>
					<td style="padding-left:20px;">Pembayaran Retribusi</td><td>&nbsp;:&nbsp;</td>
					<td>
						<input class="box" id="pemecahanimbPungutanNama" name="pemecahanimbPungutanNama" maxlength="500" size="50" value="<?php echo $obj->pemecahanimbPungutanNama; ?>" />
					</td>
				</tr>
				<tr>
					<td colspan="3" style="padding-left:20px;">dengan rincian sebagai berikut</td>
				</tr>
				<tr>
					<td colspan="3" style="padding-left:20px;"><input type="button" class="button-link inline dark-blue" id="btnTambahPungutan" value="Tambah Pungutan" /></td>
				</tr>
				<tr>
					<td colspan="3">
						<table id="entriTable2" border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody id="pungutan">
<?php 
		$i = 0;
		
		if (count($obj->pungutan) > 0) {
			foreach ($obj->pungutan as $pungutan) {
?>
						<tr>
							<td style="padding-left:20px; border-bottom: dotted 1px #BBBBBB;">
								<div>
									<img src="images/icons/delete2.png" class="btnHapusPungutan" title="Hapus Pungutan" />
									<input class="box bulat" id="pimbNo[<?php echo $i; ?>]" name="pimbNo[<?php echo $i; ?>]" maxlength="2" size="2" value="<?php echo $pungutan->pimbNo; ?>" placeholder="No" />
									<input class="box" id="pimbKelompok[<?php echo $i; ?>]" name="pimbKelompok[<?php echo $i; ?>]" maxlength="500" size="8" value="<?php echo $pungutan->pimbKelompok; ?>" placeholder="Kelompok" />
								</div>
								<input class="box" id="pimbNama[<?php echo $i; ?>]" name="pimbNama[<?php echo $i; ?>]" maxlength="500" size="19" value="<?php echo $pungutan->pimbNama; ?>" placeholder="Nama Pungutan" />
							</td>
							<td style="border-bottom: dotted 1px #BBBBBB;">&nbsp;:&nbsp;</td>
							<td style="border-bottom: dotted 1px #BBBBBB;">
								<input class="box desimal2" id="pimbNilai1[<?php echo $i; ?>]" name="pimbNilai1[<?php echo $i; ?>]" maxlength="11" size="6" value="<?php echo $pungutan->pimbNilai1; ?>" placeholder="Nilai" />
								<input class="box" id="pimbSatuan1[<?php echo $i; ?>]" name="pimbSatuan1[<?php echo $i; ?>]" maxlength="50" size="4" value="<?php echo $pungutan->pimbSatuan1; ?>" placeholder="Satuan" />
								x
								<input class="box desimal2" id="pimbNilai2[<?php echo $i; ?>]" name="pimbNilai2[<?php echo $i; ?>]" maxlength="11" size="6" value="<?php echo $pungutan->pimbNilai2; ?>" placeholder="Nilai" />
								<input class="box" id="pimbSatuan2[<?php echo $i; ?>]" name="pimbSatuan2[<?php echo $i; ?>]" maxlength="50" size="4" value="<?php echo $pungutan->pimbSatuan2; ?>" placeholder="Satuan" />
								x
								<input class="box desimal2" id="pimbNilai3[<?php echo $i; ?>]" name="pimbNilai3[<?php echo $i; ?>]" maxlength="11" size="6" value="<?php echo $pungutan->pimbNilai3; ?>" placeholder="Nilai" />
								<input class="box" id="pimbSatuan3[<?php echo $i; ?>]" name="pimbSatuan3[<?php echo $i; ?>]" maxlength="50" size="4" value="<?php echo $pungutan->pimbSatuan3; ?>" placeholder="Satuan" />
								= 
								<input class="box" id="pimbSatuanLuas[<?php echo $i; ?>]" name="pimbSatuanLuas[<?php echo $i; ?>]" maxlength="50" size="10" value="<?php echo $pungutan->pimbSatuanLuas; ?>" placeholder="Satuan Luas" />
								x Rp.
								<input class="box bulat" id="pimbNilaiRupiah[<?php echo $i; ?>]" name="pimbNilaiRupiah[<?php echo $i; ?>]" maxlength="11" size="11" value="<?php echo $pungutan->pimbNilaiRupiah; ?>" />
								x 
								<input class="box bulat" id="pimbUnit[<?php echo $i; ?>]" name="pimbUnit[<?php echo $i; ?>]" maxlength="11" size="6" value="<?php echo $pungutan->pimbUnit; ?>" placeholder="Unit" /> unit
							</td>
						</tr>
<?php 
				$i++;
			}
		}
?>				
						</tbody>
						</table>
					</td>
				</tr>
				</table>
			</form>
		</div>
	</div>
	<div class="halamanBawah"></div>
	<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	<script type="text/javascript">
		var elmCounter = <?php echo $i; ?>;
	</script>
	<script type="text/javascript" src="acts/pemecahanimb/js/edit.pemecahanimb.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function printPemecahanIMB($obj) {
		global $app;
		
		$objReport 		= new Report('Pemecahan Izin Mendirikan Bangunan', 'PemecahanIMB-'.substr($obj->pemecahanimbTglPengesahan,0,4).'-'.$obj->pemecahanimbNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->pemecahanimbPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,0);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<style>
		table {
			white-space: nowrap;
			font-family: inherit;
			font-size: 100%;
			font-weight: inherit;
			margin: 0;
			outline: 0;
			padding: 0;
			text-align: justify;
			vertical-align: baseline;
		}
		li {
			text-align: justify;
		}
	</style>

	<h4 align="center">
		KEPUTUSAN KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?><br><u><?php echo strtoupper($objSatuanKerja->wilTingkat.' '.$objSatuanKerja->wilNama); ?></u><br>
		NOMOR : <?php echo $obj->pemecahanimbNoLengkap; ?><br><br>
		TENTANG<br>
		PEMECAHAN IJIN MENDIRIKAN BANGUNAN (IMB)<br>
		NOMOR IMB : <?php echo $obj->pemecahanimbNoIMBInduk; ?><br> 
		<br>
		KEPALA <?php echo strtoupper($objSatuanKerja->skrNama); ?>,
	</h4>
	
<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="15%" align="left" valign="top">Membaca</td>
        <td width="7" align="left" valign="top">:</td>
		<td width="80%" align="left" valign="top">
			<table border="0" cellpadding="0" cellspacing="0">
<?php 
		$imbRekomendasiCamat = explode("\n",$obj->pemecahanimbRekomendasiCamat);
		$imbRekomendasiCamat2 = array();
		
		if (count($imbRekomendasiCamat) > 0) {
			foreach ($imbRekomendasiCamat as $rekomendasi) {
				if (trim($rekomendasi) != '') {
					$imbRekomendasiCamat2[] = trim($rekomendasi);
				}
			}
		}	
?>
				<tr><td width="4%">1.&nbsp;</td><td width="96%">Surat Permohonan dari Sdr <?php echo $obj->mhnNama; ?> tanggal <?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?> untuk mendapatkan Pemecahan Izin Mendirikan Bangunan.</td></tr>
				<tr><td>2.</td><td>Berita Acara Pemeriksaan dari <?php echo $objSatuanKerja->skrNama; ?> No. <?php echo $obj->pemecahanimbNoBAP; ?> Tanggal <?php echo $app->MySQLDateToIndonesia($obj->pemecahanimbTglBAP); ?></td></tr>
<?php
		$no = 3;
		if (count($imbRekomendasiCamat2) > 0) {	
			foreach ($imbRekomendasiCamat2 as $rekomendasi) {
?>
				<tr><td><?php echo $no; ?>.&nbsp;</td><td><?php echo trim($rekomendasi); ?></td></tr>
<?php
				$no++;
			}
		}
?>
			</table>
		</td>
	</tr>
	<tr>
		<td width="15%" align="left" valign="top">Menimbang</td><td width="7" align="left" valign="top">:</td>
		<td width="80%" align="justify" valign="top">
			<table>
				<tr><td width="4%">a.</td><td width="96%">Bahwa untuk mencapai tertib pelaksanaan pembangunan, serta pengawasan pengendalian sesuai dengan Rencana Umum Tata Ruang Kota, maka untuk mendirikan, merubah, membongkar bangunan diperlukan izin dari Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu.</td></tr>
				<tr><td>b.</td><td>Bahwa permohonan izin telah memenuhi persyaratan dan telah sesuai dengan ketentuan yang berlaku.</td></tr>
				<tr><td>c.</td><td>Bahwa untuk maksud tersebut diatas perlu ditetapkan dalam suatu Keputusan Kepala Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu.</td></tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="15%" align="left" valign="top">Mengingat</td><td width="7" align="left" valign="top">:</td>
		<td width="80%" align="left" valign="top">
<?php 
		echo $konfigurasi['consideringIMB'];
?>
		</td>
	</tr>
	</table>	
	
	<h4 align="center">MEMUTUSKAN</h4>
	
	<table border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="15%" align="left" valign="top">Menetapkan</td><td width="7" align="left" valign="top">:</td>
		<td width="80%" align="left" valign="top"></td>
	</tr>
	<tr>
		<td width="15%" align="left" valign="top"><b>PERTAMA</b></td><td width="7" align="left" valign="top">:</td>
		<td width="80%" align="left" valign="top" align="justify">Memberikan hak kepemilikan kepada :</td>
	</tr>
	<tr>
		<td width="15%" align="left" valign="top"></td>
		<td width="7" align="left" valign="top"></td>
		<td width="80%" align="left" valign="top">
			<table border="0" cellspacing="0">
			<tr>
				<td align="left" valign="top" width="25%">Nama</td>
				<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="70%"><?php echo strtoupper($obj->pemecahanimbNama); ?></td>
			</tr>
			<tr>
				<td align="left" valign="top" width="25%">Umur</td>
				<td align="left" valign="top" width="2%">:</td>
				<td align="left" valign="top" width="70%"><?php echo $obj->pemecahanimbUmur; ?> Tahun</td>
			</tr>
			<tr>
			  	<td align="left" valign="top">Pekerjaan</td>
			  	<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pemecahanimbPekerjaan; ?></td>
			</tr>

<?php 
		$sql = "SELECT wilayah2.wilNama AS kecamatan, CONCAT(wilayah3.wilTingkat,' ',wilayah3.wilNama) AS kabupaten 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->pemecahanimbKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasiPemilik = $app->queryObject($sql);
?>
			<tr>
				<td align="left" valign="top">Alamat</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pemecahanimbAlamat.', '.$objLokasiPemilik->kecamatan.', '.$objLokasiPemilik->kabupaten; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Nomor Surat Tanah</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pemecahanimbNoSuratTanah; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Bangunan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pemecahanimbBangunan; ?></td>
			</tr>
			<tr>
				<td colspan="3"><br><br>Diatas sebidang tanah kepunyaan/dikuasakan kepada yang bersangkutan tercatat dengan Surat Tanah<br></td>
			</tr>
			<tr>
				<td align="left" valign="top">Alamat</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $obj->pemecahanimbAlamatTanah; ?></td>
			</tr>
<?php 
		$sql = "SELECT wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilNama AS kabupaten 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->pemecahanimbKelurahanTanahID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasi = $app->queryObject($sql);
?>
			<tr>
				<td align="left" valign="top">Kelurahan/Desa</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $objLokasi->kelurahan; ?></td>
			</tr>
			<tr>
				<td align="left" valign="top">Kecamatan</td>
				<td align="left" valign="top">:</td>
				<td align="left" valign="top"><?php echo $objLokasi->kecamatan; ?></td>
			</tr>
			<tr>
				<td colspan="3"><br><br>Sesuai dengan Permohonan yang telah disetujui dan ketentuan yang tercantum dalam lampiran.<br></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top"><b>KEDUA</b></td><td align="left" valign="top">:</td>
		<td align="left" valign="top" align="justify">Keputusan ini mulai berlaku sejak tanggal ditetapkan dan apabila dikemudian hari terdapat kekeliruan/kesalahan dalam penetapan ini akan diadakan perbaikan/pembetulan kembali sebagai mana mestinya</td>
	</tr>
	<tr>
		<td align="left" valign="top"><b>TURUNAN</b></td><td align="left" valign="top">:</td>
		<td align="left" valign="top" align="center">dst</td>
	</tr>
	<tr>
		<td align="left" valign="top"></td><td align="left" valign="top"></td>
		<td align="left" valign="top" align="justify"><br><br>Surat Keputusan ini di atas materai Rp. 6.000,- diberikan kepada yang bersangkutan untuk dipergunakan sebagaimana mestinya<br></td>
	</tr>
	
	
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="48%">&nbsp;</td>
		<td width="52%">
			<?php $objReport->signature($konfigurasi, $objSatuanKerja, $objPengesahan, $app->MySQLDateToIndonesia($obj->pemecahanimbTglPengesahan)); ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$pdf->Output($objReport->filename);
	}
	
	static function printRetribusiPemecahanIMB($obj) {
		global $app;
		
		$objReport 		= new Report('Setoran Retribusi Pemecahan IMB', 'SetoranRetribusiPemecahanIMB-'.substr($obj->pemecahanimbTglPengesahan,0,4).'-'.$obj->pemecahanimbNo.'.pdf');
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		//$objPengesahan 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->imbPejID."' AND pnPgrID=pgrID");
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,0);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
		$objReport->header($pdf, $konfigurasi);
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
		
		$sql = "SELECT wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilNama AS kabupaten 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->pemecahanimbKelurahanTanahID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		$objLokasi = $app->queryObject($sql);
		
		ob_start();
?>
	<p align="center">
		<b>SETORAN RETRIBUSI IJIN MENDIRIKAN BANGUNAN (IMB)<br>
		TAHUN <?php echo substr($obj->pemecahanimbTglPengesahan,0,4); ?></b><br>
		NOMOR : <?php echo $obj->pemecahanimbNo.'/IMB/'.$app->getRoman(intval(substr($obj->pemecahanimbTglPengesahan,5,2))).'/'.substr($obj->pemecahanimbTglPengesahan,0,4); ?>
	</p>
	
	<table border="0" cellpadding="2" cellspacing="0" width="100%">
	<tr>
		<td width="20%">NAMA</td>
		<td width="4%">:</td>
		<td width="76%"><b><?php echo strtoupper($obj->mhnNama); ?></b></td>
	</tr>
	<tr>
		<td>LOKASI</td>
		<td>:</td>
		<td><?php echo $obj->pemecahanimbAlamatTanah.', '.$objLokasi->kelurahan.', '.$objLokasi->kecamatan; ?></td>
	</tr>
	<tr>
		<td>KETERANGAN</td>
		<td>:</td>
		<td><?php echo 'Pembayaran Retribusi '.$obj->pemecahanimbPungutanNama; ?></td>
	</tr>
	<tr>
		<td colspan="3"></td>
	</tr>
	<tr>
		<td colspan="3" style="text-align:justify;">Berdasarkan Pasal 9 Peraturan Daerah Nomor 04 Tahun 2005 Tentang Perubahan Atas Peraturan Daerah Kabupaten Pelalawan No. 24 Tahun 2001 Tentang Retribusi IMB Bahwa Retribusi Balik Nama IMB Dan Pemecahan IMB sebesar 25 % Dari Retribusi IMB Yang Bersangkutan dengan perincian sebagai berikut :</td>
	</tr>
	<tr>
		<td colspan="3"></td>
	</tr>
	</table>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<tr>
		<td width="4%" align="center" style="<?php echo $app->border(); ?>"><b>NO</b></td>
		<td width="26%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>URAIAN</b></td>
		<td width="10%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>JUMLAH UNIT</b></td>
		<td colspan="5" width="17%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>UKURAN</b></td>
		<td colspan="2" width="10%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>LUAS</b></td>
		<td colspan="2" width="15%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>PERDA</b></td>
		<td colspan="2" width="18%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>JUMLAH</b></td>
	</tr>
<?php 
		$jumlah = 0;
		$jumlah_dibulatkan = 0;
			
		if (count($obj->pungutan) > 0) {
			$j = 0;
			
			foreach ($obj->pungutan as $kelompok=>$pungutans) {
				$j++;
?>
	<tr>
		<td style="<?php echo $app->border('lr'); ?>"></td>
		<td style="<?php echo $app->border('r'); ?>"><b><u><?php echo ($j > 1) ? '<br>' : '';  ?><?php echo $kelompok; ?></u></b></td>
		<td style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="5" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
		<td colspan="2" style="<?php echo $app->border('r'); ?>"></td>
	</tr>
<?php
				$i = 1;
				
				foreach ($pungutans as $pungutan) {				
					$nilai = array();	//?
					$luas = 1;			//?
					
					if ($pungutan->pimbNilai1 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai1, -1).' '.$pungutan->pimbSatuan1); 
						$luas = bcmul($luas, $pungutan->pimbNilai1, 4);
						
						if ($pungutan->pimbSatuan1 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					if ($pungutan->pimbNilai2 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai2, -1).' '.$pungutan->pimbSatuan2);
						$luas = bcmul($luas, $pungutan->pimbNilai2, 4);
						
						if ($pungutan->pimbSatuan2 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					} 
					if ($pungutan->pimbNilai3 > 0) {
						$nilai[] = trim($app->MySQLToMoney($pungutan->pimbNilai3, -1).' '.$pungutan->pimbSatuan3); 
						$luas = bcmul($luas, $pungutan->pimbNilai3, 4);
						
						if ($pungutan->pimbSatuan3 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					} 
					
					$luas = bcmul($luas, $pungutan->pimbUnit, 4);
					$hasil = bcmul($luas, $pungutan->pimbNilaiRupiah, 4);
					$jumlah = bcadd($jumlah, $hasil, 4);
	?>
		<tr>
			<td align="center" style="<?php echo $app->border('lr'); ?>"><?php echo $i.'.' ?></td>
			<td style="<?php echo $app->border('r'); ?>"><?php echo $pungutan->pimbNama; ?></td>
			<td align="center" style="<?php echo $app->border('r'); ?>"><?php echo $pungutan->pimbUnit; ?></td>
			<td colspan="5" align="right" style="<?php echo $app->border('r'); ?>"><?php echo (count($nilai) > 0) ? implode(' x ',$nilai) : ''; ?></td>
			<td colspan="2" align="right" style="<?php echo $app->border('r'); ?>"><?php echo trim($app->MySQLToMoney($luas, -1).' '.$pungutan->pimbSatuanLuas); ?></td>
			<td width="4%">Rp.</td>
			<td width="11%" align="right" style="<?php echo $app->border('r'); ?>"><?php echo $app->MySQLToMoney($pungutan->pimbNilaiRupiah); ?></td>
			<td width="4%">Rp.</td>
			<td width="14%" align="right" style="<?php echo $app->border('r'); ?>"><?php echo $app->MySQLToMoney($hasil); ?></td>
		</tr>
<?php 
					$i++;
				}
			}
		}
?>
	<tr>
		<td colspan="8" style="<?php echo $app->border('tbl'); ?>"></td>
		<td colspan="4" style="<?php echo $app->border('tbr'); ?>">JUMLAH</td>
		<td align="right" style="<?php echo $app->border('tb'); ?>">Rp.</td>
		<td align="right" style="<?php echo $app->border('tbr'); ?>"><?php echo $app->MySQLToMoney($jumlah); ?></td>
	</tr>
<?php 
		//Pembulatan ke ratusan atas
		$intpart = floor($jumlah % 100);
		
		if ($intpart > 0) {
			$jumlah = bcsub($jumlah, $intpart, 0);
			$jumlah = bcadd($jumlah, 100, 0);
		}
		
		$jumlah_dibulatkan = round($jumlah,0);
?>
	<tr>
		
		<td colspan="8" style="<?php echo $app->border('bl'); ?>"></td>
		<td colspan="4" style="<?php echo $app->border('br'); ?>">JUMLAH DIBULATKAN</td>
		<td align="right" style="<?php echo $app->border('b'); ?>">Rp.</td>
		<td align="right" style="<?php echo $app->border('br'); ?>"><?php echo $app->MySQLToMoney($jumlah_dibulatkan); ?></td>
	</tr>
	</table>
	
	<br><br>
<?php 
		$objBendahara 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->pemecahanimbPungutanBendaharaID."' AND pnPgrID=pgrID");
		if (!is_object($objBendahara)) {
			$objBendahara = new stdClass();
			$objBendahara->pnNIP = '';
			$objBendahara->pnNama = '';
		}
		$objMengetahui 	= $app->queryObject("SELECT * FROM pengguna, pangkatgolonganruang WHERE pnID='".$obj->pemecahanimbPungutanMengetahuiID."' AND pnPgrID=pgrID");
		if (!is_object($objMengetahui)) {
			$objMengetahui = new stdClass();
			$objMengetahui->pnNIP = '';
			$objMengetahui->pnNama = '';
		}
?>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td width="50%" align="center">
			<br><br><br>
			<b>BENDAHARA PENERIMAAN DPMPTSP<br>KABUPATEN PELALAWAN</b>
			<br><br><br><br><br>
			<b><u><?php echo strtoupper($objBendahara->pnNama); ?></u></b>
			<br>
			NIP. <?php echo $objBendahara->pnNIP; ?>
		</td>
		<td width="50%" align="center">
			<?php echo $objSatuanKerja->wilIbukota.', '.$app->MySQLDateToIndonesia($obj->pemecahanimbTglPengesahan); ?>
			<br><br>
			<b>PEMOHON,</b>
			<br><br><br><br><br><br>
			<b><u><?php echo strtoupper($obj->mhnNama); ?></u></b>
		</td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<br><br>
			MENGETAHUI
			<br>
			<b>KASUBBID PELAYANAN PERIZINAN DAN NON PERIZINAN</b>
			<br><br><br><br><br>
			<b><u><?php echo strtoupper($objMengetahui->pnNama); ?></u></b>
			<br>
			NIP. <?php echo $objMengetahui->pnNIP; ?>
		</td>
	</tr>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
				
		$pdf->Output($objReport->filename);
	}
	
	static function printRecapPemecahanIMB($arr) {
		global $app;
		
		if ($arr['filter']['bulan'] == '') {
			$periode = $arr['filter']['tahun'];
		} else {
			$periode = strtoupper($app->bulan[$arr['filter']['bulan']]).' '.$arr['filter']['tahun'];
		}
		
		$objReport 		= new Report('Rekapitulasi Setoran Retribusi Pemecahan IMB', 'RekapitulasiSetoranRetribusiPemecahanIMB-'.$periode.'.pdf');
		
		$konfigurasi 	= array();
		$rs2 = $app->query("SELECT * FROM konfigurasi");
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
		}
		
		$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
		//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
		$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
		
		$pdf->SetAuthor(CFG_SITE_NAME);
		$pdf->SetCreator(CFG_SITE_NAME);
		$pdf->SetTitle($objReport->title);
		
		$pdf->setPrintHeader(false);
		$pdf->setPrintFooter(false);
		$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight,0);
		$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
		
		if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
			require_once(dirname(__FILE__).'/lang/ind.php');
			
			$languages = array();
			$pdf->setLanguageArray($languages);
		}
		
		$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
		
		$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
		
		//Mempengaruhi selain header
		$pdf->setCellHeightRatio($objReport->cellHeightRatio);
		
		ob_start();
?>
	<p align="center">
		<b>REKAPITULASI SETORAN RETRIBUSI IJIN MENDIRIKAN BANGUNAN (IMB)<br>
		PERIODE <?php echo $periode; ?></b>
	</p>
	
	<table border="0" cellpadding="1" cellspacing="0" width="100%">
	<thead>
		<tr>
			<th width="4%" align="center" style="<?php echo $app->border(); ?>"><b>NO</b></th>
			<th width="17%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>NOMOR SURAT</b></th>
			<th width="9%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>TANGGAL<br>PENGESAHAN</b></th>
			<th width="15%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>NAMA</b></th>
			<th width="29%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>LOKASI</b></th>
			<th colspan="2" width="12%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>JUMLAH DIBULATKAN</b></th>
			<th width="15%" align="center" style="<?php echo $app->border('rtb'); ?>"><b>KETERANGAN</b></th>
		</tr>
	</thead>
	<tbody>
<?php 
		$jumlah_dibulatkan = 0;
			
		if (count($arr['data']) > 0) {
			$i = 1;
			
			foreach ($arr['data'] as $pungutan) { 
				$jumlah_dibulatkan = bcadd($jumlah_dibulatkan, $pungutan->pemecahanimbPungutanJumlah, 4);
?>
		<tr>
			<td width="4%" align="center" style="<?php echo $app->border('blr'); ?>"><?php echo $i.'.' ?></td>
			<td width="17%" style="<?php echo $app->border('br'); ?>"><?php echo $pungutan->pemecahanimbNoLengkap; ?></td>
			<td width="9%" align="center" style="<?php echo $app->border('br'); ?>"><?php echo $app->MySQLDateToNormal($pungutan->pemecahanimbTglPengesahan); ?></td>
			<td width="15%"style="<?php echo $app->border('br'); ?>"><?php echo $pungutan->mhnNama; ?></td>
			<td width="29%"style="<?php echo $app->border('br'); ?>"><?php echo $pungutan->pemecahanimbAlamatTanah.', '.$pungutan->kelurahan.', '.$pungutan->kecamatan; ?></td>
			<td width="4%" style="<?php echo $app->border('b'); ?>">Rp.</td>
			<td width="8%" align="right" style="<?php echo $app->border('br'); ?>"><?php echo $app->MySQLToMoney($pungutan->pemecahanimbPungutanJumlah); ?></td>
			<td width="15%" style="<?php echo $app->border('br'); ?>"><?php echo $pungutan->pemecahanimbPungutanNama; ?></td>
		</tr>
<?php 
				$i++;
			}
		}
?>
		<tr>
			<td colspan="5" align="center" style="<?php echo $app->border('blr'); ?>"><b>JUMLAH</b></td>
			<td style="<?php echo $app->border('b'); ?>" ><b>Rp.</b></td>
			<td align="right" style="<?php echo $app->border('br'); ?>"><b><?php echo $app->MySQLToMoney($jumlah_dibulatkan); ?></b></td>
			<td style="<?php echo $app->border('br'); ?>"></td>
		</tr>
	</tbody>
	</table>
<?php
		$pdf->writeHTML(ob_get_contents());
		ob_clean();
				
		$pdf->Output($objReport->filename);
	}

	static function readPemecahanIMB($obj) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pemecahanimb&task=edit&id=<?php echo $obj->pemecahanimbDmhnID; ?><?php echo $app->getVarsFrom(true); ?>"><img src="images/icons/rosette.png" border="0" /> Lihat Pemecahan Izin Mendirikan Bangunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="toolbarEntri">
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
		</div>
		<div id="toolbarEntri2" style="display:none;">
			<br>
			<a class="button-link inline blue" href="<?php echo ($app->fromact == '') ? 'index2.php?act=permohonan' : 'index2.php?'.$app->getVarsFrom(false); ?>">Selesai</a>
			<br>
			<br>
		</div>
		<table class="readTable" border="0" cellpadding="1" cellspacing="1">
		<tr>
			<td colspan="3"><p><b><u>Permohonan</u></b></p></td>
		</tr>
		<tr>
			<td width="150" style="padding-left:20px;">Tanggal</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToIndonesia($obj->mhnTgl); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Pendaftaran</td><td>&nbsp;:&nbsp;</td>
			<td>
				<a href="index2.php?act=permohonan&task=read&id=<?php echo $obj->mhnID; ?>&fromact=pemecahanimb&fromtask=read&fromid=<?php echo $obj->dmhnID; ?>"><?php echo $obj->mhnNoUrutLengkap; ?></a>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. KTP Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNoKtp; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemohon</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->mhnAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;"></td><td></td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->mhnKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Data Umum Bangunan</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. IMB Induk</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbNoIMBInduk; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nama Pemilik</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbNama; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Umur</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbUmur; ?> Tahun
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pekerjaan Pemilik</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbPekerjaan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat Pemilik</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbAlamat; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->pemecahanimbKelurahanID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">No. Surat Tanah</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbNoSuratTanah; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Bangunan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbBangunan; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Alamat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbAlamatTanah; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Kelurahan</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$sql = "SELECT CONCAT(wilayah.wilNama,', ',wilayah2.wilNama,', ',wilayah3.wilTingkat,' ',wilayah3.wilNama,', ',wilayah4.wilNama) 
				FROM wilayah, wilayah AS wilayah2, wilayah AS wilayah3, wilayah AS wilayah4 
				WHERE wilayah.wilID='".$obj->pemecahanimbKelurahanTanahID."' AND wilayah.wilParentID=wilayah2.wilID AND wilayah2.wilParentID=wilayah3.wilID AND wilayah3.wilParentID=wilayah4.wilID";
		echo $app->queryField($sql);
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Keterangan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbKeterangan; ?>
			</td>
		</tr>
		<tr>
			<td colspan="3"><p><b><u>Administrasi</u></b></p></td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Nomor Surat</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $obj->pemecahanimbNoLengkap; ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Pengesahan</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->MySQLDateToNormal($obj->pemecahanimbTglPengesahan); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Pengesahan Oleh</td><td>&nbsp;:&nbsp;</td>
			<td>
				<?php echo $app->queryField("SELECT pnNama FROM pengguna WHERE pnID='".$obj->pemecahanimbPejID."'"); ?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Berlaku s.d.</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->pemecahanimbTglBerlaku != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->pemecahanimbTglBerlaku);
		} else {
			if ($obj->jsiMasaBerlaku > 0) {
				echo $obj->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Selama Ada';
			}
		}
?>
			</td>
		</tr>
		<tr>
			<td style="padding-left:20px;">Tgl. Daftar Berikutnya</td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		if ($obj->pemecahanimbTglDaftarUlang != '0000-00-00') {
			echo $app->MySQLDateToNormal($obj->pemecahanimbTglDaftarUlang);
		} else {
			if ($obj->jsiMasaDaftarUlang > 0) {
				echo $obj->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan';
			} else {
				echo 'Tidak Ada';
			}
		}
?>
			</td>
		</tr>
		</table>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/pemecahanimb/js/read.pemecahanimb.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
	
	static function viewPemecahanIMB($success, $msg, $arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=pemecahanimb"><img src="images/icons/rosette.png" border="0" /> Buku Register Pemecahan Izin Mendirikan Bangunan</a></h1>
	</div>
	<div class="halamanTengah">
		<div id="dataView">
			<div id="toolbarEntri">
				<a class="button-link inline dark-blue btnPrint" id="btnPrintRekapitulasi-pemecahanimb-printrecap-0" href="#printForm">Rekapitulasi Setoran Retribusi</a>
			</div>
<?php
		$app->showMessage($success, $msg);
?>
			<div id="pageFilter">
				Nomor Surat : 
				<input class="box" id="filternama" name="filternama" value="<?php echo $arr['filter']['nama']; ?>" style="color:#808080;" />
<?php 
		$app->createSelectMonth('filterbulan', $arr['filter']['bulan'], array(0 => 'Seluruhnya'));
		$app->createSelectYear('filtertahun', $arr['filter']['tahun'], array(0 => 'Seluruhnya'));
?>
				<input type="button" class="tombol find" id="find" name="find" title="Terapkan Filter" />
<?php	if ($arr['filter']['nama'] != $arr['default']['nama'] || $arr['filter']['bulan'] > 0 || $arr['filter']['tahun'] > 0) { ?>
				<input type="button" class="tombol cancel" id="cancel" name="cancel" value="Hapus Filter" title="Hapus Filter" />
<?php	} ?>
			</div>
			<div class="clear"></div>
			<table class="dataTable">
			<thead>
<?php 
		//NOTE: tanpa sort dir
		/*$columns = array(
			$app->setHeader('aksi', 'Aksi', false, 40),
			$app->setHeader('pemecahanimbNo', 'Nomor Surat', true),
			$app->setHeader('pemecahanimbNoIMBInduk', 'No. IMB Induk', true),
			$app->setHeader('pemecahanimbTglPengesahan', 'Tgl. Pengesahan', true),
			$app->setHeader('pemecahanimbTglBerlaku', 'Berlaku s.d. Tgl', true),
			$app->setHeader('pemecahanimbTglDaftarUlang', 'Tgl. Daftar Berikutnya', true)
		);
		echo $app->displayHeader($columns, $arr['sort'], $arr['dir']);*/
?>
			<tr>
				<th width="40">Aksi</th>
				<th>Nomor Surat</th>
				<th>No. IMB Induk</th>
				<th>Tgl. Pengesahan</th>
				<th>Berlaku s.d. Tgl</th>
				<th>Tgl. Daftar Berikutnya</th>
			</tr>
			</thead>
			<tbody>
<?php
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo ($i%2) ? '<tr>' : '<tr class="odd">';
				echo '<td>';
				echo '<a href="index2.php?act=pemecahanimb&task=read&id='.$v->pemecahanimbDmhnID.'&fromact=pemecahanimb" title="Lihat Surat Izin"><img src="images/icons/zoom.png" border="0" /></a> ';
				echo '<a id="btnPrint-'.$v->jsiKode.'-print-'.$v->pemecahanimbDmhnID.'" class="btnPrint" href="#printForm" title="Cetak Surat Izin" target="_blank"><img src="images/icons/printer.png" border="0" /></a> ';
				echo '</td>';
				echo '<td><a href="index2.php?act=pemecahanimb&task=read&id='.$v->pemecahanimbDmhnID.'&fromact=pemecahanimb" title="Entri">'.$v->pemecahanimbNoLengkap.'</a></td>';
				echo '<td>'.$v->pemecahanimbNoIMBInduk.'</td>';
				echo '<td>'.$app->MySQLDateToNormal($v->pemecahanimbTglPengesahan).'</td>';
				
				if ($v->pemecahanimbTglBerlaku != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->pemecahanimbTglBerlaku).'</td>';
				} else {
					if ($v->jsiMasaBerlaku > 0) {
						echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Selama Ada</td>';
					}
				}
				
				if ($v->pemecahanimbTglDaftarUlang != '0000-00-00') {
					echo '<td>'.$app->MySQLDateToNormal($v->pemecahanimbTglDaftarUlang).'</td>';
				} else {
					if ($v->jsiMasaDaftarUlang > 0) {
						echo '<td>'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
					} else {
						echo '<td>Tidak Ada</td>';
					}
				}
				
				echo "</tr>\n";
				$i++;
			}
		} else {
?>
			<tr><td colspan="6">Tidak ada data</td></tr>
<?php
		}
?>
			</tbody>
			</table>
			<div id="pageNavigation"><?php echo $app->displayNavigation($arr); ?></div>
		</div>
	</div>
	<div class="halamanBawah"></div>
<?php 
		$objReport = new Report();
		$objReport->addButton('pemecahanimb', 'printretribusi', 'Setoran Retribusi');
		$objReport->addButtonParameter('btnPrintRekapitulasi-pemecahanimb-printrecap-0', array('filternama', 'filterbulan', 'filtertahun'));
		$objReport->showDialog();
?>
	<script type="text/javascript" src="acts/pemecahanimb/js/pemecahanimb.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}
}
?>