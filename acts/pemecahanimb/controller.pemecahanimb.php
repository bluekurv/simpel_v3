<?php
//Check parent file
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Include file(s)
require_once 'model.pemecahanimb.php';
require_once 'view.pemecahanimb.php';

switch ($app->task) {
	case 'add':
		addPemecahanIMB($app->id);
		break;
	case 'edit':
		editPemecahanIMB($app->id);
		break;
	case 'delete':
		deletePemecahanIMB($app->id);
		break;
	case 'print':
	case 'printretribusi':
		printPemecahanIMB($app->id);
		break;
	case 'printrecap':
		printRecapPemecahanIMB($app->id);
		break;
	case 'read':
		readPemecahanIMB($app->id);
		break;
	case 'save':
		savePemecahanIMB();
		break;
	default:
		viewPemecahanIMB(true, '');
		break;
}

function addPemecahanIMB($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$sql = "INSERT INTO detailpermohonan (dmhnMhnID, dmhnPerID, dmhnJsiID, dmhnTipe, dmhnBiayaLeges, dmhnBiayaAdministrasi, dmhnPola, dmhnPerluSurvey, dmhnTglTargetSelesai, dmhnDiadministrasikanOleh, dmhnTambahan) 
			VALUES ('".$objDetailPermohonan->dmhnMhnID."', '".$objDetailPermohonan->dmhnPerID."', '".$objDetailPermohonan->dmhnJsiID."', '".$objDetailPermohonan->dmhnTipe."', '".$objDetailPermohonan->dmhnBiayaLeges."', '".$objDetailPermohonan->dmhnBiayaAdministrasi."', '".$objDetailPermohonan->dmhnPola."', '".$objDetailPermohonan->dmhnPerluSurvey."', '".$objDetailPermohonan->dmhnTglTargetSelesai."', '".$objDetailPermohonan->dmhnDiadministrasikanOleh."', 1)";
	$app->query($sql);
	
	$id = $app->queryField("SELECT LAST_INSERT_ID() FROM detailpermohonan");
	
	editPemecahanIMB($id);
}

function editPemecahanIMB($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			WHERE dmhnID='".$id."'";
	$objDetailPermohonan = $app->queryObject($sql);
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$objPemecahanIMB = $app->queryObject("SELECT * FROM pemecahanimb WHERE pemecahanimbDmhnID='".$id."'");
	if (!$objPemecahanIMB) {
		$objPemecahanIMB = new PemecahanIMB_Model();
	}
	
	$objPemecahanIMB->pungutan = array();
	
	$sql = "SELECT * FROM pungutanpemecahanimb WHERE pimbPemecahanImbID='".$objPemecahanIMB->pemecahanimbID."' ORDER BY pimbNo";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$obj->pimbNilai1 = $app->MySQLToMoney($obj->pimbNilai1, 2);
		$obj->pimbNilai2 = $app->MySQLToMoney($obj->pimbNilai2, 2);
		$obj->pimbNilai3 = $app->MySQLToMoney($obj->pimbNilai3, 2);
		$obj->pimbNilaiRupiah = $app->MySQLToMoney($obj->pimbNilaiRupiah);
		$obj->pimbUnit = $app->MySQLToMoney($obj->pimbUnit);
		$objPemecahanIMB->pungutan[] = $obj;
	}
	
	$objPemecahanIMB->pemecahanimbDmhnID = $objDetailPermohonan->dmhnID;
	
	PemecahanIMB_View::editPemecahanIMB(true, '', $objDetailPermohonan, $objPemecahanIMB);
}

function deletePemecahanIMB($id) {
	global $app;
	
	//Get object
	$objDetailPermohonan = $app->queryObject("SELECT * FROM detailpermohonan WHERE dmhnID='".$id."' AND dmhnTambahan=1");
	if (!$objDetailPermohonan) {
		$app->showPageError();
		exit();
	}
	
	$app->query("DELETE FROM detailpermohonan WHERE dmhnID='".$id."'");
	$app->query("DELETE FROM pemecahanimb WHERE pemecahanimbDmhnID='".$id."'");
		
	$success = true;
	$msg = 'Pemecahan IMB berhasil dihapus';
		
	$app->writeLog(EV_INFORMASI, 'detailpermohonan', $objDetailPermohonan->dmhnID, 'Pemecahan IMB', $msg);
	
	header('Location:index2.php?act=permohonan&success='.$success.'msg='.$msg);
}

function printPemecahanIMB($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN pemecahanimb ON pemecahanimbDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objPemecahanIMB = $app->queryObject($sql);
	if (!$objPemecahanIMB) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objPemecahanIMB->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
	
	$objPemecahanIMB->pungutan = array();
	
	$sql = "SELECT * FROM pungutanpemecahanimb WHERE pimbPemecahanImbID='".$objPemecahanIMB->pemecahanimbID."' ORDER BY pimbNo";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		if ($obj->pimbKelompok == '') {
			$obj->pimbKelompok = 'Ukuran Bangunan';
		}
		
		$objPemecahanIMB->pungutan[$obj->pimbKelompok][] = $obj;
	}
	
	
	
	switch ($app->task) {
		case 'print':
			PemecahanIMB_View::printPemecahanIMB($objPemecahanIMB);
			break;
		case 'printretribusi':
			PemecahanIMB_View::printRetribusiPemecahanIMB($objPemecahanIMB);
			break;
	}
}

function printRecapPemecahanIMB() {
	global $app;
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'bulan' => 0,
		'tahun' => 0
	);
	
	$nama 		= $app->getStr('filternama');
	$bulan 		= $app->getInt('filterbulan');
	$tahun 		= $app->getInt('filtertahun');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "pemecahanimbNo LIKE '".$nama."%'";
	}
	if ($bulan > $default['bulan']) {
		$filter[] = "MONTH(pemecahanimbTglPengesahan)='".$bulan."'";
	}
	if ($tahun > $default['tahun']) {
		$filter[] = "YEAR(pemecahanimbTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(),
		'filter' => array(
			'nama' => $nama,
			'bulan' => $bulan,
			'tahun' => $tahun
		)
	);
	
	//Query
	$sql = "SELECT *, wilayah.wilNama AS kelurahan, wilayah2.wilNama AS kecamatan, wilayah3.wilNama AS kabupaten
			FROM pemecahanimb
			LEFT JOIN detailpermohonan ON pemecahanimbDmhnID=dmhnID
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN wilayah ON wilayah.wilID=pemecahanimbKelurahanTanahID
			LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
			LEFT JOIN wilayah AS wilayah3 ON wilayah2.wilParentID=wilayah3.wilID
			LEFT JOIN wilayah AS wilayah4 ON wilayah3.wilParentID=wilayah4.wilID
			".$where." 
			ORDER BY YEAR(pemecahanimbTglPengesahan) DESC, pemecahanimbNo ASC";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	PemecahanIMB_View::printRecapPemecahanIMB($arr);
}

function readPemecahanIMB($id) {
	global $app;
	
	//Query
	$sql = "SELECT * 
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID 
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN pemecahanimb ON pemecahanimbDmhnID=dmhnID
			WHERE dmhnID='".$id."'";
	$objPemecahanIMB = $app->queryObject($sql);
	if (!$objPemecahanIMB) {
		$app->showPageError();
		exit();
	}
	
	/*if ($objPemecahanIMB->dmhnDiadministrasikanPada == '0000-00-00 00:00:00') {
		$app->showPageError();
		exit();
	}*/
		
	PemecahanIMB_View::readPemecahanIMB($objPemecahanIMB);
}

function savePemecahanIMB() {
	global $app;
	
	//Get object
	$objPemecahanIMBSebelumnya = $app->queryObject("SELECT * FROM pemecahanimb WHERE pemecahanimbID='".$app->getInt('pemecahanimbID')."'");
	if (!$objPemecahanIMBSebelumnya) {
		$objPemecahanIMBSebelumnya = new PemecahanIMB_Model();
	}
	
	//Create object
	$objPemecahanIMB = new PemecahanIMB_Model();
	$app->bind($objPemecahanIMB);
	
	//Modify object (if necessary)
	$objPemecahanIMB->pemecahanimbTglBAP = $app->NormalDateToMySQL($objPemecahanIMB->pemecahanimbTglBAP);
	$objPemecahanIMB->pemecahanimbTglPengesahan = $app->NormalDateToMySQL($objPemecahanIMB->pemecahanimbTglPengesahan);
	if ($objPemecahanIMB->pemecahanimbTglPengesahan != '0000-00-00') {
		if ($objPemecahanIMB->pemecahanimbNo == 0){
			//Jika sebelumnya belum ada nomor, berikan nomor baru
			$objPemecahanIMB->pemecahanimbNo = intval($app->queryField("SELECT MAX(pemecahanimbNo) AS nomor FROM pemecahanimb WHERE pemecahanimbNoIMBInduk='".$objPemecahanIMB->pemecahanimbNoIMBInduk."' AND YEAR(pemecahanimbTglPengesahan)='".substr($objPemecahanIMB->pemecahanimbTglPengesahan,0,4)."'")) + 1;
		}
		
		if ($app->getInt('jsiMasaBerlaku') > 0) {
			$objPemecahanIMB->pemecahanimbTglBerlaku = date('Y-m-d', strtotime($objPemecahanIMB->pemecahanimbTglPengesahan." +".$app->getInt('jsiMasaBerlaku')." years"));
		} else {
			$objPemecahanIMB->pemecahanimbTglBerlaku = '0000-00-00';
		}
		if ($app->getInt('jsiMasaDaftarUlang') > 0) {
			$objPemecahanIMB->pemecahanimbTglDaftarUlang = date('Y-m-d', strtotime($objPemecahanIMB->pemecahanimbTglPengesahan." +".$app->getInt('jsiMasaDaftarUlang')." years"));
		} else {
			$objPemecahanIMB->pemecahanimbTglDaftarUlang = '0000-00-00';
		}
	} else {
		$objPemecahanIMB->pemecahanimbNo = 0;
		
		$objPemecahanIMB->pemecahanimbTglBerlaku = '0000-00-00';
		$objPemecahanIMB->pemecahanimbTglDaftarUlang = '0000-00-00';
	}
	
	//Dapatkan nomor lengkap sebelum validasi
	//pemecahanimbNoIMBInduk
	$arrNoIMBInduk = explode('/', $objPemecahanIMB->pemecahanimbNoIMBInduk);
	if (count($arrNoIMBInduk) > 0) {
		$noIMBInduk = $arrNoIMBInduk[count($arrNoIMBInduk) - 1];
	} else {
		$noIMBInduk = 0;
	}
	$objPemecahanIMB->pemecahanimbNoLengkap = sprintf('%02d', $objPemecahanIMB->pemecahanimbNo).'/'.$noIMBInduk.'/'.CFG_COMPANY_SHORT_NAME.'/'.$app->getRoman(intval(substr($objPemecahanIMB->pemecahanimbTglPengesahan,5,2))).'/'.substr($objPemecahanIMB->pemecahanimbTglPengesahan,0,4);
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	//Jika nomor urut bisa diubah
	if ($objPemecahanIMB->pemecahanimbNo > 0){
		$isExist = $app->isExist("SELECT pemecahanimbID AS id FROM pemecahanimb WHERE pemecahanimbNo='".$objPemecahanIMB->pemecahanimbNo."' AND YEAR(pemecahanimbTglPengesahan)='".substr($objPemecahanIMB->pemecahanimbTglPengesahan,0,4)."'", $objPemecahanIMB->pemecahanimbID);
		if (!$isExist) {
			$doSave = false;
			$msg[] = '- Nomor Surat "'.$objPemecahanIMB->pemecahanimbNo.'" sudah ada';
		}
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objPemecahanIMB->pemecahanimbID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objPemecahanIMB);
		} else {
			$sql = $app->createSQLforUpdate($objPemecahanIMB);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objPemecahanIMB->pemecahanimbID = $app->queryField("SELECT LAST_INSERT_ID() FROM pemecahanimb");
		}

		$success = true;
		$msg = 'Pemecahan Izin Mendirikan Bangunan berhasil disimpan';
			
		$app->writeLog(EV_INFORMASI, 'pemecahanimb', $objPemecahanIMB->pemecahanimbID, $objPemecahanIMB->pemecahanimbNo, $msg);
		
		//Update detail permohonan sebagai penanda bahwa sudah dientri oleh Petugas Administrasi
		$app->query("UPDATE detailpermohonan SET dmhnDiadministrasikanOleh='".$_SESSION['sesipengguna']->ID."', dmhnDiadministrasikanPada='".date('Y-m-d H:i:s')."' WHERE dmhnID='".$objPemecahanIMB->pemecahanimbDmhnID."'");
		
		//Update pungutan pemecahan imb
		$app->query("DELETE FROM pungutanpemecahanimb WHERE pimbPemecahanImbID='".$objPemecahanIMB->pemecahanimbID."'");
		
		$jumlah = 0;
		$jumlah_dibulatkan = 0;
		
		if (count($_REQUEST['pimbNama']) > 0) {
			foreach (array_keys($_REQUEST['pimbNama']) as $k) {
				$pimbNo = mysql_escape_string(trim($_REQUEST['pimbNo'][$k]));
				$pimbKelompok = mysql_escape_string(trim($_REQUEST['pimbKelompok'][$k]));
				$pimbNama = mysql_escape_string(trim($_REQUEST['pimbNama'][$k]));
				$pimbNilai1 = $app->MoneyToMySQL($_REQUEST['pimbNilai1'][$k]);
				$pimbSatuan1 = mysql_escape_string(trim($_REQUEST['pimbSatuan1'][$k]));
				$pimbNilai2 = $app->MoneyToMySQL($_REQUEST['pimbNilai2'][$k]);
				$pimbSatuan2 = mysql_escape_string(trim($_REQUEST['pimbSatuan2'][$k]));
				$pimbNilai3 = $app->MoneyToMySQL($_REQUEST['pimbNilai3'][$k]);
				$pimbSatuan3 = mysql_escape_string(trim($_REQUEST['pimbSatuan3'][$k]));
				$pimbSatuanLuas = mysql_escape_string(trim($_REQUEST['pimbSatuanLuas'][$k]));
				$pimbNilaiRupiah = $app->MoneyToMySQL($_REQUEST['pimbNilaiRupiah'][$k]);
				$pimbUnit = $app->MoneyToMySQL($_REQUEST['pimbUnit'][$k]);
				
				if ($pimbNama != '') {
					$app->query("INSERT INTO pungutanpemecahanimb (
						pimbPemecahanImbID, 
						pimbNo, 
						pimbKelompok, 
						pimbNama, 
						pimbNilai1, 
						pimbSatuan1, 
						pimbNilai2, 
						pimbSatuan2, 
						pimbNilai3, 
						pimbSatuan3, 
						pimbSatuanLuas, 
						pimbNilaiRupiah, 
						pimbUnit
					) VALUES (
						'".$objPemecahanIMB->pemecahanimbID."', 
						'".$pimbNo."', 
						'".$pimbKelompok."', 
						'".$pimbNama."', 
						'".$pimbNilai1."', 
						'".$pimbSatuan1."',
						'".$pimbNilai2."', 
						'".$pimbSatuan2."',
						'".$pimbNilai3."', 
						'".$pimbSatuan3."',
						'".$pimbSatuanLuas."',
						'".$pimbNilaiRupiah."',
						'".$pimbUnit."'
					)");
					
					$luas = 1;
					if ($pimbNilai1 > 0) {
						$luas = bcmul($luas, $pimbNilai1, 4);
						
						if ($pimbSatuan1 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					if ($pimbNilai2 > 0) {
						$luas = bcmul($luas, $pimbNilai2, 4);
						
						if ($pimbSatuan2 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					if ($pimbNilai3 > 0) {
						$luas = bcmul($luas, $pimbNilai3, 4);
						
						if ($pimbSatuan3 == '%') {
							$luas = bcmul($luas, 0.01, 4);
						}
					}
					$luas = bcmul($luas, $pimbUnit, 4);
					$hasil = bcmul($luas, $pimbNilaiRupiah, 4);
					$jumlah = bcadd($jumlah, $hasil, 4);
				}
			}
		}
		
		//Pembulatan ke ratusan atas
		if ($jumlah % 100 > 0) {
			$jumlah = $jumlah - ($jumlah % 100) + 100;
		}
		$jumlah_dibulatkan = round($jumlah);
		
		//Update imbPungutanJumlah (yang sudah dibulatkan)
		$app->query("UPDATE pemecahanimb SET pemecahanimbPungutanJumlah='".$jumlah_dibulatkan."' WHERE pemecahanimbID='".$objPemecahanIMB->pemecahanimbID."'");
	} else {
		$success = false;
		$msg = 'Izin Mendirikan Bangunan tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		header('Location:index2.php?act=permohonan&success=true&msg='.$msg);
	} else {
		$sql = "SELECT * 
				FROM detailpermohonan
				LEFT JOIN permohonan ON dmhnMhnID=mhnID
				LEFT JOIN perusahaan ON dmhnPerID=perID
				WHERE dmhnID='".$objPemecahanIMB->pemecahanimbDmhnID."'";
		$objDetailPermohonan = $app->queryObject($sql);
		if (!$objDetailPermohonan) {
			$app->showPageError();
			exit();
		}
		
		PemecahanIMB_View::editPemecahanIMB($success, $msg, $objDetailPermohonan, $objPemecahanIMB);
	}
}

function viewPemecahanIMB($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('pemecahanimb', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	//NOTE: tanpa sort dir
	//$sort		= $app->pageVar('pemecahanimb', 'sort', 'pemecahanimbTglPengesahan');
	//$dir		= $app->pageVar('pemecahanimb', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => '',
		'bulan' => date('n'),
		'tahun' => date('Y')
	);
	
	$nama 		= $app->pageVar('pemecahanimb', 'filternama', $default['nama'], 'strval');
	$bulan 		= $app->pageVar('imb', 'filterbulan', $default['bulan'], 'intval');
	$tahun 		= $app->pageVar('pemecahanimb', 'filtertahun', $default['tahun'], 'intval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "pemecahanimbNo LIKE '".$nama."%'";
	}
	if ($bulan > 0) {
		$filter[] = "MONTH(pemecahanimbTglPengesahan)='".$bulan."'";
	}
	if ($tahun > 0) {
		$filter[] = "YEAR(pemecahanimbTglPengesahan)='".$tahun."'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama,
			'bulan' => $bulan,
			'tahun' => $tahun
		), 
		'default' => $default,
		//NOTE: tanpa sort dir
		//'sort' => $sort,
		//'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM pemecahanimb
				  LEFT JOIN detailpermohonan ON pemecahanimbDmhnID=dmhnID
				  LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
				  LEFT JOIN perusahaan ON dmhnPerID=perID
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	//NOTE: tanpa sort dir
	$sql = "SELECT *
			FROM pemecahanimb
			LEFT JOIN detailpermohonan ON pemecahanimbDmhnID=dmhnID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			".$where." 
			ORDER BY YEAR(pemecahanimbTglPengesahan) DESC, pemecahanimbNo DESC
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	PemecahanIMB_View::viewPemecahanIMB($success, $msg, $arr);
}
?>