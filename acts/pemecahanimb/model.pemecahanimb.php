<?php
class PemecahanIMB_Model {	
	public $tabel = 'pemecahanimb';
	public $primaryKey = 'pemecahanimbID';
	
	public $pemecahanimbID = 0;
	public $pemecahanimbDmhnID = 0;
	public $pemecahanimbGrupID = 0;
	public $pemecahanimbNo = 0;
	public $pemecahanimbNoLengkap = '';
	public $pemecahanimbNoIMBInduk = '';	//Nomor Utuh
	
	public $pemecahanimbNama = '';
	public $pemecahanimbUmur = 0;
	public $pemecahanimbPekerjaan = '';
	public $pemecahanimbAlamat = '';
	public $pemecahanimbKelurahanID = 0;
	
	public $pemecahanimbNoBAP = '';	//030815
	public $pemecahanimbTglBAP = '0000-00-00';	//030815
	
	public $pemecahanimbNoSuratTanah = '';
	public $pemecahanimbBangunan = '';
	public $pemecahanimbAlamatTanah = '';
	public $pemecahanimbKelurahanTanahID = 0;
	public $pemecahanimbRekomendasiCamat = '';	//030815
	public $pemecahanimbKeterangan = '';
	public $pemecahanimbTglBerlaku = '0000-00-00';
	public $pemecahanimbTglDaftarUlang = '0000-00-00';
	public $pemecahanimbPejID = 0;
	public $pemecahanimbTglPengesahan = '0000-00-00'; 		 	 	 	 	
	public $pemecahanimbArsip = '';
	public $pemecahanimbPungutanNama = '';
	public $pemecahanimbPungutanJumlah = 0;
	public $pemecahanimbPungutanBendaharaID = 0;
	public $pemecahanimbPungutanMengetahuiID = 0;
}

class PungutanPemecahanIMB_Model {
	public $pimbImbID = '';
	public $pimbNama = '';
	public $pimbKelompok = '';
	public $pimbNilai1 = '';
	public $pimbSatuan1 = '';
	public $pimbNilai2 = '';
	public $pimbSatuan2 = '';
	public $pimbNilai3 = '';
	public $pimbSatuan3 = '';
	public $pimbSatuanLuas = '';
	public $pimbNilaiRupiah = '';
	public $pimbUnit = 1;
}
?>