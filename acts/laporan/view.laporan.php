<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

class Laporan_View {
	static function daftarLaporan($arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=laporan&task=daftar"><img src="images/icons/printer.png" border="0" /> Daftar Perizinan</a></h1>
	</div>
	<div class="halamanTengah">
		<form id="myForm" action="index2.php?act=laporan&task=daftar" target="_self" method="POST">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><b>Jenis Surat Izin</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<select class="box" id="jsiKode" name="jsiKode">
					<option value="">--Seluruhnya--</option>
<?php 
		$jenissuratizin = $app->queryArrayOfObjects("SELECT *, jsiID AS id FROM jenissuratizin WHERE jsiTampil=1 GROUP BY jsiKode ORDER BY jsiNama");
		foreach ($jenissuratizin as $v) {
			echo '<option value="'.$v->jsiKode.'"';
			if ($v->jsiKode == $arr['jsiKode']) {
				echo ' selected';
			}
			echo '>'.$v->jsiNama.'</option>';
		}
?>
				</select>
			</td>
		</tr>
		<tr>
			<td><b>Grup Surat Izin</b></td><td>&nbsp;:&nbsp;</td>
			<td>
<?php 
		$app->createSelectGroup('grupID', $arr['grupID']);
?>
			</td>
		</tr>
		<tr>
			<td><b>Periode Pengesahan</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<select class="box" id="bulan1" name="bulan1">
<?php 
		foreach (array(1=>'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['bulan1']) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<select class="box" id="tahun1" name="tahun1">
<?php 
		for ($i=2000;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['tahun1']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
				s.d.
				<select class="box" id="bulan2" name="bulan2">
<?php 
		foreach (array(1=>'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['bulan2']) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<select class="box" id="tahun2" name="tahun2">
<?php 
		for ($i=2000;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['tahun2']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
			</td>
		</tr>
		<tr>
			<td><b>Nama Pemohon</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<input class="box" id="nama" name="nama" maxlength="50" size="50" value="<?php echo $arr['nama']; ?>" />
			</td>
		</tr>
		<tr>
			<td><b>Nama Perusahaan</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<input class="box" id="nama2" name="nama2" maxlength="50" size="50" value="<?php echo $arr['nama2']; ?>" />
			</td>
		</tr>
		<tr>
			<td><b>Kecamatan</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<input type="hidden" id="kecamatanID" name="kecamatanID" value="<?php echo $arr['kecamatanID']; ?>" />
				<input class="box" id="kecamatan" name="kecamatan" maxlength="50" size="70" value="<?php echo $arr['kecamatan']; ?>" />
			</td>
		</tr>
		<tr>
			<td valign="top"><b>Nilai Investasi (Rp)</b></td><td valign="top">&nbsp;:&nbsp;</td>
			<td>
				<select class="box" id="investasi_parameter_bawah" name="investasi_parameter_bawah">
<?php 
		foreach (array('>', '>=', '=') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $arr['investasi_parameter_bawah']) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input class="box bulat" id="investasi_batas_bawah" name="investasi_batas_bawah" maxlength="20" size="15" value="<?php echo $app->MySQLToMoney($arr['investasi_batas_bawah']); ?>" />
				dan 
				<select class="box" id="investasi_parameter_atas" name="investasi_parameter_atas">
<?php 
		foreach (array('<', '<=', '=') as $v) {
			echo '<option value="'.$v.'"';
			if ($v == $arr['investasi_parameter_atas']) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<input class="box bulat" id="investasi_batas_atas" name="investasi_batas_atas" maxlength="20" size="15" value="<?php echo $app->MySQLToMoney($arr['investasi_batas_atas']); ?>" />
				<br>(<i>Khusus SIUP. Jika batas bawah bernilai 0 dan batas atas bernilai 0, maka parameter ini dikecualikan dari penampilan data</i>)
			</td>
		</tr>
		<tr>
			<td><b>Jenis Usaha</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<input class="box" id="jenisusaha" name="jenisusaha" maxlength="50" size="50" value="<?php echo $arr['jenisusaha']; ?>" />
			</td>
		</tr>
		<tr>
			<td><b>Tgl. Tanda Tangan</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<input class="box date" id="tanggal" name="tanggal" maxlength="10" size="10" value="<?php echo $arr['tanggal']; ?>" />
			</td>
		</tr>
		<tr>
			<td><b>Penandatangan</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<select class="box" id="pejabat" name="pejabat">
					<option value="">--Pilih--</option>
<?php 
		$sql = "SELECT *, pnID AS id 
				FROM pengguna 
				WHERE pnLevelAkses='Pejabat' 
				ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
		$pejabat = $app->queryArrayOfObjects($sql);
		foreach ($pejabat as $v) {
			echo '<option value="'.$v->pnID.'"';
			if ($v->pnID == $arr['pejabat']) {
				echo ' selected';
			}
			echo '>'.$v->pnNama.'</option>';
		}
?>
				</select>
			</td>
		</tr>
		<tr>
			<td></td><td></td>
			<td>
				<input type="button" id="btnShow" class="button-link inline dark-blue" value="Tampilkan">
				<input type="button" id="btnPreview" class="button-link inline dark-blue" value="Preview Cetak">
				<input type="button" id="btnExport" class="button-link inline dark-blue" value="Ekspor ke MS.Excel">
				<input type="button" id="btnPDF" class="button-link inline dark-blue" value="Ekspor ke PDF">
				<br><br>
			</td>
		</tr>
		</table>
		</form>
		<?php Laporan_View::resultdaftarLaporan($arr); ?>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/laporan/js/daftar.laporan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}

	static function printdaftarLaporan($arr) {
		global $app;
		
		$bulan = array(1=>'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
		
		$periode = '';
		if ($arr['bulan1'] == $arr['bulan2'] && $arr['tahun1'] == $arr['tahun2']) {
			$periode = $bulan[$arr['bulan1']].' '.$arr['tahun1'];
		} else {
			$periode = $bulan[$arr['bulan1']].' '.$arr['tahun1'].' s.d. '.$bulan[$arr['bulan2']].' '.$arr['tahun2'];
		}
		
		if ($arr['jsiKode'] != '') {
			$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='".$arr['jsiKode']."'");
		} else {
			$objJenisSuratIzin = new stdClass();
			$objJenisSuratIzin->jsiNama = '';
		}
		
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPejabat = $app->queryObject("SELECT * FROM pengguna WHERE pnID='".$arr['pejabat']."'");
?>
	<h1 align="center">DAFTAR PERIZINAN <?php echo strtoupper($objJenisSuratIzin->jsiNama); ?></h1>
	<b>Periode : <?php echo $periode; ?></b>
<?php
		Laporan_View::resultdaftarLaporan($arr);
		
		if ($objPejabat) {
?>
	<br><br>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td colspan="5" width="50%"></td>
		<td colspan="5" width="50%" align="center">
			<?php echo strtoupper($objSatuanKerja->wilIbukota.', '.$app->MySQLDateToIndonesia($app->NormalDateToMySQL($arr['tanggal']))); ?><br>
			<?php echo strtoupper($objPejabat->pnJabatan); ?><br>
			<br><br><br>
			<b><u><?php echo strtoupper($objPejabat->pnNama); ?></u></b><br>
			NIP. <?php echo $objPejabat->pnNIP; ?>
		</td>
	</tr>
	</table>
<?php
		}
	}

	static function resultdaftarLaporan($arr) {
		global $app;
		
		switch ($app->task) {
			case 'exportdaftar':
			case 'printdaftar':
				$printexport = true;
				$table = 'class="the-data" width="100%"';
				break;
			case 'pdfdaftar':
				$printexport = true;
				$table = 'border="1" cellpadding="2" cellspacing="0" width="100%"';
				break;
			default:
				$printexport = false;
				$table = 'class="dataTable"';
		}
		
		if ($arr['jsiKode'] == 'imb') {
			$imb = true;
		} else {
			$imb = false;
		}
		
		if ($arr['jsiKode'] == 'reklame') {
			$reklame = true;
		} else {
			$reklame = false;
		}
		
		if ($arr['jsiKode'] == 'siup') {
			$siup = true;
			$kolom = 12;
		} else {
			$siup = false;
			$kolom = 11;
		}
		
		if ($arr['jsiKode'] == 'situ') {
			$situ = true;
		} else {
			$situ = false;
		}
		
		if ($arr['jsiKode'] == 'situho') {
			$situho = true;
		} else {
			$situho = false;
		}
		
		if ($arr['jsiKode'] == 'tdp') {
			$tdp = true;
		} else {
			$tdp = false;
		}
		
		$kolomWidth = floor(100/($kolom-1));
			
		$totalModalPerKecamatan = 0;
		
		$totalData = 0;
		
		if (count($arr['data']) > 0) {
			foreach ($arr['data'] as $kecamatan=>$suratizin) {
?>
	<div><b>Kecamatan : <?php echo $kecamatan; ?></b></div>
	<div><b>Jumlah Izin : <?php echo count($suratizin); ?></b></div>
	<table <?php echo $table; ?>>
	<thead>
		<tr>
			<th align="center" width="4%"><b>No.</b></th>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Atas Nama</b></th>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Perusahaan</b></th>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Bidang</b></th>
<?php 
				if ($imb) {
?>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Lokasi Bangunan</b></th>
<?php
				} else {
?>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Lokasi</b></th>
<?php
				}	
?>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Tanggal Daftar</b></th>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Tanggal SK</b></th>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Nomor SK</b></th>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Masa Berlaku</b></th>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Jenis Perizinan</b></th>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Sifat</b></th>
<?php 
				if ($siup) {
?>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Nilai Investasi (Rp)</b></th>
<?php
				}		
?>
			<th align="center" width="<?php echo $kolomWidth; ?>%"><b>Keterangan</b></th>
		</tr>
		<tr>
<?php 
				if ($printexport) {
					for ($i=1;$i<=$kolom;$i++) {
?>
			<th align="center"><b><?php echo $i; ?></b></th>
<?php
					}
				}
?>
		</tr>
	</thead>
	<tbody>
<?php 
				$sql = "SELECT grupID AS id, grup.* FROM grup";
				$arrGrup = $app->queryArrayOfObjects($sql);
				
				if (count($suratizin) > 0) {
					$i = 1;
					$modalPerKecamatan = 0;
					
					foreach ($suratizin as $v) {
						/*if ($v->perNama != 'PT. SINAR AGRO RAYA') {
							continue;
						}
						
						$app->debug($v);*/
						echo '<tr nobr="true">';
						echo '<td align="center" width="4%">'.$i.'.</td>';
						echo '<td width="'.$kolomWidth.'%">'.$v->namapemilik.'</td>';
						echo '<td width="'.$kolomWidth.'%">'.$v->perNama.'</td>';
						echo '<td>'.(isset($arrGrup[$v->{$v->jsiKode.'GrupID'}]) ? $arrGrup[$v->{$v->jsiKode.'GrupID'}]->grupNama : '').'</td>';
						if ($v->jsiKode == 'reklame') {
							echo '<td width="'.$kolomWidth.'%">'.$v->reklamePemasanganLokasi.', '.$v->wilNama.'</td>';
						} else if ($v->jsiKode == 'imb') {
							echo '<td width="'.$kolomWidth.'%">'.$v->imbLokasi.', '.$v->wilNama.'</td>';
						} else if ($v->jsiKode == 'situ') {
							//NOTE: specific jika situ alamat dan atau kelurahan tidak diisi
							$alamatnya = ($v->situAlamat != '') ? $v->situAlamat : $v->perAlamat;
							$kecamatannya = ($v->parentNama != '') ? $v->parentNama : $v->parentNama2;
							
							echo '<td width="'.$kolomWidth.'%">'.$alamatnya.', '.$kecamatannya.'</td>';
						} else if ($v->jsiKode == 'situho') {
							echo '<td width="'.$kolomWidth.'%">'.$v->situhoAlamat.', '.$v->situhoKelurahanID.'</td>';
						} else if ($v->jsiKode == 'tdp') {
							echo '<td width="'.$kolomWidth.'%">'.$v->tdpAlamat.', '.$v->tdpKelurahanID.'</td>';
						} else {
							echo '<td width="'.$kolomWidth.'%">'.$v->perAlamat.', '.$v->wilNama.'</td>';
						}
						echo '<td align="center" width="'.$kolomWidth.'%">'.$app->MySQLDateToNormal($v->mhnTgl).'</td>';
						echo '<td align="center" width="'.$kolomWidth.'%">'.$app->MySQLDateToNormal($v->tglpengesahan).'</td>';
						echo '<td width="'.$kolomWidth.'%">'.$v->no.'</td>';
						
						if ($v->tglberlaku != '0000-00-00') {
							echo '<td align="center" width="'.$kolomWidth.'%">'.$app->MySQLDateToNormal($v->tglberlaku).'</td>';
						} else {
							if ($v->jsiMasaBerlaku > 0) {
								echo '<td align="center" width="'.$kolomWidth.'%">'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
							} else {
								echo '<td align="center" width="'.$kolomWidth.'%">Selama Ada</td>';
							}
						}
						
						/*if ($v->tgldaftarulang != '0000-00-00') {
							echo '<td align="center">'.$app->MySQLDateToNormal($v->tgldaftarulang).'</td>';
						} else {
							if ($v->jsiMasaDaftarUlang > 0) {
								echo '<td align="center">'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
							} else {
								echo '<td align="center">Tidak Ada</td>';
							}
						}*/
						
						/*if ($v->jsiMasaBerlaku == 0) {
							if ($v->jsiMasaDaftarUlang == 0) {
								echo '<td>Selama Ada</td>';
							} else {
								if ($v->tgldaftarulang != '0000-00-00') {
									echo '<td align="center">'.$app->MySQLDateToNormal($v->tgldaftarulang).'</td>';
								} else {
									echo '<td align="center">'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
								}
							}
						} else {
							if ($v->jsiMasaDaftarUlang == 0) {
								if ($v->tglberlaku != '0000-00-00') {
									echo '<td>'.$app->MySQLDateToNormal($v->tglberlaku).'</td>';
								} else {
									echo '<td>'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
								}
							} else {
								if ($v->tgldaftarulang != '0000-00-00') {
									echo '<td align="center">'.$app->MySQLDateToNormal($v->tgldaftarulang).'</td>';
								} else {
									echo '<td align="center">'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
								}
							}
						}*/
						
						echo '<td width="'.$kolomWidth.'%">'.$v->jsiNama.'</td>';
						echo '<td width="'.$kolomWidth.'%">'.$v->dmhnTipe.'</td>';
						if ($siup) {
							echo '<td align="right" width="'.$kolomWidth.'%">'.$app->MySQLToMoney($v->nilaiinvestasi).'</td>';
							$modalPerKecamatan = bcadd($modalPerKecamatan, $v->nilaiinvestasi);
						}
						
						if ($siup) {
							echo '<td width="'.$kolomWidth.'%">'.$v->siupJenisBarangJasa.'</td>';
						} else {
							echo '<td width="'.$kolomWidth.'%">'.$v->keterangan.'</td>';
						}
						echo "</tr>\n";
						
						$i++;
						
						$totalData++;
					} //END untuk setiap surat izin dalam kecamatan tertentu
					
					if ($siup) {
						echo "<tr>";
						echo '<td colspan="8"></td>';
						echo '<td><b>JUMLAH</b></td>';
						echo '<td align="right"><b>'.$app->MySQLToMoney($modalPerKecamatan).'</b></td>';
						echo '<td></td>';
						echo "</tr>\n";
						
						$totalModalPerKecamatan = bcadd($totalModalPerKecamatan, $modalPerKecamatan);
					}
				} else {
?>
		<tr><td colspan="<?php echo $kolom; ?>">Tidak ada data</td></tr>
<?php
				}
?>
	</tbody>
	</table>
	<br><br>
<?php 
			} // END untuk setiap kecamatan
			
			//echo $totalData;
			
			if ($siup) {
?>
	<b>JUMLAH SELURUH NILAI INVESTASI Rp. <?php echo $app->MySQLToMoney($totalModalPerKecamatan); ?></b>
<?php
			}
		} else {
?>
	<table <?php echo $table; ?>>
	<thead>
		<tr>
			<th><b>No.</b></th>
			<th><b>Atas Nama</b></th>
			<th><b>Perusahaan</b></th>
			<th><b>Lokasi</b></th>
			<th><b>Tanggal Daftar</b></th>
			<th><b>Tanggal SK</b></th>
			<th><b>Nomor SK</b></th>
			<th><b>Masa Berlaku</b></th>
			<th><b>Jenis Perizinan</b></th>
			<th><b>Sifat</b></th>
<?php 
				if ($siup) {
					echo '<th><b>Nilai Investasi (Rp)</b></th>';
				}		
?>
			<th><b>Keterangan</b></th>
		</tr>
		<tr>
<?php 
				if ($printexport) {
					for ($i=1;$i<=$kolom;$i++) {
						echo '<th align="center"><b>'.$i.'</b></th>';
					}
				}
?>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="<?php echo $kolom; ?>">Tidak ada data</td>
		</tr>
	</tbody>
	</table>
<?php
		}
?>
	<div><b>TOTAL IZIN : <?php echo $totalData; ?></b></div>	
<?php
	}
	
	static function realisasiLaporan($arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=laporan&task=realisasi"><img src="images/icons/printer.png" border="0" /> Realisasi Pelayanan Izin</a></h1>
	</div>
	<div class="halamanTengah">
		<form id="myForm" action="index2.php?act=laporan&task=daftar" target="_self" method="POST">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><b>Periode</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<select class="box" id="tahun" name="tahun">
<?php 
		for ($i=2000;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
			</td>
		</tr>
		<tr>
			<td><b>Tgl. Tanda Tangan</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<input class="box date" id="tanggal" name="tanggal" maxlength="10" size="10" value="<?php echo $arr['tanggal']; ?>" />
			</td>
		</tr>
		<tr>
			<td><b>Penandatangan</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<select class="box" id="pejabat" name="pejabat">
					<option value="">--Pilih--</option>
<?php 
		$sql = "SELECT *, pnID AS id 
				FROM pengguna 
				WHERE pnLevelAkses='Pejabat' 
				ORDER BY pnDefaultPengesahanLaporan DESC, pnNama";
		$pejabat = $app->queryArrayOfObjects($sql);
		foreach ($pejabat as $v) {
			echo '<option value="'.$v->pnID.'"';
			if ($v->pnID == $arr['pejabat']) {
				echo ' selected';
			}
			echo '>'.$v->pnNama.'</option>';
		}
?>
				</select>
			</td>
		</tr>
		<tr>
			<td></td><td></td>
			<td>
				<input type="button" id="btnShow" class="button-link inline dark-blue" value="Tampilkan">
				<input type="button" id="btnPreview" class="button-link inline dark-blue" value="Preview Cetak">
				<input type="button" id="btnExport" class="button-link inline dark-blue" value="Ekspor ke MS.Excel">
				<input type="button" id="btnPDF" class="button-link inline dark-blue" value="Ekspor ke PDF">
				<br><br>
			</td>
		</tr>
		</table>
		</form>
		<?php Laporan_View::resultrealisasiLaporan($arr); ?>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/laporan/js/realisasi.laporan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}

	static function printrealisasiLaporan($arr) {
		global $app;
		
		$objSatuanKerja = $app->queryObject("SELECT * FROM satuankerja, wilayah WHERE skrWilID=wilID");
		$objPejabat = $app->queryObject("SELECT * FROM pengguna WHERE pnID='".$arr['pejabat']."'");
?>
	<h1 align="center">REALISASI PELAYANAN IZIN<br>TAHUN <?php echo $arr['tahun']; ?></h1>
<?php
		Laporan_View::resultrealisasiLaporan($arr);
		
		if ($objPejabat) {
?>
	<br>
	<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr>
		<td colspan="13" width="50%"></td>
		<td colspan="13" width="50%" align="center">
			<?php echo strtoupper($objSatuanKerja->wilIbukota.', '.$app->MySQLDateToIndonesia($app->NormalDateToMySQL($arr['tanggal']))); ?><br>
			<?php echo strtoupper($objPejabat->pnJabatan); ?><br>
			<br><br><br>
			<b><u><?php echo strtoupper($objPejabat->pnNama); ?></u></b><br>
			NIP. <?php echo $objPejabat->pnNIP; ?>
		</td>
	</tr>
	</table>
<?php
		}
	}
	
	static function resultrealisasiLaporan($arr) {
		global $app;
		
		switch ($app->task) {
			case 'exportrealisasi':
			case 'printrealisasi':
				$printexport = true;
				$table = 'class="the-data" width="100%"';
				break;
			case 'pdfrealisasi':
				$printexport = true;
				$table = 'border="1" cellpadding="2" cellspacing="0" width="100%"';
				break;
			default:
				$printexport = false;
				$table = 'class="dataTable"';
		}
?>
	<table <?php echo $table; ?>>
	<thead>
		<tr>
			<th rowspan="3">No.</th>
			<th rowspan="3">Jenis Perizinan</th>
			<th colspan="24" align="center">Bulan</th>
		</tr>
		<tr>
			<th colspan="2">Januari</th>
			<th colspan="2">Februari</th>
			<th colspan="2">Maret</th>
			<th colspan="2">April</th>
			<th colspan="2">Mei</th>
			<th colspan="2">Juni</th>
			<th colspan="2">Juli</th>
			<th colspan="2">Agustus</th>
			<th colspan="2">September</th>
			<th colspan="2">Oktober</th>
			<th colspan="2">November</th>
			<th colspan="2">Desember</th>
		</tr>
		<tr>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
			<th>Bulan Ini</th>
			<th>Jumlah s/d Bulan Ini</th>
		</tr>
		<tr>
<?php 
				if ($printexport) {
					for ($i=1;$i<=26;$i++) {
						echo '<th align="center">'.$i.'</th>';
					}
				}
?>
		</tr>
	</thead>
	<tbody>
<?php 
		if ($arr['tahun'] == date('Y')) {
			$bulan = date('n');
		} else {
			$bulan = 12;
		}
		
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo '<tr>';
				echo '<td>'.$i.'</td>';
				echo '<td>'.$v['nama'].'</td>';
				for ($j=1; $j<=12; $j++) {
					if (isset($v[$j])) {
						echo '<td align="right">'.$v[$j].'</td>';
					} else {
						if ($j <= $bulan) {
							echo '<td align="right">0</td>';
						} else {
							echo '<td align="right">&nbsp;</td>';
						}
					}
					$total = 0;
					for ($k=1;$k<=$j;$k++) {
						if (isset($v[$k])) {
							$total += $v[$k];
						} else {
							$total += 0;
						}
					}
					if ($j <= $bulan) {
						echo '<td align="right">'.$total.'</td>';
					} else {
						echo '<td align="right">&nbsp;</td>';
					}
				}
				
				echo "</tr>\n";
				
				$i++;
			}
		}
?>
		<tr style="background:#BFBFBF;">
			<td></td>
			<td><b>TOTAL</b></td>
<?php 
		$v = $arr['total'];
		
		for ($j=1; $j<=12; $j++) {
			if (isset($v[$j])) {
				echo '<td align="right">'.$v[$j].'</td>';
			} else {
				if ($j <= $bulan) {
					echo '<td align="right">0</td>';
				} else {
					echo '<td align="right">&nbsp;</td>';
				}
			}
			$total = 0;
			for ($k=1;$k<=$j;$k++) {
				if (isset($v[$k])) {
					$total += $v[$k];
				} else {
					$total += 0;
				}
			}
			if ($j <= $bulan) {
				echo '<td align="right">'.$total.'</td>';
			} else {
				echo '<td align="right">&nbsp;</td>';
			}
		}
?>
		</tr>
	</tbody>
	</table>
<?php
	}
	
	static function masaberlakuLaporan($arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=laporan&task=masaberlaku"><img src="images/icons/printer.png" border="0" /> Masa Berlaku Surat Izin</a></h1>
	</div>
	<div class="halamanTengah">
		<form id="myForm" action="index2.php?act=laporan&task=masaberlaku" target="_self" method="POST">
		<table border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><b>Jenis Surat Izin</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<select class="box" id="jsiKode" name="jsiKode">
					<option value="">--Pilih--</option>
<?php 
		$jenissuratizin = $app->queryArrayOfObjects("SELECT *, jsiID AS id FROM jenissuratizin WHERE jsiTampil=1 GROUP BY jsiKode ORDER BY jsiNama");
		foreach ($jenissuratizin as $v) {
			echo '<option value="'.$v->jsiKode.'"';
			if ($v->jsiKode == $arr['jsiKode']) {
				echo ' selected';
			}
			echo '>'.$v->jsiNama.'</option>';
		}
?>
				</select>
			</td>
		</tr>
		<tr>
			<td><b>Periode</b></td><td>&nbsp;:&nbsp;</td>
			<td>
				<select class="box" id="bulan" name="bulan">
<?php 
		foreach (array(1=>'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember') as $k=>$v) {
			echo '<option value="'.$k.'"';
			if ($k == $arr['bulan']) {
				echo ' selected';
			}
			echo '>'.$v.'</option>';
		}
?>
				</select>
				<select class="box" id="tahun" name="tahun">
<?php 
		for ($i=2000;$i<=date('Y')+10;$i++) {
			echo '<option value="'.$i.'"';
			if ($i == $arr['tahun']) {
				echo ' selected';
			}
			echo '>'.$i.'</option>';
		}
?>
				</select>
			</td>
		</tr>
		<tr>
			<td></td><td></td>
			<td>
				<input type="button" id="btnShow" class="button-link inline dark-blue" value="Tampilkan">
				<input type="button" id="btnPreview" class="button-link inline dark-blue" value="Preview Cetak">
				<input type="button" id="btnExport" class="button-link inline dark-blue" value="Ekspor ke MS.Excel">
				<br><br>
			</td>
		</tr>
		</table>
		</form>
		<?php Laporan_View::resultmasaberlakuLaporan($arr); ?>
	</div>
	<div class="halamanBawah"></div>
	<script type="text/javascript" src="acts/laporan/js/masaberlaku.laporan.js?d=<?php echo date('dmYHis'); ?>"></script>
<?php
	}

	static function printmasaberlakuLaporan($arr) {
		global $app;
		
		$bulan = array(1=>'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
		
		if ($arr['jsiKode'] != '') {
			$objJenisSuratIzin = $app->queryObject("SELECT * FROM jenissuratizin WHERE jsiKode='".$arr['jsiKode']."'");
		} else {
			$objJenisSuratIzin = new stdClass();
			$objJenisSuratIzin->jsiNama = '';
		}
?>
	<h1>MASA BERLAKU SURAT IZIN<br><?php echo strtoupper($objJenisSuratIzin->jsiNama); ?></h1>
	<div><b>Periode : <?php echo $bulan[$arr['bulan']].' '.$arr['tahun']; ?></b></div>
<?php
		Laporan_View::resultmasaberlakuLaporan($arr);
	}

	static function resultmasaberlakuLaporan($arr) {
		global $app;
		
		switch ($app->task) {
			case 'exportmasaberlaku':
			case 'printmasaberlaku':
				$table = 'class="the-data" width="100%"';
				break;
			default:
				$table = 'class="dataTable"';
		}
		
		if (count($arr['data']) > 0) {
			foreach ($arr['data'] as $kecamatan=>$suratizin) {
?>
	<div><b>Kecamatan : <?php echo $kecamatan; ?></b></div>
	<table <?php echo $table; ?>>
	<thead>
		<tr>
			<th>No.</th>
			<th>Nomor SK</th>
			<th style="min-width:60px;">Tanggal SK</th>
			<th style="min-width:60px;">Masa Berlaku/Daftar Ulang</th>
			<th>Jenis Perizinan</th>
			<th>Atas Nama</th>
			<th>Perusahaan</th>
			<th>Lokasi</th>
			<th>Sifat</th>
			<th>Keterangan</th>
		</tr>
	</thead>
	<tbody>
<?php 
				if (count($suratizin) > 0) {
					$i = 1;
					
					foreach ($suratizin as $v) {
						echo '<tr>';
						echo '<td align="right">'.$i.'</td>';
						echo '<td>'.$v->no.'</td>';
						echo '<td align="center">'.$app->MySQLDateToNormal($v->tglpengesahan).'</td>';
						
						if ($v->jsiMasaBerlaku == 0) {
							if ($v->jsiMasaDaftarUlang == 0) {
								echo '<td align="center">Selama Ada</td>';
							} else {
								if ($v->tgldaftarulang != '0000-00-00') {
									echo '<td align="center">'.$app->MySQLDateToNormal($v->tgldaftarulang).'</td>';
								} else {
									echo '<td align="center">'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
								}
							}
						} else {
							if ($v->jsiMasaDaftarUlang == 0) {
								if ($v->tglberlaku != '0000-00-00') {
									echo '<td align="center">'.$app->MySQLDateToNormal($v->tglberlaku).'</td>';
								} else {
									echo '<td align="center">'.$v->jsiMasaBerlaku.' Tahun semenjak Tgl. Pengesahan</td>';
								}
							} else {
								if ($v->tgldaftarulang != '0000-00-00') {
									echo '<td align="center">'.$app->MySQLDateToNormal($v->tgldaftarulang).'</td>';
								} else {
									echo '<td align="center">'.$v->jsiMasaDaftarUlang.' Tahun semenjak Tgl. Pengesahan</td>';
								}
							}
						}
						
						echo '<td>'.$v->jsiNama.'</td>';
						echo '<td>'.$v->mhnNama.'</td>';
						echo '<td>'.$v->perNama.'</td>';
						echo '<td>'.$v->perAlamat.', '.$v->wilNama.'</td>';
						echo '<td>'.$v->dmhnTipe.'</td>';
						echo '<td>'.$v->keterangan.'</td>';
						echo "</tr>\n";
						
						$i++;
					}
				} else {
?>
		<tr><td colspan="10">Tidak ada data</td></tr>
<?php
				}
?>
	</tbody>
	</table>
<?php
			}
		} else {
?>
	<table <?php echo $table; ?>>
	<thead>
		<tr>
			<th>No.</th>
			<th>Nomor SK</th>
			<th>Tanggal SK</th>
			<th>Masa Berlaku</th>
			<th>Jenis Perizinan</th>
			<th>Atas Nama</th>
			<th>Perusahaan</th>
			<th>Lokasi</th>
			<th>Sifat</th>
			<th>Keterangan</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="10">Tidak ada data</td>
		</tr>
	</tbody>
	</table>
<?php
		}
	}
	
	static function pengurusanLaporan($arr) {
		global $app;
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=laporan&task=pengurusan"><img src="images/icons/printer.png" border="0" /> Pengurusan Surat Izin yang telah Melewati Batas Waktu</a></h1>
	</div>
	<div class="halamanTengah">
		<table class="dataTable">
		<thead>
			<tr>
				<th width="40">No.</th>
				<th>No. Pendaftaran</th>
				<th>Tgl. Pendaftaran</th>
				<th>Pemohon</th>
				<th>Perusahaan</th>
				<th>Surat Izin</th>
				<th>Tgl. Target Selesai</th>
			</tr>
		</thead>
		<tbody>
<?php 
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo '<tr>';
				echo '<td align="right">'.$i.'</td>';
				echo '<td>'.$v->mhnNoUrutLengkap.'</td>';
				echo '<td>'.$app->MySQLDateToNormal($v->mhnTgl).'</td>';
				echo '<td>'.$v->mhnNama.'</td>';
				echo '<td>'.$v->perNama.'</td>';
				echo '<td>'.$v->jsiNama.'</td>';
				echo '<td>'.$app->MySQLDateToNormal($v->dmhnTglTargetSelesai).'</td>';
				echo "</tr>\n";
						
				$i++;
			}
		} else {
?>
		<tr><td colspan="7">Tidak ada data</td></tr>
<?php
		}
?>
		</tbody>
		</table>
	</div>
	<div class="halamanBawah"></div>
<?php
	}
	
	static function bebanadministrasiLaporan($arr) {
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=laporan&task=bebanadministrasi"><img src="images/icons/printer.png" border="0" /> Beban Administrasi</a></h1>
	</div>
	<div class="halamanTengah">
		<table class="dataTable">
		<thead>
			<tr>
				<th width="40"align="right">No.</th>
				<th>Pengguna</th>
				<th align="right">Pengurusan Surat Izin yang belum Selesai</th>
			</tr>
		</thead>
		<tbody>
<?php 
		if (count($arr['data']) > 0) {
			$i = 1;
			foreach ($arr['data'] as $v) {
				echo '<tr>';
				echo '<td align="right">'.$i.'</td>';
				echo '<td>'.$v->pnNama.'</td>';
				echo '<td align="right">'.$v->total.'</td>';
				echo "</tr>\n";
						
				$i++;
			}
		} else {
?>
		<tr><td colspan="3">Tidak ada data</td></tr>
<?php
		}
?>
		</tbody>
		</table>
	</div>
	<div class="halamanBawah"></div>
<?php
	}

	static function viewLaporan() {
?>
	<div class="halamanAtas">
		<h1 class="bTitle"><a class="bTitle" href="index2.php?act=laporan"><img src="images/icons/printer.png" border="0" /> Laporan</a></h1>
	</div>
	<div class="halamanTengah">
		
	</div>
	<div class="halamanBawah"></div>
<?php
	}
}
?>