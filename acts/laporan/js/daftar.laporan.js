$('#btnShow').click(function(){
	$('#myForm').attr('action', 'index2.php?act=laporan&task=daftar');
	$('#myForm').attr('target', '_self');
	$('#myForm').submit();
});

$('#btnPreview').click(function(){
	$('#myForm').attr('action', 'index3.php?act=laporan&task=printdaftar');
	$('#myForm').attr('target', '_blank');
	$('#myForm').submit();
});

$('#btnExport').click(function(){
	$('#myForm').attr('action', 'index3.php?act=laporan&task=exportdaftar');
	$('#myForm').attr('target', '_blank');
	$('#myForm').submit();
});

$('#btnPDF').click(function(){
	$('#myForm').attr('action', 'index3.php?act=laporan&task=pdfdaftar');
	$('#myForm').attr('target', '_blank');
	$('#myForm').submit();
});

$('#kecamatan').autocomplete({
	serviceUrl: CFG_LIVE_PATH + '/index2.php?act=permohonan&task=find&html=0&find=kecamatan',
	onSelect: function(suggestion){
		$('#kecamatanID').val(suggestion.data.id);
		$('#kecamatan').val(suggestion.value.replace(/&gt;/g, '>'));
	}
});