$('#btnShow').click(function(){
	$('#myForm').attr('action', 'index2.php?act=laporan&task=realisasi');
	$('#myForm').attr('target', '_self');
	$('#myForm').submit();
});

$('#btnPreview').click(function(){
	$('#myForm').attr('action', 'index3.php?act=laporan&task=printrealisasi');
	$('#myForm').attr('target', '_blank');
	$('#myForm').submit();
});

$('#btnExport').click(function(){
	$('#myForm').attr('action', 'index3.php?act=laporan&task=exportrealisasi');
	$('#myForm').attr('target', '_blank');
	$('#myForm').submit();
});

$('#btnPDF').click(function(){
	$('#myForm').attr('action', 'index3.php?act=laporan&task=pdfrealisasi');
	$('#myForm').attr('target', '_blank');
	$('#myForm').submit();
});