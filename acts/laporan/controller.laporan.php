<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;
$acl['Pejabat']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Additional check
if ($_SESSION['sesipengguna']->levelAkses != 'Administrator') {
	if (!$_SESSION['sesipengguna']->aksesLaporan) {
		die(CFG_RESTRICTED_ACCESS);
	}
}

//Include file(s)
require_once 'view.laporan.php';

switch ($app->task) {
	case 'exportdaftar':
	case 'pdfdaftar':
	case 'printdaftar':
	case 'daftar':
		daftarLaporan();
		break;
	case 'exportrealisasi':
	case 'pdfrealisasi':
	case 'printrealisasi':
	case 'realisasi':
		realisasiLaporan();
		break;
	case 'exportmasaberlaku':
	case 'printmasaberlaku':
	case 'masaberlaku':
		masaberlakuLaporan();
		break;
	case 'exportpengurusan':
	case 'printpengurusan':
	case 'pengurusan':
		pengurusanLaporan();
		break;
	case 'bebanadministrasi':
		bebanadministrasiLaporan();
		break;
	default:
		viewLaporan();
		break;
}

function getLaporan($arr, $jsiKode) {
	global $app;
	
	$grupID 	= isset($_REQUEST['grupID']) 	? $_REQUEST['grupID'] 			: 0;
	$bulan1 	= isset($_REQUEST['bulan1']) 	? intval($_REQUEST['bulan1']) 	: date('n');
	$bulan2 	= isset($_REQUEST['bulan2']) 	? intval($_REQUEST['bulan2']) 	: date('n');
	$tahun1 	= isset($_REQUEST['tahun1']) 	? intval($_REQUEST['tahun1']) 	: date('Y');
	$tahun2 	= isset($_REQUEST['tahun2']) 	? intval($_REQUEST['tahun2']) 	: date('Y');
	$nama 		= isset($_REQUEST['nama']) 		? $_REQUEST['nama'] 			: '';
	$nama2 		= isset($_REQUEST['nama2']) 	? $_REQUEST['nama2'] 			: '';
	$kecamatan 	= isset($_REQUEST['kecamatan']) ? $_REQUEST['kecamatan'] 		: '';
	if ($kecamatan != '') {
		$kecamatanID 	= isset($_REQUEST['kecamatanID']) ? intval($_REQUEST['kecamatanID']) 		: 0;
	} else {
		$kecamatanID 	= 0;
	}
	$investasi_parameter_bawah 	= isset($_REQUEST['investasi_parameter_bawah']) ? $_REQUEST['investasi_parameter_bawah'] 	: '<';
	$investasi_batas_bawah 		= isset($_REQUEST['investasi_batas_bawah']) 	? $app->MoneyToMySQL($_REQUEST['investasi_batas_bawah']) 		: 0;
	$investasi_parameter_atas 	= isset($_REQUEST['investasi_parameter_atas']) 	? $_REQUEST['investasi_parameter_atas'] 	: '>';
	$investasi_batas_atas 		= isset($_REQUEST['investasi_batas_atas']) 		? $app->MoneyToMySQL($_REQUEST['investasi_batas_atas']) 		: 0;
	$jenisusaha = isset($_REQUEST['jenisusaha']) 	? $_REQUEST['jenisusaha'] 	: '';
	$tanggal 	= isset($_REQUEST['tanggal']) 	? $_REQUEST['tanggal'] 			: date('d-m-Y');
	$pejabat 	= isset($_REQUEST['pejabat']) 	? intval($_REQUEST['pejabat']) 	: 0;
	
	$arrGrupID = array();
	
	if ($grupID > 0) {
		$sql = "SELECT grupID AS id, grup.* FROM grup WHERE grupID='".$grupID."' OR grupParentID='".$grupID."'";
		$arrGrup = $app->queryArrayOfObjects($sql);
		
		if (count($arrGrup) > 0) {
			foreach ($arrGrup as $v) {
				$arrGrupID[$v->grupID] = $v->grupID;
			}
			
			$sql = "SELECT grupID AS id, grup.* FROM grup WHERE grupParentID IN (".implode(',', array_keys($arrGrup)).")";
			$arrSubGrup = $app->queryArrayOfObjects($sql);
			foreach ($arrSubGrup as $v) {
				$arrGrupID[$v->grupID] = $v->grupID;
			}
		}
	}
	/*print_r($arrGrup);
	print_r($arrSubGrup);exit();
	Array ( [28] => stdClass Object ( [id] => 28 [grupID] => 28 [grupParentID] => 0 [grupNama] => Gudang ) ) 
	Array ( )*/
	switch ($jsiKode) {
		case 'situho':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(situhoTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "situho.situhoGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						situhoKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN situho ON dmhnID=situhoDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='situho'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON situhoKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, situhoNo";
			$rs = $app->query($sql);
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->situhoNoLengkap;
				$obj->tglpengesahan = $obj->situhoTglPengesahan;
				$obj->tglberlaku = $obj->situhoTglBerlaku;
				$obj->tgldaftarulang = $obj->situhoTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
		case 'imb':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(imbTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "imb.imbGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						imbKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN imb ON dmhnID=imbDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='imb'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON imbKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, imbNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->imbNoLengkap;
				$obj->tglpengesahan = $obj->imbTglPengesahan;
				$obj->tglberlaku = $obj->imbTglBerlaku;
				$obj->tgldaftarulang = $obj->imbTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
		case 'reklame':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(reklameTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "reklame.reklameGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						reklameKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN reklame ON dmhnID=reklameDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='reklame'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON reklamePemasanganKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, reklameNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->reklameNoLengkap;
				$obj->tglpengesahan = $obj->reklameTglPengesahan;
				$obj->tglberlaku = $obj->reklameTglBerlaku;
				$obj->tgldaftarulang = $obj->reklameTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
		case 'pariwisata':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(pariwisataTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "pariwisata.pariwisataGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						pariwisataKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN pariwisata ON dmhnID=pariwisataDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='pariwisata'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, pariwisataNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->pariwisataNoLengkap;
				$obj->tglpengesahan = $obj->pariwisataTglPengesahan;
				$obj->tglberlaku = $obj->pariwisataTglBerlaku;
				$obj->tgldaftarulang = $obj->pariwisataTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
		case 'pemecahanimb':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(pemecahanimbTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "pemecahanimb.pemecahanimbGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						pemecahanimbKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN pemecahanimb ON dmhnID=pemecahanimbDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='pemecahanimb'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, pemecahanimbNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = sprintf('%02d', $obj->pemecahanimbNo).'/142/'.CFG_COMPANY_SHORT_NAME.'/'.$app->getRoman(intval(substr($obj->pemecahanimbTglPengesahan,5,2))).'/'.substr($obj->pemecahanimbTglPengesahan,0,4);
				$obj->tglpengesahan = $obj->pemecahanimbTglPengesahan;
				$obj->tglberlaku = $obj->pemecahanimbTglBerlaku;
				$obj->tgldaftarulang = $obj->pemecahanimbTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
		case 'situ':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(situTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "situ.situGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						situKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama,
						wilayah4.wilID AS parentID2,
						wilayah4.wilNama AS parentNama2
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN situ ON dmhnID=situDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='situ'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON situ.situKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					LEFT JOIN wilayah AS wilayah3 ON perKelurahanID=wilayah3.wilID
					LEFT JOIN wilayah AS wilayah4 ON wilayah3.wilParentID=wilayah4.wilID
					".$where."
					ORDER BY parentNama, situNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->situNoLengkap;
				$obj->tglpengesahan = $obj->situTglPengesahan;
				$obj->tglberlaku = $obj->situTglBerlaku;
				$obj->tgldaftarulang = $obj->situTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				//NOTE: specific jika situ alamat dan atau kelurahan tidak diisi
				if ($obj->parentNama != '') {
					$arr[$obj->parentNama][] = $obj;
				} else {
					$arr[$obj->parentNama2][] = $obj;
				}
			}
			break;
		case 'siup':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(siupTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if ($jenisusaha != '') {
				$filter[] = "siupJenisBarangJasa LIKE '%".$jenisusaha."%'";
			}
			if (!($investasi_batas_bawah == 0 && $investasi_batas_atas == 0)) {
				$filter[] = "siupModal".$investasi_parameter_bawah."'".$investasi_batas_bawah."' AND siupModal".$investasi_parameter_atas."'".$investasi_batas_atas."' ";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "siup.siupGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						siupKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN siup ON dmhnID=siupDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='siup'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, siupNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->siupNoLengkap;
				$obj->tglpengesahan = $obj->siupTglPengesahan;
				$obj->tglberlaku = $obj->siupTglBerlaku;
				$obj->tgldaftarulang = $obj->siupTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->nilaiinvestasi = $obj->siupModal;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
		case 'tdi':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(tdiTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "tdi.tdiGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						tdiKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN tdi ON dmhnID=tdiDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='tdi'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, tdiNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->tdiNoLengkap;
				$obj->tglpengesahan = $obj->tdiTglPengesahan;
				$obj->tglberlaku = $obj->tdiTglBerlaku;
				$obj->tgldaftarulang = $obj->tdiTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
		case 'iui':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(iuiTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "iui.iuiGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//Query
			$sql = "SELECT
						*,
						iuiKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN iui ON dmhnID=iuiDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='iui'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, iuiNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->iuiNoLengkap;
				$obj->tglpengesahan = $obj->iuiTglPengesahan;
				$obj->tglberlaku = $obj->iuiTglBerlaku;
				$obj->tgldaftarulang = $obj->iuiTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
		case 'tdp':
			$filter = array();
			//NOTE: kenapa 31? bagaimana dengan bulan dengan tanggal sampai dengan 28, 29, atau 30
			$filter[] = "(tdpTglPengesahan BETWEEN '".$tahun1."-".sprintf('%02d', $bulan1)."-01"."' AND '".$tahun2."-".sprintf('%02d', $bulan2)."-31"."')";
			if ($nama != '') {
				$filter[] = "mhnNama LIKE '%".$nama."%'";
			}
			if ($nama2 != '') {
				$filter[] = "perNama LIKE '%".$nama2."%'";
			}
			if ($kecamatanID > 0) {
				$filter[] = "wilayah2.wilID='".$kecamatanID."'";
			}
			if (count($arrGrupID) > 0) {
				$filter[] = "tdp.tdpGrupID IN (".implode(',', $arrGrupID).")";
			}
				
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
				
			//030316: tdp mengambil dari tdpKelurahanID, bukan perKelurahanID
			//		  begitu pula tdpAlamat, bukan perAlamat
			//Query
			$sql = "SELECT
						*,
						tdpKeterangan AS keterangan,
						wilayah2.wilID AS parentID,
						wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN tdp ON dmhnID=tdpDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID AND jsiKode='tdp'
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON tdpKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where."
					ORDER BY parentNama, tdpNo";
			$rs = $app->query($sql);
				
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = '0416'.$app->queryField("SELECT bperNilai FROM bentukperusahaan WHERE bperID='".$obj->tdpBentukBadan."'").$obj->tdpKluiKodePendek.sprintf('%05d', $obj->tdpNo);
				$obj->tglpengesahan = $obj->tdpTglPengesahan;
				$obj->tglberlaku = $obj->tdpTglBerlaku;
				$obj->tgldaftarulang = $obj->tdpTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
	
				$arr[$obj->parentNama][] = $obj;
			}
			break;
	}
	
	return $arr;
}

function daftarLaporan() {
	global $app;
	
	$arr = array();
	
	$jsiKode 	= isset($_REQUEST['jsiKode']) 	? $_REQUEST['jsiKode'] 			: '';
	$grupID 	= isset($_REQUEST['grupID']) 	? $_REQUEST['grupID'] 			: 0;
	$bulan1 	= isset($_REQUEST['bulan1']) 	? intval($_REQUEST['bulan1']) 	: date('n');
	$bulan2 	= isset($_REQUEST['bulan2']) 	? intval($_REQUEST['bulan2']) 	: date('n');
	$tahun1 	= isset($_REQUEST['tahun1']) 	? intval($_REQUEST['tahun1']) 	: date('Y');
	$tahun2 	= isset($_REQUEST['tahun2']) 	? intval($_REQUEST['tahun2']) 	: date('Y');
	$nama 		= isset($_REQUEST['nama']) 		? $_REQUEST['nama'] 			: '';
	$nama2 		= isset($_REQUEST['nama2']) 	? $_REQUEST['nama2'] 			: '';
	$kecamatan 	= isset($_REQUEST['kecamatan']) ? $_REQUEST['kecamatan'] 		: '';
	if ($kecamatan != '') {
		$kecamatanID 	= isset($_REQUEST['kecamatanID']) ? intval($_REQUEST['kecamatanID']) 		: 0;
	} else {
		$kecamatanID 	= 0;
	}
	$investasi_parameter_bawah 	= isset($_REQUEST['investasi_parameter_bawah']) ? $_REQUEST['investasi_parameter_bawah'] 	: '<';
	$investasi_batas_bawah 		= isset($_REQUEST['investasi_batas_bawah']) 	? $app->MoneyToMySQL($_REQUEST['investasi_batas_bawah']) 		: 0;
	$investasi_parameter_atas 	= isset($_REQUEST['investasi_parameter_atas']) 	? $_REQUEST['investasi_parameter_atas'] 	: '>';
	$investasi_batas_atas 		= isset($_REQUEST['investasi_batas_atas']) 		? $app->MoneyToMySQL($_REQUEST['investasi_batas_atas']) 		: 0;
	$jenisusaha = isset($_REQUEST['jenisusaha']) 	? $_REQUEST['jenisusaha'] 	: '';
	$tanggal 	= isset($_REQUEST['tanggal']) 	? $_REQUEST['tanggal'] 			: date('d-m-Y');
	$pejabat 	= isset($_REQUEST['pejabat']) 	? intval($_REQUEST['pejabat']) 	: 0;

	$arr['jsiKode'] = $jsiKode;
	$arr['grupID'] = $grupID;
	$arr['bulan1'] 	= $bulan1;
	$arr['bulan2'] 	= $bulan2;
	$arr['tahun1'] 	= $tahun1;
	$arr['tahun2'] 	= $tahun2;
	$arr['nama'] 	= $nama;
	$arr['nama2'] 	= $nama2;
	$arr['kecamatanID'] = $kecamatanID;
	$arr['kecamatan'] 	= $kecamatan;
	$arr['investasi_parameter_bawah'] 	= $investasi_parameter_bawah;
	$arr['investasi_batas_bawah'] 		= $investasi_batas_bawah;
	$arr['investasi_parameter_atas'] 	= $investasi_parameter_atas;
	$arr['investasi_batas_atas'] 		= $investasi_batas_atas;
	$arr['jenisusaha'] 	= $jenisusaha;
	$arr['tanggal'] = $tanggal;
	$arr['pejabat'] = $pejabat;
	$arr['data'] 	= array();
	
	if ($jsiKode != '') {
		$arr['data'] = getLaporan($arr['data'], $jsiKode);
	} else {
		$arr['data'] = getLaporan($arr['data'], 'situho');
		$arr['data'] = getLaporan($arr['data'], 'imb');
		$arr['data'] = getLaporan($arr['data'], 'reklame');
		$arr['data'] = getLaporan($arr['data'], 'pariwisata');
		$arr['data'] = getLaporan($arr['data'], 'pemecahanimb');
		$arr['data'] = getLaporan($arr['data'], 'situ');
		$arr['data'] = getLaporan($arr['data'], 'siup');
		$arr['data'] = getLaporan($arr['data'], 'tdi');
		//$arr['data'] = getLaporan($arr['data'], 'iui');
		$arr['data'] = getLaporan($arr['data'], 'tdp');
	}

	switch ($app->task) {
		case 'exportdaftar':
		case 'printdaftar':
			Laporan_View::printdaftarLaporan($arr);
			break;
		case 'pdfdaftar':
			$objReport 		= new Report('Laporan Daftar Perizinan', 'DaftarPerizinan.pdf');
			//Overwrite default value
			$objReport->pdfOrientation = 'L';
			
			$konfigurasi 	= array();
			$rs2 = $app->query("SELECT * FROM konfigurasi");
			while(($obj2 = mysql_fetch_object($rs2)) == true){
				$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
			}
			
			$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
			//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
			$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
			
			$pdf->SetAuthor(CFG_SITE_NAME);
			$pdf->SetCreator(CFG_SITE_NAME);
			$pdf->SetTitle($objReport->title);
			
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
			$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
			
			if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
				require_once(dirname(__FILE__).'/lang/ind.php');
				
				$languages = array();
				$pdf->setLanguageArray($languages);
			}
			
			$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
			
			$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
			
			/*ob_start();
			$objReport->header($pdf, $konfigurasi);
			$pdf->writeHTML(ob_get_contents());
			ob_clean();*/
			
			//Mempengaruhi selain header
			$pdf->setCellHeightRatio($objReport->cellHeightRatio);
			
			ob_start();
			
			Laporan_View::printdaftarLaporan($arr);
			
			$pdf->writeHTML(ob_get_contents());
			ob_clean();
			
			$pdf->Output($objReport->filename);
			break;
		default:
			Laporan_View::daftarLaporan($arr);	
	}
}

function realisasiLaporan() {
	global $app;
	
	$arr = array();
	
	$tahun 		= isset($_REQUEST['tahun']) 	? intval($_REQUEST['tahun']) 	: date('Y');
	$tanggal 	= isset($_REQUEST['tanggal']) 	? $_REQUEST['tanggal'] 			: date('d-m-Y');
	$pejabat 	= isset($_REQUEST['pejabat']) 	? intval($_REQUEST['pejabat']) 	: 0;
	
	$arr['tahun'] 	= $tahun;
	$arr['tanggal'] = $tanggal;
	$arr['pejabat'] = $pejabat;
	$arr['data'] 	= array();
	$arr['total'] 	= array();
	
	$sql = "SELECT *
			FROM jenissuratizin
			WHERE jsiTampil=1
			GROUP BY jsiKode
			ORDER BY jsiKode";
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$sql = "SELECT MONTH(dmhnTglSelesai) AS bulan, COUNT(*) AS total
				FROM detailpermohonan, jenissuratizin
				WHERE dmhnTelahSelesai=1 AND dmhnJsiID=jsiID AND jsiKode='".$obj->jsiKode."' AND YEAR(dmhnTglSelesai)='".$tahun."'
				GROUP BY jsiKode, bulan";
		$rs2 = $app->query($sql);
		while(($obj2 = mysql_fetch_object($rs2)) == true){
			//051016: FIK arraynya dicek isset dulu
			if (!isset($arr['data'][$obj->jsiID])) {
				$arr['data'][$obj->jsiID] = array();
			}
			$arr['data'][$obj->jsiID]['nama'] = $obj->jsiNama.($obj->jsiSubNama != '' ? ' ('.$obj->jsiSubNama.')' : '');
			$arr['data'][$obj->jsiID][$obj2->bulan] = $obj2->total;
			
			if (!isset($arr['total'][$obj2->bulan])) {
				$arr['total'][$obj2->bulan] = 0;
			}
			$arr['total'][$obj2->bulan] += $obj2->total;
		}
	}

	switch ($app->task) {
		case 'exportrealisasi':
		case 'printrealisasi':
			Laporan_View::printrealisasiLaporan($arr);
			break;
		case 'pdfrealisasi':
			$objReport 		= new Report('Laporan Realisasi Pelayanan Izin', 'RealisasiPelayananIzin.pdf');
			//Overwrite default value
			$objReport->pdfOrientation = 'L';
			
			$konfigurasi 	= array();
			$rs2 = $app->query("SELECT * FROM konfigurasi");
			while(($obj2 = mysql_fetch_object($rs2)) == true){
				$konfigurasi[$obj2->konfKey] = $obj2->konfValue;
			}
			
			$pdf 			= new TCPDF($objReport->pdfOrientation, PDF_UNIT, array($objReport->pdfWidth,$objReport->pdfHeight));
			//dpi bernilai 72, PDF_UNIT bernilai mm, sehingga digunakan konversi dari mm ke px untuk mendapatkan content width
			$objReport->width = (($pdf->GetPageWidth() - $objReport->marginLeft - $objReport->marginRight) * 72) / 25.4;
			
			$pdf->SetAuthor(CFG_SITE_NAME);
			$pdf->SetCreator(CFG_SITE_NAME);
			$pdf->SetTitle($objReport->title);
			
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			$pdf->SetMargins($objReport->marginLeft, $objReport->marginTop, $objReport->marginRight, 10);
			$pdf->SetAutoPageBreak(TRUE, $objReport->marginBottom);
			
			if (@file_exists(dirname(__FILE__).'/lang/ind.php')) {
				require_once(dirname(__FILE__).'/lang/ind.php');
				
				$languages = array();
				$pdf->setLanguageArray($languages);
			}
			
			$pdf->SetFont($objReport->fontFamily, $objReport->fontStyle, $objReport->fontSize);
			
			$pdf->AddPage($objReport->pdfOrientation, array($objReport->pageWidth,$objReport->pageHeight));
			
			/*ob_start();
			$objReport->header($pdf, $konfigurasi);
			$pdf->writeHTML(ob_get_contents());
			ob_clean();*/
			
			//Mempengaruhi selain header
			$pdf->setCellHeightRatio($objReport->cellHeightRatio);
			
			ob_start();
			
			Laporan_View::printrealisasiLaporan($arr);
			
			$pdf->writeHTML(ob_get_contents());
			ob_clean();
			
			$pdf->Output($objReport->filename);
			break;
		default:
			Laporan_View::realisasiLaporan($arr);
	}
}

function masaberlakuLaporan() {
	global $app;
	
	$arr = array();
	
	$jsiKode 	= isset($_REQUEST['jsiKode']) 	? $_REQUEST['jsiKode'] 			: '';
	$bulan 		= isset($_REQUEST['bulan']) 	? intval($_REQUEST['bulan']) 	: date('n');
	$tahun 		= isset($_REQUEST['tahun']) 	? intval($_REQUEST['tahun']) 	: date('Y');
	
	$arr['jsiKode'] = $jsiKode;
	$arr['bulan'] 	= $bulan;
	$arr['tahun'] 	= $tahun;
	$arr['data'] 	= array();
	
	switch ($jsiKode) {
		case 'situho':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(situhoTglDaftarUlang)='".$bulan."' AND YEAR(situhoTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(situhoTglBerlaku)='".$bulan."' AND YEAR(situhoTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(situhoTglDaftarUlang)='".$bulan."' AND YEAR(situhoTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='situho'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN situho ON dmhnID=situhoDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, situhoTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->situhoNoLengkap;
				$obj->tglpengesahan = $obj->situhoTglPengesahan;
				$obj->tglberlaku = $obj->situhoTglBerlaku;
				$obj->tgldaftarulang = $obj->situhoTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->situhoKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'imb':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(imbTglDaftarUlang)='".$bulan."' AND YEAR(imbTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(imbTglBerlaku)='".$bulan."' AND YEAR(imbTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(imbTglDaftarUlang)='".$bulan."' AND YEAR(imbTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='imb'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN imb ON dmhnID=imbDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, imbTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->imbNoLengkap;
				$obj->tglpengesahan = $obj->imbTglPengesahan;
				$obj->tglberlaku = $obj->imbTglBerlaku;
				$obj->tgldaftarulang = $obj->imbTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->imbKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'reklame':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(reklameTglDaftarUlang)='".$bulan."' AND YEAR(reklameTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(reklameTglBerlaku)='".$bulan."' AND YEAR(reklameTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(reklameTglDaftarUlang)='".$bulan."' AND YEAR(reklameTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='reklame'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN reklame ON dmhnID=reklameDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, reklameTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->reklameNoLengkap;
				$obj->tglpengesahan = $obj->reklameTglPengesahan;
				$obj->tglberlaku = $obj->reklameTglBerlaku;
				$obj->tgldaftarulang = $obj->reklameTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->reklameKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'pariwisata':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(pariwisataTglDaftarUlang)='".$bulan."' AND YEAR(pariwisataTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(pariwisataTglBerlaku)='".$bulan."' AND YEAR(pariwisataTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(pariwisataTglDaftarUlang)='".$bulan."' AND YEAR(pariwisataTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='pariwisata'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN pariwisata ON dmhnID=pariwisataDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, pariwisataTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->pariwisataNoLengkap;
				$obj->tglpengesahan = $obj->pariwisataTglPengesahan;
				$obj->tglberlaku = $obj->pariwisataTglBerlaku;
				$obj->tgldaftarulang = $obj->pariwisataTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->pariwisataKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'pemecahanimb':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(pemecahanimbTglDaftarUlang)='".$bulan."' AND YEAR(pemecahanimbTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(pemecahanimbTglBerlaku)='".$bulan."' AND YEAR(pemecahanimbTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(pemecahanimbTglDaftarUlang)='".$bulan."' AND YEAR(pemecahanimbTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='pemecahanimb'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN pemecahanimb ON dmhnID=pemecahanimbDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, pemecahanimbTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->pemecahanimbNoLengkap;
				$obj->tglpengesahan = $obj->pemecahanimbTglPengesahan;
				$obj->tglberlaku = $obj->pemecahanimbTglBerlaku;
				$obj->tgldaftarulang = $obj->pemecahanimbTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->pemecahanimbKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'situ':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(situTglDaftarUlang)='".$bulan."' AND YEAR(situTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(situTglBerlaku)='".$bulan."' AND YEAR(situTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(situTglDaftarUlang)='".$bulan."' AND YEAR(situTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='situ'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN situ ON dmhnID=situDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, situTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->situNoLengkap;
				$obj->tglpengesahan = $obj->situTglPengesahan;
				$obj->tglberlaku = $obj->situTglBerlaku;
				$obj->tgldaftarulang = $obj->situTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->situKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'siup':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(siupTglDaftarUlang)='".$bulan."' AND YEAR(siupTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(siupTglBerlaku)='".$bulan."' AND YEAR(siupTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(siupTglDaftarUlang)='".$bulan."' AND YEAR(siupTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='siup'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN siup ON dmhnID=siupDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, siupTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->siupNoLengkap;
				$obj->tglpengesahan = $obj->siupTglPengesahan;
				$obj->tglberlaku = $obj->siupTglBerlaku;
				$obj->tgldaftarulang = $obj->siupTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->siupKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'iui':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(tdiTglDaftarUlang)='".$bulan."' AND YEAR(tdiTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(tdiTglBerlaku)='".$bulan."' AND YEAR(tdiTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(tdiTglDaftarUlang)='".$bulan."' AND YEAR(tdiTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='tdi'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN tdi ON dmhnID=tdiDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, tdiTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->tdiNoLengkap;
				$obj->tglpengesahan = $obj->tdiTglPengesahan;
				$obj->tglberlaku = $obj->tdiTglBerlaku;
				$obj->tgldaftarulang = $obj->tdiTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->tdiKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'tdi':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(iuiTglDaftarUlang)='".$bulan."' AND YEAR(iuiTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(iuiTglBerlaku)='".$bulan."' AND YEAR(iuiTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(iuiTglDaftarUlang)='".$bulan."' AND YEAR(iuiTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='iui'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN iui ON dmhnID=iuiDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, iuiTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->iuiNoLengkap;
				$obj->tglpengesahan = $obj->iuiTglPengesahan;
				$obj->tglberlaku = $obj->iuiTglBerlaku;
				$obj->tgldaftarulang = $obj->iuiTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->iuiKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		case 'tdp':
			$filter = array();
			$filter[] = "((jsiMasaBerlaku=0 AND jsiMasaDaftarUlang>0 AND MONTH(tdpTglDaftarUlang)='".$bulan."' AND YEAR(tdpTglDaftarUlang)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang=0 AND MONTH(tdpTglBerlaku)='".$bulan."' AND YEAR(tdpTglBerlaku)='".$tahun."') OR 
						  (jsiMasaBerlaku>0 AND jsiMasaDaftarUlang>0 AND MONTH(tdpTglDaftarUlang)='".$bulan."' AND YEAR(tdpTglDaftarUlang)='".$tahun."'))";
			$filter[] = "jsiKode='tdp'";
			
			$where = "";
			if (count($filter) > 0) {
				$where = "WHERE ".implode(' AND ', $filter);
			}
			
			//Query
			$sql = "SELECT *, wilayah2.wilID AS parentID, wilayah2.wilNama AS parentNama
					FROM detailpermohonan
					LEFT JOIN permohonan ON dmhnMhnID=mhnID
					LEFT JOIN tdp ON dmhnID=tdpDmhnID
					LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
					LEFT JOIN perusahaan ON dmhnPerID=perID
					LEFT JOIN wilayah ON perKelurahanID=wilayah.wilID
					LEFT JOIN wilayah AS wilayah2 ON wilayah.wilParentID=wilayah2.wilID
					".$where." 
					ORDER BY parentNama, tdpTglBerlaku";
			$rs = $app->query($sql);
			
			while(($obj = mysql_fetch_object($rs)) == true){
				$obj->no = $obj->tdpNoLengkap;
				$obj->tglpengesahan = $obj->tdpTglPengesahan;
				$obj->tglberlaku = $obj->tdpTglBerlaku;
				$obj->tgldaftarulang = $obj->tdpTglDaftarUlang;
				$obj->namapemilik = $obj->mhnNama;
				$obj->keterangan = $obj->tdpKeterangan;
				
				$arr['data'][$obj->parentNama][] = $obj;
			}
			break;
		default:
	}

	switch ($app->task) {
		case 'exportmasaberlaku':
		case 'printmasaberlaku':
			Laporan_View::printmasaberlakuLaporan($arr);
			break;
		default:
			Laporan_View::masaberlakuLaporan($arr);
	}
}

function pengurusanLaporan() {
	global $app;
	
	$arr = array();
	
	$arr['data'] = array();
	
	//Query
	$sql = "SELECT *
			FROM detailpermohonan
			LEFT JOIN permohonan ON dmhnMhnID=mhnID
			LEFT JOIN perusahaan ON dmhnPerID=perID
			LEFT JOIN jenissuratizin ON dmhnJsiID=jsiID
			WHERE dmhnTelahSelesai=0 AND dmhnTglTargetSelesai<'".date('Y-m-d')."' AND dmhnTglTargetSelesai<>'0000-00-00'
			ORDER BY mhnTgl";
	$rs = $app->query($sql);
			
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	Laporan_View::pengurusanLaporan($arr);
}

function bebanadministrasiLaporan() {
	global $app;
	
	$arr = array();
	
	$arr['data'] = array();
			
	//Query
	$sql = "SELECT *, (SELECT COUNT(*) AS total FROM detailpermohonan WHERE dmhnDiadministrasikanOleh=pnID AND dmhnDikembalikan=0 AND dmhnTelahSelesai=0) AS total
			FROM pengguna
			WHERE pnLevelAkses='Petugas Administrasi'
			ORDER BY pnNama";
	$rs = $app->query($sql);
			
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	Laporan_View::bebanadministrasiLaporan($arr);
}

function viewLaporan() {
	Laporan_View::viewLaporan();
}
?>