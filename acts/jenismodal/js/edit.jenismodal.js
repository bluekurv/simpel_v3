//Variables
var form = $("#myForm");
var jmodNama = $("#jmodNama");

//Functions
function validateForm() {
	var doSubmit = true;
	
	if (this.id == 'jmodNama' || this.id === undefined) {
		if (jmodNama.val().length == 0){
			doSubmit = false;
			jmodNama.addClass("error");		
		} else {
			jmodNama.removeClass("error");
		}
	}
	
	return doSubmit;	
}

function submitForm() {
	return validateForm();
}

//Events
jmodNama.blur(validateForm);

jmodNama.keyup(validateForm);

form.submit(submitForm);

//Set focus
jmodNama.focus();