<?php
//Check access
if (!defined('PG_PARENT')) {
	die(CFG_RESTRICTED_ACCESS);
}

global $app;

//Implementing access control
$acl = array();
$acl['Administrator']['all'] = true;

if (!$app->checkACL($acl)) {
	die(CFG_RESTRICTED_ACCESS);
}

//Include file(s)
require_once 'model.jenismodal.php';
require_once 'view.jenismodal.php';

switch ($app->task) {
	case 'add':
	case 'edit':
		editJenisModal($app->id);
		break;
	case 'delete':
		deleteJenisModal($app->id);
		break;
	case 'save':
		saveJenisModal();
		break;
	default:
		viewJenisModal(true, '');
		break;
}

function deleteJenisModal($id) {
	global $app;
	
	//Get object
	$objJenisModal = $app->queryObject("SELECT * FROM jenismodal WHERE jmodID='".$id."'");
	if (!$objJenisModal) {
		$app->showPageError();
		exit();
	}
	
	//Check dependencies
	$doDelete = true;
	$msg = array();
	
	//Query
	if ($doDelete) {
		$app->query("DELETE FROM jenismodal WHERE jmodID='".$id."'");
		
		$success = true;
		$msg = 'Jenis Modal "'.$objJenisModal->jmodNama.'" berhasil dihapus';
		
		$app->writeLog(EV_INFORMASI, 'jenismodal', $objJenisModal->jmodID, $objJenisModal->jmodNama, $msg);
	} else {
		$success = false;
		$msg = 'Jenis Modal "'.$objJenisModal->jmodNama.'" tidak berhasil dihapus karena:<br/>'.implode('<br/>', $msg);
	}
	
	viewJenisModal($success, $msg);
}

function editJenisModal($id) {
	global $app;
	
	//Query
	if ($id > 0) {
		$objJenisModal = $app->queryObject("SELECT * FROM jenismodal WHERE jmodID='".$id."'");
		if (!$objJenisModal) {
			$app->showPageError();
			exit();
		}
	} else {
		$objJenisModal = new JenisModal_Model();
	}
	
	JenisModal_View::editJenisModal(true, '', $objJenisModal);
}

function saveJenisModal() {
	global $app;
	
	//Create object
	$objJenisModal = new JenisModal_Model();
	$app->bind($objJenisModal);
	
	//Modify object (if necessary)
	
	//Validate object (if necessary)
	$doSave = true;
	$msg = array();
	
	if ($objJenisModal->jmodNama == '') {
		$doSave = false;
		$msg[] = '- Nama belum diisi';
	}
	
	$isExist = $app->isExist("SELECT jmodID AS id FROM jenismodal WHERE jmodNama='".$objJenisModal->jmodNama."'", $objJenisModal->jmodID);
	if (!$isExist) {
		$doSave = false;
		$msg[] = '- Nama "'.$objJenisModal->jmodNama.'" sudah ada';
	}
	
	//Query
	if ($doSave) {
		//Insert or update?
		$doInsert = ($objJenisModal->jmodID == 0) ? true : false;
		
		if ($doInsert) {
			$sql = $app->createSQLforInsert($objJenisModal);
		} else {
			$sql = $app->createSQLforUpdate($objJenisModal);
		}
		$app->query($sql);
		
		if ($doInsert) {
			$objJenisModal->jmodID = $app->queryField("SELECT LAST_INSERT_ID() FROM jenismodal");
			
			$success = true;
			$msg = 'Jenis Modal "'.$objJenisModal->jmodNama.'" berhasil ditambah';
			
			$app->writeLog(EV_INFORMASI, 'jenismodal', $objJenisModal->jmodID, $objJenisModal->jmodNama, $msg);
		} else {
			$success = true;
			$msg = 'Jenis Modal "'.$objJenisModal->jmodNama.'" berhasil diubah';
			
			$app->writeLog(EV_INFORMASI, 'jenismodal', $objJenisModal->jmodID, $objJenisModal->jmodNama, $msg);
		}
	} else {
		$success = false;
		$msg = 'Jenis Modal "'.$objJenisModal->jmodNama.'" tidak berhasil disimpan karena:<br/>'.implode('<br/>', $msg);
	}
	
	if ($success) {
		viewJenisModal($success, $msg);
	} else {
		JenisModal_View::editJenisModal($success, $msg, $objJenisModal);
	}
}

function viewJenisModal($success, $msg) {
	global $app;
	
	//Get variable(s)
	$page 		= $app->pageVar('jenismodal', 'page', 1, 'intval');
	$limit 		= 20;
	$start 		= ($page - 1) * $limit;
	$sort		= $app->pageVar('jenismodal', 'sort', 'jmodNama');
	$dir		= $app->pageVar('jenismodal', 'dir', 'asc');
	
	//Add filter(s) and it's default value
	$default = array(
		'nama' => ''
	);
	
	$nama 		= $app->pageVar('jenismodal', 'filternama', $default['nama'], 'strval');
	
	$filter = array();
	if ($nama != $default['nama']) {
		$filter[] = "jmodNama LIKE '%".$nama."%'";
	}
	
	$where = "";
	if (count($filter) > 0) {
		$where = "WHERE ".implode(' AND ', $filter);
	}
	
	$arr = array(
		'data' => array(), 
		'filter' => array(
			'nama' => $nama
		), 
		'default' => $default,
		'sort' => $sort,
		'dir' => $dir,
		'page' => 0, 
		'totalData' => 0, 
		'startData' => 0, 
		'endData' => 0, 
		'totalPage' => 0
	);
	
	//Query
	$count_sql = "SELECT COUNT(*) AS total
				  FROM jenismodal
				  ".$where;
	$totaldata = $app->queryField($count_sql);
	$totalpage = ceil($totaldata / $limit);
	
	if (($page > $totalpage) && ($totalpage <> 0)) {
		$page = $totalpage;
		$start = ($page - 1) * $limit;
	}
	
	$startdata = ($start + 1 > $totaldata) ? $totaldata : $start + 1;
	$enddata = ($start + $limit > $totaldata) ? $totaldata : $start + $limit;
	
	//Query
	$sql = "SELECT *
			FROM jenismodal
			".$where." 
			ORDER BY ".$sort." ".$dir." 
			LIMIT ".$start.", ".$limit;
	$rs = $app->query($sql);
	
	while(($obj = mysql_fetch_object($rs)) == true){
		$arr['data'][] = $obj;
	}
	
	$arr['page'] 	  = $page;
	$arr['totalPage'] = $totalpage;
	$arr['totalData'] = $totaldata;
	$arr['startData'] = $startdata;
	$arr['endData']   = $enddata;
	
	JenisModal_View::viewJenisModal($success, $msg, $arr);
}
?>