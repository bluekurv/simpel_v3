<?php
//Set flag for parent file
define('PG_PARENT', 1);

//Include file(s)
require_once 'configuration.php';
require_once 'system.php';
if (CFG_GESHI) {
	require_once 'libraries/geshi.php';
}
require_once 'libraries/tcpdf/tcpdf.php';

//Create application
$app = new App();
$app->connect(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);
$app->init();
$app->getVars();

//Check access
if ($_SESSION['sesipengguna']->ID == 0) { 
	header('Location:index.php?login=-1');
	//NOTE: There is another header Location in controller.pengguna.php 
	//Parameter 'replace' indicates whether the header should replace previous or add a second header. 
	//Default is TRUE (will replace). FALSE (allows multiple headers of the same type)	
}

//Hack parameter(s) used by change password
if ($app->act != 'pengguna' && $app->task != 'logout' && $app->task != 'login') {
	if ($_SESSION['sesipengguna']->defaultPassword) {
		$app->act = 'pengguna';
		$app->task = 'editpassword';
	}
}

//Start output buffering
ob_start();

if ($app->html == 0) {
	if ($app->detail == '') {
		//Workaround for : Unsafe use of variable in call include()/require()
		//$file = "acts/".$act."/controller.".$act.".php";
		define('PG_PATH', CFG_ABSOLUTE_PATH."/acts/".$app->act."/controller.".$app->act.".php");
	} else {
		//Workaround for : Unsafe use of variable in call include()/require()
		//$file = "acts/".$act."/detail/".$detail."/controller.".$detail.".php";
		define('PG_PATH', CFG_ABSOLUTE_PATH."/acts/".$app->act."/detail/".$app->detail."/controller.".$app->detail.".php");
	}
	
	if (file_exists(PG_PATH)) {
		require_once(PG_PATH);
	} else {
		$app->showPageError();
	}
} else {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<title><?php echo CFG_SITE_NAME; ?></title>
	
	<link rel="shortcut icon" href="images/favicon.ico" />
	<link rel="icon" href="images/favicon.ico" />
	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="css/themes/base/jquery.ui.all.css" />
	<link rel="stylesheet" type="text/css" href="js/jqplot/jquery.jqplot.min.css" />
	<link rel="stylesheet" type="text/css" href="css/superfish.css" />
	<link rel="stylesheet" type="text/css" href="js/fancybox/jquery.fancybox-1.3.4.css">
	<link rel="stylesheet" type="text/css" href="js/jquery-te/jquery-te-1.4.0.css">
	<link rel="stylesheet" type="text/css" href="js/jquery.autocomplete-1.2.4/jquery.autocomplete.css" />
	<!-- <link rel="stylesheet" type="text/css" href="js/jBreadCrumb-1.1/Styles/BreadCrumb.css"></script> -->
	<!-- <link rel="stylesheet" type="text/css" href="js/jquery.treeview/jquery.treeview.css"> -->
	<link rel="stylesheet" type="text/css" href="css/style.css" />	
	<!-- Scripts -->
	<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="js/ui/jquery.ui.core.js"></script>
	<script type="text/javascript" src="js/ui/jquery.ui.widget.js"></script>
	<script type="text/javascript" src="js/ui/jquery.ui.datepicker.js"></script>
	<script type="text/javascript" src="js/ui/i18n/jquery.ui.datepicker-id.js"></script>
	<!--[if lt IE 9]><script language="javascript" type="text/javascript" src="js/jqplot/excanvas.js"></script><![endif]-->
	<script type="text/javascript" src="js/jqplot/jquery.jqplot.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.barRenderer.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.pieRenderer.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.categoryAxisRenderer.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.canvasTextRenderer.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.canvasAxisLabelRenderer.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.dateAxisRenderer.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.logAxisRenderer.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.highlighter.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.cursor.min.js"></script>
	<script type="text/javascript" src="js/jqplot/plugins/jqplot.pointLabels.min.js"></script>
	<script type="text/javascript" src="js/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
	<script type="text/javascript" src="js/jquery.doubleScroll.js"></script>
	<script type="text/javascript" src="js/jquery.maskMoney.js"></script>
	<script type="text/javascript" src="js/jquery.placeholder.min.js"></script>
	<script type="text/javascript" src="js/jquery.timer.js"></script>
	<script type="text/javascript" src="js/hoverIntent.js"></script>
	<script type="text/javascript" src="js/superfish.js"></script>
	<script type="text/javascript" src="js/jquery-te/jquery-te-1.4.0.min.js" charset="utf-8"></script>
	<!-- tidak mempergunakan jquery.autocomplete.min.js -->
	<script type="text/javascript" src="js/jquery.autocomplete-1.2.4/jquery.autocomplete.js"></script>
	<!-- <script type="text/javascript" src="js/jBreadCrumb-1.1/js/jquery.jBreadCrumb.1.1.js"></script> -->
	<!-- <script type="text/javascript" src="js/jquery.treeview/jquery.treeview.js"></script> -->
	<script type="text/javascript" src="js/index2.js"></script>
	<script type="text/javascript">
		var CFG_LIVE_PATH = '<?php echo CFG_LIVE_PATH; ?>';
		var LEVEL_AKSES = '<?php echo $_SESSION['sesipengguna']->levelAkses; ?>';
	</script>
</head>
<body style="background:url('images/pattern-batik1.jpg');">
<?php
	$starttime = microtime(true);
?>
	<div id="h">
		<div id="hContent">
			<div id="hInformation">
				<img src="images/icons-lg/warning.png" width="64" height="64">
				<p id="hMsg"></p>
				<br>
			</div>
			<div id="hTitle">
				<a href="index2.php"><?php echo CFG_SITE_SHORT_NAME; ?></a>
			</div>
<?php 
	//Menu
	if (!$_SESSION['sesipengguna']->defaultPassword) {
?>
			<div id="hMenuLeft">
				<ul class="sf-menu">
					<li>
						<a href="#">Pengguna</a>
						<ul>
							<li>
								<a href="index2.php?act=pengguna&task=editpassword">Ubah Kata Kunci</a>
							</li>
						</ul>
					</li>
<?php
		switch ($_SESSION['sesipengguna']->levelAkses) {
			case 'Administrator':
?>
					<li>
						<a href="#">Administrasi</a>
						<ul>
							<li>
								<a href="#">Organisasi</a>
								<ul>
									<li>
										<a href="index2.php?act=satuankerja">Satuan Kerja</a>
									</li>
									<li>
										<a href="index2.php?act=bidang&page=1">Bidang</a>
									</li>
									<li>
										<a href="index2.php?act=subbidang&page=1">Sub Bidang</a>
									</li>
								</ul>
							</li>
							<li class="sf-separator">
								<a href="index2.php?act=pengguna&page=1">Pengguna</a>
							</li>
							<li>
								<a href="index2.php?act=rekening&page=1">Rekening Retribusi</a>
							</li>
							<li>
								<a href="index2.php?act=jenissuratizin&page=1">Jenis Surat Izin</a>
							</li>
							<li>
								<a href="index2.php?act=grup&page=1">Grup Surat Izin</a>
							</li>
							<li class="sf-separator">
								<a href="index2.php?act=loket&page=1">Loket Pelayanan</a>
							</li>
							<li>
								<a href="index2.php?act=eventviewer&page=1">Event Viewer</a>
							</li>
							<li class="sf-separator">
								<a href="javascript:backup();">Backup Basisdata</a>
							</li>
							<li>
								<a href="index2.php?act=konfigurasi&page=1">Konfigurasi</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="#">Master Data</a>
						<ul>
							<li class="sf-separator">
								<a href="#">Lokasi</a>
								<ul>
									<li>
										<a href="index2.php?act=negara&page=1">Negara</a>
									</li>
									<li>
										<a href="index2.php?act=provinsi&page=1">Provinsi</a>
									</li>
									<li>
										<a href="index2.php?act=kabupaten&page=1">Kabupaten/Kota</a>
									</li>
									<li>
										<a href="index2.php?act=kecamatan&page=1">Kecamatan</a>
									</li>
									<li>
										<a href="index2.php?act=kelurahan&page=1">Kelurahan/Desa</a>
									</li>
								</ul>
							</li>
							<li class="sf-separator">
								<a href="#">Jenis</a>
								<ul>
									<li>
										<a href="index2.php?act=jenisgedung&page=1">Jenis Gedung</a>
									</li>
									<li>
										<a href="index2.php?act=izindimiliki&page=1">Jenis Izin yang Dimiliki</a>
									</li>
									<li>
										<a href="index2.php?act=jeniskendaraan&page=1">Jenis Kendaraan</a>
									</li>
									<li>
										<a href="index2.php?act=jenismodal&page=1">Jenis Modal</a>
									</li>
									<li>
										<a href="index2.php?act=jenispariwisata&page=1">Jenis Pariwisata</a>
									</li>
								</ul>
							</li>
							<li class="sf-separator">
								<a href="#">Klasifikasi</a>
								<ul>
									<!-- <li>
										<a href="index2.php?act=klui&page=1">KLUI</a>
									</li> -->
									<li>
										<a href="index2.php?act=kbli&page=1">Klasifikasi Lapangan Usaha</a>
									</li>
									<li class="sf-separator">
										<a href="index2.php?act=tdiklasifikasi&page=1">Klasifikasi TDI</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="index2.php?act=bank&page=1">Bank</a>
							</li>
							<li>
								<a href="index2.php?act=bentukperusahaan&page=1">Bentuk Perusahaan</a>
							</li>
							<li>
								<a href="index2.php?act=bidangusaha&page=1">Bidang Usaha</a>
							</li>
							<li>
								<a href="index2.php?act=groupperusahaan&page=1">Group Perusahaan</a>
							</li>
							<li>
								<a href="index2.php?act=limbahcair&page=1">Limbah Cair</a>
							</li>
							<li>
								<a href="index2.php?act=produk&page=1">Produk</a>
							</li>
							<li class="sf-separator">
								<a href="index2.php?act=satuan&page=1">Satuan</a>
							</li>
							<li>
								<a href="index2.php?act=kalender&page=1">Kalender</a>
							</li>
						</ul>
					</li>
					<!-- <li>
						<a href="index2.php?act=impor">Impor Data</a>
					</li> -->
<?php
				break;
			case 'Pejabat':
?>

					<li>
						<a href="index2.php?act=pengaduan&page=1">Pengaduan</a>
					</li>
<?php 
				if ($_SESSION['sesipengguna']->aksesLaporan) {
?>
					<li>
						<a href="#">Laporan</a>
						<ul>
							<li>
								<a href="index2.php?act=laporan&task=daftar">Daftar Perizinan</a>
							</li>
							<li class="sf-separator">
								<a href="index2.php?act=laporan&task=realisasi">Realisasi Pelayanan Izin</a>
							</li>
							<li>
								<a href="index2.php?act=laporan&task=masaberlaku">Masa Berlaku Surat Izin</a>
							</li>
							<li class="sf-separator">
								<a href="index2.php?act=laporan&task=pengurusan">Pengurusan Surat Izin</a>
							</li>
							<li>
								<a href="index2.php?act=laporan&task=bebanadministrasi">Beban Administrasi</a>
							</li>
						</ul>
					</li>
<?php 
				}
				break;
			case 'Petugas Administrasi':
?>
					<li>
						<a href="#">Administrasi</a>
						<ul>
							<li class="sf-separator">
								<a href="#">Lokasi</a>
								<ul>
									<li>
										<a href="index2.php?act=negara&page=1">Negara</a>
									</li>
									<li>
										<a href="index2.php?act=provinsi&page=1">Provinsi</a>
									</li>
									<li>
										<a href="index2.php?act=kabupaten&page=1">Kabupaten/Kota</a>
									</li>
									<li>
										<a href="index2.php?act=kecamatan&page=1">Kecamatan</a>
									</li>
									<li>
										<a href="index2.php?act=kelurahan&page=1">Kelurahan/Desa</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="index2.php?act=jenissuratizin&task=list&page=1">Jenis Surat Izin</a>
							</li>
							<li class="sf-separator">
								<a href="index2.php?act=permohonan&page=1">Daftar Permohonan</a>
							</li>
							<li>
								<a href="index2.php?act=perusahaan&task=list&page=1">Perusahaan</a>
							</li>
						</ul>
					</li>
					<li>
						<a href="index2.php?act=bukuregister">Buku Register</a>
					</li>
<?php 
				break;
			case 'Petugas Loket Informasi':
?>
					<li>
						<a href="index2.php?act=jenissuratizin&task=list&page=1">Jenis Surat Izin</a>
					</li>
<?php
				break;
			case 'Petugas Loket Pelayanan':
?>
					<li>
						<a href="#">Pelayanan</a>
						<ul>
							<li class="sf-separator">
								<a href="index2.php?act=jenissuratizin&task=list&page=1">Jenis Surat Izin</a>
							</li>
							<li>
								<a href="index2.php?act=permohonan&task=add">Permohonan Baru</a>
							</li>
							<li>
								<a href="index2.php?act=permohonan&page=1">Daftar Permohonan</a>
							</li>
						</ul>
					</li>
<?php
				break;
			case 'Petugas Loket Pengaduan':
?>
					<li>
						<a href="index2.php?act=pengaduan&page=1">Pengaduan</a>
					</li>
<?php
				break;
			case 'Petugas Loket Penyerahan':
?>
					<li>
						<a href="#">Pelayanan</a>
						<ul>
							<li class="sf-separator">
								<a href="index2.php?act=jenissuratizin&task=list&page=1">Jenis Surat Izin</a>
							</li>
							<li>
								<a href="index2.php?act=penyerahan&page=1">Daftar Penyerahan</a>
							</li>
						</ul>
					</li>
<?php
				break;
			case 'Surveyor':
?>
					<li>
						<a href="#">Pelayanan</a>
						<ul>
							<li class="sf-separator">
								<a href="index2.php?act=jenissuratizin&task=list&page=1">Jenis Surat Izin</a>
							</li>
							<li>
								<a href="index2.php?act=survey&page=1">Daftar Survey</a>
							</li>
						</ul>
					</li>
<?php
				break;
		}
?>
				</ul>
			</div>
<?php 
	}
?>
			<div id="hMenuRight">
				<img src="images/icons/user.png" style="vertical-align:bottom;" /> Selamat datang <b><?php echo $_SESSION['sesipengguna']->nama; ?></b>. <a class="hLink" href="index2.php?act=pengguna&task=logout&html=0">Logout?</a>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div id="b">
		<div id="bContent">
<?php 
	if (CFG_DEBUG) {
		echo '<br><br>';
	}
?>
<!-- Start main body -->
<?php
	//Workaround for : Unsafe use of variable in call include()/require()
	if ($app->detail == '') {
		define('PG_PATH', CFG_ABSOLUTE_PATH."/acts/".$app->act."/controller.".$app->act.".php");
	} else {
		define('PG_PATH', CFG_ABSOLUTE_PATH."/acts/".$app->act."/detail/".$app->detail."/controller.".$app->detail.".php");
	}
	
	if (file_exists(PG_PATH)) {
		require_once(PG_PATH);
	} else {
		$app->showPageError();
	}
?>
<!-- End main body -->
			<br><br>
		</div>
	</div>
<?php
	$endtime = microtime(true);
	$duration = $endtime - $starttime;
?>
	<div id="f">
		<div id="fContent">
			<?php echo CFG_COMPANY_NAME; ?>, 2013 - <?php echo date('Y'); ?>. This page took <?php echo round($duration, 4); ?> seconds to load.
		</div>
	</div>
</body>
</html>
<?php
}

//End output buffering and send our HTML to the browser as a whole
ob_end_flush();
?>