<?php
//Konfigurasi situs
ini_set('memory_limit','64M');
define('CFG_SITE_NAME', 'Sistem Informasi Manajemen Pelayanan');
define('CFG_SITE_SHORT_NAME', 'SIMPEL');
define('CFG_SITE_VERSION', '1.0');
define('CFG_ABSOLUTE_PATH', '/opt/lampp/htdocs/simpel');
define('CFG_UPLOAD_PATH', '/opt/lampp/htdocs/simpel');
define('CFG_LIVE_PATH', 'http://10.10.10.253/');
define('CFG_SESSION_NAME', 'simpel');
define('CFG_DEBUG', 0);
define('CFG_GESHI', 1);				//Generic Syntax Highlighter
define('CFG_TIME_LIMIT', 3600);
define('CFG_RESTRICTED_ACCESS', '<div class="halaman"><br/><br/><br/><p style="text-align:center;">Halaman ini tidak dapat diakses. Silahkan <a href="index.php">login</a>.</p></div>');

//Konfigurasi database
define('CFG_DATABASE', 'simpel_v3');
define('CFG_HOST', 'localhost');
define('CFG_USER', 'root');
define('CFG_PASSWORD', 'bpmp2t');

//Konfigurasi pemilik situs
define('CFG_COMPANY_NAME', 'Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu Kabupaten Pelalawan');
define('CFG_COMPANY_SHORT_NAME', 'DPMPTSP');

//Konfigurasi upload
define('CFG_UPLOAD_MAX_SIZE', 8);	//Check php.ini, memory_limit, post_max_size dan upload_max_filesize

//Tipe untuk Event Viewer
define('EV_INFORMASI', 0);
define('EV_PERINGATAN', 1);
define('EV_KESALAHAN', 2);

//Lainnya
//PG_PARENT dan PG_PATH
?>
