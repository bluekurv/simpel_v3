<?php
/*
==============================================================
SISTEM INFORMASI MANAJEMEN PELAYANAN
BADAN PENANAMAN MODAL DAN PELAYANAN PERIJINAN TERPADU (BPMP2T)
KABUPATEN PELALAWAN
==============================================================

pak minta tolong ditambahkan di dalam format STDB agar bisa di isikan max. 25 no surat tanah
untuk lebih jelasnya saya lampirkan contoh apabila permohonan STDB melebihi satu(dalam contoh 5 surat tanah)

Format nomor tdg mengikuti format seperti IMB atau situ
Contoh : 137/BPMP2T/TDG/II/2016/XX

Timpakan
- acts/tdg, acts/budidayaperkebunan, acts/detailbudidayaperkebunan, acts/laporan, acts/bukuregister, acts/izinkerjabidan, acts/grup
- tabel baru untuk budidayaperkebunan dan detailbudidayaperkebunan
- tabel grup
- update field
	ALTER TABLE `airpermukaan` ADD `airGrupID` INT NOT NULL AFTER `airDmhnID`;
	ALTER TABLE `budidayaperkebunan` ADD `bpGrupID` INT NOT NULL AFTER `bpDmhnID`;
	ALTER TABLE `imb` ADD `imbGrupID` INT NOT NULL AFTER `imbDmhnID`;
	ALTER TABLE `pariwisata` ADD `pariwisataGrupID` INT NOT NULL AFTER `pariwisataDmhnID`;
	ALTER TABLE `pemecahanimb` ADD `pemecahanimbGrupID` INT NOT NULL AFTER `pemecahanimbDmhnID`;
	ALTER TABLE `reklame` ADD `reklameGrupID` INT NOT NULL AFTER `reklameDmhnID`;
	ALTER TABLE `situho` ADD `situhoGrupID` INT NOT NULL AFTER `situhoDmhnID`;
	ALTER TABLE `situ` ADD `situGrupID` INT NOT NULL AFTER `situDmhnID`;
	ALTER TABLE `siup` ADD `siupGrupID` INT NOT NULL AFTER `siupDmhnID`;
	ALTER TABLE `tdg` ADD `tdgGrupID` INT NOT NULL AFTER `tdgDmhnID`;
	ALTER TABLE `tdi` ADD `tdiGrupID` INT NOT NULL AFTER `tdiDmhnID`;
	ALTER TABLE `tdp` ADD `tdpGrupID` INT NOT NULL AFTER `tdpDmhnID`;
	ALTER TABLE `tduk` ADD `tdukGrupID` INT NOT NULL AFTER `tdukDmhnID`;

TRIP 1 19/06
TRIP 2 26/06
TRIP 3 03/07

buatkan editor untuk impor

NOTE:
? Permission 777 untuk folder 'backup' dan 'files'
- Kode kelurahan/desa tanpa referensi, sehingga perlu dicari referensinya. 
- javascript nama filenya mengandung nama reklame diblokir oleh adBlock
- Nomor urut lengkap permohonan berdasarkan yyyymmxxxx, bukan aplikasi sebelumnya yyyy[jsiNomor]xxxx
- KONSEP FLOWNYA ADALAH: 
	ketika entri data baru, nilai default-nya di-load dari tabel perusahaan.
	ketika simpan di-save ke tabel surat izinnya dan perusahaan
	ketika cetak di-load dari tabel surat izin
	disahkan oleh pejabat, lalu dikasi ke petugas loket penyerahan.
- Nomor urut permohonan
  yg tidak ada no 07 
  yg tidak ada no 10 Izin Antar Kerja Antar Daerah
- >= januari 2013 = bpmp2t
  <januari 2013   = kppt

==============================================================

NOTE: problem ketika impor adalah... 
	  pada tabel m_tree ada record dengan field deleted bernilai 1, sehingga record tersebut tidak muncul di tabel m_kbli
NOTE: kbli 2009 cetakan 3 ada yang diinput manual, juga yang gapok 79 itu ke kat N

==============================================================

NOTE...setoran retribusi bisa bernilai pakai persentase (%). satuan untuk kolom jumlah, misalnya m<sup>2</sup>

07-07-2014
v entri setoran retribusi imb, maksimal 2 digit di belakang koma
v entri setoran retribusi imb, masukan persentase misalnya 3,5 dengan satuan %
v cetak imb dipisah jd 3 file

v update system.php acts/permohonan acts/imb
v update database 
	ALTER TABLE  `pungutanimb` ADD  `pimbSatuanLuas` VARCHAR( 50 ) NOT NULL AFTER  `pimbSatuan3`;

v garis pada lampiran jk font naik
v laporan siup, ditambahkan nilai investasi, total per kecamatan dan grand total
- laporan, tambahkan filter nama, kecamatan, nilai investasi, sifat
- cek menu Laporan Masa Berlaku Surat Izin

- update controller.imb.php, sehingga semua retribusi yg sudah diisi harus di-save ulang agar rekapnya benar
- system.php
- semua acts/

INI CATATAN LAMA:
v ho batas sempadan jangan auto uppercase namun mengikuti entri dari user
v ho rekom dari tim teknis bisa banyak
v ho mengingat no 11 berubah. lebih baik bikin open sehingga bisa diubah sendiri
v tdp, pencarian kbli bisa diketik, jangan hanya tree.

01-07-2014
v rekap keseluruhan retribusi perbulan/pertahun
- apabila terjadi kehilangan sertifikat yg telah terbit, apakah sistem kita menyediakan format tertentu atau form tertentu untuk menerbitkan sertifikat pengganti?
	USUL: Jika pemilik IMB kehilangan surat izin tersebut bisa mengurus lagi ke Dinas Perizinan dimana bangunan berada atau didirikan. 
		  Pemilik yang kehilangan akan mendapatkan "duplikat dokumen IMB" sebagai "surat penganti sah izin mendirikan bangunan".
		  SEHINGGA: Setelah cetak dokumen, scan lalu simpan. pergunakan untuk legalisir.
		  		 	ATAU pergunakan duplikat

					legalisir
					-> dicatatkan nomor urut legalisir berdasarkan yyyymmxxxx, tanggal legalisir, [nomor pendaftaran, surat izinnya]-> dmhnID. print pdf dengan pengaturan lokasi teks
					atau
					duplikat
					atau
					baru
					
					'137/KPPT/Reg.IR/XI/2009/000001';

	TABEL:
	CREATE TABLE IF NOT EXISTS `legalisir` (
	  `legID` int(11) NOT NULL AUTO_INCREMENT,
	  `legDmhnID` int(11) NOT NULL,
	  `legNoUrut` int(11) NOT NULL,
	  `legTgl` date NOT NULL,
	  PRIMARY KEY (`legID`)
	) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

- ada masukan dari pak rahmat, bahwa setiap pemohon yang datang di BPMP2T,agar terdata, terlepas dari apakah berkasnya diterima(lengkap) atau ditolak (tidak lengkap), 
  sehingga bisa menjadi tolak ukur kinerja pelayanan (tamu yang datang),bagaimana menurut Bapak solusi untuk permintaan ini?
  contoh: di Bank, kita akan mendapatkan nomor antrian, sehingga berapa orang yang datang hari itu akan diketahui jumlahnya,dan bisa di data per tahun/bulan,terlepas apakah mereka hanya ingin mengambil brosur atau menabung

  

29-06-2014
- lampiran imb
	? poin III.1 lama pekerjaan?
	+ folder /acts/imb/
	+ file /images/bg_logo.jpg
	+ file /system.php
	+ UPDATE 
		index2.js
	+ DATABASE
		ALTER TABLE  `imb` ADD  `imbPungutanBendaharaID` INT NOT NULL ,
		ADD  `imbPungutanMengetahuiID` INT NOT NULL;

		ALTER TABLE  `imb` ADD  `imbPungutanNama` VARCHAR( 500 ) NOT NULL;
		ALTER TABLE  `imb` ADD  `imbPungutanJumlah` INT NOT NULL;

		CREATE TABLE IF NOT EXISTS `pungutanimb` (
		  `pimbImbID` int(11) NOT NULL,
		  `pimbNama` varchar(500) NOT NULL,
		  `pimbNilai1` decimal(18,2) NOT NULL,
		  `pimbSatuan1` varchar(50) NOT NULL,
		  `pimbNilai2` decimal(18,2) NOT NULL,
		  `pimbSatuan2` varchar(50) NOT NULL,
		  `pimbNilai3` decimal(18,2) NOT NULL,
		  `pimbSatuan3` varchar(50) NOT NULL,
		  `pimbNilaiRupiah` int(11) NOT NULL,
		  `pimbUnit` int(11) NOT NULL
		) ENGINE=InnoDB DEFAULT CHARSET=latin1;

? Aturan penomoran retribusi

18-06-2014
- Data 2005-Maret 2014 retribusi. di print dari sistem lama, di impor / di entri manual.
? ASK: Survei bisa diinput. atau scan?
? Data tidak boleh dihapus

15-06-2014 (CEK FOLDER JG) (CEK HASIL IMPOR DATABASE) (CEK LAPORAN YANG DIHASILKAN) 
v IMB ada perseorangan dan perusahaan. Revisi di formulir pendaftaran.
	Ditambahkan oleh Administrator
	Bikinkan program pemindahnya, serta mungkinkan untuk edit nama dan alamat perusahaan.
v Penomoran TDP harus unik
	12 digit
	--> controller.tdp.php baris 193 itu 0416 apa? revisi 200 s/d 204
v Laporan, list pilihan jenis surat izin terurut berdasarkan abjad
v Laporan, imb, kolom 8 lokasi bangunan
- Laporan, reklame, kolom 8 berisikan lokasi pemasangan
- Laporan, siup, baris total per kecamatan serta grand total
- Laporan (Daftar Perizinan)
	? filter: data [nama pemohon/nama perusahaan]
	? filter: kecamatan
	? filter: nilai investasi. 
	? filter: bidang usaha
	? filter: sifat
v Pada entri siup [+ jg tdp], kbli 2000 dan kbli 2005 (cetakan iii, 2006) 
	ALTER TABLE  `kblipublikasi` ADD  `kpubPublikasi` BOOLEAN NOT NULL
  Login sbg admin, set publikasi ke 0 atau 1 sesuai kebutuhan
v Pada entri tdp [+jg siup], mengacu ke siup bisa sbb:
	Ketikkan kode pada textbox di atas ini atau silahkan telusuri daftar di bawah ini, kemudian klik kode yang diinginkan
v HO, batas sempadan jangan otomatis uppercase
v HO, rekomendasi dapat lebih dari 1
	Setiap rekomendasi ditulis dan diakhiri dengan menekan tombol ENTER
v HO, ybs=yang bersangkutan
v HO, isi mengingat bisa customize sendiri
======================================================================================
INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringHO', '<table border="0" cellpadding="0" cellspacing="0">
<tr><td width="4%">1.</td><td width="96%">Undang-undang Gangguan (Hinder Ordonatie) stablat Tahun 1926 Nomor 226 yang dirubah dengan Stablat Tahun 1949 Nomor 14 dan 450</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 32 Tahun 2004</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>5.</td><td>Undang-undang Nomor 28 Tahun 2009</td></tr>
<tr><td>6.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>7.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>8.</td><td>Peraturan Menteri Dalam Negeri Nomor 27 Tahun 2009</td></tr>
<tr><td>9.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 04 Tahun 2012</td></tr>
<tr><td>10.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</td></tr>
<tr><td>11.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014</td></tr>
<tr><td>12.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/674</td></tr>
</table>');
INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringSITU', '<table>
<tr><td width="4%">1.</td><td width="96%">Undang-undang Gangguan (Hinder Ordonatie) stablat Tahun 1926 Nomor 226 yang dirubah dengan Stablat Tahun 1949 Nomor 14 dan 450</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 32 Tahun 2004</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>5.</td><td>Undang-undang Nomor 28 Tahun 2009</td></tr>
<tr><td>6.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>7.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>8.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</td></tr>
<tr><td>9.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014</td></tr>
<tr><td>10.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/674</td></tr>
</table>');
INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringReklame', '<table>
<tr><td width="4%">1.</td><td width="96%">Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 32 Tahun 2004</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 28 Tahun 2009</td></tr>
<tr><td>5.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>6.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>7.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 20 Tahun 2007</td></tr>
<tr><td>8.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</td></tr>
<tr><td>9.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014</td></tr>
<tr><td>10.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/674</td></tr>
</table>');
INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringPariwisata', '<table>
<tr><td width="4%">1.</td><td width="96%">Undang-undang Nomor 12 Tahun 1956</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 18 Tahun 1997</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 22 Tahun 1999</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>5.</td><td>Undang-undang Nomor 34 Tahun 2000</td></tr>
<tr><td>6.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>7.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>8.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>9.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 07 Tahun 2006</td></tr>
<tr><td>10.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 10 Tahun 2012</td></tr>
<tr><td>11.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/21</td></tr>
</table>');
INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringIUI', '<table>
<tr><td width="4%">1.</td><td width="96%">Undang-undang No. 5 Tahun 1984 tentang Perindustrian</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 32 Tahun 2004 tentang Pemerintahan Daerah sebagaimana telah diubah dengan Undang-undang Nomor 12 Tahun 2008;</td></tr>
<tr><td>3.</td><td>Peraturan Pemerintah No. 17 Tahun 1986 tentang Kewenangan Pengaturan, Pembinaan dan Pengembangan Industri;</td></tr>
<tr><td>4.</td><td>Peraturan Pemerintah No. 13 Tahun 1955 tentang Pengaturan Usaha Industri;</td></tr>
<tr><td>5.</td><td>Peraturan Pemerintah Nomor 38 Tahun 2007 tentang Pembagian Urusan Pemerintahan Antara Pemerintah, Pemerintah Daerah Provinsi, dan Pemerintah Daerah Kabupaten/Kota</td></tr>
<tr><td>6.</td><td>Keputusan Menteri Perindustrian Nomor 148/M/SK/7/1995 tentang Penetapan Jenis Dan Komoditi Industri yang diproses Produksinya tidak Merusak ataupun Membahayakan Lingkungan Serta Tidak Menggunakan Sumber Daya Alam Secara Berlebihan;</td></tr>
<tr><td>7.</td><td>Keputusan Menteri Negara Lingkungan Hidup Nomor 86 Tahun 2002 tentang Pedoman Pengelolaan Lingkungan Hidup dan Upaya Pemantauan Lingkungan Hidup;</td></tr>
<tr><td>8.</td><td>Peraturan Menteri Lingkungan Hidup Nomor 11 Tahun 2006 tentang Jenis Rencana Usaha dan atau Kegiatan Yang Wajib Dilengkapi Dengan Analisis Mengenai Lingkungan Hidup;</td></tr>
<tr><td>9.</td><td>Peraturan Menteri Perindustrian Nomor 41/M-IND/PER/6/2008 tentang Ketentuan dan Tata Cara Pemberian Izin Usaha Industri,Izin Perluasan dan Tanda Daftar Industri</td></tr>
</table>');
INSERT INTO `konfigurasi` (`konfID`, `konfKey`, `konfValue`) VALUES (NULL, 'consideringIMB', '<table>
<tr><td width="4%">1.</td><td width="96%">Undang-undang Gangguan (Hinder Ordonatie) stablat Tahun 1926 Nomor 226 yang dirubah dengan Stablat Tahun 1949 Nomor 14 dan 450</td></tr>
<tr><td>2.</td><td>Undang-undang Nomor 53 Tahun 1999</td></tr>
<tr><td>3.</td><td>Undang-undang Nomor 32 Tahun 2004</td></tr>
<tr><td>4.</td><td>Undang-undang Nomor 25 Tahun 2007</td></tr>
<tr><td>5.</td><td>Undang-undang Nomor 28 Tahun 2009</td></tr>
<tr><td>6.</td><td>Peraturan Menteri Dalam Negeri Nomor 24 Tahun 2006</td></tr>
<tr><td>7.</td><td>Peraturan Menteri Dalam Negeri Nomor 20 Tahun 2008</td></tr>
<tr><td>8.</td><td>Keputusan Menteri Dalam Negeri Nomor 7 Tahun 1992</td></tr>
<tr><td>9.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 03 Tahun 2010</td></tr>
<tr><td>10.</td><td>Peraturan Daerah Kabupaten Pelalawan Nomor 04 Tahun 2012</td></tr>
<tr><td>11.</td><td>Peraturan Bupati Pelalawan Nomor 23 Tahun 2014</td></tr>
<tr><td>12.</td><td>Keputusan Bupati Pelalawan Nomor KPTS.821.2/BKD/2013/674</td></tr>
</table>');
======================================================================================
- cek 46-Finney Henry Katuari.docx
conth format retribusi.xlsx
FormatRetribusi.xlsx
LampiranIMB.docx
RETRIBUSI IMB MEI 2014.xlsx

060614
- Pemecahan IMB, ketika tidak di input data perusahaan maka tidak bisa di simpan
- IMB>> info perusahan tidak keluar di pencarian, dan ketika di input data perusahaan si pemohon maka data tersebut tidak bisa di save

gini maksudnya pak, mirip yg di point 3 tadi, info untuk data perusahaan selama ini tidak bisa di simpan,setelah di ketik maka tersimpan tapi hasilnya kosong.
ini yg disamping contohnya pak..di input bisa, setelah di input di save, namun data tersebut hilang setelah di save

contoh: 2014050023

hendri, sebelum saya edit, konfirmasi: 
1. pada izin perseorangan, maka data perusahaan tidak perlu diisi?
2. pada izin persorangan, formulir pendaftaran di bagian nama perusahaan dan alamat perusahaan ditandai strip '-'
3. imb, bisa diurus perseorangan dan perusahaan?
4. pemecahan imb, bisa diurus perseorangan dan perusahaan?

1.benar pak --> OK
2.jadi harus di dengan (-) saja pak ? --> saya cek.
3.untuk kasus imb, ada pemohon yg mewakilkan perusahaan pak --> dan ada jg yg tidak mewakili?
4.sama seperti imb juga, ada pemohon yang mewakilkan perusahaan dalam pengurusan (direktur) --> dan ada jg yg tidak mewakili? 

..?
biasanya kalau perusahaan yg mengurus imb ada perwakilannya pak.jadi kalau mis: PT.AGA mengurus imb, maka permohonannya diwakilkan oleh seseorang (direktur).
jg ada mis:JONI, mengurus untuk bangunannya pribadi? 
yg seperti biasanya kalau pemohon hanya untuk pribadi maka data perusahaan tidak di isikan pak.

SIP. dipahami.

- setiap kata Ijin di rubah menjadi izin, kecuali di nama Badan

280514
v. HO dan SITU. butir membaca, point 2 dan seterusnya bisa dimasukkan secara manual
   - DATABASE: ALTER TABLE  `situ` ADD  `situRekomendasiCamat` VARCHAR( 255 ) NOT NULL AFTER  `situStatusTempatUsaha`
2. cek email dari hendrik. format retribusi dan lampiran.
3. IMB. pd loket penerimaan berkas, ketika input data perusahaan tidak bisa tersimpan. 
   pd laporan, tidak bisa ditampilkan imb atas nama perusahaan tersebut.
4. semua kata IJIN menjadi IZIN, kecuali yang mengandung nama badan: badan penanaman modal dan pelayanan perIJINan terpadu
5. HO. butir 11, peraturan bupati pelalawan no 23 tahun 2014. --> BUAT EDITABLE
v. HO. batas2 huruf tanpa strtoupper. 

020514:
1. input data perusahaan di loket administrasi, ketika dimasukan alamat perusahaan, maka ketika di layout print terjadi eror data pak
contoh :
-alamat jl. teratai atas, sukajadi pekanbaru
-alamat pemasangan: >> (error) akan terisi oleh data perusahaannya, seperti yg saya blok

2. kemudian data di kolom tembusan juga akan terjadi effect yg sama,
contoh:
tembusan:
- camat sukajadi

3. minta rapikan antara 1 spasi bunyi butir kedua dan ketiga

TAMBAHAN: frame.set.php, frame.menu.php, frame.css, view.permohonan.php

160414:
- tdp, alamat perusahaan, dituliskan sesuai tingkatnya. KEL. atau DESA
  apakah berdampak ke semuanya?

080414:
- pertanyaan pak:
1. kenapa yg di laporan IMB tidak lengkap untuk kecamatannya pak
2. untuk nama perusahaan juga tidak lengkap

050314:
v siup. entri poin 11. pake format baru.
v tdp. untuk perpanjangan, maka pakai nomor lama. artinya bisa berulang.
v situ. cetak, nomor hilang. 

180214:
v ALTER TABLE  perusahaan ADD  perOperasional BOOLEAN NOT NULL;
v UPDATE perusahaan SET  perOperasional =  '1';
v ALTER TABLE  pengguna ADD  pnAksesAdministrasi VARCHAR( 500 ) NOT NULL AFTER  pnDefaultPengesahanLaporan;
v CREATE TABLE IF NOT EXISTS siupbidangusaha (
  sbuSiupID int(11) NOT NULL,
  sbuTabel varchar(50) NOT NULL,
  sbuKbliID int(11) NOT NULL,
  sbuKbliKode varchar(50) NOT NULL,
  sbuKbliNama varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

v update acts/* dan css/style.css

190214:
v ALTER TABLE  tdp ADD  tdpNoLengkap VARCHAR( 50 ) NOT NULL AFTER  tdpNo;
v ALTER TABLE  tdi ADD  tdiNoLengkap VARCHAR( 50 ) NOT NULL AFTER  tdiNo;
v ALTER TABLE  siup ADD  siupNoLengkap VARCHAR( 50 ) NOT NULL AFTER  siupNo;
v ALTER TABLE  situho ADD  situhoNoLengkap VARCHAR( 50 ) NOT NULL AFTER  situhoNo;
v ALTER TABLE  situ ADD  situNoLengkap VARCHAR( 50 ) NOT NULL AFTER  situNo;
v ALTER TABLE  reklame ADD  reklameNoLengkap VARCHAR( 50 ) NOT NULL AFTER  reklameNo;
v ALTER TABLE  pariwisata ADD  pariwisataNoLengkap VARCHAR( 50 ) NOT NULL AFTER  pariwisataNo;
v ALTER TABLE  pemecahanimb ADD  pemecahanimbNoLengkap VARCHAR( 50 ) NOT NULL AFTER  pemecahanimbNo;
v ALTER TABLE  imb ADD  imbNoLengkap VARCHAR( 50 ) NOT NULL AFTER  imbNo;

v ALTER TABLE  perusahaan ADD  perRT INT NOT NULL AFTER  perKelurahanID ,
v ADD  perRW INT NOT NULL AFTER  perRT;
v ALTER TABLE  perusahaan ADD  perRTPemilik INT NOT NULL AFTER  perKelurahanPemilikID ,
v ADD  perRWPemilik INT NOT NULL AFTER  perRTPemilik;

v ALTER TABLE perusahaan  ADD perKepemilikanPabrik VARCHAR(255) NOT NULL AFTER perAkteTglPenerimaan,  ADD perKelurahanLokasiID INT NOT NULL AFTER perKepemilikanPabrik,  ADD perLuasBangunan DOUBLE(18,2) NOT NULL AFTER perKelurahanLokasiID,  ADD perLuasTanah DOUBLE(18,2) NOT NULL AFTER perLuasBangunan,  ADD perTenagaPenggerak VARCHAR(255) NOT NULL AFTER perLuasTanah,  ADD perKlasifikasi VARCHAR(255) NOT NULL AFTER perTenagaPenggerak,  ADD perKomoditiIndustri VARCHAR(255) NOT NULL AFTER perKlasifikasi,  ADD perInventaris BIGINT NOT NULL AFTER perKomoditiIndustri,  ADD perKapasitasProduksiPerTahun BIGINT NOT NULL AFTER perInventaris,  ADD perSatuanKapasitasProduksiPerTahun VARCHAR(50) NOT NULL AFTER perKapasitasProduksiPerTahun,  ADD perTenagaKerjaIndonesiaLaki INT NOT NULL AFTER perSatuanKapasitasProduksiPerTahun,  ADD perTenagaKerjaIndonesiaPerempuan INT NOT NULL AFTER perTenagaKerjaIndonesiaLaki,  ADD perTenagaKerjaAsingLaki INT NOT NULL AFTER perTenagaKerjaIndonesiaPerempuan,  ADD perTenagaKerjaAsingPerempuan INT NOT NULL AFTER perTenagaKerjaAsingLaki;

v CREATE TABLE IF NOT EXISTS iui (
  iuiID int(11) NOT NULL AUTO_INCREMENT,
  iuiDmhnID int(11) NOT NULL,
  iuiNo int(11) NOT NULL,
  iuiNoLengkap varchar(50) COLLATE utf8_bin NOT NULL,
  iuiKepemilikanPabrik varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  iuiKelurahanLokasiID int(11) NOT NULL,
  iuiLuasBangunan double(18,2) NOT NULL DEFAULT '0.00',
  iuiLuasTanah double(18,2) NOT NULL DEFAULT '0.00',
  iuiTenagaPenggerak varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  iuiKluiID int(11) NOT NULL COMMENT 'Jenis Industri',
  iuiKomoditiIndustri varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  iuiInventaris bigint(20) NOT NULL,
  iuiKapasitasProduksiPerTahun bigint(20) NOT NULL,
  iuiSatuanKapasitasProduksiPerTahun varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  iuiTenagaKerjaIndonesiaLaki int(11) NOT NULL DEFAULT '0',
  iuiTenagaKerjaIndonesiaPerempuan int(11) NOT NULL DEFAULT '0',
  iuiTenagaKerjaAsingLaki int(11) NOT NULL DEFAULT '0',
  iuiTenagaKerjaAsingPerempuan int(11) NOT NULL DEFAULT '0',
  iuiKeterangan text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  iuiTglBerlaku date NOT NULL,
  iuiTglDaftarUlang date NOT NULL,
  iuiPejID int(11) NOT NULL,
  iuiTglPengesahan date NOT NULL,
  iuiArsip varchar(500) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (iuiID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

v CREATE TABLE IF NOT EXISTS mesiniui (
  msnID int(11) NOT NULL AUTO_INCREMENT,
  msnIuiID int(11) NOT NULL,
  msnJenis varchar(255) NOT NULL,
  msnJumlah int(11) NOT NULL,
  msnMerek varchar(255) NOT NULL,
  msnSpesifikasiNegaraAsal varchar(255) NOT NULL,
  msnKapasitasMesin decimal(18,2) NOT NULL,
  msnSatuanKapasitasMesin varchar(255) NOT NULL,
  msnHarga bigint(20) NOT NULL,
  PRIMARY KEY (msnID)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

v ALTER TABLE  `perusahaan` ADD  `perTempatLahirPemilik` VARCHAR( 255 ) NOT NULL AFTER  `perPekerjaanPemilik` ,
ADD  `perTglLahirPemilik` DATE NOT NULL AFTER  `perTempatLahirPemilik`

v update acts/*
v update index.php
v update index2.php
v update index3.php

- masukkan kelurahan baru di kecamatan pangkalan kerinci
  dengan kode 00, nama NOT FOUND
  pergunakan id-nya (2306) untuk impor

- Ada perubahan kelurahan pada Kabupaten Pelalawan. 
  ukui 1
  ?
  CEK ULANG KELURAHANNYA! bandingkan dengan pdf dari pemkab...

Manual entri : 51430 Perdagangan Besar Bahan-Bahan Konstruksi
Manual entri : 56140 dijadikan 55240 - Kedai Makanan Dan Minuman

yang tidak ada di id_tree?
- 679 681 773 792 

- SITUHO: 22,21,23,20, font 11, cell 1.10

- SITUHO: jika kedepannya masih kesulitan, maka akan saya tambahkan di setting (sebelah ini), berupa pengaturan halaman 2 (font dan cell height).

060314
- agar buku register tidak menampilkan yang belum di-save
- agar tdp, bentuk badan wajib diisi

- LAPORAN terkait SURVEY berisikan
	berapa yg disurvey...
	diterima, ditolak, pendingnya

v tdp filter bentuk badan
	buku register, filter by tahun dari tgl pengesahan

v reklame, pemecahan imb..bisa ngurus lebih dr 1

ALTER TABLE  `detailpermohonan` ADD  `dmhnTambahan` BOOLEAN NOT NULL;

v siup, jabatan. 
- tes setiap hasil import
- bagaimana cara memindahkan lokal master yg sudah dientri, ke server pelalawan
- bagaimana juga cara impor-nya
- informasikan (manual) bahwa data sebelum 2014 

ALTER TABLE  `reklame` CHANGE  `reklameNo`  `reklameNo` INT( 11 ) NOT NULL;
ALTER TABLE  `tdg` CHANGE  `tdgNo`  `tdgNo` INT( 11 ) NOT NULL;

//12-03-2014
ALTER TABLE  `pemecahanimb` ADD  `pemecahanimbNama` VARCHAR( 500 ) NOT NULL AFTER  `pemecahanimbNoIMBInduk` ,
ADD  `pemecahanimbUmur` INT NOT NULL AFTER  `pemecahanimbNama` ,
ADD  `pemecahanimbPekerjaan` VARCHAR( 500 ) NOT NULL AFTER  `pemecahanimbUmur` ,
ADD  `pemecahanimbAlamat` VARCHAR( 500 ) NOT NULL AFTER  `pemecahanimbPekerjaan` ,
ADD  `pemecahanimbKelurahanID` INT NOT NULL AFTER  `pemecahanimbAlamat`;

ALTER TABLE  `imb` CHANGE  `imbNoSertifikat`  `imbNoSertifikat` TEXT CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL;

==============================================================

NEXT:
- surveyor, berdasarkan tim-nya (group-nya) yang berisi sejumlah. ada print-out nya.
- Tambahkan where condition utk laporan terkait status operasional.
  dan permohonan dari perusahaan yang tidak operasional akan ditolak
- jika dalam sekian waktu belum, ada denda, barulah keluar izinnya. apakah ini terdetect di awal entri permohonan
- TDI lembar ke-3? DAFTAR BAHAN DAN BAHAN PENOLONG PERTAHUN
- setoran retribusi imb
- impor kbli 2005 cetakan 3, masukkan juga cetakan sebelumnya

Buku Register TDP --> ada filter cv, pt, dll

==============================================================

18-03-2014
- Di imb: ada perseorangan dan ada perusahaan (atas nama direktur)
  hendri, ntar di print outnya (utk perusahaan) seperti apa tulisannya? apakah ada nama perusahaan?
  sebenarnya print out ga ada masalah pak dengan yg sekarang biasanya nanti pengaruhnya adalah:

  saya contohkan:
  seperti yg di samping pak, namun kalo kita tetap pakai format yg sekarang di pendaftaran td, 
  apakah tidak berpengaruh dengan laporan tahunan untuk imb? 
  (ketika kita ingin mensortir sudah berapa banyak perusahaan yg mengurus imb dalam setahun misalnya)

- kemudian pak..di pendaftaran
  ada masalah baru pak
  contoh kasus:
  satu perusahaan mengurus 2 ijin SITU,SIUP dan TDP secara bersamaan untuk wilayah operasi yang berbeda.

  --> yg telah kami lakukan adalah (solusi semntara yg diberikan bg acil), input salah satu,cetak,kemudian ditandai oleh op penyerahan bahwa ijin telah selesai dan diambil,kemudian input baru dengan nama yg sama,cetak,dst
  dalam artian 2 kali input untuk satu pemohon tadi

  misal no surat:
  ijin 1. 137/II/19 -->untuk wilayah kerinci
  ijin 2. 137/II/20 -->untuk wilayah sekijang

  ---> masalah yg muncul adalah:

  ijin 1 telah dicetak dengan alamat kerinci, ijin kedua dicetak dengan alamat sekijang, namun di buku register, alamat ijin 1 akan otomatis mengikut alamat ijin 2.
  bagaimana solusinya pak? khususnya untuk perusahaan/perorangan yg megurus dua ijin atau lebih sekaligus ini?
  hendri, utk dua item tersebut, saya akan fasilitas segera 
  ok pak
  kasus ini sudah terjadi
  di pt. indomaret untuk HO, ketahuannya pas ada revisi ho untuk indomaret pak,ketika di print alamatnya ngikut ke alamat ijin yg satu lagi/terakhir di input 
  ok. 

19-03-2014
v. dst di rapikan dikit pak, ditengah MEMUTUSKAN

v. penomoran pemecahan imb ada sedikit revisi pak,
nomor yg saya saya tandai adalah sama dengan nomor terakhir nomor induk imb yg akan di pecahkan:

-137/KPPT/IMB/XII/2011/198
-19/142/BPMP2T/III/2014

menjadi : 19/198/BPMP2T/III/2014

sesuai dengan no terakir induk imb nya

==============================================================

TODO:
- pastikan data lama sudah dimasukkan, ditransfer. shg tidak blank.
- tes baru ubah dan perpanjangan
- review koding terkait perusahaan franchise, misalnya memiliki situ/ho lebih dari satu pada saat bersamaan. efeknya apa?
- kapan NPWP dan NPWPD, ataukah cuma satu yaitu NPWP?
- Hapus kelurahan 66, Gondai
? Kelurahan Bakung, di Langgam, adakah?
? Kelurahan Bukit Agung, di Pangkalan Kerinci, adakah?
? Kelurahan	Pompa Air, di Bandar Petalangan, adakah?

v ketika salah input tanggal pengesahan dan di edit lagi, tgl masa berlaku tidak otomatis terUpdate,
  tadi kasus nya pada pada TDI, itu udah saya ganti manual di DB pak, dan udah ndk ada masalah 
- permintaan otomatis isi KBLI pada siup
- pak ralat situ pak, kesalahan hanya pada no 10,
- dan di gati dengan ho no 12, hanya itu katanya pak, apa saya langsung eksekusi pak

entri yg kbli, siup.
autocomplete

- ip pelalawan pak,125.162.56.227 port 3306, user root, pass bpmp2t, 
- masalah KBLI yg saya tanya barusan sama bg hendrik itu hanya meminta otomatis mencari sesuai kata,
  gitu chat yg saya terima pak
	ya pak, masalah KBLI saya udah telpon barusan pak, yg diminta autocomplete,
	
	situho yg berubah
	yang berrubah line 367-507
	file terlampir

	yang situho udah pak,
	trs yang view.siup.php pada line 335 - 491 -> TODO: AL, ini view.situ, kan?
	udah saya update juga ke server pak

file terlampir
*******
pak, system.php juga saya ganti pak,
		$this->pageWidth 		= isset($_REQUEST['pageWidth']) 		? intval($_REQUEST['pageWidth']) 	: 215;
		$this->pageHeight 		= isset($_REQUEST['pageHeight']) 		? intval($_REQUEST['pageHeight']) 	: 330;

pada line 1291 dan 1292

masalah selanjutnya
Urut KBLI sesuia yg di input OP sama autocomplete
*******
Pada Jumat, 21 Februari 2014 11:54, Tengku Hendry Firnandes <headshootpliz@yahoo.com> menulis:
v halaman 1, full page (kalau bisa font 11/12) dan di pojok kanan bawah ada tulisan DENGAN KETENTUAN
v halaman 2, full page (font 10)

SELECT daftar_status, COUNT( * ) AS total
FROM  `trans_daftar` 
GROUP BY daftar_status
LIMIT 0 , 30

daftar_status	total
Pendaftaran		165
Pendataan		32
Verifikasi		94
Selesai			8131
Ditolak			1
Lunas			140

==============================================================

02-02-2014
v input umur data pemilik usaha, level administrasi -> permintaan jadi input tgl lahir saja, hingga menentukan umur
v input masa berlaku surat, selain siup di buat otomatis di tambah 5 thn setalah mulai berlaku surat di input, sehingga user tidak perlu menginputkan lagi, hanya buat otomatis pada nilai input pak, bukan di hapus input itu,
v pemilihan bidang usaha KBLI pada siup, permintaan tinggal pilih dan klik, 
v semua huruf perizinan di ganti lagi ke Perijinan, 

v Form upload kelengkapan dokumen disediakan satu persatu, dimana bisa lebih dari satu
27-02-2014
v. bagaimana dg terinput dua atau lebih?
   validasi, kasus perusahaan yang sama dengan permohonan perizinan yang sama berulang, misal di-hari yang sama.
   kalau perizinan nya beda, gpp.
	--> KETERANGAN: validasi sudah ditambahkan

v. ditandai masa berlaku yg - range waktu pemberitahuan 30 Hari/1 Bulan. bisa print
	--> KETERANGAN: bisa diakses oleh pengguna dengan level akses Pejabat, di menu Laporan.

v. pengaduan diarahkan...  ke pejabat yg mewakili bidang.
   apakah semua atau khusus perizinan

v kiosk multimedia -> input nomor pendaftaran, output status.
v status survey: belum disurvey, dan sudah disurvey.
v tahun pada nomor surat mengikut tanggal surat, bukan textbox
v Jika ada survey, berarti lama penyelesaian terhitung setelah survey lapangan DAN/ATAU berkas lengkap diterima
v Penulisan perijinan pada BPMP2T pelawan yang benar adalah PERIZINAN bukan PERIJINAN
	--> dikembalikan lagi ke PERIJINAN, khusus untuk entri dan cetak.
v Pada rekap / laporan SIUP di tambah kolom " Nilai Investasi "
v Format Survei = a. memenuhi syarat, b. pending dan c. batal
v Hak / level akses untuk cetak laporan dan informasi laporan realtime = Data Informasi dan Kepala Badan

==============================================================

http://localhost/simpel_v3/index2.php?act=tes
Kategori 		(alfabet)   21
Golongan Pokok 	(2 digit)   88
Golongan 		(3 digit)  241
Subgolongan 	(4 digit)  514
Kelompok 		(5 digit) 1457

JENIS SURAT IZIN USAHA PERDAGANGAN (SIUP)

SIUP KECIL : wajib dimiliki oleh Perusahaan Perdagangan dengan modal dan kekayaan bersih (netto) seluruhnya sebesar Rp. 50 Juta sampai dengan Rp. 500 Juta, tidak termasuk tanah dan bangunan tempat usaha
SIUP MENENGAH : wajib dimiliki oleh Perusahaan Perdagangan dengan modal dan kekayaan bersih (netto) seluruhnya sebesar Rp. 500 Juta sampai dengan Rp. 10 Milyar, tidak termasuk tanah dan bangunan tempat usaha
SIUP BESAR : wajib dimiliki oleh Perusahaan Perdagangan dengan modal dan kekayaan bersih (netto) seluruhnya lebih Rp. 10 Milyar, tidak termasuk tanah dan bangunan tempat usaha
SIUP MIKRO : SIUP yang dapat diberikan kepada Perusahaan Perdagangan Mikro

petugas loket informasi = formulir (x) + persyaratan (v)
petugas loket pendaftaran = cek kelengkapan dokumen
keuangan (NAMA SEMENTARA untuk PETUGAS PENERIMAAN RETRIBUSI)? tugasnya hanya mencatatkan pembayaran retribusi saja. bukan cetak skrd
petugas (verifikasi?) -> petugas administrasi untuk cetak izin..periksa berkas..tanda tangan blanko verifikasi
pejabat -> kasubid pelayanan/kabid pelayanan dan survey periksa dan paraf. kaban tandatangan.
	Sistem Pelayanan Informasi dan Perizinan Investasi Secara Elektronik (SPIPISE)
	http://id.wikipedia.org/wiki/SPIPISE
	kasubid pelayanan -> perforasi?
petugas loket penyerahan -> sertifikat/sk ijin
petugas loket pengaduan
surveyor

Izin Usaha Perdagangan berlaku selama perusahaan perdagangan masih menjalankan usahanya
Izin Usaha Perdagangan diwajibkan melaksanakan daftar ulang setiap tahun

Kelompok : Perdagangan Eceran Jamu (52314)
Sub Golongan : Perdagangan Eceran Bahan Kimia, Farmasi, Kosmetik, dan Alat Laboratorium (5231)
Golongan : Perdagangan Eceran Komoditi Bukan Makanan, Minuman, atau Tembakau (523)
Golongan Pokok : Perdagangan Eceran, Kecuali Mobil dan Sepeda Motor (52)
Kategori : G --> Perdagangan Besar dan Eceran

kepmen dalam negeri 30 tahun 1979 --> terkait kode perijinan

ANGKA TIDAK DIGUNAKAN LAGI
loket 1 informasi
2 penerimaan berkas
3 pembayaran retribusi
5 penyerahan izin

LAINNYA:

jenis font pdf
times (Times-Roman)
timesb (Times-Bold)
timesi (Times-Italic)
timesbi (Times-BoldItalic)
helvetica (Helvetica)
helveticab (Helvetica-Bold)
helveticai (Helvetica-Oblique)
helveticabi (Helvetica-BoldOblique)
courier (Courier)
courierb (Courier-Bold)
courieri (Courier-Oblique)
courierbi (Courier-BoldOblique)
symbol (Symbol)
zapfdingbats (ZapfDingbats)

1. Klasifikasi Lapangan Usaha Indonesia 1977
2. Klasifikasi Lapangan Usaha Indonesia 1983
3. Klasifikasi Lapangan Usaha Indonesia 1990
4. Klasifikasi Baku Lapangan Usaha Indonesia 1997
5. Klasifikasi Baku Lapangan Usaha Indonesia 2000
6. Klasifikasi Baku Lapangan Usaha Indonesia 2005
7. Klasifikasi Baku Lapangan Usaha Indonesia 2009

var selectedDates = {};
selectedDates[new Date('04/05/2014')] = new Date('04/05/2014');
selectedDates[new Date('05/04/2014')] = new Date('05/04/2014');
selectedDates[new Date('06/06/2014')] = new Date('06/06/2014');

$('#mhnTgl').datepicker({
	beforeShowDay: function(date) {
		var highlight = selectedDates[date];
		if (highlight) {
			return [true, "ui-datepicker-highlighted", highlight];
		} else {
			return [true, '', ''];
		}
	}
});

.ui-datepicker-week-end a {
    background-image: none !important;
    background-color: red !important;
}

ui-datepicker-highlighted a {
	background-color : red !important;
	background-image : none !important;
	color: white !important;
	font-weight: bold !important;
	font-size: 12pt;
}

<div id="breadCrumb0" class="breadCrumb module">
	<div style="overflow:hidden; position:relative;  width: 990px;">
		<div>
			<ul style="width: 5000px;">
				<li class="first">
					<a href="#">Home</a>
				</li>
				<li>
					<a href="#">Biocompare Home</a>
				</li>
				<li style="background-image: none; background-position: initial initial; background-repeat: initial initial;">
					<span style="width: 5px; display: block; overflow: hidden;"><a href="#" style="width: 99px;">Product Discovery</a></span>
				<div class="chevronOverlay" style="display: block;"></div></li>
				<li style="background-image: none; background-position: initial initial; background-repeat: initial initial;">
					<span style="width: 5px; display: block; overflow: hidden;"><a href="#" style="width: 222px;">Life Science Products / Laboratory Supplies</a></span>
				<div class="chevronOverlay" style="display: block;"></div></li>
				<li style="background-image: none; background-position: initial initial; background-repeat: initial initial;">
					<span style="width: 5px; display: block; overflow: hidden;"><a href="#" style="width: 90px;">Kits and Assays</a></span>
				<div class="chevronOverlay" style="display: block;"></div></li>
				<li>
					<a href="#">Mutagenesis Kits</a>
				</li>
				<li class="last" style="background-image: none; background-position: initial initial; background-repeat: initial initial;">
					Mutation Generation System�
				</li>
			</ul>
		</div>
	</div>
</div>

WHERE last_login < DATE_SUB(NOW(), INTERVAL 1 MONTH)

SELECT * FROM table
WHERE YEAR(date_created) = YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
AND MONTH(date_created) = MONTH(CURRENT_DATE - INTERVAL 1 MONTH)

mm = (pixels * 25.4) / dpi
pixels = (mm * dpi) / 25.4

==============================================================

NEW TRICK:
//Query
$sql = "SELECT * FROM m_pemohon
		LEFT JOIN m_tree ON m_pemohon.id_tree=m_tree.id_tree
		LEFT JOIN trans_daftar ON m_pemohon.id_pemohon=trans_daftar.id_pemohon
		LIMIT 1";
$rs = $app->query($sql);

//Array
//If two or more columns of the result have the same field names, the last column will take precedence. To access the other column(s) of the same name, you must use the numeric index of the column or make an alias for the column. For aliased columns, you cannot access the contents with the original column name.

// Fetch the table name and then the field name
$qualified_names = array();
for ($i = 0; $i < mysql_num_fields($rs); ++$i) {
	$table = mysql_field_table($rs, $i);
	$field = mysql_field_name($rs, $i);
	array_push($qualified_names, "$table.$field");
}

while(($obj = mysql_fetch_row($rs)) == true){
	$arr[] = array_combine($qualified_names, $obj);
}

Impor_View::viewImpor($arr);

================================================================
	<tr>
		<td></td>
		<td></td>
		<td colspan="2">			
			<table border="0" cellpadding="1" cellspacing="1" width="100%" style="font-family: Courier;">
			<tr>
				<td width="20%"></td>
				<td width="4%"></td>
				<td width="45%"></td>
				<td width="8%"></td>
				<td width="18%"></td>
			</tr>
			<tr>
				<td colspan="5"><?php echo $obj->imbPungutanNama; ?></td>
			</tr>
<?php 
		$jumlah = 0;
		$jumlah_dibulatkan = 0;
			
		if (count($obj->pungutan) > 0) {
			foreach ($obj->pungutan as $pungutan) {
				$nilai = array();
				$hasil = 1;
				if ($pungutan->pimbNilai1 > 0) {
					$nilai[] = $app->MySQLToMoney($pungutan->pimbNilai1, 1).' '.$pungutan->pimbSatuan1; 
					$hasil = bcmul($hasil,$pungutan->pimbNilai1);
				}
				if ($pungutan->pimbNilai2 > 0) {
					$nilai[] = $app->MySQLToMoney($pungutan->pimbNilai2, 1).' '.$pungutan->pimbSatuan2; 
					$hasil = bcmul($hasil,$pungutan->pimbNilai2);
				}
				if ($pungutan->pimbNilai3 > 0) {
					$nilai[] = $app->MySQLToMoney($pungutan->pimbNilai3, 1).' '.$pungutan->pimbSatuan3; 
					$hasil = bcmul($hasil,$pungutan->pimbNilai3);
				}
				$hasil = bcmul($hasil,$pungutan->pimbNilaiRupiah);
				$hasil = bcmul($hasil,$pungutan->pimbUnit);
				$jumlah = bcadd($jumlah, $hasil);
?>
			<tr>
				<td align="left"><?php echo $pungutan->pimbNama; ?></td>
				<td>:</td>
				<td align="right"><?php echo implode(' x ',$nilai).' x Rp. '.$app->MySQLToMoney($pungutan->pimbNilaiRupiah).($pungutan->pimbUnit > 1 ? ' x '.$pungutan->pimbUnit.' unit' : ''); ?></td>
				<td align="right">Rp.</td>
				<td align="right"><?php echo $app->MySQLToMoney($hasil); ?></td>
			</tr>
<?php 
			}
		}
?>
			<tr>
				<td colspan="3" align="center">JUMLAH</td>
				<td align="right">Rp.</td>
				<td align="right" style="border-top: 1px solid #000000;"><?php echo $app->MySQLToMoney($jumlah); ?></td>
			</tr>
<?php 
		//Pembulatan ke ratusan atas
		if ($jumlah % 100 > 0) {
			$jumlah = $jumlah - ($jumlah % 100) + 100;
		}
		$jumlah_dibulatkan = round($jumlah);
?>
			<tr>
				<td colspan="3" align="center">JUMLAH Dibulatkan</td>
				<td align="right">Rp.</td>
				<td align="right"><?php echo $app->MySQLToMoney($jumlah_dibulatkan); ?></td>
			</tr>
			<tr>
				<td>Terbilang</td>
				<td>:</td>
				<td colspan="3"><?php echo ucwords($app->terbilang($jumlah_dibulatkan)).' Rupiah'; ?></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td colspan="4">&nbsp;</td>
	</tr>
*/
?>