<?php
//Set flag for parent file
define('PG_PARENT', 1);

//Include file(s)
require_once 'configuration.php';
require_once 'system.php';;
if (CFG_GESHI) {
	require_once 'libraries/geshi.php';
}

//Create application
$app = new App();
$app->connect(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);
$app->init();

$app->query("ALTER TABLE  `situ` ADD  `situRekomendasiCamat` VARCHAR( 255 ) NOT NULL AFTER  `situStatusTempatUsaha`");
?>