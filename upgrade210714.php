<?php
//Set flag for parent file
define('PG_PARENT', 1);

//Include file(s)
require_once 'configuration.php';
require_once 'system.php';;
if (CFG_GESHI) {
	require_once 'libraries/geshi.php';
}

//Create application
$app = new App();
$app->connect(CFG_HOST, CFG_USER, CFG_PASSWORD, CFG_DATABASE);
$app->init();

$app->query("ALTER TABLE  `pemecahanimb` ADD  `pemecahanimbPungutanNama` VARCHAR( 500 ) NOT NULL ,
		ADD  `pemecahanimbPungutanJumlah` INT NOT NULL ,
		ADD  `pemecahanimbPungutanBendaharaID` INT NOT NULL ,
		ADD  `pemecahanimbPungutanMengetahuiID` INT NOT NULL;");
$app->query("ALTER TABLE  `pungutanimb` ADD  `pimbKelompok` VARCHAR( 500 ) NOT NULL AFTER  `pimbNama`;");

echo 'Done';		
?>